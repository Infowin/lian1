// Publish Methods
//publish roles
Meteor.publish(null, function() {
  return Meteor.roles.find({})
})
// Meteor.publish("Super-Admin", function() {
//   var user = Meteor.users.findOne({
//     _id: this.userId
//   });
//   if(Roles.userIsInRole(user, ["Super-Admin"])) {
//     return Meteor.users.find({}, {
//       fields: {
//         emails: 1,
//         profile: 1,
//         roles: 1
//       }
//     });
//   }
//   this.stop();
//   return;
// });

Meteor.publish("UsersList", function() {
  var user = Meteor.users.findOne({
    _id: this.userId
  });
  if(Roles.userIsInRole(user, ["Admin"])) {
    return Meteor.users.find({'roles':{$in:['Admin', 'Commission', 'Director', 'Supervisor', 'Administrative', 'Agent']}}, {
      fields: { username: 1, emails: 1, profile: 1, roles: 1, createdAt: 1, updatedAt: 1}
    });
  }
  else if(Roles.userIsInRole(user, ["Director"])) {
    return Meteor.users.find({'roles':{$in:['Supervisor', 'Administrative', 'Agent']}}, {
      fields: { username: 1, emails: 1, profile: 1, roles: 1, createdAt: 1, updatedAt: 1}
    });
  }
  else if(Roles.userIsInRole(user, ["Supervisor"])) {
    return Meteor.users.find({'roles':{$in:['Administrative', 'Agent']}}, {
      fields: { username: 1, emails: 1, profile: 1, roles: 1}
    });
  }
  else if(Roles.userIsInRole(user, ["Administrative"])) {
    return Meteor.users.find({'roles':{$in:['Agent']}}, {
      fields: { username: 1, emails: 1, profile: 1, roles: 1}
    });
  }

  this.stop();
  return;
});
Meteor.publish(null, function() {
  return Meteor.roles.find({})
})
