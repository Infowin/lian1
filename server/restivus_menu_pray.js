/*Api.addRoute('praytablea', {authRequired: false}, {
  get: function () {
    // console.log("in serverpeople"); 
    
    var countVar = Number(this.queryParams.count) || 99;
    var startVar = Number(this.queryParams.start) || 0;

    var filterVar = this.queryParams.filter || {};
    var sortVar = this.queryParams.sort || {};
 
    function chgSortObj(arr){
      for (var i in arr) {
        if (arr[i] == "asc") arr[i] = 1;
        else if (arr[i] == "desc") arr[i] = -1;

        if(i == "pray"){
          arr['ordernum'] = -1;
          // arr['year'] = arr[i];
          // arr['prayname_text'] = arr[i];
          // arr['pray2_orderid'] = arr[i];
          // arr['prayingtype_orderid'] = arr[i];
          // arr['prayserial'] = arr[i];
          
          delete arr[i];
        }
      }
      return arr;
    }
    // console.log(sortVar); 
    
    filterVar = Api.clearFilter(filterVar);
    sortVar = chgSortObj(sortVar);
    // console.log(filterVar); 
    
    console.log(sortVar); 


    var cursor = Praying1.find(filterVar, {skip: startVar, limit: countVar, sort: sortVar}).fetch();
    return {
      data: cursor,
      pos:  startVar,
      total_count:Praying1.find(filterVar).count()
    }
  }
});
Api.addRoute('praytreea', {authRequired: false}, {
  get: function () {
    // console.log("in serverpeople"); 
    
    // var countVar = Number(this.queryParams.count) || 99;
    // var startVar = Number(this.queryParams.start) || 0;

    var filterVar = this.queryParams.filter || {};
    // var sortVar = this.queryParams.sort || {};
    
    filterVar = Api.clearFilter(filterVar);
    // sortVar = chgSortObj(sortVar);
    // console.log(filterVar); 

    var cursor = Pray1.find({}, {sort:{order_id:1}}).fetch();

    for (var i = cursor.length - 1; i >= 0; i--) {
      if( Pray2.find({ class_id: cursor[i]._id }).count() ){
        if(Number(cursor[i]['now_use'])){
          cursor[i]['open'] = 1;
        }

        var cursor2 = Pray2.find({ class_id: cursor[i]._id }, {sort:{order_id:1}}).fetch();
        for (var j = cursor2.length - 1; j >= 0; j--) {
          if(!!filterVar.year){
            cursor2[j]['count'] = Praying1.find({$and:[{prayname: cursor[i]._id}, {prayitem: cursor2[j]._id}, {year: filterVar.year }]}).count();
          }
          else{
            cursor2[j]['count'] = Praying1.find({$and:[{prayname: cursor[i]._id}, {prayitem: cursor2[j]._id}]}).count();
          }
        }
        cursor[i]['data'] = cursor2;

        if(!!filterVar.year){
          cursor[i]['count'] = Praying1.find({$and:[ {prayname: cursor[i]._id}, {year: filterVar.year }]}).count();
        }
        else{
          cursor[i]['count'] = Praying1.find({prayname: cursor[i]._id}).count();
        }
      }
      else{
        cursor[i]['count'] = 0;
      }
    };

    return {
      data: cursor,
      pos:  0, //startVar,
      total_count:Pray1.find().count()
    }
  }
});
Api.addRoute('praytreea2', {authRequired: false}, {
  get: function () {
    // console.log("in serverpeople"); 
    
    // var countVar = Number(this.queryParams.count) || 99;
    // var startVar = Number(this.queryParams.start) || 0;

    var filterVar = this.queryParams.filter || {};
    // var sortVar = this.queryParams.sort || {};
    
    filterVar = Api.clearFilter(filterVar);
    // sortVar = chgSortObj(sortVar);
    // console.log(filterVar); 

    var cursor = Pray1.find({}, {sort:{order_id:1}}).fetch();

    for (var i = cursor.length - 1; i >= 0; i--) {
      if( Pray2.find({ class_id: cursor[i]._id }).count() ){
        if(Number(cursor[i]['now_use'])){
          cursor[i]['open'] = 1;
        }

        var cursor2 = Pray2.find({ class_id: cursor[i]._id }, {sort:{order_id:1}}).fetch();
        for (var j = cursor2.length - 1; j >= 0; j--) {
          if(!!filterVar.year){
            cursor2[j]['count'] = Praying2.find({$and:[{prayname: cursor[i]._id}, {prayitem: cursor2[j]._id}, {year: filterVar.year }]}).count();
          }
          else{
            cursor2[j]['count'] = Praying2.find({$and:[{prayname: cursor[i]._id}, {prayitem: cursor2[j]._id}]}).count();
          }
        }
        cursor[i]['data'] = cursor2;

        if(!!filterVar.year){
          cursor[i]['count'] = Praying2.find({$and:[ {prayname: cursor[i]._id}, {year: filterVar.year }]}).count();
        }
        else{
          cursor[i]['count'] = Praying2.find({prayname: cursor[i]._id}).count();
        }
      }
      else{
        cursor[i]['count'] = 0;
      }
    };

    return {
      data: cursor,
      pos:  0, //startVar,
      total_count:Pray1.find().count()
    }
  }
});


Api.addRoute('praytableb', {authRequired: false}, {
  get: function () {
    // console.log("in serverpeople"); 
    
    var countVar = Number(this.queryParams.count) || 99;
    var startVar = Number(this.queryParams.start) || 0;

    var filterVar = this.queryParams.filter || {};
    var sortVar = this.queryParams.sort || {};
 
    function chgSortObj(arr){
      for (var i in arr) {
        if (arr[i] == "asc") arr[i] = 1;
        else if (arr[i] == "desc") arr[i] = -1;

        if(i == "pray"){
          arr['year'] = arr[i];
          arr['prayname_text'] = arr[i];
          arr['prayserial'] = arr[i];

          // arr['prayitem_text'] = arr[i];
          arr['pray2_orderid'] = arr[i];
          
          delete arr[i];
        }
      }
      return arr;
    }
    
    filterVar = Api.clearFilter(filterVar);
    sortVar = chgSortObj(sortVar);
    // console.log(filterVar); 
    // console.log(sortVar); 

    return {
      data: Praying2.find(filterVar, {skip: startVar, limit: countVar, sort: sortVar}).fetch(),
      pos:  startVar,
      total_count:Praying2.find(filterVar).count()
    }
  }
});*/