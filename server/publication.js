Meteor.publish("userList", function () {
    // return Meteor.users.find({}, {fields: {emails: 1, profile: 1}});
  return Meteor.users.find();
});

Meteor.publish('System', function() {
  return System.find(); 
});
Meteor.publish('Addr1', function() {
  return Addr1.find(); 
});

Meteor.publish('Pray1', function() {
  return Pray1.find(); 
});
Meteor.publish('Pray2', function() {
  return Pray2.find(); 
});
Meteor.publish('Light1', function() {
  return Light1.find(); 
});
Meteor.publish('Light2', function() {
  return Light2.find(); 
});

Meteor.publish('Mgyear', function() {
  return Mgyear.find(); 
});


Meteor.publish('prayingtype', function() {
  return PrayingType.find(); 
});

Meteor.publish('pdf_path', function(time) {
  return Pdfpath.find({time: time}); 
});
Meteor.publish('booking_praying1', function(id) {
  return Booking.find({praying1_id: id}); 
});

Meteor.publish('templeClass', function() {
  return TempleClass.find(); 
});

Meteor.publish('familyPeople', function(familyId) { // 回傳所有家庭的成員
  return People.find({familyId: familyId}, {fields: { 
    name: 1, sexual_text: 1, addr: 1, mainPerson:1, isLive:1,
    // ori_id:0,
    // familyId:0,
    // ori_family_id:0,
    // name:0,
    // isLive:0,
    // sexual:0,
    // sexual_text:0,
    // if_is:0,
    // mainPerson:0,
    // bornYear:0,
    // bornMonth:0,
    // bornDay:0,
    // bornTime:0,
    // bornTime_text:0,
    // zodiac_id:0,
    // zodiac_text:0,
    // telephone:0,
    // post5code:0,
    // mailThis:0,
    // cellphone:0,
    // intro:0,
    // if_ad:0,
    // templeClass:0,
    // templeClass_ori:0,
    // templeClass_text:0,
    // templeLevel:0,
    // templeLevel_ori:0,
    // templeLevel_text:0,
    // memberId:0,
    // longLive:0,
    // lotus:0,
  }}); 
});
{

}
Meteor.publish('templeLevel', function() {
  return TempleLevel.find(); 
});

Meteor.publish('Cmsget', function() {
  return Cmsget.find(); 
});

Meteor.publish('Cmslevel', function() {
  return Cmslevel.find(); 
});

Meteor.publish('Teams', function() {
  return Teams.find(); 
});

Meteor.publish('Clients', function() {
  return Clients.find(); 
});

Meteor.publish('Interviews', function() {
  return Interviews.find(); 
});

Meteor.publish('Portfolios', function() {
  return Portfolios.find(); 
});

Meteor.publish('Products', function() {
  return Products.find(); 
});

Meteor.publish('Provider', function() {
  return Provider.find(); 
});

Meteor.publish('CustomerLoc', function() {
  return CustomerLoc.find(); 
});

Meteor.publish('TeamLoc', function() {
  return TeamLoc.find(); 
});

Meteor.publish('CommissionLevel', function() {
  return CommissionLevel.find(); 
});
