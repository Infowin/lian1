// Global API configuration
Api = new Restivus({
  useDefaultAuth: true,
  prettyJson: true
});

// Generates: GET, POST on /api/items and GET, PUT, DELETE on
// /api/items/:id for the Items collection

// Generates: POST on /api/users and GET, DELETE /api/users/:id for
// Meteor.users collection
Api.addCollection(Meteor.users, {
  excludedEndpoints: ['getAll', 'put'],
  routeOptions: {
    authRequired: true
  },
  endpoints: {
    post: {
      authRequired: false
    },
    delete: {
      roleRequired: 'admin'
    }
  }
});

Api.getSortParams = function(arr1, arr2){
  for (var i in arr1) {
    if(!arr1[i]){
      delete arr1[i];
    }
    else if(i == "sortField"){
      arr2[arr1.sortField] = (arr1["sortOrder"] == "asc") ? 1: -1;

      delete arr1[i];
      delete arr1["sortOrder"];
    }
  }
}
Api.getPageParams = function(arr1, arr2){
  // I20160309-10:02:50.744(8)?   pageIndex: '1',
  // I20160309-10:02:50.744(8)?   pageSize: '15',
  // pageVar:
  // { pageIndex: '2', skip: 20, limit: 5 }

  if(!!arr1.pageIndex && !!arr1.pageSize ){
      var index = arr1.pageIndex-1 || 0;

      arr2.skip = arr1.pageSize * index;
      arr2.limit = Number(arr1.pageSize) || 5;

      delete arr1.pageIndex;
      delete arr1.pageSize;
  }
}
Api.getPageParamsNo = function(arr1, arr2){
  // I20160309-10:02:50.744(8)?   pageIndex: '1',
  // I20160309-10:02:50.744(8)?   pageSize: '15',
  // pageVar:
  // { pageIndex: '2', skip: 20, limit: 5 }

  if(!!arr1.pageIndex && !!arr1.pageSize ){
      // var index = arr1.pageIndex-1 || 0;

      // arr2.skip = arr1.pageSize * index;
      // arr2.limit = Number(arr1.pageSize) || 5;

      delete arr1.pageIndex;
      delete arr1.pageSize;
  }
}
Api.mydate = function(str) {
  var dd = new Date(str);
  return dd.getFullYear()+'-' + (dd.getMonth()+1) + '-'+dd.getDate() +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds();
}
Api.nowdate = function() {
  var dd = new Date();
  return dd.getFullYear()+'-' + (dd.getMonth()+1) + '-'+dd.getDate() +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds();
}
Api.clearFilter = function(arr){
  for (var i in arr) {
    // if (arr[i] == null || arr[i] === null || arr[i] === undefined || arr[i] === "" || arr[i] == "") {
    if(!arr[i] || arr[i] === null || arr[i] === undefined || arr[i] === "0"){
      delete arr[i];
    }

    if(i == "year" && !!arr[i]){
      arr['year'] = Number(arr[i]);
    }
  }
  return arr;
}
Api.chgSortObj = function (arr){
  for (var i in arr) {
    if (arr[i] == "asc") arr[i] = 1;
    else if (arr[i] == "desc") arr[i] = -1;
  }
  return arr;
}
Api.dateTW = function(doc, x, y, fontsize, datetime){
  var top_padding = Math.floor(fontsize/10)*2;

  doc.fontSize(fontsize).font('BiauKai').text("中華民國", x, y, {lineBreak: false, continued: "yes"})
  .font('Times-Roman').text("  "+datetime.yyy+"  ", doc.x, doc.y+top_padding, {lineBreak: false, continued: "yes"}).font('BiauKai').text("年", doc.x, doc.y-top_padding, {lineBreak: false, continued: "yes"})
  .font('Times-Roman').text("  "+datetime.mm+"  ", doc.x, doc.y+top_padding, {lineBreak: false, continued: "yes"}).font('BiauKai').text("月", doc.x, doc.y-top_padding, {lineBreak: false, continued: "yes"})
  .font('Times-Roman').text("  "+datetime.dd+"  ", doc.x, doc.y+top_padding, {lineBreak: false, continued: "yes"}).font('BiauKai').text("日", doc.x, doc.y-top_padding);
}
Api.strChtEngMix = function(doc, str, x, y, fontsize){
  doc.fontSize(fontsize);
  var top_shift = Math.ceil(fontsize/10);
  var now_x = x;
  var now_y = y;
  for(var i=0; i< str.length; i++){
    var nowchar = str.charAt(i);

    if(nowchar.Blength() == 1){// 如果是英文或數字
      if(nowchar == "\n"){
        now_y = doc.y+fontsize;
        now_x = x;
      }
      else{
        doc.font('Times-Roman').text(nowchar, now_x, now_y+top_shift);
        now_x = doc.x;
      }
    }
    else{ // 如果是中文
      doc.font('BiauKai').text(nowchar, now_x, now_y);

      if(doc.x-now_x != fontsize){
        now_x = doc.x+fontsize;
      }
      else{
        now_x = doc.x;
      }
    }
  }
  // now_x = doc.x;
  // doc.y = now_y;
}

Api.returnPDF = function (doc){
  if(typeof doc == "undefined"){
    doc = new PDFDocument({size: "A4", margin: 1});
    doc.registerFont('msjh', msjhTTF);

    doc.font('msjh');
    doc.fontSize(30).text("無資料", 100, 100);
  }

  return {
    headers: { 'Content-Type': 'application/pdf' },
    body: doc.outputSync()
  };
}
Api.returnHTML = function (content){
  if(!content){
    return {
      headers: { 'Content-Type': 'text/html; charset=utf-8' },
      body: "無資料"
    };
  }
  else{
    return {
      headers: { 'Content-Type': 'text/html; charset=utf-8' },
      body: content
    };
  }
}

Api.prayPage1 = function (doc, str_top1, str_top2, str_left, str_middle, str_right, noMorePage, entry){
  // doc.image(prayLight1png, 180, 40, {width: 240});
  doc.image(prayLight1png, 180, 36, {width: 240});
  // doc.image(prayLight2png, 180, 40, {width: 240});

  str_left   = str_left.commaToSpace();
  str_middle = str_middle.commaToSpace();

  doc.font('msjh');
  doc.fontSize(14);
  doc.text("|", 174, 8);
  doc.text("|", 424, 8);
  doc.text (str_top1, 180, 10, { width: 120, align: 'right'});
  doc.font('Times-Roman');
  doc.text (str_top2, 304, 15, { width: 120, align: 'left'});

  doc.font('BiauKai');
  doc.fontSize(36);

  var topMiddle = 205;
  var topLeft = 250;
  var topRight = 230;

  var arrLeft = str_left.cutStrToArr(16);
  var arrMiddle = str_middle.cutStrToArr(13);

  doc.fontSize(24);
  if(!!entry.type_text && entry.type_text.indexOf("拔") >= 0){
    doc.text("超 佛\n薦 力", 266, topMiddle, { width: 80, align: 'left'} );
    doc.text("蓮 往\n位 生", 266, 663, { width: 80, align: 'left'} );
  }
  else{
    doc.text("注 佛\n照 光", 266, topMiddle, { width: 80, align: 'left'} );
    doc.text("祿 長\n位 生", 266, 663, { width: 80, align: 'left'} );
  }
  topMiddle += 55;

  doc.fontSize(32);

  //////////////  中間  ///////////////
  if(arrMiddle.length == 1){
    if(arrMiddle[0].length <= 6){
      doc.text(arrMiddle[0].toStraight(), 284, topMiddle+40, { width: 40, align: 'left'});
    }
    else{
      if(arrMiddle[0].length >= 12){
        doc.fontSize(28);
      }
      else{
        doc.fontSize(32);
      }
      doc.text(arrMiddle[0].toStraight(), 284, topMiddle, { width: 40, align: 'left'});
      doc.fontSize(32);
    }
  }
  else if(arrMiddle.length == 2){
    doc.text(arrMiddle[0].toStraight(), 254, topMiddle, { width: 40, align: 'left'});
    doc.text(arrMiddle[1].toStraight(), 314, topMiddle, { width: 40, align: 'left'});
  }
  else if(arrMiddle.length == 3){
    doc.fontSize(30);
    doc.text(arrMiddle[1].toStraight(), 244, topMiddle, { width: 40, align: 'left'});
    doc.text(arrMiddle[0].toStraight(), 284, topMiddle, { width: 40, align: 'left'});
    doc.text(arrMiddle[2].toStraight(), 324, topMiddle, { width: 40, align: 'left'});
  }
  else if(arrMiddle.length == 4){
    if(arrMiddle[0].length <= 3){
      doc.fontSize(30);
      doc.text(arrMiddle[2].toStraight(), 224, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[0].toStraight(), 264, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[1].toStraight(), 304, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[3].toStraight(), 344, topMiddle, { width: 40, align: 'left'});
    }
    else{   // 有第四排的時候 (從16的開始)
      doc.fontSize(22);
      doc.text(arrMiddle[2].toStraight(), 240, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[0].toStraight(), 272, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[1].toStraight(), 304, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[3].toStraight(), 336, topMiddle, { width: 40, align: 'left'});
    }
  }
  else if(arrMiddle.length >= 5){
    if(arrMiddle[0].length <= 3){
      doc.fontSize(30);
      doc.text(arrMiddle[3].toStraight(), 224, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[1].toStraight(), 254, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[0].toStraight(), 284, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[2].toStraight(), 314, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[4].toStraight(), 344, topMiddle, { width: 40, align: 'left'});
    }
    else{
      doc.fontSize(22);
      doc.text(arrMiddle[3].toStraight(), 230, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[1].toStraight(), 260, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[0].toStraight(), 290, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[2].toStraight(), 320, topMiddle, { width: 40, align: 'left'});
      doc.text(arrMiddle[4].toStraight(), 350, topMiddle, { width: 40, align: 'left'});
    }
  }

  doc.fontSize(22);
  //////////////  左邊  ///////////////
  if(!!entry.type_text && entry.type_text.indexOf("拔") >= 0){
    doc.text("拜薦".toStraight(),   180, topLeft+450, { width: 38, align: 'left'});
    if(entry.type_text.indexOf("冤親") >= 0){
      doc.text("解冤者".toStraight(),   180, topLeft, { width: 38, align: 'left'});
      topLeft += 85;
    }
    else{
      doc.text("陽上報恩".toStraight(),   180, topLeft, { width: 38, align: 'left'});
      topLeft += 105;
    }
  }
  if(arrLeft.length == 1){
    doc.text(arrLeft[0].toStraight(),   180, topLeft, { width: 38, align: 'left'});
  }
  else if(arrLeft.length == 2){
    doc.text(arrLeft[0].toStraight(),   160, topLeft, { width: 38, align: 'left'});
    doc.text(arrLeft[1].toStraight(),   180, topLeft, { width: 38, align: 'left'});
  }
  else if(arrLeft.length == 3){
    doc.text(arrLeft[0].toStraight(),   140, topLeft, { width: 38, align: 'left'});
    doc.text(arrLeft[1].toStraight(),   160, topLeft, { width: 38, align: 'left'});
    doc.text(arrLeft[2].toStraight(),   180, topLeft, { width: 38, align: 'left'});
  }

  //////////////  右邊  ///////////////
  doc.text(str_right.toStraight(),  402, topRight, { width: 38, align: 'left'});

  if(!noMorePage) doc.addPage();
}
Api.prayPage1a = function (doc, pos_num, str_top1, str_top2, str_left, str_middle, str_right, noMorePage, entry){
  // doc.image(prayLight1png, 180, 40, {width: 240});

  var x_shift = 20;

  var topMiddle = 140;
  var topLeft   = 150;
  var topRight  = 130;
  var topTitle = 15;

  x_shift = x_shift + 210*pos_num;

  // for(var i=0; i<4; i++){

    doc.image(prayLight1png, x_shift, 30, {width: 165});


    doc.font('msjh');
    doc.fontSize(10);
    // doc.text("|", 10+x_shift,  4);
    if(pos_num != 3){
      doc.text("|", x_shift+190, topTitle);
      doc.text("|", x_shift+190, 560);
    }
    doc.text (str_top1, 100+x_shift, topTitle, { width: 70, align: 'right'}); // 總功德主
    doc.font('Times-Roman');
    doc.text (str_top2, 5+x_shift, topTitle+3, { width: 60, align: 'left'}); // 20


    doc.fontSize(18);
    doc.font('BiauKai');
    topMiddle = 140;
    if(!!str_left){
      doc.text("超 佛\n薦 力", 56+x_shift, topMiddle, { width: 80, align: 'left'} );
      doc.text("蓮 往\n位 生", 56+x_shift, 454, { width: 80, align: 'left'} );
    }
    else{
      doc.text("注 佛\n照 光", 56+x_shift, topMiddle, { width: 80, align: 'left'} );
      doc.text("祿 長\n位 生", 56+x_shift, 454, { width: 80, align: 'left'} );
    }
    topMiddle += 50;

    // var arrLeft = str_left.cutStrToArr(25);
    // var arrMiddle = str_middle.cutStrToArr(13);
    //////////////  中間  ///////////////
    str_middle = str_middle.replace(",", " ");
    if(str_middle.length <= 6){
      doc.fontSize(28);
      doc.text(str_middle.toStraight(), 69+x_shift, topMiddle+14, { width: 40, align: 'left'});
    }
    else{
      doc.fontSize(22);
      doc.text(str_middle.toStraight(), 72+x_shift, topMiddle, { width: 40, align: 'left'});
    }
    //////////////  左邊  /////////////// 名字
    doc.fontSize(16);
    if(!!str_left){
      str_left = str_left.replace(",", " ");
      if(str_middle.indexOf("冤") != -1){
        doc.text(("解冤者 "+str_left).toStraight(),    0+x_shift, topLeft, { width: 38, align: 'left'});
      }
      else{
        doc.text(("陽上報恩 "+str_left).toStraight(),   0+x_shift, topLeft, { width: 38, align: 'left'});
      }
      // doc.text(str_left.toStraight(),   0+x_shift, 200, { width: 38, align: 'left'});
      doc.text("拜薦".toStraight(),       0+x_shift, 465, { width: 38, align: 'left'});
    }
    //////////////  右邊  /////////////// 地址
    doc.text(str_right.toStraight(),  155+x_shift, topRight, { width: 38, align: 'left'});

    // x_shift += 210;
  // }

  // if(!noMorePage) doc.addPage();
  //*/
}
Api.prayPage2 = function(doc, printType, arrObj){

    // initial definition 直的幾個 橫的幾個
    var x_num = printType.oilpray_x;
    var y_num = printType.oilpray_y;

    // 每個燈牌的大小 (給底圖用的)
    var each_height, each_width;

    // 不要印到紙張邊界
    var a4pageHeight2, a4pageWidth2, width_shift_init = 10;

    if(printType.oilpray_layout == 1){ // 直的
      // 不要印到紙張邊界
      var a4pageHeight2 = a4pageHeight - 10;
      var a4pageWidth2 = a4pageWidth - 20;

      each_height = Math.round(a4pageHeight2/y_num, 2);
      each_width  = Math.round(a4pageWidth2/x_num, 2);
    }
    else{
      // 不要印到紙張邊界
      var a4pageHeight2 = a4pageHeight - 20;
      var a4pageWidth2 = a4pageWidth - 10;

      each_height = Math.round(a4pageWidth2/y_num, 2);
      each_width  = Math.round(a4pageHeight2/x_num, 2);
    }
    // 每一個項目的偏移量
    // var width_shift = 0;
    var width_shift = width_shift_init;
    var height_shift = 0;

    var text_height_shift1=0.0809*each_height; //8; 上面文字的位置
    var text_height_shift2=(7/9)*each_height;//77;  下面文字的位置

    // 文字的細微的偏移量
    var text_width_shift_modify = 0;
    var text_height_shift_modify = 0;

    // 預留要打孔、掛標籤的位置
    var upper_empty_shift = 25;

    var useFontSize = 14;
    // var useNameLength = 8;

    //  根據寬多少個來調整文字大小
    if(printType.oilpray_layout == 1){ // 直的
      if(x_num==2){
        useFontSize = 24;
      }
      else if(x_num==3){
        useFontSize = 16;
      }
      else if(x_num==4){
        useFontSize = 14;
          if(y_num==4){
              // text_height_shift_modify = 1;
          }
      }
      else if(x_num==5){
        useFontSize = 14;
      }
      else if(x_num==6){
        useFontSize = 14;
        // text_width_shift_modify = 5;
      }
    }
    else{ // 橫的
      if(x_num==2){
        useFontSize = 36;
      }
      else if(x_num==3){
        if(y_num == 1){
          useFontSize = 30;
          // text_height_shift_modify = 10;
        }
      }
      else if(x_num==4){
      }
      else if(x_num==5){
      }
      else if(x_num==6){
      }
    }

    // 文字在中間的位置
    var text_width_shift = 0.5*each_width - (useFontSize*1.5) + width_shift_init;


    var i=0; // 已印寬的數量
    var j=0; // 已印長的數量

    // 每一筆資料開始跑
    for(var ii=0; ii<arrObj.length; ii++){
        var entry = arrObj[ii];

        // console.log("x_num: "+y_num + " y_num: "+y_num);
        // console.log("text_width_shift: "+text_width_shift);

        if(i == x_num && j == y_num-1){
            // doc.addPage();
          if(printType.oilpray_layout == 1){ // 直的
            doc.addPage({size: printType.oilpray_paper, margin: 1 });
          }
          else{
            doc.addPage({size: printType.oilpray_paper, margin: 1, layout: "landscape"});
          }
        }
        if(i == x_num){ // 寬印滿 -> 高往下跑
            i=0;

            // 重設偏移量
            text_width_shift = 0.5*each_width - (useFontSize*1.5) + width_shift_init;
            //text_width_shift=0.4*each_width;//3x3:0.4;;
            width_shift = width_shift_init;

            // 高往下跑
            text_height_shift1 += each_height;
            text_height_shift2 += each_height;
            height_shift += each_height;

            j++;
        }
        if(j == y_num){ // 高印滿 -> 寬重設
            j = 0;
            height_shift = 0;
            text_height_shift1=0.0809*each_height;//8;
            text_height_shift2=(7/9)*each_height;//77;
        }

        doc.image(prayCandle2png, width_shift, height_shift+upper_empty_shift, {width: each_width, height: each_height-upper_empty_shift});
        text_width_shift = text_width_shift+text_width_shift_modify;

        doc.font('BiauKai');
        doc.fontSize(useFontSize);
        doc.text("注 佛\n照 光", text_width_shift, text_height_shift1+text_height_shift_modify+upper_empty_shift-3, { width: 80, align: 'left'} );
        doc.text("祿 長\n位 生", text_width_shift, text_height_shift2+text_height_shift_modify+3, { width: 80, align: 'left'} );

        // console.log(arrObj);
        // console.log(entry.prayserial+ " "+entry.livename_text);

        // //  印名字
        var dataArr2 = entry.livename_text.split(",");
        // console.log(dataArr2);

        // 切斷名字
        var nowsrtlen = dataArr2[0].length;
        if(nowsrtlen > 6){
            var tmpArr = [];
            tmpArr.push = dataArr2[0].substr(0, 8).trim();
            tmpArr.push = dataArr2[0].substr(8, nowsrtlen-8).trim();
            dataArr2 = tmpArr;
        }

        // // 姓名的列數
        var name_text_height_shift = text_height_shift1+useFontSize*2.2+upper_empty_shift;
        if(dataArr2.length == 1){
          // name_text_height_shift = name_text_height_shift + useFontSize*2;
          doc.text(dataArr2[0].toStraight(), text_width_shift+useFontSize, name_text_height_shift, { width: 40, align: 'left'} );
        }
        else if(dataArr2.length == 2){
          name_text_height_shift = name_text_height_shift + useFontSize*1;
          doc.text(dataArr2[0].toStraight(), text_width_shift, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[1].toStraight(), text_width_shift+useFontSize*2, name_text_height_shift, { width: 40, align: 'left'} );
        }
        else if(dataArr2.length == 3 || dataArr2.length == 6){
          if(dataArr2.length == 3){
            name_text_height_shift = name_text_height_shift + useFontSize*1;
          }
          doc.text(dataArr2[0].toStraight(), text_width_shift+useFontSize, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[1].toStraight(), text_width_shift-useFontSize*0.6, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[2].toStraight(), text_width_shift+useFontSize*2.5, name_text_height_shift, { width: 40, align: 'left'} );
          if(dataArr2.length == 6){
            name_text_height_shift = name_text_height_shift + useFontSize*4.5;
            doc.text(dataArr2[3].toStraight(), text_width_shift+useFontSize, name_text_height_shift, { width: 40, align: 'left'} );
            doc.text(dataArr2[4].toStraight(), text_width_shift-useFontSize*0.6, name_text_height_shift, { width: 40, align: 'left'} );
            doc.text(dataArr2[5].toStraight(), text_width_shift+useFontSize*2.5, name_text_height_shift, { width: 40, align: 'left'} );
          }
        }
        else if(dataArr2.length == 4 || dataArr2.length == 7 || dataArr2.length == 8){
          if(dataArr2.length == 4){
            name_text_height_shift = name_text_height_shift + useFontSize*1;
          }
          doc.text(dataArr2[0].toStraight(), text_width_shift, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[1].toStraight(), text_width_shift+useFontSize*1.8, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[2].toStraight(), text_width_shift-useFontSize*1.8, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[3].toStraight(), text_width_shift+useFontSize*3.6, name_text_height_shift, { width: 40, align: 'left'} );
          if(dataArr2.length == 7 || dataArr2.length == 8){
            name_text_height_shift = name_text_height_shift + useFontSize*4.5;
            doc.text(dataArr2[4].toStraight(), text_width_shift, name_text_height_shift, { width: 40, align: 'left'} );
            doc.text(dataArr2[5].toStraight(), text_width_shift+useFontSize*1.8, name_text_height_shift, { width: 40, align: 'left'} );
            doc.text(dataArr2[6].toStraight(), text_width_shift-useFontSize*1.8, name_text_height_shift, { width: 40, align: 'left'} );
            if(dataArr2.length == 8)
              doc.text(dataArr2[7].toStraight(), text_width_shift+useFontSize*3.6, name_text_height_shift, { width: 40, align: 'left'} );
          }
         }
        else if(dataArr2.length == 5 || dataArr2.length == 9 || dataArr2.length == 10){
          if(dataArr2.length == 5){
            name_text_height_shift = name_text_height_shift + useFontSize*1;
          }
          doc.text(dataArr2[0].toStraight(), text_width_shift+useFontSize, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[1].toStraight(), text_width_shift-useFontSize*0.6, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[2].toStraight(), text_width_shift+useFontSize*2.5, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[3].toStraight(), text_width_shift-useFontSize*2.2, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[4].toStraight(), text_width_shift+useFontSize*4, name_text_height_shift, { width: 40, align: 'left'} );
          if(dataArr2.length == 9 || dataArr2.length == 10){
            name_text_height_shift = name_text_height_shift + useFontSize*4.5;
            doc.text(dataArr2[5].toStraight(), text_width_shift+useFontSize, name_text_height_shift, { width: 40, align: 'left'} );
            doc.text(dataArr2[6].toStraight(), text_width_shift-useFontSize*0.6, name_text_height_shift, { width: 40, align: 'left'} );
            doc.text(dataArr2[7].toStraight(), text_width_shift+useFontSize*2.5, name_text_height_shift, { width: 40, align: 'left'} );
            doc.text(dataArr2[8].toStraight(), text_width_shift-useFontSize*2.2, name_text_height_shift, { width: 40, align: 'left'} );
            if(dataArr2.length == 10)
              doc.text(dataArr2[9].toStraight(), text_width_shift+useFontSize*4, name_text_height_shift, { width: 40, align: 'left'} );
          }
        }
        else if(dataArr2.length > 10){
          var useFontSize2 = useFontSize - 6;
          doc.fontSize(useFontSize2);

          text_width_shift += 9;
          doc.text(dataArr2[0].toStraight(), text_width_shift+useFontSize2, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[1].toStraight(), text_width_shift-useFontSize2*0.4, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[2].toStraight(), text_width_shift+useFontSize2*2.3, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[3].toStraight(), text_width_shift-useFontSize2*1.8, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[4].toStraight(), text_width_shift+useFontSize2*3.6, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[5].toStraight(), text_width_shift-useFontSize2*3.2, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[6].toStraight(), text_width_shift+useFontSize2*5.0, name_text_height_shift, { width: 40, align: 'left'} );

          name_text_height_shift += useFontSize2*4.5;
          doc.text(dataArr2[7].toStraight(), text_width_shift+useFontSize2, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[8].toStraight(), text_width_shift-useFontSize2*0.4, name_text_height_shift, { width: 40, align: 'left'} );
          doc.text(dataArr2[9].toStraight(), text_width_shift+useFontSize2*2.3, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[10]) doc.text(dataArr2[10].toStraight(), text_width_shift-useFontSize2*1.8, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[11]) doc.text(dataArr2[11].toStraight(), text_width_shift+useFontSize2*3.6, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[12]) doc.text(dataArr2[12].toStraight(), text_width_shift-useFontSize2*3.2, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[13]) doc.text(dataArr2[13].toStraight(), text_width_shift+useFontSize2*5.0, name_text_height_shift, { width: 40, align: 'left'} );

          name_text_height_shift += useFontSize2*4.5;
          if(!!dataArr2[14]) doc.text(dataArr2[14].toStraight(), text_width_shift+useFontSize2, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[15]) doc.text(dataArr2[15].toStraight(), text_width_shift-useFontSize2*0.4, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[16]) doc.text(dataArr2[16].toStraight(), text_width_shift+useFontSize2*2.3, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[17]) doc.text(dataArr2[17].toStraight(), text_width_shift-useFontSize2*1.8, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[18]) doc.text(dataArr2[18].toStraight(), text_width_shift+useFontSize2*3.6, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[19]) doc.text(dataArr2[19].toStraight(), text_width_shift-useFontSize2*3.2, name_text_height_shift, { width: 40, align: 'left'} );
          if(!!dataArr2[20]) doc.text(dataArr2[20].toStraight(), text_width_shift+useFontSize2*5.0, name_text_height_shift, { width: 40, align: 'left'} );



        }

        //  印編號
        doc.fontSize(useFontSize-2);
        var tt = entry.type_text.charAt(0);
        doc.font('BiauKai');
        doc.text(tt,               text_width_shift-((useFontSize-2)*1.5), upper_empty_shift+text_height_shift1+(useFontSize*0.5) );
        doc.font('Times-Roman');
        doc.text(entry.prayserial, text_width_shift-((useFontSize-2)*1.5), upper_empty_shift+text_height_shift1+(useFontSize*0.5)+useFontSize );

        width_shift += each_width;
        text_width_shift += each_width;

        i++;
    }
}
Api.prayPage3 = function(doc, arrObj, datetime){
  function printTitle(doc, datetime, entry){
    var title_y = 18;
    doc.font('BiauKai').fontSize(14).text("財團法人", 50, title_y+3);
    doc.fontSize(20).text("桃園佛教蓮社", 135, title_y);

    doc.font('Times-Roman');
    doc.fontSize(20).text(entry.year, 270, title_y+2);
    doc.font('BiauKai');
    doc.fontSize(20).text("年度 "+entry.prayname_text+" 詳細疏文", 310, title_y);

    // doc.fontSize(20).text("", 460, 15);
    Api.dateTW(doc, 630, title_y+3, 14, datetime);

    doc.y = 50; // 內容開始的高
    doc.fontSize(18);
  }
  function newPage(doc, datetime, entry){
    doc.addPage();
    printTitle(doc, datetime, entry);
  }
  function printPageNum(doc, num){
    doc.fontSize(14);
    doc.font('BiauKai').text("第", 360, 560);
    doc.font('Times-Roman').text(num, 375, 560+2, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁", 400, 560);
    // doc.font('BiauKai').text("頁，共", 400, 560);
    // doc.font('Times-Roman').text(totalPage, footer_width_shift+102, table_y+table_height+16, { width: 25, align: 'center'});
    // doc.font('BiauKai').text("頁", footer_width_shift+135, table_y+table_height+15);
   }

  var nowPage = 1;
  // var totalPage = Math.ceil(arrObj.length/26);

  // console.log(arrObj);

  var year = arrObj[0].year;
  var pray1 = arrObj[0].prayname_text;
  var pray2 = "";
  var pray3 = "";
  var old_pray2 = "1";
  var old_pray3 = "1";

  printTitle(doc, datetime, arrObj[0]);
  for(var i=0; i< arrObj.length; i++){
    var entry = arrObj[i];
    if(!entry.type || (entry.livename_text == "" && entry.passname_text == "")){
      continue;
    }

    var now_y = doc.y;
    var next_y=0;
    var passname_x = 410;
    // var passname_x = 310;
    // var addr_x = 570;
    var addr_x = 475;
    // var addr_width = 300;
    var addr_width = 360;
    var addr_pass = "";
    // 換頁
    if(doc.y > 500 || year!=entry.year || pray1!=entry.prayname_text){
      printPageNum(doc, nowPage);

      newPage(doc, datetime, entry);
      nowPage++;
      pray2 = "";
      pray3 = "";
      now_y = doc.y;
    }
    year = entry.year;
    pray1 = entry.prayname_text;

    if(entry.type_text == '小燈' || entry.type_text == '吉祥' || entry.type_text == '如意'){
      addr_x = 300;
      addr_width = 500;
    }
    else if(entry.type_text == '燈主'){
      addr_x = 420;
      addr_width = 500;
    }
    else if(entry.prayitem_text == "初亡"){
      addr_x = 565;
      addr_width = 360;
    }
 /*   else if(entry.type_text == '初亡'){
      addr_x = 475;
      addr_width = 360;
    }*/

    // 每次新的小標
    pray2 = entry.prayitem_text;
    // pray3 = entry.type_text;
    pray3 = PrayingType.findOne(entry.type).value;
    if(!!entry.passname_text1) pray3 = "拔-歷代祖先";
    else if(!!entry.passname_text2) pray3 = "拔-冤親債主";
    else if(!!entry.passname_text3) pray3 = "拔-地基主";
    else if(!!entry.passname_text4) pray3 = "拔-嬰靈";

    // console.log("entry");
    // console.log(entry);

    // 每段標題
    if(pray2 != old_pray2 || pray3 != old_pray3){
      // console.log("new title");
      now_y += 8;
      old_pray2 = pray2;
      old_pray3 = pray3;

      doc.font('BiauKai');

      if(!!pray2){
        doc.fontSize(14);
        doc.text(pray2, 15, now_y, {width: 100, align: 'left'});

        doc.fontSize(18);
        now_y += 14;
      }

      // console.log(entry);
      if(PrayingType.findOne(entry.type).print_type == 1){ // 消
        passname_x = 120;
      }
      else{
        // doc.text("陽上", 120, now_y, {width: 350, align: 'left'});
        doc.text("陽上", 50, now_y, {width: 350, align: 'left'});
        // passname_x = 410;
        passname_x = 325;
      }

      if(!!pray3 && entry.prayitem_text == "初亡"){
        passname_x = 270;
        doc.text(pray3, passname_x, now_y, {width: 350, align: 'left'});
        doc.text("生卒時", passname_x+100, now_y, {width: 350, align: 'left'});
      }
      else if(!!pray3 && entry.type_text != '燈主' && entry.type_text != '小燈' && entry.type_text != '吉祥' && entry.type_text != '如意'){
        // 印各種類型的拔
        // doc.text(pray3.replace(/ /g, ""), passname_x, now_y, {width: 350, align: 'left'});
        // doc.text(pray3, passname_x, now_y, {width: 350, align: 'left'});
        Api.strChtEngMix(doc, pray3, passname_x-10, now_y, 18);
      }

      doc.font('BiauKai');
      doc.text("地址", addr_x, now_y, {width: 300, align: 'left'});
      now_y = doc.y+5;
      doc.moveTo(10, doc.y+1).lineTo(820, doc.y+1).stroke(); // 劃線
    }


    // 內容
    doc.fontSize(18);
    doc.font('Times-Roman');
    if(!!entry.prayserial){
      doc.text(entry.prayserial, 15, now_y+3);
    }
    else{
      doc.text("0", 15, now_y+3);
    }

    var livename = entry.livename_text.replace(/\,/g, " ");
    doc.font('BiauKai');
    if(PrayingType.findOne(entry.type).print_type != 2){ // 不是拔
      if(!!livename){
        // console.log(entry._id + " "+ entry.livename_text);
        doc.text(livename, 50, now_y, {width: 300, align: 'left'});
        next_y = doc.y;
      }
    }
    else{ // 所有的拔
      doc.text(livename, 50, now_y, {width: 300, align: 'left'});
      next_y = doc.y;

      // 拔渡者
      var passname = entry.passname_text;
      if(!!entry.passname_text1) passname = entry.passname_text1;
      else if(!!entry.passname_text2) passname = entry.passname_text2;
      else if(!!entry.passname_text3) passname = entry.passname_text3;
      else if(!!entry.passname_text4) passname = entry.passname_text4;

      if(!!passname){ //.replace(/\,/g, "\n")
        var tmp = passname;
        if(tmp.indexOf(",") == -1){
          if(passname.length > 8) {
            tmp = passname.splice(8, 0, "\n");
            if(passname.length > 16) {
              tmp = passname.splice(16, 0, "\n");
            }
          }
        }
        else{
          tmp = passname.replace(/\,/g, "\n");
        }

        if(entry.prayitem_text == "初亡"){
          // passname_x = 260;
          doc.text(tmp, 270, now_y, {width: 350, align: 'left'});
          passname_x = 270+100;
          var p1 = Praying1.findOne({_id: entry.listId});
          if(p1){
            var passObj = People.findOne({familyId: p1.familyId, name: passname, isLive:0});
            addr_pass = "";
            if(!!passObj){
              if(!!passObj.addr){
                addr_pass = passObj.addr;
              }
              /*
     bornYear: '28',
     bornMonth: '10',
     bornDay: '15',
     bornTime: '12',
     bornTime_text: '亥',
     passYear: '104',
     passMonth: '10',
     passDay: '4',
     passTime: '7',
     passTime_text: '午',
               */
              var d1 = "", d2 = "";
              if( !!passObj.bornYear && passObj.bornMonth && passObj.bornDay){
                d1 = "農"+passObj.bornYear +"年"+ passObj.bornMonth +"月"+ passObj.bornDay+"日";
                if(passObj.bornTime_text){
                  d1 = d1 + passObj.bornTime_text+ "時";
                }
              }
              if(passObj.passYear && passObj.passMonth && passObj.passDay){
                d2 = "農"+passObj.passYear +"年"+ passObj.passMonth +"月"+ passObj.passDay+"日";
                if(passObj.passTime_text){
                  d2 = d2 + passObj.passTime_text + "時";
                }
              }
              // doc.text("生"+d1, passname_x, now_y, {width: 350, align: 'left'});
              // doc.text("卒"+d2, passname_x, now_y + 16, {width: 350, align: 'left'});

              Api.strChtEngMix(doc, "生 "+d1, passname_x, now_y, 18);
              next_y += 16;
              Api.strChtEngMix(doc, "卒 "+d2, passname_x, now_y+16, 18);
            }
            // console.log("next_y: " + next_y);
            // console.log("now_y: " + (now_y + 16) );
            // next_y += 16;

            // if(now_y  > next_y){
            //   next_y = now_y;
            // }
          }

        }
        else{
          doc.text(tmp, 325, now_y, {width: 150});
        }

      }
      if(doc.y > next_y){
        next_y = doc.y;
      }
    }

    // 地址
    // var addr = entry.addr;
    var addr = "";
    if(!!addr_pass){
      addr = addr_pass;
    }
    else{
      addr = entry.addr;
    }
    if(addr_x > 500){

      var StrLength = 15;
      if(addr.length > StrLength){
        var tmp_length = addr.length;
        var tmp_pos = StrLength;
        while(tmp_length > StrLength){

          addr = addr.splice(tmp_pos, 0, "\n");
          next_y += 16;// workaround

          tmp_pos += StrLength;
          tmp_length -= StrLength;
        }
      }
    }
    else{
      var StrLength = 22;
      if(addr.length > StrLength){
        var tmp_length = addr.length;
        var tmp_pos = StrLength;
        while(tmp_length > StrLength){

          addr = addr.splice(tmp_pos, 0, "\n");
          next_y += 16;// workaround

          tmp_pos += StrLength;
          tmp_length -= StrLength;
        }
      }
    }
    // doc.text(addr, addr_x, now_y, {width: addr_width, align: 'left'});
    // if(entry.prayitem_text == "初亡"){
    //   Api.strChtEngMix(doc, addr, addr_x, now_y, 18);
    // }
    // else{
    //   Api.strChtEngMix(doc, addr, addr_x, now_y, 18);
    // }
    Api.strChtEngMix(doc, addr, addr_x, now_y, 18);

    if(addr.length > 13){
      // console.log("doc.y: "+doc.y+ " next_y: "+ next_y);
    }
    if(doc.y > next_y){
      next_y = doc.y;
    }

    doc.y = next_y+5;

    if(i == arrObj.length-1){
      nowPage++;
      printPageNum(doc, nowPage);
    }
  }
}
Api.prayPage4 = function(doc, arrObj){
  var nowMember = 0;
  var nowPage = 1;
  var rowPage = 16;
  var totalPage = Math.ceil(arrObj.length/rowPage);

  // 整個頁面的loop
  var width_num = 12
  var table_x = 14;
  var table_y = 24;
  var table_width = 810;
  var cell_width = table_width/width_num;
  var table_height = 550;

  while(nowMember < arrObj.length){
    doc.lineJoin('miter').rect(table_x, table_y, table_width, table_height);

    var left_shift = table_x;
    var top_shift = table_y;

    for(var i=0; i<width_num; i++){
      var entry = arrObj[nowMember];
      // console.log(entry);

      doc.moveTo(left_shift, table_y).lineTo(left_shift, table_y+table_height).stroke();

      if(typeof entry != "undefined"){

        var type = "功德";
        if(entry.type_text.length >2 ){
          type = "供養三寶";
        }
        doc.fontSize(26);
        doc.text(type.toStraight(), left_shift+15+4, table_y+2, {width: 50, align: "left"});

        doc.fontSize(36);
        var name = entry.livename_text;
        if(name.length > 4){
          doc.fontSize(26);
        }
        doc.text(name.toStraight(), left_shift+15, 135+2, {width: 50, align: "left"});
        var donate = "5000";

        doc.fontSize(36);
        doc.text(donate.toStraight(), left_shift+15, 350+2, {width: 50, align: "left"});

        doc.fontSize(36);
        doc.font('BiauKai');
        doc.text("大德     元".toStraight(), left_shift+15, 282, {width: 50, align: "left"});
      }

      left_shift += cell_width;
      nowMember++;
    }

    if(nowMember < arrObj.length){
      doc.addPage();
      // nowMember+=23;
    }
    else{
      break;
    }
  }
}
Api.prayPage5 = function(doc, arrObj){
  doc.fontSize(32);
  doc.font('BiauKai');
  doc.rotate(-180, {origin: [150, 70]}).text("普施圓桌", -260, -260, { width: 200, align: "left"});
  doc.text("測試一 測試二 測試三 測試四", doc.x, doc.y+60);

  // console.log(doc.x +" "+doc.y);

  // 弄正的
  doc.rotate(-180, {origin: [150, 70]});
  doc.text("普施圓桌", 50, 450, { width: 100, align: "left"});
  doc.text("測試a 測試b 測試c 測試d", 50, 550, { width: 400, align: "left"});
}
Api.lightPage1 = function(doc, arrObj, type){
  function printBigLabel(doc, name1, name2, num){
      var offset = 2;
      var yOffset = 4;

      if(y > a4pageHeight-eachHeight){
        doc.addPage();
        y = 0;
        j = 0;
      }
      if(j % 2 == 0){
        x = 0;
      }
      else{
        x = a4pageWidthHalf;
      }
      doc.rect(x, y, a4pageWidthHalf, eachHeight).stroke();

      var n = name1 + " " +name2.replace(/,/g, ' ');
      n = n.trim();

      var n1 = "", n2 = "";
      if(n.length < 11){
        n1 = n;
      }
      else{
        var cut_pos = n.indexOf(" ", Math.ceil(n.length/2)-2);
        n1 = n.substring(0, cut_pos).trim();
        n2 = n.substring(cut_pos).trim();
      }

      var font_size_cht = 15;

      var n1_x_shift = (a4pageWidthHalf - n1.length*font_size_cht)/2; //  - n1_space*(font_size_cht/2)
      var n2_x_shift = (a4pageWidthHalf - n2.length*font_size_cht)/2; //  - n1_space*(font_size_cht/2)

      doc.font('BiauKai');

      if(!!n2){
        doc.fontSize(font_size_cht);
        doc.text(n1, x+n1_x_shift, y+offset+yOffset, { width: a4pageWidthHalf-2, align: 'left'});//.
        if(n2.length > 16){
          n2_x_shift = 3;
          var size = Math.ceil((a4pageWidthHalf-4)/(n2.length+2));
          // console.log(size);
          doc.fontSize(size);

          n2_x_shift = (a4pageWidthHalf-4 - (n2.length)*size)/2; //  - n1_space*(font_size_cht/2)
          doc.text(n2, x+n2_x_shift, y+offset+yOffset+16, { width: a4pageWidthHalf-2, align: 'left'});//.
        }
        else{
          doc.fontSize(font_size_cht);
          doc.text(n2, x+n2_x_shift, y+offset+yOffset+16, { width: a4pageWidthHalf-2, align: 'left'});//.
        }
      }
      else{
        doc.fontSize(font_size_cht);
        doc.text(n1, x+n1_x_shift, y+offset+yOffset+9, { width: a4pageWidthHalf-2, align: 'left'});//.
      }

      doc.font('Times-Roman');
      doc.fontSize(14);
      doc.text(num, x+1, y+offset+yOffset+31, { width: a4pageWidthHalf-2, align: 'center'});

      // console.log("x: "+ x+ " y: "+ y + " name: "+entry.name);

      if(j % 2 == 1){
        y += eachHeight;
      }
      j++;
  }

  var eachHeight = 52;
  var offset = 5, yOffset = 8;
  var j=0; x = 0, y = 0;

  //////////////

  var old_status = "";
  var now_status = "";
  for(var i=0; i<arrObj.length; i++){
    var entry =  arrObj[i];

    if(!entry.people_name){
      continue;
    }

    now_status = "1";
    // if(entry.p1 == "-1") now_status = "1";
    // else if(entry.p3 == "-1") now_status = "3";
    // else if(entry.p5 == "-1") now_status = "5";
    // else if(entry.p7 == "-1") now_status = "7";

    if(i != 0 && old_status != now_status){
        doc.addPage();
        y = 0;
        j = 0;
    }

    // if(entry.p1 == "-1"){ // 光明燈
    if(type == "1" || type == "3"){ // 光明燈
      yOffset = 8;
      doc.fontSize(14);

      var num = "";
      if(type == "1"){
        num = entry.p1_num;
      }
      else if(type == "3"){
        num = entry.p3_num;
      }

      if(!!entry.people_name && !!num ){
        if(y > a4pageHeight-eachHeight){
          doc.addPage();
          y = 0;
          j = 0;
        }

        if(j % 4 == 0){
          x = 0;
        }
        else{
          x += a4pageWidthHalf2;
        }

        doc.rect(x, y, a4pageWidthHalf2, eachHeight).stroke();

        doc.font('BiauKai');

        var n1 = entry.people_name.trim();
        var n2 = entry.p1_name2.trim();

        if(n1.length > 7 && !n2){
          n2 = n1.substring(6).trim();
          n1 = n1.substring(0, 6).trim();
        }

        if(!n2){
          var n1_x_shift = (a4pageWidthHalf2 - n1.length*20)/2; //  - n1_space*(font_size_cht/2)
          doc.fontSize(16);
          doc.text(n1, x+n1_x_shift, y+offset+yOffset, { width: a4pageWidthHalf2-2, align: 'left'});//.
        }
        else{
          var n1_x_shift = (a4pageWidthHalf2 - n1.length*16)/2; //  - n1_space*(font_size_cht/2)
          var n2_x_shift = (a4pageWidthHalf2 - n2.length*16)/2; //  - n1_space*(font_size_cht/2)

          doc.fontSize(14);
          doc.text(n1, x+n1_x_shift, y+offset+yOffset-6, { width: a4pageWidthHalf2-2, align: 'left'});//.
          doc.text(n2, x+n2_x_shift, y+offset+yOffset+10, { width: a4pageWidthHalf2-2, align: 'left'});//.
        }

        doc.fontSize(12);
        doc.font('Times-Roman');
        // doc.text(entry.p1_num, x+offset, y+offset+yOffset+24, { width: a4pageWidthHalf2-offset*2, align: 'center'});
        doc.text(num, x+offset, y+offset+yOffset+24, { width: a4pageWidthHalf2-offset*2, align: 'center'});

        // console.log("x: "+ x+ " y: "+ y + " name: "+entry.name);

        if(j % 4 == 3){
          y += eachHeight;
        }
        j++;
      }
    }
    // else if(entry.p3 == "-1"){ // 藥師燈
    //   var str = entry.p3_name2.trim();
    //   printBigLabel(doc, entry.people_name.trim(), str, entry.p3_num);
    // }
    // else if(entry.p5 == "-1"){ // 光明大燈
    else if(type == "5"){ // 光明大燈
      var str = entry.p5_name2.trim();
      printBigLabel(doc, entry.people_name.trim(), str, entry.p5_num);
    }
    // else if(entry.p7 == "-1"){ // 光明大燈
    else if(type == "7"){ // 光明大燈
      var str = entry.p7_name2.trim();
      printBigLabel(doc, entry.people_name.trim(), str, entry.p7_num);
    }

    old_status = now_status;
  };
}
Api.lightPage2 = function(doc, arrObj, datetime, type){
  function printTitleTable(doc, datetime, entry, nowPage, rowPage, colPage){
    var title_y = 14;
    var title_right_text_shift = 610;

    doc.font('BiauKai').fontSize(14).text("財團法人", 40, title_y+4);
    doc.fontSize(20).text("桃園佛教蓮社", 120, title_y);

    doc.font('Times-Roman');
    doc.fontSize(20).text(entry.year, 260, title_y+2);
    doc.font('BiauKai');
    doc.fontSize(20).text("年度 "+entry.light_text+" 簡易疏文", 300, title_y);

    Api.dateTW(doc, title_right_text_shift, title_y+4, 14 , datetime);

    // 外框
    doc.lineJoin('miter').rect(table_x, table_y, table_width, table_height).stroke();

    var left_shift = table_x;
    var top_shift = table_y;

    // 畫直的線
    for(var i = 0; i < colPage; i++){
      doc.moveTo(left_shift, table_y).lineTo(left_shift, table_y+table_height).stroke();
      left_shift += cell_width;
    }
    // 畫橫的線
    for(var i = 0; i < rowPage; i++){
      doc.moveTo(table_x, top_shift).lineTo(table_x+table_width, top_shift).stroke();
      top_shift += cell_height;
    }

  }
  function printPage(entry, rowPage, colPage){
    var arrTmp = _.where(arrObj, {year: entry.year, light_text: entry.light_text});
    // console.log(entry.year + " " + entry.light_text + " " +arrTmp.length);

    var totalPage = Math.ceil(arrTmp.length/(rowPage*colPage));

    var footer_width_shift = table_x+350;
    var footer_height_shift = table_y+table_height+15;

    doc.fontSize(12);
    doc.font('BiauKai').text("第", footer_width_shift, footer_height_shift);
    doc.font('Times-Roman').text(nowPage, footer_width_shift+15, footer_height_shift+1, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁，共", footer_width_shift+43, footer_height_shift);
    doc.font('Times-Roman').text(totalPage, footer_width_shift+83, footer_height_shift+1, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁", footer_width_shift+113, footer_height_shift);
  }
  function newPage(doc, datetime, entry, nowPage, rowPage, colPage){
    doc.addPage();
    printTitleTable(doc, datetime, entry, nowPage, rowPage, colPage);
    printPage(entry, rowPage, colPage);
  }
  var x = 0, y = 0;

  var nowMember = 0;
  var nowPage = 1;
  var rowPage = 4; //10;
  var colPage = 7; //8;

  // 整個頁面的loop
  var table_x = 15, table_y = 50;
  var table_width = 810, table_height = 500;

  var year = arrObj[0].year;
  var light1 = arrObj[0].light_text;
  // if(type == "1" && arrObj[0].p1 == "-1"){ // 光明燈
  if(type == "1"){ // 光明燈
    rowPage = 10;
    colPage = 8;
  }
  // else if(type == "3" && arrObj[0].p3 == "-1"){ // 藥師燈
  else if(type == "3"){ // 藥師燈
    rowPage = 10;
    colPage = 8;
  }
  // else if(type == "5" && arrObj[0].p5 == "-1"){ // 光明大燈
  else if(type == "5"){ // 光明大燈
    rowPage = 4; //10;
    colPage = 7; //8;
  }
  // else if(type == "7" && arrObj[0].p7 == "-1"){ // 光明大燈
  else if(type == "7"){ // 光明大燈
    rowPage = 4; //10;
    colPage = 7; //8;
  }
  var cell_height = table_height/rowPage;
  var cell_width = table_width/colPage;

  printTitleTable(doc, datetime, arrObj[0], nowPage, rowPage, colPage);
  printPage(arrObj[0], rowPage, colPage);

  for(var i=0; i<arrObj.length; i++){
    var entry = arrObj[i];

    if(!!entry){
      // console.log(entry);

      // if(type == "1" && entry.p1 == "-1"){ // 光明燈
      if(type == "1"){ // 光明燈
        rowPage = 10;
        colPage = 8;
        n2 = entry.p1_name2.trim();
        // cell_height = table_height/rowPage;
      }
      // else if(type == "3" && entry.p3 == "-1"){ // 藥師燈
      else if(type == "3"){ // 藥師燈
        rowPage = 10;
        colPage = 8;
        n2 = entry.p3_name2.trim();
      }
      // else if(type == "5" && entry.p5 == "-1"){ // 光明大燈
      else if(type == "5"){ // 光明大燈
        rowPage = 4; //10;
        colPage = 7; //8;
        n2 = entry.p5_name2.trim();
      }
      // else if(type == "7" && entry.p7 == "-1"){ // 光明大燈
      else if(type == "7"){ // 光明大燈
        rowPage = 4; //10;
        colPage = 7; //8;
        n2 = entry.p7_name2.trim();
      }

      // n2 太長要斷行
      var arr_n2 = n2.split(",");
      for(var j=0; j<arr_n2.length; j++){
        var s = arr_n2[j];
        if(s.length > 6){
          arr_n2[j] = arr_n2[j].splice(6, 0, ",");
        }
      }
      n2 = arr_n2.toString();

      cell_height = table_height/rowPage;
      cell_width = table_width/colPage;

      if( year != entry.year || light1 != entry.light_text){ // 開新的頁面
        year = entry.year;
        light1 = entry.light_text;

        nowPage = 1;
        newPage(doc, datetime, entry, nowPage, rowPage, colPage);
        x = 0;
        y = 0;
      }

      // "姓名"
      doc.font('BiauKai').fontSize(16);
      var xx =  table_x+x*cell_width +2;
      var yy =  table_y+y*cell_height;

      var n1 = entry.people_name.trim();
      if(n1.length > 6){
        n1 = n1.splice(6, 0, "\n");
      }

      doc.text(n1, xx, yy, { width: cell_width, align: 'left'});
      if(!!n2){
        n2 = n2.replace(/,/g, "\n");
        doc.text(n2, xx, doc.y+2, { width: cell_width, align: 'left'});
      }

      if(x == colPage-1){ // 如果到每列的最後一個
        x = 0;
        y++; // 下一列
      }
      else{
        x++; // 往前進一個
      }

      if(y == rowPage){
        nowPage++;
        newPage(doc, datetime, entry, nowPage, rowPage, colPage);
        x = 0;
        y = 0;
      }
    }
  }
}
Api.lightPage3 = function(doc, arrObj, datetime, type){
  function printTitleTable(doc, datetime, entry, nowPage){
    var title_y = 14;
    var title_right_text_shift = 610;

    doc.font('BiauKai').fontSize(14).text("財團法人", 40, title_y+4);
    doc.fontSize(20).text("桃園佛教蓮社", 120, title_y);

    doc.font('Times-Roman');
    doc.fontSize(20).text(entry.year, 260, title_y+2);
    doc.font('BiauKai');
    doc.fontSize(20).text("年度 "+entry.light_text+" 詳細疏文", 300, title_y);

    // Api.dateTW(doc, 630, 20, 14, datetime);
    Api.dateTW(doc, title_right_text_shift, title_y+4, 14 , datetime);

    var table_x = 15;
    var table_y = 50;

    // var col_width = [70, 190, 170, 390];
    // var    = ["燈號", "姓名", "生辰", "地址"];
    // var col_title_y_shift  = [8,  8,  8,  8];
    // var col_title_fontsize = [14, 14, 14, 14];

    var left_shift = table_x;
    var top_shift = table_y;

    // doc.lineJoin('miter').rect(table_x, table_y, table_width, cell_height).fillAndStroke("#CCC", "#000");
    doc.lineJoin('miter').rect(table_x, table_y, table_width, 29).fillAndStroke("#CCC", "#000");
    doc.lineJoin('miter').rect(table_x, table_y, table_width, table_height).stroke();

    // 畫直的線 & 標題
    for(var i=0; i<col_width.length; i++){
      doc.moveTo(left_shift, table_y).lineTo(left_shift, table_y+table_height).stroke();
      // doc.fontSize(col_title_fontsize[i]).fillColor("#000");
      doc.fontSize(16).fillColor("#000");
      doc.text(col_title[i], left_shift, table_y+7, { width: col_width[i], align: 'center'});
      left_shift += col_width[i];
    }

    // footer
    var footer_width_shift = table_x+350;
    var footer_height_shift = table_y+table_height+15;

    doc.fontSize(12);
    doc.font('BiauKai').text("第", footer_width_shift, footer_height_shift);
    doc.font('Times-Roman').text(nowPage, footer_width_shift+15, footer_height_shift+1, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁", footer_width_shift+43, footer_height_shift);

  }
  /*function printPage(entry, nowPage){
    var arrTmp = _.where(arrObj, {year: entry.year, light_text: entry.light_text});
    // console.log(entry.year + " " + entry.light_text + " " +arrTmp.length);

    // var totalPage = Math.ceil(arrObj.length/rowPage);
    // var totalPage = Math.ceil(arrTmp.length/20);

    var footer_width_shift = table_x+350;
    var footer_height_shift = table_y+table_height+15;

     // doc.fontSize(14);
    doc.fontSize(12);
    doc.font('BiauKai').text("第", footer_width_shift, footer_height_shift);
    doc.font('Times-Roman').text(nowPage, footer_width_shift+15, footer_height_shift+1, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁", footer_width_shift+43, footer_height_shift);
    // doc.font('BiauKai').text("頁，共", footer_width_shift+43, footer_height_shift);
    // doc.font('Times-Roman').text(totalPage, footer_width_shift+83, footer_height_shift+1, { width: 25, align: 'center'});
    // doc.font('BiauKai').text("頁", footer_width_shift+113, footer_height_shift);
  }*/
  function newPage(doc, datetime, entry, nowPage){
    doc.addPage();
    printTitleTable(doc, datetime, entry, nowPage);
    // printPage(entry, nowPage);
  }
  function printRow(doc, num, name, birthday, addr){
    // "編號"
    Api.strChtEngMix(doc, num, left_shift+5, top_shift+col_data_y_shift[0], 16);
    left_shift += col_width[0];

    // "姓名"
    doc.font('BiauKai').fontSize(16);
    doc.text(name, left_shift+5, top_shift+col_data_y_shift[1], { width: col_width[1]-5, align: 'left'});
    left_shift += col_width[1];

    // "出生年月日"
    var tmp = "";
    if(!!birthday){ // entry.lunar_birth_text
      // doc.text("民"+entry.name_date, left_shift+3, top_shift+col_data_y_shift[2], { width: col_width[2], align: 'left'});
      Api.strChtEngMix(doc, "民"+birthday, left_shift+3, top_shift+col_data_y_shift[2], 16);
    }
    left_shift += col_width[2];

    // "住址"
    // doc.font('BiauKai').fontSize(12);
    // var addr = entry.addr;
    if(!!addr && typeof addr != "undefined"){
      Api.strChtEngMix(doc, addr, left_shift+3, top_shift+col_data_y_shift[3], 16);
    }
  }

  var nowMember = 0;
  var nowPage = 1;

  // 整個頁面的loop
  var table_x = 15;
  var table_y = 50;
  var table_width = 810;
  var table_height = 493; // (rowPage+1)*cell_height;
  var col_width = [70, 200, 165, 385];
  var col_title = ["燈號", "姓名", "生辰", "地址"];
  var col_data_y_shift   = [8,  8,  8,  8];

  var title_top = 15;

  // console.log(arrObj);
  if(arrObj.length == 0) return;

  var year = arrObj[0].year;
  var light1 = arrObj[0].light_text;


  printTitleTable(doc, datetime, arrObj[0], nowPage);
  // printPage(arrObj[0]);

  var cell_height = 29;
  var left_shift = table_x;
  var top_shift = table_y + cell_height;

  while(nowMember < arrObj.length){
    var entry = arrObj[nowMember];
    // console.log(entry);

    // top_shift += cell_height;
    left_shift = table_x;
    doc.moveTo(table_x, top_shift).lineTo(table_x+table_width, top_shift).stroke();
    // console.log(top_shift);

    if(!entry){
      continue;
    }

    var n2 = "";
    var num = "";
    if(type == "1" && entry.p1 == "-1"){ // 光明燈
      n2 = entry.p1_name2.trim();
      num = entry.p1_num;
    }
    else if(type == "3" && entry.p3 == "-1"){ // 藥師燈
      n2 = entry.p3_name2.trim();
      num = entry.p3_num;
    }
    else if(type == "5" && entry.p5 == "-1"){ // 光明大燈
      n2 = entry.p5_name2.trim();
      num = entry.p5_num;
    }
    else if(type == "7" && entry.p7 == "-1"){ // 光明大燈
      n2 = entry.p7_name2.trim();
      num = entry.p7_num;
    }

    var arr_names = [];
    if(n2 != "" && n2 != "闔家"){
      entry.names = entry.people_name + "," + n2;
      arr_names = entry.names.split(",");
    }
    else{
      arr_names.push(entry.names);
    }
    // console.log(entry.names);
    // console.log(arr_names);

    // 印出一列的每一個名字
    for(var j=0; j<arr_names.length; j++){
      var name = arr_names[j];

      if(j == 0 && n2 == "闔家"){ // 一列的話
        entry.people_name = entry.people_name + "闔家";
        // var birth = entry.lunar_birth_text+entry.lunar_time_text;
        // printRow(doc, num, entry.people_name, entry.lunar_birth_text, entry.addr);
        printRow(doc, num, entry.people_name, "", entry.addr);
        birth = "";
      }
      else if(j == 0 && n2 != "闔家"){ // 一列的話
        var birth = entry.lunar_birth_text+entry.lunar_time_text;
        printRow(doc, num, entry.people_name, birth, entry.addr);
        birth = "";
      }
      else{ // 第二個人以上
        // console.log(entry.familyId +" "+ name);
        var p = People.find({ familyId: entry.familyId, name: name}).fetch()[0];

        var birth = "";
        if(!!p){
          if(!!p.bornYear){
            birth = p.bornYear + "年";
          }
          else{
            birth = "吉年";
          }
          if(!!p.bornMonth){
            birth += p.bornMonth + "月";
          }
          else{
            birth += "吉月";
          }
          if(!!p.bornDay){
            birth += p.bornDay + "日";
          }
          else{
            birth += "吉日";
          }

          if(!!p.bornTime_text){
            birth += p.bornTime_text + "時";
          }
          else{
            birth += "吉時";
          }
        }
        else{
          birth = "吉年吉月吉日吉時";
        }
        var addr = "";
        if(!!p && j==0){
          addr = p.addr;
        }

        // printRow(entry, num, name, birth, addr);
        printRow(doc, "", name, birth, addr);
        birth = "";
      }

      top_shift += cell_height;
      left_shift = table_x;
      doc.moveTo(table_x, top_shift).lineTo(table_x+table_width, top_shift).stroke();

      if(top_shift > table_height+29){
        nowPage++;
        newPage(doc, datetime, entry, nowPage);
        top_shift = table_y+29;//cell_height;
      }
    }

    // for(var i=0; i<rowPage; i++){
/*    while(top_shift < table_height){
      var entry = arrObj[nowMember];
      console.log(entry);

      if(!entry){
        nowMember++;
        continue;
      }

      if(!!entry && (year!=entry.year || light1 != entry.light_text)){ // 新的燈種
        top_shift += cell_height;

        // 畫橫的線
        doc.moveTo(table_x, top_shift).lineTo(table_x+table_width, top_shift).stroke();

        left_shift = table_x;
        top_shift = table_y+29; //cell_height;
        i = 0;

        nowPage = 1;
        newPage(doc, datetime, entry, nowPage);

        year = entry.year;
        light1 = entry.light_text;
      }
      else{
        cell_height = 29;

        top_shift += cell_height;
        left_shift = table_x;
      }

      // 畫橫的線 每列中間的那一條
      doc.moveTo(table_x, top_shift).lineTo(table_x+table_width, top_shift).stroke();

      nowMember++;
    }*/
    nowMember++;


    // if(nowMember < arrObj.length){
    // if(top_shift > 514){
    if(top_shift > 543){
      top_shift = table_y;
      nowPage++;
      newPage(doc, datetime, entry, nowPage);
    }
    /*else{
      break;
    }*/
  }
}
Api.memberPage1 = function(doc, arrObj, datetime){ // 簽到名冊
  var nowMember = 0;
  var nowPage = 1;
  var totalPage = Math.ceil(arrObj.length/26);

// console.log(datetime);
  // 整個頁面的loop
  var table_x = 30;
  var table_y = 55;
  var table_width = 780;
  var table_height = 488;
  var cell_height = 35;
  var col_width = [40, 35, 70, 40, 40, 90, 70];
  var col_title = ["市府\n編號", "班別", "姓名", "性別", "蓮社\n編號", "簽到", "備註"];
  var col_title_y_shift  = [5, 10, 10, 10, 5, 10, 10];
  var col_title_fontsize = [12, 16, 16, 16, 12, 16, 16];
  var col_data_y_shift  = [11, 8, 8, 8, 11, 10, 10];
  var footer_width_shift = table_x+315;

  while(nowMember < arrObj.length){
    doc.font('BiauKai').fontSize(14).text("財團法人", 90, 18);
    doc.fontSize(20).text("桃園佛教蓮社社員大會出席簽到名冊", 200, 15);

    Api.dateTW(doc, 630, 20, 14, datetime);

    doc.lineJoin('miter').rect(table_x, table_y, table_width, cell_height).fillAndStroke("#CCC", "#000");
    doc.lineJoin('miter').rect(table_x, table_y, table_width, table_height).stroke();

    var left_shift = table_x;
    var top_shift = table_y;

    for(var h=0; h<2; h++){
      for(var i=0; i<col_width.length; i++){
        doc.moveTo(left_shift, table_y).lineTo(left_shift, table_y+table_height).stroke();
        doc.fontSize(col_title_fontsize[i]).fillColor("#000");
        doc.text(col_title[i], left_shift, table_y+col_title_y_shift[i], { width: col_width[i], align: 'center'});
        left_shift += col_width[i];
      }
    }

    var sumColWidth = _.reduce(col_width, function(sum, el) {return sum + el }, 0);

    for(var i=0; i<13; i++){
      top_shift += cell_height;

      doc.moveTo(table_x, top_shift).lineTo(table_x+table_width, top_shift).stroke();
      for(var j=0; j<2; j++){
        var tmpMember;
        if(j==0){
          tmpMember = nowMember;
          left_shift = table_x;
        }
        else if(j==1){
          tmpMember = nowMember+13;
          left_shift = table_x+sumColWidth;
        }
        if(!!arrObj[tmpMember]){
          doc.font('Times-Roman').fontSize(14);
          doc.text(tmpMember+1, left_shift, top_shift+col_data_y_shift[0], { width: col_width[0], align: 'center'});
          left_shift += col_width[0];

          doc.font('BiauKai').fontSize(16);
          if(arrObj[tmpMember].templeClass_text)
            doc.text(arrObj[tmpMember].templeClass_text.charAt(0), left_shift, top_shift+col_data_y_shift[1], { width: col_width[1], align: 'center'});
          left_shift += col_width[1];


          var name = arrObj[tmpMember].name.trim();
          if(name.indexOf("(") != -1){
            name = name.substr(0, name.indexOf("(")).trim();
          }
          doc.text(name, left_shift, top_shift+col_data_y_shift[2], { width: col_width[2], align: 'center'});
          left_shift += col_width[2];

          if(!!arrObj[tmpMember].sexual_text)
            doc.text(arrObj[tmpMember].sexual_text, left_shift, top_shift+col_data_y_shift[3], { width: col_width[3], align: 'center'});
          left_shift += col_width[3];

          doc.font('Times-Roman').fontSize(14);
          if(!!arrObj[tmpMember].memberId)
            doc.text(arrObj[tmpMember].memberId, left_shift, top_shift+col_data_y_shift[4], { width: col_width[4], align: 'center'});
        }
      }
      nowMember++;
    }

    doc.fontSize(14);
    doc.font('BiauKai').text("第", footer_width_shift, table_y+table_height+15);
    doc.font('Times-Roman').text(nowPage, footer_width_shift+20, table_y+table_height+16, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁，共", footer_width_shift+50, table_y+table_height+15);
    doc.font('Times-Roman').text(totalPage, footer_width_shift+102, table_y+table_height+16, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁", footer_width_shift+135, table_y+table_height+15);

    nowPage++;

    if(nowMember+13 < arrObj.length){
      doc.addPage();
      nowMember+=13;
    }
    else{
      break;
    }
  }
}
Api.memberPage2 = function(doc, data, datetime){ // 封面
  var imageBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAokAAAKvCAYAAAAGIlUKAAAgAElEQVR4nOy9WbAk2Xnf9/vOOZlVde/tnumZAQaYAUCsxEoQJAiAiyiGSFkkRQmUSZGSxTA3mTLDkmzrwcEIhyP84hc/2A9+sMN6s2RTpLhIwQ0ExQ3gAoLEQqwczAIMlsFwMFsvd6nKPOd8fjiZWVnbvd0zPdM9098v4kbdyszKrbLy/PNbRVUxDMMwDMMwjDHuRu+AYRiGYRiGcfNhItEwDMMwDMPYwESiYRiGYRiGsYGJRMMwDMMwDGMDE4mGYRiGYRjGBiYSDcMwDMMwjA1MJBqGYRiGYRgbmEg0DMMwDMMwNjCRaBiGYRiGYWxgItEwDMMwDMPYwESiYRiGYRiGsYGJRMMwDMMwDGMDE4mGYRiGYRjGBiYSDcMwDMMwjA1MJBqGYRiGYRgbmEg0DMMwDMMwNjCRaBiGYRiGYWxgItEwDMMwDMPYwESiYRiGYRiGsYGJRMMwDMMwDGMDE4mGYRiGYRjGBiYSDcMwDMMwjA1MJBqGYRiGYRgbmEg0DMMwDMMwNjCRaBiGYRiGYWxgItEwDMMwDMPYwESiYRiGYRiGsYGJRMMwDMMwDGMDE4mGYRiGYRjGBiYSDcMwDMMwjA1MJBqGYRiGYRgbmEg0DMMwDMMwNjCRaBiGYRiGYWxgItEwDMMwDMPYwESiYRiGYRiGsYGJRMMwDMMwDGMDE4mGYRiGYRjGBiYSDcMwDMMwjA1MJBqGYRiGYRgbmEg0DMMwDMMwNjCRaBiGYRiGYWxgItEwDMMwDMPYwESiYRiGYRiGsYGJRMMwDMMwDGMDE4mGYRiGYRjGBiYSDcMwDMMwjA1MJBqGYRiGYRgbmEg0DMMwDMMwNjCRaBiGYRiGYWxgItEwDMMwDMPYwESiYRiGYRiGsYGJRMMwDMMwDGMDE4mGYRiGYRjGBiYSDcMwDMMwjA1MJBqGYRiGYRgbmEg0DMMwDMMwNjCRaBiGYRiGYWxgItEwDMMwDMPYwESiYRiGYRiGsYGJRMMwDMMwDGMDE4mGYRiGYRjGBiYSDcMwDMMwjA1MJBrGCxDt/p7tOlL3t7Hi8d9o1nPGc7ryW5PrdUp1x6thGC9+wo3eAcO4pelHXFl9y+rkMmO0TO4m+7V566s9jQzE7j/Xr6Jfsboywa1us3+qXNkvAFUQWf7vljukqqiAdJ/S7kMOWVUe4+PYckzbDlBFEZGN6avLLvdnfbZ2E5ysvt923q9mv/pjQwRFEcqr649d+/3NqAoi0h36cs9ktIHlf5mtqFtZcHx84+9rY327LpBT1rP+urp/hmG8GDGRaBg3CVsFoq4tcJWjslzF4m7b/PEHZXP5FZSRqhotLKuCSKTIHu2Wdd38nDNkQVXxoaw9xYyvyv9ZyzaLwNxyULIqqHahqkimKMGRYFV02Jfx4W+doN1+jE5CSop3gvb6zTEI1vHX5saiT2Q4Bhkdy/px9KLxzOM7RR/vFIgbb7avtl+f2/Jq4tAwbg2kv3EbhnHjOFUgbpvZaw2FooD6Zd3qclf785ZVCyWsiowVUbDigl5aELctozlvWvpkUEbDa+6WGyxrOZNRgvODLuxfM9oddxF6qmnYhgyadYtJbJuyUbcqdEcWUc0Z8Y6UEt6X/Ui5bMuJGyyFq4c9spLSCdTxMa+RdfusHYtv7j7L72yrde+U499ptd61wPq80fdnGMaLExOJhnGD2fYLvBqROMzbJRKvZeNSLGV5ddKZwkNX9F8RRDK2JG5TEDkXFSRC1IRzIHggE1PGOXASGBybmgCHiDJ2eA7iUEeOUN3ymiO4bn0qQAIJp4ucsV9WirWzTZEqVMVNnyPBBRQp1s7earp+xONz1QvmsxTgNQhEOCMM4JT1nyowzxoWTCQaxi2BiUTDuAnYOeBvEYibxqEd8WrAij3wGqyKu/ZvfXbOGdyGNBqIRNyZ+XF5dNyOTIQEwbsiwTSRM+QcURVSaslthtiSThZ4VXJSYttoM29o53NSm8iaWBzPadsGTYpqRsQxmU04f3Cean9G8kLroKoqqqoS8Y6qqgh1hYhQH+wX66J3JM04cWTNg99ZkBV3cjlZWkSjc+Scca5zn3eWxxVhlnpX+hbxeEos5cp38Uxv4adYj88KBzVdaBi3BhaTaBg3krGns5u0Mmhfh9F4GNR3rUvXFtyiAlbfjiSdWwpA1YRoZ/kTDyiVSrF0qoPYlumO7n0DPgCKjxGcB1F8UnAOFku7posJp0CMVClD00KM0KKolhTtqOix0swTqc2QEyfHmdgmUipuaYdnljO3ewGnZFGyV0JdQcyK95BUSFLujseU/RfBi3b7F8FXkFIRi95Byp2FtyT8SM7gwSWFUM6Xo5wX73LZX+9XLcI5lvMjuXsdxS/yzLXg9eJUy6VhGC9KzJJoGDcKhUESDi5At+Hy7dlqYTzD6tdvYZstb6trccW61IubvLbN1TXmnIs1TRSyK+IpyXLjMUHM0EY4aZRLF7n06GNcevJJaBqOL11hMpmwN5vRzhec39tnfnzCH//BB3C5uLCdCEGlCMVu31WVrIo6xeMRp4g6JKeSOUxGs4AmvK9wDkiQSEgWEgl8KO/Fo1KWz5LBeRKJGDPVtGIRE9Uk8MY3v5Wq8rQpM9vfI1eeLA5Nmf1zB7zk7rsJ+3uoJiJKdXAO7rpdODiA2hfVL1qshMGVc+mFoihLIs9wfnc8IfRRj/3pXb5b+15PYSz4xs8EQ1LKzhgIsyQaxq2EiUTDuFGcIhL72ePBe1hMWQ7ia+pvm0Acs11MdJa+PrZxWH9eriCPp3d71r/vrXrzBXrxsj79149x8YmnyCcL4vGcx778ZR5/5FFy01LjkKyQlUocPiVq52mahsoHnEJqI5OqglhK8ziKOBQtcX29UFQn5a9z79ItK10CiqpSOU9KCVUtVs9cXLhe3JAAk3SUNKNa1kknfoOnSZFQVyzaFl8FFm1DVVVElHmOhElNzsWS6kNN1EyjCR8C89QSphMWmkgODi7cwcte+XK+7jWv5e5XvYJFJdQHe8j588L+XrFKQud+XvviV2IAlyWKVlNlTmebtXp8bZwak2jK0DBuOUwkGsYN5TQZt0RTHsQP4kryh3cbMYqwTNBdz77NmpciamX7neUqp5LY0XaisUnQxLKBwyPl8hVYtHzxs5/BN/C1L36FS489Tnt4QtM0OFFEPKSIy4qXIlyc5mINZJR9PLzPz7qiv+j2/7fhtBjrxhbJdcaryLJMzulfBxEvZX5Zb0mUEXUryTy5W06lGFezuGKxxJEFshMaTYhzRFEInle89tW87s1v5uCO25lcuK1YGu99mXD+oCTphE7OSS4HUvtuRzo3/dpBxZzwriLlhHe+O8ZlDcf+Ghl/TFNGRuvqE3PGNS+zZrw822/PMIybGROJhnFDWROJa9nJKUZ8FTaUoOYyiGcdciiKNStnvF9axUS2qEgoSrJtl8rgeAGHx3B4opcf/Wseue8LPPxX93HxiScJCJoyLif2w4z26Ijz1R7p5IRzfoo2TdGsXorbVhN0lrtyf1la/676rIyzptdE2pix6Hwm+DM+ty4YT9uP8TGuL6Ld5xWHujx8z65LbpHgiSgn7QImFdQ1J2lB8p5cCSdti9aBC3fdSfbCHXfdxXu+8zthf4K/84IgArMJnNsHusSaoF2cJ7QpEnwAHCm3BFetJNXsOnClXEPjZXVUJsiMi4bx4sZEomHcQFZCC3e4+BSIXWZub/lJXfmXauRmzLlk3wIlLtBLl1DhxuZFaFuuPPkUj3z+83rQJr78yft4+OOfwR037GdHFRNeAj4UoZlzpsoCOeKTEJygKngvJctYMk5LTCDqUNIg2pyyouDGYq6U3NkuM65FVI4tg+NpY7YJu35/TtvWinVty7p1x7GNpdfY8gjSfaYTWSLkmMBJEYwU4ajiWMQFiCeTqKpJV3qnNFFUVfCBnDPRwcI75gHufcsbeNkbX0s6P+Ou17yCO1/3GqFyJX9cBE1FzK9brHNKOO+H1/4hpMRIrp48zXmZ3+TNkmgYL2Ysu9kwbha2pLBmdOmypP/LOPHDMN80c2ofupi7VFakAvO2WJJyLskjzkNSOF5w7rDVC5daDuYN80stx4cJv1D2FIJUxBjJOaNd8kQVHKIeTQmPo21bJPiSh+H9kIuhXSdo7fNY1kx841yMsSt23WW8LvLWhdw4KfharZT9/vWk0f/rFsmzxOeudeYt+yRaRKUMcZ3gVIr4zwo5D+koAuw5jyQlJSWk0kAxpURVVYgE2pOGaV1z0jbkieekybz0KHP3sXKSGu640MJcICrOSenhqJRrQXLnmi6xj8754q32HrryPWUHy0H1cZ9O3DBveLAxc6JhvGgxS6Jh3KT0WcYZRUm43mrYxYK1TUNV12UpBdqu9EwGWi3TLh6SP/9FfeyLX4F5w9e+9AgPP/AQLmamQHW8YN972pSovCc2LV4cAcGzTOSIdLFoIpAV360+ltTjsr/dvaRPAtmqlNaPUTazr09zHV+rW3mXBXG87TFDnsg1bGfbsmcK104s5lzc8kP7QhFil2jjvS9WUldEeaYsG2NEVZlOp7SLhrquadqECrgqsNBEdI5YORoPL33tq3jdN7yFPKu4cO89hDe9QYbM6tp3B6BD28KSPFPqQqr0NTCLFduP4hdPdVcbhvGiwESiYdxkrGegljSFTggmLX9Doslo4cuXOHr4K3r/pz/NYw8/wjkX+PIDDzETRxWFKiZkEQkI+9WENG845wMxxiFLVwVyTHiFyvnBmthqRgW8L9nCU1+sjQllvVTLkODQx0SuH19fBeaUGMOr5WqWX7dUjqevC8VrFYlXu9ymJbSzJDqPqpJS6mYsT4o6IcZYWgJ2hbW998M5jTHiXHl0yH3WtyqaBdfFOLZOYTrlSjtnHiBPK3LtufuV95InNW/99ndx7o47mLzm64QqlBMQG5hOR9nVJbYxU0IPxskvV5dTbRjGCxUTiYZxIxn//GRco7DgNCFJO5OiK67Cp6/ApUM9/sKXefyBh/j0hz9Ks1gwqyuaecuk8kxcTdvMmbiAtg2ShDoUC2GOCXIn4FIeyq70oq63EPU9i4voW8bQxZSG+VUoJW10JBSLQFzPol495HH5nW1Wt9PculfDeJ3bYhaX89zObjLXsp0hqWXHcuPj7T+Xu1I2Rdtp1xNaVkR2zrlYCps5IkIIgcVigWRlNpvRpJa2banrevjeQqiYz+eId13nl0D2MlilY06oCNl7GoVqf8Y8tbzh7W+lCY63f+e3wV23wz13F4tjbGA2HdLTVbDEFcO4RTCRaBhsaLUziwmvTbq6DZxWb2VYd16+TRQRF4EnLvHEJz6jf/Xhj/CVB7/AzFf44zkXsoN5y7QqIiHnjPe+WKCkJJH01qaUEr5zX/aWq9zHlynDZ1NKhLoi5tTFGioOj2QteQyu1Bj04pAUV9zM0AseP2TujtmMNVwer45ed3Et4nGbMNxMRHFb17leKucstonErRbMrhZlPz2rDHUbe1HYu51LIlInGmM7yhaHEAJN04CX4TuDkkiSUkLEFTd2//3pcv2TyYTURnBCxtGmiNSB6IUTlHkltBPPoba8+RvfTtib8oa3vYWDN71BOHcAVeeSFi3JUZ3YvRbWLnvDMG5STCQatzi5eHFHg5xnGaa1UrR6i6UPjUOtuBWxtFbKZlBAXdhXPzqKJpBi3RFXklEkJtLTl3n445/RB//kL7g9eR77q4fYV4+LcST4hLqLD9zGMy0LM+zyDjftWfOeCWNR9kysiFfrul7p2NJNG9dCHH/22R7j1X7+ep/Lq2FsyQTwGVKXzNz48tfvl0uKA1JwHFWOS1XkO37o73HPN76JyatfJVRCaWe9fs0r4EsZpxCGaz5rn4ileBTtvgnpOmGrjup77rDQjhOXtk3nrPmmUg3jqrDsZuPWpRcMsiYaKELxNIbYNVlay8Z15HIuJWqcKy0xtBOc0iWU5tyF8nWfDSol1rBNoB5/udV7juHKUWa/zWgj+JhIsS0xagK+8uQYr9/5WON6JpCcRS8Mn62becw28bUsfn369tZrMz6T473ecY3Xk+VxF3e/Y9QVkK7dNOCTMhVPpUKzSFSyYP/8hDuPWiZHCebAvFgicblc6FVJoy5iT4tAhOFBaVy6c1dc41g4niYETeMZxnOLWRKNW5diQiz41SSGcXeQnZ+FwTq4PnnoZqcZJ6UDMl1tw7LdXD6XgEWEJ5/mqU9/Th/88MeRyyccP/oE8fCEmffUztPOF4QQhnXkmHbG/N1KnJa9fDXcCIF2M9GfP5+XD0rJlbJAKlCJK4lMSUsdxcpxlFo4N+VyirTO8UM/+eMcSWT/rV8vvOR2qOjM8d3DkHOkRYOvqpJ9D0sLoergEi+tE/3QajAtF1ttS9n/unpr/bb4jzMskGZJNIyrw0SiceuyJhLHLe5WBpPNQLalCpRRaRh653W3SI4E5yG2ZXBMCictVBU8eZFHP3ufuibx2Y98lMcf/CKTeeJgodwepuSjOZOqZtHOixu2sx6iaVkAuiuQfT2tby9EznLX7urYImoicZuldJz13SfUhC4eMrWxFNB2jlYhIbj9PZ7OCxZTz0vf+Bre9be/i8Mcufud7+ir5zD0mu6z4YXOrN5vdPTaPXhl7azujEXiKNhDu/U+E5FoGMZVYSLRuLXZllCy9pMYBkyWVUHGg2pWyKqlLd1yKmiXdNJEuDKHi1c0PfBF/vKDH+KRzz3Anq/KIJyVCQ6fFBaRiQ+IlLjDajqhaVtctcxKDkhxTwOt6C0vEuH0BJOz2und6mwT2b1Q9N7TxDiUOfJJmYUamogKxLriStPgq0BMCSpPdBBD4MRlvv17v4eXvel18NY3CUSYhPJlaYK67n5XriTY9zHAq7Wfuoe3pThcWhMdzzRhZnVdhmHswkSiccuynoTSj0nbytKk0SQ/Xm7sak6ldR45wdExHM/50ic/q/d96C/42v1f4KDJnMueWVImCbSJOFdaq5GVuq4RVU4WC1wohZSzKhklVF2v31z6DUvuSqQ4sySe1TO5Z5tYvNVFYp/Asm5RHBJbnJA0Q+hqNcZErQ6/iIQQmFeeJkUOZnucHB4xne4BcNI2xMpxqC3pYMJx7bkSIt/13h/gdd/6LcJtB1AH8AEVLW5ptPx+er/3SvBhXvtZFpGouDVL4+msOwLOij02jFsdS1wxbnnWB4612tBkxrFRo27DAm2KBB+QLEhf7mUR4ShDmnDP5cThiVDPHW4R8SkhKNE5VDKehHdCTJGmLZ+vao84R5sTIbhisenWnbvGd86tBUPewoxF8jh7uX/dsB7pplXxrFI3u3pBv5BFptPOMq6bLRIFqHL3kKIKkkldiSRFQENXPikjZA6PrzDbnzFfLMhdfc2Zwl6YMl9k8uUrTM5PuO0ownGrHIiwSOBBaod3paRO7MyJpVVhW8I1Ola/Arflvx3hIisr2NVjxzCMbZgl0bhlGYvDXUPFqqUxrw4+meJSzt3/CXj4y/rnv/17PPTRT7LXZs4lcCctU6B2AU2R1HXPKAkomdoXa2LwnqZpCFVFk0rsV9JcCii3LSEEpIsRE2XotHErs977eWwJW2cs9LZZzk7jxSwSYZmoAuWYqlxeM6VQeg6OJqeuP3guFm2ENqdSyBst124IOKTUZUyZtm2p9qa0Ho69cugzcRp46WteSVsF/sbf+TtU99wt3HWhM1kkqBw4fxW5JW5DFZ4qEmGLRdJEomGcholE40XPerHndXJUnB+PMl1CiGZUMkjpW0vOnWUjl+zk5ODSnJO//JT6eeQv//TDPP75L+MXkTpBpaWGoWge4hj7pJPewpX7xBftO6B1EVeymkAAy1jIPhOVYR3X/ZS9INDOHdqmRB2K0O6LgoeupeD4O98Wk9iXXxmya3X7dfJiFon9NTQ+Jt9fp53VtXWl4U9vceyvV5d1u8wanews5bNJlmI0C0TnoKq5Elve8s538MZvexdtDdU73ir4BLPOFY3HqSLqSrLWuB5j7uvqQJ8Uk1IqiV7j8jqDejRLomFcCyYSjRc1fauyMeNrfkg1ydrX62AonCiZnBJO/LJsTcxwfAJR+dKf/rn+2a/+GpPjhoNqBvMFoYVKhZmvaNsW6UTgeADuB+G+3Eg/ghXBcbqoGQ/gWUwklq4hoF2rQBFZsbAWy1c5v9vudan7XH+NSB5dG117PHhxikRYLV4+PqYVV7Qsr9X1h5Z1kbg8H8v6hqd1tGlShkmFVp5jUY5d5mgi/P2f+C84/6p70b1a5K47wJf4xWHDZRPlN+lK1kvMxc3dtwwcROKK6XApEou8NJFoGKdhItF4UdOX8Fj/vyfHYnUA0JyHMm4xRrxmRAK0Cg3w4MP6iff/AQ9+/BNMomMKsDjh3GTK4nhOXVeICuSMF0dq40igdoW2xxvfsGp0k0cu0F2JGLeqMFwnpUTorIilzt6y/Vw/3Y+Fd3e/cyIggga30ie5F5Sha2vXi80Xq0jcVWfytE47a1P6T6yIy21LbT2HTmg1l1hEV1o5NjHigqcNniMvvPFb38lb3vu98PKXCj5D3aWYOUEF5m1DVdUAJE148V1Ky5pAHA5umYg2JKsZhrEVE4nGLcG2rigrrkhVpB8ZNQ2j2qMf+5Sea+ET7/8ATz34MNPDlnNSUSfIzQLnIFQlllBFSk/jKtC2XY3EwTK1FIkr2bUbgfSbInHcHWTsGlQp2c63MpIVAXwIxBiRUPpWh67LR4xFqHtksAyWTiDlXEYp2eOiDD2u+/WOr5EXs0i8mraA4/ljN3VyqyJxvMw622JCMxl1gjhPakv5pwpHnLeE6YQTgYvScnHiePW73863/sP3kidO3EvugkqYty2hntCSuqZ+DukEoqC4oUgmy7jF9RCOqzlRhnGLYtnNxouefrAfWxRzzogvA0okE/phL3VpLFFh3nJX66kWidsOWxZXFkwSSFpw0rZMq4DmTDOP+CqQVNEuRkpVSZoRJ8s+0GewrYWc0xLHtT7APttOIy8GnFKshW1Eu7CCmLqONChtijjvlvF2OZfY0i72MAu4KnRdP0ocqmp/vVB6EecXuAq8Cq61v/T4wUXyqlt5PY521zb6WEftnpq8gmZFHBw3c0IVWMQFwVfs58xeqLmn8XCpwc2ccocKTplWFTE2TEPo8sdSF0IijKuWjtPd+5qnhmGcjVkSjRc9KxahdYtijiUuKSVoMhzNQYXLn/or/fgf/TGP3fd59trM+eRx85aZr/AoKSXUCV6W61QpSRTOOXwVVtyXQwuxNbZZEncexxZLzK1sSXTahQhIESquCsxjgwuhCBAnpXxLh6riRIaEjL5gOYDvHiI05eKiFlfi29KL293cs60bzbb5vUvZr1m3h/9HDzAbNUh1uY5hem/5daUOY9u2TKdTFosF3vvyHTtBvXAxtjT7Fcd7nrvf+kb+5j/8z+Geu4WcuyLdkFOLqyuUUkKqcn619mlvje/eWp1EwzgdE4nGi55x8srYFZmbFied1fDLX9Mv/OGH+PjvfoD6JLIXalwuLfA0tQQcE+8gJo4Pj7j93Hma1JJUEV/KffQZtapKtSW7dl1oFLHSidXBWph3lmcZJxiYSCw458AJTU60KVLvzVjkSMyJ7GSj6KXrrLxkxWVl6gLadbJxzpX2cymjafVhYp0Xg0jsxV4eZR33FkPfXVe9W3mc/QzL+Vnc1qZFPSs5I6PtQp/EpaSuYLxzDnyXnR48uQsVSKo0sSVMpyw0kYKg3nHshVd+0zfyjT/4A1AJvOxOYVKBJKg9CejTaoROEOpox2RtxwzD2MBEonHzsX5JnnIT1/VFtsVCaUacK1a73p28SDSPfFXrNvO5P/0I9/3Rn+MvnXCuyZz3E+K8xBvGrqyGBKFdLKh8oA4V8/kJ3ntyV++QXOLaVJXaB5qmoaoqNOdTXVunicQyv7xuy0Idzx8Of0dG9PVmXbg+gzWgkpctD2VplSrT3HLaKBYTVi1VKtCS8aEmS+ab3vVuMglXT4i5RfuSRThEFMlCSi1tmyAmQoJLTz7Fk48/wcn8CIcnyFLADH2y6RLe1+LuvOah+LTPS4vZ+HM9NyLZaHCr6/btV3m7SOyz6HvWY2GH8N1ehOmmCITtD0Y9Qxkd54oFPnja3HbzusLdqrRdcpJ4V2oxOk/lPUcpcrQ34aTynGjkm7/rO3jl297E7J3fILguwSW4wXI4RJyOH6xk8/9twnYX17KsYbwQMZFo3GA6V233bnAN5c6C070Mlpv+Y50loNVirfPsiP3TDN4RSQRKnCFPH/Gn//bf65c+/ln228wsQkgQchk0lwNZ70Zeru40N9w2dg3O4/mw25053u62aa4TmSsCa7Tc4FodpqybHjtXfGcBDX0CiAg4V0rEhNKIsBfBpIzr3bP9WoWVmE/oS8hIsQZ1bt0+4zi2GVc7Fk5pRbn9JXdyz6tfxUmOzG47R+sdbjbh3EvuQCvPZP+A6bmulZsqTKdw/kDwDryDUEPo7EXSPQj0r+x4HXZ+ZF5SB+0CrhzByVxpWkgKTYOezHnska+UhKSUicdzmstXOHziKThp2HMVX/ncQ4Sk1LgitDqXdqa4VhO6EvKgsV1JptLO8rm8rlaz8/0o+cY5h+Bo25bJZNKVXCrLplSKXPcPKQ5fYjS7MIjcx+mOrvH163xd6JZrzC1F+lbLeH+t5WHatmXHnxmv/1rIDhrnkCrQxJZGEzKbcegzF0n8k//+Z5m94y1CBdllVAKOcktw0t9YdFkyJ5d7xbgFZ4CVe4quVSTILC2pW9t69mwRoKPJhnHTYokrxk2BrL9xm/O33ntFliHqJesAnKM9aahmdVlRyoQYy7gVHTx1onc3jvmJMmsy4QyX7WmWuLOsdGcNfFc7MJ4VM7bO2KV3Gt6XmDBHJ+BiovLFarNYLAhVhWppP+hcET5OBB8CbU6lTaCAiiNLRlh3vLcAACAASURBVPColNIkSQDvymcnVfk/xrJf+xNaIlpXtC7R7jmoEo0XZntCrDxhLzA/F9AQqPaE2R64ieCcl2rimcwcbjIhOxlqHRbBtVp2aPfrtoeTTHRCq55YVZIWisSscQ7RCU+en5Q41EVLdJ5FEk4WjuQckxx58nxFSAmXhLSYMwkTnPPkqCiR4KoipiKIU1xdoykvi3qnhGgmEDoBJ8RYknG8Lz1/so5q/WlGgud4Mcc5RxVCud5DGOL6VJXUPQz0At47R8p55Xe27VrprZDDMqOTNpQrXJ9/FWyLW7xWXIY6F4twlZXb6op8nAhpwe0XzjN7eg7HCpXiJg6k7Wotdko2y1B824tDnCPnzlI6EnXrxsZxTONprvaVD139ZMO4qTBLonFDGW6yo9E6jd561gYvWb0xq6Zu8jCFhIKE0kYvUayHl+Z8/Od/WSeXT/jqffdTNRnXJGr0RRnXd1abuZ5FbKgmNQ6HU2i71mp9sk9Miaqa0OYEUsRYX1YmekheSjcOcbREsvPDtIVkGjLf8C3fTHXugHmOfN3rXsvdr34V7B8Itx0ACrkFF4pFsKjQQeyvmFD9yPqjCq5kl+vIirmrq84u+rhRGasCAFKJZUVAXFe0GWhaqKrlfgCkFqQCckl+Cq7U1jw+5PirX9OvPfpVZq7GNS2f+9in+dL991Mlh++FW864zvpYq1CljE9ClTNVZ7UUERKpREt4LUlSbUvlfRGQnWWsbdtBLPf39nE5oNRGah8I3hM76+61WvDG15Zfu87y2ml8rl3sfYZ7bIub2vmKHByX2wad1Ry6xHv/+c/A298sTKVzQQM5kcWRfCmWE6Aov6QQfOmE1N1Txkk3AAjE7t9iYx+xxf9sLmnjhYyJROOGsksk9i4fz1rAebfMcvFc3ERQSpYoEJsiOqLCxSMe/bOP6Z/98m8SLh9zAU+VoGkaplVdsptfwD+BXV0yetYH7Z7eIiLBl8SPpinuUd8VNG6aEueH0pKRKtCK0gDqHVGU5IULL3859bk9zt12G7ffdQd33vMyJi+7Gy6cFyY17E+BTjgNo612Fp00Mtn05qkhsm2UY9C5r7tKeNeVLaagsrW8Nk27CnylxEpOoyLso445qlquQyC2LSFUxZ2t3YW9SICHNsNTF8sHn36Srz70RZrjY6Z4vvzg5/nS/Q9R4wgIcX5C5TxVVQ1Z9b2rulKB2BYLZZdoVYdAu2hKdjbF9ZxzZjKZFNHbZXCPheQ6Z4m7/lrzo9M0DnXoYxifaySV2EWR0mUnRSXUFVLVzFNL8sLl3BBv2+fxPOcHf/rHuPNb3ykE3wWVZggVxATOl3ADgdS75rvt+OXFWOb3zy39tJWdWi66sb/9P7o+wTBuTkwkGjeU3nUz7rQ1nt4z3Iz7Qb2PwUtt16e1VD+TFohw+UMf1/155Nf+n3/HNMJeC3XOsIhMu/gtRjFZL0TW2/35TNcrenWZbe37eiGpTpgvWuq6JtSllaB2XTDwgVQ5WgdPpzlxGnjLe97FK9/4evKs4iWvf41w4cJS2JVgr9F7igh0UuZBZy2EEhOYGL5xYdQWkWGw3nns2oeTbXbRKfOXcZFXy3qKUVIdYteUtNK5hXExdl1OG1o6snoNl8LOdFkiucyQki1PkHK8ZFg0UNflw4dHpMefVC+O9sohX/n8wzz+1b9GsnJ86Qp//aWvUJ0s2BMHTWaqMMXDosUnLbGT0mX2d5bH3Flq+/jTHPOZcbMAuuVJqjyYjKJdz4ir7T9zPanEMV8scFUY2jLmnCnVpxQ3qThpGyRUMKu5pA3txPPD/+q/g1rg9a8qZkQPOC3no66ALkyiawU4fr4ZJsCpSnCwprJ2fzPTovECwkSicUMZ30hh88l8IzFkJBLL+9z9AW3iib/4pH7qdz/I05/9PLclITSJOgs+lazj1EZSSsz29zian+D9C/cuPcT9sRSHfV/nntP7O5e4tDCbcNJG5rTkEEi1pw3Cva/5Ol719V+POzfjrre+Ce68TagDzCogFQvMuGXFKAlg2DhFfDnpLTSpJGB4X3pi5NxZgPsgShnWM1iIRdDBnbx2Dra0Wuyn95+9GjK6WnyZvuiydmcqL+ePEkeGEzvu3pMzmdKzW5wnkclAlzaxTLIabSnnEnc4WCH7Uk0ZiBEkFBEdOxuwUqyRTeTSn3xYaxyfv+8+PvuRj7NP4MDXxMND9kJNc3zEwd4+qe0TkiDmUvbHi9+ZOb92RrdOle4DuxKt1hOurrdIDFKsoVIH5s0CgMoHNDGERtR1XQSyKsl7FiFzEhxN7TipPX//J3+Mc9/2TUINKTW4ybSztLvNzizjB9VtnGVF3GF1NIybFROJxo1l5HZZ8cDo5vyt5MwTDzyod2rNh3/9/Tz6kU9xW6PUi0QNhLri5OiYSVUv24mp0irLsjg3kF3Z0WfFFC7LkSwXXC1WvHrCNgpxd1nRzgUut3Pueu2rec03v4XLlfCKb34b3PMSYa+GKpSYuz4OL2UIfb6brG5GZOv31MZM8JuWwbEuRLcJwNFH1uZl3bm5a0JX/l8PsNNlncTVBZf/OCnJ0f3+52Ig7Pc5Jh0eRFJSgi8XelpE/CQM5ZnK5pZ1NUsmc7/t1GX7a2dhzUuxQvd/SmXDGfjrJ5QotPc9AE9e4YO/9dv444aJOiYuENvFYEkcFwvfVgh7GY+nQ8malVPEUgyOcSpbS+9cawLWWTgtcaXJUToo9dnfKVOLHy6y3GV513v7HLcnEKpSSzMELnrlpe95G+/5kR/geD/I7K4LOOdJubPGAuN6izv08k6BuHLIZkU0XmBYdrNx09DHyfVeyp03Y81l0EwZYuKuXMOVyMvnQnOUmLSJ/VBz0sxpU6KuKzQr2g0oPoTS81dk9zZeAJxVI3F9fhJI4ogOUpdskp1wMpvQVi17E+VySFw4X1FNMvV+oNFSc7AfJl3oStDkkVt5ZSd6sSWDUTCETgR13uQMpKx4J4NRpljayjJCWW5YXy/AOp3Erk0/Axfz2Po5tiQWj3gn2NK4KPrmuqWzbAuQOkthf7zBy7CJ4GVQX74ORWM6N1gxnTiyZpwUsZO0T6rx9HpHOle9at9+sN/3qrjAEebnK6kXiUdmqnLg+GLVctuFCdU84ZuWycwzcYHD+YK68gi5K6WUV+tArh4l4x/LtpJMY3ph+FzHJYpIaa2YEt6VMj904k67gunQZe6rllqYIjBfMHWOtFjwkv0Zd5xk3IlwkKNymxOcLr8vd8q96SzLYY+JQ+MFilkSjRtKjorzMlhUomZESkGQrLmEa3VWDBHpslpyKWXz5CU+8m9+Qf/6s/fjLh5z3gWqqJQIRR2sbKIZUVcGvc7CsbQM3NyWxD7BYKg16N3giizWpiKOMqXsTF+Dr5au+0uCyWzKUbsgTjwnwXHilaYS/uYPfC/5YI+Xv+MbhLsusIzNKkkmQ6eabS0FBxfzKQe3YzAchxisj53rsalnivjnaMC9HmP61nWsh01s2cCpguM0M9Uwr7MuSobYqZxLhyX5+ouP6BNf+CK//1vvJywSkyyElKiTY6oQouK7OMU+xi9pHASrl9KRxnW/R+1iUPss8dQVvu7rYq4L9ucrSaz3GvQnpS8WnroLrCTcCJHSvSWJcuyFowre+9M/RvXt7xIqLZ1cgqAIUbRLJdISi9kZdmMXb+vEoZRQhNDdw3Z9mWc5SAzjZsFEonHjGFxmgEAb29LzmBIzVTm3jAU6aUpG7OOX+OAv/apOGvjiJz7DndnjLh1yIBU1jqaZd71e+xpvXUcMXZcf/fubWyT2dQz7Qst9hmvbtqULRWcRdZ3AdqFYU6qqYpEy6h2HKZImgWOXufctb+Td3/M38G94g3B+BtrCrC6uSk0Q/Jrbf83d2lnNtoYGDBOu8thZCsX1b+V5E4k7RuvTNjtORNi1C6eekqsQ1lftstyxjlJGKg/7WopC51K7JSlcmUM9hctH8PDD+tRXHuOBT30aN2956pGvMpGK5vgIhyBZS5xf2xDwpSh9pLjRUyZqpqpK0pOvwvKa7nphr+xat+/PvmPPblZjcpdvtibSeE+KpY1m1ExbBZ6WlpODCT/63/4s85CZvuXrhaAl6cr5EmNLlyCDIs5vfN85RSoftn5XfZchWA3pNYybEROJxo2jVwkpQVVifxh1NNCckd4SctjA/Q/rv//f/y/2YuZAJuhigbaRGjjwNZIibU6od6jvXH95NUZvKLk39J59fg51F2eJROdKR40wqUunkhjxVRhq+6kqqV12S8mqtE45CXBcOS67xHf/8A9yz7d9i6At3P3Skt3SRkqf2z6hJOOrMJQekm4gPMsSMh7stiw2OtAdJ0DO0pirHXnGU8sA+yxL4uxQY+vHt/6R9WiInSL3rM2dsf1tnGVVXN+v1EZ8CNA23UwF19V1VCAnBn+2Ak88wclXn9CnH/kqD37yM0wa5bEHHyY0Lef9rHSfaTIz79FFy8HeHofHx4h3uCqwaBrwOhTyHh/nuFRTv6/X4ze4TQD2YsxROip5Ldc1FEdEBkjaWekVV1dED/McYTLhKDY0U8/r3vVNvP2H/x7ce6dQOVqXuySk5bU37qTjnQfNJaZ0i+XYRKLxQsJEonHjULr6ZG6I/2raOd45vA/QtBx99TGdHjV84N/9Ry498DAXFsJem5G260JRV6TUIp0LTL0rN2y0JDaoDMHz44HkhSISRYTY9bGNXQZsXzy6bVs8gel0yslijtSB45y58MqX803f/z20E0/19jcK56YwC2WlXSA+0LWjW7ZZQ6QUIocSV6ebGb9luaW+38ZWK9szcEt3O3nGR58bkbhtVtmb7WwVh1vWPRafG0Watyy3i63n9TQL76Bs0/BbizGWWoqixJSp/CjLPAk0sZgks8ClOfOPfEw/8ycfZV8cjz78JeS44YBAOjmhruvSpSe2iHP4IMv2jv0+jayIY5F9vUTitmzqfrsh99tdikQAJ777LXXu8SoM4RwZofFwSSKH5yd8/0//GOe7LOhiec+lHmtKJKErxVV+M1lLIfZtvx8TicYLCUtcMW4cAtpZEPsbZVXX5f/5HKjZv9LCUeTeGPBzxS9agriS9KBKbhucCFEghyI2YyzlPSYulBFIioBMo7vxeruxm5WEDi7npJnZbMbJYk5VVexNZ7gWjg5PkIMpT9Ew3684nimvmGbiuQkvPwgw8aWYNSUWM3dtyHor4phiHxmZpEZxc7usa+sybeugtz5xXcDsxJ0+iD7bEfaU/VoJp+vd/9vWMT5Pp6HLda7U/dy1L+w+18Mm1wXR2v4O5YRSREJAtRQkzyiEitSlDQUfyKktpYq0Kyw97dylWWGReXQGF++aceV4wWNVZn/PwSIz3ZuySJkcI9O6Zj6fE/ykRC+MLo7nIpnFqSs/cV3ts973nq7yUjgmgdQJ5F6wtrn0TplWVSmQriUT//jokNneAS6WD9eHkfOXIhwpHC1KkXinIOVc+VGLw6QliWZg7TsWzrY4G8bNglkSjRvG2BrlUl56mpsG5onP/cb79WPv+z3u0sDkuGWaFUklWaOiJHDknHFVoEHBde3VshLUlxIm3TaSAx25sm+WLitnJq5ISUwR7wabWswlBuxo0cB0QpoELuXIK9/2Rt79d78H3vKGEkNVFRGo3qEIbUoE55alVSg5QCmX+MYVObZWY+Y5CQ08xQV91cs+443v4Fqvi2cSjzn2tZ42/yp2Z+cm++tn1Bmm/0DqC5uL63SqErrYQ0QgR/CulJbR3sJIsSo2WloPHs259JG/1Pf9f7/EgfNoTEzUQRPZryryvGEqHlLc3gFox3V/7Sxji9frMbrOggjl95+kFEXoOhsiCpHMtJ6UMlmhKpI556GVYdOmrtuQcOgS86mnOTfjh37mx3Fve3MpeFl1rntRqP0QE9rv3YpwN1VovMAwkWjcMJSSGViJKwH1LfDQl5R55M9//le4/OVHcfMFvo1MqgpNEW1bJpOK3LSISBGD3pU4Ikr2pcdTEUr3kODIkpc1jwdZ2sdK3di79lmDpUhx27ngccGTUsKFwHw+p7ltj/Pvfivv+sc/JDSN8rKXCkE6M5WAl0EIDOsD0FKeBRmVmumm9/ohk0pMGf37tYLCKzvJzgHwTG13ikt252fGn3vevr6znetXqxefUbzhqSs+O/lKcYMrdNzmsF9l6uo59q+ZZe/iTCTg0JRw+LK5TLlYVOGxxyFMePzjn9D3/cKvsJ8c/vCEO6oZoUmEVH53q0W7r9fvz21cCttE6eq5WOKDrGRix6ZlEiqylOQ5FzwxZ0KoaVMkOlh4x7wS7n3HW3nnT/94ifW94/bilxPtkuZkZb/8to2bYDReAJhINK6Zq77PnTaQK2WQSVoGmosL7n/ff9K/fN/vcz46zjUZ3yR8P6R5JcZIVQVybFEVKh+QVFrBRRTnPBJL2Ze6K8MRnUdlaWV4rkTiiuVvzaKxvty24sK7RGLOmXo64ahd0DhHrj0LpyQHf/cnf4zwt94lTDuhIqCx9FmGYoFEVh2kKWZCcOS0Gp6IQk6lHFF5OwRFnt5WbJ210/lMBN+6MF3PJt6ymeeB7Qk04z27mv0cf36bK3nTXcyqCL9GkVi8xbnUWuxj5TpLck7lQWC848vNFpHYL9+3JUxDJ5je0tx3O8rFVBeBRx5TYubSX36a3/+136JOEHLGK0MimetEY51OtyZuaxm48pMdlWeSbvnevdx3GxrPH5+X8k8RiP0DmO9qVDaxpZrUNN3xtscNk8mkJI+JEoNwMq140ivNfsU/+omfYPZNbxMqICgEoSF3JXO2hCmcchPd+ry14zdiOtN4rjGReKuzzSp0yvSxixi6m19eCgqkK5o8Hti0uILLUhlyqVtIyjBP8MAX9M9++Td4/IHPczBX9iWQ502pNSa6Ibz6Fe8aXFYtCUVmXD/31ip9sHzd+Zca37u0XMkT0X5/uhZz3d73xySUeoa9r93BUEjZiyNnJUxrLmniSu1443/2nbzx+/+2MA1w27llbcNhh07vefyMjpGzB63ngnV9dLNzrfu566e3daU34CScun/D95+X7/u/PHrNmfSFL+mX7rufSav8wa//Jnvq0eMFd4SayXGkiqX+J0Bu41CfESjxximhrmT6u+CZN4thGbemINfjjPOaJb0/juVD3SkCe8fDXp8k03aF6WMQGu84rJQf/bl/BV//KmEqZKeoShfnWUJhBm3dn6vx7svytJ32UGaJL8bziYnEW51rFInrswQGkZhzqdOnAAmkv7OqgndoLxABnrrCJ373A3rhuOWjv/17nNfAdJEJ88jMFVdxNZ3Qxrgz0H1bO7Dnk3FGZW8R6buZDMW6tR+U8sqpzV18lObe5ZeZVDW5jcTU4kJgkSK+qjh2cLkS/sG/+GfwrrcK05KIomIDhHFzsW4FRlOx1re5TGyAhx7WJz73EJOThr/4jfdTLVpym9mf1HgV2vmCuqpo2hZVZbI3Yz4/IdQVTdMQ6qqUrWkjbs12u2Gp39I3pl8OTheJV4PTklzWijKfeZ6s4Yf+5T/jiQm87JvfJkX4CSJh2HhsS2dL7Vs47rCg77Lam0g0nk+eZf0I4wXPugurf8Jdnz+yEsjoT1NnpXOlNEuiK+Tri/UQEXBK0ohqLJtqgePMHQvlXHRMsyCLFm1LkLuq4qrA8cnJhhVxbCm4GZJPSqu7ZUcHr325jYyQUZdXEmZgaUXMlAEG70A8JycnEBMVHhDCdMbxxPP0RDi8vebiJMNEIDble0h67YkWhnE9WbMeDg9F3ewm67Kat0SYwnzmuHjguLgf+FqVuTz15IM9jjTTZgUfEBdICH5a03ZF3tuUCKEi4Evva/Hld7SWQd1PKw9s5TeI5CEusheS6Tqoqyyl4L1XuF1qLswhPH3CyxYCR8UFX9pTRnJMaMpLgVh+5kNOUB/qeZpA7LOjffdnAtF4rjFL4i3Oisdjx02J9fk7RGRMEUJxn3rnS6GNlHBuUEQlZun+L+iHfvk3efz+hwhNy0Shisq+r9B5C6rUkwltTkMm5vPRqeFaUVnWWwu5xFv5bv9KuY2lpdHnVVdzP7BpFrwv8VBkpa4qomYuS+awhnd/33dzNA284fu+W7h9v9QU8XRmyF6I34CDNwzYvCfI8nXseXakkj0fO1UZc/khLBQ+96B+4SOf5KN/9MdMW2WGh3nL/qSmXTS4nJjW3f8IQUqB+aqqhrqeK7skq6/9769Qfni9QHy2bTlTSlRVhWTlqJkT9vZY1I6ng3JlKvz4//RzcM9LpLT3662JwnrlgA3L4NUOy/bbN55jTCTewoxvTkPdtnXWXCGjScv1aOmt3Aeyq2rJ7suJIB7aBK3w1d/7oF6Int/+hV9ib56ZKISu/Ia2kUo8uSk3/yYnVEocEty8IrEfiPqyGv1g1AtEYCjmC53VYGT58NmhTogKCzJhb8pFabk09fzIv/ivSjHsyvXBijB2ZctNcBIMA3beOxIQNRWr3zhL2nmIkdIkXEsC29MXIQkP/uGf6Efe/wfURwtup2LSRFybmVV1ubfkjPeC5ozrupz0lvw+qMNvuV/0D2jXk16w1nVd2vrlklSWvBDrisMafvC//qfwjjcJtUAtxXOQM+oEcWFr9YGelbjItXkmEI3nAxOJtzBX/QQrq8v2y5fFu/Ztufiack5dnbGqJKY0Cg9+UT/wy7/Okw98gb155gAHxwsmLqAUC5rvy3F0reaygO+6H8DNKRJ7+kK9DnB9CRm32lVjqNc2imN0WkqP5EngMAjNrObt3/5u4vkZr/6+vyUclE4pUUvnCucr2pw6K62NEcaNZ8WAuCN+rs1FFOpQiKf83oXygJliJFRV8cEmVx4qnz7i0sc+pdN54nd+8VeYqSNfOWLmK7wIOSYqgUoFpzoK4Sgdlvo6iD3JrSaMwfUJVym/4ZJ4U4relxqLOWfanGiC57CG13/bN3OpFt7zM/9lEYsOCEJ5TN7hzZEtmtBEovE8YyLxFkdHWbcFt3mz32JN7AVjaT/VtaDqaoOJZjhcADUnv/ch/Z1/+4scRGGStVgKvWMipQBvipHJZEKMEQXquuakWSDelZIU3m8IRBhlB9/Ay3fcXmxsOYSx+NbO3VVm9uJ2cEs7z5W9QP2mr+Nv/KP3wutfXYaG0Kcsa+lrLRBHqa5Bu0KHXP9sZsO4Gsbu5CE+bsvvMWtGOo9ATJHg/KoVXEFjLPeNUEOKpYBnTp1LOvL4H/25/v6v/Efqw4Zpo+zh8SmBJnxePoyt94Ye79J6IW949vePpIm6rmnbiKbMNJTkmslkUhLP6gnHqSWSmdeBpyfKP/mf/wd4/SskuQhhArjhflB2aiS+16b3RboHz4/99o3nGBOJtzyrMTmDK3M5YZORaFyN6cmQMvPHn+KTv/NBPXdlwUP/6UPcTYUezgleEFfaaBFT6YgQAvO2KU/fqsXqEPzQOQW2lbW4vtaAZ8p4MOq7OfQsy1hoN3jJxmeTOI4dvOLbvpHX/9SPCC89D07R4NEm4ULVrVyhki4pKONx+NiC95hING4Up3oi1gSMoiUhrROLmopwVMpKpIttzqn08lMEJeNQnGpJdjts4MEvKycNH/319/HkF76Cj5Eq5xITrEtL/rb6isO+jMSie3YhiaiUY2rbSF3X5EVLXVW0belhHbV4RyocqXI87SNPTpXv/ec/xROTzBve/R5RcaslcbaJQXZMt9++8RxjvZtvZbY8vY5v+t3kkiCBFPexc/QpjeI6q6P2T/5AA9PGcf5Sy+0LOEiOHFuC90Am54ygQ1OQNpe2YRHtaqEVC5qqLovjjna3F4jX0xrwTBn3f3ajk6cCQbVkO/tSzicET2xaKlehTmic0gThcOK4VAnM6mLE9cUu6+pRdeNQvhwvLEt+hI3yvIbxvCLsiGXeIl5EZMV62FsWBVbquPQltFZKZEun/g4Cl2fKzAcu3jbhyUnm9qyEJKg4cnfP8N7TNg1VVaFawl985+loc0K867wez/7G4bOQVXHB0+SED44mp2Usda/+BKRNnFOhSoELTy3w5zwsElJTHohd+Z2rZpwLQ66fytI9v25xNIznGrMk3sr0j/EwEonL27MD6J74lwKREjvkHORY/ldXshYXkebT9+sv/p//mttbx/ksVPNIlcp2XPFFA6MED5Gtws/l7W6jcWYwLF1MN4pdLcD6Er5N1wdWUy5t7qLSVMJxLdzxhtdwtFfxnT/7U8L5GmY1Ko6oxVrYj6mDlQGW2eQbct4wbgBn/fZ2CZkdIS2b5CHshBS7GwBw0sITF/XRD36YD7//90tsYtsSolDlTKVCLZ7URuqqVFxIqbiGj+cnzGYzFosFwT27hy2vm+EmK/ekrp+8xO736h3HsSHv1zzhM6/4jnfxnp/58dJOUzPUobvx+SIQKVbY0InEMqE8gCJiOtF4zjGReKszEh9b79t9iwBKbFFGEec5SQ0zH/A5wyLzyG/9od42z/zxf/gtQtMyVYe2kQl+xSXdC7yxm6dP/OjpE0D82g6lLWLyZhGJ6y7wft9bzdR1TZqX5BPqmsd1zh3vfBPv/ol/DBMRXvZSCEqS8iU4cYMOB/qRols5y0DI/r1h3CiuIn55GxuX7djdOlog54yTbka/TGrA10vR+OTTsEh68b4HeP/P/yrTk5bbNBAWLdWolmtVVSyaEyZVPYi25yoBbln1QIdElhyLhTGEwPz4hPr8OS6TeSJkfvTn/iW89XXCBFqNBF938Z6CqJbEtf4cdG7nrLl0pXpuDsEwABOJtzxX80C/WCyYTCaDO3reLqiqCT42+ItH/NG/+UV94iN/xbnjlslxZOYrspabY4rNhpDaFk84FolDj1eWsUXrrub+sze6oPawf2vCtbeCinM0TUPlaxaqxGngyZnjH/yv/yPce6fgXcleDoGoiVoqhBVtvjQcdgJxGEstJsm40aw9NLEetAAAIABJREFUrIxDVtZxq4ueuR6glLkRh8YGCWEoHZM7d7HPWn4nEWjKA+vhn31Uw8UTfudX/gP7yRFiKa8lKRMcEDMaE5PJhCbFZ3Hwy9+9XzvoZbZ1OZLe3S4KEjMBIaWEqyc8rZFL5wPv/W/+KY/KnFd8+7tKpxYvjHuvD3Gd4qzjivG8YSLxFubM7ESBmBPiSjyhol3f5RL3kx59gg/+6/9XL37yfi5oRTWPTNQRuljDJpaah+Mi032ng9JTuXQmgU0Xci8Sh/1cE4i9ELtZSuGMg+HH+weulPkQh56f8UhzxA//b/8L3Htesi/nUupJsQ5oGRBTKQwEnYvJ0dXPZvn1WLcF42ZgfLvIK9OWbSi7X/ruZLjuwafvOtJ/ZiXmUbV4MUTIaGdtzyVGt8+GVi0/wLYF9aX81qc/p7/+f/zfnGsy1SIRcqb2gUVsEe94tna49YfDsfdDAakDTWwhl1JhvhOH0/095vN5cUdTOsc8qS0nt+9x77e+vZTKmXiiZiSEUkWi60OtmqnEwkyM5wdLXDF23rwVOoFIl2wikH0XE5Twc9E7j5RJrklHc6ahxovSNA3ZC9PptNQQW8PpclP/P3tvHi9JVtX7ftfeOyIyzzk19AQtCrRC04BMInBFBGxE/TAKfUEEvOIFvQ/EGeV6ffqujyc+9HlReY58nggiXqGBK4OMgkgzNE2DDA3IIILQ3TTdNZ46mRkRe+/1/tgRkZF58lS3VHXXqar4fT5ZdU5mnszIIdb+7bV+67c6TV9cnWHs6xD7g+/7ZrmnkiguH3P/2KM0ZrsamVk4VE/ZPGcERRRyi1iLqKFu59EixEZ/1U6SWM4UbDM/H5jigF2G/ldy4ft7nHzEqoY5Q/IgFJGu0QVS2TUl1ds0noMYUGtABLUWowakZjKCQ3ssfgv2GCiCbTJ4Lmn7TkCu0q+KtBrq/kuNAr4ssdamJhqfmmiwhmOTLVyeEX1ANDJSwzcXYw5s1uw7UMJUIHjcyDW7QyWaiEjTdBNikq/0JrcMGHBrYMgknuVYKF329Ymt7oVI8BVjl8PUQxAmH71G12rhDf/fK9gzrShmNWsup5rOwJrUPWgl6YmiLATPIO0EFYMhdnrFNqi2WC5R902oXYSsZ059qkhi30Tbhe3ZUhUIdYS1gsMZHFgz/OgLf1W48LyU+bCGKtRkjdWNxtg0j6dac+rw7olFDfhmGXVDl+OAXYl4/L2Lzi2b2owjrCKS6X6pfpGyh4IgiWOhAcQ0fXOSfoY0C92ijUQjJvuoWcW1V3xI9wfhrX/5avYGy6j0FConpGlO57/BKGQhxYFIamKJTVzKJJWXhcZ021lcnjEtZ+R5DmgirRh8ULyC3bPBIa15zC/+FH4kuHt8mzBKpvq2ySrafsPacP4PuBUxkMTTHDv1LyyQvz5kRQJquTGCVk+TQrRLYxOghPIDH9E3/8Vfs+6FvKwZRcWGgPjIaDRiVlcYZ6lDjYhg1Szo9lofRtPp9uYkEeaEr08S2ywiLDa1JK/BU0sS+7OZ7ZJ+sraGygmHrfKE//Y8WDNwtztLtKk5pX2MCKARK42BsAjdp9S+8Ca1ophuBGCHYZEYcKqwLb7soEjsgo7pfk3okcSFOLR4fnhNIz4NiRia5o9UkrGCaza1cwv6NMkpyVlMmhXtBa77ul73vqt431veyYYKuVfykDaeLszj0Q4Hv1A5WCaJVucz2+vmQDJVqAOFddRNtjOm7rSUVWxesEqSpYxsQeUD3lomTtlcczzqWU9j7T/cXxhJqv214aElijuc/6tCxEKYHyoRA24BBpJ4GqMvEre9s7+9vp1CsND0IL3baLsHzYIoGoG6LsmyrPE+DHBoyide9iq97iOfZK1OwS/Zl+nKnfhOZtfL19+SMXtLPtQ9vd+JE8SdjqfFqikN/eOwpGAvjZUPkuw2bOY4JIHD54x48s8/G+5+12RzkTXloR7rXYjV/Si+guC3dxmC/IDTCrfku3qc7/vSVQuP1/64+iliY8rfBL4IlDUcm8CxiV75ujdzwzWfozg0Za8ashgxGlM2MAYcgnOGyqcts82S72kmLvkuStr2ZmEufWmN9dvNXF+ruEpfDUtxSBP5Cyb5Rt6kNQ98wqO43aMfLpw7RovUhAMuVSQkOZS11ef0PKGj38KcPIeGWA6bzAG3FANJPI2xkiRCRxShR9LaCCqrg673fm7VED3OuhR5rr0RNOPKP3u5HvzMF9gfDXmtRF9jjEFPcwH1iZJEE9PEBY8iNpWMZJRzoJxSnbeHx//SszF3v4vgTJqmIpKqycxLZAMGDLh10a5zraF3jBFTK9TC5vuu1ne88m8YTz0bAbLapy7omLqQ67rEa2S8tkYdUsbfVx5rbde93O9uXuXi0I8vfTcEG1tCGLfd3ypIVOI444AEDqxbnvpbvy6sWTh3PxCTFVAvpkeNyRsyT+0GCxNaGOQqA/79GEjimYIdsnn9ILE8B3S59KAxgm1+r5Sb/umT+uY/fBnnzCJrs8g+lzObHGOc5WQx7VB9CLumw/gbwb+XJC5nRk2MiRy2v2M5bDyP/D9/GT8Cd7eLBNumFVqTyNbnLJXJTuO3b8CA0xcRqENK/c0qjrz/Kt0z8bzjr9KseedTE5lqJCtyqlkJIVl7qUma4TZehCV3hpYMrpLKtLcZBdtmDSXibTqUpLtOGUoVgyciWc6EwCQz3JgHnvzfn8f63S4SgqTpS5KcKJyxKbBXHpzb5rXvm2DjYKhEDLhFGLqbT3ccpzzZr2pCGuvWzyxKIwxvuwSl/QMfoITzK8Pta8Noq2SvLSg3J+TjHB9i0iiqYuzZOx7OKLg8Y1ZVSOYIYqgEjhWOMILDG07Ok5iE5jZPfxSSHklZnZ0cMGDAyUMrBemPBFRN0hpRkg2hBazhwIalInJkTw6VMKoD+IBEpa4qcucwolhrmTWa676FTls+1iYWG6WbaNpu2KWV++h2fiYrrgsascagpWcd2BMtuVjWS5MsfmyjY7aCMzbdH5M6vluNZ/ugw4Z0wDeAIZN4OmMHDU//+laD2OeS/XmrXgPWNFNR6pgE3rWg77ta3/TSl7O/Nozi3J+sbiwp1twI9YHAdoubMxnLmUYfamyRM9HAxMBsveAJv/BTcN+7CqO0w0+dmHOReWewyzBWa8CA2wrLJeeEmAijCMxmQA5V5POXv1E/8vfvZexhj3HYaU0WArZOYzZrbeQ2ulqT3TfUV1jIEi5oGDHNZjEuVGRse6zWUIeAUcFkjqrybNnIbO+Yx/z8s+F+d0lxJnhwGUEEQTHRpu5ua+bNcQ06rXp3sCf+3g44czGQxNMZO5HE/m3NDrZPEvveZQr4akYmOdx0mA/+zev1nAq+9KGPslEreRXTBBTnKKPHOIuIMDs2Y5wXxJ26Gc9QLJNErFBrIOYFmyPLtzzsgXz7M35YGFmiBIy4tJBY02UVoLW7GUSJAwacSigxnb8iZJikLaxDYnU3HoSZ10+84c18/qqPsR6UcaWpaU8EawVtfGBbHWGbTWzjROt44M18qlSfJAYxDTmMS9nF9AB18Lg8R7XJihqHHeUcm03ZXHM85vnP4di6Y+Pb7iwUrumeBqkCxmWdtKVFF/sHkjjgFmIgiWcalhpV+t3MMN/ZttMNLBFbAzcc5TN/9Tr94vs/zF41UFWMs6zrXq6qChUoxmvUdY2TVhh9emcSl7ukV3VS9++3DVaY1iXkI27MlR96wfMw97goFXasXWglD0HTwqKKiDYf0UAUBwy4tbCq3AzzkrMxiYiJzVMzWYwY78HkEELjsRPQz/yLvur3/5jzo8VtzciqyEjmc+n7cUJYNPnvdzT379v8RbqPpOlLWUy3e6upAiRgjKGua3wMZBvrlJMp6zbHG8PhLHJ0PeNhT72MCy/9btGxIRiD08b2x5nVjYwDMRxwCzGQxNMc2zaExyGJ/duTCXRMtmbXHeDVz/9NvWDTs78GGwLBQa3zEXw0Iu6qCbqq0pRbzhySuFMHc/9+fURpRunlGROUez/ukZz39Mel7sM++dPeAgGEWGOsRVEMZ6+mc8CA2xpdablD49OqyT9mbkvaaBajNo0gEWrl6Lvfr3sr5V2v/l/ks5oiSrL67mUQW5IIi76pC8fREcbtJBGgNtp1SBtj0BBRZ6hiJMMwioIvK0KRs1nATWPDg374cXzbYx8hwUnyXNWev9by83evc8CA42NIY5zGaC1wVhZ8u3py6pILsRn0JhFiwIaI1EAZ4Vil59WWjRKcT6XRLfWUmeAzxePBCEEjagVvQI10o+PONCyTRWNMssxoNEiq2pHKRJyVmTOUmQUjoEoQqKSZvNIw9HZtStYZoMgZ+g4OGLA7sZxRTETKIM30FUhVFo8QRJIPoTEwMrAG1++BG/bAwZFSFi71oakhqhCdoxah1hQfRdK0KWsMmUnjALsYyryBDeab0dqky4Jpd2jscUIkVxCN1BrR3GGdILOa82th/9EK6p4no8S5vCWkVaLWmORHMQ4EccAtwpBJPI3ReWOxyPZl+Q4wF0eXFdY6KAW+/BVlUvG/fv+POe9QzUYZsJLsHMqRIaCY4JuRc+kZvJGmO8803XintyZxoSy0oiRkSME+1j75IWoiizZzlMETneOIRDbXM57yR/9DcAE2Cqomg5uxWOpPO/vYFenNQn/kgAEDblMsVV6C0Ds3G8IVQa1i0OQdWytcf0Q/8NK/YPML15HVHhOUWJXsKcZQB2JVk2UWVaWuU5OLyVyjTVZ8TM9iNFUS2jga2/62dlgBTSJgyWJHmkBV1zXF+pg6KJtrhrs99hHc+bLHCBmdNQ5mXq1QSV6KXXwbdNEDbgYDSTyD0GoNoeeLWIeU3WrNjkKEWSR8+NP6+j95GWtVYL84bOnJm5DkYyDaZLBdNFNCRWWhbNKawKrE09on8eZIYldyArCGoDHZTGQZm8GzNXY8+sefzvWm5E6PfJiQNTOZm8fPYdGGoiGJvSO4FV/dgAEDdsTS0tfvAu439wGdjhivqZMYC0H5yrvep/uD4e9feTnn4sgmdTrnJVVerGsmMgXFIoTGHcK5NKavJX+meeL+71aTSXc75q9NBvRvs9bivacOkbhecNBGLvrO+1LvKbjPf36aMBbILBqSk6vYrHkCGWwSB9wiDCTxdEdPXLKSJEZAm7JDVYFaDr73Kn3PKy5n41jFPrHEaYW1yWMrEMjzHA0eg2BagrRUg+2bwJ6pJLEzxY0x6TJtMratvMcUGVuF5cY1x9P+xwuFvXkSiRtDGTzGunkmYlU0lv5yNGDAgFOCpfi5zaewaTZLwbXZcAN4n873AFTAV27S17/gtxlt1WxgIHokKKKBTAzWa/I7bBtmrG1mOafHOx5JVNlegm4bXCTEVNp2GZUotQWfZxyKJRfc6xIe/JwfE87f2ywIJhk3igHbkNhh7NOAm8FAEk93LAW5bTvhEBILmlXc8MGrdXS04h9e+7dslMo4KDYEtPaMRiPq4NOoZl8zyos0qo+5VqePkzU7+VRjmSS26DKJUXHWElWZViW2yDDjglldcVOmPPEFz0cu+VZRGwnGQuN9aABCnPsj7mQ5cZq/fwMGnK7ox8turGmv0aO9fVvTn/fNDOeSTE0ijgGoI1992z/oFa9/E+uTmnPIyX0gD6B12nS7xkqs1jTffaf1t2+j04aI5WYYIVV0jLNsmcjEVxRFQeVrMuvYNMp597mYBz3rx4RCYM8ajHKQSFTBuKFpbsDNYyCJpzuWSCL0Ah80GprIF974Dv3A6/6OC6JlXAWcV7SuWC8KZrMZxliMSeXUNIpKu666PkncySLmdMXyLNUWbZC2xuC9T2TPGmpRpqHGjQu+9Xu/i2/5L0+VkAtqLZV6cnGLIxBbz6GdMJDEAQNOCdrKi9AjiZE5SRSoYyAzNp2mUQkhJIKIYpD0d6FO+r9ZmQwR/+16Zcvzpt/6XTbKwLo4CjWEKk1pUWuoNLlF0FRqdhr72XZNW10kia3nongwzjKRgGSG4D2Fy8hCkp0fMp56/zo3UvIT//cLUlYxT+4LynKn94AB2zHkms8wtNNVlOYfr1BBcXjGBd6xMY2Mpml3O3IZ1axkVGQUWUZdlmRisV7JosXUzU5VDY0aBpiXZXUFuTrdcHOZ0Lqucc4hIlS+RgVGa2MmVYlK2xmZCHkhLs1yboO5ptkqoXmfgqTZqW2An39QAwYMuK0hpPPWtL8Iqc4r6SJEciMEXyUyJ4J1jllZowieJv65LGUEiwxGlmrNCllgc49jtrfgUCyZGSVmlkoUGheEEOb2Ya28ZQ6DNpcoBi+G2qRLEDOPv5mlioFx5jCVZ91k6LRE65qs8pwTDdlNR7kw5HCsTrYMIRJimNstDBhwHAyZxNMVqz62LpvYEJUamEWu+qM/1wMf+ywbk5q88rgINjNUwWOtoaoqsixLu8qgSbeipttlqvRLsnFbWfZ0zyb20d/RG03df20wj5lhZoRNEwhrOY//xZ+Ge18sFIbQjPdy0ozDMnMT2zaz208uLmgVh1g9YMApxVyPuOjW0Pkqqkm2MdZ0GUgl9QNKd7+m+9l7UAs1fP5vXq+f/scP4I5M2WdypA4phxcCRZYTfViQvMyzhWkSS2jSODYuxlltRYxqkqVWqIk+YK3BihB9IMsyqhjxTtgycGhkuOxXfgZ7z7sImaQspbhFs+1t78fSL8t3HHDGYyCJpxirTs7Vd2yDVfo5/dGy2ropnvgAh49BKXz8Za/Sm675AnLkGBs2Bw0Y0tSPfnDqY9k4eicjWDj9CWJr5dM2kgTRrsTT7tZjVEbimGjk2FrGxQ9/EIfXHfd/+pOFPJWhF7CkgN+Bz69Wyg8YMGDXo4vbS1rjeTyPKRyXntk1n9PLX/JS9k4D50SHLSsyDGiAqFhrqINHVRnlI9QHQlBMnjEldGP8Uqdz7KoSLdpZzO2x9ONzYRyTcoYtMo4VlhuKwFN+/tmY+95NMAI2A2upiMmOS5thCSi0jS3aPkvvhZshfJ0tGEjiKcaqE225AUVb93+RHVlkVJ8a7zxwwwHe/bJX6b5ZZOtfrkWPTFhzFispaygiqXN5B5J4NqEliYbYaH0WSWK0glEhi5apg6+O4Idf+L9TOk9xx28SnBsi5YABZytWkMT5yFOwGmEW4FgJE89XL3+zfuZ9V1LUSuYDTiDGkIz5M4fWEUKkKMZslTMoHKJQ+LQWBIkEs92ux/QSoAsyIB/JjCWGQGlhtu44VChP+qlnUZ6zh+LiuwqZpdKAabSXiSImq//OxVUXSaKapaaeAWcsBpK4W7Ai2LQnYUcS+6ej9P8sphFvQak//xX9yxe+mAtLodickQUlE3B5DjGidcAY04zWGz57ME3mtDGzlUUheV17xsUIrSJHcuXeT38CFzzuUqFIljhtWXnAgAFnIZZD6FJxJzX+NcXpIDBVjrztH/Tq17+NtUlFpiluRwFMKhM75wghEFGiSc0xRbI5pLLJJme5gmPnw1UIku4TgUya5sMQsc4xjTU6ztlyyo0u8qRf+yWKi+8sZAY10pBDaf6V7aGteXFtud0yhL8zHe7m7zLgVscKrtbZqMD2mRxCGutEmhuKr7CNBibzGbevDWtHp+x3I7AxjYOqU5Sx1iJRCU1ZYUAKqju9ExvFOGVfXcbURqZrFgqbDHWz/DY9zgEDBuxy6JLDlW2aT4iolri1jM1zxlznPBtZZCNCrsoYi1UlxkiMkbquyUcFEuNKSc9yablvk9MqkoxAHTy5y3DGEqqavXnB5FjJBaMCrFIcLdMEGSNIDIhLzhaxIZXbOPBCcmLA2YAhk3iqcXOixOXrNZVDq+ZstSQ7G750o3Jwwtt+5/9lNKnIYqQQi8SAEelmDyOC90n/4pw7Y+cv31K0pZn5hJU2k5j8IV0QyuCp1woO7s/4oT99kbBuQVJDi20nGAwYMOCswzZt4nJFSKCqK/LMzXWKPqA3HUI04z0v/Qvd/Oy/sX8zMPYR5xzee4xprLk0VThai54g0hHDvvbQhSax0N0vNb2IpC7qVl7kEKwkq5zZKOO6Ai77lZ+Be99VyNsHVbCOOnqcdZ1jhiwcx5BJPFswkMRTjR5J3NZRBo1Dfq9hRUCNUAGWiKsDfO0wr3jef9dvoaA4WjI2qQOvqiocSuZcMnYWQY0QVVNpYfjsV0xZWSSJBQ6fZ9yUBe7/1Mdz/uO/V3yuSHKhbIYWDhgw4GzEwh5fl66QxQlYMdSIj0huiTFgMDCF6sqP6Tv+9K/YV0aoPGvOIcFTVxUmXy72ybyhjl61KSaC2CdsCqgzaSygTWVnp6l8bRFKYGst44bM86Rf/mnk7t8qOAOZI6JIY7a9iiT2n38giWc2hhVul6DVILZaj4UMYo8gtvl+Q8BVAa49xN/8ygv0/JlSbJWMoiI+4uua8VoBmSUaIaB4jR1BlGYO8QBtiOGiJ4003pCzqByoJtyUK+f/wPcK0SNYYiPrHmj2gAFnLzoHqx20iV513hBsMiQrAIPajGiBsSX/rvvIY5/3v3HtWJmsOzZ9iYiwPhoDixnDaBrNYc+3tm2yC2ae4eueM6QStvcenKU0ypaJbGqN5AaZzrhQM976ey+Fa48oBzdTybzZLLeyJ9tc5i8vNlFwwJmOIZN4qtHbeYber93UjpW+VBHqAFsV73vRH2r1ha+SV5GRGkwVEFUkM8zqCuNs0h5GxUnaVXqNmNx1ZYjT3cbmxKBLhuDt+5F8ykojbG7kPOZXfwbufidBFM0tAUPQQC52CJQDBpzt6JebG+bYdjqnMrBC1G5Mp0okoFgFCcBM4SvXK8cq3vjbv8e508ioDqni0wvQKq2bY2q4M81Gf7ki0jayxBgxeUYVfdJeiyTZkU/2O+2ULe8chwrhpj2OZ77oN9JkFmcX15+FUvowe/5swUASdwN6J6JnnsZvx+qJMURNOkJrLOHwEWwtfPiVr9XDV3yUvVVcmEFqm+Hw7YzP0DuPu12nzO9/NpNEg2KtpQ4RH9OoLA0RYxxTgXs/4qFcn3vu+ZNPFQrSm2dts5vXNNv6VL+IAQMG3OZY4Ew7acl791soz8berUYTm6wqEAs3HuHjf/DneuxzX8ZVAadC3YgNbbPRdyYj1p5MUpz3PVuc1hKnKw1DZ5vToitPG4hBwRqOETm2kbPnXnfl4c/9MeH8fY3XTTMcAHqDAiIRxTDMfz7TMWwDTjWWgku/qzkCWEOIaeanNQJljQ0GZrBvy7NRJXsEF3t/J4v+h2czCTweTEOQQ+0RETKbxu9ZlxNVqHPDkRFMzxmB+MbyJgVFA8ng/JS+ggEDBpxKxOayYyBoVEKtrq9fLUpZR6GuA9EqOs5hJLA359DenJsK2Bo5NsUjmUuNhrXHxlRGzo3tJkOtivEL8kidX1oCKZCGKljBlxXnFCM2JjV7jlXgLcw8lDVgUN948BiDr+tOHjUsLWc+hkziqcRyJxyLBqlJo+gRSA79lYeZ8tGX/ZXuPRb40tUfY28ZycN86PvyPOVVY/N2GiZ/tsEoWNN0e1ub3peY9JsUBTcV8Lhf/zkOFIHzLr6LqGijVSRpQweGOGDAWYuW9EGvy3dVTF0R2xfu2+jEA5Ey1qxjYBK5+q9fq3tmgc9ecSV7J5G1YLBNh7KG9GjSmP/XJukVW7SZwhZtJrFdD7LmYGqjzZoxN83ecsL++13M/X/hp4SxAZemw6gTxJiG6CYXxaFx5czHQBJPJXpv/bZusa6LLA2apwzgDR/+07/Qf/3A1eyvDfuwmCpsKyu0ZFF053Jy/2/O1qkrqRtQkxW5tAbjgreWrdxwx4d+J5c852lCHsG6NJ+1X15pzcgGDBhw1qFfRoYV5K/nKdhvEVy4XwhgkpF1yszFRDjrmG6vlBve+i698n++kXNqWI+WOKuwtvVViAsJgha294RRFgmkaKo8KUBhKX2NaWbOa61o4ZiMLd98/3txsBAe8uxnCplCZvHBI84RNekb7SC3OeMxlJt3Cfo2A/1WZwnAVg2V4ZMv+2v96gc+ygXesBGAynfmqa0vFsxneRoa8tnsMtsg0ukWz/Lm5pj6eVBJnYIiFvHJNuJQFrnk0ZeCeIJ1yUai/bsQ5tZEAwYMOCvRlnGtzis/3Q003c1A1fwfmMfmeRJAkndtWaUsoRokCjhHJMCa4faPf6T8wLOfweGNjCPUeCdUNlDnKYMYlsLQqspRpFkjmkttU0V5EjzGOrT2jFxOMc7R4MnLmuuu/iQHr/wkX/zrv9WkmSxx1qVOZ1k5j2XAGYhh4sqpRK9jbCHI0FyvEerIJ974dj2vgn/5xys5LxjyyuN6c1jiihLzspy4bxqtsNTRexYjs6ncExRCxCjMfM29vv8hcMfbC4UjAM7I/LMyZsggDhhwtmOJjC1kXGT7j6tKs2LTX2X5fHqThoA4h+QFFWBMyfih3ymPP/9cfc1v/z7nqmVUekLpycR0zYg7QZqCR1w63gjk1lHNSkbGUU1nRIHxqKCKASlrnBg+8+73Yc7boxc9+lJJTDfZ6ljnBpp4FmAoN59idJWJbmfZdr9FqOAzr3uTfuTNf8/5M2WtCmS1Z+QcQRXvPda6lWWGvs3iMonsP9/ZjM6UNkYcDqOQ4bhpDR7xO/8VLrqdkFk8isEkWt77nLxGMhmS8QMGDGC1XdkOGsX5TRFFCUHJrENDU6DwEZxBDSiKiT6lIg8ehS2vb/+5X+O8mCG+nTm/Osb3dYmrZEVKktkYaSzSgk+WOCi4JLHZcsKNI7j0x57ChY98mJABxiR51MASz3gMmcSTha5WvK0f5bh/0nfN7+6v7fimyP5S2D9V9qvB+JqsGf4OkSLL8L3tYTdajnlwiJJ+jsyDyHLW8XjoP0533Lu8c7o/Zm/V8S/ctzWNNQZv4IivODwe403EudasVhq9oqLt5BorGDGLU3IGnHIc79xbtYYPGHAiWN7kH/9Oi1eOZNbAAAAgAElEQVRFkiZaEJxNtR+xEL1inIFWN2ggGAvUxJEhMyM5vGekHKtZE0Me2uffWT8kzHXo/UNSVaxNHcu1CNJM6ArRg0ZMgDUxXBAzzp0Cs4bFuvSg2myStzXtCKtj43ASnnYYMoknihX6j9D73S3dZ3kguzb/zL1JI1Q1eMO1b3y7fuy1b2PfLJLFZJyaOmwbHYymWZ7LBMgsHdPy7X0S1T9rl8lff8xTPyOpvYggemrNuJc7tdvjM9EsWEOoxDlh7u26DWmudRTDsbHjno9/JDeMlO/4j48Vckc3vsAsBr0+wR/i3SlEb9FZ2SDQ+17EFdcPH96AWxXLsXGJKN6Sr5/3HuccECE0AaxSPvhnL9frPvgx9lfKuheM97hmDJ+PHoMk7aD2nlhbM+82gRAXDquN822ji8NQVRWMCg5b5VG/8rOEImLvdZdUZZGUZ+oaJOsAzhJtoN/m173WVjhvb/nrH3BqMZDEE8UOIuH2BLD9+wg7b6QUCBG8Tw/y6S/p6170+1wwU0Y1CLEhQNp1p+1EEv896Gfats8x3l6uWM5GmlPc/NI148TF4CbRLHR270QSbVDUGiZO+HoBT3rZHwhrAtYRiFiddwMtB7UhyO0CLGUvWh86YamLdIlE2qW/GzBgN2AnTtlWMBCB2oN1MK249p1X6NrBCR968zsZV4EsRvA1a6MxVVViZTFgi5qFYQpC7JIB/URAkMaUu444azHiKHPhBkqOblh++HnPIbvH3YQiJxhBvGKMtOnRbSSxa9qJTSAdSOJpg4Eknih2PKsXf9+2JmnvfiZ1wNlQw9cPYY/W+pr/9kLOr4VxUFyMHYHrO+q3hOdEsBNJXB66tMpvcTfBNg053vYyfDrXaLavb/k1OLXMrLK5kfPQpz2B8Q88RHCR6DLEmHknorDQaNRhiHKnHr3PpD9JCNh2Pm67vX+fAQNOMZbDSwxpChQK2josGDNvbKxqiIZj/3iVvuPl/5Px5ozbFWvMjh5jrRhRE5qYLl08TBNaElm0Ghdi+/I6Y30y7Y5emUWP7h1zxHgmY8tlz/85uNu3CqOGCqoSvcdkGVGU5VED3Vpy67x1A24lDKr7kwFhkUQsXXYkiED0NQCGiBV4+6teq5/+27dxgTr2RYOLsSubtrY2QDfQ/UQRxRBlcVg8zJ+nnSjQJ5BGwUaDbUq6pxrLx9AvLfavW34NRlMpJ1jhqHjGD/kPgkZw2fwP25QULJJFetcN2DWwrOjs7/08/6Y3GFasAbsY1tpGg5500xqbyKaRECOMM8gjG9/zALnsF56L27+XY7OSPB8RQkB7laa2xAzbp7QsD2Fo4YqcWV1hjGG9GBGObLE/ODaOVFz+wt+D6w9ouOEA+BqMYPIM7Z1x7TIIvbXkZL5BA251DI0rJ4qlTGFHAlfk0gW6dHxX1s0caMBFgUq4o1tn4+iUg5Maq9JZ8ZnmMZczfnDiGb65PDHN5GxLyK3PIsyfU9Q05e5k2uNP8ejOtlSSSu/puj6Zhvl71nbjSe9vsqLgaKiZ5DmMHeTSkX1Fie1UgeaxuwkLw4D73YN+hnfFuSDdP0vnykAQB+xyKGAaomitRZwDTTTMZo6gHusMrAnTLHK4KrnA2K7ZLj2ILGQKAVycb6NXVaPasF5VFcZaIgFfluwZrRHKwJ4AWWZhBtZGEEE1ICLUoca4gjrUjGzacC9PphkkwacPhnLzSUL3pe9vnZqsk9JbnBryGJrrHRFmFUTHF177Zr3mje/k/GDJtipGYglpSNzCyRQa7V1k54kqt/y429xKbIjTaiHzckOIjYlQhkbrd6qw3FzTlptbw3AhvV++N5aqvQ0MFXB0b8F9/9PjufAHvkewgloQ4xY1bM3jz4Nda649kMRdgxXnHizyxqFhZcBuxrZys8bkohBj6kQ28125Dw1BVEVUoIzwiS/qG178x6xXgXFQJESszjf+0bCgaY+y+Jz9TCNAQMmMRVUJ3pO5ghACBqgKy425cmRvwdN/59eFczbACSqGGoNhnoXqk0S0lyQ5Se/bgFsPwwp3olilUetdOoK3pG1LJ02EGEAtXPMves0b38memSLTCuscM/GJEC6UeefE8GR8eOlw4nwKi7ad04pRRVCsaiJcTem8sjDNIlN3ckreJwpl+8QZF3uvo/chtYcbDJQOJuOMwyZw4cMfIjgBm+aTQgrQ/edoEYnHMZsYcCqgsNg5KvMmFkg/b5Mg3BYHNmDACSDZbCliDMZaFCXEAALOOQST5skHhczAfS+SR/zXn+SRv/xsbhhBZQymaeJbbOTTRjeYEGVul9ZuvAXIjKUKPsXW3DHTiuCUIBHxkX0eNo7OuPrlr1ZKoAqdtU96IiD2Jnw1V0tXfB6w27ELlvgzA/1sVqd/O879Lc0uqwb+7Xp93f/zEvZVygaGkXGUfoZa043TazN6sKgnOanNJLJ40i5nCPvO/suzQk8lVnVmL2tu+kEyCMwcHC3gYKFc9txnQUb6UIx03olGTJdnnfP+NFt1OHF2HzqiKIP2acCZg+RemL7NydfQEnvlYoyAs+n/kWPP/S4RvuMSueyXnsuRkWErM9TGLIzva3XmLTnsGlZ69zFNs0xmLEEj0Ui6oIizoIEiRPZXytc+/EmOvfv9ipqUuVxuW+ln+bvXNeB0wFBuPlH0asGhdxXN1RZIb7HONSJAO3KPYzWv+bnn6/rhkvOlwNYR9TWmyJj5Oo2AY05wsiY2BFmtJflGICIEjRjTTBWJ6ZWopmOuvMdmLulNak+e50QfFl/PKcbyAPs8pCAlxjIpZ4xGI2rvEZcxtcrRNcN3P+ExbK5l3On7Hi6sWbx6rEvjsYT56wd6H2pc/B0zRLtdgP45FzR2WXwl0lF9jVhJnaE+zM3SBwzYbbilq/KqXJwlJju1SuETn9fXvuRPWdus2asGGwImKlaSnyJG5rZtPTlOW46Gtht6seu5baRU1ZS1t5YDY8PjXvDLcNdvlmAVtRmmtcZp10mTZswYNHVrixvi5y7HQBJPFM2Xv9Wv9RcrA6hXjBOqGJKeRFNplzJyzZveprebBK64/A1cmK2TzQLadJLZPKOKPg2AZ9HSpS/8PWELHGOofI3LM7z3qEaKLCfUHmMMTixek41CXdcUxYjoQ8rUNVNITqUmEVY38ljVzkTWGIP4iB3lHJ5O2Vqz3O1RD+Vbf/iHhHEGRUY74FQx1L4mbzuct50ePZIoMJDEUwvV1FTU148mHRdoDDhj0RiJKslKpAfvB6I4YHfiG1mV+4TREtNovxr8Jz+rrjS85Q/+mL1lpAixI3gw17d33rEKLsxtoqLMdfAtWgs2MWkggfeB6XrBdfssT3nhrwnnjGF9nJohewujOqiCpzBtbWaIn7sdQ4S8lWBpeIcVvPdgLIGUhscHqGHvZs3GsYoLR+vE2Qwksr62hrOWelaSu2xe7myFxCbt6k4WMfPRpySLCGItKoYyBDAG1bSQqk/jmYqsoCxLxBq8BurodwVBhPnOtr0uiBBEqDUSQkDEsjWZYdbXmI4zbnQR9hYwtngJqEoKaKq4RhyuYdU+vflE+trTAacM7duf1qFEFUWTsN4ZCzVITWMqHNN0nWbFGgjigN2KWxJahFTYaC9WexZPapKfolUOrVvqPYaDhVIaIXjF+0hmMmw0uGAw2mgbaaxqVpSeTexdWgIZI2WT2DCVZ/1IBZVTjszApwMMJoIDb5Je0VmHYohBh/h5GmDIJJ4oVmQShWYX1mtUqYgoNUWwMItw1af17176ckabM8YKhXVUkxm5CkVRUMWA74nq2h/b3dzJmnQSSWXmsq4xzuHygjJ4orHMQk2W50RtRjxFMDHgImRiwPtuduepwvJYvnZX3BLrGCNFUVDOamKRs5kpG/e6Cw/51edKcAFvLVYsTg3EJmj1p9bvdHoMFji7A3FRS2WIaEzfaapAvO4As+lU1+56Z0EiuKZrPaQy2C5STAwYsA3HW50XGiKVTjyt9Lv4YwqI0xL/z1/SN73oTzi3VEZ1GtLQZgvrNvHQxLUuU9g7jmXSCBA1dA00JYEyc9zpO+/D18bwXT/1TGEEahN19U0joBEzn0Q2nH+7HgNJPFEskcSFUWAKqhGxhlkoGZnkK8UXvqpv+M3fZ21SMVLINGUXc7GYmD6PmQZs5pIuTud2BSfL+qaFJZXhvI/gLJu+xq9lbOaG8R3O53se9f3YzDFe38Nf/95L2F/DaFazUQu5yknVRn4jaHU0nWVDz+oGIHMFR8sJYW3EllMOOuUZL/oN4Zv2okXWfGYmxUYfILOp+7w36QBY7JxlaQ7wbfFCB6yG9myaUBza7NYUvPDZ171VJ8e2+I7/9CQhB1r9bVN6Hj67AacrUhyKTQwyC7GqjVF19ORGMFGgilRXf0rf+tJXcO6hmnEZ09rDojuEkiax9NEfZdqPt9poGus6mWlLljOxwnRsechTn8g53/9giQVA6s4OSY2IRYZz7zTBQBJPFD2SCNvtbhRFRFOjilc4Fnn1z/yynrcZWa8jqoHMWjQkUb3WPnWvqRKWm13ozSbWxf+/UUhUvCilNVS548JL7spB8Tz8Ry6Di+4oFBZCCTaDWWD2ng/qu1/5GvZNPGM123aYtzX6YmuYd1135XmEqYNjewse97yfhULgrncUMgMWfFSIijNu225c0dTIs/ycDCRx16A5zzyKaLJrQoUj117PvmD1DS9+KUaEx/3XnxHGBvbvJRpNHaO6mDQeMOB0QksSFbBtpFpBFNPmSZpmlkj42D/r3774zzi3VDaqSBYXNYnJCmfRbHvZyaKNrz4GjDEURcG0nJFnI46WU8xoxMExPPG3fgXudPsuix8R2pnOMaaK+IDdjWHiyslAI54H5uyhO+GUWNdkYvnEm9+h+w/XjCaekTfkmOTFFxUfA56AMYKxBhsEo5rIYu8kNfEk1ZnbQzeCLyz3ecz3cc6lD4Y73C4RKB8hc2nc0nicXphRRo94sGT/8G4dHakIB46mwfO7YJ/Rn2PddoGnzKtQZfDQJz0W7vJNwnqGxoDgoA44Z+fjBSQZ1OLafa4s+lzO74btNewNOHXo5B00EyZigFnFv37q07p+zLOmEMqKw5/6nN7gai556HeJGosOC9SA0xytewZAaBYe28pf4vz2EAK4povYRux9L5Yn/sbPq7npGO968Z+xp5zHsdYnsUWfIPY33y2yLDU8ltMZuXMEXzG2BkJkfQZv/eM/51HP/QnlvD3C3uSQ0fHYXbBuDLh5DGHyZKOvOJb0BmfGQAnrh0v2TTx7PRRAqMrOZsVam7Qd1lAHT9XMdO5GyvV1VzucXO0JvtPtbRewNzBzyT9rc81xfRa4fqywvxDWDdNcYc2lCDPK8KEGaxpzR+GYBbOxzpavTtrbttOx7jRTtIXs8HrbjrzSwSQzTMYZrGVgBc1sM71+3u0aQzKotc51vmT9GaQrn/sbfG0DetD5f7p8/c0sIsq8szn5TDUzL61jNPGszyJFGVlXy3ga2OMN7TJqTMpk7PS438jxDBhwm2LVd3JFULI25YJi1LTxH1uuZUZ1zphDI8NWDrWdyzZW+eP2/4d5XI6N/jczFl9WycdRDNSBtQh7Nj1k68LUw6xCNOUSQ1CkbzZwnHNLV1yG8/G2w5BJPEGoesQkzykxhiCGSCQjzTiWqFAJ4f0f00+94Z2cGwzjyqMIrsjxMaQTrfnmp/NTwBlqdJ5B7BbTRVNUYyHUHouQZRlVVTVaOu30IgBEIaoSrTCxyiQT7nDPi9nMhCc/40eEC88DIgGDA2Ljb6WAy7MmgqSD2HfeudhDJdblyevqBE7W7btTs2Ps6/t2tRmkpEGLjTia1HhsDbNpTRxnHCmEe176YO7wsAdLO4zaSOpOjr1yo3FzjZrpfurF3L6Ce2CHJwc6/3/eeLL09vbkHLJ0dTvZQUkNKKIks9KvH+aL77mK9VlgPVqqquKzV1zJ123NHb7nQYhNOznb+re16JXoWvSdBRaOefgODDjVEFIXMyuyPSu+n8bZ7kS64z3vIVTwpOc9Vy9/8R9x3mZkj1iCejLr0Do0MTetQRbbDIg1iDEp2eA9TujWLudc0qijGAs2BOrrb+LLf/Fq/fpYeOBz/rNQBUxhm/N96SD7J7mmjbs42zWE9kek2qXzdsCthyGTeIKQXs2qzTyZtpNLI5QKX/6avuGlr+ACGTHyMMbinKWsy5t/fF28LCM05tZYQ+2Tt2EIiXiKCD4GpnWFZpbaCZPcMlnPueQRD+YBP/Gjcul/+VHhDueBUYKz3RpobFu7pctqYgyYJPy3meusRE4U/Uzhqkdc9bq7ZG1MEwh8SN3gs6qkioG4nnM4g/3f/m1c8vQnJxGMSRQkhBTudIdvv/QuOz7xgJMO7f+woOvt3d5c35L5dlulaNJchcgXr7hSN4KwJ1piVTPOcqY3HMRtVeiXr9XW9f6W6LGH78CAXY15mF7c0K66dPc3ICaliO59sTz5eT/N1t4RR3OlzhyTUIE1yV+2+R/S+RJjshRTVTJ7/ByTVRh74QtXfoRrP/JJDr7nA0odIaTHSsRP03nYXPrnpLF2ONV2AQaSeMIwhKDdtJJYzbAkM1+CQgWv/s3fZQ1LVZb4EFBrqOs6lZcb8rdcWrUacc3FakSYG2KlWcvpNqPphKs1UEkanSTGdKOb8mxENl5jSyPHCssNWeT7nvk0LnnGU4QL98P5+1LNzrpmNmizGEtv59aWZUNaXcfFiPXxGtEHThR9b0MVutdoNOJibGwaYiOkThej80utkZJIcIJYwygb4yNs5Zaj65aH/uiTYGwgS7V/tQKZS00LQ7liV2EbMe9l9rZVhntE0QefSKIYmNR86kMfwc+SlKOTcxiDicqH3v3e1EAm0ly2H4fpXY6H4esz4LSGJblm3/su8gM/++Ncv65sjQ3eGaroCZIa+zxQk6azGCdpLQoxkTppHCWahxRN9jldUkOVsVjWZ5F3vep1cN0BZTLDWiEQkUZLHEy6tAMaOmjj5EEj3WoOe2FW6oBbFUO5+SQgdSAb0ECe5RBSiz+l598uf7PunQaKOo0icpmh8j6VWWU+k3MV+uXV7rmW7u6sJdSeaMBmjrr2ZMamRo4IW9UM9q6z504XcumzngEbmXDuniSKtNr46aQFM/S6edun8T6StabDkpijVTBBWR+voeXsZLyFC+hmL/euW1V+VqAoCiblDJM7ggrRKHWW8YM//RP4DQd3voOk9mdLHTxicxQl68RsA04Z2g+Rnctlfe/RDv01RCO5demeEQ5f9U+6VsOazfFlzXg8TgbwYjA+8tV//gJMSsjXqPAUS9mQbWvODpqv4asz4PSHgSwDGyjuf0+57Beeo2s+442/+xKsZBgfGi/RNK+ZRm8oYohe0Rg6BhFltR5eRNCgrImglfJ3L/kzHvPrvwRrkWjS4y3Li1qiKL0ywnKjYJvIGDjirY8hk3iCqH3SIkYfenUx5Yvvu1K3PvRxvebv38v+IOQ+UJAMm1UV5zK8D9smhrQEqS8iVhrSpGzT5bW7rszadMKJRYyjrho9x8aYb3/kw3jAr/68cNH5woX7oDCQWdRYgrOENgjE2GVQktkwc4LYwitf/vTn+Pw1n6bcmpxwh5piurpvO3ZwFUFs34v+ewIwnU4ZZTlap13txBm++z8+Du53D3H3uFjQmOx7MFjbjtoblvhdgyYb0CUFjqM12nY7aSSYKEjl4WsHeP9b3kleRWxQiixnNpvhjCXLMsRH9mvGwY9eo2gaSdkuNtu+cNtWru3HM2DA6Q4NgFhwhrX73V2490Vy6TOfwlEiYrOUPTQG165dPoAqmaTiTL8KFpvz2C7E74jWFWOF0cSjNxzk6le+WplU5KmFBa8R1Ui/j6XL8rcPFJrnSj+mu9wG78+AgSSeMJwzye/JWkCgrqGK3EEL1o+UjCYV4eiELCSbDlXFOMusKsmKvHuc5c7ktju3b1bd36W1J6VpSsuh9k3GRPAWJoWwuZ5x00i4caywZmEE5AbNDBWRQCoj1NEjSBpj1pyFtmcgF2NT8BMDtWKnNcwC6/nohEliG1Ck97o7AszqjE1b4ggGXJFTacDmGVOBI2PLda5ORHhkIMvxZdmUJwXVmBpXYhowP+D0QBeoZOl/pGlTdqBC7pUxSYuoIZJZR4yRWVmmqUGTCrtVgVcyMQuZiR2/ysNqNOAMRdLUm9T4lxnI4YCN1GsFpTWUISZNupHUmEKyZcPIguVYi3aN6oYboBRFgakDmQ9szJR90wDBQhkxUXBiUqNn1KRR3Olgh739KcFAEk8SFE3tskHgqzfq3/3hn/PeV76GdXG4zLBe5IRylvQXITAerVNXocmi9ftpW38/xVslGkVFiQvdK+lsUUmZzBihyEaMshFg2Aye6e328i9ZxYOf9WTu+SOPl6QWlu75DA4POByZybuHlZg2cSiEOpEoaTMuPsDn/lX3R9ctxCcC0+hNsoarRZOsGGoL3jYTAEQRtCPQfRsfb2CqkdpajsXAZGx5wJMfzV0u+0EhgxBSCdLlReqWqyNOTGooMksWDAN2F9oMOs0cdFjsLO7YXTIIZhb58vuvUlMFRCO5yyBEYu1xeYbJLFEDe6zjUx/8UNILN9n/vu6xyywuYUgsDjgjoWnt6r77WcZFD/1uedAPXspR8bjxGkFTAyTOIFYIRqkd1M0Iv9WJgrRWVdFTEwkSGY9ych/48kc/Qfm29+jX332lMvNzVwIVIvNMobb15XaJbDKXXTwYcJtg0CSeBJiGyBAiHJ3xl//Hb3KHCkZ1jaqSu4xyMiW3jtC4bocQEueTvr3Nanf7bc9HY9+i4FzGtJyhmTAzSpkZLrjHxXznjz8Nxka48BxwzS6xJWMhINbiMKg2xCk0mcK24UYEaw0+BsQIaMRiOfjpz+FmHhvizWoqbwnacgXMB8u31y9zuGVtpjdQ5YbgLBMJfPP97smdH/19Qm6oQiDLsvnKr2Cz9HqssaxohRiwG7Ck+es+8uUsOs33IyjccACmtX7myo+wblK2O212BOMcZVkSnaFwGVrWlAePcuMHPqwXPOyBgtBtFlqyuPy9a68fdtQDzji02vgYwNhkKJU7vvmyx8qXvvivWhyr2fzKdYStKTbGuVLHJCUPzTrURtNWmxibczQrCsqqYmTSeZgZx7iG973mTXw9V556n3vDfqDIuxOsfw5q/zjbK7YFhwG3JgaSeDNozULbLklg4eeoMdnzRoVK+eJr36Lnbil7yTChQo0QgiczKT3XPh5RMSLY5uxqZw63C6Ah6fNEW99ni/c+Zcacw/iQHsNETF5wkwkcXbd8z5N/iG+59CHCKE9pQWPTownpzItgZW51I9K0pklvCXRzlbCIJRK6DNw//t3buQMWI4qehOaP9vUu+0Ea0sxqVU3+kzHRUSMGjZFMHFUMTNccd3voA5mMM+73lMskNeEYjDOE5qXQlO2BhiAO2DXoqdFX9ogk4S3tSCMfPGoNKiYtJFXkmve+X0dbnqJSxEeCgpPkFSoi3bkaQiA3ljxEPvCWt/ND3343GFk4ZwOVdCCKSRZP7Yq1aiHSHa4fMOB0hKY1gbZZxCqMAg/5xZ8QtgJ/8/xf09sFRyh9I5lKWmDiXHzYT3QkiVTy6PXRp1ismux0YiT3qQP6/Gi44k9erg997rOEcyXp5LX1rhViDGDtAie07ZMM599thoEk3gxMzwex9R7sz1NO7vKez733A3ruVuTT77qCc3Fo3U4jSV/xvq6wS+0voSVMMK8s5yaRwxA9USOZyzuSKnnOJEama5ZH//RPMskjaw+8l5AJ6mvEFYvkr4d+Bm9+Ze/S/B7aq2Lg0Mev0b1Zjp1VVN6nTN0JYDkz2AWA/iE1nW7GGLz3aPP+hxjxI8clD3kAF//YjwjGw9ilyTCkUrOzZmVWdogvuwy9oN+fid3dXHsky5K43TkiUEcPaohfulY/d/XH2Vcbcq9kzhErnzLyIaZNnLOIEaqqIhhH7jLG05qjn/hnPTQy3Pm7HyCYNLoszVo3+KrGneD3e8CA3Q5l3iOi2jaLGLRwKcO+ZvmR/+vXedPzfoMNl6OVR2KkLCuyIkPr2GUPW7TVsCCLsbYtS1sFG0BrOPq5f+Oqv3q1PuhnnyWQNvWl9ziXYRuCGFjK5PeFxEMwv9UxVFBuAbz3qXRlTKcpbC8EDz5ywcxw/hacGxxFI/+jSBy8GfTRZbP63bvLY+c6+UWTQazrpPuz1pG5nCpEZj5QW8th9RwYKV8bw7GRckMWIAM1BvIi/d+SvjZN3zQTbyNPjeZDpZOHLHaRRaGoFS1rQlUzWhtTaX2zpfHjoa8tNAouQh7S/6KCihCNSdXvqBQuw4qjVqidZcspW+t5ijhrGQHfPXZhTJs4TeULbTKz3/jhDriV0VMGJCjgA+IyQDt5g8bAyDgoPXJ0yjmaYUuPC6A+JM9SZ/GN6bv3HhOVUZ4TjTCZTNjAsTYJjKcBPDhxWOiamVye0Q5mXBAmDOL5AWcIFPCSfBADgIAnec2LWHBZSiM5FT/K2fIerEGcTc2SIRJ7a0l/TQvNWmJUsFFwYbnrOVk0rk8C50wVZjVUNTEEsmb6FVG77GE3o3pZjjWcj7c6hkziLUA/m9hmtYBGh6jwla/pu155ObevBI4eg7zAOMO0LBk15c0g2wW+rQaxe57ItjF1xiVT7FrTCRkKRxBlIsK0KNh394t49DOfJnzTuWw00/M8EUOTFSFNo2ubUVr0MzbdbTJfqGN3r4hTYFrzrx//ZDK3tpbSlwsZ1W8U/V3ocvd2asypGeUF1awkczlTTdNUDsWa+zzye/m2Jz1eGCc9i22ypqH2WOeSXqa/6xz0LKcfrE3dy82GJ2loLXggOt5x+d+SbU7ZP1on+kBdB4JGvEJW5IQQOtP6clZiRgUbGxuU0ynvfcs7OFIYnnifb4cL9iAmnQ+h0YjTzXsAACAASURBVK3GGBvXggEDzkw01ti0OfNmtgpdx4hEuPB8nvjcn+Qdf/oKNo9uMYqQYfG+xDl33ERB12/JXKvYD8FFiHzpn67h4usOaBgF7EXfJDFGlIiIa1wpUhwP7KwbHnDrYcgk3gz6pLDNKHbkqE5dlZ991xXsmdRkk5LxeEyUiIbIWl4A8v+z96ZRklzXfefvvvciIrOquhsASRAguILgKnCnSIIUCe7gBlLmKpKWSVmWbM3YR5pjazwzx3Pmw8wnn2PPjD5Y1tHII8uSKUtDcBNAEAQJgsS+iwQXcAdIAARAoIHursrMiPfenQ/vRWRkVlV3A91YqhD/PtFZGZkZGRH5lvvu/d//JWZ3nor0ormpski/07TeNJO9iBFoVGmsobEGP6q4TxvuH1vuWTG8+uPv4/X/8z8XTt6XklPUIGopok2es2TG0pDmVM3GYOulzGexkNFpeudnABs0HeDO/XrzpVcxcgXiLBrCMRuJfV1EmMv+eJN5LUBRFGmiLypqY1gvDWec/Rqe/bZf4/SP/7qwWmX9rtiNIrZwhBi6bDjpx/c7d9XR1NQY8KhDFazBayCiiSsYgGlkcsPNKusz1mxJM5ngp1OstTjnsmxTWuC1fyd6hNLMplTGsSYFJxjHzy9PWZZ4xRnBmbzQ6PFXN9WUZnBiDNjZSF66SNHOUJo8f0Yll81TMC7NLS97rjz/vW/iwGrBhjNEMVS2Ovzx27lGNydnJg5+xPuaVRWu/49/zmX/5TyoFSMgatLisJ0bescdRu1HFoMn8Qho+X9JADvdrpANJFED375Fb7nkCp5gLMQGNSEnpkSaWZ2Kqme0YqBboR+C7jv9Go3YsuKBZsY0eqZrBR/917+flEyf/XShNImH51N2Gj5C/k5tPTALF8RCooCwuDJr3fvta1YEpg13XnY1+4LFT2dYhdIl402O0SXXrjBhHo6HFJpQSffalAUT4FARec7rX81pH3mPnLZ3nEvtmV7mW98Fu3RefY/igMcGjua3MILGiBpBEWLTYKKDRvjqeV9gT1RcFEQsUji8RpoQcFmTVEPE9mvP+pgXN0qczDDRcOPXLuepv3YWjNYgJEEQ64qFhJmtTr3lSQ1O6QE7Fd24udTWuyorQAweO3I8/dy3yy/vvU+fHAu+8+XLWNUUp7KaTLh+ZGzZkGv3L6t3VFXFdDbj0E/v5IF7DLd9+ev69Le/IfkNC7vgvNhSj2LofA87BiPxCGi9iG3YCuj4idQN5/27P+bJNcxmE8qqINaeIgplURDFUGctKRPn7vYW0jMM+2HXtKpLlk9RFOyvJ5gn7OP0X3keL/nYh4STVpN7LDkqCUFTNZHsdQFSqDXrG1qS0ZX2LxPzet1Z569Lth43br+LlYNeb/jS1zjRG9TH5EmsPYVLfMFjur+tgdgmk2Zvqs2h91BU7A81+0eGF7/jbJ770Q8IFeCEKIoB6iZQFBZECDHgxGITkTIZntKbzHWetTrwnh9DyNSAhYlASBmU1mSR3UhpC/Bw2/lf0moaGGMR70l1ZAWcUBQFDsHXTfJiSOJeiQg2qwqIgLMGq5aowrWf+7z+6ic+LFg37yt0QbdFncYBA3YD+hScTiDXdM1ccyjaViVMZ1BaXv7bHxFmys/vvEMP3PITxtOIW7LelmlV/TlvOTTtQ41BKUJgZQpXfurzPP1lL4eT9xGJSX2DRefFgEcWg+d2CQukeaUre2dMkorREJOTalZzx9XX6moTKWaBwjpEhCJLCRBiF+Y6HIwm3l9bRWS+Pz3OJPK0M1/A+Omn8pJ/9lvCE1dhbGGlQiV5V6zJK642TS2ysALrfJlLVlH756ZGoKSDxMgtV16nt191PaNpwNUNo8Ilr6dZPE+Y8wgXEnGWXl+89i14mkvG20aoiatjXvCms3jeh39dGEF0ylQjEZtkblrPqc4lblQ1h0zoMuRacvZgGT5GIJv/7unmAhCz0C+khc76XffAHXfqt668mlFDyrYUwWbuoIjgvcd7n6SijCGgnfffWoshJZ9Zm2gZzkd++t3vw933snHXPckjH7f3Ig4YsKsg0rX1NFZql7QYian/VUVyQDiBseHVv/dJuddGGktX/WuTA6QfZl6aF/q1IQrrsF4pa8++Dc+3/vozStOkcrcsLhy74XsYxx8xDJ7EXhNUkraeJfPYICnMx5wUEQJiJQlPe2U09ViNiCpVSPuLmDLDkpyAbFr6WI2dW76VvPG+wa5UNE2D0VSeCCzqDLORYb9T7ltzsGrACp6IwyBtiKAfp6a9gMVd8yfzlZkh0T7EQKdHZwANaIxIhBM3AnvWG0YKFkXamtCZY9laeZsHATOvN03OSlu4D5mTKIrXCE3ElgVBlIDSNAFblfhCOGgC65VNEjeS+JXOFMQYKMx8Lu/P6ZIVkg9HcB7GmEcfC22m29l7YiQbjgEbYaxA7RkHpQgRa4SoyZhMrkjFGYMoBI2pC0rSGW0XfMqcq6iavPYjD9SqTqIQdE7TaG3FJZpGy9mVYZ09YKeiNx/0YbMHMb0qmdluIIbEUSRCJRzYW7KvmbDi59G14FPvCCFQ5nrpKVQcu6TMZXUPsn4pPmnjjiaJOiVEJOsKB0yn+hFDQJwQUNyQwvKw4/FtJHZWS2spbX69tcOC91hroPbMfnaHVrHgok/9LU9WFho/zI0/YGs9Qub7rDVELNPZjNWVFXS9ThOadRyQmpOe93xe8Tu/Kexx4AQvBiFndy7z7mB7y2ebt1oD3idNQUSp65qytEgw6A9v1WvOv4inMkYnE1wxgjyAaLuSm9uJ3bX2vYPLq8flU6mDT3p0IjTBJw6aNcS1ioMGznzz67jLBV76sQ8JNtUYNQBBKY8i83QwBHcYtPcoaZKKMVKIAa989/Kr9cm1YwVDIdlbvEw/PYz3ehnGGJzCXim58fyLmO4d61kf/5AQY6pStKWBmL/nmC50wIDHELZpzCn5Mbd6azPxUOGkE/jkH/4Bn/0//h0aNlj1Qhm146iPRiPq2SwVYWi/YimBpYX3HrFgjWGk8IPrbuS0m16lKy9/oRhniSEQRDHGQggYa/GxQYwdKEOPAB7fRmKHnBncc8p1BhDZk1A4mDXgDXdc/S32TJU9GzFz59KnQhvqzYjt57UNK29GXde4sqCyBYcOrrO3WGEaPDOJbOwd8dZ//DE4dV9y84vgNBH5txPJfrBQwDlDmNXY0lGUJcym4Au++7WrWAsGP5mwpxpDVBqN6XqNEFQ7O7UdANqQg0pcSERp37Pw3QKmLKhjIikLhjD1aGG5t1Ce+6bXcOonPiinlgIEEE1Zy6SKGnEWMKUdRoldgK3rv5K4iql0A/xiPz/+2jX8cgZFHZKO27F+p/dUpaM+MOHAD3/OvdrAO9fhpFVoanBJXqlNVJkLvpuh3Q3YtVieXTRGpPWu20SpKp/1VHnHH/yO7m2Ui/79n2IONRiU0jom03Wcc4TehNjv4y21ymAoR2M8kTqkaMFeU3HlX36Wtzz/hbASMaVQtiT53OlSpGjw4j8SeHzf5S0G+S6LKpOjOseGhmQt3vYLvfbzF3LN577IWjMn7bbh1mDStqB/2PdssPiac47QeByCcyW+skzWKu4ZK+/93/4nOOUEabN4Y4xJXLitsXwceBkhZ57YXF1CiCAOvv0j/d7XrmIFi2kC2nj8rCagqE2hhX7jaUMIR8NR7Gp7ShILN84y8TW1szRrJb8slDPechbP/+RvCGWEyiROjBGcsThjIYIphlDDbsCWTbilJwhQe/DCzRdeontr4QQpWa1GXfj4WNCGxFaMo5oG9gXLl/7oT5RZG1pL2LJK0sCiH7DLoWgStW4VAoLvZXEF9r74ucKLnisve/dbma46vEmh56Iotuyf7ZTVF9yufS4YkQtVFCEQfn430y9eqrd/9XKlgVjPUvGKrORhxZAY+QMebjy+jURIHjkxXcJEu+5phTuRTJ6PESJc/+kvcFJwPJGKtWg3ZXYpJm8JZqmf9Gs0A6iPjIsKmUYwwi+05rbC84H//V/DM58klBY0laQzxqVVHBBjIG7rfjl6OJMv3AiEkDXolL/59/+BvVPFzQLjUUlhLK4qsYVDNSUCbDcILJRoYhvpgozC2ERedhWHxPPcc9/M8z7wdl74iQ8JYwtVgWKICN7HeTaeTfdgGCV2NvrcpD6FoU00msU6STrd/EO9+5vfZ4Slrmvqut72mA8GIUaapqEylkphXAfk/oP87CvfUOqU6d/n07bViAYM2BVYcjTI0maylFT3urVznlJhk4ZiITzpvW+TZ73h1TAumW5MMAg2QhmUMiiiaVbsdHE11V/3KD4G8JGROJwVwsaME8Vx5Xl/x2V/9Wm4bx3jCoJtJ01NDLHlUNWAhwWPeyOxtTkiPcOQnti0xtS4Rdj/zW/rz7/9PYqNGTqdorVPjT7OM7UOhzZZpeXtiSaJm+lsRjBQVwXn/pv/kd/8X/8lPOtUoTLJeBODzTWbtWnQ7H07PhevKVFFA1jh9uu/peHvv6d7PVS1h3oGUdmYTVMN6WwcGiRldGvv2nr370j3oi+gPdPAQRp+9V1v49QPnivP/+C7hcoQbCpoEzUiSFL396n0Xgzh+NyDAY8JdMYi834oQGUcBPi7v/hr7HqNiYkhZZztdNweKlQSJ3a8tspkMsHXDSvGsaqWG792Odxxd5da3T+/xYMc0ykMGPCYhzGGGEJH0E+lMSMxaJKmLQysFpzxvnfK/b5mtGeVOqYSfpuyntk8V46rERKVGAIWYXVlRHPwEHZ9ytp6wzV//ldKaF0vqcY6vpdQNuBhxcBJZOtQUleMXEgpwBue0XrDqhcKFYw1qAmImk7Tr98ZkLhw0GUR0Y7DFwJ2VLIuyn0uwChyaK2QPaVLhc2NmS/0YqopiyqqbcWTY1xN5Y4fidgAT/AWe8iz6oWROKgsIgbrHGIM7WJOoqLBd+ThloPYKeyTV3tL172Y1gbeGKYFHFqxHFxzPLFUkACuRBCMpCyg1gAV5xJ52bUclcf9OmdHoyOeZ7pCyF7ElM4FzCIcCuzFMpKUIUlhE+2B1L6OxaEuhTALM0xVYK0lqhKmNUUENInTW0f2Zg8YsLuwad5beqHlIoqzSTYqj/cIXclKXzdJhmylxI8LDhycUTpD9AFnZd4/dfF7XMjqAFE7uTgxUE8njMcjgoDxcEJw0CgiEYwl1jWmKI8L3WrAkfG4n2GTS33e1oQeOT3GlKxSKwTLZ//0z3HTmhJDE+suu7hNze+n9sOiftRWBqIAUZUNlPvHlg/8L/8Snv002fP009KEJKYTNFXAWpmv5tqySceM5Ee1AE3k0//hz7jwT/4M532qdKHKrKmRVovQBwgR0wqK9661H0ZvpW+WV41t+DkYmDnYqIQHKsPL3vFGnvWutwmFgaIk+Q4FDXQZ1N1xBg/irkFHb8q/bauTaAE83H7ZdVpf800NBzewpJKYTQidzuGxQAXUCHXwGGdTOxehNJYT3IhL/5+/gBkwm7dny5JROkxSA3YxpPUikmSkQo7qYKSbfVxZpCTFvWM+8Pu/x/4Vy/q4oBlZQk7qbCXPkkNhfnyTQkXYskDdXBexaRpC01DGyA+uu5H919ygVpN+qanKwYH4CGLwJMY5xaL1akQfOg8WXvjpl76qJ24oT5gZXN10oc+Yw67JoIsdHy9mN7gKiM18QjGoptqzViF6jxSOKIaN0vKSD5wDL3iatCUihFTNpMsezlsrBijL5fYOg5ZT0tZabksNpguXJGmwXjO5+Ye6ZyOyZ5a4ikGTbpUxJg8O8wo0seWp5GN2gse9700VZZJ0QdM0FEVB06QJuXHCxCn7R8qrzj2HZ557TioxiIFoEI1Jk7J/mTKXFxrMxN0BDTlrMgRwSRvNhJBknmq4/rMXcuIUVmyBxlRmrxDBIBDjsXkRFYhKKTYd1zpC9EkqdOYR7zl44df1vhV4xjmvFymFWAdMUfR4FcfhJgwY8Chj2YPYh+ktxkxW1ejEbnLCsYgkfuJLniMv+9h7dLWJXPnpv+NJQShqxYrBx8BMk/5to0phLDakg8yyaLcjh7dbmdIQ2SsVl3/qc7znxS+GvRUNkcI+7v1bjxgGI5HsnMshTTGkUGYTkkVyxz166d98lif7gj1NpBRJ9WClYNrMMKbc5I5tDcVW0BeTvW5RkRAxYrBFwXoIzEYFp730V3jGuecIJkBZAoam9hTl/OfpPC7dk6NDzGKkm685MzFFs3h3wX/7v/+EZ9RQ+iSYba0ldtr7i/qPW6H1HPYh1iadO2vxdU21ssKhULNB4Jx/8ttsjIWVM58nVD0pGyFlcLeKB73rDgzYTRBJkk6tpzqGgMk6oN/93Bd17wxWmkWu8HHI15p/v84f+x6ONiHthi9fyqE1xynPO12rZz9VzDHK7gwYsKtgoGkCrrBp4hsJp7/zzYJXnvDt7+rkmu+zguDKgqaOiVtvBdMkDyLMhbv7kaiWuuUQpK6p7hcOfuNq3fP214pzRed0GPDw43FvjidvX2a39SVvbLKIfnbVDTwhOEYbHjv1WFJ6f2g8VVESTcTbiLeL0jfLWc3RBwoxmCA0TWAG+NWSg2slZ37w3CS34xIP0aOUhSM2sSP6tmx+zYbS0fKjWu8hLHoUJZdianL5vVs/e6E+sXGsqaU0KYMUianmdJxPpjE5+ubh5XwPtwq5t/IGYtMHClcymdZQVawXBp52CiunP1U4aS1dkTVEJ/h87KC6YB30DcQh3LA7oEaI1uAFGmJqlxG47Xb93lXXMvaJu4QoUXShnNfxMBZFDaKma8/9+ktWldIH1hrlugu+CtMAGlP7bOPiAwbsYGyi9S3v2Ib31zb9QJIii0ATYppECwsWXv+JjyPjEbEoeGA6haJkFhpC46GusTEQM894K0PRaOK+lwGqQzUX/9fz4M579f477qJwdpgEHiE87oe5kLMlW122mHMmJUYISrUxYy2AC4FxUeK9Z+obnCRiLsx5dttJY6gqRpOyfFGVMCqZjR2/FM/dzkNlwZgcQm4riqRqLAveQ5lzKPs8ysOhNRJj5hcuvAYUObxrH5iwz4PJ2cNl6ZIXUhOfpF9nefla+5N130CMpCzUJuSsaCNQFdwfZqyvFLBScMgBRnJZtWQgN9kctL1Ye1d7mW3HrQE7DApMQ4PP9ZnT4sVAHUAde31KCutjubLRcTuXJS95e+yxK9HJDPfABGaapDs0ErcRxx8w4PEEBZo8DzrrkpahMVA6KJ3cayMHC0HLkton719hHeNRymhu9RJbtE6WVjXEiFBgGEdDNfEwgz0Tr9Q+lQkcDMWHHY9rI1EBrNCQqHU+eCJJ04lo4Ec/06suugQ79RTGMm2mmHGJyQZUq/EHoNnTkdgaaQPFGIO1FhGLYDnkPetOuFNq3vPf/WPe9bufgKedIphUdsxq6hxdYWXShOVN7kydF+XBuTKMMV3YWbXVfVRk2sDd+/n216+Ag5McIk+d15gkWGp0s2hp581hLmvQ3dP2/gpgLK6scNWIaYxMDMxGBW/72IfgqSfL2skng7FoWeExGAxOJYfA0yW2Ga/dtUBy5agZBokdjAgYWwCCaMS1lpoKt19xHZWHKErjtBPebbEs0v5QsdmDOD+3CEzqGWNXMrnzbiZX36DUngJSBZih7Q3Y6dClrcU2K3FZeskChRVMiDm5K5XOA4EnncibPvlh7qoCG04py5Iwa2imM4IGmqWiE+l8BFHBRsGqLGjy7jGOH/3N57npM1/KXv1MmRrwsOJxbSTC3MAJMWVLGgRChEM1f/Vv/0/2YKja9BQj1N7TNEkhPng/z+LNx2tvqMn72/eKCF6UUFrWnXLm215PcdZL5dRXvkhwpAQSa5MOYCsA1avNvCDT8yAnp74HUTXVuzWmjRkX/OCSy7XysFaOksdRoAl+y2op7TW2Hp1OHJV5Nnf/9KL3bGxs0MSAHVdsEDjj5S/ilDe+Nl23K3KmdvLiCqQ6vaqdkdx+Z5dd+uAuf8BjFAZoQj3/zWceGuAnt+t3rrsRap8qGLUO5daTfZwbQEuXaM+pv1lridOasRcu++wX4da7tb7tLiyWkL3uAwY8HpF48ikpRYxJiWTGgC2yvi886c2vlT1nnsHJZz6PBwi4oqIsS3xMiYkqaYw35IgVc+cDgHWOkItZyKzhtutv5rarb+TQtTelqkiDjfiw43FtJApAUApJtYCNSloFeeDbP9YT1iNu4lGfNAmLKglaO+coyzLxE6Pionbl5vq8PFEYj8dM6xqvEVM4ZhKZjAwv+OgHhJFAVfYIGUDhkkeyO2DyaZREXF/ZV47eVuwbiSGEeSLLZAZTz9UXXESsG3yMSaRYwFYlnuTBCb1WItnTaTWR+12ck/4jmbModCvCwjqqqqIWZT0G/Kjkpe96R7L2nGR5G8Ep2CCY1stpySnMEUvEabr+hSy8wZOzoyHAmi2RWZ2TlBxsRM7/s7+ESZ3E0/N7i5Damgpd5qM5DhOEktqp6Lwt9zcbIqUxFEEZ18ot532Jm867SKk91g2e7AG7E9s5GJd3htCwbKmlxZNJ2c6l4Z3/5B/Ki3/7I9y313KQJs2FznbaiEikCKmPt326/YppkyqNlUXBSByrKuypI+f/v38J928MRuIjgN1jJOqWfy7s28qusCZlKwPQNNgoUCvn/dEfs8+TSLNVyqZqmgYraUKZTCZJNwfpDKf+pNUmdBza2MCNK3xpuVcbDqwYPvCvfh9WHGoiqrHTc+kyuwAfw1xA+vCXe9hbsvw+02bnTBsmd/xC+f6tui8LgouFJgaiKr5uFriGHTWyv6/naVzeD8nGDSHgEWJZcPb73sNrzn07POs0wSqKQeN8VLBGuqSaBa/pViPWctxjwKOL3m+jS7u3e48CRKUwqYLC/ptuVv7+ezqaNKyYguhDt9hq10ztYqRbjB0nI62f5bxgJObyYkRlpIb7fnQr997yI+684nqlTv7vB3UK29ynAQN2HJS0kIuRVr8XAVtkVQ4xYEFOORFOe4K8959+gunY0Yh2IeU+2j4Oc31h69KxQt0k3d7aU3k4cSbcfekVSsjEkOXOtNVYM+AhYcdL4HRGVD/kSU8QG7rEh3b/8msqORFflW999ov6hPXAnmlg5AVBaZoaW1hSxqNgomJcSVSIRoCI7b5faXL4KgiINTSiTAvlte87l/vGDp7/DMEk3UTT03ua8zwEMYdJ75cl22jBkozdrtaR38rvIIIYIHjA8pOrbuSpE6E6NMWZEZ6AsRCCp3JF4ixusVJrM6yDACZp19koWEirRGsRlSy4GvHGcMgE3LveLKfuMTAy2ZA0iF38LaBNtlmy/mSbxwGPKhb6n8zbXVfWkiXZmhDAJm91ICImUqiBjcgl//U89k1hNRpC43HWYKIuhJmPt+OgXY4s8xsXkrFEUI0EjZiiYByF6z59Puc+8xnK6U+WaJMYfapOkYbUTv+xy+TqPS7dp6EpD3jUsE3j27ZN9l9QQA0iZp6oyOJ4EEi2oimgfNmL5UnPPV03bv4R41owwRCtZRY8wRiiEYymvlQHj1iD0cRGNybTkQxUCm7iueoz5/Pec9+CJ+DKEa1mb6IqWdCIOtPx2ftC+J1z48Hfsccddo8nEUC3LrF3OASNiChoymZe3Qic2MAoJGNQ0C0rh0C/XvH8NraZv524tLXUwKHCcLCyHBqn1VX0PpP2N+OwDXcT0ffw19evyqIxCZYSFEJkLRiKjRknVasYYuJPmiQULj6mMPs259OG/XwMSeLGJmO0tI7YJD5jHQOxKNiQwMZqCSOB0hBMBOPmGaKH8wjK0jbgMQVZ+kPYPKj44NPAraQkpRgJeUpxZC7ies1q1uik9lQ2V1eIife6XLXoeONwiTBeI7ZwSfoqBPCBJ9oK1muoI6aVlmoXY8qCgbipiw5ujQG7FN0iLrf9oAHFpldsZL0wHCwNjTVIkaTWrE2avG2SitdINR51NKmuQEV7aIWRT/qpPDDFBZMynWMyBzt6lZ1XcFnGMJUcPXaPkdgzIjY1AE37Oi/i0mdEJGUr3nGPXvXFr3DZ+RdRFQV1DB1p3mUeXjTgbWq0bfir0wtsD6skfiNCjKDWcOrzTucpbz1bfuWtbxKcwVRlmnCO13Vv8qlvzn4WYwiaJQoCfOMrX+HqK65kfX0dg1A6l1J0cgh4NBod1lUvCoWkZJ86eiZxnqSDs4TSsiGBek/Fh//wv4eRggYkG/Op7OCA3YDOyGpDtbQLNsVZl1b5ISTvtDHkootIk+os3P7VS9WGVP3EiUnc2SN838NhOPbLS6qQJpoYiTESQsCJoXQF040J37r4K6k6UJQsDAza69Mxxrmmad8DLnNjepisBuxYyOKfC/NrbueFFGmfMVAYzv6nn5R7i8D+SnlAA25U4RC0qcE3uKrMCaINxtnFpMg8ILTl/VyAydU36K2XXqVJuJRufAmEbmxoz2s5+jb0vaPD7jESM7a7INu+1iP6KSnkhUaolZ/fcDMniqNsEievqMr5cfPkF5lnW/ZXNy2HYhmNRprC8dr3vBNW5llfPng4xtqzR0LygGqqvZn5Ik4MoHDb7VoFqCdTRmWV3ptrQhOVJgZms9mWx13gb8XklaT3eWNM4nBqYKOAs977Tnj2U4VRElo1JmlBxuCHjroLkPpRb8XeD9UiRI2pDRpJ7TAP0EU78t96h9589fVUUQizOpfEjMmjLbLthtn+teO1AV2N6FZrtBCDKNz+o59y12VXK9PYrRTFOVAlago3H24RNLT9ATsey06KZR6gKqjJ0asAJ+3hI7//e9znFH/CmI1Q432iNxmTSthaa/HZUbGdkkGU5Li55FOf4Rt/+wU4NKWb1EmOmXa5thAC72PwUBwVdp2RCD1jsI+lcHE7sRly+bc77tXLP30+o1mk8IpT6eRrAKIk9cM29Nx1AomQVeMTBKOprqUKMC45qbIhFQAAIABJREFU+73vhBc8WygAmxJlJGduHms7PdLnrSSdxuB9J39DhI3b7sBNPSu2wIrg6yZ5Q2LK3q6qKnlRhIWtu8q2M+ZjlsbhjMUrWFvgsLjRmAOl4cnverMwcvioIIboFfUhJSwM2H2QXrvUiBWDcTYbiB5Cg1NNpS+nkfP+43/CbEypVFmtRqnNSuIltlhOUnk4Q8/t8ZPOp3RSVM45ikynqKxjzZZcc8FX4M57Nf7iXqgD6mswSjSyMEktc6Dp7xswYIficE04VVVNjT2gUDmooHrJ8+QF57yeZ/7qi5FxhYhQB9/ROZqmoTTJmRGNErI7cZHyZbAKa5PI3vXAbZdfo3igi4QJnrjZQHyQdK0Bu8lI7IVxjmb+yOsULBHWJ9x2+TWcqBbZqCmtwxpwxnY3SMkh5hx23i6zF+ZcxUYM+9Wz9uY3JE3ARFZCnMOrHjcSfjJWN+8X6Aw/W6SQnxUDs4YbvnEFeymQJqSSgdZRZs3Cuk6rO38UOnAtl6QV51ZVGo3MjLLfT/mH/+YPwYR03a2n0dpUND4Mivk7HW0CRuep76HzTuf3+RiSIoCxcGjKLV+7XO/+2hV6oqkYxVRZIUxr1GeZpiN4Ch8JT2IIYcHD3iXhhIhOasppzXWfuYCv/u3nlWgQ4xY8q8vj0UJzH1yJA3Y4FjjJS4sfA13ms3UlKgaPQmV46QfPlWe9/1zu9TNi5ZgFjw+BsixTyLiN3OV5tm8gtvQuG2EVx2gSuPnrV4FXqGe5CESq+8xW5zf0uweFHW8kyrbU1MMjR5yRJoAZsa8WqvWGsUmlhWJMiRstQtaebvUBITXg1Hh1k5cjiGHqDBsnVLCngEJQm0O9MM/Eepgw77zpr5ZnGGMEV7AShKKJlGrQkK7Ve4+IJCJx4bCF25bQ33JFvER8LwW6MJaJr9ETV1lfKVCT1ba9T6KrSBIMz0kMA3YuWgMReoN4b7EGZOZheqd1Zt6W7Ih9M2G1ifj7D7AiyUNXFEXyWBvpCOh9D/bygLXMIXyw2zLi0tYmZrUh53o6w4owLkpWbME+WyH3H+QEDzQKjU9eE1KpyQUlhd53DGujAbsCy+HlnuNEALEGNUKjkUBKcw4AhYG1QpoTxhx0kWp1TNM0qGquUCYLnP2+BFYfofFUUVlZr2EjkOQyIlYDjvm8vBQNn2MwGI+IHW8kPiTInGhLsDBTrr7oK1gfKYykUnrInIvEvHH2tZz6xqntNWJvDBuF4WAFb/rwP4AC6HGc0C7f6+GFpu9K0jeGqD55aO6/n1tv+SGjkMrtuRySlqid16Q1lI/4FVlcXKwhaKRxQrNS8ZRXvIjnvPHVyDNPE6oCnIOY+JHiHDHqwNzfJdhkuOXHuWNhvpRTNCWJ3X6P3njx17jmy19L2fVRqVyROElFWqiJtXPt0EepnVRV1fWFsixxbXKXD2jjMTNPfd8B7vvhbRy66noFh0MI6lP1psNwSgZDccCOhs4f5/2791rrTIFUkz2/YGyBjgs4YYX3/95v8UCpHIoBCkvIUlOQNH1t3Byp68vXGGMwChs/v5vpVddqqy6w1cSyMJsdbchxwC40EnuJKe0We88b3yQnFhF85N4rr1cuu0YrknyL+oBF8BppiAQRNBt3C9ppOr95oil5o3AOjUJt4P4i8sCK4ylvfJ1QtrwmkxNIMsfp4b4P6ey63mFyR12/6x4dicUGxWRDsfXatIknqjo3ardBKj1o8D6AGqJzbBSG/SM4/Xc+Kq/45EeEwoKYtBmLuDTxq0ucrWGi3LnILXrTWNuPOnmNeb7IodoYIES+c+HFrM6UURBiXacEqxgQlzLwxVoiuqXH73gOWptKTy4d38eY+r8RmuC7PmEQHAarivGBPVJw6WcvgNvvURpYEUPRdrxWkkMg6pwnpUexCBsw4DGNfmiZRYfKMgRwuboRxiXZtJe+UF7wprM4aALBWsQaxBo8SVGgjdz1D9kv2xfVUxphHIQrL7g4JcIFnfe5fI6xdw7taQ9zz9FhV2cOKBBjVnzRiBGDK4oUCGo8zJTP/+l/4ZTGUK3XiM1CuNnlHdGunsJy3diWDNtm6pbWpfJ7hWU2Kvj1f/376FigABWTUl76RuZxvM4jHqs/62nqeDZPVnErtewHAW08q6MR67OaemS533g++D/8CxgJgRrrRvP3srjaHBZyOx8LpPAtXnNi8gAt+KahwPLL627SX9zyY9aaZDianPHcl7SZP0pXyhzmLI12UXOkhcxDxaaj6tYX2VIoptOGlZHlus9fyCs/+VFYsUgxHyw0B93b81XVbtE2YMCOxRLtr89RXn5bP4FL2h028oJ3nyPf+sa1WkwCKzNFvEespSgc0sQFncRNXkVNjo5KhAO/vJ+7L7tKT37LawTRFETL79uqp211ngM2Y/eMUv2lgcz/NL0rrIPHpxQoBMehb9+iJ5sSd3DCeFTiNeKzuLaIEDR2bvS2DNjyCqSua3yMTJuamkhTGM58/avhjNNEnvs0SaX7Nt/mluh/rFOcbHqy9H1LnRgFDTGFwjK2mv6Wy5Mto+20lXE0kyllWTJFedHb3wgverYgHqoRAUPAbGkgDtHmXYB+2+hHV/MfGmP2MiiFKeDWX+gVnz6fYuYZFymzvk1CUZsflyRoYLPHb/n1h4rtGM3LvbZ/Tv2tNVadEUYI+7//EzauvF7v+Ma1qWxfy6NqoxE537MrBTq4MwbsVCzNLf0kz7m7fGmuWyAHmiQDt3eF1//mBzn7Yx/AO8EZS1VVzGb1pq9cpHtB5SxWIy5GZDrjxku+DtPEZYy98aHzPg797UFj9xiJWyCSHAAhKIjB2cQ1xHuYNXz38muRAxPWTEGsm5y0kQZ+7/0R68KKJmHd0WhEEAhVyXplOe01r4CRTeRcMVsbQvoIGUh97kWueqGhyV7S49NnxBYc9A3rlXDGOW8UKoPvacRt1cj6JZIG7BJs8XsaMUhUCgQ8fO/rV7O3MayVowUdzn7dcugtUDLdYzsj7ZHc+qG0hQSYHHmwQdknJdddeAlX/t1FcNd+8HGegNO/L8Zs650cMGDHoBe/7c9nutVrurgPSBq7qyNOPfs1Ur3h1TIpLdE5ptOaqqoSn7nn9FmeM33TIPlxhGX/D3/G5O+/rbd967tqdUG9ddP4tKuNn+OIXXCfNnvOepzZLHOWqzug2YNmIFqqSc04wmpRIMEjGrrqCkahdEV39LbCw7JeoPeejekUrUqmleE+F2BvBRIW2uQmLq08tKzsTdAt/1y4Fwv7NYsCG9l0TQ8mOaD9rDeGDRup941oThiDDUDoCP52yRNpyRwH3aYw+4CdhawT2kdXYaRtfFGgATY8JwXHaBYJ0zrxDiUubG1/W6xoLt1xj1Ov2f5ytvGcq/QN1vn+NrmtaZpkDM8Csj7jJKlgvVaUVLav5fw+QuoGAwY8YugZfpbNduDCk+UQUpEKVjR4sIH7bOSQaKrbnMtxBpFt5yZjLT56ytIxMo593jCewj5voI55sBi4v8eCXWAkHgEKRiPS5eqH1GZ++FO97Vvfo8QwObSe9ANjStooyxIjQjOdbdZnWgp7rZQVxlkOxYYDRlN1kWc+VXB2oWk+rLIXuv2xFybV7hZEVCJhi+t5MIgGJiYyWXG85R99iLN/4/1wypMIkqb5LrszYz42xIE5vEvRzgHdosh7iHDwm99Xbv6B/uCmm1mtEk91O49dYvAKdotw83JbPRbPYJuQtlX7385QTI/SaTQ2TYMBKusYFyWosmILzKThuvO/lOo7R0FCxHbB5u7kB77FgB2N5W4iegSen9Bf9+W8SsGNV2DvGu//3d+i2VMRVIiZjwip7wUzr3aWEKk1pIWbKnFjg33R8e0vXsLl510A0W5xgsO082Cx4xNX+h7sFnF5f1TE5gFagQB3fvM7uFnAmaTjVJVl0mlqPGqSf7woCrzOTb0k4jm3q61CM50RCkH3VMxWDGe8401CCYjBBGCLdnq4c3/QF/9g39Z6EkUIZnN93HZyNEvHjt0EOd/XGNgoDWf82quwb3iVnDpScGAkXXQMIYlmL3Xs9EVHd+4DHtvomklr7+TQUsgTgS0c3PUAX/nL/48TGkMZIoeaDZwKhbU0vQzftqpKCjObrn+0GqXtexD6CcPHBMmGWm/O6r3Yey59XVRAU+b1qCwAiF6ppzPIySgO4Zc/u4NfXHWdnnL2a4TRfOpMH48p5DxgwA6GLP+RF//SGmNLbsXQ+2wuZJQymn2q6z5+9UukvPjLynfupAygmj7hs4Foc5iwTWYxZYEPDaUmMX7TKPt/cBuTVQvfv1V5wbNSIYt23EgfH/jwDwK7cpQy/ceokKuCJA+WoD+7Xa/80lepGkVrj3MuhYskaSQaY7o6rctE2T6iAK5AxyPO/uD7OPsj74O1KnkaNGkTwtbObn0Ybv3W35MgkLWAmJfnY3N4bVmXbrkj9b2qjTEcHFlOf9ProBQoDCEqIaaOvdlA7B+otw3YwTC5+NVmWIA68r2rrtE93rDSRAprsc5RlmVXzaTz7LUl8MzhG8WyN1B7fz+YR+mFjpf7Tn8xtOxpTB7N9HnvY1ZQMBRFkSWeBKORlQjXXXwJ/PKBtFCN2snetFUjumNy+MXkUb1xwIBHCUfVdmXzvqgxOW+sBWfAeN76mx/lEJ5atJuH+v1z3h8NIQQ0pjmtLAq0qXF1YN80ctMFX0mcx94XLkjhDP3oqLALjMQcUF3iRXXciBxSCm0JuKh85+prWWkio5mnUEGigBo0CkjSSEwFwjWV9smagSKCEEED4iA6w9QI90mgPOdsOe3Nvya4tOLpdAeXTzcbRunhYbr9vQwyif0OIalEHwIx4tRio0HUoKSN9u/sOXGSamTGGHHOQYipXBKwbiIvOufN8NxnpSxuDzYanBTza910DzLjTPM24NHFNkaHLm1bvkDufT1ve8CjEpHowUcm3/+J3nLZ1VSqWJOqKGgvYUOwIPNNRVLdY5voDJqlRS2S2i2pb7XyVO0W9KE9dpcl6VqCQDRz47Nf+s+yWBJQRRCb2nAQpZFcZ1aUkQorXjmpMXzlz/6zUnt0Os1qC8mT30Y8Wu/GJjHi3msKiz/EMMHtDmzRybbqd8v98bG2VthqWbfgA2gjASxmO3eFb1WTFMmohGc8TV70xtdzwCiNMUlxRCNWFWKqBe+cy04cg5MUEG28JxaCmMBK3XDb9d+En/5MaZLTYn7flGRZDpz4o8GumKXnIa8tfGkCEHDWpFXFRs0eKSgaTyV2wTPWP17Lf+qnn8QYs4h24kxNmpqNAiYrZaqqMjLUqol5tBRKelhudO+ct6PmGrP03SYVU3emIPbKHnVhvsXDMvNNrrcr1L5JSUAxEYsZVdSrFWiTbppxaUVI6ohxTtMfsBvR/bgpHayu65TNrNlPPvOMN5qUxDGtIUTGRYk1JqkHtILZPc/2dqXz+u/Z8lTkoT0uHyMZi3Pj80gQkoLBQuayJv+q1UgVYC0IrDeISqovSzJ60bi9M723cwiNPX7Qb3Gy9PeOCL4c4QQXrkGB4JOxlhMpEQMW7rdKvWfMxERsVVCWZSfdZlAObqyn46nJ8nRpllNRRJUiRMY+wsSnTj2bdd9rkWQLDDgq7AojEeg8Yf2k9+Q8y89qD16AkssuvBjxcSG0GiWiZj5op/rLyavQZE+JlVQCiNxYbVUwHVs+9M9/F0wqOVeKzfVadZGAwWYX93EzoGT7H1JIguIpzTK5MINGjLUYYwgmJbFIntS6rZVxKyy1UYKkOraFKShsSTPzTJuaM1/xMlipktxP6yTMpJR+ObYBOwvbTkpLOyRGWs3osiiREFPHmSnMIl/+b+fBAxvsW9uDQdCNGVVMtA7/IDSQ+iHm7fY/lO1w33MkHca+OkDrcXSaFpBqUqpciJH6gUMcuuJ6/fmlVys+9iarucD+giZj/3w6DmS7gx1gKQw4amzze3ZPt3EhbpeFv6Mg5BAzRB8QzRE/Uc58z9u4z3riqGLd1xxqZkSbImAaIqurq1jnusNsdS+Mwg0XfzUpK0gBPs4jjHbXmD4PO3Z84soyulJ5+VGzZw8cd11xrT7JG6pDU0qKxEnawlTrcxBjKgmRhLR7YecmeCYGXvBrr4MXPlsoDMZCU9c4W+aQ1CNrILVZpf25LZduTv+JJgPXpSSdw8lwCOk+1ESEZFBaa2kiTEJNvVrw9g+9F049OeUDaW8Wy6G0mKvcDNiF6MJHib/bKgMQBerADy/+mu6rhbEXVoqKMKvRECjUYI3FE4iquCMYYtsZcsftMo5kCB5By3BB4FtJJTphXqlJhNIYbrrkcg6MYM/TnqL7nvN0oawghs7znozGuS2obGEHyqKtOGB3oP2td7rN91CgkAT3c7UzY23yLp5ykrziHW/SkyfKDV+8mDKkTGdrQVTYmK5jXYnp5VJ3c36e6myEH1//LV7+k5+rrlbIaacIRDwR58zj96Y/SOx4I1G247XlH1+MgAemykX/6VM8oRFOs6vYaY0QSeqJi+hn8gZVrLMQUrausyXOOQ76GfGEMWe8+bWpuKRxeO8pilFKEAlK52JhK6Jsp+R4jDdg/rBVexdNHvycaA0Ce0/YRxMDqqY7sZgNWtcvqC7gTE7PbhScYX+oqfdWPLBi2Pu2NwgEah8oXOIhag4fGjELBuJWE96AxwC2+x22sUY29ZUYklagALMGxBJ+ert+76tXsrcRnI/EXK91pRxBiNRNQ7TgygJ8OCaj70hG3JFwpO8+shG5+f2JtiHZyyhIUGwIjLxy4xe/whtP/RjsMVD1xELa8UoOP2+1o4ZtPzP0ox2NlnO6PAt0P+3hDJkd/tsrEJCOjghpnnKFg2bK89/5FmGq/PKiC/XpbgQHpzQKVVVRiGLEomGuq7rskHER9tTwnb+7mHsrw+v/2SdBIq7MoWnVQa70KLCz3TxHMT/EEEAj+vff0bWJZ2WjIR44hEOTNy3DkIyjhciO0mU6e42dcO4seELl+JWzXgXPeIowdmCWJhSXMh8fSXQrqf6T3ikFjWAd5SlPlmrPnuS+X0K/qoQoEJXQeAyCFyWsVbzl4x/grA+9G1YdFJbCunnEJCbB8h3DoRlwbGh5hTGmZf4s8IW/+GueaMeMvbBajaiqCuMcPgYa5hyj4P0Rw8db4eGsqnK479oK21WDMTnRJoZAIYa1omIUBXP3AW78zAWKWgi6mIDQhhJ7T5exswfsAVthq990K4qHLj3uBgiCsY6oybcSY5KWYjSC1RJOHPGRP/g99s9S6VcRYTKZEH0gqp/TybaI2RUaWWkit1739/z42hvhZ79Q+pGtwUI8KuzsMadnCG0S2hXAZPd1hJsuu4o90VIFGBmHAq4oOq+hyR60KEmXTTRpMnXGowrGFqgYZqpMxwVPe80roBC8SArK2iJnaQlN7XHObKoBu8DnOg63YHmC2eoNrbcjtoPLnlX2PvEkgpXONW9i2lq00j8mG8quLKhF2C+B6uzXyTPf9qZcToXUu33KHjeSTUQl1add5tMc4XQHPLLY9Dss79jih+pXPmk9IZILm//owq/qWh1hWmNFCHXDzDcEA40T1BqaEGhaD+RRYjtjrZWseqjb4b5nKwN28wcWyWHtIkuzoHxlHKV1TKcTVosSd2DKPTf/kObHP+34iQvo3e+uck3PKBAiZhtFhwE7D8uL6e6n3mKsjEuPO338zFN0V9EsAMZIklBThcrBikFe/kJ5+gufz4HplKJKFVqcc/McAeiV7pt3FhNh5GFPI+xp4NYbboKZRxtP7L59wJGw8++SzDvOwj4BzXI1RGVVLW5aU4klxkgg4vMg2/LvYG5stmN/rD2FdRhjqIOnJuJXCg66CCslOJs7rGTSbWqgruyRauHR69HZSExfb7OXUbB7V5gQuqorffTvpYigKkwaTygtzWqZ/PhOwFkQkzK52wm3LUEW46YM7wG7D0IrcCtQR9ZU2IPDeqV0STfQWJuMQ+8JBmKuj75VqHh537Kx9mASS47q/I/Cm9ie1/K5bVcqrDWg2+oQk8kkeU7rhn3lmD04io0mr0jjIum+d8yWY7x8lwb/xy7EVovppR/eLD3uCuQ+1fiQaPNkrQQREENjFErD/QX4tTEbdcO4rAgh4L2fH4ZsKPY6h1VwQXF1w94AcmADrEGs2/lJP48gdnx7W3B69J40+GTsqMAdd+ktN3yzMxCjWay/2hqIRjc3MhMVEzVlQxuhriyHSri/MnD60yRlFie1J2vt3KPZjxk9omzzSOyZeUqy35SUwBM1goWXvvbVzApLsJYgAi5rI2YRcQ94a6hDpChLghUOifKW33g/XfUIkflNz39rmynTNxB7tWvTUx3C0I9VHMUPYwCNAQtYIhIVaoVb79DvXHU9OmsoSkcTGqImQVxVxRQuJ1al7Oa+7mBfyP5wBltrrLVb4Ni25eNtZ6Ru54lMFVvmQuA2vz/YpPdYa0jyHZoSe6ahRlT58qfOg/vW4YEJND53VAjEeemxoZ/seBxWtqmVfdrqPT5LYyhdO1hoD8uLix2KtlBl5ezcsyimG4cEC0Z45blv457CI1WReIgtFUzmCgPLc7pRIAZGWMYNXHvxpTCpabPDBxwddn7iCrmQSH8kVUBSqBcPB3/yc9bE4CJMZxuMVsbQJlj03GZt2S2YG47jsqKua0ajFfb7Cfdr5D2/9Ql+uWpzfNZ2+VV9j+Ym6/tRGulbCobJCTqaJ7XyOaezUYCrPScWBc10hlUY5UoYnmRuFmXJwXqCrKzRjOEpbzhLsInbGGNEjOsMUGRem7ar6CJbT3NtpviAxzi69Pj5bxY0zhOafN5mcMFf/DVrQXHGLigBtH2q/2tvVcVoq8nyyIklD+mq5p8/wuuHneSXnvfHj/Z5UZY03neTmojgjMFtzPjRFy7S+2zkVz/y68KKIxoliEFRCthU+3ywFnceDtd+RQQfA9ZkbdnQi764XhnHXl9KO3pNYXnu28HYrAeac5dNZPzC58jJL3q+Hrj++1iT55uceJKiY5sjYpCSXDamE0pKVqLF/+RWdS9+Ts4ucwMv8Siw4z2JQCcfYSBbalkPKSjMlC/8+aeIBydYhNU9awSNhManzMo2rNwLM9sIiKKiTJsaNcJ0OiVaR71vBXPWK+TkV7xYKHNH7nkLD8cVWeR+HZ+KI0cMZWdn31zhXtIHnnySjJ/8JE56ylOY1p6VcoVSDfX6lBgSeVisZaoerQruN573/dbHk2i4dUSvGONyaE3wKIHEK/Foet7rwN2mPRrXsJp71LGtpyq/oP3freN2ZP5CABr43ucu0Acu/rqu+eR1N852nrP24y6mfmUjCx7EBU+AkU1b//WttmNNVDnS8bc6p3brjsO837d1poVcXzZ7FFv+Y8yr2lWx3HnzLRz4/k/x3/+J4j0mEWRQJMkKwRYDyiaW84DHMNrIzFae6ogSjWVCoEGJ1iQaVMhhVJnTnzY3g13UBnQLWlYelNKiK1Viecs/+qgcXDEccBGcoTCWrbi57UItCkxmierhUMoYufHSK+DgBMR2Do0Bh8cuamk9aEgXFgLc8mMdNYHKFcQYaYKn9h7jbEec7ye9tLkYLYzNnEPrCKXljf/g3VAJjNpVyFwQd1OTWyIfb6KZPwxtdFMN2uwIag3plM0jYAIvesvrOON1r2BjbJjUDcY4VlbWEBGs5KoYIjTWMF11uFe+WDC2q7mbbkxLAU5TZVuLs0tgYa4ZN79uGVZwOwSt96KtOQxttRBN/evnd+ntN32XGy65jGoWKTOlI63V4twwFOm2/iC+HFo+1mzkh3J9x/J9RufHUDMPPZs8jvg81qhN5cWMMWiIjMSyRwrGs8iX//azaeKKENWjZE9ta4wvjSPD2mrnYJk+sWUGff4XNSZPs3MgdAuFtoRj2pYmqN08jLarr5j/fuKJPPfss3jRW97AFGhi2PyRRU8MWIPXQNM0WK9855rrsth/071lwOGx443EFEJtpSQUTKq7nGBg0rAqDowwM4pagy3Scx9jVzWhNRKNKiav9oKAukSe1dJySBue+MqXCA4whhDjJldM5zQ7XOM7niGCI3gRW+ePhqxqD+lXryzPePPr5MT3nyOv/I33csCCR9ioG6w44sRTSYHGdN8+/K/+BdjUKa1YxBhiCFhSbWer4FQoxGRV+7kCpSDzMmfbuq4GPJpY5s1vaYyopnJWYpIOaBQu+s+folxPfcyJoTAGDQGswZYFDsEhC4kcsL1x9ljHVudsew06mJZe2Ku+krmTMRuPhRh83WCCMg6wLxiu+9z5ShNZUaGEPEa07pT08HBEIgY8ctjKo6jBU+SwaiEGk92G2vhOZ7bv8erqeEue73Y6uvlgKWM/X3JoRSSNgbLgJe95q5zy7rdSlyZJb7W3Rg0STVemr53Tg0YigWpcUQisBcOBG25WsIOBeJTYPaOM9moFi6ChSQLQFOjBSZdU0tZfjj5gjO1S55fLHLVSFpOmRgrHRmjQcQlk1qwwFxHuYSEY1HOdx+V9PLJtNOWSpI4RNSY26moBFfyiiExPGnN/ZThUWQ5Uho0Txtw3hv1rhgfGQhyZ5D1NB4AmpqHNA3WEmU91aXNdRBNZ0kvMHqku0WWXDHK7HK03Q4yhIwAlzZskczNpGKvBqVAZh5/VuCxNMZ1O5zXQTQrdxpzgsV1m8E7Fcgi7RYVBfPIQGWepg++SXqL3iI+4OuAOzaAGvGB9TLU0+zQNmWdNL2DoQo9pqGpKlsxhZ+glQ+U+4yLIzGd+r0IdEHFQe+Ks7qhUii4sSHYteuHjViWrnjRg4VAhMBI5QIMZlXNNX+YUjxYRqKoCr5GNegYkSRxzcJoWuQOOCrsicUU0hb8skgp8A4d+uZ89M8f5f/TH7IuW6JMuW5hNGRdV8qqZSDTJeOyyoZhLW0QB4yyNKL5y/MqrXg5PPAmyhAdiu5No+RTSJyjZGBCPAAAgAElEQVQtoeNMkibOdpF0zN3+MJ7JmP/raNCaVl8NSiBQFpYz3/N2OfN5Z+pf/dv/C5k1eDwvfMOrecErXsrqk08WJMIpT0oHCcr03vtxM8WVI1hZ6aRw8Jo6uDVzL0gMYAxickhFdJ7oMuAxh2UjxBjTK21JWtE3HkS49YYbtVKDCx7jHLOmZlRVeO8praOsLF7nRwwCPq+gbDy6dv9Y8S5ul8CiRrpsfavkJH4h2KSMUEQwUfAIQSBI4uv+/+y9+ZMkx3Xn+XnuHpFZVV3dDTQa9w0Q4CGQhEiCpHhAvCTKKGp0jKRdzexqNfsP7G/7F+yY7drsjtnMzsq0NjuzsllpRqObpESRFHiJhHiAuIHuRuMGAXQD6LurKjPC3d/+4B6RkVmZVdXd1eij6pml5RV3eLg//77v+75+z0FIIv09Zzn16mGGP3xUTy4U7P3APYKVsUhAiyCN7ZxtVP4it2kIeYMkmqCgFnxKgBxVMjBw8jSVHxKNYK+6EjFgoyJWOvSmS//mKyMZOkh+MmTHRNNcyRgo5wsQmL/6SlgJ3HT3XSw98Sw9wHXGbsn8+wZJXKmGFGXJMDvqPSwHH3mCez//yQR2TCkosW3jdsk7ie0AJgKSxWlDILxxXFkGN/T0xKIx4r2n1+sR88zeWttOKDQ7d03jasjC3sCKFU64wGc/9ykwiqpBxI7xDNeSBT7vc5ZOO58sT2RIpQUbpy16jykLajwiZXJUTYTbb5R/9u/+N6hCcvp6OTPBZA6jSHpireXhxx7VXdLnH7/xLfpRuOX667n62utwPcf8jgX2XHM15eIOIhFz5RVC4bIjmR3l1qMmQ7j5SLuh6GnP7rQLeSbP+FrrT/43uV0d/09nLHapWjO3adpP1/9onKA0u4jo0eNIpTz6re9zjUYK6/A5UaWVpskhZyVPtrrPGbQJH5eKzdRPTH+m69dNShAQI6hXnHNUwaNi6PV6hGFNEEnovhhCHSg9/Ojr3+SYjfzKXXfBzl5C2q1pn+fmHm3budt6LJ2pC8r4z2NBYDWgEWJ+1/wkqaTvbfw0ICsDZDhEa8/JV9/Q44ff5NTxExx56xgAr7x5iGWn/Pb/+LuE0ko/b6/pNi83W/1kjQKcxozoz7YARUCUD/3mr/LXB/81Vw4V10Q7ulG6vFHrSuo6YEtHiJEywE/3H+TeN45onDNirrkCFTM6hk7HPnaPL7cO/wzskncSo0pK9GoGsuDBC4d+8gQMhZKIKtgYESPUWSewIHEKFcFYS9SY+IeDAc45TOHw6qkE7vnkRzg0Z+H26wWbeA8hj6hTq6nI+G95bph+7sDjm4Iidp0cAcGMHZMArjP7NEWqsdzDjWqGGpOylh1Qms7BWZoKMukipqzmn/vcZ4VhTSFG93/pAQb7XuTQM6/h6yGldezXrEVpBQ1RtbBcee3V7LrmKqy1XHvddcxfezVcsVOY78FcCUUBZJHuPLtLgKyslgJpkiiMGaG2uTNp+VvN+U46gd33rlM6y4GcXK8BSPPXTbmHF9I6592chxGoGZ2biEkI8fIQKHni69/W3lJk70ApVPAS28sZVVOVI5FMq5O2ZrhloiPfwOFdaCRxo7Whu84ijOqkR5f4uKaROfGRwti2/agP9Ho96uGAOWOwvYLH/+yv9b3//W+I9CwxekyWmUrWaeAXCcp6QW2KEzd1vhc1t0klaESMbZfzjAbCputDRzIro6iIdgAEQVCM5imVj0nbMBRw9BgMg3LiOM/tP4gOh5w6/BZaed566y2ctQwGA3q53j0+Yn3EGEcQ2NmDX/7d32LXrTeJusTgsQrtaCNdBO7SZoxJl1eb+4gu7xLS5XcGNESsMeAKuPNG2X3vu3T4kwP0aiVKxIpgRAjZN/dRKXInbwIUCoKyEzj5nQc5uqvUW3/lcxKMYrFJ77WhRZEnt+0vuXqYmFnzhsvWLmknUUl+Qs4BQ0JIUhOV5+kHvs+rp2vKGFED1qcHLZoRqh8EiBBqjysSmXznzp0sDwYsDVaQxXlWjHL9xz/CngUDJakzsAZjYKmqWCjL6Q1FZnyVmYucm6233SnHY1d9mdLhNBPgGLDWIDZlMJt+wV0f+aAcf/Bx9S8dwnlPSYFWHmttyjzL0h/LSwMGg9epXnuTpZUVXioLghW8FV2WyNye3bzz/fdw7R23MHfnHYIPYAWxkhwUGHXWnWrwMQSMs/iouOyNayQjNOtfpzFEsOsQyui/sd8vx95hCjy1CknJFXQoSvTAi/r6089ypZb0hklqatWgLDLTAZymjzj1sC4SB2gjx7GWI9lN1jF55NOOy1cURdJhdQXDylOq8NZzL/PTHzykN378Q2JcEsYxZJS2nYheHNfnorEJB7Gd+4U4JhcjSKtL2OTGOlITd9Jt+0LwHuvcmKMojLLzUSWePIWJcPT1w/rik/s5/cJrnPzpYeZsQRhW2KzP6yqfMtp94qQuREVsoPI1vaKkNCXLwyHqLB+7/2PsvvsuSeLqabzqRqq6EaxLfpI6xbrRsKbqkCiJF+1TBTUW+vzMpz7Og4/uZ4ekhNOYXyFGbJnqxcd8vUWhZ9OzVp9Y5oUfPcLLtuLWL3wW6wwaYxprAqsv6gz62FaxS9pJhBHQJWLSk6QRxOJODNgVCwa59FyUxIPqeTI3yBDypMRqanw9DKdPnsajzC/u4K1Qs7SjgL5hYJQeCr2CEANRLXNleaFP/7yb9z7VycwMYhFpSdhSOHS+JPQd5nRWsneWyvuEloSI2BTWj1ExdWTBFKCGug5IrexC2DkQrqoscyuakmBMh++ZMmASt9FZah8oiiI9t85Sk3g6gTT8GpO9uzgOJa4qPdgZVIQJaopMdMTdwSN/MONfL1lb5QhnG0fIhcQ5Bakjc+Kol1coMbRq7ZyZY9dK61wGourdczmT5QG8rxARvEb6/T6nhyu40rGLAgYB5g2RkBCUbK3TsM1JHHMOxzje+QdRw1i8HrJDIEgnTGls5gT6NLnFGqxzREmZ6UYUg2a0yUAdQC1mkLZ/5UA4vqywEokVlCYSh4o1EROUEosEZa4o8THkyAnM9frEGFkaDqidUO8oOW4De4oUtraY8b7nMut/ZtkkXNHcM1zOSg4Rb4Whhbo0zHuL9REpDSHUWe4u0CsKog9pIlaUWIQr5ndQ1JF5a8ErUqZASdN4mnwB6EQ+ulGnLWaXtJPYDd+GELBGUnmwl1/VvhToMGDKcZaeNIBUZz1UcDFxhwSbuPkRtCy4+8MfhDtukV1W04AoqUPJEe7LfkJvOoOT9wkltNbmATHy4S/8In/zb/9vdjrBeSXGgHMWDYmjVnufwgBB0RhwIqgmJ7O0lkIsy6+9xUNvfZuqEAYOHVq45qabuOWd72D31Vcxv2uR8obrBQ+FSYOnOIvEGlNYNIeR2rB0UwZwQnC2DfF1vjcz1dZZYjTgdJeb5DdfDrd91cDa+aNt10Jy0CMQIl/9kz9nZ5Uq8/TFUdf1mvtYv2LKuV3Jc11/o47dRmytY5m1H1sW7URsOBxSliUofP8rX+fz734n9Aqsy8j5pFe4FTqgDVhzZcccxMZk4p3cpyktooiSnL4Qk/OmwEoNhcWISZJotU97eOsoVEGffuhhjr7yKm+9+FP6mlBe4yM7TEGvihTW4COUJjl7ToQ6eBIe4ds+VEPElAWx56hL4USp3PH5TyeZtUxfsJPnwuXR/4zZjBNSNI3Z1oz69syNv+Z994i7ZrfOh5Lh869ThIhx6TkxIVIaSxjWGGMorUuKAjHiq5qjh97gZA949bBy27WCsS3XQHVizpznBZOHulXAxUvaSYQMHBpSFhiAWp5+8EfYXknwFaIBk0nETYg5mBGSZBCMEWpV6ugRsXgfCRhqgfd+4uegzPFL04QcSLPUy+5JXW3Ng2WMSSKv2aQhitx8ndz6sz+jhx/djxtEXDSEXMlGjRBVsSbNohtPTGMq3WbF4r1n3hWEoPRVmKsirigY7nuZl599jReNsFQNkLKnH/30/Szs2U15+y2CAXPFYupBbSSGgFhLCE2IaNxBzM8/MD1U0x1cJHNRmnDUmBM5yWm8DKx1gtsTpQ0j+1jjsBBg/989oAvimLNA5RnG4dgkAs4cTVyvFN9mOnGzjmHSuvtc73zOBj3srteEG0PwFM4wqGr68312KRx78CG94hc+KRhBbWg5Ut3g6Va39jGcRkS0HaS6QQWyBxDRDpooCT0obcqQiKSVX3hVCZbjzz7HkZdf4+lHH2felViFwiY0/VpTYEjVpoxxlGrwQAgeYyQVJFBaFMw4S2EEm/TYML2SAYEVq3zkl3+BIyWw4EhJg9O1/NL5ms6XS9jWmXxLQyNqIw4CxhB9jbGGT/3OP2XXKc83/83/g42GqGDEYUNAYsSKaTvvuq7bSkk2wg5T8PR3/5F33/TruSRU4ht2ZXW6Hya7/Ev90m/ULnknseGeGcl8heWax//hB+xeWSEiuYJKmoM3DmIjeYNEove4skwDlkAIETM/xymtuOeTH8feeVvbZGITpmg6/EBm47/95/12WuMkNtZKOEQFo7znC5+TfY89oTuMslsKCrW4wjEYDDBZ2Fyi5gfd4kPARkOd+Yve1xSuwKihr0J1akA/Rvp9i2pkTi310PPU177LsnqkV2hF5BSeT/3TL3LNve8VUzgwYINmnl0cZWVP2FSqd8dBapC0rk/YIm6XqaM4diqdL1ZyuO61Q/rSY0+zo/KoT8K/YlOiynr1aScdqe7y5xtpXM/WO76NrL+WzXIim/V6WTJIa4/rlZTW4QcVPWvZ/8OH+egH7oVeRK7ZNZZUkQZLLpv2t9nWRgNylMGJpJBl5rYld0MSWuAz0nByBVaGvPjYY/rDb3ybXoD5aCiqyByWq2uHDDyFdYQwZNGWhCpx3qIIvvZ4fOobrcGalJkuOqqU4kOqBqY+TVW99yyLZ7DYY/Hj98liSa4xa/ExjGqkNye1xe53t251o9lqjEkJmBrZdc9dwqmKeue8Do4t4YAeNtV3zhWLAprvEbjCETRiVJGh5+nv/5h3/9oXYYfLyUAp8bOOStEhtuvkhy10Hy5tJ7EZ2COI5JSmN49ruVSzgMPmsnJCgxwqog3xOGYOXSK39k1BHQPBWk5qzbHFgts/eV9CywSQ1Hg0RkJMD7q4S/vybdScc61jOFYdQwAJcMUi7/v8p9njLY995e8RhWqwktBHkRZVhCQobERx1uGrGtWEKorAYLCCKwvKfpn2FyMhZ7QVNkmFFEAINfNG2CmGg3/6NR77z1/WoYUPfeaTXPvxj0pK1a6hV0CbJDCBv3Qf9u443vncJIW3WeBjG7i8bAxNzL8kOodAFXj5J0+yoxYIERGLM0Ko0mxe1yovlKVexm20fDzL7MzNch41bWz17xtECE2HyjLNupuets0qKAbo9/vpfwehChRqCUPPsR/9RN8sA3d94dNJOzFn3a5CO7aoTT3/iR8DWZnVJKUES+agBWAQ4fCb+tDXH+DVg8/TD4IMKm4wDhNJlU8UEMVam2SNNA08dfQpCakB9URaoXQfQ3IWO0hY0KSmgYxqeVcoYa7kl3/vd6DIXEWTEObCFVM5w11qzKVuM32uJqqDEGoPRlpefCTTiYjQtxg7x4d/+XM88ldfJy4NqAYVPSO5tnMqHuHJ+sAaiWgTgEKPnmD4+NPa+8T7JGWRp+fL5SOa5LJvRbv0vRwBYsBYgTpyat8Bitpj1CSysWoO0+RWp6PVAMSkpIeBrzH9PisxcNLBb//P/xPceZNgzSj2poqYNENsZVguc1vlGEJbu9mYfG1EeMfPf0RYVuqvf1NjUEQMRnKompGOXB08w7pOM+SJ/biyaDX2og9JighJGYqaMtgKa6mruu004sqQhbLgdO158u+/x4Pf+p6++2Mf4p0f/lkGpUj/2r1tDszUifgkf2nCYZRO6PVys2kh+LFroHDy8Jvs9IbnH3qc+UGksAUaYtsuZl2XzUQA10Mqz9XWC3mvvfLG9zMTtcxtvlEEKIoC6kghhgM/eoTDheeuT38cu1CmwgGSq6/oZEbuFrYpYcsuZSSBroKNCisVK8dOYE6v6GN//x2OPP8KcWnAohrKCL1okRAymCDYjD4lgCBFVYxLNcrFWaJqksfJ1VWaCa6xqYhDo/KAZj5kSCHumsiwMNz2/vcg77hF6Of6zRiKLI8zq+DCVgIVba5lDQ0vN6HBDW/coOy+/2Py1p/+lV5lHP1eCd5Tx5CWtiZx1q2l1oQmI0KJYY8r2feDn/D+n3sv1jUyN44m/zHtczpve6vcgEvfSYRRVkHHoUnVVarsrNDyQiQ/yA0KqEExruBU8IQichrDib6w0lPm+jKqjCLQ1UFLBMhVGNNlZ9MGy2ZGB6nIirWG2inlvGPZROasUHjBYlocT/P1iyTZDx8D0mQe08X7UoEXJxaCjtAsTUvELMWTiuxErDUEX9NzljDw7Fnos7sGlmv6apWaJKmTRLKSjp/J6SqShpJuhq02GY8yLrzYTWoZuz7neH0vtAkQgqZJFuRycLlNB2XBK6x4nQtCGdKdEoQQQkIRO1fkrMK0embrbbadC+exFRk/A5t0FE0esCIkx0MS8mVtKm1Y+siuooClGun3gOwZcvmgSediSkoEcdaN3YsYI2IMSq6NrCGFk2qgFuaOD5RK2H064AeRahiZKwuIgehTSLlRSAghu5qSJdckJgclp5gLYEwqsyeaBBksJpV+M2ZUDlYSQmXFIiFpwg5KYakwqYqXGEQS8mXFTElUWvPrlrBmBB6DaHwNNuIXSurjnipE+oUd3beMIsaM7BoBF0A0EAaRXh2gSkocSJY8mqmh1rEtcgMuAycxNZcYaowYHnroIVQiQ1+zUBaIKqJZmUwVkxGwEGMqE0aktpbTiyW7b72JT/+L34M5I1x7BYq0s5VplVW2SBuZaQqIsQx9TdHrgShf+G9+i+/+8Z9jak+MYdVA1s01XwvKb6p06MRvk+vFmMKihQp9NaycXGbf937Ekw8/yimtuf6uO/W+z9yP3HS9iE1IcEtTsGTnU1uBXGNN0oFEc2c9DkNOgo2XvClJFYAsd2Rd4nNmiY8nvvsD3esdtvaUpskkh2hiW6UIzp7HN2u9t8tx3Oh+ZqGNk3VjNuJ0TpPMUWFVPWshORyu8jzxd9/Qe37r14QiOfArgyFz/d6Gjv1ytsZBDA1/D6iHFUVZEgcVReGytI2AB//0M7rvx4/w8oFnKYY1V9oeZrlih3HYCHWIGDFZlihgJWnsaQYaGj57nOgLRNNrUgc0opkDmTiR0Qc0O58DazhdGO7/6AfAmTbruY3AGDM+xnT7PS53eGIjljREKUtYLPngz3+Sl77xIJwYMvCBwspICo2MBub7JKRwswNefmof7zq1AkNg5w5wLidCmkQNyOtPC/1vBbu0ncT80FTDIWVZgK/5wEfu49vPPE+oPFVMKuzBp4qQpSvwdeIplnPzLNUDqoU+x0r42V/9Be745c+nLVoLPiDGYVNi5ypi/6bWXr7ErXBF1uKOFO99lyx+5zqtXnwNhuPSQ817k2w4raNrKhrA6jrC07bVFLxXH+g5Q1n2qWvP8OgyewrL8mMH+dtH93HVDdfphz57P6dNYOc97xLKjCYakwu3KAM/xLk+KoJlxIGZ7BQuu/utCUF1NncHMWmNnnzmWX1j33NUQ6EnNjlEPlUuiiSyfjfzr7F1kzm6y5yjTuJ6nKF1tx031uPPyoJexamcsbtpzmN3zWiaChrNnw09RrBBOXrw5STXcdNewRkWsoN42bXFM7TCpmmnFXJ1GkPRc1B7jDg4NuDU4/v1mR/9BLtccfyNtyAqi8ZgxVEvrVCIwYpJtJaYo1GakrZa7mfb56S75oiphCuj/ycdxGAg2pQwQYgZoBAwEJ1h2FNu/sDPUNx+kwA4SYxJBVRWl5qDGRzpS9hmtt91GnZz/raBHRy849Of4MmvPEDPWnpSEAZVqjZGcgjH1teUyGqtEJcGDL7/Ez0y77jhc58QJGLcODigkxo4W8hRvLSdRNK9Knu99KEoWHznnZyKnkVnKYKiPoVHy6JPHTyhLPBOOBRXCLt6/Mrv/jOOlpGrPvFBoRDUR8SRyitFBZOQ5/bBzI1jazAS17aG9xM1JikiDBSR+37l8zzw+/8R6wWrmktKJTM6qnjTzOy6tp6DaBif1SfpCZMyRKvEYywUbDTYaIlBWXQ9qteP8u0/+gtO20hd/rX+wq9+kcV770l6ZBaEwFxRUqF4AonibsZrcl+G0HFsQs3tfQgp3FwHHvjLr3B1MMjKkKLspfBNjBhjUSPtfWhsmiO1nmj2ekjiesjc25H9POs4zkQXcZoEzlhN685/zW8JrRL6tkAGgf3f+h7v/Oe/0cpv+eDHZKm2ooUQUkIJOSHRA8dPgYcTB57X7/z5V5irA71hZGfRp3e6Sol4Tqnrml6R+nkNqcdxxra86ITkdegyjLf3xtFonoPu/WwsVclJz4pFCDEy8DVxfo6j4rn//o9KypCRFOUgTVgbvu9Yq5mCNG9la88/h/XZe6V8+DOf0ie/9m0ISs9ajMap1yk2yH2MLPb6PPqN73JsZ8kN938kgUS24xR2bsRlOASsa5d0D6PA0AcKl9hvQoTr98hnf/vX9Kk/+htkqcJYh4hQDWq8hZVSePdnPsb1/+0/EUwNizu4KtZQCEEMWpgMRVdgHQ3nxOrk3O1ymcudo6kmmRQibbbz7TfIle+4Rfd4x0/3HcQESHVOs2OptLWVXccbVNKDqzJyzGEivJzXa/6vJYIorjBYBY2aUECTeHOBiAaDwWCHNbutQSp4/I+/zKk//is90TN88X/4Hebvvl0keno9h6UhRV/+XYKx0vIwgeR9RDj19EFdHEIvKKVJJH0FpLBgDE1xuY06fpO/bdS52wgqeU62Dveo3f4sZ7ZzAGvpK85yGkdLpf4k1QLuLGeUMBzQK0reevEV+OlrWu0opLx6L85t7T5ISYUNVBUJgBee+bu/1yf+4QfsqAW3VHE1Dq0DhbGEuqLsFcmh84GFosSHUaJJYR3WGmLt2yQineiHRBvKTCNwnvsnkx1FaPs5ybCfFSEGUha0tfR2LXD7/R/i9ivncNftHasigiXTX2R8gpptonjMpW8tFLuxxScXUxWkQf0cXP+Zj/PIP3yfYqlijpQkZLQDKnRoHam+cyQOKoaHjnL6OHB6GeZ35XKOIzpJFzzcamHnS9pJBNossNDMqk3keCm8WShxZ5kiZ6IM64Ds6HO6UF4tI9fPwSmEHaWA9FKafPSIcagGbGHXnDaM13HZuiaSso/FmFSVoLTgYXnB0RtAZVPNbMnkzm54BlZf4gZphJEjODZ7764vUJQldV2nbGsRovfJUcxVcawxWOtYHgyYc44CS7U0ZL4/h4uBQizzA4VaoLBQR6wTRCyzc3cvI8sZ+0iSFrLGQh1YqKA/jEidkMMGWYlGCJpI+FbMusLTk0jcpV6Cby07E9SxWaYr6g+d50JGy831+9Q+0osC6nADnwpR5OSMrWqiEKoKa0uoFWq4ojbsGRqK0xULUmAj+JDCFoOqotfroWjiBmbuH9ZgxKYMZp8mu8aYjOaNI4mwOtzbOoedKIhtws+SCzaIwSBUIXBy+TS62GfJaUKthDRrtilZzhpDICUwubW8p8t/DjvbmvB/csdRIuIEFnpy3AXtFY6qCknBLo7fL+04iU6SUomLykKwcPyUctUuSXzEzr4amhSMymJukWt/STuJI1QqpjR5IgTlrl/6lNz1sY9AFXjtsSf16NEj/My998JNNwjqoVeChUUzaggWM6aLh6RMWsEmGQWJY4hiE2rdIu1kpnkfE6KhQOFQiYhV3vnJ+9i1FHll30F6fjoxXwV8N4kh/Zo+505g2my6+d8pxEFNqYpIRKxNISLRdl+iUC0PmC9LIkmmwhaOlXqIAnMrgW/9h//Cnvfcoe/9zS8Ke3YgIWKnFzvoHGH3mC9hy6Xd6no4Cl2K8N0v/x07hhHjEkbftPtgErm/L0VL4J9mZ4scXg7WzWBev6rMaCSSjIJDQjxEgBBTpQgM9bDiuW9/j9Nzhvf95heR/qynY4uYgqWEQ0egRn/wx3/G8itv0Ksi/YwirYSaoiiQwmJwBAIISCEESUoHRhI/OaoSMi3CNLXqtXEwxvul5rPTUfSjy6dWlRR+NkkE2oSINZbSWKxVrrrlJpxLXHkNEU+kMI5MgMMI1DGiJreI7KiMOS5w+TiKE4ji5BMjk8t1vocYUAuOAHt3sffu27j6NBw98HxCcCVlM0Pqv9J6BiGhyH1XMBxUGNvj5f3PcPNtNyTeqEbUjNRRtmrY+ZJ2EoGcCGGy2DOplm+MyJ4doJHrr/qoXC+QHr4Irkx3NwoxhERQ7YaMfMA6Cxow1rY6W6vIwjozArWlzFlDqCO2SJl4AcWg7LntJmE5Uhs0GLBh/Ho1nW3TwTbE7+4lXUujGfJMPdfcDiih9og1CV2Jo0HaGJMrviTuj6KItbhcNaYMwptPP8eX/9d/o1/8vd9hqYSFW28SKYupM4ENRRrW8yTPMMxyxjZl+6s6NiVresSEyEfAe17+zg90IRrmraMmiQD3nUvVIpzDZBkh9WHq8b9dGolpgU3b1VQ7W4mcSUdxvW01UjjaWS/x2zJCbiz9ouC1gy+wNGd43xtH4PqrU3hSJgbRKddkrDlchCPczMdhmmMUI0QDPnLsBz/Rb/7lV1hQS1lrchA1tWkRkzQnjTAYDLBFAcET0BR+DjFXfPKYLJvWFA0YKx7Q2fe0Pqkb/WiscUZijIn+ghBQaoGqELjhWtndTwqsYgxODEFjSsSIkWhSbekWtWr2xXYEa8wMiQuPJGmoUPOZ3/p17EnPl/7lv2LRCiY73d2wM5AcRQ1oiDhrKY3w7CNPcvPnPgUuYqxpx//JBrqVklYveSex8dyEkccTJZUAACAASURBVNhzG4IRUsWU9pbasfVMI+jcGfXbrKYsfTLKomJ82a3QOtazPLuyznQywHMGX1lChJvvuoNjTz2PYhgMhsyXvcRvy72t6cSgu5d0I0r3qcSqwWuSY5FMNlYdeZvJITSEjvZVG9pDUI24KOwOhoXlyAP/9j+iO+e46xMf0pt/8TNCSXpKIqN4Q64ikuondPFPxgaUtp3o+PmMLTN54mdsoyGjuaKrHMEOQt52bO0xavq1qvFvHMUF4Znv/JBrtUSi4owQjVIRUScQAoWYXHkleSeT9+ps3KpZzuCFDvivz4lc4wgnxcYn0dUmJJn/S9K/KbrR/N7oV8aYZIl6LtU8f+xvHtD3/Xe/Lcwpwdqkaa9Awx1tiPf5/jeDnbvYEKh8HEkcPOYMX8Yn7iGm7ljzg1RHeOMET335q/r6Uwe5ugYbQ470kMAAQ65wAkSlsC7rWqbqTg1XkKitBFSMYezQZNWDurqth5wJbZqa0C2imNZzeR8BRQvLSRv48C99NvEQCWCKhGzCqM6wGUW1xg9oFMG6bMahWZGIDS4f8o8W0ABS9LA3Xi+cqji9o9T+csQNK0qxiCoOg2rnecgSbjiDRs+RZ56FYyfhml2oSZJ5MrH7xqPYKo7iZU5oGZXUy9SF2Xd0xn/ClFUv91ZxJpYdxYQiTjQoA++974PE0lJpoCiKFNLJwrKqOtKtOgtvYDLEs96yXWv2Z0xCoV0dWFDDXtNn7sSQ5//hIX78B/+v8sZJwutHQZNmVspWHHcQJy/HrPZxIRGAVfvuHmNMveZLTx7QQz98VHcEg9YhoyhpzZFkUcr7HqthfpY2WclnvWUuxGsj57AZFoUMd6RXS67P4v9GoTQWiYo/vcLw0FF49iVt8sVCzKiuSUj6JGo5dv8vtOfdtYabqQn5q6PigxJ8HE0eG23TACf2HVQOvKRf+9f/l77x6AF2DpUFL/QClIExJYVZz/zZ9DUzD39iP21mevPyoZ22DTQQF/vsuvm6tKCzq/uKzkAza8zZHoeSrQpJN+2kcLBrgZ/7xc8x1EBZlomjrilSSFtQYZQdb4sC0UC/UnjxVSUGZFpDGYt2bQ1M9zJ3ErftvNt6jvfdd8iJaiVxAZGE+uWZu5UL0/zGxJwLS3SGSiKD6PEmzS57x1YY7n+Fb/zL/1P/+n//A33j0ScV78HCEI/P4aOW1NzpPFpHsc2ySW/NDFRJPudmT0PXuhXN7prjGx2jgPdw/BQHHnqUA48+QWlSgCFkclRbxShLeZxJPdOzdcCaZbqhvwth5+rIrmUqoNkhnJzwdM/fOZcqSIVAgWHn3ALV8gqP/eMPIaQohzUp8WJs+93KLmMHfIYX4TxaU42kEAMxYI0gVhJ9Jelqpez7oBx6fJ/+zR/8J775H/4LuwfKgk9OocrmUhzOyDJZrcvNbXiMKZnZpgmlNdjCceX112Fvvy3hhBeoTV8u1nSxBkZVcoUsYQO3/ux7gaR/Wdd15oiatj+JTVa7sQwGA0Shp4YjL/8UkLZQQ7vdBr3t3N+L6FE6b3bph5u37cLatBlu9wer+NJSSomu1ClkIukBdMa+7R1ldzBRgSqmrF1p+EghUqpQmBLBMl9DaYXdtQEviAUhYk3EzJhjdfmrk2PXJKfovHU0TYfW+cr4T+mzapKQOLWiZRUpveLIFWisaZHEMS7PZh7mJUTsnSWofS7WlKvshq0ntULruk4DmqTwto02aSfWIdXFDIkdY5pwJYlKQUO70NXt8GIZ3ZqqNdEHjEnY/LAaIq5IaE9MCT3UsHB6yDVmjvnlgKk982WfYV2NOdYXYjLRvV9GxyW+yFEHFeG0r7iiX+Z690y5Kdt2pjatX0tFuz0szosWVv2gxqrirGnjzEp6dnu9HoPhkF6vgOw0Dk+chBAwvWLd/W4F23YSt+3sbSovpnGPYotU3XbPe1j5yQH6xrZcnRgjsSHZvF2H20UQpdl34gtZwCG4kI7Po6hPVRXcEL77n/+Ce4/cr3t+/j7p9S1IK9STNzji/U3dN1MctM2wSf3OSQLNxA5l8jh9wEbDa/sOshiFHbZHrD3R5BDoRIguyEjqQybu/ZmIZbeHtc79v9ASL+uKeZ9FA14dCs7PROe35hrbfP6FSXSHOgaiD8Sq4uQbR/AvvqLuHbemu9rIdnR4ju32WN02LwpKm0LwqRwdIuCVvu2NzqOOMFAe+8M/0SPPPE/fg5A4hqdPn6bs9/Kzl0468YwvnKPYfV5UIISIWkN0Bi2FG991ZwP9jpz4bTtra+TXmm5CM5WJnoPdO9h17V7iyiF61oDXxDs10iKKdV1TlmXOgnfEKvDC0we4vgn5wPQQzUXx8Lw9th1u3rZzMxk9S6sakwB9x3vu+1mWqkErQQO0nJC3y1Y5iNkaropMih1bg9qUHVmqYTEYHv67b/HDf//HygqwFMAn5pHOcBCzD9pBd0YhkrVCw5thnd2u8ki77q21Bg6/xbOPP4GsVOiwxpLrm+uaaRltmG9aaPVMOH3nwgk833aux7TWtZEcvheR9p5MhvNj/t97z2AwQH2gNI6FuTlsUJ7+4cO57IdBY2yTtsYE0rO1CQ+yqklcULPOgQixrgFJeqtVhDePwWvH9Kv/y/+hR586yM5gcSFCSM/c4q6dYyhiYxdDu2nMWotHGUjktAlcfetNLTflYroHl6q1k8jc3NUKvpl29Uve8+EPsWyFlRAJKFI4lFSnHvI8pK4prKMeVrgIxw+/CW8c0VRGLI73pe2OtwYfEbaRxG3bJOs6P2mWlVENE+nffIP0di9qdXwwkroRybDW+e0qZ2bNNoNLiDhNme4iiTMZVEESiuiTGjsFwk61DJ97lSd+/w91pRTu+xf/XFgwhE5hUMlB6PY6tDtsfsxR+E07wc72O/vpOoLS+X1sVQW88syjj6kZenaUfaT2gBKzo0hUclnhlOBtZMSpZDWPrrHuqZ/LoL2eCPe5dtVnM1UZO5Y1Tm0M0epem64A+SSnrr2fo2UCiZfY6/cJIbC8stLWKD7yymtw/AQsWMyu+Xa1SQT24nGbJkxHsXDjXEYQHbx5gr/8V/9O93jH7uUhzjikqZ5hLXWMDJeXKDtlCaUbtpdRYtzbYY2Y9mRpPu9TgYFb3nM3O3oRe/01SZNFzMV7Ty41a+SKct01MRYlIChX3Xcvg7/9GnMIcRiJxqIKMabCAcYYjDEMg6dnHYXrcWJYc+q1QyzefPVYB6HkprrFvPttJHHbzsma56Wh2aySdhGF+T57b7mRypJLHaVw84VKRmjMkMqmNgOJRwkCFBZxqQOxeRDygyF9NSwMlVPPv8rwpcM8+O//UAmxKf883unrxPu0z+fx9NfafCsGFSN44bnHnqLQxAsTkcR/a7JqcyazkaT11ia/NM6hWRtJ3KwM6MltXizZzRs59rWujUhTJTz/1yRBibSZztZaquBZXl7Ge09ZlvR6PUyMlCFy8sBz+uLTBxSfdP1CyMSrKWgiXGRjnBFim3AjKbx8/BQP/tGf6d5YsmPFs4uCnkqSq5GUiOCMpSzLNTd9oRDFJhEJoCxLKiJ7bruRPbfflESasysbL647cUma5kl8MykK+apGn4m6110lXLmTuauvojawXA8RkyJEomnCVlUVzlhEYTgYMOcsLz61L0WK4vhEtE34G/vh8rZtJ3Hbztkmw6qrvgq852MfZUUgZPkBY8x56ySnDc6NQ9PUYwVGYrkiyUE0EBstxaDYCPiAQyiKIg1OKHMKvcqz8tM3efi/fkmpIhIadDASW12SMHFgnYvDlM/ndNJMhYvan3TEpteYer63nnle/TMvaDEMFCR9y4QSCs6YpCPX3Vb3ujZyLUx33s76NC6y0PNmHct66xpS9njz6i4bNGKcxZYF2MSl8t4n57IOvPnSyxx67qU8msVG4nUMvWzsQo1p3vtVk8KgkUB65lQicXkJTi7z3T/8E61fO0K5UrUi1JA0Ow1JCsgGxYbVSOGq69xpp5N2PiapXaqACvgQqETZ+a67ueFddwvGYEwS/1e2ROHP82pj5fMAkwWHnM2otAl87nd+g/t+/Z9QlS4l40kqJRtjJCC4XkLoRQRjINSenz7zHAx8Hsh8S98IaMp9ibKaD36Z2tY4y217e2xajycCBnwh6EIPeo7aJ9bI2znwr5VU0XTsTcWXVsiYlMySRIp1zKksA8xVyhXBwEqEChgGTIxoI8o7TQftbbaGe5OLmANZbnvo2WlKXITdpofWHh8DasaRu9ZExmQ+Gh24zbyHFxOXbD07F8d1jD+njOn3rRUe7aK3TZstomAHntJrGrgqP0qmuQiuZ8yDq3Nu9Lw1vGRJVZoMuUZ4sLAUuJaSYmVIIYYojJXYnOYUzrK3k6c4WRu40RUNziBzJYhnCQ8ZNT2bhKdtW8M6bA0DI3pNaVnuGegbTlvAOobDYZJdKorUr2nKoJdcpas0QjHwQAEhYqUp3UjqI6GTvfc2nuMFsm1O4radk01DEWEkA6Mh8fTcLTfIca3UqrJYWKwqMYTzrpWoU57ldrDqLGMlMcldPvDGCbIiqZpC/l7nLFOrUAbl1Uf2IT7qUim859e+IGIt1iSMdFBXzBVTQmJdXssmdTKzku2ccx0yDVmYWQDHK08/w54hWB8ASck6QFSFkBBfyQPe+UI8uo7m2ezjXAf/yX2eKbpkpqF1G9jGyBFvVxq11S4ib8Ydq84GEnItwpuvvs7pvsDpJdg9j6KEusYV4xIeFyIhczJBraUxGJPPN2ICcHLIY3/6Jb1iAEcOvkSJIRolGkWAIo6cRBUINnEOE+d2xjXq7je34bHay5vgOE5KQ01+HqjnujvfATdeK1f0IHUuSlIr2gIexttsdpKlLMLu668Rdquu9A07vNJ3DmtSwooaCzGV5YPcNfvA0qEjcOiI6ryKXH0l9HoJoTQjl0ljHFU2uoxt20ncts233PdFsoRHVOgXvP/+n2PPUuDxB77HTlfixKFhc7PEVqEHMvt/6GTo6uj/yaFGJMtqGGnD0VahUEEq5c2nn+OEU8r+A/qOz39GZD45ZmVRtuWfVukhnofxoXUClDEQKYbQDtaqmsIkEV5+6hmOV5HCx5Z7GUJAjLS8uFZapDMYWhll466qac6ZDb7TnPcLaWd8DFMck5nJUlOWbZ0KSa7OLEdnEoVrLSpSB1xUjj95QHd/+L1inMOURXru8jNwocOaXRTeNgOygokCJ07z8J9+RY/te4G6EhaloCaMMr5hVMPYpDYZGicraEeTdG35GxE5L4lyszREo4AvLdfdcStICq3b1OG07JAL3+IvDxubADVfGrR971UwRK66/RatH30OESHGkB6NRlw7auLyWoNF6EeFl17h2A6jV167V6gDWphOn3hxqC+8HXb5u8Hbdn6ti4hNeWY0xgzNe2561zvYe/fthL4jGiHU/rwd1kYe4Ja/1EkWCAK1YRWqE5xJ3CnVNhwtIvSNww0D9uSAlx95ile/9yNlJaSyrFlCphnkuqZEdJPKOk3bflMRQEPE2FGN8lQLODLc/4zKSkVYHiZR85iExA1CYR3G2bEqIN0klWb7jTWoq4qg2YHc6Gs9rt7FnqjSnPNGzn3aMrMqrcw6nsn/DIpVZUEch194GczI2Z/FxWsb5dvkOTYO4uRv1BFWIo/8+d/q8YMvs0scVpUQfeIA+0CBUmRqShDwBmqbJmtxok3C6ud+1TXshOo3wybLisbO70GgKg177roNnAFJPOyoMUWdL7TnftlZvvoCqSRvTtHTCAV8/Au/AL0iFVGoKpxzbQZ8Q2lwzuFUmFfLTx95kmd/8ngS4M7jg6VTuWpr+IjbSOK2bZJ1wyzdn61Ba4/0ChZuvE4YKpUTXT41YKeZrWh/VocwjUs35f/Jzw2aqEJGKBj7TzvLOjr7EMkcFWVn2WewEnjyOz/AlaVe8/H7RETByggFgU0PM3dtbD/Nbppjzw5DiAGnluef2s+8GHqFJAHiENvQaQhhdN4d3ubkjNIwfm2m2RmhimfhpJ1zuHlSH/MMt7fe+tOQrbG211nOyNqcu2n7stbiAO+V1w6+wN0nTsLexRatuxisi4I2oeb0B7zwtQf0+IGXcEtDXNHHuqRXR0i13jV6jHZoCQJCM1GTVt+0iyJOoq7Tvq+F2m74vDTxlxsHI078pwLX3H4z3HitECNqJUltwchJ3yKOxvm2ycvYpRlpdvB6775bloYD3W0stnAEjYkOlRMpsWAR8IEYIgcefpxD88J9tUI/wfHatN8tdN+2kcRtO3eTcXBilai0cxBD6jkLIfQdtnBsplLYtDDyrJneuIOYUAlvEoII6fmP2WGMGXmwEcoIRRCcpuBxEBhIRAshDIbMqWFHBVdEB8MAasboh2PHsBkn3T3fKduXdLJtiC0lCaSbNTy1RM+mmXQdKpwYCjG47DBCU05snIg/0l8cl245FzuXJJDN2vf52v96226u7UzksJvYIumad9GrEALDlQHGRxZ7c0mIWoSqqsacoPbeXQD0qkleaZza9jy9Mr8S2a2WhV6flWrIih9QLvSoQkUgECQhcpATySL0olCG9Oq2v3VRxAnbrHtuZoSbAbRfpAVyGBMyypvRqW00cZNMxz+OOezOoRqgqrBzPaoYcGXRTlhS+DllO/tQYxDmjEOHNa4KUIVM24hoCCPKwna4edu27exsLNMPkrdiHMz1oHC8+0P3Euz5k8BpbL3sxibcF/NA1BxNVwx3lFmpbUhacsgwoBhniaQaoHFYMReVH3z1AXjjmOqrh7ExwfUX+kELGnPmn4HDRxgeP4VfHowQJ2tSR6lJuNm51UGGaaLZa9lGQ/7r/b/WS7Om49m+Zu1r8reNHt+5XIsIGwqHTqKLc3Nz9FxBtbzCy/sOKh5KV45l0Db0g/OJgEyNYqdMqPE/BzXLrxym3v+CPvfwE8hyhQlKv99HsoPbDN7AWFY9ZAQvo4HnG6k+E+tOpjSHxq+57WZwCQENWWnAWnvhO4TL1BRD7FzcNnDjHPR73PORD+J7BQNfY4wdZdpr4s+LJjQxhhoXI73Kw1vHFF+n5ZqM6Bi3DJi43VS37dxMxj9KqmQEjGZ0CincKUBhuOnuO6ksBJsreuTqHj6OEixCTrZYCxGEKYP4DB3E1qnovFpeVx50GsFoye9OTFsdRq0hWCEYJRIRk0LPRYAiWmoi6gzGB64QxxN/9Gc89FdfVYYBqxEhgo5moToatscH0FmvWabjdFBtvrTr6ujaE6GGEy+/rnFlhb4tUhULk0IvwQrRCpFE4h6hhaNX4yCPEMYJYegcumkG+Wmv7jKtjt2UVxpwdc3XJGdyI68xruU6zuPk91VO4cQ2Jx3GyfNedS0613V0bVdvtz2eiXMtncX7iprAQr/PwR8/nFDsECH6sTbSNLXNHN26dyOSdQ9Hf4Jq4sRqSNywuoYTS3zr9/+T/uj/+wtKH8HmmIIPOFNgsThxGDUYNVhsvha5bUraW6NC0OV6TjrQk45hg8SOfhhvD2dqArmfaiYsqXpThRJKx43vf68kUXODc+UIhWpqo28VT+N8WYe+06QwdXrW5JzH3CrFc/tHP8Bx4xlGwYjLiVABqylaBInraozBRZivI8P9z4A1Wag79eHmPKtyXEy2dc502y6IxZZ0Bb6qwUD/9ttkSEyZwkZQH1rovwlNGWNGlSPO0mYR/6cuG8cTUiaXa/TakoZqB6EJmjoNYxI/TAxS1cTjp6kOH+WNHz+srFQwqHPIQqbW1T1XW+tBbp0cHyHAj7/zPRZcOeZEr0pSWW+beWBubL1Ei8njaV5NmGeak7YeirhRp3Qz1uny2tZDJJtjn1affKOI42RizyxrRIAjSqg9/Shw9CQcO8nkHRxzFDfJJv2cVfsQGVV+USBaDn73h3pVMMwPNUlOtYvKGIrf/NZYmhSMEn/CDD7orPdp25z2/UysacPNs2StTUk1heWG22+F+T6Urr0wYxI826PvebN2rhxjur8CFBbuvEnqnfO4uV6L7AJNOe00r5GGgx2Zx/Ls409AncaiiLb87ui3Bldgu5lu27nZOs+JacYIVVyvl75Yw4fv/zhLsU6OYq4iYa0d62ybzLNpAsOTg2eLrnT+71qXTze5nTW3a2RVuGvs/EziGo1lOEoSXQ3Dike++T04chJsQaxqaFBFmhkuox5trdcGrM26a7aZTig7ETGNSocPq61qvPetQ94970nEddLOJBy7kcF3mrN2Js7bOdsG0UeMJE20yd9nba9zDaYd73rXbZazuHp7CTeRHC6jDhx64il95rEndATjn0fLiLWowWLa7M8GbQ6hyhlOCgFWntyvrz32NAzqqc91VzwbVrehySz7VYfTQRQnr3ez/lit7HO1xhFttFRjZFANWQk1N991B3RoGxe6DOlWsLE+XsFkIWxVBQkwX/C+T340UYU6fbZOtDugLaKw/8mnQCySoysAMWhWirj8bdtJ3LZzN53wYzpfkvq9ps5UE4Eba7jm5hvRnqMmtoNvF5nZqPOxnsMwGfpbb5n1bBYKISEiUXMISXDG0jeOcrnm+3/xFSWAsUVOLDBojFizSRmok9e/Pbj0Vg2GOAyEyEuPP00ZwBmLKdyGz32Wk7OW47YR7t80VG/s1NYZWNdC8851nVkI4eRxTzv2tRDGadubZht1hpv9OzGYqLx44Fle2f8s47HfvE0m2sq5+C2T6zYOI6OBpdHfPHn4LXj9MP/4pa9RnK5wjCYDY8c3Y2LSvG/a5IDV/cjZmKqmsoG5rrRYC6UjzhUs3HVnCoNqCsgjkvtBulUyt20TrOWTT/5Rh/SnCOoK6Fnu/qXPSqUgNqlrNBI4kxMUAKLiVOC1Q2rEjrXFrZJ0tC2Bs22ba3kUaqPMCraLuOQRxPcsywQMgSLPzzSk0ICqEn0YhQDy6ikEMAVZmDyEM3D44hSHtMtl0onlV62fj9k2yFuDAMWIq2F34ehFm7OdFXII0ojBez81QeRsrXt02nyPnrKXpYaqwEI07HQ9quUBpbFo1qDbCMo67bcxvlyb9Tfj+DrXtP08TWB6ggO4lk0L6a5nk/d6reVmOXqzpG/a3814+1mr3N7kMU07Lp3YZ/esjTGE2hNqT1Fa6kFFUTioNaUDC6DnS7ZjyvVvJi0Cw7qiZ0vmg0Jw6o6vsIAbUUt09bUTkVaiZtY90DwZG/FZ1rfuttp2q+P/n4nnpgLG2aSHqok2sRxqdKHHilHoFW36tay6+G9vWdLL1ibRP+h0fkBOzAsolQbmrEH6jmUbGYrgGiSRKQ4ipMhXIL2qAD2DDx4r7jw9TxefbTuJ23b2pqu/TvZ77dcYk4MkCtHj7rhFdL7UsrTUJ1foiSFqxDmXdPo6jlqjObaR2dtG0ceNWFpudWc+bVsiqVJLruzZhjL88pCTPz3M8e/+QI/34dZPfiwtGMOmOoitdTpIBcTIKLvUFDzz+FO4EyvMzc1hxaTKA1PPe/Xnqbvr3qc1nMpVTs8UB2Dadtbb/1r/T+rknY3NclKa7a+HhG3UyZy2zOT6s5zTNkOTlEwVhp6eODhyHG64onMwrB7YzmWgE9Z+HjXSK0qoPPsffEivXFJ2moIiQB0CPoS2HNq085vVDtZyHttlO/d+rWu41mRlI+YzZcQgYAzWGq658zYWCmDPLpBR39A5AcSsruy0bWdnSqfy06rZbkZwEaw4AhFnIC72WQlD5us0j2ocxG4RBSMgxjJvDa8+uY8bbrk2IcYuQRXeR5y7/IOx207itm26dcciaQYSY/B1jSstFKls3Y3vegc7T9W8/uQzqTarpEzPqLRIg2UGJ3AKH6w7kIzEd9cexFcN6m3hdsnnsT6SlXLeUmajSkrAKaLFIFgrlLbgsW8/yGCx5JZ3vRO5bg+qCSlFbHvN1rJVR7Hm4NxGtfIHZfD6m/RP1dqLwuL8DjwR7+t8/NOvxUbD0Bv5fb3lZi2/Xpm6bttYtWz733TUD0bI9Fr7mXUsG3H22iNY7YmntwbJWOc8m2W6aGzi8RpCjIiR9DkosaqQEDmyb7/uuf6j5xXvWFUbvQ0hxPbzm08e0Deefo6lk0PmNUstmUBvoYcf+KlP2NgzLymhZer1Z3wiMEIKO99FWs5gu94a1/tMHcZm8hpjxDvh6ptvwPUkKdt3slMUXRdR3rZNNAG1I7kyQVAi9Hvc+ZEPsPd05PnvPNgmT6mMAAnIz6YqYVDx2sEXuMHfD4UlasSIwW4BBxG2OYnb9nZY7nRdUeRyYQCBK6+/hr033ZCkV3Jn32S7ptXOvkc9U47hRtGFye22Wo8iSTDX2iSaGxXNRGcd1ixGy3wN3/zyV5WgiNnc+dm0KyVkIWMRnn38KT104CAuprC+z2WppvG8ut9n8Q1n2bTQ/axlxqRwJs9nDc7gRjh8a73OZJvdzOtpNplkM3kOGz2GWf/Nui/N93YyZJJ2mwF2FH2KCC/sf2aCL9HSBjclVNZsOg3CE22wmRxWgX/827+nWBoyr5bCpHKPWENd16u2uVYbm4YCrtV219rupoV6zegYIoo3sPOaq9h53d5c/qhzvORJ62bte9uA2U1ZAZ9EaxAUS1MxK3Lj+9/FDR+8hyoD2d1JXNeR15BE6g889iTEpLlkxeA1vi15YReDbSOJ27ap1n1oRlpV0oabxTpAoIS9t97IjiVlED0LrgfBUwePKxwaU7YzWZrGdyhVq8I3TEGk1uuIJzJQm4QTWB/VGXOCmnUkdzRZK01MCjljDCYGXFSKOnDyjbeoXnlVy5uvEzrZ3M0WIyOH5Ey1uMaiifmLGAMeXnjiKU7XjvkQUSWJf8dxOfNp4b1JDpeYMxts13LMVHWEGMnGB+7N7JgnEavVtrqtjXEDZ3ATm8/rOrpNmKuznDI+UE3ei1moWYOYaQyUGI6fXoLlFbToIaZxEGWEjm+Go6g51CdNyK85RiBE3vzRozq/B9IeZgAAIABJREFU5JlXi4SIWiVKBMYdPFnj/Gadu6q2z7FMII2rnMKxY15j8jdBIdhQ4pRJ6gZeI0MjmBuvkyt7khPPJy5y4zxv2+ZY7udSDSxWtWkloc0Wg1FF1AKRPXfcKpyqtDZNi00RIF/VzM3NMaxTxSIjhrqq6TsHrx7SlQUnczdfg8l1uDezatjFattI4radva3xfKxqWJMDcAwE61CJ2Pk+tfcJhWuIxpmXOFp9JOK8etOb/6BuFDkDWm2tyd+iJOK+tRZrDKYKzEdDWecVfI0gY+ipQVrZhqjnqKfYQj3KXDSUDYjbySRfCwWblZwx6/s0Ox/35kLb+Wpvs7a/Vqb0NCHoRo7JRsAHZCzkOUpSOldJlmaXRhoVg5izeLVdQJaGLEqB8RHvPV5jKsnZ0USduf11UMX17O2QnDEmJaCFEDBlgRYWnBALw3Zs+W0y7QxF0nnROI+SIv95WVDqnoW5QmoneJufn6hYBD+s2iTKwlg0RMoYQQ19SLrcebq1FWzbSdy2TTOZ8kqcIGiqfrQPlnXM7V4UuXJ3qptMbIVKg0YwIzHitQbQlpsGrUbaNH2/Rl9tUk+xkT2YDK+uOrcpKCKMnMHJZZtSf0Vh8epRoFcU9IeRfd//EdQRxNLV8gLGyj1tGEmUGZ8hcbFOnk4OYhXaazEt6WLsvGR0zSZ16TbiQK8X2p0W0l5v2fP5OlNr1lOz+jWp87fRfY39l697U1Vmre21Iu8CVlKmPSQkuzp+TIkjUXplNtp2NiaSqXcKJoKKwROJgwoC7PvJo4SVAWVZUvR7qEAVPIRI0ZGAavizbTh84hib6zpLt3RSDH7SQZzWP2xWWLosS4wxVL4mWIHFRdzuK9LzvcbqW8PFOM/WKfHVtJ0u9cECBZDSzG1KniwK7Pw8LC6ycPWV1KKEPBFr9HmttTjnkkoFgvMKR46zdOS4EkJGzDe/KMLFaNtO4radm3VmbWtZGy5qfjBCuWsn7N0jy6FKzmGnwkqDKJ4rmrDWspOO31qOykb31w5qebBqBrSm1NNcNLz53Mvoy68pdQQfiQ03SxOSqKqbN4AEZeX4SS1UMFFxHW26aQPiWg7NWr+t5wittc2N7v982mY6lOsteybXeCP7Xms7x986AkFbqZixkmWbYKqNWH7eJ5ECgzGO00/s1+rUafq2aOuCN8eW1l0/O3zafxt5Hjfads7VUdQQCbUnorh+j2tuuQV601lc207hebAJsDZOvKM5WWiCm2sKB4Xlnfe+l9BzqEvh4+a5CSFQ1zVeI6JQRAiH3+DUobfajeu2k7ht23YWpp0XOSSWPqS/VUdhJpcK3S/uuQKxtq3y0a3fPAaSiYwiWTKOIK7n+HS/r/XeXUc2sF2BsXJ+7WXI6FvQJBbeoBtWlTlj2ffDhxMRWkwqGt+o63b2uaHyfTL2Nv5FAQwrR09QmlQysLm2USLaqYE2icSs5RxNQ7EmX9PQtWn/n6lDuOqYzvF1Nk7pRhzIWee53vZmnucGagyP7yexsU6+eZQOYytneOb7fgYag7OvRftoQ1DM/8/em/ZKjqx3fr8ngsw8S21d1UtV9Xb77qt0NdKMNJoFMx4J9vgT2N/QL/zKgAYGjIFhA9bYs8ADGZLu1d379l7rqaqzZJIRj19EBLckmcxzqvoslf8Gu/KQwWAwSEY88X+2ooQCOHL833/1v7Gfz5nNZhRFUaUQtNb23uvY3130BT6eItxV35bUavcpz37ouGqIWG6Mwann7ofvxZQzrvp+W7bCxD+6gssWZ0TITWQ6ubUqzVUcX1US0xg0XPd/+D2OjI/RKeLcFMdrgCwziA/pI188fMLhwyehglLJXguLxK2QuMUZMTrWaftYGCxDmJtqglLP93/8o0ql6b3HachQ0FQ3r0wiPYJZF0OTxjo2a6yebltM1KZ3yxiNauzM1pOZDwNRebzgwe8+gU++0KMvPk+6tnq2TazPhoGiW3NPdU04ePBopb5KeB+4x5W6JwpSmwpDQ8ebz2pTIe4smCIYTz3/rMenMlrdfMdhZzj/4NHjKk6mELeXKKDEdU2o0BiEjEf/38+Uv/ul7pWCP1rgioJMDFmWVTnZuznQ++6vb5HW92/f+9JXRxdTxo51z9uIkGcZGOFwccLNt+6ETrY2OI1t8WqRZMDGnyk1ZNjfvyASJHwId9+isHGstqaOc2vC+xrMpTzGKR//8tf8/pe/hrL+nl4HbL2bt3h5GJh4yhhXquXtDIESyQz333uXXyeWxIVVuaoi1iJOK+sPTVLQAPoEncord8JEMUVAbMJE9bBAsqhcgYvCbu4My7Ik25mRKWRq+Lu//o88uTHTP73zF5LN53W/iKx4PW+Kir0oPc8ePwnqPgGbvEATw9W5wlTBZN3xTQS6s0ziLxursRbje9SzD9oTxZijj6w5PtXJQuK7Ub3Xvn7HUwD3JtN4+PxFJFl8ZBrTBWOrVmiuzSCxioUv2cHCwvOf/urfc3Ph2Sk8s/kulL62E1aDVYva4Lgi0hZyq/tLf3fuvVmm+X2sBNhOAqjX1jnd3+17EZoZV9a++/H7XywWmHzGfG+H23ffARRczJXeekE4U19vMYwqmHZcAFXe/NJcFK1Qz3D7puzevqksDzAxbJlzrooyIQrGCBbh8998zMP9LH5g8V15DZ7nxV3qdNSWMCofbHHBUa3sWipUBQOHvsD58EFaa8mjPWIK0ZKYONG28DfVIuS0gsy648lZoKv2qtorUgu8Uc3mvSezFj1aMF96ssMlWbZTzezqPa1wMI1voC2osLKKXjnkATEUi2VoS2YrtXefd/NUwetlMHunraN5zqsYD856T6c93n0OU9nGpoBoWD3f+CSZSOXdGViUVNmmd7kKD2RZHip3hr2lIy8c1imydPiirO+nKajJtP7epMyrWFiM1oklszMwhqX3+JSqw5porNkq3P79GggYXwe074+ODWLLWbG5Asks7M1Zlg5fuiruqc0zxJoq5m0uwLJElkUc9CsK/crj/JnEOIZVz63vIVMLBbZ77PV4ThcWMvRHfK4CZNKw3bCNdYkJI+Wtd++JYlS8I8ssxbIIzp3W4NTFnM3R4SIJTLHOJimp1AN6MpI3Yqp4fNXE2mjkEFdXTQxNtqenXJnqUINJExVSsZ6i4Qouem6Lgi09RixPfvspJzMLH3+hvHdHyCzexvSEEmxhUqem9z9ZlyWj6ZQc1qVjqZHJNMcrTx895jZSBYbGGEQUK4ZV4mzaJNsUhJvofr5D9Q0JeCv1TWDYhmz0pqAvSskUYaKKUbh6crcg0FhITLi/1r6OSraZecQLwX5KNWQs6lRlvFIcnUDpQg7blL85kWy8HJYgZKAgMIY/+7VmS8eOZIg6xChWBC+KouTOhOxLldOKxxvtsImEtjb6oloYaDUAxD7Qen9j0WTaxep6UlxFX5cl9kMz7uIK68vqc1OB0nvEWpbeI7MMc+8d8QRhQ+MzkVBB611J4qNli7MhzAMrcRKltTSq42kCWVojGWAm/PDP/yk//+X/gvUONKRLXeBDZAgJoaRc4cgyYYaBoxPY3fta7/I8cWGYxNfDT+g1gvT+XC1jBW7crLKVpPADsyyvBuJWSJEms9xTcXNSGbJTSvtOixaTMxCSo9nergpQNKh9c6fsLDy//A//GbDBsJpOFpcN6LLao6+x82QRVCbNSTPaq62Lgzj12MvCkLp/jFk7i4B4FpyVSR2rZwqTmKDdv4WWQ0aGwJODEPqjgaSKOyuUIAQaVRDLv/93/44dySqzEQi2h9XaJS0ulEl2xS8LL+t5daEieCM4A+9940NQxViLoohdP71utWMvB9L80feYG/tbZY1w/9vfxGUZToXMzvDes1wukcwiCplIsKk1wcuZgwOtVhmvAc5fSEyCft/+sQe+xZWA9yGX5vWb1yhFQxgcCarQZkicIdvAMW/PZvl1gmL3d8fSoVXPWN1dwbSvfc06jFc++fVv4fCQtO7NxEbVoFZL5NZqmfanscLmClTxww6PsElAbKiX+/qoD6l81zu5ycp2LUM2UZdOUTsPlX+ZQtppr7H23LhtUk/f/oSm13T3nV05N9nMPX4SV06TumQjJEc08eA++1LnWY4ByrLs9UBundv8jgcWWxWDSEeBFAID9NY11teVqUXnuxyKr1qdNzjOaNB2GOHdb3wA0VXC9QTCb05pKRTRdio7RwggQvbuPTkxSjEzLHyJqjKzGVoG28TmwkadRx88ZEUFc4Vx/kJiRDXpSc/OhiCpKydscZmR2Ib3PvoQbwQ7y4JXmUCe58D0CaCJKexhnx3TWeyWNrWLsiLMjGA9lE+fKaUf/iAbzEt3fy9SRccnmotp2YINtbu7b4zZG9o/RTCcgpclpJ1FADxt21/WPa3b10Ry7FJp2ylaheXRcazkLK0faCuQDIcfffoF7vAEV5Ts7u62cjM3hT1G3pF1+6d+W+sE6LH9Q8d66zRCqZ7CwOzaXnAIi85qK74SjdXUdvq6GPDFEnbnzN66xbW7byJZjohEG9tYJprpiIJxyvOnB6CKx43UfHVw7jaJvQ5fUh8bOqdRbItLDOcc1gv7b9xkqSEVe+6o1FViV3PnQhywqxlqGkZztjI+AaXzpwqefVi5loaE8zlK8eKYzEOK3laWPgjLdG6zuoWGOKl17tzmxCQCxdEJuRfEh7YbBOc9GKJTTX/7VrKGTLinTY6l412bs00hG+a3XsGGqQ+77VxnM5nucch2USbUMXT9kFs2/a2VoJielgEyD35ZBO/mFNdNQaV35F3BWMtC2x3iACfkhWcvmzFXKIoCm2c0Aw6vvv+18X8dbN8OpyBc4508dk7z+n1mFpvW3akAJ4qZ5THlpY/3pbWTULfK7SR2IWBmM1h6it2MF66k8AU3bEi1qM4xy3J8GYRBK0KuwvL5IcjrESMRLgiTqOl/Wv/dtL+H/pXX60P4Xl1Ya8Ea9m7dQOc5SPAIzvN8JU6gyHqPyCmM3tBkMVRP97yhv8cwJIyq92QePvmHXwVjfQWnDhsFxErwg8kvfDOc7OHBMzLWs3+nYRPHzpl6vbMwdGdl9vradFbWcco1ho6f5jyI9qYpjZ+0A01bD4vD41O3fR0kGTcuPf/l//xrWJbBe38g33RTrTzG/PVeq4fJ6/bNJt9onwA5dJ2h+lQVk2csRdm9dSPYJMaFi+tbHMB24roo0KibVMeP/sWf8cN/9mf4eUbpglNflgUOLdjFhxE1Q3n05YPX6hleCCFxCC3vu4bk2I6pvsWlh8D87TssjbJURxnpfT+QcWTIBrFvAB/Lpdt37pA6LB1vZtEYy/iwzlayKexmKnz6i1/C4RH44JHtgcJ31Bl99Wl9yND5LhSePnwArsRK7S0awjxYHP0ZU7pBjpv3t4kw2TqnkzFkbEsCz7rtpTheTGxTs11T2jqlT0TaNp2I1NtYXzawEki7cb4QmOpnj58M1jnYL7B+HowC4vKLB/D4CXPnmdsMV5RgQhgRlWR9Z0geyy3hVk21CXYll/pQu1ZsFDuC3Fg9Y89l3dyykpkoERtG4O03hU47UptfG9rpMiF9vzje/dF3uf2HP+BEHaVEcwFXz0GqCjHzyue//bjBbF19nKuQONbFzXAfvbZYW1wdeAfv3pPSgMlspfJJ+ZunYIg93IQVGDp/CssBw8zkIIsokGVhUp0Viv/sS01x7Lx6rOm5f2GjCefo4DkSVfc+tdHU8RvH2jpl/8vGuutswvy9jG2sDVPaO+V+x4TvoXP6fqdFS4j1G863Khw+OQBtmwwMGxBsiGXJ0eOnuvjtJ7qLJVOJQbKFMk2wTWFqzeKpiU0EvCn19JmLTL32UJ+ndHwuN3B9HwS8c3HuGriWdP7d4nwx32H33pvCvTuiezPUGpZl0YpxC2BFsQqPPv+cKif0a0BXnbtNYoWGfUqKu6XeIwjLo2NUhfm1/eg7Jt1TtrjMMAZmMLt5Df/weVgduODdLDZOmEllGp9383dC38TtG7ZGfWW7+7RzvDlIpON9E0YVj62nfhEZPO5VMcCOF7765e+4+90PkUwwIigOi205dK2YMq3cRoyf6D044eDBI26IRb3DWKnjw030dO6aQo5Nsl0BeN15Yd95f8DrhdLhYzC01K0Ujd33TscXEyvxFxusIEDXhlPib4fWZRptMgS7VE4WeDGYfE6I2ddvkzi0Hvcxa5LQ+CYUEMvf/sf/zM0jT66CEa3Sm4Xvtr6Nrnq3KTyuNmSajWDfd9oSBntUyhWLq9rqge553Ws3/07lXFmiM8tH3/tOHLccdm5j7yoeqZkYYWuLeOFgUHHI7gwMfPePfsLn/+G/YkpFVPFxzLfWUjpHnuVYdfDiEHavnXfjvxacK5MYBpz0x+oHbozhxcPHSOHqOG8No+zXyAv9yqIsihArMRPe+cb7LFxZRb3Psqx3kq5S9E3EWdieV8GgNessvCPPc2ypfPab34EaDIJ6FwTjWE4JAbNX1q1JqEjBw+O2PDqG54dYbU/ElXB6ituaKiBucuyq4rT3/DL6qvm8Q3gaheMF5fGiygIiTeFlBEng7GXS1MOLFxw9fsrxk2doUYb0j2j1DbeQAlVvwCZ2r31avKrnkc9DtpW7H7wXPjxrcWUZCf/4ra1UOlGdv8UrR7CYMDhjQR0f/uD7nPiSfL67svAAEBcWQzx6om2D8auL82cSExsoSUDU2uNPBffiWPM396XQEnA4n8KmCBNilW5xwZHleUiYLh52ckxmcQtHZixlWWKyeqCtWYlVj+fuYD5mHN/HlK1DCgjcvU7fZNcqY/q9syFOHgZUPTvZjBdHi5DztYQss7XZS988lViJ5iAmtTbaHx3DghgrL/ZfYn9Y7cPmvfW1d3CyNMPnDJ3XpyK9rDDKykSS0MeCJVVwKr8u40sfs9iss3rPaTBjTUYyeUAvHZJFfmsDtWt1nyQ1skkNCy/V8ZJsUcLCk4sN47iP7RiwKa7uTep2d5Hua9Xm0rfJWd9g+XvYx2Y9fczgENOQWm7of44JRVFwIoru5KQkwdZm499tt/7emrf4ulC9pbOM7PoeSwulU3KoTCe890G7o5BjOHz4hP1vv18PuFcY5y5mmYEWqPeg8OjTL4IQEXMlWmODzZbTdWPQFpcGQZKZ37qOzDIWZYEKLZvE09oSTcWQzVHz7yl1j9XTty/LMpxzLBYLdFnCbz/R448/UxwhLVTE4KI17jQmMJASCy+evVBeHKtF0IZauc87fEztPGYvN7Z/iir7ImGszZvUMVZvt/51fdUsM1Rnn/OGdM4TBQ6e6+LghdJ8F6aEkVkzA/oHT3TP5MzFBgZc4taX+agpUDeqHVrIrfuWVtq6Yfl1569d4ABkFrMzY3ZjP2hEGpNSsk3sTnGvAfl0qRCcU8JTsjeuUWQWp34gI5VHgKOnz0YG5auFcxcSm2FvqiWiBg9PPPzuZ7+A4yVuWeCcw6tHUayVblrTLS4h1PuwUsgMH33vOzhVdnfbVP8KM9eZYMYm2G6ZdQP/kIDY3Tc0ea87rwkrEpxWshyxwmye8+z3n/PFz38ddMtlexUkdD5YiRNVo9rAwMPxk2ecHDwPhGNNILbZQiOBdZpwb81jU/d369t04p4ihL6sbeh+1rW5zzNcJ3g3D+3f9FhXULTdcEdGwHkOvnzI0y8fhMV2OnfCBCfUC/YqMpyPcQCd8tu//wey0mM1xDxVAW9Ducz0L/KagmL6lof6vPotHlmhFde0fUAwPw2GzitRClFuf/i+YIHcomUJsBLCK+mYt9PWxYF6yE20+3YO3r0rOs9YuBLJbEg52SrvoSx48fRJyIn+GuDchcQUy7YKPeB82AiD29//v38DJ0vNsgxrLUZMMOVPRlqvgSR/lSFiwksgkN25LceLkyrGWqL667Ljw+sQIzFWfuqksa6+dYLmEEsxMxZ1DodyfHzMb//253z2D1FIjJOsQi8jAW0mPgiIQQ2oxwtYlqMs1lgbu/vHBLa+fVP7dp3gtq6uVykw9rXhrPe3bn/z2FB9TVRheBr7KxW4BA/jLMsoj04oDo8BaecGX4PeeIcSx9+DZ3z+u99jCx9YbBGcNOr1OpijeSwFXrfspt/WpvWtq3+oLhWQzFIa4NZ1Cl9C3Kc0FrmwnacuMAQQNAymOzssQhyy4bFA4elXD1+bZ3r+NokRjXwAhMfmOfrqC8qjw/DwUtw8AcTUM+cWlx6eaDdlhZ29XTgKk1Nms1OlPuqyK839jT/W2jU2B4ZNx4Ok1h2bCiWyCqpKNp8jBsrnR8jcwOOn8M6tuuzQddK/3rfUeYePD8hKg8Q8tV3nlSaD09u2gf3pvtb13RBSuXW2iNVEfMaMLKdB89rddgyVTeizhRuqGxpMGqvXGbr20H4ba/GNp6OqWGNYHh1z4mo7Rd8I+jwGEVllxOIi/eEXX6pfFuSYMBRnFt9w01BVJKl7+gJri7RMKtL1Ws98jZezDpy3LmvLJkjn9Z3/4bc+Ymfugm2xzepyXquYpH2NNtVz3+I8YUywK83zPDCJWcbNt+5gDh/ij8voIJscJ8K7YK3h848/4Y8wr4WgeO5iVmUHnWw5TBYeyLLk4Wef68xkYKzossSIhIFNNcTW235hVwAGYy1gIM949zvf4u2P3g8TjverAavjR/mqA6o3BcQ+J5i+smNl+o6rhAFoPp9TLgtcUWILT1Z4Pvn5L9rpGhrukK3dkTw0xiDGhG9nUfDo08/5+B9+FQzjOwLiVAFt6rF15ceYtyls3ib1vywWsXtfU5nETZnGKZgqyDff03Qkm+WUZcmzLx9y8OXD8LJI9DzuqbbvSqmsb76AzvP0q4fMxCKqlN4F4ch58CHG6cq73ne9U/TRJuesK9u3WGouosbGmMLArQ/e5c5HH4CAwYQxy7l+4bDZLrbT10WAQhAQvUIWwuD80Z/+KSfLRXiGJmQPMvGdT7+/+vyLoPF8DYTEi8EkKsyivBpoesBazMmC3Cs4pzZD0BQzTlBrpmhLtrgMUBMYqjzjaQ52N8PPMzhcAGGysQRhJ6RIqtVrnvDRNr2P+1iuhozVO3Fowwu5WUez6Ngk12XYVibwtL9zrrNQLJfMswwXB50q0HVZopkNZ7nQGGdDoO08sulGwGk0vI4ezmQ5crRkVw3Wp7Zoux2pfZ3O0oHZa0gQ9j39PYTTTO7rAn6/zDH6NCxTX2e16hkIWp5eLJOOSZuxWKkrsVkrzKSpW+E15mRuMLZ4cmMwJwtmlUt8EBQ92gqz1NRCJ/7ealjAOythEYIPNoneIC9OwrcnglhD6X0wn1Cl9L7tlJhYuCjApvtOXvl9fdNoEqqm7qNGfxnx1fE+J56K2ev0r1QOJiaE+fE1a+w0LE4l2nOqhsw13edTWDjYy3i+BzjI0iItXcM02tSUyVuDEVucE5Toe+IdmdgwwVjFzzKyfI4/Ce+wNRneObIsw2tYCO3PduB4CXv5ed/GK8e5M4mID1u0rq8m4sLz2W8+Rn0JzqXoApUNIyKnUkVucYGhcOcb73HrvfscLZfYPHyAQ6FC1jF8L7VpG7AgU9pTT+IacoT6YPyf5znqPU++fAA2azDspp6g0gTYdO9v9k9RYBQyR8smrHHxjbIYnaV/x9jBqczsq2Dn1rV16rWm3N8mdZ3l+JAnn/celiVuWYQxNMU+nCihBGZSKDWNtwJHC7783ceVOr0alrV2cBlj4Zp9c54YS9+nhOgDKcqC9x51PsaCBGeFnXdus3vndtCA9Xi7+sbWFBi3uDioM1sF5vDGm3dwKf+20zowfHpnVfELB89fvAY84kVhEqH6cKSxyvrVL37BbBaClYpJeT/rU3pXoVtcarz74QdcP1jyM5SaRIksYS8hI4OT0ToVIjTjzPUfHzqvySCOnTdWR2iAIlbwIrWKrnB89btP4NEB5u5NHCG6BsQg8p36RKQ9OZ2cxMJCm0NdxYqn+JQ2r8GQGrZ13ZHzxkbeleewaePWYMj+b1156ZQdY0BPx1hGmPUZSFbsGn1g9pxTcHVulpU7a+zosgfJbMEJWCPolw90efCCPbW9153ctgl2n81zm3V03/spbUiYGp+zKAqsGMT7iuFXI4gR1MCtu++InaUGDNdz/mzMFl0I3TiVQZ64cf8uJeE5J/LYO09RlohYrLXs5jPWDK1XBhdHSIyohUTDoy++Ij8uYblkYQ1z72lG0J5ieL3FBYdSp8+xhmu3bggzJZvN1B+71uga7JrqT9oorYF5SCW6DuuEmq4Ke53tWl9dXTVzQp7nlSd3UIk55vMZN7Kc4uETzd++KWK1CjuSAhADYKRSGcaKAcUVxWhwa2WVGV0nTH9d+09b7mWhl1GakBau65gxVV2+7rw+wbO9r123l8TkaZX1ozIhSK+NQpgQG0JW931oXiAKc7mJjile+fgffsV1O8O4miNbEeD6GjiCTYS806CvfeH2BBWtUiYm8wwBjLUxlE+wuSy9w3tPqYo3Obxxk+szWem/hO0MdcERQ4alsRMBbr8hPjdaFCCuRARMZkPmIkCdp1ie4B88wnzjnfNs/deCCyEkVtki4nPCKzgwpWdvZwfu3JF57lvxPlJ+5y0uN1QaLIyC7MzAWEp8zAHrsaza9AntrBFTBMS+Ms2ifSqws6oAh4SvapJyLjDl1oSVqwdflIgr+e3f/C3f+f43UDxIcOhKTKIS598V6i8Fh+1vy6qw+/KFwyHV3bq61h07L4y1qWszWJ0zcO4qU2kG9q8Kmf37Yr3UJhm+sZCocpdbg1iNY2gj93Kzbc3r13R1OOY8JrNVwU9+/itulrQDZA94AHfvaxNBsCvY9bGJQ/mVpyB9QuHbqH3F0j0554Ktp3NYCXaRNs+Q3HL33Xcgs5Ane+D29ySNa6ze2OQmbvEKIZWdLjGKiod5Tn5tD7c4xDaG0ib7bD0UR0fMXwMm8eIsdJofTbQo3clyliedxoqBAAAgAElEQVQLkkceUEkLY2rGLS4PROokBd45mGWQG07KosoU4oXKO3LIG7Hvd/s6p2MZx+pZd411AmKCtcGT2zmHyULmiuvZHLsooQRLMoiPaaLWtLHrWYr028xNaf9Q2b77GNo35Zyx61wWnNrkYM3+bt8M9WEzaHqC98EjUyU4Z4WFRk/BVH6g3caY4LUM4EAPT5DCrYSw6bZv6DsY+hamvlNj/TF2/aEFTMszvFEmz3Osta088ktXclgsQpaVxD6NPPo1h7e4CEgPKcbsvXXnDkt1VfYgp75afFsx7Oaz0e/oKuHiCIkJEiWBwxfkquzOcjg+DHZWDeYkWFu9Bk/oiqPF9IuguQUrvHn/LqXUTzhls0iG5kbbeXP7UKlWJ7SjO2mlyWJIMO2eO3RsLFRPYj1SPLUsywI7rp7i8JhnXz6EoyUcnhBcXHw16Yfza8aoqdYzed68SPWzz/mme/9D99mXVWQlPFFHMG1uXcHgNIJh9zwRqdpy2q2vzk3QPa/63XP/3XOm9mtf/44tQprvu43aF0eIjRg0Nt0HV3t6ruQSVg0OUqKgnsMvH8GDJ+xaSy4GOyLwwfr3f+i86vLx++srG9hQA2p67mmd130dnr55bnoWFsFosEksy7JiMFOoqdLArbffrDvMxPmoEjYaW2pSZ/d29rogkGC2U5vhKG/fvxe1NRoWR2KCA5NRnHO4kyVHB8/Pt91fE85dSExhBTwx0Ur6co5OlGWJL0r49DN99PkXIVlzk/Ldrs8uPSotjSpiTAhvkxs++sF3Q7iXnke8KWPzKhmq0zAfzb9tngeVVtxXliU7+Yzd2RyWJXzyhZ48eqq6LEL5yBIKDOcuz1atSMYE3ZfRP1OfydRrnVV4OwvOKjBuUn7d/ilM2FidxhiMMTgJguImNoKxwvq3Co9+/Xt98ZtPNHPrnVTG/l5X/mVgqN/6hOkhVN+bSGCTiML8zLJ/51YUDtdgKw1eGogxYIRbb7+JZkEwTKYXaUEvRskzw6xnnL2KOP+7bHy8xgBOQR0YS65h5fb0wSMe7Rru/PgHrQ9OKz+9LS4rRBKbKBWjgXfc+eA9Tvz/xX4MLdG1Qeyva4Bt6CnT9/cYs3EWobOv3iozh48poADxSiYGdSFscY7wxc/+gUdZyY8++O8C2+M91lqcC/nLK8cVEeo4iRnGGDyuWkhV19a6r8P+/nViUzhPQmnfvWtdaPTehzDERk5lf8/6/XfZ1d4UdEPnpiC7Pe2adG1dfaerTCym3Q8Goet0kX6rat2PPqy61EfHC2NYFkt2r1+n2JW4ski2hdq6v4pFbBknQukdubWg8Hd//f9w/UR5w2b4sgwMjICRVVtAY0zLPlZ6ylSG5b62Newt12xSq8/WC6t99ovda9mo0qiq1jrsiXNlNUb5qLo/8SXXPnhPQpDxyLZ8jQuZLV4S0rDY/BZVufOtj4KHsypgoAwB0r2RONQqjx58xXvn1vCvD+cvJEYYCKnFTEx1c/Ac4xSKkuPnLzheAK4EG5qsTa/OLS4lVoZ3r+FFyCzcuo6fZ7iimtJWkNTJp0Xz/K+TqWpixcs4mlSkqfXzX/yGw2sZHC/g2ryK2WZjTJzWNyBRoWXNSu7R02JT1nbTc1912VcJSYL5BuX7QsBMwaZev+la5WJJNsu4dusmbgewIdi1hEL1wqy6ULse733ISKEenh+x7wzXfHBk0aRibbRvioD3Kj2YN4GJQt+EgvgmOWEN2fU9uHWd7RR0ueFjaCMjMbqnahh893YoM6E48exYW61lypgFzGLJ7NDMdLVw7kKiQGRDai9njMDRETNjEe+xSwcovZbZW1wNNOLNAXDzmhQW1ehBmViOoQDaQwzX0Bi+TrAZst+bymauZDYZQbKBSuWb1545WCxcFd1fRSo2cRLMcD+JTLN/exn7use78SnX4WUIomOYkiO6N3dwjFvYxyj2oVdQ7Ll+91pDz6m2y60ZyBSqQ0Qw1lIIaGbwRusO77CI/Y1tMM1Oefbp57rrBD08xuztoAbEJcYzXrPH2xlqL+sh+6ZmDus+NjGE8RFa0hrDTCEpE0u8j9SBfUKqkbrP+l5IYwK73ziB2bU9mI9k26j6edLuLc4JKSuOxhBjogZygTffkDI3qpmCk+BUaARjTdDgOMfz58+5e9438DXg3G0SgUrnX+W7FMA5Mg3Gw5lPaftqIXHLIl4xSFCZGggT2DxnadtG76KsnYzHJvnTCBpjdmbrVNdjNlHN481/q5yxUSM8i2YX/sWRipo67l2E0pjcJgQzHmr72H0N3esmAuJUO7+uHeJpbANPg6mMaV97Nm3vafp3kzrV1O+IZBbnHIV3FPjovSlRJd02ah26gvoSNGTwyUvl+nwXnMd5f6r34mUtNjYttylS3MSk+hcRMEKpPgiJAsgFmUS3OBWaJmvp+YaB1/LclSyMstQimiIIvnQV+5i9JnGaz51JhMZHXtmJAK7AlUvyUlkePGNZ5kFAdIrPqLybt6Li5UbLbCA9e1WY5ey98Qb+5AlQq2EhMlFRfBQZZtSmCHennYjXCYhT1dgiBlHwooi0zxHAlyVGhCdfPODOe++AqdNIqXbqN5FRkuiFmVF5t6ipU/GNqenXMbSbCtoVkyTtfWdlEBPGPLYn1d/0Pt2AReyW9Z0eNT13uC6e3xCLOHbO0PHUx8nmdb63y+6utGLNNt+zcOG+Wn1g2bzh8WefEUItGtSVIbfxBpTYVPX6IDvY83d3/7rjfb+bWV9oMKKQnkVjEUdI03br9htVodFXcIipHztni68NaV4xEszcHB4rCnszbr13l5uHHvfZl2Q0gq17xQr40r0WlPAFEYUbQbJjr7uyRDQwKY8fPAy5bD1gpRIqtmzi5cfKM1TCRGaE+x992FIvr3tZpwpxY/s2rXsTpnFdPUE11hbgZllGZoRHn38ZnLrK4AndN9fWqrbglZnneat9Qzlqp9zXGEM0xqyNXWOoDZswiEPM46bbJtc5zfFmmXXnjJVZqWPFz2b1+sYY5js7ZLNZo2CnUNM+QzrHxYD3/PLvfoaIsFguEZEqvM7YfWzyHkz9jk7DSk5Fs58TUpxJT3QQsoa77787mCt7i8uDsEggDKhpsWAFrOHe977Ju9/+iAU+erYr1lqsGHxZcvDk0Xk3/2vBBRASYxOSyjlSuIeHh4HSxfDkq4c8ffCoFVwqjGMXoPlbnAmtWJcpRRKAgfe+8UF1KOWONTF+GWzOajWPdePRTZ1YqvOln8Xqm2R60VQJNiekWK8QVOuFC+qNZw8eBSHRROvFOJE3+6+Z/SMJiX1t6+6bKii16klb37EGUty57rF0zaofjVR90gqcHvcPbi8JmwiP68qITI93mIa0KQLk0PHqnaTusxSAPpnwSGYRGzKGuHaIiJaA2PwTQmYrEHj6DOvBGSXbnQWV2wALO5q3WmSSve6QYNiNm5jiS1ZlxIP4nmfjkYZE3e3LOmpifzuq68d7e+ve3VqQ7luwdbbBAlucL7zW844ImuIv+4Lb997hrffvc+JLvJEqW5FXhygsFovzbv3Xgguhbk5hG5xzYAULHD59gS09vnAcPXvOsctjIEVTe0FvcenhVMnSgO01rOIAfMn+7VutcTRNEEOT76vCWZmJdd6cPqmt0qo2Qo1Ew2rP4vkhnCxhnuHU9+ctr9TNoLMMYyz+aDjouJd+Ln5T5uYsx87St1POX9f3laA+UKwrHGxSJjzPs0sCzXeo+z41Fyrx6beu7bzH50K+O2eWB/GsenNEVgSdld6MLCIHzzXzgFcWrsBGL+luO5mgGj7tvY6VPUsZqG2fm7KbIX4jAtjgMOa9UqJw41qw39iSiZcbTZOJlJlIDDLL+fZPfiQ8LzXf38c9P6EsS0SETAylVxaHxxsJ+k2y/jLh/IXEKMFDSKbu4udqC89OYZh5WDhFigLUASbEDIunX7YO36INIwavPqSesxZcEULgZAafGXGoeq9onJRK9QSPSxP+TXZIJE9G32K4KsYBqcoBtZq7TWTXzEHdwPY40HnhWo41sf6m4OKj0FbtWxHAmoOHx2jM5Ruvu0TxKHOxUCrgQCTmLjeV7ZuP1p0ej1GF/TnLIxdC4Xja1rsNFrPpgdonDLaIJpGVMbHJEjaPrfRjZ38fTiMw9nkKr6uzKzSE92bNhQba1qpLZMX7top32HdNakes6ugAI9cURJuCT+p/q2FfEcvlGt5xb4JQozZD8nCCROEwsNBSX7b1ADtkV3RaMQozm7F0YcJs2bmmSZbG+9BhG1P7K2/njoC9EsCiy1b67hsYi5nUJ7blbZ5sCis5INkcVu2JlzCBYXUxDfMspr8MJr2KWsPSO7IsD/bQxkRmyfS+O2vf5O3EdSFQjX/R2z8sqAmZVaxg85yFKjthyRDGUlXmuWVZth2/0tcUQky1V29Kmit8J6vRxSe7zr+FLVVHEhGVk2dHzMgqD2cDsCxA6wezzd18NdCayJN3mTXkN67DLMPuzHAGxBqMtfXEO5hy5GJhmvATHXG0dnpIYT+MMUjp4csHtTNzYgF9m6xSBKxhZ3+PazdvrHwjlXCIwUtPOrOB9q5TvU4tO4RNBcSpquF159L4lzPU2ax7yv4p15hy/Wr4HGA01AhiDZJnmCwjiI71gmXdFxSX7xx+9ZC5WNT5ilExpn5/Wg4zZ2SHW9d/iXV1oaxqJZrRBSCyid6TZRkYwVmhyASuX0ebTOwWlxPdxS3EAdWgWVAvL1wJYhGxmChI+mXB86cHQbup4f2vFiB9WhtGzA4uOM6fSWysZiEKgwpPHj6qO9srogJHx8obO9vP8opAqJ93FbdN6iwicn0P9udIbjl+8Ji5zRAT2LOuLVZV54RJ5bQTz8tWp7aZu+ix3Jh3kjbLisG7ki9//zHvfP+DGFw8TfSJfIosZvx/vjNnntnVYN0ynuqwr083wVSBMdkTTnkSvWxgOrZB26agW+86dXP3eBLs296xddne+IeNOoZCPLXY3Ab722LgJJjq9LXVWstoaM2VmTK+ewBe+f1vfos1BsmyaiJ1zvWmRq1YRR2OPrGJKrpZtmse0KeeHlPzV23r7o9EhPHhzhXCwhRDbg3FcolYwwc/+T57mQcrW7OnK4jmq5PNZrAvgjUabHujltNaPCGFKtLO/92WZmrUZ18+nL+Q2Jwo0xpX4eHDh+yoiwd8SJt0eAS8UZXffp5XA61BXSTyyUouhg//4AfcPlR+d/Q32KIONBKC32rILvKq2sS4sNSnfp0uFLbLDal0TWQWncKDz77gHaeNmXL1Gkm9ZjKL0bZU0LV57DoATWEQx9o9Vq67f12/nhZntVFcV+9UYXFo/1g9p7Vh7GPEILwmJrIc2SzH5pXk2hp3241oTHRJ3+yUg4ePue5iWsjIIKb3aUjg67ufrlBHR8Dr/u6rs6+Px0IUrbUnrQJ9SzDVIAqIUV9uraVcLvD5jHe+801s7sCVmPz8p88tzoYqYxC6GrZqNgNvUIFSPZlzSDTcMZFlRHwIPdqdhxrz2WXHxXjL48pOpBb8Xjx7zo4PnnXiQ4wiXjw/1SC6xQVGg0EE4qdq8JRg4I0P73PjuePkP/1XdkoPJtgxJTbRNaoKE0M/5b8qCNW2jN0yXYwJT+uEwj4hqvu7Kbs1bfwE8F4RUTKEk2cvYFFAnlMbz9BaviaHFpvnEIe0MS/bKpzUBCGvuW9q5puqHxpNbe1fY1OY0D1vqOTa0aHb3jXFK8Glce2hMq3j0tmfTAi0//0ciwvYLKPxHoSGPVVdoLK1awpfIpDN58xmmh52ZSPVto9iZf/Tz77gVmkR57GS4cTj43WstWgMydQUflvtknEhLbErpnPvff3Q57DTFwJonRDZJ7irhDnGavTIN8Fu0ngoioJZPufICvbdt7m1b4T4fW1xhdB9nLMclh6ykPoryzKMd3hVvPfhE1suKQ3k813E1FqwFiV5yXH+QmK3I1WDnZULCbVx4XiGUBwekUtcHm8/0KsHVXwcwC0GDNx6/55khx6dWZUyypNQ2YHQI+T0CS4vi/EbO3dMEJxyXUwwpG+WEBHwgVVdOA/PnsP+7fZ5sRuaE77kFvUj4Wd0XIAd668keAwdH7vPyeroicc3OWc0NMsahnBswB9irboC3Nj1Vhi2gTLN/SsCkQimSi/XaJMRmOdixpjEviFV4cWXj3TmMmaEoO8ppI53LqQqG2nv5HYPIC0GpjKHfQzlunO6MEGqDYtPCaGM0LAgPXYF3HtL5rsSMq+UQXDY4vKits/tHFANJjGZZffGNXh2gNGQwgMXFkaZGCgdvgTm6TRFRxaxPQ248Dh/jW2DRQJCsGDnAlOUxjsF66E8WVwKQ88tpqMavKNnZEXnx+OltbA752i5oPAN3jAa5NcVhfhoFdSAGjwm+pNthnVxEKeeP1UYbZ7X/B5S5K70PRw+ehJpd0VbgTvir9iXZBZyu+ItWrGK4hHqtGqJoVL6P7EU77AvvmSvUC60tqH7HUQcF4ba071+c9ukbGK+Nm3PULsGr9+JAdkX51FEqn5uxv/ru0b3+tViQGt2zUvw2PXxfZDMEqUuoFa19RtShRR++5IxWzhyDC7G7FQNKthmfw85rkyNJ+rjdlrWvgs1stJ//eeHnkxlUzzWBO89Js8o8NidGVjw4vHqtwLiFYCkrctip1fFwPz6PqUGh62yLNtjhvNhk8TBRwyOKUMROS8uLsxbHoycFVcuyU7CBIaLAVBVyVXQZXnezdziVaHFTtUG7/Ob12BpMDszkoWdlo1wwJUgtoYJYkMhZUNsWvcY26nSmKgag5dFOH7+gv0kBKTQQem81G8imHlOJrVAdxbuvY+JHSs7Zd/Lut5Zsa5tU1isoTo3ZcBOg15VqteWel8FyEzQpTbPZZXxaC7QUHj8+8/R0uLKksxYlCAkrjCRE+5p0/2b4qz1aGMYMVHtrMCyLPDGcO/D96NwHwKxraj7t7jUWHmeAqgj392hiPOTMSbYwXsfwjEVJWUG8/hNaFIFbZiy8iLj/EXaKMqboLfg5PAIjo4xGrw6VRWLIM5z8PgJ+Oi8oHplHsLrjMRgEQfoMElJxQSKiWrnt9/EGark6g6HFx9t8+oVnGDDuR002YgpbMUQQ1a1WySEwGBY0Bg6PzFGCTZO1qKdawAmsxSurFJCffbJpxU1Y2KcyDRhV5O+c2Q7u0g+A2soNfRZxYap1pk44r6+torUmUOa/dFi4jrMWJc5WsvWJU1CakdsZx/r17/ZM26bZVfZlLkcq2/KNfoYseY5fXU22WFnYCkK1pLvzOMHZmImlRgHUEIa1BUofPHr3/DpL35JbmzFjFfCWIP5nMIq9t3run7rMvKbsoqbCnFpJLHeYLwhNxaTWUoDd967B0CxKE9V9xYXF0ptSgGE8cgQ0sN+8F61OwRU9/UYenysZVFU8TutiVEqtA6Jk9DmEE3vPHURcbFaqcQ0OZBpPZmpKpkKRwfPw8CkKe3SeTd4i7OgQ9DXO1f0ecpH3/sOJ77E5lmwhTL9Mf4uE7oOJX33U3hHNp9V93148AyKMtjqNgTE1oBkLbvXr8nu7VviBWyeBVYkCWvxHPM1he9YJ5hvWs8UAe00OG39U4XMV4UhxyQvoNawe30f9nfJdndQFwScsMDwFb1sY3wcdb4KveQePWHmICZq6VUbNzFkU7mpID0VL6tPU/+lVIbN4AFFUbD0DjezvPnuu1A45rP5lqC4qug+19yQ7+6EaBGdOUdV0eWSYrGMLFcYh110imwtBNdd5wLj3NXNNcMQvMvcYgnHXtXXUYIDo0iYILe4cvDASgi3pB+NQs3t9+7hZpaycOTW4nFVtobeOhsD/VQkZmxs8unaTXXLDp2f9nlpa+pUTBWEoa98ymJRFAUmN5Qnx/DsGYulZX77Zm1gXZ8ZmPk8A2Mo1CMmC5M/wVbLxiDaY44GQ3mtx9jVKfvTsT6bvk0EyVedmrEZHmhyeJVG2b7mdesKLG5Uc41cD6gyinTjKIa2hJ1e6ldBIhOyUMfd9+6DDeyhiXZ03jlMFAy9gonqMbEE4bHwLA9eaOZD7EA3EK07tddFNUAzbmLrPmL7g/WQts5tosrr3Hu1zZHiUMpAppa+8oa6r/f29li4E5a5IO/eDV7NaX02Fndyi8uBPjucaGpgJRzLdufh3fVtbtAAxcmChSkqSx+gx4jjcuPcmcT2SlsoFktlscQiQcXhgqpMnLJ4cQSqmKiG3uIKoyP3sDNnmUkQekSqgXxSVWsYh5fJ8mwi6KwIXNDKuAJB6JzNZuR5jihkHig9ZlFCWS+kWgOTasgWkJiRyCCqaTtsTGrTiIA4hKnM0atkBV8mpjCFU8qm41PqGPrdd35LMIsq/2qRNMuYX98HQuDnlKXIWouiFRmtqnUGI+fBQ3l0gtU6PE2y0Vt3D5vEOBzql7Hv6CzMZN/xbrzQ5gLk5OQEm2UclmU0bYllDTi3nYOuCobfGiXf3aFU37INT+9KbrP6u2mGaKLxHfS9Jhd3uFvBuQuJXUZBSweFCytOF3X/hIYevnhRl4uhcra4vBBGfL3iKg6iR+bNa3JiFZNnIaCvtSt5XIcEkL7j3X1TJ5epQtNQvd39lc0VplefJyIcHR2FyUph3+QcfvypPvr9Z9EdvCeIqxHme7twfZ9r12/iaDOkDu21ExwTSJrtDiyV0Oed23e/gTELW7JBXdd33bqaXr+vmkUcut4mwmLzfqcwppVdZ099Q4KoSL00aLGbKW0lsMSz/8ZNkORBXE9c0vA/UdU6e0hkV5588UXoi55rdK/dJyBOXTB9HSrnoXJNAbvJ0iZb0DzLWJYFsjeHm9eqFFFewdhLNNNvsTEUwAh7N65XqubWQscroh6TUgUr1H76/STA+I6LiXMXEiF4aXrvARd5fI8RwTkXVrDeYxBeHDyLA69WqpotLjcqWbAzk7YcIDxw+wZvvn+f0nlyY6Pn+zSGbqP2vGS2bAwKK44eJk6+zQlrPp+zO99BvGJKz/MHjzl+fAAO6jm/3YE2y8Aa7r//HhIHuCoES4+t2BT26mXc8xiTOJVRHDrvNNtpMFbPFKawWW4KgzYFJjo9pbdAJDq9WMPum3cEa3DqKttDtJnar8GiORfsqxx89pvfheNSX6MZUqnLDE69l6F3apNnMtbnUxdyCSkfbyqvgJOwhXA/GX/wJ/8o2MR4ByKhi7ZM4pXB4JM0sHdtf/D4kydPWCwWIXaiSEujc1XejgshJBoxlfelcw4tSowxVSwufLAHKJdFiKO4xdVCj4DoIAa0JYQcsPCTf/yPKL2LuYwdmZiVjAurqMTQzjU8KvWKbyie29raO4zglLJNVDHxes4VgnNJWZaUy4JcLJkKj7/4igeffh6cV7ziaZjEiAR1M4B67nzwfrU/TcIebU3SfYzdEJrepmPCVrXqpi1UjPVF7/UiozN0rZcpMJ5FiJwiWE8RFPu8w5vl1j2fpBJOdb1z7y7McxAwYil9WogL6jweKFXrjDCpooPnnLw4Cq+TtAXBrqBYtb0TQqTv7+b9bWIy1Bd39LTfbauOlX0h3qqPxL7zCtbwze9+JxTOLc47vILdMolXCt230QNoSGmZvJodWnnzQzAF2tnZiWECribOXUhMoRgAcJ7FiyOOXhxSFAVAVDcLOM/MWChDoG3va8+8LS4xVnRxA4XUc+O73xPdnfHcL0Hs4CTTN4l2J+Lmv+vwstieU9XnNQTtrVI+gS5L9MUJLMqV1WsLxsCdN2Rh4sAW7R1NTBvVpwodatdpBLwpdfWh6W06VNdZmMChusaExCkq7rO8J1P7p3ssPb/UPtsQ9rzAzbfuwHwnBMLGY43Fo7iyRIzBAHlSc8cg2aiiTw70xnw37DbSCtLd1551auaxsDivAi/jGk7AZ4b73/gA7r8jKaWRsXY4VvIWVwgmLgzm1R6J3kqJed7/1jfl7fvvxjSNoD6kbqhj1nLp5ZRz9262LZsq4enDR8yPoirRgFWp4r9ZDzx5plybi82yMwUI3uICoLLhACov3wBL59kaA0b57p//Mb/66/+CPXLMVJAUrwNaK7z0r2kYDwejfh/Yh2S3pe3rNNmPpjptKBuEj16d3etW6rsWK7QqDGTaUBWbRkDt2L5MLEXpMJmhjCmfcJ6ZOvj8K+X6B5Ky1YTvIQ5sySNhN2P//lu4Tx9hCyXLDcfLAj8z1eTfbI9077Nh30b3GG0hs48ZMz390J5f6++/T5CnT+BY2dPGmICwurAYXyeLqZ3kmouKup4Og1C9d+NtHPOCDhWkl3rVEzixwQBq4nPzgPNYtWEBnVu8eN64fw+MwVpDEBNBjGBTHzXsEp0Gsx5ZFhwfPMcXdezE1M7kuCLR+9fL6nNrhlkSX4cD6Xo3t/qh88x8J2C3NJ5Bq2vT593pyBRzNH3PPt5oul5yDlMftFZeDGUZtBSoUhYO2ZlxYpRv/tGPYRbV9fFMwSdqs+/pbXGZ0CNImOqQQfb3BYyKxJU2Er9LgeWS4zzjWpJjfLTtjXESkzal75KsXvZC4tyZRGh7AWXGVoJja3WaJvOirHr46hK8rx+Upslv/BaVyBjHT8nAcmY4FE+ZCa7n8zut2ngqTstQ9KpJ0++eUSTtKsuysjerJl5gx2SwXFYFhZTQsP4bgEwo55ZSFOccblmQZ1nwgFV9pX11Gmjn34uEPoH4vJBUrRC/EWp71izaozoBs7MbfKIG66l/V3Ez8xmHz18wN+fOIUzG1OcxxPY650KYIB8Iiv39fU7KJc+1wO/NQH1kELUqc97vwBavDnGpF/6wFqfh+3ClVqpnY0zMZBS4Q4jvk9aM+UUcxzbFhRASKwhks5x8PltlLNJq9+Rku3q7amhOVJ1DJq7KQMEIH/7kR+Izw8IoLjeDQk4SFlfz4K76UzdzC0+xG1t7Oy/x/RSRKqB8YkbS9virr0KZpldmCuJWVY6VKhgAACAASURBVKC8c/8eag3kFmttYLm8Yo1pxedr/tsnbG+iBk1Ya+MYNzoD6quahE+rKq/6hXabhzB0381+7l049Ix7Q7mvm+UTc+mi3dSiLMIz398DCe9FMDVIC4g6yJ9GUkREqnAej778KjiH1RRyfV3tb8s6VIKt2fyb2vRa61CZNHiPUw8ZZLnBWItDOSoWsDtDbuxhPnxXsKZiM03TC3yLy4+x10kEsmCiodKIEiES3uM8x+abL6ZkzWUvEi6YkCjk8zmznXm/mkk1pO3b4mpg4Etp7VLqcCui8MZN9t98gxv332Fpgt1Q69wJA7cheDOuUwlOwZCTQrfMlHr6fifnLe99nNqDTaEvHZ9/8ikha4bgvItx7xpqOvWA8uZHH8qxK0J6vjwL/2qtcjuNINy8903tFcds/07Tjm69U8pNbcu6dr1qRmld/3ZtASWzeCu4zPD2+/dhZxaOIZW9olT/Y8W2LmhrHC+eHIDz7Qmie+2edIF9bTwNXtY7MXa+F8ijLZmIsFguAyubGRbq+MN/+k/4/p/8FHZykl6+UmGHKOhnatMWlwTWYHdmLH0ZxmMbTHWWZQnzGbPZrNbypMXkWCrSS4YLp0/I5zMyVyuSU4aKNCAdHR6yH5bGl0cU3+LU8M5hssh6CKDKn/3lv2K2gP/9f/qfyRRsx+Fdmu9Gx6auLmN6c8L2IakN+ibrTezjpgiRK5Oj1l6dFWuEYIzl2eEhLBYwn0cbM2l5oobVbgbXMtzMomJYuLKKJLCS0L7ZBun8PdjmfpvCvv5ZN9GfVmU4Vm5q8OahY2OxJEOBlAO7sasluK3ur2T4GO4L0qu9an/Ydy/t3ybUVwZ7Qichy8oCz90P3iOYT/mQhi9eI73LYVEhiATbWoMgYtCnj6F0zIxF1a+EV+oifW5j/b1yHzEDS1LNdfttHarnIOP9tg5lWeLVYzNLnucYF/rQ7+Tc+t43mYuDzIIYlicLZjtzkkpRddBlbIvLgu4DFFYHdWO4eec2yyefsJsHm18vijMCWVbPTz11XIX342IxiRhMnpHN5v2pqdQFJlF9NbhucfmRnnR0Hmzp9FLqsGCz6GFmmX3wvnDvTSlyi2s4PjXZwSmM4qQyL1lw6ZZdd573Png4p0m+YQ9jEU4ePtKgiY/CSmXUFw31Y4e+9+2PuPfRByxdWbGTTSZ1SFgeFxBfHZs4Vn5KXV217tC1zsoEbsKEruunde3qHjcx80NKs+gFShTNLTrPmL3zZm+M9ooNkxDvL1Ub3jV49MVXahtsu6UONbMi/K9Z+Iz179jz7OufIWzK/jYxm82w1lIUBeo9R4uTkMrwux/BvTdl9xvvifoCxQcNF0GwBFbi2G9x9RAGV8Pb9++hmcGhlN7hgd1r+2CkDkIPLVOKV6lh+DpxsV5zAZNZxNoVT8IU0255dBIlhqtB5b7u0B4bQYCkivbOxY/S4JBgSmWBG9f40Z/9Sa/tV5rgWrZ61SQXrbJU4u9xQajpTNM9ln73Tfzddo0NGGM2Z8n+KU3sgX2Jm1cOnxxA6RE8htqGzGnw4vahQ3jnmx9w+6P30czgVVsr3BZzJmscf0SaMnzP4fX3L406hi/TFuqa2U9S7MS0dbOjdI/3le+e2/17qB1dpPsYEuj63s+xerr9Ugll6bkYaT0f732VdEBMsDu9fucNrr99B25eJwiJUl2kyX3V64lkrxgYsuePnmCcIk5XbFa7bU7H+p7nkEA8hVHf5Lw+jNnUNutZLpeo82RimNmM3d1dzO6Me9//FuxkIeHTPAdrWPqgssiSQ892CrryEAQEbr15G7XBNtwYg7fC9du3iB9VECavhky4ggshJLYmKRs9hgagxTLMfHpBGr/Fy0Vn4DXWYo0Nu20IZMvMgvEUM0MRPTdTKI4oW1ZZS9axhVPYRNhsgpoKZVggq1TL0WlFY8gRa22VNcMqmMJBcl9tplYzUgu3IiyMiuYGbyylNjzGe6459PfXhdMwT1MZybH61x1bd63T9N+mzG0XTacmh3LiS9id4ecZaEllgKiEgL8plExDU+O9r19GD/5kSYaQmRjuRaenQzxL374qjF3X5ln4psSwWCxworwol8GjOaNihgo8xkStRuy6K2JytsUIQkBtj84yCtEQmskEm8T5/l7vOVfJHhEulJwV4rvt7u+J7MyrLAthEPTVJHny4rCKUXRFBfctelDZRVkb9dKeb/zxT6Ww4KylVPCYSj3tvSezbVsRlXbasiH0qcq6dk/aM9l26xhVxY6oHpu/nXrEtlXq4rVif371938PLqgJU9igUEmMi6ch6PyNt95C3rghPjMYm4fMAd637rXb9qF+6bu/LhOX9htTZ1NK2xAD2ervDvs3du3ToFvH0P30XR9WJ4Kq6+M9jjGh3diC3XvpY+a6x5uMorUW531wSNqZce32La7dvg35rN3XDd2yaajIrLHE1Cvg4PPf/R78cG7vKQJyQlO47PtmUju68RJX7ncN1pUdOu7QEB9SDdbkHHlHMc944wffjyckTYdJ3ARRGYF0TNG2uHzo1WhI87iCNeQ7c0rRGGsXvBXm1/bq2LadueW8FkSvAhdISARUUZtBZuMg7MIqt+FF9+zxk4pJ3OJqofezSjJV/E+J6fpmGezscPvdexzj8LMMn5nK5m6e5VWMwYRqkqJm8C5anMBNYBH8yRIOnsOzQ2IeMaq0fBiMxNhvu3O4tk+2t8OJKxAb7H9Pg5c1AA5N3OfNRk1Rh27ajlfVbtUg5CQPzGcnR9y59w7vfPieYDqmEtr7M8Apj3/xaz35xW808zCzF86n8ZUghQxyKDqzlDsZ/+zf/gX4Msa/68ElHjO22AxJ3bx3+yZkwbPZOcdxscQbeS3ehQsjJCaPtyzPYZZH7ZlWqpSkSnz4+Ze1kLgVFC89murhPjQzU8QY9igm2DLmlp/+27+k2JtxSEmZCd7AsigqYRFo2VX5Kr9Y2AYZENoT7JAg0xUcxtRyvQxRVyVupGV/1j2/Wb8AWeF5+PNf6m/+9mca02mAatV2Q2RrBLCGux++j9mZUTpX2S02t0FhZoABXGdzt852sWtTmP7uK9/bthQe6bTbALrX67Yv/R7qr3U2es3z+4TPqn966um2MTGLJ2XB7q0bcPctmb95p7HAWoOiBFUOvvqKp198xZyYL9z7KrJi46qABDtHszp9TBmWW+/HK1bLDdnYisSYdzYDm1EaOBbPC+u58Qc/FvIZSbul9EVX3eIqYWUOapoYes/O/h5LdZX9b7Yz587bb33t7TwPnPtysTtG2J0ZOCciYeq0SAz2KojC8cHzBg2klfpki6sLAVJMe4159Dwxtdhbd+Sf//d/qea44P/4q/+V6zZndz5Hl2U7W0uzPpGNFhib2pp9XWyXKMzF8sWvfsdzCx/9yR9HOyoTAx6TwigG8VqVO+/d47Nf/gZxJTjFrlGNNIWV7r0lobev/NDxbpl1+191X64TWLrmBmlfX67iswg83Tqn7jPGBDbMe1wGt95+E/JkktF/rVX1moVS+f3PfsncQV74uMCKZgwifWeN3kufwV7f/Uw51jy+rtymKJwj251zeHTEP/8f/kfYs5AJmJCFpVpoNs5RBrt2i6uIGJTe5BmmULwPZkB2lr8WL8K5L46CPr/+WzJbqZsre55oO2UUxIWQIIEq8Vs28apghWILW8WsEBYMmZgYUJrw9s6EJxQc5aD7O/g847BYclIs2wKLTmMDzkOdmViNJuORynRt2ZrHjIJVRY9PmJUu0p/BBjGTGBLIJEFRwmC3M5MX5QJmWVA3b6BCXadunaKO7bv3PjZtzK7nrPaIQ5hyzbGyQ30y1i9TvZ+HjqeUfN4KkmfMb+yDujoOIZ13Xto/q+DrpbLjhR2v7EbDfEedttFo+CDTu7oOU2wET2NuMHSdKdfsO9dayxLP8X4OO0KxlwtW0ZiGD+pFapK7g8js0ZW4B1tcaijteag5H1mDN0EWUdVg/9uNj7jhJS6L6HLuQmLze66C+xpTZZio0pC5ECzWioHjYzhZbFnEq4qBx5pUsy3VQAZv//THcv2nP5b9t2+zMEqhnp293bXvxyQGrWNQn35/3ezi0PnGA4uSuQOePAmed/SoT1LIqGt77L9xk+Nieerrn1ZAHDtn3bmvSjDc9FrrhMUpwvEmZdbdc4qZ6QWcKG/fv1dpiH0MMCVQ0V9KWEukGkUESof/7cc6c5A7KE4WLMuiEpLG2jzU/u49bFLP0LlnRV+dKvDs5Ihv/PRH8P47kr99J2iZraEZRLz6ni7LzL7FZEx6rrml8A4Xv7dslrN/4/rX0Lrzx7kLiU2ICDbLIM+qwVFVsdZWK2ZR4OCZLg4Oth/sVUAfg9g8lLxIm3Ex0zlGcOJhFsIm/dlf/BsW6rC7c44XJ3VGC0kWhs3tjM0WWjZtQ16rXYwJly2GqMMwdVegGstnCFI4/MmSB7/4lQKU6lEXvhVVai9MVdid8+0f/QCsCQ4PI+1aN8mPTeCVp++Ee13Xhr6yTRvKs2yb2iYO7R/qi+brPSrsrXt/ZDW3NcQQOCbY6d7/4EPyu++ECADUAdbHx0kPVvj0dx+TeRCv7M7m5HlO4V2LNTxLGsupIXT6MPqedb7DKee02uWVnRvX+c4//cfC7izYw4vBAWWv7bsP1PwWrxdu3hSbZ2RZhjHBZrcZIWBlUX6FcO5CYozQUX3UeZ5DnseBL8DakGDbimKXDp4+Z3FwoNuA2lcXK+EEhiZhyViUy2CLd/9t+eYf/pAlPqTSsgbUtDOLdBxFTpN1ReVsk96m1xuDc47d+Zy5Gj7/7cegPgiOKUwHQfuICFgBHHvf+obs3X6D0kQHCkaEGYlpyjr3KyIr6u+xtm/KtK4VDF42zLga9awq0JELA7XQ23RK6k48K+8h4I1wguMkg7t//k8EG6pcLhb1+UMzWHroTvn0tx+TY8gQ1PuQgWSg6a+KFT9VXV3Hr4iW6cbAN14aOJkb/uhf/0t46424oKzbZ8cWD2dp9BaXBgbAOZjPWRaOUj1ew5g439s97+Z9LTh3ITEJ4ymnaBIGlmVRSerOOcw8w3vPvofnP/8VX/3iN9E20dNLs1xG5f/rCKF3RlyZ17qjcvzbAlm+g1pgBt/6b/6FLDLwxqClC/ZEkmFMhsEiahCxGJtRugY7TRR8ABWpbJCsCKLB6SPZY7UEKxNiAQ6pIAdfw8hgrTJiTcunRp1xMyKtI8E8V0LsxOMlPHgafqPBk1sVEcUJOAG1FlC+/6/+pZwYYQlk8x2W6sCG+1DnsRIyszhDtYWJN/SMqsS8vw2PbWl7b6tqb5aTbvzBFmPZ6RcVg4oBY+tNXuLW9SY3fW1YPV7tX5PRJd1Xs4/SVr8jBieG0oR/vQTHI+vDA27G+yydCwF9RXAGCmv46Kc/4e5PfwjzuDBySp7PITLJld23hLiABtAYWxMHz3/9a81QUBdsfj1kGkx7vIRnX30fHeFrSLj3qq33vdov7W2wbzvOKem7TP9q+l40bIhBqc/34lHxqC9DbgZf4sslJguEg6qytMI7f/ITrv3RDyTEX80qhxsLIeBW7Lf6va7jJsr5T59bnBErwn7nm5ekislsSMtnhdLAEpA3bhPeg82u17cAvMg4/7d8gA00eUbhYwqcaCxqjWCWjieffsaj338GS/c1N3aLc8WAMKneUaIhE8veLn/+3/4bikzQPAten86hrg4c7VxYEdpZ3qreD/weYiouQozFLAsqEJyHZcnjjz9VXLTjpRZIFA3BtQ2hn27s8+YH73Pvw/c5LpZIZnHOVTZuVfop6V9jVQGS49+nZdqmlG3ZkE0+czq6NmpDaRJPU98YFEDH3amMCXHZnHMhMHkjtqVDWYiy/8Pvybs/+YEEz3ZCPDeBZmKdJOTY5sfjSygcLx4/xcRxuFwWAORZFhcbnfZMfABnsUvdBNVkHv/uGpPMbEaxWDLLcvKdOYvFAjKLs8LCwr0ffhuuzcNHHvOip5s29KQbvEyz+xaboTHW+dZOBSPcvHObm2/ehizHCRBzeV91nL+Q2AtlvrNTZRFItonqPLMsozg6YfniKEj4TSv9sW2LK4vMWDJs+Jhz4blRjnN4LiFFmWQ2eEQ7Ra3gjaDqkUacwCYq1R9U7GEXTTXXupAcr0I91xR4l8cnWGPYyWchIxFSrW+9BFYxU2GmJtqpKfiS7PoeJ1aR3RlFUbCTz/5/9t401pIjy+/7nYjIvPctVSSrWNzZJJv70gt7YXdP98yop2fQs3TPSGPJkmV7LEA2YMMyDBi2/MGwvAAGvEL2BwH+Ygi2BUgGBI0EzWizrNFMz6Lu6Y29srnvazVZrKr33r2ZEXH84UTkzXvffVWv2CSrhqxDFN+9efPmzYyMjPjH//zPOcaPKMz7Du99iaA2VuvC1swX6HY9j7t3nUPg7Q5qOAgsHmb/1fM5n/t8SROJtW9IpaKOFEbSL77jnBuqg0S1akLOOVLrDfQHY8F6zZZs3sg1KhlW4d4wHNbyp+KJOzMDRKo0TYOImLv5HFq/g56Lc9mhwfMhU9xkCjuOMT5SorFVsAoq6g2EiydliMkm+55MPwnMG6HZmApF706tDoQ9V5ftfWTn6ppiQDFi//qag7dp3hfI4uKDxPGgM5oGto4cJTkGV55TIGUkKadff4MzJ994O+IPLtufdFPIsQzo4lGNnPjI/dJcexVHb76Os9oz17SY3BQa7/Eii/QhVHfWCjAY1z8uJlomcor7dwQUDxOM8XaaYBreyranPnLy5VegT6BScksWNnGEslSANnDNLTfJjQ9+WE73M8KkZTabQbIceZPJxNII6eKa67WuahHPd90XGrBSt51r+/k+O2ewyjnsQoJnDjr3cx1r1Wqwiq9gfNTGSSxCWUSWg/e83e8+Ru564D7QuKANxUDQErWWYFzENKVk/T1G6Hpeff5FYt/TOE+fIhklNM3+HI0r2O1c9/+t2IXoUqupjKUQK4s3gXnsaTemQ591ITCXzH2f/ST3/tRDTK+5pgD1QXNC1SW+l+rvXrYLswUwUlttSYZJQNpAVwO61iSTfy/aRU+mvdYEtq44yo5/xQbGrODNBZJy5scvvcKZzQZ2ZjBdX2T7sr0PTIEMznmLRkyRxjvEwWe//EVxyfOH/+f/o/M393ACrQv4kV5PRhNvtbHuylMnn5LId7Tf4HZbM49dKGA8LNBYt1/fdTah50zjPLtvnIEzezD1xBRxvq0nVU7Xrk88tNefgJg5fvMNzF54jckkEFTp5h05uIFJdLLw5w3MTdHZXQhAXGujALVz7X/Q76xGgB/KRsdyanq/peTYSzuPtlfX7RJ4yEuA4sLBRR6+V6U3jhUdoOpw/0IIpqeLSru9zRX33y1MhEV1EBncpDlGfCmvV2FQzRaRAHGB+XPPKF0kqLGHk2Da767v8W2DlgXYKkDMsux6/snaYP1xDrO93p/ogAwh29WmosN1bWDedxbE3QTb3ghHPvcpOTLBAtyEgS3K2fLfuZJK6rK9x21lHB8tF8qGMvY1nptvv41rdjInT54kaoS2fVdP9WLZJQaF6+kIm0ePoF7QMgs4xJIpo0jMhD7Cy6/tF81ctveXCZAh5YT3AXEOlYy79gScuJL7P/cQ02uPMSNb3jgR01+dJzJ+DEDG5epMLP8OX9N5bAyY2rYlxmgMUtcRMrzy/UeUpDQ+4Cq4rQETWG69hAn7ccrtn3lIZjkyi70xV8HjnaNx3gJldBkkXAh7dtBnh2H1DtrvsIzgYdnDcx3zfG7lddsuhHk8kHGtbNg4KXbRivY5kYPjjg/dZxHrzr6QVwZDH0YcQFkcDfXLux5i5pVnn8XNI0c3t5i0LV3Xmdt5OqGL/b5zGger1MCTc13nhTK3F/L5Kmh0LDPeKpDUXNChbehy4oxL/Mxf/LMgPbRCTD25upZr4IxenlbezyYrf2sKliPXHOfoiRMmwxCByxVX3j0bP+wZwAnN9gYRRbxNVmQlRwtAmIhnQx1vPvPswoV2nn/7f/SgDy7bnxirPl8PjfOkFOlSRpwbJs4rPvWgbNx0gjgJpMJ+VQlDznkUq8g+1+qYKdlXEWVlwlx7ehfgcn2rllUX9URDYLud8tyPHkdffZ3dk28Yp6Rp0VaAd94ihr0DEmy0PPCpjyPbU+ZZSRjISV2/BA4PYg7XXVeN8K3bDwvUFm8s+nj1MT0sODzs9nWpfZa0iay/7sMC1XGU87prqBrEpSjiKmmoF+5NK+fKrcwqdEHwd90uOEfWAiapUflKTNGYxRTrj+JqaiRNnHruBd176nl99annaFTI85447ywIyluwTCgg8yftr4f9/ltxN1tsf6ZJpulcbcemaVFx7OzOyNMJH/+ln4dbbxA2W7IooW1wwQ8A2uqcFxB8OR/i+8qWxpnxB95o6o5MJ8pMk+mF38Zx/FK2SwIkDilIRkkFpAnE8kGNugzO20CXIhuI5Uy8DPTe31a8dIJFMra+hHgKJsxvHGeCMps45o3QSbbFh8gSMByX7Tso99qhTqcMHO+WninGyObmJl3X4cXR785oMgiBjSiQ2OdSMQ2mtQNNAAdzD2dSxzzYs5dzNr3jWMd4AXZYveJbOeZh37+VY4y3X+jxfxIwNSxoR4uR2vaVQczZ8n+6acuZ3EEDOIYCcQrknMzjUt3MIRQfWmEQc4YshFlko4dpFibi0WxaVO/9ou+O2Pa3yh6/0yYsFnT1LMZ9dT6fW6Da9pRTPvJmk6EFfJFral52k2NeCRglI79s71tTWEwKbYOfNCRRInKJoKd33i4ZTWKtIaqoMbmbG6h39PPEhrOcbTlnfNHbxJg58+Mfs60LDdhQhxSL5pP3Axd82Qb98PLddlazTuETX/4V+erZv6s7z79KVGErCz5lnFqOw1p5RNQmUXFC8J4+JaS6n4rLeR9YOqBs37ms9tNVHdfYDno//msDmDCPPU0IaB/xwTFtAo/97h9q2mq555e+IKiiPmI0kiNrXkQ540CUox/8gKTvf1/nOx0+ZbZDgJgXuexgKcH9YZ+sg/SZ+9PMLOeaHH7ngLas53K+Z3zpmOdo63o+g2tXD/i+LOtTxwEThzmP1cVDfT8EAzkL0tMyrjmE2PdMmikz7elF2ZPMp7/4c7ajAy9+KLXXOL+2RWqaGEkZ1PHIH32D6U7Phtpj4sQNNWmXzrmCyxETPb4eEUHzei3i+Pvr2uOghVT93lBtaeU3a7tVHacgQ6ogO24GFXuOQ2DPKx/87Md5o0lc//GPCrKYHcYk74K43V+T9/JM8t63pXusKxsEbrntNjZPzZmTuf3ee88rWXqv2KWBhat2aLypDUTEXAElmTGYiN5hpcheeOoZ0OXB//LD/P6yc6oGxEGKEIRP/eu/Ls0Nx9m66Rp2G3DTiWnzCpNQyy157y3RMKDnSYNxMRmUVZMR6ExdT3xzh/nJN+mefUENgLnBfebEFS/9AHVgErj9w/fxkc88RG49Z+d70HhLBaQ6pF8BY7ZCCFYd6SJd62H2eTuYxre6z2HNOTckMa/AfzV6fGNjg3nfETanHL/xOq646Tr89SdQKUElLFjwJVOWFzEKIoGXv/Vd3YrClnpC3h+1Divavrftai/MDgLUY6vMZ9/3NE1jXqdorvKqQfzgJz/CiZ/9lNz1pz4rxiAa83rZnXzZDm3e08UemkAz3aC3NBnvi9XDJcMkLlkRTvdlfTwk9i0fm2ZHefHZ50CLW6Ag/wVz8T64e5dt/12uK0Apr10LZJh6PvvrXxb2Et/6+7+tZ1/8MZttQ9ZMSpncmx7LdEwNZKVtW9IKkzGk0nibZs63C3BUlsu54n9PHUTl2e8/yh3XXQObDd4bjJDCo4h9sWhu4Jo77hCS0jzyqGpU5n3GOTGcISN3Xs70Kdlz6ZdZl/PWr16JZq7QZrU5V783Bk7navoLcQmfi8mqQSAHRfCufnNgFNnPpqnq4vxXvpjLmOXKYncImnBF9pCFeUyk4NjpZ9x21we5asMLKNIsoiuljn/l3FZ/RzUhWeDkKZ5++AdcsRfxadEeuT4v9Xgr15iLBGsszXi7ylLWNnorEo157A1En52Ruzmtb8hiORHjpOHaj9zFiS9+zqKvnHVizUpOEefPP/1dnkXe47bPM7S6vYTNg6UI6zxdNh23PQzvzmleTLs0mMRi1b2CCFtHjyLBE3M29rDqcorbeeICadbB7l79sq2UB30jXI5Rex/Z6OHed9e7CGS4YguuPsqDf/pLcuzWD3A6zenI+GlLu7lBFyPinSUSLgEhl4IdqJujaLFKZYwhdY9zBOeZiOfsS69x6oePK30y8FifEdVld4lA1giS+cC9dzIjkRpHp2kATCJi0c7ln3sb84Qdxl1/roCUCw1YWf3uWz3fC2UeV783gKPiwq7BLlkge0EaY8SYNtz+kfvhpmulve5qaDzzGpRSb+O64a7+XlKImZcffUonnRIyeF0AxFVGfqzxq5+vgsKl4BouXIf7dugcxTn2ZjOaSUszaYma6XNi1ytX3X4zd/zi54WQrWShE1K0RMjBh0tr8rtsl5wphW1WB2qlMmOMqBNuvO0WyyzwPrCL/pyUxAz7BrjNK68AvygPBpa6Y9iG0DpPf/KkMs51N2YK0mV3wnvfRoW4FCIWqzGYAJMGXCDm4ls7vs3tv/wFSUem5I2G07NdznYzpA3M+57QNmQsn9xwmAsEEm9XNPP5jlMXUFnMjUZJ76OqeIQ2Jl744Y9gZw7z3kAiI2AkQk6JPkVc20BwbNx6s9x83110QZDgB3coGCMFeXDNr57buc539bOBFTzPNa4CuTG4O1d95cPUZT7fbxzmfqy7zrcSIFOBd92WVdnLPXHimV57jO0H7pGSGJQ+xSE4Zel4qy/KYqA/dQZeO8XT3/s+07Jd/KLsYnIsseQ1wrra28kajm2dTvSwz4sKuKYFH+hT074k7gAAIABJREFUYhZ78I4Td9zC9t0f4O4/9yVh2kAIQ5ob3wRijPa+AvNz/Lts723TdW9GRMMQvKQQSkWehHLk+PH3TSe56CBxaUCtzIYqTCf0MS5F9o1F8FWkfPb0GdOe1bulC8D5djIdl+0SthUXQR5tMrGWuZlc8GjwpiXZcJzyypupo93eRFpL/eEmDTFn9AJcYD8JgDzwks7xu0sTal4wUKvBEQJMxJPO7kGvsNev0EW2vyvMijGnGYLQNcKuRmaaSHkR2FOPn0tN7MNe40Eg4Hzg4DABPedjEg86vwvZ/3z2Vo4lYrlfl6QLqki29s0ovYM8bcibLUiENljePx+gaurGP6MLV7EKxJwAQfc6SLBFYOpMfzvPcWAI6/5gOWlXK5eUQ9vPrTCIh73ed8JSqTVN45FJw491zs6G41SbYSoWxSy6SHOTMiEEvPeXlKb4sl08O2ikHSimUs9bssOFxvKUOoGc3hcg8ZLSJFrUoBjQm0yImnHOk/ue7GyST5ps4teEJuHs6dNcVQ9QGcULcAVdtj/BNn66pT7SpfYqRUOVDQulpHhvuRFFHGxv8KV/7zeEufKH/8f/pcEJ3WxOK1ayz2fFlXq4YztIo3YxbRwtvagcYDO/orQhEJ95QU+HzLEP3SOVjTe2yqLDBQsCsER8cP0D90qjoi/94EdIZ2yUo+T8c4JoYfdXo3Pfxusaa0Dr+9XPD2r9g8DnvlJzaz4b39d1GtSD7rsFh5wb3GfKvRoviMfJmzVZOhon0HiOX3uCN+m57aEHhTYAJrlx4pZTtFRkiP0dpAchwCzy/KNP6PZMcV2ijxHfNjg8KaVDpTdS2X9vZb8D6C3ZW32ORG0R1CdzMc8b4egdt3D7l78ot281IJYv0odA0owXR0wRyRnfWDS09xcn+OqyXSq2PG+stZIRoA0BVGialhM3XGfP4/sAYlx0kGiTtiy7wHyArU1x01ZTF+lyomlaPEqM0Va6IgSEM6+dhHkyzYnIwnWVdTm/QbUi8L5kbu4+oezbdLyxXSrXei6r9+U8u4xtdQ1QD+FG733J9GIAkQFIaeOQK7dglvmpf+vP8zv/999me2sD6RKtOnLqh+OsihYOSuuyztbtdaE5B+EQrFTW5eAOrbkghRCVH33jYfamjmN33Ap+OrSDOFtbLZHuDmgcVz9wj+ycPqOys4eenpH29lDNuLR4zsR+EBkBk/MHq5z/+i4kAOWtfO+dAverCderuREmhJX1zRKrKYh3aOOR6YQbP/5RuXHqjUF0CiUyfek3h+Ms3gxCgJTZO3mSlx5/miNRONJOUCIp52HsXT3WvnQ3Y92hrLw+TzOer5WzjM51ZIcFjjEnZNIw18j2jdfw4S/9grDVAAmcH9LZiNgCMZSI/BS75Yo0hzz3tc8zC4x+IX/fqu2bMn7SA77VkzioMca2Zow+x8cX11b68zAkhgB9xrUNfT9j5pT2+us4f+9+b9jF98eOgJw4V0SiCpOGK6+7lrkItIFeM0kVL96YIBwhw2s/eKKE3mVSPZYJnVZ+KC/9WxVqXxTT9a/3nZuu/FvZPNQfViAliKmqbgff63hySnn5UGO3YYyxCOmLpnP0u5ryfhds+Z0h2e/qJZ5voB+VyKu/V7WkKVnQRD2FhC7dxYiSCyxMCuBw2KTjscoTwNAVnAPG7JNzMHFw3XH5/J/7dXZSTwgN9JmAw/V5kQ7HCV0y0XIIAU3Zgqyw6F+nDK7DVRfikjdQ6gws5DVu0loB5CDt1mo3qPtKAWrD94rurqYyaVNmIymPf+WP1LSJCrX13Pg+eUyrLTDx3PKzn5EPfOGnZb49JU8naBZa3+LU+l3V0MGiDUi5sDQlhQ66pA1c0giObNwWq5VQVr9Xj3c+d/N5+9+KVrEee/W4QzCJLKrJWHmuWhnGkcSRpNS3Lv+cc+WBKyUhS88U8aiKlZFUJaeEEyFliCGwFxx3/sxnhSu2YWsDvCMP+RBlKYC/8pBa7ik5lXEgw5s7fPt3/kAnWXAxlmdMCeKQrEv6wwo/8/j8RRYViGTBJFPaQ0WsAzmHipSxeaXvCsMC3obmZZY3jhNal/Eko4t74d3i/pfvpJTACbENvCmJ+faUD3/5l4RjR8x90BRwSF04LgdaedfaXIPdHmu/kly7tOswzmgmah4lLM9Lz2Ne+ZvKHakzzern9a8dIZ/3X85xOFLMPbn8p+Ql9rjaeMxUFtcU82I8jTmtn2PWv10/B60+WusetdWpYuXfxZ6DbRntFpdV+1h9OYwfmT0S/daUe37qIXD23Nc+9F62i36FwoKV0JytIoATmEy47pabSQ4rITayOgH7DC898ZQ95V0kMYr2E0u4fUnbW1lGjXEwNkFIHfxUwXsrWh/jQKsp1sT1O67g8FwG5xqAoCkTfLB7EhOCs4kmKkRF1CFJkCSmcdvtiSffJL9xFnlzD7fbw160z3qFLlvajfGIMIzApg8aJo/KahRBPRT35ygCd9wLYooGSMrn66ojuHFjLeNF2yw2gNI4uPUG+VO/9iX2nDDzSnSCBreoQJEy0+kUVWXWdzRNU/J37r9FY4CzNKbK8j52y976MLnum4OOrP5OzgQxoNImOP3ya7z63R8qmtG+AyxXJIVpMcRr9yQ5IQeFacNdn/2U6MYUN2msuov3hBCGakhjdtU1lkNx3nfDNY/Ci4bzHDIRvIOykLfKOL5dlrNp4Jz3S3k5a0BejHGRRkgEmTbMSNz90Mfh+FHz9fjRs0udvEbXgpDVnp8Ye3umEhDh8a99S9ObO2w6x7S4Vrve6hW3raXQWS1DqSz34QGQjlzwFWLsA/MsfwfY94wsPYOV8R4F7NQFT/29LkUrpOC9sawCIQTmmthrhc/9u/+2fP7f/HPCFZsGEIONBTHGxe/UJpP9J1HXKiLgxQ3nLpRUOcW1X5eXzjlqqqOUDRJmzcP4VEZQk6yo4BUkZnzMSAIfFekzEhN0Cjsd6fUzzF55nfmrb5DfOAu7PfRArzh1kED6RBBvx0vJLiHlkrVAhwXtOFuwYtKEnBLe+eEz73yJ3MXG4mzwsYLI+t2h8Q64wfvA5AXY/uXERbAlcL3fBvmbc5bI3mXu/PD9GHi/rEl8V6x2RMFW1QjQ28M91H6tk8ma3tg4b0BmI9DYgQZwH4JbdII1g8OlYKuux/HgetA+Y4s5Wy1eFO+ElC2gx7dhYBmG+hBF6O7FoZrxrupySh1j1cWTkqxqgYE7A0mIs3QyVavXZcLZqIgTfAafDKC2jf11GKNRwCowBJGAHS5RjuuMqahtEEuqCufcAAQHzZcI3gWodZoVVG1t7ZxDsco9DnBL/rHyu2PA3ITyxnF6Aq/4jquOtqRZIneK04wXyyw4390bAFB2QuoiXhyL6WME/kasx7pBdJxi5CexgwZoYfG85JTYaCfsxJ7WeytnGUGaMOgZtbZdmbS7rqNtW3K2z5k2dB58ThzZmDKbzcgln2l15akIMSVEM23bLrGMq4BBxid6HjuX9vAnsXMCyEOAdzcal3S4n6Y7HAd9JGdgMYsSvLV5jBHBKvvknHHes5d7dmPEXbWNO7pp6MILCXt8/BrmpvZlLXrH0DSlFKNYoNLZGcfbTVzXE/tI4wO+bUBZy/yfy827lqll/y2seRTra1gGkrWiDIAmLcAQy18o+/vKRjth3keSQt9HRIRJ09DnzJsB0B62N6CRIecnWQihHc6xrlGFMgTV8yrrVGtE26h9RBpvhKkrefKKp0RcU1bYGfGe4OzZMcBQ9M6jhTCUMTRaABHj37JVKvQZP8/4VBZoGRurY7SdQ0naXEoqgi7G6tHi2Oq0lzVCYfLHoLBazAYYvSwYBHGyr4Ln6mKkgsK8sl+t3TR0j5XvjF8OZ7E6L1+iZiSMwHyOC56YshUacI41hXnek3bRQeKA1MtkBRjV5ZUrTxznJcpgvKYzOTJx3sErryqb14gET5Zaou+S73/DA1ftIFq3jivrzECRBfz05eEHmMXekkPbmtfAUhYD1I6yOjWQw160II3TZ3jp8Sc1qDDxgZeff4G902cJ4tAuEmdznn/2OXIfcSJWJ9kAnKoqUYzVdJOGUIDi0auPccXVxzh2zQk2tjbtep0w3dqk3doUFxwyaWFzG3I0PWqcE+pgrFpGljJY59Jqvnweze3knMPJYiKwv7K+8WrfKEyiQxAHRz94q/zKX/o3cAS+/g9+S3deP0Ozp0zE0TpPRpm2E2Z9R4zRmDTVodsO4PMS6nihpP/oug7nLWXcc489QbM51aP33yXSOJxkED+4BVWVtmnJMS1YrtZz/8/9jHzj7/+WKpGNrSn00SqBOGNbm6bBN8Yujksdjm0MDnV0v6odNqp73fuDtq3auUDQusCWc55TYeBSea2yfM3iHbEkHvfBA1bGTrwrTGJv+dacoL5Bmob7PvUx8VdsDwARRm10wOQqtaOnbCx+9pz81nc0vnGGNkvBJKOApQJs1hfxW2+H0eKOg1lkRDMNOuGVpqwu7VxYLBQbQ1TQlMhYEmMXSs3cIGQvzIgcv/MWPv3LXxCu3LQf8osnP6VkC0mW3bw1NXKd3wcnTF1wAtL4xcLYFxo3lRy+Wk46FzFvMmDo8UZunD1D3t1TV659fvos3e4eO6fPsHd2h72zO5w99SY7Ozton5ifPm0sYMkBXIO1tLC1znt6Mh+8/XamW5vszWdsbG1yy2230WlCQwON59jNHxC2NyD10Hq8+BLI6Syxvm+MZXWOMJZ5pGRSAWz8qufgBlHDuW08Zw3P8rm+tmYsvlTGywHsrvuwbUgoIXhuvOkmYQDiF90Z+47bJQASGS1Bis5J7IG/7qYb+SHrGUTK9g0Kk1j8qjYAjiJa9wWvXDo3dUyw7bODJteVjYHCAjrBOU/CJugmeISim+uSrVgJEAXOnoU3z+rZp57CdYlnnniSZ555hn7eMQkNOSUkZhofaEMg9RFJmclkwtG9GRutlbTr0t5SHj1TviSky+AsNcX85BlekWd4WbWI1G21b+AW7bOd76TdoCdy4223MT26yc133Ym/4Rrh6JEyWAMUFtMJfT+3yihDOxW4PZpVs5irpWLFpXYtg7gvn2fNuOBw11wJGT7x7/wFeeIrf6BPfuM7HInQdj2bPjDfm9M2Db3L9lPCEkisUdaDRlCXl5syojGFc7PEh7FxPeXhN0YjXSoAcXt7m73YETKEeeK5HzzK/UeO6gxlettNUlOFQNGe9bZwGGQAjQfv+fiv/7J85R/8lh7JmWnwNAQaH+i6zhiKJiC4fSBxcGW6Ag5dnazXAz2tbbXGljSca7433mctwBsBv8OMBuNAoH2l4grxU92PFRgtEdjFtSwixOIlCcEYRfGOeYrExnPTPbcTNyY0N1xn/V3WnN++JskmvRCHywrqefn7P9DtXnj5safRnRnBN6aE9IF5tOdSvENU9k/a52mH857O0ACsDWxZlWFoMC2jFhbNlTFcs+mNPUIzaenI7MU5eXNK1zoe+MwnufrBB8QaSMGb+9QNgSr7L6y6N8dtauygs7yoNUNC4S3EC8Te2DopN3oe7cJOvcnu08/rCz98nO70aV5//XX6eUdNayRA3/e0YkyxJnuOvIgtvnKmcZ7psKpcdrFDBdQmUdh9+AlOx8hkOmWn73n9K9+hJ9PlhJs0zFNWNw3c9+GPcN0tN0DTMv3A9cKxYwUJl9VM30NbVjJOiMEYSD+0TWVCywJ9HNFWFtd+1G1W7/9B3WlfP7mEmcQlItSQM8SM+kAXe07cdAOIrq3x/V40uRRSeAADhM+Yi48uw6PP6j/5a3+d43sZlbw04WRx9B5OeeWjv/ZFbvgLvyzaZtS3ZCdLk4+9Wg51v4T76AUN3IBVEXAlurvqTGICPOzNISmP/suv6HOPPcm2NJx+9VVmp89ytJkSoiJFu+mcQ1SLO1rJfcSPtEJtaJjP54skyk6shuXIrbh0GcVFM85LpoVVqb9XxeQqQicZvzHhbDcjeiE1jjwJ7OaeDz/0CT5w5wcJR7aEY1dZI9USdHUQ9KWCrRRWemUOXHKfjD9wi8ki5YSjgMsIvPIazLN+9Td/G7fbsSkB7RMpJSYbU2K2YJY6uQkrIFFWBpLiHhpAjCyzM7m6Z1f+1tdj8FSDVsbs1b5AmGxayjjvbP9gzER2wkwybG7w4M//rHDiqgW9sjK5K7oYLHNm/uM3mPTw9X/xe7qdHBve9Jl9igt9mZZggTGrJkIqeStTdRrgF8BglIJmX/COW26H4fpWtp2P6VqehHUfCBs+z7p2++p46bKu7JPX/s7itbVPypnZbEaz2dJ5uOmeO7jygXuE4Ega8U1Zv48GqiWWY3hTWiADCbrnX9Zv/cs/YNIrGz1s+gbtIimbVjGlZP21SE3WlZe0VGQHX/NqewzHyBUgLvavqX7GGlytf2Wx3SX7zVqeMGc1lt47A4gkjt18Pfd98RcEerjuuA3lRaI0Hs+NaHBL7TYe/ZfHAV3Qn1Vaobl4KLwtOt84xc5Lr+ozjz3B0z96krQ3Z8M3yN6MI73Q4sgxDddX9ZOmtklLfXMIusmmBR/rJqUEAA5VjXRxvNYHuhQN5+VE64OliAt+CJaa54hrAik49nKPm7bMcuSKE8dho+Vjn36IjSuO0Nz6ASE4W9AGD0GMRMhqLVRlX9V3vc7q5n0D7GJTZW7Hu6+d2y7mJLzyfO37uPalKh1IlrBdSxCkXEKk0ztlFx8kllVLzItoSIdaD3vtDP/oP/gv9OrdPEy+C3PEkmh279qj/Pz//F8Kmw6w3EUxZnxwFCUIfogE2z+oXDQbA5Z1J3KYk9NUfRMw680N8uKr+vV/8s+4qt3k0e9+n6kEtI9MvNUmzn1PE4K5jRHCqKJNjWD23g/BGbmky3BhkU6i/q3u4yrGH4vQYXmCGQvTszCAA7KSNBsrKRYwklGys8hHvMO3DTvzGe10QjNp8d5z6z13c+zu2+HKI8IkwPZm8SdZ0mEb5JZBWp0wapOPxdOx64cUGUgVhIsF7vTKK1/9pj7+1W9wlZvgdjtIGZ344mLOw8Ber9WNQOKgyRI3RM/WM3o7QaJHBi0vGBCPORlg9QtQLiIkL+xoJm40fOzPfEmYOHKZXBSTMdikZa+Jhakp5f3mz7+iT/zR10mnd9mcTvHek2MydrYwJ27E7qkT89K5Rf7DoBcGEldBYF7TRqv77WP/xn3yAAC0n9U5/2s7Xhq+a7WXFe8cfYxkgaaZ0OdkE3zbcDbOufqWG7np4x8WNtuS5qY4+ipQMaJpn2bMlkYJl9Se+5d/zFf/2e/oZhKaBNJFPAbiagCWqtJnAzRD5PW66z/PdQ73Ki8W7weBymG7FNAjC3ApahMxlL6rSkIRHLlx7Ghk3gif+IXPs3XHrcJmqZnrPAOIEbsM58x7FArVlWJcpLkZusPoeutA0CcbJ06dAvXw8mv6zA8f5fWXXyEk5fTrp0jzjiCOHO16G++RmAlRaaTmX7VnsUb9V+3psBAu42PUbEnwZTlh/0CCDOc3av+UUYHGB4bKSuWYqjpEf+fSzlmK6zh4Y/WD5/TeDptHtuk08ZEHHyRtTbjxcw9ZwM+0ZWBl67g5ftbWUYar89YKSFxnlxwpswIEVs/d1mGlnyWbizRnsmNwy7/X7eKDxLx40G0xlxEp+o+dyD/8jf9Ur9mNZbW6rOBTIHp4bUP5tf/lv5buSEO7vQ2l8L3amhODjn/CQKKs33V5lxJ53CWe//b3dBrh0a9+i9efep6jOGS3YwO/lJIlFfeFc87Kco1cIeMUEWPQJyLDJK1lQARYV9FmDBCrgH9dipIhzcgYVDlzU9aUI7DQyNSSjHUg9t6zR+JsgLQxoQ/CTXfcxk133c6V118j7tiVBqBDM0yy4xVu9Up7Oyw1qL5OWOIXg/rAykbg6ef193/ztzkSHU0al+/MB4AVv+Rio5TQWzDdbx0kggGvVZBYQVZt56orzCkRQiB2PY3zqBOi93RTT9pq+dCXf1Fo7FKTAiWis0bPDkL3qh/ulfzk8/r4t75LnHdMXFmMOAtsGhYK2KSlfrGoGPpL6Z/AEkjUVZnIIUDi+djEdSDmJwWJefXJzMkWHqmAoGRBPLO+s77ctpyZ7zHZ2uTmu24nbzRsfvADQuOsi4m1tx9YraGhBpBYGRpzEWa610/TJsfX//E/13anZ6qWHsy0bmlok9xHmqYhpUSfrS9o3F8153wgcfAEKIxLota2SOgSQ7n63QoUnUKjMixMq2QloqZT3prQbzY8+OVfFLY3YertgRNjZBcL0f2EV9UM79fAlfMtDbnzzPP6xkuvMnvjFD/81sMcocHNepou0eJo1NpNKPrvvLjm6hlxzg0gMWM13+s26vfKYnsYG/NiUVnbRFXxo8V1HZfrb9XvDUFxamx9BZwSFsyk935IbVYX+C545n1P9oIPgTMSeXPDcdOH7uOBTzzI1gc/YGUMK0ML+8BTJRjXTqCj1+vA1nhb7cMXHWKdByRqAfwVp2jONm+WcfGin/+7YBddk0hV+Jb1kxNXckCBFOYhaqapmN1V7Y/leAsZpslBFHWzXjhql6RjdbIdeelnL4mbK4u/SxOkLlBjDUpZTLojoJyAmQnVt/Z6NrJjMu/ZSGpzvXhcVqS4eKogWpwjSS6N5HBFfLs04QmF0VgMWjaoLQYyAfJIgwcWTERlCMUmYXP96PAESj1uZRVs4wAQQcvALwbcRI1QKXqnDOSUaZ3nyuRIe4k+CMc6uHKWcbu9so1Y/rZsYLHkTTOOwoJ5tOySKRFsGNuGUoTs9p2YE6FEmsY2sLPZkHqYdpHtZMmqVcG3YWAOoqoBspRMK1oYUXNFhaHU3blULWMt3Hjbvv10oflbGqjLPYx1YnSySCVSv5Ohm/dMtjcGZmXBwNS+ZvewVq0YUzddG9gJgiSPU2ESWmIX2ZhMmc/nlrLElQmsRN473DCx2dEPt1Bde+2HBIjrPjvfAnmtBvGA72TAl7YOPgyyippBAMC3DTOKpEJ74jQQNxs2XekbpX0rQKvPkJUFc3hZRKbWIA8StHOLep30SpMgqNI4oet7JPiS1sRYpb6kbAkjtvdCiIJ9zGndXtuhLvbL+0GGoWWR4n1JX7Vo35wzE9/Qx2hSiNYzD4KbBPY81sEnrXkISj7GWPTWHixCeshjY398MMZNxSQFNQUMsXgIxBZ9Wx24vczps5Fjvcd3HbmP5k5OatVZnPXXmmqm3h9FyV4sHVBh4VTV7iUMQLEuOqqMB0ZSjFwXJHbyQ+sqQw5N61+VZhzrbsv36raRa5uUh8WXpbuBHBPBObuMmJh44egcjpzt2NpLEB10QANIIjkxwI7Q4Io+uWYMWYAmXHGj68LNv9SHF7sveXIuGaB4DhuIkNrGvgT5XNJn/fbaxWcSR2oRXdqS8bPMH/7V/1F56kXLMVWSgVbNU+2Ap5vMz/2Vv8wrE+Xahz4iyYFvp4OLBspkvI6xu8jWp2h1WFmsWlQt0q8+cCmWfUoNSfqSFOP0Lie/9R393d/+p2y6hiaD2+tpcbh5pHWeMLq94yS5Fdy5ki/owOCgOpHLmklWBFZAYh2YKptVRftLzELRjA4Tiuyf5McpZFbPhfF31IH3zFKPL4xibBxHrr0G3Wi5+Z47OP7AvSYdiR1MpoAaYNIywCGLFeO4HcaXq9nS/0jRKWV47Wvf0if/6OsckTBMBpPQmDC9gIMmBHKRU4QQiGpMw3Rjg9j3w2S5uK5FO6yCRAMNK/eg1l8eA3BZREgyfg0ELQm/y/PTK6RJYN4I933qY/SbjUxuvI4yq9XZy65bTGNl0eAl8iQq7OxBFh79/T/SeGaXTdcQZ3Pa0AzM72w2YzKdDgxP21i+RVnDRh/kbl4HAPUCQOLwnSUX38FMYn297u/qsSq7nXM0vZlCgyP2pgVUJ8w10TWePcl88mc+Jxy/whYxU5M4DOwXiwl2HHW7RJlpQRYnz/D6Y0+p2+t5/vEn2VSPxoRT0wL3OQ3nN5x/zS26co1L17+yfdxm1ZyClMWOYiwoLABNdbeTlaaOZX3NCpBXpCwNKQhzMjs+8+DP/jRHPnKfddYggAy0fVbT1EoBibFLhMYvFjdDF8iWzka8DRjzHrrEmaef1W9//Ru0vdK99Bp+HpmGlm42YxoCAUfuS27GkasYWJRTzAtNpdb7sdJmNeH8ksdktR3zMmu9dJ9W78Garn2uoM51lt2yS1q9pyNDG9ghMmsDv/Bnf42jH7nP0po1xa1vmeHLwVn+C8z7jrZ48Bh/vIZSXFpELB/m3bdzMInj86+u/GoX/bzfRbskQaJttdxRj/9vf0tf+P0/po2K1wV9n6OtmlKKpK0J1370AV4/0vDQX/lLQuuG4409NjC6sWuYl3fbxg/LAGJLjbSun9G27YK96bNFJj/3sv7oD7/GpFce+ebDtOogJlrn0S6yPZ2Sux7tDZSMc6ENVS9g+GWXlwHcamLcOslXkLgaTQrlgRnAnSx/r4DEpePKQquW3TLIXJ3kz8WiiQg+tOaiKi6VXhf6xl4zbtoy18Snv/CzTO+5U9BUcqq5IYLUJsXaURyaIhHLOVfzTkqydBTmqy7R1r0y+8Fj+rV/8XsEBJeUpiTPbbS6st2QsyznDOJwwY90WQe7mw8DEsf6RFi4m4dtJUgklfE9lPPzudzPUKoZOfAbE87EOe7oJh/91V+0JVZNB5LLJDHkMoEhxUZMA3h+9Tvf0xeeeJoJzsobdj3T0EA2AD3rO7vu4Af3+apdiCZxFSSu22dp/5XJ9ycFiRNvz1gsScWRPPR5sqUF2pnPyMHRB2Hrmqv54IfuE45sQeuH2aa6EevCsHoQhAIea2DabGZympjgzIwnv/JV3XntpAUylLaWmM217BY6xGGBWGUklZjS/dd0LnfzKggi58GVOAaJlamuFXgaKaxa6XddiuYCdY7YOH68d5atG06EIFuvAAAgAElEQVSgRzZ58PM/I1y5PaQGql4IFVlaVORUUjSVR7emABNsIcM8gXo4+QbPffWbejRMePjr3xxKWDYqTHslRMU5c9vGGGmbhlm3Zwv2kezFXpT+lheBNge1j4xA4iIF0DJITCvfG++3Knk4CPgNwHLU7dftu6pZdNZohBAsnY63hNF9ELTxPPT5n2av8Vz3sQ8Jx4+WpO4RmlB0o2WBnSzPZ8YAfE08PqQMgv3zrVxinE05mX2wYAUk1s8vvgv23bNLFiRCRuaZ03/vd/Rf/Z2/x0YP0/KJF0fq+gI+lM5BvznhWen5jb/5vwobHlxY9EJZ6HlqqP+lAhI7tSz6rgKVpMWVV06sT8xeekXbeeIb//IPeOVHjzOZJbbU46O5XEIIli9SLffZpGkXpaNG2q7lQaSmKDkcSASGY61GlFaQuBREUPZN1ATXo+ses0JyOJDolCGKeFwjuOb8HqKn8+J1QuliT7Mx5Wzq6FtHP/Ecv/kGPvqZT+NuuFaqxgmwdBfNZOE6qsAEFiLyCsA0Wu7JKDCfg3ge/53f0xcee5JNCbgusSGONOuYthMoDKI4CwiadfNynj8Zk3gukAiQvQGx5Bi0kD4v3IBZHF2KtD5Y6UEycmSD/siED33+p62IvQOkMDRi7ZJyMuCiFMBYHrY+wemzfO13f1/bBEfDhNmpMxzd3CLP+2HBMDDTB7iQLxQkrn52WF3ihWoS9zFFyQJ1KgDzTQlYyJlZ6q3fTwK33X0nutGyeestgkaYNOSyAKkl6MZVgypI1DrZ5tLxNVtKqx+f4o//v9/VrVnExYybmNZQsu3fqAxAJr/DINHuw6IKyzgoxRWmrI5H081Nzu7ukkRxbcOuRvZc5sGf/gxHP3K/PY+NKwBxfFKLN0sBKakIg/siLlYMQCdIL7yoP/ijr/Pak8+xnRxpZ8a0VJmJ2ZLkNwkD1VrY/66nmU7oU4dvGytTygL4qi50hDUh/rgNLwQkZhas6/ie2HcXjGU91lK717Zwy+P6cg6P/WBxXAHJtPresli0jclBCjCedXN02rCLErdarrnzVj7xcz8Nt9xo/bcGBjqPpoiEsMS2mcBIF8UM1jE1lwxCZAmxDi9H51xjQxfttvyd97JdciBRxtvnGf74Uf3Hf/1vsD1X2mx6kho96b2nz5HshF4cp6+Y8MW/9h8T7rpFcEVDUVaecZhEVx6ciwwSM1bOSVPCUZiqzliCU9//kT76zW/z2lPPMYnQJGXqG1xWum7OpGkRVfb29rjiyFFSSoNwWXx1Wy9sPHCMK0KcawW6bqIezn9lUh67pOvfChLr9dbPhv0YRTnDvoCFyjaJvVkSQNdUHpmi/4txUWmgBLmI9/QpQvBEUUu94oQu54Hd+fQXfpbtB+4VQvmh4EDcMBmlrsO3LYixOi4sKhloLuW4SjTM/Jnn9dFvPcyp51+micrRZmpu6hL1a+x3YjKZELMtXX4SkDgwsuU+mk5rpJgRSztT6wlX15dX2zcVjeZWM2E+n6MTx64k9lwmt4GPffanmJHYuPF6ofFk7JmKMeO9K3ja7ohpN0Od/SDCj7/9PT357Au0payYQ4b708d4MJhbdS8fGLji9vW5cd9ZOubaiTyt3eewILHg5uH6VazGb0fGbbSEzSm3f+oTwtYUNMFkMrCHCwZbB81T1Y86ccvpWlKCLLz+g0d12zU88o2HaaOyLZ6+60od8xL8UTwLNa3MOwUSgcHdDAuQKAUYSnntvWfW9UjjiV6Y5UjYmPBmmvPBTz7IBz50nzCdlMoio4Vt0X/VBcnYVS7ekUqKLvoecgOvn+LNHz6mLz39LGlvzuuvvIZTmHiPzntaH/Di6LpuiBR2UUvqmSL1yaXOceqG6i06vu4sa9vsrbqbx0zikvAir29vO8boZ0fj9z53aAXq9bdXjqMY8KljaEoJpGSnCJ4u9rgQLB8jmblkrr31ZvK04cGf+hT+/nst4ErKA98GiycQhowI52QT153Uu2xLgHAdSCxv9oHE0WcX+xreabu0QWKX4ZEX9R/+t/89V8xhkvJSxxcRoiZc8HRdz5tHN7jzN36VO37556zEQLNIf1CzUQ0rgGoX+QYnjWXV7eD0Do987Zva7PZ8//e/RrsX2YrKJFmAjsRMwgbdXk3bk6PVYJ3P5/R9z+bm5gBkUkpLQK4OHIz+Vo3KqjJsXV3dJVaRBXs4/syxDBarkHuVARwzhOPfqSxS3f8gdkjLe+ccfU7LtV9LgMgQKOEs99cQfFPAF42nE+hbxyntuOPjH+aW+++V2Ho2jh1blMMqk3hKowokmGtFS6CV9InGe2M01MHZXdiZceoHj+ojD3+XLd+iMdGItUVwnr7vcWE5x+TbCRJrGyYx0FVdzg4ZNInZmYZtmoS2adijpxdFJg1zTfTBM/dw76c/yZFbbxKVvCh9Zjd4APJDiU0t80bMoB5eelW//7Vv4kvEaCi1ZJ1za5lEeGsgcd3na4+9NDHntZ8dBiQChLqYKP2iTx258eTgOH7zDRz7yP2CJHMti5qTwPu1GljLbW0cYvk1AmK6z6w8+7Vv6smnn2cjCbo7N+1xWRi5YKmYau694L3p/2rO0zoBroCUtwskjtPakHVpWM2qzHIibE7pW8ct993FsXvvtna55pidREyWKB9AM1rc67EEsw1FA8Y12lNi54UXeeSr39TNHk4++RyTecZ1ic2if3VOmM/nkJW2jJfV07Czs8PGxiY5JnSUlqZc2OCNWOoXSfa3je2wr33WgcTa/mNmcV27ngskrrPDAMXxPaljQ4Qyf3ZMplP6vl8Et4ktYGpKH4KnF+V0P8NvTDjtE5/61V+Co1vc8NEPC5KNBXaVAGF5obPOLgGQCCwVIND6fryDs7Z6P4LES9O1PkaLk5ZelKTGXDlXMtYjZaIBSZnN6Qan+ojMOhtUm7B0OFhzLy+Bm+uloLRZgt3E8T1hows8fvIMxyZb5L2SvFqAxiE5o6KQFullZrMZwXkmm1t0XQeAFtBSK18cZKKyVoQ7gMTKmEl5Vwe78qmT+srGtZrfejiejgaqeuzR7y2A4+LvAHoO0X6qavnVynvLCWiDXBsanDP2L3c9TWOBFDknq3Qxn9FIIGQbEK/cyzRz1cZ7sdETKiiqWh4BckzkGAmTSemqDteUNAmNjZB7s9NsbG4wO9oyO9KiEcun1ic2fEOKyXKeaf6J+uEYIGo5xww0NaBZFwP2ah42Y0wSPniyF7qUCE4IPjDrIpojIUG7OSHszCBmpHGD21zEbljMpsdrQzOA2pQjvg0QlW6zkdMh64b3aASNmaCwEdqFS/YnsDGQfidt3YTdabK0XWpgrIuRdtqym3t6jwWmNI5UJhnvvWVrGKUTsn5Vkq+XRZaxFpXGALrMtFOm84yfJ7wKjfdEnwne4cUZk971loxZGfSoqvs9BItecO7x4VxWF3RQwSZDXmpY/O0k47amnModOtngzVa5ss2kNtD4IvFt2lIxxaFYpLddQsm8kPOClcoYeyierd2sV3WCn0U2e7XymZKZz2fD/fLe03rIXU9QaLAF39GNLeZlLCApvnhfXFmc5VTqtsvytY1rT4vWxfD+1rRxT4Z7Wtuk4Gjbh0WVnrEtpc06qP3L8Svgs0Xi4rsV0Kx6i+p37bpsPlFRe2aDw2MsLWLAfHNzk93dXYIX8rzj6s1NdvdmHN+ccLWfshvFSkF6GfRcGdNfDxRQHd/PcT2XpI2xyOr294ldfCaxgo7aiQZEV5jE13b4zf/8v9KjZzo2ujSUNWp8sBQOjvJAK/Ortmg/cS+f+o/+stQVTXVZVFsc/525DmTt2+X96kCXs7Etp8/w9d/6f/WZh7/HFbFhmjKb6kl7c1pvIuGYi5tzmOhl0BymoosCBnfmtGnZ6xbVUdYNErZd1m4f3ldGcIXV0/pZBYkrLr963EHEv3LMyjxWMPhWA1dqNHgqLueMBa4EV5jUbIxrzQ83sIEllZLDUoSk4MjBMffCrstsX38td370Aa68964SDjxQMQtEqwwAMquJ5lPsLEI1g1VuEOgjr333EZ2kzCtPPs/rL7/MRBo8RfwOg3anXmHVFqqOtJG13VbaYBy9bPonYwqH3Jiy0CZWdtZbo6Peai1veItI3pi29DESHUgbSGo6xb513HL/3Vxx3932bFFPHCI6sJfV3c/4dcoDpfnSNx/WN555kTYLIel+xnCljwx2AJNYtWprdYwH2MDmKMYYjcagdUyilhXQPsZNMLF/sqT0STOucdxy9x30Qdi+8zYh2Cl2xR3sCXjM3T92My89dzoaPXY7UM/zX/lDPfvSSZo+W4BU07A726MpUb2i2FhR0u0kFsdm5F6uVT1yGS9ciTJebZ+DmMQa9FGzE1hgnM2k2Tj1knKHAoyVo1cf456Pf5TYCOG2W6yAuHMg5pBEZJB2DGx0XZ5mhZyAYC73MzsgntefeEof/8736V4+iZ/1bOCRVOqwqw7jT6za2b5Dslouz5xJOZPFEVFcCKS+N1a4pOnJOS4tPhbXv/Jel9+vtuUQOOgW+437mGjex1rDslbxIBMFX85nvFBc1ZWvq6ozPoiISUBms9lQQIGsS7ke61hUPTY5JuZeOLsROOtNuvOlP/+vMb3xeuHYNkyaYXxYGq/K30NPw2u+fBBueyt2zmPpaAdZxDUI9fldfPZetksbJPbAbuJv/fv/iR7fTVyRHa4v2i6nQzqOSqXvtp6Xtx1/9m/8T8KRgAahVtxYDw4zlfhfC+bGtjo3jTYtpako17KI/jJdxmKwVou6i55nfvufarsXeeSb3ybNIxMRmCemIQwatqWs+8I+ELcuLUK93oM+uxA7n7v5oECC8eAqIkt6xrErVVcm/32/d8C5jI8zdmOvS9Wzeq7ja9D6ncK8iEgBVVbRZ+aUmVc+9tlPc+z+e4RpMNchFTguUm7UPGFD4mmy+fNThOwNNKqHU2/w4mNP6Y8e/g4b6gh9ZOontM4z29ll4gJhdOVVXzrUyC61uqHkMXPBAiiKiyiXurADq4cBWxWWNJ8isggwGLW/ljbKguVznAR6Z2nUelE+9tAn0eBw118nbDjm2VI0OSjMqBuiHLNmgjgkFTd8nznzzHP67I8eJ+7O2FRP6xZJgA1g27PdlOo3Q+RvyZFXryu4RTm08b0fyyDWRY0O/VLNXVx/I7EABXnpNQYiQhjKrKWUkCawq5EwaYmaOXbtCW686w7hiiML8bOXURLsjDfl8Wjcy8xTz8Q3Nj70yYLuuszOk8/oY19/mI0Ik7iY7LOqBSyoEtTSN60CmgWbZ259LZVVck1BU6+j7DnWranqUIll6ZhVE+hGTGVKJoGZTNnp9+hF0GnDnmTu/OgD3HTX7cJVV5RnJIMPIBmNGQmBLKOkxMoiYpls56CmP+f51/TJbz/MS48/bfpCscVHSNnWcBWLje7x+D6vpv2pn1U3ueTlgfUwmsNhobHSr5a+dkBFm9VzHQPHcx1v1cYLnOH4BxACb7clgeQd2duY04vST1t+/td/hTOt48oH7xcaAUqfHgNlYfBS1a5RAddQhGGsZ6wAUWzXdy2l3ej39+HVd+P3LwG7pECijN4j2aD7Xua3/5v/QU+czpx95gWOtC397gwJDGkmvDhaFfYCvBgif+Y/+w/h43eLTluoSbhXbvZi+F/VQi6f12BrQCKsPIA52yQ8GnxD/aWccar2ZL2+w9f/9t/VU489y3Z2dG+eZWNjwya6Mtmnvuic8rnvz9sBBM9l+wDXmhq6Swzj2wgSy4YDz0VE9oGe84HEfZGApUScz5YMuVYxAFBvedtS45h7mAe49o5bufH2W5ke3aa54TrBu4WWSrBo6mDgJpXyZ6GWsqudb95B21r/7iKnv/tDfeqRR5m/eZapOlzMbAQ/VMOowG+oOKPmGqsBr6sgUVVpnF8k8i3lFC1Fi2k26/sxA7103wpIDE3D7nyGNN6CgMg0raUdmhw7ym2f+KhwxThdCWQxXV0uCyQog3pMNumr2MT/+inoM6deekVfefll6BOaS+L8ZO7rlNLAWtXzEhGr66tqbcvis/G9VdUhDdQ4YKD2BYcslU0bvltz95UKQM45+mQ5Wtu2JRYNbNja4MRtN7Nx9VUWeDFpByGYYnWHLWK0LFRZFfIrWrRvBspMejJ79kV95nuPkE7t0MwTbSrMBQW4swA3Y5DI6Jkr3ZEYu6Ekn/clh2JJy1SZQBEZSuJVMFTBT20fL4uqIvX4PRltW/ZyZK6JK04c575PfxKuP2HPRSPQOLq+o2kXOfSGxNalr0iRDvnKq2dgd5c3n31BXd/z4mNP8eOnn0d2O6bi8MnqzYcR0zW2cWDOIuBkPQismsx1bOrKhqXt+7WZFwYSD9IkHqSFvVQto8SuZzKZggin+xlydJOdFm7+6L186M98WdgIpgnHqpUsESy6TOAMZBGFfBnX4JaFO/9SzXv8XrSLDxJXbQV00WWe+vv/RK/dEf74t/4pk5RhHmlaP6xI0rxnKp6+dbwSMg/9xT/NNX/6C6KtZ0g+MJqjYRkknvM8DgCHg60mu63uIxaDjyus0ss/ekK3dzP//O/8XbbPRqYRXEy04vEi9H0PlDqrJXq7DjLvNBg8yM4FEuvnlxpIPGjf1fPPmBu2mi+6MD86l6SKmzR0fc9u6phsbeKCZ2c+o9+ecO8v/imuvv0D4ppmwSiuCcpYsIsjGwEDIvDySX398aeRvY6XHn96qJrgxaFYeopxMEuf05BvsIKaOsmE0aTu3UoEcHltYGO5TOA+ZrhUaVHvhtrPO3u7+KYhOojOcfNdt3NqtsNt990t/uqrQJREBu+XdKhDLeI+QQ0AKnlBiZk3n3pW/Tzy2gsvEc/s4mJmWhi2cX+o+R9VhHGJtPF9HV7XwIE1CwdVRX3pi8kyJwQsdQzFtZbKwi8J9E45cf11+Lbh6InjcN0JyyfUjurmiN3/1ZQ2WfMAmKkBEiLU2uv9a6/z2Hd/oJvqOfPKSXTeM8EtZSIYWLAR8yWii8VAPYWyXwV+qjowh7Hq/srfVJi4cZvU+1X1vrUNY4y0bTu8P5t7rn/wfq750D1GCh3dtqCm4mYcorgxEn0A4wqxMLO1CpBzWO35eeSpb3xHn/jjh9lKDtd1tCU4yRJzZ8tJWiqnrNPbXQaJ755ZUKDdgHnf0zQtXezpPMymno3bbuQzv/pF4tGpbFx7DBUhpshQjnO8QKss4lhqMFq0vl+jiy+2XfTAlX2LARl9YA4sdGOCRqOzfYx4taSnOWeaibEaWYCsTFTYOflGQQDRaG51w2C1gIVvXbC9dKqlk1fRdWUhgg/UZKPM5iANV82FSXQ0p/aYqOXSawr7UwFiCGEYGKo7/VK3txI4cKkMfssRjfZniNjG+kq/O6MJgasn28RZJOaeq9uWee841gsuNzBTan5Fq9Ig1DwgFTj3NYE5jqR27xVFckIaz55P8rrrdXMzcGYqSA8tQlDIKeMqSyoQRGzeG9WDrWySE3PLIkXvNarNbfrdkvbFO3LRDFVX677k3r0Bgz4nHEqOke1mimsCMSdSAjm1y/HNKd5PLQCrdfhgORTrcwGQnZVZpLUE3aqKtjYh5BQ5LZGNiWPPKzrxNN4xzzpozarWMadELpopqZpb1k+ugyZ3TX8T74hkAwCazeVe/qWUSilFS9mSSqLhHUn4ENjcaiS0YmBXFm0PLEXA1zQgQ91rsElVKZHwCio02TPdS0xUiRG8ayFlK1PnFv1SkhYN6+L4WRbst5RtUlnmChi91eoOJcBG1QB0ELc4fxZALudMVFscRAEfPDOf6VthNrca0LtNw+yKCWnT0kxNJ25YKI8nb4ECiKHvjFUMobE2qG70LsPpGajneG54daZsKYQc8Gou9r7qH6UuEFleQKzYuzXGjAH6+81Sb1r5LvZUrXOLY4rHzxNHZ9DoBHe2U46LiIfGeYaCpFr+V2Uuaiy8l9Eie8QgrguyvGzvrF1UJnEM2la1FVJ3SJmT3/qeXv1m5B/973+TK+eWEkZLZYNsMxFTF+hyYsdDuOtmPvff/VVhCqZJdPsSfcrSu5WTGts5mETBzi+rosWlNyTFhkVwSg9n/9W39fd+8x+yMctFZG3BN/18PkzKVY9VWaFxQfiLZYdhEsd/D8skVnunmMTV/dcxicAQIT7+TMfvCxip7KCICeNFTA+421gZwI7Mz/zKL1qOxVtuXKSDAPAlqGLUmfrY04aGcZLg3Pc4V5I19j0vff8R/fHz/z97bx5sy3Hf931+3T1zzrn3rSA2YicJgDu4iKQka7UlRypZZcWOl1Ril+2k4nJc8ZKyXSnZcRKnstku57/ElXIlLscuK1Z50WpLlC3TNG1KJEVSJAGRBAECIPYH4G13OWemu3/5o7tn5syZc999WB+o9ys83HNm5vR09/R0f/u3fH/PICtPc3kfv2oQn8ZE7VzSejZt0oDanuJnaHYcZ7sZ9rNIIiMXSTXrfBRFNvyYVBWXyYWNSQTczjmsq9lbHabMNkZ5z0c+hLv79syfpuBS+kMxKcDHB582RvTA2WQ/PVESp6R1cHmfZ7/5hD7z+LdSFpuQUt05lUQjRHrWPvsrwgAoDcdJMZEONBKlPWvAKGrHBKCafP7EGIIV3IkFN995O6duvUk4sdNFcCZTePYbtqao39bu0YGubOZNE0gGiYcraDyPf+4L+tLTz3GqmqOHDfjQaYLVmrXApJLyr3ARRgKBdW3p0AQrkQ6QD4NXOt/N/PyLtrFsFtRI4p8lsowenVd4J+ycPc3i9Enuufcd7N5+awrMsabXbnZ+1JqDU1i3LRZXg+IrcRjgxZf0kc99npeeehZ/sMT4yK6rCauWSlLeZGtKfSNt8GndcClYZTxOYVMj91pqEotv4tRaOtYkTvkjrpV3Jd/Ia0hEewuE14itUuDgqm1xeQPXChxIYH9u+NE/+oeo3/cuSbmh8+pvXOfS0c1beZNT/BYnAeIWS991efXlmgCJ0PvcbDimhhYu7MGlqD/9F/4yb20r5iufUgTlHbC1iTjWayTWFc9ULb/v//qbwskqZ9A4AiRmLeNapYZyJZCYDwTJJkVNmqKURi+RYn/xn/yCPvuVr7LTknKrtp75fMalvcssFotUbtEetmkRLuTQ10Hi8UHicep3FEgsILMEswAdZ1gyEUmn6S0ZEcSmzQkzxzJ6fGUIM8ed77qXWFtuvuuuFPEnGUigKUrTuRS1aSuIoVv0lGSKq5xNke+YZGdZtSlH8qXLev78eS5fvMTh5T0un3sJx3okYkkBWMzOnQYu+zi6ol3zASPuSJBYzIwuZ1Qo4KItuaiDpgwVMSQ6GGdoDTzwse9gP7acuPsOSeTkScMUYgpMiJn/ThBa3/Z9G2IGkNJPEKuWw+ee12efeJL9C5c6IvDY+uRvaWTNxNo928FzVtUutdpaFHRIKRxjjCkFpnMsTuxy8oYznDp7BnfbrUJoU3pCl55fEEUlfXYqXTBHp9UqY0xBvUcKwXje9LJc8dKTT+vXv/wgs8azEAdtSODPR+auQkNMUcKq63RQZdxlMBGjp0Soikjel/bAJOa6DcdH2XwGMm+jKxyPySQ9O7HD6TNnmJ/c5S333JX8L07twrxO/eCb3B8WD/joqYxbzxqlBTCmQCZ85pSKyuGTT+tjjzxK7QNP/9Y3OKmOdv+Q3dkcCdoDe00KAKNAYSYwBlM7PJq022GdhPrNAhLHmX6mQOK1DBAhz505Wt45x0G7wtVV926F1gMCtWNl4WBu+MAP/wC3fs/HhN062THz5ipoyUyTf5u/Fx3tUJHUV+B1a+pva7nmQOKk3+ClPdiL/KO/+Ff1zgNLvbckEjCVQ8kalOCTo/285hkafvyv/DnOn7Ccfdf9knJOmjUPxCtGfG0ZgNvGadmJO5cAon/iOXWx5os/9c/Ye+pZnE9ms9qYLpKzruuO13AIVMoi7wckvUdFMb/WsgbOtoCw8vnlgsRy/PUGifEK11RVRdMk53+soW3bblKsMiH2bDZLprkMRLwopq7RynLoG5hVvON97yY6w6lbbmZ+910JeMyqtIjWfYS0b9uUM7qkHmuzZm38nBXQkCMZAixXNOcv6KWLFzncP+Dg4mXCquHShYtdruZKTAIioU8pVto8TOc37GdjTKdd8t4jpgedAcWbpFGrrM30Iwl0tMGjtcM74Z0fegB7522SuF8k3ziZmBIfZX4rfcoLSwjpmGoCVc4Ve3IGZCZtwJYrVpf3UB+0Wa1YLpesDg5ZHhyyWq2IGfR0vsHWUs9n7OzssDixy2w2K6BQZLHIgScChbtyoB3sgZ+ujckSpNONt/ygNMQUnFEAvgoYR/vNJ/RrDz6EP0xk2DPVRHrtes+f0HosiRamLuwM5X5ZU9lnNwkbU1Uy2aVn10TtqGgO2hW2rrDzmt2TJ6F2LE6fZvfMKc6cPStudyf1gbPrWsACOock6nmHHCTgNVIb1/udkoOT2gDes3riKX3y4UeJByvOPfk0NYa4alnUs0wwn7SLq4NDZlWNc479/X1ms1kOrkrgQbIbgI+BIL3v9rUKEgvInSr32wEkQhoSbfZV9d4j2ueqNy7z9BrD4eESrSyrueX0Pbfx0f/0Dwpzgd057NQo0ERPbVKAU8mWtmZpLP+gG2LXceJrL9cESBzvEtbzS0akaaG1/NxP/jU98ciLnPUCks28mQ/P2t5kcnii4l0//sM8v2v4wB/4ccElgtYiGyrrKTkGSJwsJ0Se+LXP62d/5pc5eRA52UTsymM7jY52GRIa31JZtzbxDE0/s9ms4z377QISN+73GoLESOJyGwIl6M2Wpf4FFJaI3/KMiikxhGQeNMbgKPyVeYJ0loZIsMksvVJFK8s977yPm++4jbZ27Nx1h3TAKYMoZZMmYmgyTD5emgCNDr/nq30+joFLl2HvQHXVcOml85x7+ln2LpxyRTQAACAASURBVF1OpMuFGmQLSCxUMF4jVVURB2O0gBXnXMoZnDULRKWez1i2DdEZvBNaJ0RnuOmu27nl9ttk54az+QUaBH2kHk9t2UQ+/Vgoc5YUrVX3tNffRSWTLksCPZl5oAN+BQBlTdrQNFpIZDqia+iprgZ1CZJM5omuI6Y87JA0lGK4/Nw5nv/WU/r8E0/hItg2ElvPTj0jLJtOC9zEgK0cbdumTWJOrSeDLFPleagk0JeqHDttDtBTuhgH1nDTrbewc/pkCrRZzIWTuwmIuyq9ASUqXYBMYC2m91OMxT+s9GcJNOrGYSwVSwCx8ew/d06dV77x5Yc4/8wzmCYksmtXU0UwEbT1SO678n4RktnRN20isS5m8sFckp5JJruO6wBxeM0az+XwvRldd62CxDcDQITk7lAsDyJCaGPHtRg0uYJEH1jMFgSUw+hpKmE5M1yulN/7p/4EsztvFeaOQMRI2iyVjETdMNPuhv17eh0hvi7yhkc3l+zCouu71MItZolICNDC07/wr/TB//fnuXGVcmtWdU0TYjY59wv3vgTOvu+dPD1XfuQv/1mhNiCWIdFvl2roGCrs4e+gn1TS72M2H1rw8Mgvf1J/8xf/NTepo14FSkokoz2nVpTEOQdJgzoFkLd9H8vrARJhuybuSuZmyJP0gL4F6IMoZFqD2AeTHA0ShxyJx6tf/7mARKRPY2eQHJiZgaOYzuwnts8gUDgHVWKXDg/AqV27X2cqNCa5JOTfJW5pg1YVjVHuuP8dnLn7Nk7de7dEEwlGiAhKxGE200mmjl3rH8bXDHfeJVKwLPRiYNmAEdrnntO1OsdIVVXivdfhcxjOFSUiugugkP6WQx/I3G8SJGfDqVzKT2wNZ269Oac1HPgbHUPDv7FR29bmqXd7vLhc4f2aqk6cOF8CVEzWDOIDl85f6Dj8yntussti4acUYuc2MzSHGwVTNGhRpzfRsn4sqlLvLISdBYiF+SzVsAOBuZBx+ovhORmdo58rNZuTi5aHqOjBAReeO6cLcXzt81/i4jPP4prAXE3izM75o/t8ztp9LmA3Sug0+ianiClgskgQ7YGxJpCVfDK3gbDtIHE4jqdA4hTYky3HC4gdAvW1343A4xRIHJ4f1/talrRpzi4usbyLpjsH/fpUotDLHGgUDio4Pxe+8/f9KLd/z8dESZYERVNAkx1s3rqbjipxHSi+5vKGRzdPPmMdDDKy2cNGLoUWapd2mi7tumf1nCZ4IpEQk+mitpb5smXXZn+urLMcAj0g5zg9uguKTxqwHrzQLbiaJuRlgGXkppXl5mDZaSLaBLSyOWgha0bHk/21PQ8cW4Y7/tfq96/0HlNi8kLTmUolT2yjvwo5iEXWQMJYxmkGoQcybgReokRCs2I2m3GyFU4FCw0YZ5IF1kLnIJFpWTpgWLRiA6BYDJ/dxkLS78SYHoWpJoAI6Wrfsj83UnzW0sInVEbxqJgB9+Fm32nPA5pfhzBS7Uge5EYhWEmbuZiDTnyLVLM1t5OCZ6ZV9hM4eXC+u2xqUZkAhmv3HV82wkxDRca4aopicxnBh86nrqoqfNMS8jSU3/21AVy+JCXwhGY8xO6aob9l2VyFEFIlbYpoD7XFVWkVrirtg7FGty6K1ARWZG3ApnFUkGim8Ir5WfqQIsoD6XMrnGkN0rScagUJFvHKLAomaoc9h3hHGc7vsXvvKHUp15W+V11752Lu7zdSrnUA93pI4pTtZfgubhOr4GL6ZxUwhhsbCyuQ2qZxZvJ49h6p3JXnh+vymsobDhI3ZH2e6IGYRt71wQf4+k//EqcrS7NcUtsU3KEoJmuqEEFbz0uPP835mcJzLyq3nZXOSTZrVIwxGyS6U1KuKf6BHUiJiQ7EWAvLFV/9xV/Vs43hwU98ml0v+Daws7Ng6ZPPYcjruglbmn19R3TVolOatKuQzFiTAyGSDEuLmestRs2aDtZBGH2mBx0UsKbY6kBjj1bKz42CGEtoGp792jf45oMPEaxotbvglnvu5Na77kDuviOhV1f452TdSWeNe1HKf4MKjlDO8OrKQV1xajbDWJs0RCLTfbptUi5ooyt0AtF1ZnDN/oe9ZnbqNmWxmQLjU8/pSLmK4TG5+OjgHKk7h4tWCrDrb5J8Vy2IsGMEOyCRnpQwmBCEgSZ8Ct2y3qcomMEUnsnSkaTtW/OdzF/jYAwLBXj20HdYfNcfKsmv0lRw2ICP7D3+hL7w5DOce+xJaBt2qhnN4TJpATEELe9MsaCsa/7KZsoGgxsBrqBZ45QfdrFol79KOp9A7lGde11eS0lpAVP0fTTrGy4T6XJKD68vljXN1ywOWn7tn/1zPnB4qHu7lju+77sFCVAZpHIEsp5nINdpcF5fuXZA4nBuXAOJeRawDm68QRoRPQwtJ+o56ts0gETWIvqcscSDFXWEpz7/RW6/7Ycpy0skaWPMmNj4CEkBKa77LJK46EzOS3vpyw/rw5/6HGdXwqJRZtYSrGbS4fUuHmpavp0nuG1av9dCG/hK6jNO1zg25RUfsO6aQREl+GNYTsw+dckvsdeyGU3E4eM8qs6AeEGawAlbAUI48LzwW4/y/MOPcaBeZVFz8i1nOX3rTZy68QZ2Tp0Ut7NI5sQSYKA54rqAPNWBv13WQBbXsmIuNCkrSvLZBbV9A0tavfL+bfTcwLy/9spOaQBl0IG5jpJuMqFyLf24XtyGInBwYu3+Y4y1Ue/1r8efBfpyRQaelLGseMN2pM92dgWACD2IH9Rt2OPDKPBB0UCO/ByOa9u7zxSC/+JHWW5jPKny3eZ78M8MvHGbBg6WXHjhRbURLp97if3zFzn35NPMjUNbj4uR2cpTiYGQEhpIHuPFH09VCaPH33XlyMxaxAA+j5OCUW0cPHLtzZeDYb0mx9X0TfkjXq0Uc/Q2GbrZfDtKssYcLeO1rpicT7ia9uI+X/yVT3LeBe645x3KHTcLQVHXp7QURs/5Okp83eSaAYndBGBGC4OSZ0aFec1N99xBePRJmmWg0hSxWLQPmnfX1lqsKCfcjN/85Ke5/Yd+AE7OUHrH8xjzIrhd0ZJP9AEKqtksWRZPH+GRp/STP/XP2G0VG5S5c0hITruzRY33cY3ep0yYVgttx/Sd38hAlVcibxQIfLmy4d9VxkPRYgw0gwUYdPQwQJm60jBdb3eXKYN+Fy2jBaVtG6x1qAFjlFVoU51EEBV2K0s4aFgdPscLz73IOQwxRk3p+Axud46d1ezs7HD67BnOvuUt2BvOCid2U+V9pm8RsjoJxCbtkapipDfcDXkcS/Bx167SL9v8uybGZWculEJrJYN+y++0TL8BR2oMRwBxyFowaMyx5GWN1LGqeKgp7cCNdkE+3b10/W9Xg9y/kXWzqiAgfRqzmHkc16LPxwFiuVuRxEUoSooQ9wHEZhVdSY8Y4aXzhEsX9aWXXmLv4iWWB4c0yyXLg0PaVUNtbCLcDikf+O6qpa5SMIL6QOUcToTV4TK54pDyfXdWHcmR2LnKEosSPAd7SB+L0HVp0TjF/nsmjyKMfPler5nm2xnkvVwZrmeJM3Hz3JQvrc+MWBFoVivO7Jzg8uUVO075+N/9h/wHf+a/gLecXLtX8b7p94D5Xbj6bd51uUp540HiQA1QJvs1PyERWK0SNYM13P/A+/js1x9l183wbYvkCalkS4hGEzeVD+jhigtPPgsvXVR23yJBSNkwSlTnMWaYoTO/qnY5R733OLX87N/5e5xdgWsVF5QQFDUmuSm2DS5Ha401Vtc62Hs15LUAjK9GmcMyylgTehNcMclNyRD4daa50cXdRKaaxuUIHAzvn/xbybx4iaYmvRJC6wMum+4ATAidw7tFUAu+XeLlkD29wOXHnuZJYxBr1KO0ROoTO+yePc387CnMvObkDWfkzE03wqze7MeskcRIIikWk4DKYKx2jvXFYX/sQzcsTnrzca9UHJQ3BtXDL0e9H8fUBg7nkysuJaWSA83k2KixHc0OdhgZqAk951tX7amuWlMkJpagqfomTUrZWHaIvauXlMOqCfyJwMoT9vbZv3hZzz/7PDYoh+cvsXfhPO1BgxD7NIQxAS6nwgxhjgFmfVYYBCcQ1CBtitxWK7RNg7EOI4KzKdVayAZCf8RDHAbx6bi9I61iMTeuaatyfa9DhDdO1hIXaK/hLefGa5xo70JQrrN1hV811AhnpOLyxUM+909/Xj/yx/5jEWdSPgO6V3Pipbwur7W88SAxS8zhA5Mue1XVfXQnFujMERpww/y4IXaZCSKwcBXRGM5UDp5/Ee68MZvXsmZnYG4+aj3qotcUTElKHwKuquDAUx16dr0jrgK1dVCDDyGp043tOcZyHlY7McKHL9t138TXV4Yaq7JvsEWLJv26W64ZbmgKoBoGq5isJSmR10O/nAI8ivQE75qi30Ofacc5h1VBfURytgnRlNbOKBAjqhERcF2mGCAEJKSocS9KvQrsrAKLQ49E4cTCK0ufcprZKgGKAvSMZF5Ci4gbBH1pf34ciT4CV9v6uDNxAyGGjqC8N6Ry9ItYZHSfbuFYO9DfVwZ/t0kxVpRitoHLNZCX2+0Z5GfPJnphwJ4wdb9RZUqGidITFiDGLtq5RHnKOGomZD1qHnMdt2RZViPYJnDSK8uVx0WIjScG8AgSk4+2pbeuiA9Jy0dOJYgkQm4A6U2zhacxueGk3MshtEQRiBFrEwdoD2x7bVyiXSpoIv3rAGDna5ubULSNE2NjG0B8PTWM1yU/u/y81gJZJhQiJrIW6RzEoxJYmBmNj3AYmAdJmu5VwNW2z3fR7cq3bLiuy2sibzxILOsTvfv0xk5aLGgAgZvefjcrZ9jfX3G2mnV+NYlTSYkmkdGqWGLb4A48z331G9zygfsRR55pTe+fNV2dDS1RF1HXBIwROGj5xi9/QneiEA5XLKoaVeXg8JDZziKbdHStTOh3W1Mv0IZGauLc1A7turw6MgQTSt4dly+wgTSuROZqjtj1jtMHdi5+SgaGmoGV7XbRGiOBtMERSAu8EYxmouHiX6UpCtsZgz9Ysbc8x6VzL7IKHnUG46z6GLHzGjGOeragms+oqgrjLGfPns2EyspsZxdPwBgntjJUs3k2X7s0vuezrvNkqNrptIiDSNnSLxk8bYDNY8qalm/8u8GzGuj3NkX7Oo6LMaO/U1XsTPFFewh9OkcG5smJCWYMiiUWNET/rw0Y3xCbFhVN2T1XS2ITNLYNoQmEZsXB3j6iStu2iSP2cJ9muUIgBZL4wKKa0RwccnK2ICwbDEoVZZD2M5l+LZI3tpm6JacILc/JWINo7ObaEDyCoCXvNemZag4ilMwGMI5wLu+WyrqmcPIx5Yet5VkNNPkb115jJuGyCfp2lsj6WjZeu6fWubXfx8h8NiMsk/fhiariqa9+g1s+9Wm96fu/W/CUzLq9fHt36TUnbzhPYidlApF+0ihaHXwAJymv677n7/3X/43eehlOLD3zkLI7lmi44gdR50JWRji3Y/iJv/M3hVMWdRUBSea6bqJKQ3sMCJS+LomvEVgGXvrCl3Vx4Pm3P/vPuaExmDYlN2+ahmq+wMeYyLCrlF6rmIjKRDeM+HIlvdYgOgym+ROHwKLz13mdXpgxaXZfEZm8bsiXWII4tpFhd9raifODL+nPKPJDhz51w3tugLBRR12h3hv3H5U7RTw9PLbB30jRWB1RR928p+ayxvcrGulh+zvuRgCJa/eL0re5uwYga8d7M7r0ZUkEV9Hi0Si4maVZeYwFm8lvQoyYWcV+u+Ked9/PQWzxleGu++6TRgMnz545UvNXSMPXTLNTU9IguCI1qJA6H1OtUDZ7ccBrOXw2WeM/1P51fsuDMjY0hNr/fpg/u0Tey1Q5PnDxwgWefPQx3cVg20Cthqe++TimDUhUZq5CNdBGTYukGpp2ycLNwbdIEAyRaNaDIja0aHn+MWqyn58iREw0IGneLBuLIkNi6E3uv1GIwjZ+wHH3xytzEU5xHA6vTT7ho/sok9cN6ze+z3r9/fr5/OLKxPVT5U/11ZAfsSPen2jnlep3zazNW2Tokwh0lDhDbsSxSD6YNgmakid5T6WCGIe3sGeUvYXh9/xXfwpqhbfemHLBm8RGikgXWHcdL7728oZrEscagaHvdScue+pkc+6tb38bt1xWLn/tMQghcbDJOsiKkoDWLAo7CP4b31T3ofvzvjkALt+rOMBuSikvaMCJpDRTF/f41X/0M9ywgtNeIDRpoTSGxc4Oq9YjwLyuaVfNBjej0Jsgx5GusF1TOM493fn08PoBxTezvJ4BNVPUPBsKr6xl2HA1GFUxbqnyONCmOza4dxlr5L/duNK+PjFqH1CiIJR+ytkS2oadWU2Mkebyipm1GXDktJGLBU0bOb3Y5fGvf4P6Laf54Pd+lzCbMatdYh0wa0bl0gHpnms8fgOwVkDcMGJ7/PzCKPvHlFowR9mmQk3Kq94BThLYtJbOAb7kHS7XQk4TKJQcyP19Bn09OB4HpvWuKkIfTWwMp8+eZXFfJdXBis9+8t+pbSO2CZxwNTYqetgQvGdnNmO5v2Q2m6FaEQ8bRKEyhhDY4K8cRw+rjuYZTb2bxo9BNuioez/BYf2n5qopmQI2VzL/botyvtr7vBFyrdTjjZJxVPrw+9BHcZt0OebrZIkzbWReCdrCr/9//5jD0wt+8D/7I+n9qZOGOnJ17CTX5ZXJGw4SOzlqMIVAIGAlpZq6/33v4abLyme+8ViKlIq96SJqD6KK1i40LZ//t5/mYw/cDzb5MkocLBQFleZFWlkHnDOx0LTg4YUvPaSn1XEqKHVQ1Dl8CFS25nD/kKpKCc6jT7Q5XYaRHAHZ7VCRSQCwzcQcR9e82XDhqwXSXq1ytu12h+WP7zUJ/o64/kr1faU8j2mcbpYv9JrmzocRUkArCRhK1iYaTdqkpNXNvx+U54yjXa5w1rKwFda5lBfZCHZesx9bZmfPcBBbHvjY72B+11sFiWiXnUbWfQI71NGDQx3AlLEfX9bNpDplbVSi78mNKWn5oibA14b0rjYttJ7l3r7uX97j8PIe7aohth7vfUpJWDnUCNXOnBMnTlAv5tR1TT2bYXZ3hPkM6ppEuSC9P0zOGNPXMdUvhIDJXKpCn6mpA4xC9xmE+uwpOBn52O//vXLhaw/rI196iFVQgl9xerbAuYhvV+zM5rRtC6TAu5VvMJVLWmDfG2s7kyx0wLinaUo+jtH01wl0GVGmlbdXBkBXumbb+XGqvau976shVwNeX5V7XcW7/mYAn+OI5nLsqO+aDxTLmbEGscJB00JUZtaxEEd72LA6d4EXXjhHePwptffdKSgEckBdMeW/2RbCN6FcOyBxIBtO49ZiRVKknYW733k/ZmU5/NlfYG7Wd7ljM611DiHw8Bd+k49dXsKuhXkNavPkv24+mZiyMgK1YODjv/TLnG6VIBVL32AlEXo7U3VBB9ZajDEdp+KQDmBY16JRLEBl0s9G2PD7KPV8Pd+PV4P38EqA6ahyrub+Lwd8vZaaxuPUZ2iaOk49hmWOf7OW1Uc1BROkLyCFxy7vjTKILHmpgc5vruRilqhYZ9CotE0D1mCsYT8E3vnRD1Pf//asiAugAbVFi6YdYXnvxiHdRizfal3LONTSDc8p0Aaaw0PatmV1uGS5t68H586jradZrhLbQaacEs0k6QrahhSraxyVCFWMicJFGoIV2r0lF85dTH0QIyHBVg0G2hzYs3v6FG4xY3HyBGINJ0+fEjurmZ/YxcxqcDYFgIQcDWxtCvyIPc9myLmdddg+q2CEM++6V77jvnshCOc/9wV98muPMNe8GEpyj2lj2njWsxkrn3wQZ9b1fs5Dcyf9Rnlj7LDdktGNrSscvxoQUzSaV3Ovq5WrLWc7OD1iQ3cV5b4c8PtmAIZjOY7mdxjJXsZqybITNfleV85BVEKIKe+FCIeHhyxO7fDC8+e45f67UxkxYqy5oj/4dXn15JoAiVOv5hAwNasVrrYYZ8BD64RZFA6tcFJSMhWVmBngh9o6WLUNi52aRaMQHXgl5YnOK9VAzaGT9xdiCMn8pYGVNax2hAuryEk3w++vOLV7koPlIbZ2qAg+BLRNEaqF+2xMF2AH/jRX2n11/lQycPQ2x1Pnvx5yrXMjrvmJwRXB2MvZ9R9Xg/hK+2rt90Mfre54Mh+mvNIFECSTspDGj1WwoY/AVkq0agadkqNbNWkknakx0eT0chE1hmXwhJ2Kgwpq8eAckPgYy/h1w7d6rLEl+2b5nPaytCnTsXSEqZraRlRoArMGZsGwCFYPg8V5oFFWq0jThPSuDjaKLpOGa4gE36Akk7FzqeecKtqGBGSKRlUSH18wMAM0KrtBqVplx6e6nPQoRoXoIOQ0ddmZBTv4HgPikr7F5DmnXIYRQvExtZJ+swpcNgE/s6AW6y0HB0tOnDhBaFbsHS6Z7+6gITKbz9C2Sb6SA63YhvaGdQAibG6mXyu5WnD1apbzSqKcjwuUr91Z7/WR46w/Y6LtkqUqCrStZzabYWKeuaxlv21gd4bayKV2icwqipuEswafg+HSpuu6X+JrLW9o4MrwzhuZL/LHkjFAJRJDwGJhv4GDlp/6i39Vb73k2W0iahJIdIN0QAq0Cn5meRHPT/zpP8Hzu5abP/J+oarSZG46EpK1unVAs6i1Y0yjvYWnPv0Z3Ynwr/7pz3MTM3TvkMV8TrtcsTNfEL3HNy3z+RwfU/L6YaYVo+uT9NRkPdx9FdX8GGiOfUBea5kK0hgHcpTPMf8tgKwswOPrdOB4PzY1TgZ9SFw7HukjZY8TUDP8PBU8EieuWRMzfXx8/TDYZK3+43bJ5vG19m2pd3+wDzwp/VNAYpReU10CY6wmLryO4D2bnUPihVq7l+SGWOdofEtrwOzMcCd32Dlzire+953CjWeSzWmgQUsFZ1VlHKgRx76FCZ2mv00DrUdXDauDQ927fJnl/gHL/QNC09KuGkLTQqYJMgr4wJy+LSWverq9dv/K8ZBT4JUxGWOkyiEzQyDVaVSzD2LjWzAmL2KG1nuMs7QxEVQXQH7y1Cl2Tp/k1A1nkbyYnbr1ZikcrwSf6Ly68qGhzYTmioma/CADcO4Cn/n4r+rMJ0CvrWenmkGuNyStiiWbi4daRB24GExotta0iBPnjw7QOF7gxXD+VNUN7fbWe01w3QyDPczod2N6nQ0QNwbMo+tEw/q54nOh0+0alz9u17gd3YZ0mxY2Th9/M2oVjyNdgFxeo11dsWqb9J7FALOalYWXdMXv/gM/wWpRc+OH3y+4pB6JRogm2R9gnWD7urw2cg2AxIHn0pRKcRj9G2PihGuBZeQTf+v/0PCFhzm1isQMEuuQBmIwmk21QqgchwK3ffg9vHRqxnf/yT8qzBw4UDFrASGUKgxf/o4GImkGCFmzceEyD/+LT+g3f+PLLKKhaiK2DcyMw1nD/sEBZlaNdlvjfZXZcPYdpnsrVZkCi683UHytQeLU+SLHAYlTf7fVr7v/CCT25U4Az+E1VwCh4+jmqwGJ6583Qec6SJyux1CTONVumfj9elsNaoQg0GpAFjN2b76BO+6/D3fnW4XooTIEEYyVDFRKS3PfdrWn1wpGkh9du4KVh4sHevjCSzz//PNcvnwZYgJ1lqT9sxkQakhP2lqbNOihzz4yBITD/hwC+gib52MCZsNxOly0Lem49x4dbg5ypL6PIRGdD8ZwoNBxQbTC0recfstZbrz1FnbPnqa64UwCjc6CRKhMZoiVzocRQFuPGMczD35Nq1XgWw99HXvQMo+C+MjcVclP0Q4AjGyCJyntGYLEkQZsGwCCTdeFqwGJa9rLuHmP8v2o8sbXvBKQONW+VwskHgV2VTejsqfa+2YEiZs+h0d/H65/kvu69YFQW5qZ5aLxvPd7P8Z9v/t3CqcWQIDaJga8okQQuo3SNi7S6/LqybUNEgdVK6H2RgPiI0TLYz/7cX34H/4CZ5ZHgMSgSO1YWcOeE15cGP6Tv/HXhJtOQi0o6yBRJu6NMJjAByCvUVgFePhx/Sd/9+9zUh3VKjDz4GLEZc6xtTbnLPXFB8xoP8iHmsUCFMeZCcLAh76r85sYJK4lgD8mSByeezkgcXivDW7KrtwkW7WbE5lGhvc+CiSO63e1IHHt/KhdZnR86PPKEfcp13WaRTFEKyxOn8Q74d73v4fq9lsFZxMwycT03Subk3yYPIF3RiDNbYgkCqsLl3jx2ef1hWef4eCli+yKS8ETGQh0Y6BoYhScmG7RdNamBSJExNnOZ1CUNbBWYnaNMfg4AJQDIGWzA3zXF0p3b1VN98j1MSIp8CMEkKRhLNrIKFAZ29HjrHybzpc+tYZghb3VIVI5Tt1wlrvfdg+zt96S0pjUKSAvRRklsvGeQFvT5LcKXHzwa/rYl7+aSbFb5rZCgu8AzZA6ZkjNAiPQcUwAVMpZP/bqgcRJ/73rIPFNAxC3yRR1W+y+Zy24gGLwAjqrOHnrjRxW8D1/5A8JbzkFtUFN8qeOKC7thpKZ2Xusy55ym9PwdXmV5doFicOBlh15UpaLmBzEmwBPn9eP/4W/xtmDQDRpYbIhOdqrS+TCJoKxlpUR9p2w54QP//4f4c4/+KMSKjDSu2VecbwpHUjpql7+BaCNnPv0r+u/+4WPczo6qrzztzmpRYgpKlKFZK5q2y7zBNBzCxaTEZJMUErnfzgGi683sfZxNXVDs+14cV7jopsAj1P36cDQsP9hgydxW/0my4StGsErmp3L/WwPYIagOGXcGUbAbgOB6/XYvF8P4sa/U+iibMumYpjRp4wVNcl/xxjTgRtIIClmn6D9doXOKw5RdOY4detN3PfhDwinTiSOUmOTd601a/6LFghBcTab1cr9A0nj/tJFXnrmOT3/zDM0ewdIE5KWTKEajP1ugzQF5ifGeJfJJmtMx/65x30vCpicun4MUmTi3AAAIABJREFUuqIksDPc2JT3c3hsXFRAu/E/rGMQaIjcctcd3HzHbdi770hRRVb6HeuwO8pcs/IcPvm0Pvrl3yK8dBnbRmpjsUGJzQon6Tk3wRMEbOUIIayZoqsMvH0O3NsGKDfNqZs8iVO/Y1SebAGT48/DPp8GdaPevQLIGpY31jqnsRXWf3MESNwGosfHu37OgYzDem+st29ikKhCMv/GpKCZVRXNwZJ5VePblqqqkssXkaCxNxVbQ9DI0ijm5jN89Hf9IKc+/IBQG5CQyPo7jc2EpnDYPccEieV5X5erlzecTFuvEiQq2Qlfgef3+Nk/+Rf1lkOwtSH6QE2a/EKe9asoeI14awm15UKz4sT73sEP/Pd/TjhZkQlzJuq1HmEt5eAIpKSLlY6OIwLPvcRjn/qsng2O3/w3/54dtdQIhNhNyo1vqeezFGVZJiAj6ADoGJLLZInSLBJk3cfx9ZQNc/HrBBI7kHUFkDi8Zzq/Wf9XEyQOf38lkHhkeRO+junzdpAIIMZ0PmZjkAi9ZlCcxXuPiOBcisgHmFc1h82KMHfc+c572RfllnffJ5w9mcChzUN7gFj6V1T74BSle2kuPvk033rkmzqLwsEL57FNwMVIJQar0vkvFgC7TZu71j3bok6vMO9vA4tjZoGtsgXcFGAqcXvwwpHaPHoQ70VpLITKsCTyrg++nzP3vT1pGTNgDEAbkvawN9sDTz6v3/rNh7BBOf/cOU44hz9coTFinaONKUWoqq75cpocuJSSF2yv46amLK5du43Metgn1wpIHF7XayZHIHkCJI7LHYPobSCxc2EY1HubpvPNaGpOWsLkduGM7aLtQ+uZZXcIEaEJnmp3QUPkkIA38EM/9iMcVobdD71XOHsiOxdG1ArIgITeTMTWjrvnCu/wcJNwXa5e3vDoZhkCNGHz82BAGMBrAowIsKipT+zgmwOMWGIIqCUDyqxZtBb1io+KVTg722F58TIY14PSiUE3TBEIAz4oXa9v0AhiMZJy7OKVsFtzftdxcOi5dNJxcHHJyWiZqVBL0iCe2DnB4WpJLYlORCSDWWJKr5ZfwJi5f4fO6MPF7c2Qok/LM3sDZIoP8Ti7ys6sdcQ1a5qzwTHoF4jjTkzjwK1xPc1EOyBpsmQAVD1ZQ5K/x+wm4VdN5u0U2jYwm83xIXAYAnE+o3FwKXqWDlg4sEpLQKhzPySTmaxlD5E+J3PRcrWeUxh2lx7jI8vLB9TW4TCJ7zS3qZhjI9OpyzYj/rf05RgzjPvniAdodNNDuCt2YqHeAB+SApvL97GbiBTgXH4rfR1LSUYEGyLzAL71VDPHiZijo7P5uUu/mBfMkvsalMNK5eCEU7PyXHaRpV+xqB0SIhFBTZoXQUFTnmZrHRqTBi1sGeFXIs8+LpC5GsCjZbN9xPlrbZl/uYDuzQIEjxJRkBDZnc1YNitcXbG/XOKcQ1DUGgzCrF6wCoHGwj6BuDvnRRe5bD1vm4OagBgLxq5ZDI1xm0rDciCyqWnfVs/r4PAVyRuuSbyiaFqgiiN8Id4VBDkMPPS//x19/te+xBwDK09VrF0mOYNLk7jFvHM0GpAoXDhh+dH/+S/B27OPVaHbKCJFe9J9TSBx4pqi3VRCyn2KZIJbm0Krn3peubDHL/+Df4Q9aHFNYLees1wuWVQ1eA+Zi04kqeJxJlFwaOzMbWPfw85Hk9cfKG7TxK355I2OraUUnNAkjst9pZrEcXnb0umtBTJMtE8nylq7dkCqvPY7s1n/bW0cfh/XR0aZBYb9ZTRp2NZ8CQ39mFGonGO5XDKfz4lG8AqtgSZ6ZF7TBM/9738vp992l3DDqdS/zhAIYG1icsnjz0rmASzviw+Z7sWDOnjuOR578Gt6+cXzhL1DdmdzrErnU1iyK5TnH2Pc6KfJPj7i3FhzWvroqKxGQ9kGkoocZXodBpOV93CoXZy6HtanEROV2lU0TZOsA86y51fERcW7v+ODLN5+d0pJJpCChRxRA2Jzy7320WyX9vnWlx7Ubz3yTSqvVJrcVZwKtRFi69HWU2Vf6WL+XjOVD8yo42PFJ3F4bJsmTEa/PUqruqaBnCivfB/79gGdJvE4msnuJ7GfQwxxZCY/WpM4LmtKQzrWJJZ6T2tGN+t8za/JWURBfUhmdZtMyeoMUZP2pY0h0zzBSiLLSvjo7/w+bvuOBxIrQu2gSn6JFlKqPdbnx60gkfGJ6/JayRsPEkcalK2a5AmAJqtI+68+o//6//6H7Cw9s0DKIIF2qqCZWFarFd6k3JCihr1dy7v/8I9x+0/8bmFmUGM2gZb07BzCpkF6CCBj8Djr0g7f2H4wawSflpD41HP6rYceZtEov/4vP8GOWqRpqBEcQq2S2HhCTM7w5aUbaQwL0Xg5/nrxnQ3l9QSJ5XM3TI7I3VyunSrnKJB41L2PyuUM6yBxrdwtIHGqDsPjm/VZb882kBgkRdOW7CllXEiTfA5XIdI64dAq1Q2nuPt97+T07W9Nm6T5LIG9HATSPdOxBrhghJiPtwGef4HHH/qqNntLwuEKDhtqsTibzE+h0LVIeu6F18xGupR9RbPWmfqHt5T+79gVADbHfwe0s6ZvQyNZAmJKU2R9bG5EBQ9/ywR4GdT3StrHIgUwJSAZ8W2Ly5maVr7FVi65pViDd47LfsUHv+ujLO57mxCalAxANM034hJo9z79FQOHK2haXvz6o/rYl38Lli2zKFQq2LzxZADap+o6NjP3n3UDhA2v68DPqMyj80HrsUDTcUDiBoh7hSDxKFMzrI+T9XrKWtvMljKOCry51kU0B2yFQFSlJRKNIdSWy+0SObXD0gg/+OM/Sju33Hjf24UTO1DbxBJSVbQSUTHJXhHoB47Q50MfHC5fRrDhuryG8saCxH5j2qmOJ0FieRFHmiFpI3zzef3Zv/I/cWbfcwKHxIDXiGRHepN3zHY2A6BZefYXjtn73s73/uSfFRaGYE3HtdaNSOmBIBwBEvPuR0R6J2Vj8Bk4RvVZA5NVGy3QRF789Gf1Ux//FeoAbuWpvbKjlllMJjwlLfo+O70PQSK8MdHNRa4EEmFTAzcOIBkCgiuBxHwg/TkGSJz6uw0MvhyQOPQ1KnQo4zoUap/u3BFa0qn69OfXrx37JhZNWigaoZJurwBJawkGzHyOObHglnvv4eS73iHMcpRypoAqfuKaI3rXwGEos3cax+H8BQ4uXtYXnvgWF558Glm2nJrvpCCOFtom5SxXQGpHVCVka4BRQJN2y1qbHNpHOHTY1i6z8Mh3VCU7qsQBKf3EWFvr38GC3KcqHJU7oUnLJ9a+TgWrlN8WH8CxlDIL2DQKziaKnZg1rDGmTaLGyGw2Y+kDwQpx5tiLLe//ro8QZo5T99yZiS9tb10R6TW8kicwr5z/4lf0mw99DT1smKmmrDQI6gN26Dd7BEjsz22CxEkN2UR/HuVr+HJBYqFCmmrDVP3gyiCxAPij6tmVPwKJ5Z0v9+3av6WMNztIJESqqmJFoDXQVpbTt9+CnpxzzwPv5swH3i/szpKqsHP/V8ASNYFKyH5viacq/Sub74l3WRmtf8es73XfxJcn1yRIHA4AHdNpZH/EKIrxCi/u89N//if1lsuBE62ksPkYIUf/lhy2qsX/ybLacTw/hz/wk38e3vM2CW4EEvPNhyBxytwMdFqV4ptVdj9AR4nRldv65AsZNZnoEFhFDr/wJf3av/8NLj3+NDsrZZ53swElbNF0dF34Boz3awkkSvYWHYOnqwWJ2+6vsg72St1fDZA42c6NQBozeV0xL0dJQLHTHOY2eSu0FmY3nuHeDz8Ad90uJRAFNAVEAJ5+M2VND7iVwjOa+QI1Ei/s8eSDX9fnHnmcuQfTBgxKXVWoD9AG6qrqyKvbtk1pt7IWH5typscBJY2MQP5RMqnJLYEHo77pPg+B3MTifLWp6fr7br9+m4m6e3fXrg0pK1MeK5oJvI3CarlMpmjf4qoZKyLeCU0lyKziO378R4TdGeqS+V8UtG2RqkqaReeAbM3wCtHQPPSwfv03fhOz3zA3Dh+aI+u9qUkr0brT/TQEiePypsy1b3aQWGiSyrmXAxLfjKZmSHW3CAehQU8t2Ln9Zj74H/4euPMWAQ9zly0UCghYk+IGJG1ek5+zEEJqs7WyBgdCSJa58cuWS7tqWZ93rgPF48obHriyTTrX+IE2ZWNkCFDBqjaoFeJhk8PnBdWIsbYLYJGYBrQxlqYN1KrJ69yDdYM45m4LnNbRwBbpRvPAeb/UV9Miu8axpwqVy7skSSr3kD6/NIPzc9ibGxoiOyFnjhk2dTR3dEEswypdxbg/TuDLUWa77vzE74pPMeRJb4JT8Ir10+MHfbyWknj+e9HBZgam2z/WTsFme3ruwvW+nXJ7mBLVtDkp2raYtXQiyS+xcYZlFdGTc/YWVk7UJC5Rk/beUcGIwea6dGM1a7Gx2SysimgCGUYdi1VkfhiYBajFEtRjjOKzWTt4v0YKXcoNWTsmkoNWCojeMv4mKWkGn4vbhSFvNKBzSxAdbBp0c4Oguq6dF9ZB5nHkqOcPA63h8NmOflskBdd5VJUYlbZtmbmKuq4RH1m4mmbVMq8cbavQeG44eRZWEXZMHyDjfQKIKFqlxTVqxNYmzfQBLrnIS9JS10qjAUwy/ZvsR2lHlRv6UQ7btK0t4z4p4z7K0XPNt7uUQMgybsmfC+H9y+2bK83jG8wBo3FZeAyPKmNYVuz+CsGAN7ByNQdV4PRMiTuCWQiYGuxwo2zwTYurKyAFfJrs22yNdHOqzwjQwbqCBdbm3c3KDb/EXgFVAuoUTMmaJtLTTJWULWJeHvL8bSDXnE9iObTxvKa0eJpIdb/60z+v3/qpX+GW1rAiohIxJmUwiFljUkWTJ0LLgW8wp3c48/77efdf+JPCggQsswZQoIsM0fx7GVVso9ovc3sTo8/BLkLz+FP64Kc+w4lgePRLD1H5gG1C9iUCouKM4IylaRoq69CQiHt9G7F1RUTx3lPPZ4nyZMuk0EVtFw2E9OfLh20BAGuaMLMOVMdAqGhHxufK927hGfnyjcsb+vat33+kmRA7eZ9xncrxTls2/o3218bBtV39RDrfumG5MaGVrhwzLHeg1ez7JPsrhcw15pJbRBtD2vHm3MMxJk67qqoIITmLBwFvTOIfy9GELZH73v8ebnjfe4VwCG85m83KTErZzWd7KRjtNz1K5l5ROL/PV37xl3QWwIReI7JNczv+DJuRx+mazcCj9fpNSwF23d8jNhRT94XNYIpt95mSXisUurKuKFfwb4OJeWVszhQSqbfA0hk++B/9mHB6B6zrTHnRgJA2r2YYlFcWzqZFD/YJhyue/+a39ImHvo7uLTnlZlReCcuGaj4jhMCqXeJmdfKVDoG5dWibXGgiikfxMVC7ClVN/pXSp0cslF9F+z7Urll6jr3pvh1o3OKoX0r35CjtK2k2+/7rNYcCSJANUDv1u22BN91mZPScpszIKqSAoeGUoL2GdErTPcXO0N0ngzUAg679RiWuByVpMetmL4WY5gxvtZu/nTEdTRaRxIYQeso2N5/RirIXGsxiwZ6NvPP7PkqzcLzvgx8Qc/ONWb0oYAwl4PSKMhj0Yf1rXvcAAz5EbM65rhGMSWwOFkWGAXWhTZ898OJF2oe/qc2lfb719Ud46LNf4G33voMP/dD3s5oJsw89IBif/XxTT05ijVLP8e7otwGwfONB4iuSCL6Fx8/pL/7k3+DGy2nbYBDaZsl8PqPNk7iE9CLPqjktkcPQsrrhBD/03/55OFUJt92YNC2Smd+amMw1I/pG2D6GrlbKGpzARkZjPh988mm98OgTfOqXPo5tAgtxsGqZG0uzWjFzVXqBfGRWWVZtyNklAtWsThP8asW8nm3edzRJjYmIVehAIowAIqMFeWT/KaCqlDNU7Q9NzEnrtQkotoHE8nkDEIzS9BUfxe0+hqNjW8zl4/Z0ARGDenY0J6wDP6UHh5Om6EF55EWryvxixjjatk1aJElZPnwMSFVh64qgkVZjDwqd5eRbzlLvLLjn/nuxd92VLPAxBTioWddcdJsd6AavqibfPmNSBG2ZbNvAxYcf0xM4vvKpT3PS1LSHS5yxqS4hrLXrSp+nRM2muXnteR356yvLFe//CszK6fvxQOI20+WVQGgnEwESKwcXrfJdP/K7WGlgcdstgnNgJbnjdOCf8r9+M6ghg/3EwnDhK1/Vxx76Grs4lpf22L90md2dHVQDEhW/atipZym/dowJNBkB6X2xCwh2zkGIXeaZcbvGfoovByRerfm7+yxxLS3hqw0Sp8zmR5mTp3I6T1kWjgsSeyqtbBEwksjco3Rg1Kpgu7SbCW01TYMzBmstlXW0re+SPzRE3M6cS2HF4uxZQm352Pd/DzvvuTdlRyG7NgjJf9naAU3TFhnUM303naZwjVUkawGjKsbl+XVtQY5oCJhoYf8ADhse/dznVZdLXnriaR75jS8yb5RdcVQBzP4KnKVZGF50kVs/+kF+4E//54JpibVFqNJcWFglhuv/dZD4ZpSYthSHkV/8K/+LvuVyZPXkc5ySCvLk7YnZ/EzO/WwRazjwnssW7viuD3HhpOX7/8s/LtSSFkiATMq95ktWPrxaKBEIbU4xlBdqRJIPkZGU29Yrz37ui7rwkU/8wi+xGw3OK3PjYNWw42oO9vZZ7O7gY6ANifLHkNT1hTR5KONdatcsWf+7fn1Pq1J8bgAwI03NyB8sgaF1wNSZ+iYA2tb0c2wHietlXx1ILH58pe7DtsfRb61I5x9boor9qP3jgJK1euR7DV0pPEkrJMbgMvCSwcIrYlBraDSR0PrKIIsaWSx469vv5rZ73yacPU0fmZyyFUSNaM4hnOqTh2qH4PMBAz5H5aMpPV7iyYk8+euf1xeeeArXRoz3yW9RUl7l1WrFYj7v6jl8LtvA9pTELT6X49+93rPUGIxMSQIP25gWp3+/4YN3zN9MgSBvYGmVUBkOmpbv/IHvo773bsFG1FmiCONlulvbCnhUYNVAXaexEQJcvMy5hx/Vr33+S9RRMCvPbl2hjQef5hfnHKvVKvFmlrrl7BshRpxNGv0QwqTGdps2dauWNRZg2aHc/GfaJ3FbQIgyAvWDl3cbmDsKJG58L/UcHh9Gyk9R5gw3a4O6X0mSqTq/cwNNolESGE47+nxtn35WM8pSVTT4zjoxm82ITUjuIpWjFeXQwWUXOXnPHbz3uz/CjR/+QFIzGpP9m4W0DmvKQV5VuZmxy2FerlqvfDmxCRI3gjKTY3Qq1+f5MVv9rCZfaFoDTzyvP/1X/1dOqqGKEQ2RmTVo6zHeMxNLpYkBYmmVS3PLubnlj/2N/1G4+UR2vsseeINqTdb/t5F8G4BEYNny5Cd/XW/cD3zm7/9T5pcb5i6ZZE1lk89WZ7bLT984fG3ZrxznZoE/8tf/O+Hm0yBC0IBxNV4jbsBTt6aFGcrLHUE6+NttbQdta3KgS5PM6gTgq4/or33ik5iV59Iz53BLz8l6xmq1oqoqDIJvGkJIJqDJwJEtIHF83VoT84miPevL1Y1AgTUwONYiMgIEI6B4HJC4dnwil/NRps/jgsQxhY+hB8ddIJXJIM+u32doQi99bCfqFAEqm/z1uknW0Qafgj+sRY1hz68w85qb77mTez74fuGWt6QI1nmVGlB22CSTjEryvfXR4zIB8zaQqNJvhCTk9+niAd/4d5/Ry8+cwy49VmGxmLFqG0SEeT2jPVyuaQo2nsuW7xt9MOKB3PYbvcL5V0PWNFdXOF9kCiROmpG3gIopLdbkdRMgRYWUu7qyaEwBLXe9753c+JEPCJVBLSkAadQaU0Cnalrsy5iPEYNmsCjglcOvP6Jz4/jGVx7kxaefZSYWF0GbNC5mYpGs7SanqGx9Hr8Mwdh62yJJwza2VhRAvAHSui8l/WM5F9euK7KVmmeQqzndZxMkTj2r4/A8DttpBt+HIH9MwVR4OtfKy5vo4/mYr6u2hppEkxUjyWc3+eqXQMigqY4zMZ0JvA0ejO02o29/4D0sF5b7vuejOfANIIAbbD2ibPqcS7qviPRzy7jaY5A4MEkNl8Pi4N60nrp23e+iKLFtEkl/C3uf+rz+3N/+u9zma3baiFWIkmmkvGfmLFbBH64wzqLzihe05eLujB/843+Im37nx0RnNoUJGIuuAqa2nTvSOIp6K/j9NpRrNnDl2CKAg1s++E6pDlSX/yAyM0KryUfPqCbfRMBa19FMGAXTeHaDYb9pefBXPqHv/cM/IVSS0llp7FTO5YV+Tequ0L0WAm3JeRlAqjppiGYmAcWZg/e8Tb7rffelyf2pZ/XFX/8SX/o3n8Zag/jkv1ibCuOyT2K+lY0DJ3Tp/8HYJ2gAiteOpy9pd5re/d6/UUeAbL2Jw7/Ami/b1chYszs+Ny7zOPeJZRIz60V3moDuujLp9b9JPIUGiaz5/3Q+nFF7s7OWyTmBSiUFmPi2YbFYEFYNla2IUaldTSvKzo1nuevd72JVC7tvu0vYndOl9KiLD5pFQ0y7/7rC2V5jUI1SWmlu2NqziIqQ2nb+2WeplkG//MlPs7tUTgbBSYWxhv39g+STFHzyh3UuBYjQL3q509fA/Mt5LmuL/dQiM1yUXylgfBnAcHtRVwaHxy4zrvfrFCgRhQWWZj9n02kDj33pt9jZ2dHzJnL7A+8WzMD9A02bBc0lGyFqJPnXJv9XVUWsJFoIE1k8cK8A3HfvbdzXKhe+8nU9EQy/9enPUfmIb1oqY7HG4qOmQBlr0zxb3BFi73PXu6NEopQ8PgMXFB2wpFCA3KBbsoG1952e7j4VOjA5BHnD/tyE6S9Dok4+owF0XTs/OcZkvRlTFp3I5rHUVzrpUy6a5gFnbGfy1hwoGUVSli8F9RHJINHUNavacO9HP8CtP/A9cMsZweVNqCrRt5h6Pqio0u828/qQuU+HWsRjyfiV7opN/tF1lQBis1pRz6s0PKn47M/8nJ5dKV/5uU/wVmoWCBo9QQQj0IYG62y2skVslTbe3ntqI8yjIAcN4EiYNvWbrbeYynO9Xg4Fz5tV3uQgMceKVZZ9C2cWtexr1BNWqJwl5LzIzqVmjv1hTBsw0nB6p+ImZrBMOykyEXDlzCTI6eTVGB0CSNkdC1VW14s1eTeWXkRZZOqcE3U2S8PhTOTFueiFHWHmhR21rA5W7DqLrlJgi8RNTcdQCugbR2MepWVMf4cT/tVJiRCGl9+FRwGN44DDbeBlyPifdvR9OwtILb8NkvNqkx5JGo19ucNdaMwanbIglL60LqWeUms4JGAqx6F62srQuMDZhbCyUFtP5dLOuDya4D0Oi1iLy5ufFHCSM/Uw8rdiPcgouSSQBsHKJ57OeiZ1q8phg7U1+BQItjNf0ATPzKS8z2pSBpZhW4b9ONQsdqBmaPYvC+oVBsAUYHy95JUCxKspYwwCp4DH+LvE5B6wsBWIYJzDtJHq0LM7S8wNkk5l+q+sBR/seoeLuaJlr5p8yiqbLdKK2a2J+0vOu8TbeMlFFsawmC+4uLdPZZIJOlHtKKEEQGSJANnHNn9c45rsNlXQA+QjAOBR/Tf1fXxumOL0Stcf5/w2iSRFWwpaGYCL0vZRsWOwN55fh30y5shde7e7dmWLRt64FfDsMxWNGGBmaYJnVUf2q8ipeeTWk1ZwHq1tAvBIcuURiDlwjqIpLJt3AZN9jBW65BJXLUPAOEhWEGNLPa/QVYtEA61y5iBw5iBww0qZLdvU9mwJT65XaQOjGomGLsd0Gt81MUZ2dnYgpplaSRRdJmeJ0oGiaFiv10RpdI3Km9rcnCawgKB431K1ji/+b39bz332K+xYizQeq8LMVfjs7B80ac7mkgZIUDio4OIJy+/96/8D3H5atFY6r7JhrubhvQcA4JUsXRp6+pzik9FkIu7hk4kxUBk7iFjMW+w2wt6Sp7/0oJr9hs//6r9ltgwsmkDltfON63arsu7g7G0klF08A3Oyms2JVPp2F5OIHZhUN0zDMjAvD0wSQ5PyMK3a0Ew85ec3/NyBkC28ieOI6g1A2NWhnE/HzdTTNIKOfXxy+UbNJrAcgiEzYSKnD2yxruLCwQHzG05zoT3krve+Ez+zvOP97xFuOA0kYEhXVdMv3GXAtAPHcUaTc3fz9G+N+1OBtk0PaeX5N//4Z3TRKFUbOe3m0HiEFFG9XK16Dj+T/BJDCKkfjqA42nheG+bo9UVkWyTyVJmvphx3HhwHqKiGyeuO63tYQPSxAy8Gn40CPvmUHTQN0aZN5koDzazi3d/9kRRc4Abvw8DdQCHHKGkXLFXOEbLfWa58DAFjbQoUzHBz79En9JEvfokLTz9HrYZZAJYtCyz4gBVDyP2zFuARFZO116G4sZT+0PW+66KiB2CucwVRHfDg5t+MQN9Q6ypZa77ex2by+o3nNOFLmObJ/niYqMMG+C/gHLq2DjXy29191j8Px2FJDwmbm8CYnwOZASEoRCOINbS1Yc+APbPLd/7wD3LmXfcKFXD6BGhIdHKsawSjxlRe8Z/Px4YBisNNR9mmbjRrOC8Nv48u9lnXbLLGO2jE+ghLw+f+z/9Hn/7cFzmzjJz0jkpT5LWrK6RNcdIhg2InFocQfeYlnTkOZ44XZo7f/5f+DLzrTmE3mZvJ1GCQU6iP+n/IDnJdk/gmEE0uu1RuBm3gg9/3vfyLh75Bu/TMc5RdQDvakEQbE9MEA4ix1Ci7Xjj4jS+yc+v3I0ETbcgW8wAcwZ94ldIBxKKlQrsUf8bY7kUzxtIWoEheKFWSKdotuO37Piq08GO/4zt56dc+p7/2y/+SKoL4BIptnlhcTJ+r0gA1acKeqNtUejRY15xdycnakBcj3T4BjuUos/LVynG0ikPet3FHjLkgy+a/y7U9uodoX8YQXKdsKENfx2S2DhI5cfdbufs99/PWn32dAAAgAElEQVT/s/emwbYd133fb3X33vuce++b8fAwETNAgIAIgIRAghRpkdRombZsOioriWOnVEmqUsnHVCqfU/mQKlfKciVS4sSyrJStmJasUCIpkWIsioNEmYBJiiApEpxAECMxveHee87e3b3yobv32efcc4c3Ae8Cd70679yzx969u1ev8b9W7rhVODRKK3llcsOyUDjQbofCLTATIlXBpEomZMvMzGQ5/1y9S0/SsU8/+h90NI2sktzIoe2QqBgDk+kU5xwxRiqbLIltCCngHd1TH19KGt5vN+HuYrObL4T2cs2dhJLzub5zqYSfHdXpnXaB2giug8cf+Qr3vfmOFGJgijWx14ayEpz6sq5q0GSdts4lAbHcJgSMy1akyiAkN/XanTfKfbe8Cc5tQITnv/p1ffzLjzGdeiohQZPoTKgpPKBP3NIFXEadKbMFV1CELfLD0No4dCMXLMbt+EyUeX50OanEW861fdC2hFOaGpP43eyYZTRnQRwKjOV3DnkxAyW+E5DG0Ul69y0pNt+OG0aHVqlWR/z0T7+P6u7bZ/GGtc1rm80ra1I6RQQjJvEg5vmlGcQV90NmL1bEXVjGTI6UXJhCsKoQDd/7vY/rDx/9GsemQjONmNgRAJvjJUPwjKqaoDnLOiZXuLUWsUnYnISOTWPg9puTcJwLCvgYUDMD+369C4G70b63JCZrXo6rayOcbvnwf/vf6TWtod70ydKUS3dZcmJEVBwpbsajtER85XiuCXzo1/9n4VAFozppLeLmZzqAJKSaCFRcAk1iGy1q2e5l1GeARUAHPg2Fc9/+rrLZ8vEP/w6rXqg2OkZRGEeTwEWhj9OEmTuiuOkLTlaZYL7taJoGQszlDw0qabFqSzk21T6pIYSAyQKGzW58twCfsgiTIbK1Ysm2mbN7qMCyGyTL8NpDcNlSbcU512eJN5XtS6clq5ojFPezpijr8uxpmyatXbTPRqWpuOnNt3PD2+4TqgpGoxQMLvn9GUlYYwNZcI4xD7TwxbFRXn1fIWhmdkmQF/kCTiVZoSN0j39fv/K5v2AUBTcNVGJoQ0tdpzlQEnCGVt9iDd6SCb+DULZ03y6JLYu01dK43NK/d5p36O0EZbObC3P49048YacEi53+XtyWrP/JyjbNF6qT6QQFNiu48aH7Ofb2e4XKEDKf7Bf6ovGcD8lw+EWy6SVt8AHE0j7zHEym+uKTT/P4o39J1UXqKIyNo5tMehxFUfqyqSWWTbPXwWvsAdidc5iQM+9jGrsCPWh7SZQp5RCL9bFAgpU+SwpcnHM3i85bEod/L7r2F/cnvjsbP7H/HlgXi2sWel5aEA5iriY2l9giSSAqfdQG3/NU1ZQgVEKSVBWXbxpVUyiKQBsD0Ri8E6ZGYVxz2k+45tabuOXeu7j+LW8Wjh1NsYbGzJKXBt6GZYL5jmN6h33lGltOkIXf+cDFaykJ0F9QNn74HCvnOv23/+Ovcuhcy2oIVEF7ZSPDG1PlGHxtHJvtlCYF5iSUOe+p6hEvVcp9//BDXP3+dwkrFbPKApL7QHO1qYX2Ddr9RhAg970lESBkoQ8BGsv9P/kevvexP2WUtYD03stCl36HqP0C6NuOpm5YUXjhzx/Vl4447njXg0JVL71fGd8Xuzz1F9vpN7N1dM7sn78Ng8YMG5TPWbvnVqGL/NJb/nt4+keK1Pi/+g5/9ehXeOmpZ5NF0XeIJFeVxhLInpiYkmBPCkxCXddMp9MkMFiT+jDHeTjn+u/oAxhJltu+0REryeRfLBg6YOxz3fAqKi86aAtGMFm4i6TEk67rsDnuL3QJ+61xFT4kuIjilkvC4ew5uhAyw4kcOnmCMHLcf9892OtPpSQUlzIO57IFl4wqHXzPWRryQJzFPS4cC6n8o3M942vVYyM4cYRXzmAnnsce+RIy7aikwomhwiDW4XP7o2rfJ4XRJovPwDJ1Hv28l21wedzKF0IXG294Ke616zmDrioCUB3gW1/6Ku+4+U1w7BB25PAaemteb82CpYrwohWsHFese4pBMpq8xohWgjFQXXcCESPX3nAN177zIc4++hX92hcexU9aKmtSqcHpNHk2EMQIzlV0ISQc0C4Jf5XLiYadT0qmyEyoyvyq67p+nDgxCfKMJJyFEOZi2oZegblYyCVC4V7eedxGNBrOUWdsFgZnXqMiFCY8wsRPxAjdtJ0p1yRg8qqqZu52Eeo6JSQWITjmohGurvAo0+gJjcE7y3V33MJdDz4A152EygjHDycPhc0mVxG0VArL7xZNu4f65atGy3RIQLKrmS7yrc9/Ua+aKKsTz7gLpbroFk9X2TaZTKhGDRqUSKqTbitH8B7vhKM3XQcr1cDtnTjnFt62INReGZzp1aF9LSSmAUTGeAPUQwWbDqR2mKiE2M3iWjIvFRFCLhmk0bO2OubcuXPUh2qORcP6eocYR4KPWBgQ+ccFhONupZ3Wg4XxOaQtOFJzx89jU6kEpBJwFetrldRt4MnK67OHLS+/FDmmFVWbYFu6EIje01hHLRbvu1RZQSOekDTclRVA8apUVYX4LiV3kCo8lHgVZxL0kIYUC5ogMSQngWgv+cYtHZyfolhYChMYxm/tQXgYnr/bOcNa2yEkC6G1FhGDhpDjLjUJtxhinAmAmp+r6zyVmVV7MZXDG8VXjk2JqFFaCZyphGMjl+Br0srRt2OWzJGuEQaMaieFpGj5loUwCAEqh8ZUnDKgWHHUFtjsYNJBp9qePsNa1SA+4rsOI5aqriCGOUORSooRGmYvs9C3uwl45yMULjt2T5VNLiHtZMmDrUP3fKyIqrotD1i87xxEzODepdyoIyu/2bJYKik1BdbIZjXCJHdizBaqkK9mi1VxICCUFvS8btCk/rV4BWMRTcIeMSAue1+aGrxyuhJeqaDG0XSKkRS/tzZaYTqdJjDkbE1sqhrjfeIn00CIHUaEqngbshe8ixH1Aclxsl3XYYWBhd/0nou5RKA5S+jF0zJoseFcNUiaQ1Fnyn5MgN6R1H5IqBb1yigJMdbivceMaroYiRmKTVXxbZvCprLS7g10RtjE01mYVAKrNWeDx9bKybFwfGxFxk2GzCoPPtQQmHVKfvEieUwMBtuyoSoL33umhRMWPRJF8U0hM0VyFY5sKld7x+MbHY3YHBM8i4s3+Y+iBDTNiM4nrth2LSv1Soqn1hRW4Q4fIkhxK5ewnaQAbxfy1AvUF/Tg+4/2tZAIzJKrINVGtnDb/ffw/T/6PJuEZBVhBoGg5AEpBjWR2tQ9MDBR+bOPfpIX1ww3feA9OJM1mFKBYjhRLsUgKYx5+Hubw4a3NUu2D5u05XfGh1w9dQIw3Hrd1XLrT74b1ifwyqbyymmee/y7fPHP/4yRaVjf2GTFOoy1OEkJLBqS630afe8S8d5TWYuVhNhfVVWCxBiUjlNVbI5vkqz1SsZTM8b0WvxcuxdiznaDS1kUUs7neB9DriKVFhVLqf2tEGOCkcjuniBJMI4x0nYdtm5oNVCNG7pO8LlfTOU4evIEzbjh0MmTHL/rduHksQxWGCk1Q6ddS12N0jDIlss5AWwZbaNt0zPJJfsH1xRicrFJxROPfV0PY1lxNSYki3HM7yr6lBBmMoTKkGnOJWPILtnLgzYs3bfN9r3HHO64+4JppwSSvRx/vtt3czcvu8zQbWoVvA4WSFUkQpx0PPEXj+hNP/M+QRWtBU9kbBPrn4PNHPC3Ra9Fj5u8aKouWJnFAhMCUrkEyi0CTrjhx++XG+5/K2Dgqef1+W99h+9945s8c+4so7URBINTYbK5SYcnSqAxCeKpquokDGYBsMwTUExlMz8RCCkZQ/r+0l5AhGE28flbhZcJ/tsdX+5fklJijP08SV+ScPryvkjij2KToFs8ExEIvsNUjmgMU1XcqKb1XebnnlgLUxNpjbJ29Aj3PPgAp+64DY6sCZWDY4ezFUVAA9gcP5zn+JwFevhuF63Ll1MQGiihvWA4JJFUVKJy8OSz+ujH/pgnNiOrJkF/YYohYHYNoId66tqOLoZZBnOIrHctjCrOuIg5dZW01oLGNMZzlRkrC8gd20nD2xg5Xk+074VEmDG4FKgSWXvz7TI5VOvp0HHEG8YqffZcH1AsCTtx2rUEn9yJlVo2nn+FcE44+6Wv66G3vSWFuNlISSDYMpAvYpAozAU3l8tsif8YrqnD7cLW+IhFoVMNGhVjXM/I0zMBh8ewNhauP8apu27mb/zN9xOefkatCj98/Dt87xvf4rknnsJMPUeaEboxpY5C5ZVDTQLwVqCTFE/UBY/LYNDF9QwkQSOmmJI2Bpoqt8UYNIZeSNqrsHe+guFOx9R1KmEYuiT8lvifqKnubDMeMe061AoeWA8txlmiscRaaUVoDjlGa8e45sYbuPpN1+OOHRHWVkmZySnrWNspUjWUQvIBcNWofyeC9IDbQzvVooIQBn+XVzy0KG95xDzmLZLjayJ0gek3vqs/+tb3Oa3KyFbEzhOMTZUyFGLIsZGatOySJTjELZxL+Mn3Sm3YGqu4rYVxByvi0uO3PN6lSiHb7voDgXjJ9u1Ei/OJX1y2b2Y53P4awzZJTBa6EpdXaGQcz3/vSW760ctsNsr42qsoAEXLwgWKrGCZdzcuJnD1FkfJCoTN0cAuR2nbheXemhS/ePM1cvWNp7j6/Q+nC55dZ/NHL+jTTzzJ9x7/NhvrE0yrrLmEDdsGj9GY0ihyXHNVVYS2Y9p1iBVUU3IiMUWSlapQlgHkTu7T0ubt+nGxj5f9XvZutystWBTOIbRNJCmcGMFimfpUC5sQaepRLgLhMMYx0YBras75KV2cEkaW6AzX3XQT9z38EM21J1Loyso4xzWX+EKTcA2dS4pcBBEzUwQWp2HZZge7BkymrH1DuiThVgvXG66BQBbaKpgEvvPZL3KCmlE3wVWGICEJuwK+N3dLwlDMbXfWYJzFTz1GLTEE3NqYW37ynYzGCqMsAhWPTm7AoMbAXF+9gQyIPe17IdHnkCsFYlCsNeCEd/6tn+XYOc+//zd/QN1GRFOs2WK0tq0szlnEKxI7RrZiZRJ55KOf4n1vuQvGph+9xQXTa9aXgHa2T+zlgG00sHJudpX2knRhVmTQXABNDBgfsLdcKyjccOMpbnjvO2FjmhIcfvisvvzUszzymc9jpp7T7SbVikvgts6iPrmKWlWiF2zmmkaEaOldQCKOYEB9cu3uxYW8zNW8V0FxR2FTUuF6FcHWDimJTMEjlcPUI16YbNAcWmWzawlWuPG2Wzl89AhqHdfcdCNcfUxoqsRknIAoqgGp0poYNGWkk8HNIWEjGlIG4NDdXGgLky7bZRZ7uH1nMc/Bcnyp5vu5XG7hS5/7AoelwuQavjbHlZo+e77A9BQYjyxOFCGmQI8U3rrwLnRh23Dfdtu3PMbASrkdXcq4xUsRe7hX4bD8ll3273adSEnI2LpPABsUZ+CZb3xLT9fKXVdfJZVNQNfb1dYtbbLMD8EiSAzbHDUm9zYmYwLKzIIm0ifFiQjRxFncrkoSAI6MGB+9UW674yZu+8l3AQbOrBOee05feOoZqgDPPvkUp198iTBpESIaJ0QCq2tjQvCELlCN6mT91rS6SyQDxc91XK9Uzfp/97G4E203RoeVnUJUfJY6oiie5KEQa9AQMSsVG11HNa54ZTKhWmtoowdnCcbgVhz33Pc2rr/nbjhxVKgsjMfJK1EJxC7B1RhQ4gx2q656djCHN6gzGKBhBvIifM7wXQ/77bLSFouJSXHVT7+oX/rEv+PEueRmnrYTnHNI1D6be7gOziUuqlJq05vG8Vy7zkMPvoWuDhA9Ggzk2PkebQTQGKDww2Gb3mC074VEN3gCa2xiPLXl+gfvFUdD+P0/VG19isExdlZCLiaBsg2e2rp0XozUVByKlhcffxK++5Ry102CZQsqfr/yXsTAEXZ4AduoLIvsKCt/2QEzf3xvWTSG2HWYqprBW2UXpGYXJza5PJJCHjCVSZrpqIK2g5O3y7F7b+anf/7dqZC6gn/pRfUbm8Rpx9e/9jXoAt/9q28xXq3x6xPWxiNqFfy0xSHYAE1V03Uem7H2tov52svivxiwv1eas4hVCUKks0o0kU4DUxMZHW5YPXGUB9/6XibRc/zaa4Ujh5J1bZQTmmJ2qRUG3AtMDs24I5XMsMWcHdQFFXAZnHbLqx5isg37QWfvu5yw3RCc255dxtbYVKf3zCZjsUgXsCiqEZNjMPv4XZOytYfzRWRWctBGkyFHdM56NQcFlC0oF2IlLALpsuV6J7f2paBFQWsvQ+y8BcxtKnWU3ztZEPu/IY8zUIl9iTejBRYlowwYC5sTajXQZUgsZ5Iy05tO5nlLyfrtn32wf3hcJYPsYBEKIHcvgCqYPDFK7LhGEEvOrGXxxnBiFXv8Vjl1160QlOMITCdMnnpaz718mm7a8uQPfsBzTzzFYWsJG1OONDVd22JiUn777OHBmFUdwFKd57jZ3r1c/ABbLcsRmIYOU1dEk7KOPYo6k6oeEvFGue7mG1gbNxw6cQwzqrntgfuEUZ2NEwKrq+k7kqwixiWefG6Dze//QJumwRw/DNefEqkt4gM4g5qSgDiI5ZUMgr9E4luS9zj3QFKeam5CmC3HLe+onbf3a9UCQ2u7ljoIT33mLzi8EVmrR7STCU3TpHVFDFYNLYk1l+f0WYuNPoIqtXVMg2diBa46Cm++Ua47tgLO0YghkOJ5beZzIpKAt7Po3LPBfjS/cWjfC4mZNw0GdXKTuetOwobHHTmEdOvIRh40Upiwgmof3NxYS20dPkZcEJqNlsc+8Sfce+c/3KUBXJyGsUdetch8FgdqkVnnsp7zDg0RU1VojjHTuetIX6O6FzKdIeFPZsBmV6Wj6zxcxqsAuOMr4rwHsTz4wN2gwoM+wumzqQUbU+Xl07z81DNsvHSal5/7ERIiLzz7fMpqzDh8xbRfhB8lTUqRrd2ziN0oYlDJcXY9gPbs+Yad0W/P144CrVWaQ2tce/11vOn2W+HUVTCqhVGCQcIYVkJHshaSxpfNC1C11RKT5b/+tiU7vOs6KpegK8jlsVRZbpLeq3VMtz+0B3oXciWWXH/FOeLmJr7tWMURYoI0mkwmuKpKVtTe7T5/zR42ZG+tmzXzAqy8Ox2zLHbxUtCFCJwXes52AuKFUMHhVJi5PTPsl2871AmT02c5u55HhSzcdy88TLfOx35hhx7VYHihOUtdvkcMiskejOTuNjOz9NCKU967SoLWqUeM7rxJRppm1bXdg+mCz7yo7fd/QJh0fP/b3+b0i6/QdV3iLzYl9QiKaHH75iS13FGShek5S+ygyf2zLuwfCs8AmkEaoyZrYUQIokRTc/zakxw+fhw7rjl04jhH3nQdHDoEoYOjh5MnonJZus/9YeysXyAlCJ1e5wsf/YRWU8/Z517gh9/8TqqyNZ0SxxW/8J/9PT36Ez8uBWO1B7Je8n6LwFzYz3nNom0MGBdEuwz/unbw9Es88sef5lirqKb68dOuTV7DBbfKTEjMPNBBJTV0AVs5NirDfe99GI4eIjjFoklxyf0lVuaU9EXPzdaYyQt77P1E+15I7AXE8l5702JM7r9RxYTAyAmh7ajrMbGLeK+MGkcbupQRpUqMCsbipy1H19awG9NsLZpfyQ2gbUTcLBtq94ae5/YdDtsOs0pYntRSXA2L6PflOk7m64zMjlu86wIZoBZKSSNIqpyu1ui0wxxuZMPX+kINmw280gi1qTm9YrEqWB+pVKlJVUu6GJP1Q2Su5jEmYx3KDH4FUoakccnNJRnfqli+lCwMWkPIWGbi8gKRX1h0hg0rNCuW0VrFobHh6MgIIwONhTpZA6mbwTOb/LVcn9wqvKf2FGwzII1LWOZpLifNqDD44TcL+xduXBTyQJrgRnI8WAhJOM1VUpIXWWi7DmNt7xYUEQixfwez6xYpUQhFUx+2Yemj9KtrPnWba5bjF/YvtmHr9S+OdspU3st5S8tXLrnm4rbF2MPhuUvbUc5ftDoOwy/SXzOGKMn96MThvFJVxeoTEc3wSwMBYu6+i2Nt2TGDDaX06Rwfka0nGSdb9y/hLXOv3Q0s9ZqXbWsgRkKNvNCourrhpQbONGlBqLG4KNhowHdoUMSQQ15SjLT6kJLqBkKzlDJs2ZprxaTkGCG7zzNm4QBjtu06rKvwEvCkuTGNitaGYC1mbPCrFjs2tKuWetUwHhuZeENTK7JW5/eV+Y3mv3MShXY+tauLrK53HGth9UzAn4uYGMBa/Ebk6CTFG1NnF7+tEDVbFUmhh9WdB+1aQv0A7t/gzsedDxVGpZr4EYoYYVZTJ6TKPyqYtmXVVEjwhJiMHpGMvkAkFENANpGaog+J4GOHE4hiaS34Kikz6V2ntd3ODdLBGM2Ww+3G/RuB9rWQmCxnGYOqD44qfC9pZO/8qQ/wmX/x21gDTQZ8NsawtrrK+uY5pBTyDgmWwFaOkamYbG7w5OPf5e7nX1JuOCZS54FFqjZgnbl4K+IF0k63PJ/myML3hTXEzJfAswasxawmRrdy7IjccevN5ADAlMgx7WAyhRdfVtqWbv0cL77wAi889zxnXzmNb7sevLnENUnlcHWFrasU0+cS3Ixzrgd2ttYixiAFfxBYWVulGY8Zr64gLkFLKDBaWYFDh4XxKGX/NdXM/VXQU+3WIvV9X12MNn0xL2nxXJ372rI7FmswkqwTwdNuTjTGSMh7htY6ozMhXFWXCIr07ugLofPNPt/L9S43XaqYxN2O2Wn7tjqmZqVneMDAElLXNV0MTNY36MJM8ChZ6Tv29ZWwEM61wfQ8J/002Ouu4rprTwpRufqhB5Lk88JLYCxnvv8DHbua0Hm89wTvM8xVoOs6vPdIDAnj0SdcxbI/+vR3zHiLISSBRVXx3vfl3dqppx4f5cTVV3PVNVdz7ORVcOxI8kasjBIMUI4X7MscZUvqaPB3jDKro61FZEk1h8U4aCNf/8Sf6Lc++xecjJbwyjqHXQp78d4j1vDx3/yX/PVTR5W7bhRTCYyrWR8uGVrn5Ta9XGNBY694p+IVpT86LAY8fO8zn9fD1Yj2zAZVLj3ZiRJCJA6ySwz05WIHNyAET+MauuDpbM1Nd96RNfShMLg9XQnT4LWkfS0kwizb0wq9ejTMSlq9907Wa2Gli1jrEm4gQrs5obIuAQVjMaJEVboYkiATYHrmDI99/I+59z//e2DAOU2AwgUSh9l93si0GLwt1gx8x7lzIunFGIW6grUKjo0FC5UI1wDX5LquyZ8js8Wuf5+StWwguwm2mON2EqpkiXc3msFx+X5G2FcvddDFi5bknmEa+lIK5156JeGCDQP7S9wG89at7YTPPvA97xnGeu2WUDQnDO2SxPJaA2rvVUDcq+C3zIJ4oZnQQC4Pmd1isnXhn0ynVE1yt0WTgd+zIr3nsIYrlYTeKh/bVHWFysKpI2Ash4/fLVhLpYN5PRfju1O8xmDk68IsGL6HUltvGPOkmsNSTOZjOY7PJsFEcxEHRNCQlbEcw6n5X4nXllJn75tP6Nf+8E85NlF0c50joxXabpKaYAxGI3Xr+bf/6//Be//jD+mZxnDru94pNGbQLnqB0V0Jr14giOkxOvsYQI3JoBAsTOHR3/9jjq63rI1qBGh9ynivJcHbDUs9xsy6+2XHBxrrCJOW5sgq66ZD3nJHVjWuhE648mlfC4kl2zhgttQA7X2v156Qq998m7Zf/jZ+6vsKIVVV4X2Kb0iug2SJ6mLE0+Kc43DT8I3PfYF73/denY6F5qbrhRjRKpvw95kscVkol20oi3nvyiUxvxTrqKhRbJMZoSaQbakSE405HFgJEE1SLItbicG3RFCbvgtIR5T5/eT9mn6rZkwxUjUIIy63UnJd2UgPGwFz3yGEnnnvN5olt6QH0hCze8XwwrPP4UzKSB8+XY/TOHAN7yaw9fu3SSbZ7Rp7vv429FrFJPbH7uJu3m37+QqXw79TWT5JGIlCLh03w05U6DH5umlLp0DXQZNgmbzGPvFkv1IkQzzVuVRdjEhjk4t2XOBNBmOTsm7oYAuZ/2h2LiY+IsRcdU8RsYhoz1d6/sIyh5Iwp44uwgGV+G9VxMwcnTHGXB0su7xtfp4u8tgnPs3xDg5FQdQSQ9eHhnjvERUOjxsmL2/y3KNf56VVx60PPwwaUVkiDl0BRg4FMNnQE3P5QgGjETENZ770mB7egEMTZYTtsXmNSILqijpL1kpe6dlbzRZFIyYn6ihnNs7xrl/8YAojIq0Nr7USuh9oXwuJiXaIkbAGrOfdH/xZPvmVxzHGYW0O5paM4YcQQ8LFs5VNtZ41YoiYacsRMXztD/6IV1Yr3v1f/ANs7WYVQ/Q1m19XDA3x0xKzLI4SeliCYhns6wCLJJcwOZO6xPlpdgEUztsHQpkZR9F0Yc1u0XS9wpDL98wtJQLWVjPDphYDgqS1o6jUOS6mfy6YExCv2Pe8pGFDo2iPV1hw6hRefOY5jm4zb3q4ocHf5XJD4XExlq7Utu2VhR0si/05IvOuoQuwIC7Pf947De+/2x13i1/cSQg0C7xiN+Fwu2MXzxPtZ1gas0qPXxlVUWvwmuqPOweEQFS7742IhUw0PS9GlQJFIWIHg1/mOnTZyE/9l+Gi8nnJE2pmZxQppHxLOl6WxCeX5Mg+239Jh0eNM0dInj9GUoy1jy0VBoPh3Dcf1+888hVOrHc4FayxrLdTbJVqyo/qhjjtoA0cx/HMl/+KZxvlPest1KN5yLZeIs3/vYZKgjCbf2ItqKZony7CVPmdX/8NrtlQVqeRSlNVmvFKQ/SBMO0wCMZKb0oI2YJYLOqiYFTwPmBdhV1ruOltPwaurFSvk0lwmWlfC4klqsDAFrdinsPQGKa1w6yu0E02cDLD7gKQKDhX91qJloGqIL6jloqViWcaYipl5irEGKbTjqapeHtt6wEAACAASURBVKPTdhVCkpFVis0OmGVTlzi5EMIcTtdc+qSk/zSbi4chBCA5kNzM5MlMRQgsZAbeouzhSRa1oTeJRWG3LNT7g4kMPcpb96UcUsgLRUwxVSFY6uL6UsDMBL9h2cRlcDU7Yk/uwbJ4XtfZjS6ztHM+uIe77dvtmXY7Z/Fv1TSfguocatLwOO9T+bFOIpvBQ53qIXfBzyCZ9jOVcRpnLlxgVglm8XBmAlw6fRZu0rOY/hoGTZU455WZ3rVcNCH6RJahUCgDF/ScwjVwf/tsQTPMLIgaAxZJOuukY80Lqyo0MbmhxSjj8Zhpl6tceZ9Ko/rAqmvwmy2rxqYklu2GnOyItvqqUeHPAoQYs0feQVDGGy12E9bqEdp6NCccdV2HK/W+o0+6QFaOenzK3qSYuPmEwFmgq6CqhF702WoGPqAF2t++BmbFQ3oyMwtWIFUDWbnpejkz3YSmYtrlihnZ0lhqfaokdP6Y8RId0IhhBHz3S4/x/GPfhMceV9pUZqlpqjlhRHf5bKFtd+wvmjltcqXrIqXFZLUymmPfomLzizE5dtQaSwyKDvlVduErGZnCkjHV5vdlw8FSodCa2UfyNlP4dSQx/ix8+hjoYuiz/VITpLfGlM8W2nbHq0uLQ0gWxtVc7ee8aJ04ebJXknqKulQgiVkzD+gc/NCWjl9sR37nPWbfwnWHv4eL9vmS6MV9FmnYnu2Etp3aW/aV5y79sKvbnK3jbScBccvfojNYF2Z9XlUVHZE4aghN3U+mVyPh59WgVK86xf8FUQLJZRs1Zl6R+VLmR8n7IxgxmOyGXaZnFBYmJf3XDD5zfEh7JiPWpCzdvD9mxrbF+puFWUGyu3km3IbOY1UShmobobP8y1/9dcLmNGHZNhXr0wlt2wJK6Dy1qwjEVH0mdFQqmM0OvvuEomSY81mbA2wdbK8VKT0vTgYDAR/hW9/XE27MyFa0bZvX6eQFdM7hRg0boSMUmKRCkryAUuafcWhVsTFybI4M1c03SME+ZJEHHtBS2tdCorCQwi+zMQdke5WDlTEf+Dt/i3NGMSsrnJtsQgELFouq0GogSoJysGIgJouiTjvWjMW8dJbPf/j30sU7n+MdXv1nvhJpaHmTzPx6zhsLcuCAI5UElBgxVmYejwHzRei9zIvC9swiOItFL1cfCo5zvHlwARk0xRqLLYx6+Cz70B+3pcW9tBFTdRcAK1xzw3WpKsMW12X6Lq7RneLiLoZ2EsAu9FoX+tmtTefblkt17l5jFYfVJpbt9yHQiXLTW+/mzQ89ALnece2q11w+uFRkctxzEfySG9fMhbdsYdaZQRRlcwtlvlIstCWnbjvlf9n7KmE0pQ1aJE/y/Iox13ieNcKaHD849bz8lW8oX/yqHpnmWsXA1E9pVsZoBui3YhJih7METagDEjyHqxFf/syfQYhL33NYaP9rST3/Dx40waC9/LWvw7kJ2iU0ki62qSRjhiCa+BapbO9ehkGoRXlnkgDMz4lyroa//vd/OcEpiUuW321gzA5onvZ/L82bgFg0SgFg4Pg73s7Z2rAuAWmanmkkHKXEVKKht7CoBkQjTVURzm1yol7hzJPP8qN/93nFS8JvylyjL+I+dPMEf3mf+wqhpGRLypaFrZwnByP3n2zpxcp8QPcSThaXfJYx6P6+C9a/uUsu7hjsNHMfmbve4s22WySuOJJZ+wzZMgEgysk7bxOfLS6qmmqbxpBczjkMoK+8siBQbSdc7WZZKxa1oXWxWNiGnxjjBQl5F0rLnm3ZJ8a4tG3LnmsxXnO7e+pAm9mtDwotKi9RA8YMEhjyezPZKtPFAOOaQ3feLCduv1EwuRrKFT+A90ZFKDA6s/4Xt60ZhKMUxXPgU84Jd8x7J1jgLfl4lZkw3vOibIZUQMwMLmvuHQ0u2ivP+bcRM0PGEUG9Tz98hNbw+//kn/Gx/+2fc3hTqbymQB1JCXWVGEyc1alWIdeXV8QabBv49pcfg3ObKb8vhP7ZLzaO91KSyqA9YlLHPvsj/ewffYqRGmpjidHjnEtGnTJXJT1rsdymMW+S0B0ilalQDG1lmY4sL9fCoXf9eHoFnB/M1hud9reQuGSsm8F3j4TUeTh1Qm5/+O3c9rb7mBqlzbEgKrFnAGgaZGZwJd+2rDYNoe0Yq+FTH/49eOE0nDmX1cuZWCoihFyf11nXuxve8FQYsSQNtoQC9Iwb9rRg7TpYlwmCu30WTt/3pLOvmQ130MlW4NAqJ2+4jiAp2LuLgaqqepy42lWoD3MlzWDeWlIEvGGf7VVou5DYvJ2OvVSfna57oW3Zy7Od7/MOyTrH5nSCqvaA1jFGpl1H6zukqbjqhmvh5FHciSP0A6TwrStHXjh/2qbt283jXl4beiwWzln8LmSWfC84QLYPS9mJ8mvwwSe0B0ihH196TI9PlcNtZOwjrsQ8FnlqYDEbbi9ZvVWM1CGmEqrEPglPIFWjuQJefOFRUQQlQDeFLvLdf/8lzGbH2FqsxnnkkgXyORZVTYpXlJhK8Hnv6Yi0teED/9Hf5n2/9IvgYhLOh56u1wXTv7y0vyOXF1+wpndv89+QtcpRDVPlrb/4czAVvv7Vx1iZair3BOiSIN4gCRahskVDt4TphCOjFZ7+1Gf1+Tpw/4c+KFQ2uTh8wDibtJl8+zeEtrLdBJath8Qlv+1iXaXBqYtC4U7MX2Tn9W47cd0M0bJee7553rSbjJ3q15bg/LyCOOHUTTfwzaeeozGGdtJimiZp611ScsoiFCRZxuIeh7LqedTdLm3aaf+ALud8ulCr5MUIeMu2bXc9GbgphxQ0IpnvdF2HhASAbqylrhte0ZYb774TrBIkVTJ6A3Clnraf97MjthMQlwmKMvgGWFoEeeFk3abDBUCUGCLiHC2eSkEifPjX/hmnJsqog0pnx4feGjh/0aHwCCmBtwnA6bPKNUeF2iU+WdZF5YoxEXXR44zB1RV87zn9i4/+MdeIg9b3TbRxFh9dcBGjgqhBNSf5qKJERBxeA9pUnLOR6uG3yY1rJpWVNTmGSWJWcvcnxNmrSftbSAR6iazQAo9NWW8GKmDkBKK+QseKStLOshtuqJVFBJWUMaghYKwjEhhXDXGj42gHnRHoSBKpIVWmiCkupriM9ivG3uWigkC2nRCYJu/s54UuZgvoitsuFFvuvR9poc8W3ejSl/mS2eHRs3LiGJvqQaEZ1bTZhSXWEHxILkno47YM0tc9H1ZniQPn1ZbszQXaDQdxL3WcryRatKQu7tvpvEI7WWK3O264L0qyIHZdh4aArRskKl3n6bpAOzbEkRNjk+2o7VqaqqaPC97PgdWLvH/Jrh5hJn+bhWPO93Z7Pm8wL5c1c1DIFGMMnphDXRQirHllJSh2YUwUhW2Z0jYXU6zpO3jfVzHRGLNXYeBf3/MDXXoSck6BSTgYeGAKq9OA7UL/rEbB6kxInmEgJiNQCCHFNRrBqEkCYmVpnWVzJBCnYMZgFPUeKSUk3whGnEtA+19IhNkslDiYjdlpbAwhKNYKnDwBrZfr7nmzhq9+j+A7rMQ+lmXrRcFkmIiIQvSsYvjsRz7G5uGamx54QLn1OsGmkm4hw030peTYv7LHedMO8224a6vrYEFUvIAOk9zRZc4vavw7Kcyy5Y9d7nX+zbt8pINvmVlUtx6XlRaX7LbWWTiyJr6x2k0947rCT6a0Xce4qrFVtoybpPCELBwWm2typeZrDy1TeeN2sEg62F9oLn5vyf7XmnYS9s6nlTK41m4uelXdgqu40/ExeugCo6pCVemCTyDaldBVFnPqFISAdRZb1XifYrx2EnL3De1gJJjjO9te4ALNaXvpuG3aAvNg9yn7OuCwsOl5+uN/oo0HP22RqkI19o9ZLGlDqJcSk7mMrHMLsd8DFX1RwXy1SQEiJgq0LXQVT33yM4w3A4fqEdON9cyzNNXh1lSEJZ2bQ8OMoJKgvCAl/rQx4K3hFT/hx3/m5+HU8QSgLSZBa2iaXAmK7YB2o33fR/08LC7jJcEhUTXB4WgHRnn/L/9dJhJRa2Yaps4vWEUrm3QtarLQFyIjcRy1DYcmymMf+wSYVNMZLQHhmgUW2Qoz8kan3VzTuzGs4XHDzy6XWzx88VRd+N5XNGcBnxkI+ufLVsTCbIGU4bc25uSbrufqN13P6Y1zRFVckwSILckS5VbbCDQXmsyyl/2XI1nlctNufXG+MYvb75tBdlU2oTK0m7lUm7Vsdi2333dPNi3ZFNQfI9a5nNV75fflnmgHXnDB19vL9/Dei9v3SD54RBSnBroI3vC5//ejNKqsNCMgEszeeFO5fZRU7i4IaFmTYEsZOr2UfXahpEDnWX/su8ojj+lXP/05DruK6WQjJ6UsWNcXPE19wkpO2IqAOkMniju8yu1vf2vid+SwsUFG8xsr8OLCad9bEre+5mTIL4NJI1TW4AO4qgLp4Pqr5drbbtbJX31/B19kGkzNaIXWdzgriBhi6Iitp7HCd7/0GLd953s6vuc2IQRwM321uOXeaMNwJ2Ymi1KZzH6WmMKdjEjD47a99vk0TGbB00P39DKaWcu22/Ea0sCKOvcMmsehNTlRKAWIWwzUltvuuYu6U04/+yPCJEFMdGGaXTlZyTmP5xu6ohcFy91As8/HenglCTe7tWU7N/H5Xmu5KzqBK8cQsSJol8IEbF2xMZlgDzXccMdtgjXQprrNprYpAxa5MsbuRdKW6bjHZ9piRBtsWORHy74XaS/8a1m7nTOw2YKp+eGn/0yPvTzl8GYqQzeZbGKabUDB4yKnir2lMeYk4c6SallrTGgcGTg2HZcsmHMQcq8FxQjR8JF/9L9zampYCyYhgwg0TUPoWmAgHOYYUCEbdTLPCSQeEjQSRIhWuPfBB+DmN/Vhh7aIO0boQvf6AJN/FWjfWxILaf8xc5O4xKn24YHOQmV58IM/x5lKUoHxJYJisSr6mBivhpg+qtSuogrQdIHPfuRjCfTUCJAnIyRYjOx269ujg88bgBa7dWb1ZY4hL34vdVUuXmPJ/jdIt26lpX7JvGtYXQhwZcobqN90rXDD9fKmu25HK8u5zQ1M5bDWbrGED4U+YR6bbPi9k3VwJ7qSBL+90IVYSncTAs8HQqdQgZ9SI3iE4AxnQ0dcqXnoZ34Kjh1OkotzYC0hht6C0ic0vY5oJ/6xyH7njtuBHy37Hp42nH6BeQzCxftuaZ9qejff/oF++jc/zJ/9zh+w2kbitKNpmt6dXOIQh2UXlz6/FHieJDTKuElKwlzJ0Rmy7aUkXfyhy553+BUhwrOf/rweaSNr00DjI1UUaudoJ5P+tO3iMAFUU/yitZYYUy6Bry23PvxOcELUkMJiDHRtisM22NeDjvSq0L7nEjP3mpn7FApAkBRDpbEUZhe4725Zu/s2OlHqXBrOaxL2YoyIaMLalIiScONcio4lRhDjaDycfexxnv7kpzVtjElriwCppFNhGn1Di09QIldKaaSLogUuOfT8mIXfyyblTm7gvRy323k7XmChncvau+W6e77hq0ALbZiDfRq0sS/7BXlu5PgcJ9Aox3/iHXLqlpsJktw2ofOEjL+nGlNMlAAaU3ZsxuRLglBEYkRUMdm6pRpT9aJlQlL59KmK6bekA3b9XOru3/Z977Edu7WxCIuLOIo9RiQpHMao9n1YPsRIjCHFW1khiqYyZCbFYhEVhxC7iBpL2xjOOuXBn/0Ad77vXXDLDYLJAoITcPTJdHMZuvuYLpZ/7PW47cfJ4MOM35d9aQzMPuW8GEo8qEKn/M7/9I+55oxnrYUq14z26rF5vKgoKskXkJJZ0voR0aQIhoTUITbFETsVTNRUrz1XHggxlekzKlhV3DZ9cF40EAbL0gazHxHoSqBLQSZXmPou7ZwGPvlb/4pRPqlCaVSRNlCJxUTp4y3TOh9JeJGRKBHnTI/rSohUOJCKsyJw5y2CGIy1fRxnVTvQXN1F9+jHf4PTvhcSt1AetMPBnyqBZKBsBYwFq0xXG7rasRkTppKrK6IP1HXNpGvxMQwCg+dvAVBHODKFYx0w9RCSSydZFH1JnE4yo/SNeX1w5z3QfnlMWfjeV7Qg8G73DEuFXZMFRQmsO0WOrHA2tJhxDTAXn1g+BUtRRGYZ0DCLd4S5uKG9xhLuNd7w1YhLvNztWIxP3OnaLleY8NMWiUrjKozO3s10OqVZXWFDPesaCOOGsy6y0ViGUkACLU5UmP7B+niRtKQDh/iJAGQnU6kQFb0SfQ7LUJJEOY2sbXqOa41tO4hKVN9blwXmklWGlJI3TEpQIQFtA301lr6qyML5i+U7L5gyYylK9jKaM4UoEAIjVyWv22bHcbUY73FWUB/w3vdxhsPTltGkm2alyRDFsImy7pT1sU2IJrsx9X3J9F9dev0JiQsUNWbz+gxsFgDneNu738k5G9lsHIxr1qcTKmPx05bxoVWCJHwmV0o9FuOHpCxdF2DcRT7x278Lpzdho0sHWqB2dMEj+WcAggFvcho/B0z6gK4AEgNVxZ0/9R5pj4xYveFqzuEJRqhslRYgaxGb4HGsNSmOKSaBMRjoTB7bonPCImy1zvVCI4GUTjZP5ytUXo7PXu6717bsdG4SAJJ1qPCVUiO7uNa89zhjqaxLwoOfCemmqQi15Zx2tM7gxxUPvPddHLr1Jjl15+2zEiTFRTlQnocVjA7oAmmoDOW/HQmjsN8vJI9SDEmQsjIrReo7mEae+P2Pq3SBLvg0f8y8Aja8mC6oe2KUELs+HtjEtDbFGPHeMyw9Nxf3ewkQBIqMq/nHnBu8eDGAak7MiOnePkKrPPexT2rdKY3YNLaNoNbgRZMHcDAX5noj97FtanCWtvV0oqyvOn7mV36Zn/v7vwQmZAnHzPdarvpV1uED2plev5Gb2crupGQw62yH9+AqTjzwVqmuO6Fm3eNfXqeua9Qr1lpOnz3L6uoqMvUJbyoz8RIXAhnAvYscdoZX/vQL+nQVeMsHf1qwFQpbAmOHFQRhJ1iGAzqgV4GEgkgLteHhD/6ssBn42ic+pZNnXkJUkRjRkMvFCVjrEJOtFNuQ6jxcTn+7HXASd8NQ3Okal4P2aiW82OMKSHYU8EOrsJL6P8eGziUFaQLRbtsp1I6JUbracM87387a7bcIlRSQzEEDBvc8sJ5cOpKZq9XA1rqwmvQwmwWlglsoNrs7v/9D/Q+f+DQnTYWqx1iTlARKCbr5l1XWH5NduSKC7zxRhNo4HAavESUhGjAQNk0x+V1Cyag8+xy02WAca3YSC8wa7SPtk89q3cEXPv4pDk88jWvw0WONRV2qux1jxOVSe+XZ5+4t0HYdzjnEgXfCxprDvPNtcs1aBZWZE5JL24pwe0B7o9evkDigWT5UYtZSV2mkVMJP/vKHOLTp+aP/5Z9yKAjl39raGu1kgsXOafamR+jP2c9iCRsdX/6dP+JHK4a3vPMhOHUYqes8F/NyGTPutimVRl73RtwD2i9U6muv1DAS7vkbPyef/a1/pXTKqhpqhBB9Wv+iErNVRBA0aJZHigBTOPkS93MWNNOB+fh+14IFco+VWC7VelcgNHajvSbg7DX5RHTmSkxKpPalQUUSpmU3TTBc1lrUhyzTVxgnvGIDt7/9Pmgajv/YvSnk1BpS7QlNwvxWWYOiOssBH7ooKkLSlo1ZGIukikeS/f22Mr3LlU75xsf+mNX1FhcMzgqtpuNjjLiciLEdFVe0MSbhAZNiWa1CNIKpBkLiohB3yahAzw3i+waWVJ1Ji6k8buY1X//05zl+LrDaQR2F2Hpq6+hiIHrF1lW6xC7zyFaOkBXZVizv/du/AGsOqki0MpRXF1t8QHukfc0hyvjZ9qUrfS3lwjRTgAjpya1y6P67hAfulcPXXZNM10ZSXEdMCPiLbuFhtpkKWBHqKIw3AyvrLZ/9nY8oONBIDGEW3FzOZ1A28MDWfUCvNUmaD13wqHVQCYwd7/mlvytn8WyEjqBJ2KglZQSGEAgxafp9DdmoEHXm1hyO+WXjPEfy7yVD+NXASNyrC3mn8y8kllEl3zumxBWr80zZew/WpKS5jK4g1hBRzrUT7nrH27jqoQfkqvvekqA+nKHt2oyBl5Eelrnrlm8+oPOg4dueSwQaWOuMGaBtlETFaceTj/ylnv38o/qdL3yJQ8FAF/oYRFVFQ8RkK5gym0OL7ldVzd6yFP5hkVSBJ3jWJ9MMy5bNJAsVkS7FfJqLRZT5HWWJ62syWQcCP/rKY/qNT/853/jTP2M1WsY2xSfGOHvm6H1vRZzLB1gYtL7tMMZQrY454ztOvP+vCU6I1uKRpSEVBfbnYPzvjV73lsRZHGKeIJJ0Gw0RcZI0rW7KpBZEI7Uozhhi67EZkBTSJB1C5YQc0zD1U1zl6HzHiow43ClseFhzWCcJByqpe/MK/WXT7A7ogPZOKgkwflTVyfscwYhCJcSVEZ6OEAWdpgxCtcmFo1ZQJVVLGF7PzF97keYYvpEtQtNeS/P1x13sQrdHN/du7bDszRK53XVcnN+mZAEyRpyxRJ/iEJu6oSOyGT1hrWFS2Zx8ZIGUiV5nL0YXfB/yUhBQ5MCOcsmpFxCXWGzLiAh4JGbYIWNxL5/jkKyw1im2S9VwkvUwWRGjgvp5p2hKNpm360h2SxdBLGVFK52B0dpqj43IFkEr5rXwwoOeeoMHs7k+9COU8CopbnMNMPWcdCPWJp61TgmbG2gzSrXHY6B2NVaV4AMmA8AP+3F4L4AKh0bhbGixR1dzgwJKlbLDpe7PHxps7LCxB7Qj7WtLYnnHuz3EcBJBHic2CW2dRBjV/LW/8zc5V4MXk833qbzeYlZZPwnKdmcJMSZX0OYmP3z0a2z++aOK1/kSGMN7H9ABXUFUBEQ0B0dUNRxa4Sd/+UNy1V23M7rxFO2hhqnNySnCLFauyGq9yzQFmwczS/RahnFmkKUu2Vc7y/l8sAx3u++F1J4e9otRMGGW0SqaslRFhGhSzNW6BH7UbTC+4Wre8Xc/KNc8cK/gBB87Ch5ejClZrwiIwwV71piF7wM6b5p729v0Y/JgRRCTBPXWwxPP6Gc//BH+4Nf/L2qfINi6rustiESlHsSzR5m3xiuzcdPXUTcpsazTyGbnqVbHvOnO2+ZL8l1yKmVw0+iK83t6wcyIQdVnBuH4v//Rr1JvtjQBDo1WaNsUTuGcS5nNCo2rkLBcoSl9IZrK8EWUzQoe/On3ZQ+hxUdPLUtsYGUyKFvW5gNaTq97SyLMM29lVlfWYLBYEMXed7ecGVkdTTvWNGFNdT4m9xtLBFGJfeBwRBGJNNHQrEf+3T//1/zCPXfBisDRQwlyx4eEvuNscgtYu1TzPKADetVIl1i2NYdj1A5qw80/9xNCEJ7/4lf0O4/+Jc2kY0UsBEU0YfdZZ9mcThiNRoSYOLBmIXIOslakBwNG083NEgFyr/GB5diL6oKLFDYlzqfF7XT90tYCUm6MmYGQG2GzbRnVDRoj0SfXW0TxRphaw6aJ3PHAj3HYRG5++J29e1kFXF0ir2ULiP8Bi7l8tMzFPExmKRuDhuSJ6oRvfuozNKc3OKoGK4Euap/NbEt8QB4zJaxJcq3ioWwfs6XZGJOqgjmHugovykZseeCt96SKK1vanBp7SeqBxRRsX4wwZWoX5dEAhIBYk2DivviXenXrOCwGgsdH6euIAz00kJZ5pYp1LuEqilCZJEgW3OLoIzquOFMFrnvo/sxcUhJPGETcpjwAZoLhwaTYM+1rS+KF0OIDGyTFU40rPvRf/wrtqGZKAritXF+foo8FKZOzBJdDcl1bMYytpZq0rJ7zPP7hj+pnfvv3lCDQdWAEYy2t73pA2wM6oNeUZJu/MwUxeCdorVz9jvvk4V/6WzIZW86IZ1MinRO8RkIINE1D13WgqZ5wyLFCQ9riWt7O+nIeQtvlhLu5EFq8/rL67ZIByVVTDKdmi0k9HtH6jknX4kY1Xc5afilMcNce513/yYfk5MP3y83veUhwEarZYqwLH5hh9g0/s0YsfB/QxdOgL4excDF4ahHw4P/9l/WxT/4px6OlzklIQ2+VyWFNvfKQ9203Uo1JBo2maYhGONdNmdbCpjOM77ojx6VudTlfEtJZ9nBS/DLc3JznTbFGYNpBBx/5p79FdWaCdN0SmJ+teMRqDW3wiDUp1rLrsAgWYeoD0VgmRrnvp94DN18rVCbL1/O8Z8vYH34OaEd6/VkSh0ERC5uHzDOWQ0OOS7QwuusWecF2urJS0b1yjkOuSdAGg0k8ZPkmkqsjCDZoqlAhhrHC0595hB/Vgaduu02v/8BPCD5ADeIcHZF6WHvzYKAe0GtACn3MbV/DdaBpW6BTiGKx0sFVh3nXr/x9efILj+ihZszXvvAIJ8TB1CNBqUzO4jQp7sq3fs7dZfIsLDF3AKoZzm+f4rLsVdA0CBq3Cqfl79B5CEI1GoH3rMeOiY3ccs/drDnllnfcLxxeAQOtBqykmrwFimi7VmyxFPfHx/n9B3RxtGC2LWuMApV1bD7zPGNf8ZFf/01OxppRG4mtRxuXrPFxYGWfu2zJTo9zln/NC5oqdDHgoqHTQBhV+JFlwwmcOrE0ef2SvfN8oeAj1tXY/Lw+hv62xljoAnjLD373o7q6EVjDUVWW6XRCZZaLIAVyDjODFzJIKndrs6LlLFMDmyPHre94AGrpYzCjRmQRpqtYEPvQmNdP5aHLSa8/IXEHGsqPZm6rMOkmjA6t8ov/1T/kyGnP537jtwmtnzt/C2J9pgJNESViq5oYA6stRAx/8tu/x396953KDSf7sw0mDX6vGHcwRA/otaEhoLIANjPloU/LSYJTUWtSjocxvOm9DwlTz0OH1vTLf/j/sWZMwn7L5fr6OCmRhcqTqbTY+SSLvNbC4/laG5dB+YgkAbHgHfZZq6pYhKZpWJ9sQoT19XVkVLNplHf8zPuRO24VKsAqMyaSmQAAIABJREFUwUAgYnIpp772cu7S3nLFQjLFXAPnFd4Dn8alp34+kUMLfOSvPvsXevKs56iHetJSGUesKqYLpVn7dyMzj9WyBLBCxhhqZ+mCx1SWqUTe/fM/y8u1hToLTFzO92wQ4/qHVlLip0kZKxAiL33z23r0dMsXP/ZJrolCLZbJdIqtXEqU0/mQk+JiV6GP94+dp4sdlXOp5KeB4Cz3vusdPF0H6rtvS/Mkk89JQIXmwgLkAEj+fGjfC4mL86dX6JbtWDbZooITmtEIYuR5abErlunIopMJjc2xIL1UOH+RqVEqa3DZJOM14kNgXFkqFVY2I0wiqKDRY8lwBaYCJwfhEQf0mlGSB2P/N5AsTQvKkCAoBkXwZGiKkWWjAj+u2JwErEaiD6w0TXIZ+UidA9ETZYxEzcH8vXttPl7vyqMLW0rmLEID62HBtEubFR8ifnOSKqoYgzrLWQLtuOG0g6MuJlBgLBIjtTjUe8QsAQrOtDfryBsu0uiy0CL/1uEfEYwa8HB0PXJkUxl7SdVFYpx7BaVaSZkXOjRjFAFskMAiAxibbtKCNdjKMZGO6cjix3UqSzd8zQtxk5eCSu1oyNWBXBYYNWQ3hHJ0qhhWODoB6yNdDkZWXeg7WQ5nJ5LRi6NiKkMXPJ1RXooTXlmxbKxWIEpIwZEYMdQZK7REYJbnHt7jSuU4VxrteyHxvChPtuHgiJLmUUAheu54+CHhXMsrv/Ev9eqVBm07XJhpN4WMpgxOsYYuBJxNQehtTkrpug6jwpoxfO63/jU/8T/8N8iqRcQgkpbmEEMyxx/QAb0GVKwd5W9YsobECCbBchhrMSXrUgNHbr9ZHr7hBp79wiNqfOSJx7/D6Y0pq1VDN5kSQqAeju8duPKyyit7seJdisSVS3ncTuf2GHDZoqgCWlm8RqI1TLqWUFlu+rG3cM2D9wuHR1DPoLMSHmVEyjvI76b06556QmaywoGYeIlp4MIEUidPFf/Vb+pffvIzrJxe5whVfv8RlVniUh+DyLxVbbdQwhgz5JERXt7YgOMrnHzLmxmb2GsLlzNXQ2MqMRjRmYAYOvoEuGde1P/nH/8aV52NrPpS9UWwzvU4q4tr65AMoJ2nMSm8IoSAWmFiFI6scfMH3s01Y4HaDtqUknlkkKRaOqBYEPvwmgPalfa/kLiNZrStwjSccQImW/OCxoypqDCq+NB/+Q/4+K/9c461KeNMmJXy6TGhcgxJmYQBTeFYTkANFcJqMLz8+A94/k/+XK/+wMPCyCAkE3qKK1rWyAM6oFeBhpr8YEzPkTGEqH2yldGcFakGKgVruOZ9DwlRuPr+t/DEo1/RJ77+LY4dWsEE8FOfMzY1hVUNFi1IMVU7CYa7VWK52OSTS5280l+vWHwGmdrl7xAS5qRUjhZl4mDl1FHe/v73CkbgqqMzc6AaQtfhKpMsNtn9qEoG1d7BtQy9kDD4mb4vsUXpjUrD+aOLOyIwhd/9J/8nR9c9R6oRToXJdMLq6gqbm5sZC3CWZxzMLN5QNNdhnrt2VjjyL19mUxewowpvgeuvkbXVGuwO5ecu0bpjc8hkF5TalufIsYEbHV/+N3/A2pmOVWmwNmFFhhDACH5RiRxQD8ovBg0h3yclyrUCUyc8+NN/DW65VkajLJCGgDVVv1grIAuX74e9ZvieBc/JAW2l/S8kXiSFzGVFLFEj1go4A/feIdW1x9V/70X4/9l782hbkqrO/7MjIjPPufe+uQaxRIqihEJmQWZUpmJWRJGfOHbb3T9//hzasbu1m9WubrvtpT9bWwV0tdhOrSACzYwMyixIQRVQyFAgoAhV1PTeu/eek5kRsX9/RGSePOee+6jivVevisrvWvfeczPz5MnIkxGxY+/v/m4iJluG2v/KiJHSWNTnWpNVyXw+p3JJyqIQmDSRt730lXznvb5OwwSxF90lZTw7ByL7GrQ5YjGSa0fc9siTSNN6itKljEFNPENIxpAYg1oIEnDOwAWHuNvjv0nudt9761tf8Wo2xFAVUIYsup0tk6EyQHcus/KE7+dZ+HKNui/F7eqOuSXXsO46FuUGV0ryqdLLmaAEUbwTXFlgNqfYacF97nsZW/e+l1CaXLuTRfUJBVcU/blXM2i7v18qHtEdO44ly1gae9dZ07fiPHt0KGPkc698rW7NAofVovOamkA5qZjVc4qiIEZdqxe6J/ycu4/JL7risHbAu4sqfMtTnwRVkQjGK8/K8nffpdYM7MXBzbiltyIt8hZecqJPb5g3XPvuK/SzV3yQo2rRtgYVRAwmL5bKgfTNsN1DjqLJ4WONMXm/raMlsFNZ7vKtTxEKwauS6mLYXIs+tV3jepmf8fm/dZCzWerqjoCuc6dsZ8WFvATxSv2eq/Ttv/4HHN7xVNbhmzpNaM4SjBKyF8Ro8g525+sQJWV6sTHlOIFj976UG6rIk3/2J4QKcIaQVzNRI04WxcwTb0vwpIe6YI2XB8YnfsTp4VQrlFvxbKXDY/IwhggeaCF8+GN65dvfySQIRSohQtu2VJNJr+0WWk/hHISI10hVVczmeRJlYVgCS8T+ddmg+6EL2w3fDwuDrpuUu/8j0ouDrwpeL1+PpghC1j8VEZyx+FmdJLRCNiiN0FhoDfjKcv+HPYTyfvcWCpOW6mIWlvMSUWvNtpVdS7v3md1Hx+FeKNCSbrsburdXjPHVm7fUNbL11UWSEnfOY4DdD12jr/+l3+L83chGC1YjrWHfBMgO6blO+pdBkncRkjROmQqo0xiDmtiXwixtyXWu5fL//LNwz7sKpaDGEjH9HGfJOoyDBvTqBmu2d/8Ojcs9BmX3UnL/bwPza7/IZNvr//7Pv8IFJ5XNNmaB+LhUZi8l55jcv2LKYM61qrt75CJIzPOscbQKN07hkT/xg2w+6oGCW+Hm7mfd5gtfeBLXHDNiLe70tBSNiwlCUvw5P0mBZiORx3cLw270mLLCOJuymTVJ3nS3cL/xpSgK4nzOwSAcmHkO1wJN6tzk4u+QVkshLoIDnb5ad64Iex/o8QEfcbpYfWCH228JBoNyIE1sWJNWNRXcXCk3lcKJCk66iC8d1cYU71PZS9+0FEWB9x7NteN2d3epyjIZXQPjcJUT/OU0rzuf0b0GYodhduXac+rixyBUrqCZ1+k6VfHeU1SpHFhXtSk6Q3CGbRs5WSgnKqAyKQPVmf0Lyu73/Qx2yZfceMrT3KlxKs/qfo/YnvtNpyktENo0I8wDG3PPho9UPhmIXfh4nTbiuudtaEAaHc4FXVZ7+ltUJdvaMi8dTIp8sEVz+stiljpVQ24Z1g4VqnmBmPgkk1qVuXJoxzPxERsXBuIQ3SLvVH05hIApHHWIzCQyKyzblWV+oMp9ZqVl+zz/PZ2mezl2iFuMO70nMWVbdisnpQjdzBFhu+bv/vRVes1b3sVmHXCzlokxycDLZF3B5eyz1Blsv0pKOtp9JpUaagsnppaHPefpnPf0xwqVoKbAx4CzLoW7c3eOugg3IQtuiR1Yo7fS2TNixJnH6vCx5GIBQpoBP3vFVboV4Op3v49qHtgUR0VabAWNRCuZc5dkYSQm+gadkTWYUJeMurx/P6zz1Cx5cuLiAB3sX60Cs+6zjSaNuqIoCCHgioK6bQho3481RMykZEbgOC13f9D9ufgBXy8cO5QtlJETdU6xz/O7b7h1zfFtrr1sALxPg/Us8pIf/Rk9/6RnqwGjsfdUB4Em8wOqPLD7gYdRBsZT7JwIA+/b8DJMVBoL2xsFX/vYb+Syf/F/CSXQcf2yIblUW/lUU/7g/MMknD2PaHeOmC42ZOUOTnj++Md+Ts/bCRzy5DKTe3OWu+uxWS84Sswh9YUnURQK4wgh0IrQTB13f/g3ctMhx/3/2XdJdIqxxZ5zjzizuNNzEgFUc23N5DNfEFo3Ku799MvlrX/113pBYTlsSryPFBFC8Em/KdAnckUZuOXzi6IoaOuGSgwWQb3hTS9+Bd9xj7tpcb9LRVAKkuFps3o+ZA20ODADV3ppZzSOHKMR5xTDOFT3V7rwG0SbCmF+7UMfIHh4zAPux03v/YB+4n1X0YaAESH6SAiR0pY4hOBD0lrr+oMOJquBJzAKS8Lca7Fu52qorHu9EoqGhRG5WjvX5MWlE0NsPQIE79PEVha0PqCFpRZh48gW3/CkxwsS4Pxj4CCox+4jJDziNsIpQswy2BXYZ5ztx/icgNi00Coff+XrtTq+y8FWKfJAvbTIYdmzt6A4LG9fXRjpioEIIM7SWLixjFx++WOTrFq37gixX4QscXJX+2zeNnQ6CPTi2EtYDdNapRd1r5V/etUb9cg8cshD0UQ0N25o5HZe04U3ftF+WF7E+bZNRnXpOFka7nr5o7lww4DGhdLCiLOK0ZMYkrci5FBX56nzweMEaJTr/vZKPb8xvOIFv8eheWQLgRhTFYV+pZbDw/0KMElcNL5lo5yg8xZTOGYCu5uOzxeB7/vVX0o+wguP5hnHLFZ8EfBhUXtzOGpJonxFMlfxrN+kESP2ojMEYaViC/STjs88LUNA2oAEAAc3Hufj73u/Stty8+euZRKF+uQOh6abST6nbSnLci9VMvczG7usT7M0qax6AFc5hMPdCnu8lEP0NabXnG/JkBRBI7TBsznd4Ph8h1A5tm3kgd/yGA7d77LUY0uXSvB1pczs2iDgiNsKQyPRrGyHPoLTGU+GFc+0QKsRhyJNgGjgqk/qK379BWw0gWLeMMFiB560Vc92P9+YheG07rjOuFQWhlbIJfe+6CL3e+7TuOTbnijRBrSsklevcwf2ru813NQBnaPf3t2XbDgvvWdlEdjimQAyV+KVH9dX/uoLObTj2YhQGotXv9ROZZHBLZo4h/2CDwhG875Uicm0imxO+CetufQJj+Cyf/lcoYjgChRFVtOXR5xxjKZ4rvq91GmyjhMALnDBQx8snJhx3j0vVv3E52CnJvqkh9jpPA1XakPdJ+dcmgwysd1Zx6SOHPORj/7xX+hNWwWP+O5nC5slqBAlV1JSTeUCYdlAzFgnOjpixG2NNY9mgoIIuMRWSqZi4dKqJihccJB7PuWxQoDw6c/q217zBjYnBSfrGZV1OCmSTExYrUiRPq2bvMmexK7/7S1rtvpieV9P0NeFPbgaUj4VYs66pHAEa/livc09v+F+7JjIRY94iHBgChLwLnPEjOu9UqH12GIcgs8p9qxCVl6v8d5171PAEFOIVA1cfY2+/P97PofmgaL1bNiSENv0/uGiQpefKx1sX5dhv0hmZClkHQRmVvCHN7jkaU8UKoMxjgA0TUPpCvqqEvt5EmRhS8IggWXFGF59nZI9u54N3LTDS37zhVwYhIOuYLazi5vYvm8tfeSKITxccHWLts6orKqKG5qG+ZGCy37wuYKNhMImKa7brQD/Vxbu9J5E1YBIylS24voUeh8VMZlbFCLUHv7u0/r6X/zvnB8ECSHJgODyAJC62UKyQPsO3onnOucQMTS+paoqrmvn3LBV8r3/7l/Dfe4uVJagMU+O0ivZL83EaWSiq2MxioKOuF2im9hi7J/jENPCKnkhFBNDqkgRgDay+7FP6nTuef/b3sXU58zG1VAZMWVFdl0jv1hnrK7jKq4mqfh8nnV8w/RxunSuhdcxEsUgIjQojSgHLrqAez7hscLRrZSQogHcQr4maEpUCyFNrNbI2HlvJ+jtQV3eMKzQMQw5Jw96xBJhpyFe/Wl9+a+/kEPHWzbaSCU2zy3Se876GsRxb/i1gxusiYZew1WaBcCsMHzT93wX1205Lrj8kUIJvgmpqo+zi5PAnnDycNdS+5Q971t9RhcRhIglIHPPFb/2+3rjFR9hOo+Y2lNuTpnPdymyI8XGRXu7dqT/tf/sCESTFpU2JlEsE4TjE3jQv3g2Ry5/tDBJHlRLlxswdqCzjTv5MjYuOmjsnuQ0a1ibe0ckzVSlY2Y8TWVp5ooNYMUsSwiw3In7jm1SSKwlIiEycSVmt+W8osKFAm7cTcvCGJNnxBrEGkKu3pJOtnzlluVIwogRtzlWrbJVj4PSG4jqPda5RN0KPlUNMQ6jIR1sDDfEmsMTx3ahqAiVTwPUqjdvySuzZn/vmdC9x3ebO5FiGwf8qJVz9UL5wwhB3hdM7rLOsB0awrSgtR4tFZkYGgm5xnJOxCGNFwDOphPGOM5xtwcs2YXdaiMP/12YGZYf796gaiMEg2kNmydrtnCUJlXV8l4xIth0CGa44Bk8m7o6XwzmkG57t20Yqm0s7Gw4bnKBC7IoR6+pSU6sPIW3bW0UYNj+U7zPkilRdYQazvOGk9sNlSnAOUJok7Mjf0An49O1pe+iXfvjYP9wQTYt2JaW3anjSEHiWOYcAtX1Oogjzizu1J5EzQ7ztCJalhUA0pPcW2KpBvM1v/ti/cRb3s5GECZZwjbMGyojlNbRti3iLGKFum0XpbjMouO7AGUQVAwnRbn5YMkzXvBfhU2BaZG9hJInF5PJ+YJ0NquuSBqM/WTEucAwLCUrf1ePY7F/OaIXUsJY0IXrZN5y/ZUf0o9dcRXlzDNRQyHSL5piTHWiy6JAfcAZS9M0FEWBMQbvPWINXmMfbludiPtFHUnYt9NjNMagRlLJsKh98oyI4NGke1qkrGxfWi687B4cuevX4CvLwUsvFjRAaVFMXwKsMzRkeL9GOuLtCuu8akBKXMrPUPAeW3SiiApBaa/+mBazyOt+9flszpWpcQTfYAtH9IFCZYVzt/rBmVMrcYWjlyJRVkx+tkuiEeYEcJbolRNTy9Ne9D+EjSQ5NQztDj2Xw3bZYWM7DELOw+7c0Z6WDDGJ4POZm8h7fvt/6tGdhuuu/BjlPGBi6itRFewiETMOjMROBSQKaGGo24aJq2iaJs2XRkANrbPMneHaaeS5v/drwobkLKKBb2uc+8467uSexIXq/Fp0IYfkEkGoufTZz5Crrnif+pM1KNg2UjqHNckotJImGKJSZWmM1U9YZHdFDrgC75W3//oL9TH/5seyerZHbLlQyV+3GuyM17GTjDiX2Of5W7IdV1wWy2+xKBGxgrYecQ42Cs57yH3lvEsv5oNvebvauefk9TdRWUvrA04EY5Kn3ZAE6xM/WCFGNFNBnLME9s9+TkZbxEiqBKECXiPq00TXqQ20bUo6M4UjitCK4o3w1fe6hIse9mDhyBb4Joti2yyNtdzWpTZ3lJT9b9+I2wodP04GPFcGBpZIIqsawbpsebUtBMfOB67Wl/3273JhdGw2MNFEMRBrmDcNVVGAT2fsvImwnjs7vJyhF3F3PuPg1gF83eLbFjspqBH8xPCE735W4vi6vXbfreKsL+zgJSM5AlYE730uWRuT51SBFj74Ry/RL7z7SoyZELdnWFdhXVrEmXxeXfGKwrIKSKeX2i3EiqKgaQMUjl3x7Ewsz/35nwan+AjOFH3H6egrI84u7tSeREjeRBgO1mapx6kslPRtjDBruf49H9CjtfB/fuf3Oa8Rpj4izuJjoLAueTKi4qwlqvYcqt7o0wX3xAelLg0nppbLHv9I7vHcZwpbJdg0GZlsx/crwDxmiQ569jjTjLidYNWx+KUQc9+SwZbeQ6kkr0WI3PSBq/Xqd/0tbqfh0GQDG6GtG2xhc/hLaOY1VVGkKi6qNL5N3gyWPYmq2osau84Q1Nh7e0QEI4ITA21EjVBrwBeGk9oSN0se9KiHc/D+uWJKF1LPs63P3MN12bC39v6MOMtY4eyt8/4qikpSs6BuIRjC+z+qL/mNF3IsQLFbs1VO0NbnutrJA03ULCS9MBJrm57FInQJKGlxsqolaPKF2aqins0pJYWRvSg7heHaTeU5L/g1YasgFGbwvoSlRBT2UjRWuZfdMYHBPAVI7p9RfWJjBWC35VMve71+8LVv4dBuw8GsZUhup+aSksZa2kGBiHXJO0rEOUcbki6qxWLKghubOQ960uP4/CRy3+99lrBVohZEl/nNI84+7uSexH0G64Hnw2uaNJQc450WnPeoBwtz4S7veJf6D38SfKQNHikcjUYwJGOx9Yualivo+qirHI33HA0FH3rj29k4/4jeuGG5z5MeJ4UzeXU76MldBE1knGlG3G6wutRcsY32RSpoqTkfK3P1NKYkD2MwlUGi4chDHiCPvvc9ue5dV2ix2/IPH/8ktnLUvkU1aY3aqiAEpZnPsdb2oemOA9b9HfKhfN7fVXvp6sqGEGhCQ2EcKoZQWA581Xk88InfIkwsHJiAE7xomujEEHMNaieGTBBZ2+ZbUmt5xG2EAX9VZOFB64yklHiVv0sf+PxVV+uRufCa5/8vzmuFybxly02Y786xhcNam5wEA2pBF2YOA/5hx8vrDLY+JD24ngg09TwnKiqldcTG46fCd/zoD8GG2TODD03N4cwjfVRsxYO9Yjx2fMPFNiH4JnkSNfI3f/5yvYtOuOo1b+ZIA1sU0HpMaWlDSLWkNRWS8MH3BMMuc3vIwYxAIZbQelRSPfPolVkIyOEDHHvUQzh20MGGS8UrNPZjhMkJcOMUePZxpzcSh11pzyo/Dxo+eErrAMUbxZUWfOBEoaCejcJBTKtIn0uJFVZSebIunDEg5HafkRwlIR07rzniDEdPBDQADRBbbFmkjq1deCxzrHJS6JjdPOKcYhCuW+U1wcAjs3J8v5OFcQigKmBs70EPKNYoGE/Ec3xqqVC2LUxRKFP2pA8RZy1BfcqYJsmAOLcY4lYzmyF5ZtIiMHmAOi+QcQWmKGg1MiNQi8FtVuCgjg1VuQWGBZeZJIDfnz/kDi97mzwaiLdT5OfDDsdpS7LeWg8evkonSD3n4E7LQU2C70ahso7gI56kYuG3Z2xOpjQaliJJMKiaBUDsnxmji2z7mMd54yxt8JTOsVPXxLLgRvVsTx2Hy0W1oVUP4n7t63IxrbBsLWbr0cJSJ4nR44yDeQvRcH4s2dyuOVwrB9TQakMTPYUKQSNqDJr1fkPIVI5TBSuDYowlIDQxxfV2nXKz9TAVbnaBwwJoKjbRqvZeet+mkp4jzi5GI3GwwlraNOh8pe1K7xmCtukhnVgu/+5n86qP/DLzuWJySn6RvQiJJ2XAp+Lme3iJ3d8cmiqMwxnhnS97NSe2Cp5xt7uqfeBlQkgDlXbcmKhgk0xAyJmTI0bcXrAfy3ftqj+H8pY4tyI5X0x7nlKnsWiPHODrvukRgoeL7va1+o43vDmJyc8bHJIypUPAkfiEhSkIg57dVzMaeHmMM71ElYgkyRKNtDGkCXuzYi7Cg7/5m9i61yVCaals0ojrmDpWDJHkRdSQJH9W27paTWN0g9w+sN9z2W+MmooatELzgY/on//68znmDYejwTQeY2DWzpi4Imnc5shR5VKlLSmWlwTrRLI7DPV2O+MvtC3OOeZtQygLdieO2Ybl8P3uLUEM3VPVXW5XJaWzAbtqYGulMPajPwxOYEJe+nnDtW98m175mjextdNwVAv8vKaYFunZzhxhH0OaB63DGkfIYfTVUHOfse0D6gzBWYKAqQoa43nwkx8LX3c3OVyZpVVVjBG1YDCjgXgb4U7PSVxnJCZ0FVQWDJUYwrL+1Paca37/xfqpt7yLA95Q+RSuiqo0OTPSxQW3JUgKO0Shz2YjRlxh2J3VWOuYTDe5QVtumlqe/Ys/BxdfKHFicq4zvW5bMClM5/YNao0YcRtiv2Gk71ur3N8Omc+nWVd0eMQgs1KbFnFFmrCNyZkGkS+84z36Dx/+KDJvifOGA9UUE5TYJAmO1Vq3wkITERKVxPtUYtM5h4+BXfWwMcEdO8j9nvBYYaOAgxtQWoKPfeZqH/Ia0FMWfzuRx/XexL15tCPOKVa+oL4UX0g89HjVJ/Slv/E7nNfAtAkUmSjRIyR6xCy2RB84vLFFaNo+cap7RJrMSXRxOct3teJKX5FELSLCXODmUnnKf/xZ6jJQfd3dRZ0Sc8WRRdZy7Nug0GkH9DqFHeW+62XDikl7HlIleVCbyOwdH9A3/uGLObDdcCBKEoI3EFufImbOEDTPRz5SksLO3tD/QGpvpxNpFaqi5MR8lzApaa1Qx8j2gYJn/c6vChPSua0htDEtxIqCqJFCVhoy4qxhNBJXjMSF930wqcW8vDMmySA4l7x6XuG6m/jzn3ueHjseOBAMRUzv85kP5VT2NRIhVaTw3mNKlzPnAo011JtT2ouO8KT/+NOpGkthQRbi2ntqN680YPVbHfvSiLOBfpxeNZKGD5ys9Kfh+1X6cO/inMkrR15E9c99l6wlSXO0AJhHuPZGMI4Pv+ktevzz11FFmKjFdUoDrDcSDYAxtMEn3qETdkOLOTDl0gfdj6MPfbBQSKq1TABrMV1wvOOAZA6Jhty2bnJdIoStuzWjkXgmsOdxW0NnWH/glziRxl7kvX7/R/Tlv/G7nO8Nm3XAtRFsyszFSPIkx5gTppKxSBvSIid7FjtjsLVpHuiMpSIuG4mmS2g06ckovKEVZXtiufu3PJxLfug5wsSkeLEZlHHt27BsJLYpxoXrvOf7GImLeSRmVWtSEYkWPv3y1+iVr34LG7sNR3Fo3RIltdNq8iLWmX8oOXqWMr1T0mZrUru7+9AZiSmKrEQjBGvZkcC8cnzL930Xm09+pFAqmFSpzNjkNWxjoDAWQlwuNjHirGE0Em8J9htgQtKM2n7PVfr25/8RWydaqhBxQTFF1mszZlCFZXhO0xuK3b6FMr2hdnCiNBx6wD15xE/+sDARcGlw0OzV7LO71liEQzmHJV7YlxosR4z4crFuKDnd52yfcyrZ6+J9MiKT24Rr//ZKvfrd7+FIKDHzGqvJUHSTAu89GiJlUaRs05C8knOJhKqkOO8QdWV46JMeLxw9mDyWe3TtVv43p9g39rGzihwIBdboUArrjcS8b3jYUNTcB48zBprAiQ9crQd3PS//nf/FgVaZtlC0Phk6Nhkvego19KEodhdWHuokDsvuhR4IAAAgAElEQVTzdds7j5/PQ7tpwW+WfHYSeM7zfhYuuatQ5p1Bwe31VAO9AnAgYjDJMEMWDdV8/4RMldBU6i7EVM7rH67VK175OjZONFz7kY8TT+yyZUvIVWQ6u2FYISbIoIIRyRh0pP4XTeJPxhjTvNXpKcaIWEP0yu7EcN1Bx3N+61eFTUM0ijEFSLqkLuFrnWrAiLOHkZN4S7DfQ5gJIFvf+AA58HVv06MnAic/9Q/EukGipIEEzfVm00mWHm5NDK69ulmRIhg228gXP/xx/v4lr9DZsS2+/qlPEKIgxSK7C1bCXaTXRvYmESxhdNWPONM4G8/TPufsvXaF67UMy7Lgwoc/UC6897343Nvfq0dMxSc/9GGMF9rM7bJlSTObA6l0WWMssRDu/oCv59ijHyY4oDR4TWG0foF1SwzgsT+dM8ieFwlfinPYOYM177AGCJGb33uVvv73/pjDs8Dh3cCGMeA9hbFojL2H+lQY6gOuVuFad8xwX1+LXIQTbcPXP/HRcPFFybNtINYBUyycBDIwFIf8V4Oh9S3GlYCkkpAiibFB5zxI7RERaCMn3v8hfcOL/oTyphl3kYrq5C4TW2BEmIVkUXes22Eovb+9kllRBkKbnBlWJHEOnaFtW6y1hBiy/I0nFA6/OeE5P/6vktvTGIxl7QQ2drPbFqOReLqwBmLk5i2LBKXGc8CmLKyYhVVFpV8hrnYo2LshSSGkrLdpqxzxFr15Dq30JwgSCJhBya/l84guZ6p1i+w+R6DfcKZuxIgRtz06bmBZlGmDjVCleq/aNMyNUsRAVZTUdY23gel0g7CzSxOUbaPMjaEuJHn2rYPgcaXreWlLkJW/I84ZhJVMYVnh2GVo3peoQ/nQwTgY24BUFkFTGnITOewdmycaqpnngCmZ7e6wsbGBRGhDAGuwLhmMp4suwtRXSckRpSgplFtOCyhdL2URY8SUdo9joGtSarvpt5c4UEnGYpHUMjzJa2jFZOqFTZqkjXBwBtMbZhzyhna+TYFBC6GNISsEyKJS0aAfqGSjMy68pM5YVIQ6eCJKURSICM4kcmZoIkwrbrKB682ceGiCmSTrMISIzTqnktu1tmLMiLOK0Ug8LRhibDFOuPz7vkvK2ugr/80vUc6V2HoqV0Lsiuwtwg/rDMW+TBH02W1GoWqUK9/8Vm4ykQuOnadbj3+kgMdVSZKnI/YH6GuDLiH/v6doyzjJjfgKQK9Dml1CURWzNeHrn/jNQgvltNRPXvFBiCkxxpUF280cKS2+suxI5JFPfhwbl16S6qcZkqEIxOAx1i1zDfPrkbVxO4Cu/3cYdR6acEvyLv3BmpIRY4QQYB6YX3G1vvl3/pAjXtgqpsS6pSpKRITZbJeqqpLuocbT/v6HmcwdIpm7p9BoYDfAXe5xcbbATF8E5hZ9tk9l/NCk3ZtC9CnV2VqBVlPYug186GWv1KOtcOVf/hUHoqUMSjmZoj4k/iVgbSpJGYhrK4EN+YZJLFuJOdmSnGxpjCE0bTpfNeGG0PDUH/5+bj5YYi6+SJIxLFgW0bI9dKkOYwc86xiNxNOEWAtEyguOgDfyzd/zLH3LH76Yo8YR6rbX3Or4H52hGAd8xI6XYjSRfDtYhQJLu9NywMCb/ujFPPOC85T7Xipoi7WCuLJXyg8kj+La0NjI4RjxlYhcZ7mr2dzpIkbjMSVc8NiHy4Gv/Wr9m5e9joOTit2mJTjD1iVfzYOe8C1pQjp4kD5mZ+hVDAq7MjwO+vKI2xc6L9zQgzjkZffbV41E34JxfVWfI97w+hf+AUe2A5M24v0M5wzOuV530zlH3cwxxpxxbYlV6lEsDGHiOO+h3yC4VNQhZq3A4cIlyHISSq+f6yyxzaocmkLLVn1aCHkPNXDdzfo3v/cn3Pypz3L9PHCgSWUtvRNC9DgrmBZsbm8dA0E0LaAGOQ0mLoxbyE4PTeRH5xxBUmWjwjkkKK6acGNs2TlQYR/9jXKsjFBK0jx1jsa3WLemctGI2xRj4sppQomJW2FsKtnUWN76K7+pO1d/mkOtYrzHatwTbl4lMNtBZ1/UdgYTMvejKNi2keMbhmf+0vMSo/mi86WXCGARPe5OvbTyWiF0j56QEV8J6MYvIVdFAhChjg3OuLQKbpTZxz6t02j4qze8kYc/9puY3u+ekkQWzSJ7oZOsSSfstRP7zxp87rAqx4hziDzo9WHmwcC21khc7E7fufcQhPkH/k5f/Fu/m7yHTWS649kwDitJcH3XN1hrmdiUANUSswdST8uA6cb6YQJOZ2x5Mcwmjose8w1c+iPfK3ESEQra4ClcQfAxJdnI/kaihvSIE2N+xvNNUYUWdt9xhb7+D/+UAzuBatawaUtUlVrbpF8YApVxGB+xSC/xFm3yCq5K9pT5InoOZl68YQ3RQKuphK1tAnMj3LRR8O3//qfh3l8jVIBYGt9SuqpPTFpKShLGTncbYzQSTxNt8BS5FBEhpGSU647zBz/283qRlkyaQBnSCg4GnkSzGM06cdEhFmKqnqIoUBWiM9ysDQfudhE3Ws/T/+1PChceRp3p3+9lUYnFkvgh6yQ59kgfjBhxR0QfV4xJoopIlK4atOKipHBaR7Sv58ikAlL4GWuofUvlsjCvasrANJZO6PtUI+TYd84dOiMCBkZg/qpvyawmIUIT2H3fVfraF/wxmzsNh6XEzhtMVJwV6qbBFC7Ju4jgs/6mLStCCAjxjBmJnTGUzmeoLdxUwjP+3Y/DAy8VLSHlC0PrI4UzS6v9pYV/H6hKJSZN4dIcNG9hu+ZvXvQnerS1fOb9H2bahKTnGxQTNSW1lI5Z24DJQtltwGoSjrck0fC2bTFZvaObr+zADgVQI7TBZ24iRCPMfMPWoYN8IdY8+Ud+iOJhDxQqUJdOJAg+JO3STlx/aEX3GpZf/m0fcSswGomnCe0Ffwfsl9oTr/i4mhMNb37hi9hoY+48C/V5Fc3h58WjvpDAWdTxtNbSNA2lFESBYIVtPH6zZONel/BNv/ATQmX6UeZLGomd8Zn/HTvbiDs6dKCZFiVVTe69D7EzECNqTSLsI71R4ZMpiCVxEAvr+j49NBJXVQ3HPnPuoaRxbCmBZcVAHHq6WBySDmoj4aqP6kt/44UcOt5yFIepfcrclUhUTR6wfEYRIfpAURTUPuRw8+kZid219eX1ep1Ew24BN21YvvP5vyJsWUIhSEylADvJ0LUhIWXFeAxEEaxvCf/4Rd70B3+m9dV/z6E5VE3ExZQMU5YlwTeISQsn4yzRpnZLTAai+IjGiHUuGZ+5f4SBGPgeaZ+cDe29xxaOmUTu9aiHcHyz4p4/8F3CZoFaaKKnknLpi+qXd7lizNCgXm32iLODUcn1NLHgpJiFK9woN0wVv2k4LhGZTFJYLJDqpkSFmErqdbqlqzlyneBvGzy2KvESUA0Uqmz4yOFG2dyuoYlJr1E9qYpLpCSTTcMgxp3/duNHV9Bp7GQj7shQSAoD3SSblQSkizUayQ97Eu0oBgZimnekHwRd5iB2fXodMT/tZ9GRxjX2OcWQXgMLbna3CCZz4iRq8nBFn8PMEeqA3W45vOM5GAWaBhGl0Ra1hmgXeoDpVNqXn7NyZqZOkWRAWTFIyBnH1uINtJVlPrVQAEYImgzTYSZz1Nhz0psYFifOmS2aw+WWCEGw86AH54HNFkofMKKoUcQJbWwhayA6ZxNFVwUToAjg2mRQuiwe3mlE9tdC0kmMA43r7p41bRLEDpqqx2xvltywaWGjIFhDxFCagYEIS+ceJ6pzhzFx5XTRLelUCZ2rvig4/7JLBS149Lc/Tf/2L17LYWPYcpbgG5wYvCRtKrNmsFlaICqIZraVSW5+Fwxxp2b7Hz/PP736L/Wrn/54YVqALorThzZXhtmnc419bsRXCvpQXefQP1UGZNddB7vWcngXh/dYyrBcRLBHnCN0393wazAMDETov2xVhRjSeNt4+NyNyizyhhf+PlutMLWGZt5AVWKyVywOJGJ6jviAOy56+o+AzR42bArjls6x41t85XjU5Y/jui3buwwLWTArNRciMiZJxERNFImIphKBYiEERJUie02vf+d79K//5KVsnWw5UAdclCX+O+zVbeza3LVbpUu8zP+vadPQ4eGMZbY7YzLdpNHA3Cg7E8c9vu85co8Nk6sZDRJt+g9fc+JuIXiKQ0aceYxG4mki5M7ZpewnRMK0wAa44FufKEc++lGd/92nke05G0XKkiumFY1vk/Mx97S+I7L43yBo0P6zJBOmpVHMyTkffOWbMYp+1TMvFwQ0BKQqMEUnd7B3IN0zGY69bcQdGEvLrFVie/d6+LxnQ3GJx3ZrPmP4vrHvnFOs3n7RVIQkkLOdJRlQxgJkAzEI//N5v8R5c9jcnlMUJbPZjKIqUg3ieYtBKKUrhrA4/1CXMZ6B777LzNeoieZgE/+xIXLggmPMt2y21DLLNlMrxLDExy0szEONswXWSaq5HIDjc973R3+mW3PlmvdfyTEVylpxdcCVJc1AZaMzhtNr6esrd23tkiq7Y7vtw3B5n4Bp8mUHmNgKolAb4eaJ8Kyf/fEsmJ0OzmzgXgEE9s5ZS+UHR9ymGI3E00RX+STxJlJn9lHBGKIEzNTxyH/+vbzmef+NIsBs3rA5nbJbzynLghBirysFe+crqxCjopJWfXUmAU+K5DkMJ2a89y9ex8Oc1RsmmqqyKEQ8iNszua3zlowYcUfFsN/ocMPK/n1nl/36wprjx8XVHQQ5oqKQaynn7T5AED76klfq0d3IeVpQTSaEeiFtM6vnWGOWPGcdOgoQcMa+e6+RSVHSNg0iQhM8OIM64XPXXsf125bzbWpAV+e8i151WomJjxupbEHTprYQgE98Rv/0v/4Gm7sth7XgSB0oVCiMRazmSNaijat6jX0YORuI0SwnCokuQsuwXJmoMxy1ScLjTfT4AxXP/L+/D+5zd6GU/CXl2i0DKtQq9iQojX3wNsVoJJ4GuodX0VTDUrO5mPWkpPOhX3RUnvavvl9f/fzf46ApOFnXlIVjPq+pXLE00cWVQSnGmAS4ncVaQ4iBGCNtInywJY6yFa588av5QhmJqN73aU8QY9PAIZoHmNWV2Bke7EaMOCfIz3EXQu48SLCQAhliGApbCh+v8z6ubE7HxZWdI637XGGP8bCyUyQXGACIAVrlH179Fv3Ym97BecFi53NqAlbAFRVt3VCITSXj6ibRdViMncuew3XlVG89jDE0weMQjHPMfaBVMOISj7JLvNJFSLmzDpO0TWpo3TaURUnlHJ95zwf08PGav/yDP+XoTNlshU2T3uB9i7EGxRBbT+FSdrJfSTwZSrIFk/7218yy97BDl3jZGZUAriqIVqgNXPaYh2O+8X5CoeCSKPeSJqSsNwaH9JA9zvvRm3/WMY5wp4moMfFdJBmHaWPEAD7EJHVTCienhhsK5WantIVDBTaqyR4Dcdj5OuOQXAidqGkwSR+MREXrlkkb2JpFLvSWQ9sBakW8LDKbhxgah2PnGvEVimG4aviomzX71x64ilPxHEece5zq+wg+yT40wsGdlq2dhmLe4GJkWhQU1hHrFnyqJRxCqlKyLpzcS9acqe9/RQxejEkh75BKSU5ckTygahaJVJJqMHfvR6EyJVJ7mClHTjQcqoWjtbJVB6oQmZ88gcnvb5oGKRy2KJZ4luswDP/2yY4rx3eG5ZA21RmSrYHrmzmzrYr28BQqgcIx8w3IHtO+/6w9NILuelizc8RZxehJPA0IKdMLSOToGBEMLj/8KVsypVoeeNC95cHfdrmetxP4wKv+EjvzWLX9895JCKwiGCGKQFSiD71GFZASWawwm8+YHjiAbyJXv+LNbEarR5/2BGEqMDGpsDp7OR6rFQpGjLijQ0gcp3W0ihTWigMvxSINc908ucfrPvh3Xw/WiNsMQvoGI3vLki5JpQSYX/kxnfiC977iDWzWMWUUlwXtfE7pCgrj8AjztiEaoSxL2rrGYZaMoq4i1jDM+uV6E1VSFCrxDYUYIrFwqCQBpu2bj7Pb2EW9aWP6ZEfTuUijQhPSBV2/y4f++CX6qfddyeFoKGYNlSvR2DLZmDBv5riyAiNs13OcsbiuC+jydXlZJK309zTvX01uGRqZ3T6Xs1F2JXKX+92TGzcslzzzKemiVSndhJZIaRZaj11fWvJcDWzhTrAgMPa72xKjkXia2OON6J2JMRtzJpVAMoH7PvNJwtxw+JpP6uzvPoufe1xcaCfCckZZlCSB45xDTCYtK6CpNBOAj5FqY8p8Psc5w6aP/O1fvI5H2UKvKz2XPP1ysWV3XYtJsZvkOm/9ftHnPdtHPsiI2zH2rfE6xD77hvprtxRjtOvcQjL/cJio0hnxlggt8Jnr9KW/+bsc240c9koZFXGWup4zrSpi3aZsZSNZwBnquk7VTOJiLIb1C/nTufb0QmhDwJQFwQi+adjamKI370AtEG2S56lcek/I743ATcdh1ujH3/BXfOgt7+Rgq1wQLca3GEml90xnXDqHj0lZ0hQOI4YQ/b6FHLprHJbZg3w/WLyG5TlEc7h5Zg03lvDYf/Zc2LA5Eya5YpPjImVmD/Ue9xh/A5rU8DNG3HYYxbTPFNZZWZmJqwbmJP1C2yj84/X6sp//ZY7sBCpfU+RBonQF2kRMVKqipA0ebyWFnTV3XomZM5I+MCJ9pwQwaggGGmM4UcGDv/NpXPStTxAmQjTSq/mvlhtrY8Aau5gkVfNKNx1nB+1Zauc4Q464PeJU4eF9Fjq3xuAbKb1nBvvexy8R3lfoDSZQ1EKQRWlGhyINcLzlz3/853SzDmy0kWkbqCSJqgdNHsVhQYNh9RBgXy/hapbvl4Mue7jjBHbZwx0nsDGG7Qq+4WmP54JH3B8uukBwRTIaP3KNXv22d3PNe6+kCpEiQBHBRbAa11bxWr321etfMoBzQsl+oehuPjKFoWkajMKkqmhmDd4KYVrwxVL5tv/wr+HirxamZbIGbaq80rJSY3o/DB6QPd12XKHdJhiNxDOFQZgDBg+xpnGsNqlTlNFjGkPz1g/oG1/0J2zM5hRty7SaEuoG08K0KPHeo6qEbCQqHRcm5vqemjTBsrE3JB1DMhZ3C7hxy/GgZ17O1z7jCcKGy2xnCG3ydIo1S50v+FQOaVhpQllks41G4ogRI84UzoiRmMfFaEzvQbRBYTfwDy/6C/3IG9/GgaLAtp6ijRhVavFYV/byYucCQyOxtQtPtompBnIwcEI89cGKG5znMU+9nBMnTlAG+MS7rqDYbtgKJiVNngEsG76mv8ZV/mUXglaBRtJ8UYglNC1RDHGj5PN+xqO/99s5/0mPEQ5OAJOUPAqXikdokioap4/bP0Yj8TTRL2b2MxJjBGuYE4ka2RAD8wi78LmXvVoPN553vvYNlCo4rxwqJvi6waM459Acju6yz2DZGOy2dQPFcDXqDcxKyw0lPOS538bdn/JYoUjex6Tcb/rEG2vtGu6VEkR67uIoPzBixIjbBPtYj+u8ScF7KBy1byicpUCghqv/7BX62Ve9ja3dFmfAxUjRRIwVWlGiGfBuzhE6I2w4tltN47kCvjDMrTI3yWGgqtB4DkpBiSH6cMrzf7nX1GG/MHsnf9Pma7LzpN87K4W7f+P9uaGEB//wPxc2k2C2RlKiSnZqSC6zN84ht3+MnMQzgJ5XwQpvQoBcxqmwFsTQtjWFK2ACJ6aCLQrqrYJyJrjoab0norREEO05VsOwwKoWlYnLlQCGIYuyDlxQTpjetAvz9CbjcqhaW4wp+o4aQ8B0xmIMiLU9OXwPZPT2jxgx4tyhG3dN4WiJVK7EaoTdGlrLsej4QuuZIjRNi9jFSCbWEnyL3SfD9rZCWEkcWeX/4QOVB6KnqEpUwZkKbTxRkwbh6WA1bL4uvLxO7gZyGT7vKYuCyaRkJ7bMppYbbGR7o4KJAaMEFcRYZD+Lf5xEbtcYjcSzgNVn3pqsCSWCdVX2tQfu/R1PFVrPob//hNZXfIpDtiKElmAlDWKSVPitZkMtx5zTAJKzwmT/pbAolCrMTsz44Ov+molxemMlXPKtlwtlzJl/IfFEIIm45uvEpsFzv6oU3fp1rP88YsSIM4ZbGdjKMoEQFYvCLMCnPq+cqLnidW/ivGCwIeCMRWwSoG5CIGY92z5sfY6wzjhTBlnUUSmM5UC5QV3XyYFgIqbII3PUM3b9pyrqsAqV5ITYVEuYtbTRw4GKmwk89bnPFibkmtMWugQVkmScKMgZqn094uxjDDefJr5U9DWE0AugxhiTpw6Scaek8kmfu15f/wu/yuSGXQoBlyLBSUw1ewQ7DkhcSaPWPpElbV0VRS1MwW5T46uCemLZroQHPeXxXPwdT0mq98YQoxINebWnEBUjJnkWnV1uaO7tfaLMPu0eMWLEiFuNW8BFXEXUJAsjbYRPfk7/+D/9CkdamJ6csxkMpRG8AR8DlS1o25ZowDiHdIoR5wirnrvV8K4Vg/oAqhRFgQ+BaIW5b3FlgQ1fvpG4Lot5aCSuJvGsQhQqDGoNcyt80UWe/h9+Ei67WHARdRbELonbK4rpy6tkC3+cQG7XGM3508TapMlBgoe1NmXcieRQbuqJHsMsNFAYuPCYPPGHvoeTBwu0sLnmZTLWXIyIxj6D2ZvEBbExZbPZARdxyFuE9HrezplUBRsRNnYaDp9o+NCr38RnX/kGpVXwAXGCMWCIKdMv81yMs4umyHLb0vpw7N8jRow4DejKT4dbwVczYiBEqD3XvPkdHN32HDjZcNiVSOYeRhQfIxFFrMkVTc7t6NUZZUKuUpLH9qEGY2w9ZM5427a0MSAiSRbtTBi3mj0Sw01AFEVl8cWIph+bf3qZthg50c65lpqHPefpcNndhAkEZwhd7WsGHtPUWpCIEsYJ5A6A0Ug8A8iP/V4OtKYuFjQOsoFT1nBLwLpJEsouDPahD5AHPflxzKwQxBC6ZJLBV3QqSYO+EsDgOkRJq8+6oYgwjYYD0bKx0/CeV7yWz7zmjXrVW96qUnuMCtF7AEyRSq6ran++JZ5lbtvYv0eMGHEukfjaEfGe5lOf0Q+99Z0capWDYtHdGlCCpkXvtKoIIaCqOGNRH86pF7FDH6RZGVBtBJeVJkQERHrlic5oO53rX/feddcime5kNV3TUDuxNRAnFfd81EM5/8lPEIwmKpIkJptFepmbLtEozSu6qFA24naNkZN4urgF2Rti7JJoqAAlnZfOph5XwfGpsDMxSAxMosHESBDFik2SN6pUGAIp060JYU8/04ERZxSsT6EYLEQfiBpxIXBeazl6ok2raW/ARKxJ4euA4khEY6EzFAWTe7l0rsWReDxixIgziXUJEnGowJA40zGrPojk9OAolK0ybZQyCtG3lGXJXD1KkgzTGLIuokFapcCgGs9cib1biSh7s5uBvgaykChHltTeZBwCbaBMxKD+PGf6urrrMblCSoGhbRswDucsrUZaItE6dpyw4zRlUCLYji+/in6+SLOKImM06g6A0Ug8Hazw9HR1nyxedqWEutVUVxkgoKkiS/Tc5xlPkWs/8Wk9shu48eOfItYtpU1JL0AuiK6J/Oss6gzKsnDqsMMJidPig8eHgJokV7A1mbIzm/O+17yJk6XBedWvftrjhA2HmICIJUaPMQ6Tc9JCCg5gs2CtDD9kxIgRI84CvE8VpyB7oGLEWpslvEic7qAwj/zhr/x37oZFNeKqklldI870nrBbmxRzW2CVg7gqgt3tDyvjrNUVJY3TgLCcVd1dSxRQH5gWJWHeMCkrfIjUwROc0FYVNztle6vkqd/+NKhszrY2xKAYO+Q+sUIhGL2IdxSMRuIZxqkci32t5Ez1EEnu+NR5LEwMj/uRHxRq5QO/+SK98aOfpIzQzlsCmYdiLfgWJOZssVTX0sXFOVUWXMUYYyoiL2BKx3w+R9uWjaJid3vGEVtw5UtfizHoVz3jiYIBocXYAjQQvGILBzEiJpOQRZZJyCNGjBjx5eBLDB9dnfoQAtbaxPHuwqxtC9Gw/bdX6VbrONamiIsa4WQ7ZzqdoG3bRz4WnrfYmyjnmJa4R4JmaCB2cmYdjaiDIVPbz4CFuK75qWhDel0UBW3dUBrH7mxOubVFIHBSW3YqeObP/xQnK4W7HhOKnOwohuiSvq6wvs7yWHv5joMxu/l0sNqhWalMkjt4Jxcj5CzkwftUIISY64Tmnt9G+MIJXvZzz9MD2w2H3QRnYD6f9/yUrhZnjGnAq3x6azDLhqqqIsZQR48rE9fQ53qeVgwmKLtWuH5LuOQxD+X+3/9dQuUgBihzKaVM/O7CA10JPxiTV0aMGHF2EWNcMhaNyfENH2E38L//35/Sw7vKZhNwEZyxBPVJY5Cs2JB/9567bCbKOVTT7gzETusW9nr0Vg1EyGPuGqPyy0NXWWURdo8mJa4AFFGITUtpC0xZsK2BmYPt0vDw73wGF37r44SpxeezOQw+BhoRnJg9CY4LhlJc+vwRt1+M39AZwqIjpMyt7qfzsAtrbnbe4ZyBPrklpyx/1UGe9V/+PfNDWxwPLXWIBJ90E4uiSB7D3NGGA8WqjEEQiDZVWPFNSxGFQixtCDQxMA8tG85yeBb4x7e8h/f/txfqX/3ybys3z1PGoAYCmROUP6vs5HfOyp0cMWLEnQ2rSc7DtXRXZz4tiGUhytxG6ne9V4/MIgfrSNUopVjquqZ0FWVZdmdI4+DA8FIT08/teIXbhZ6tLqqwuAgupCnCnmX71iiExlNVE3yEWQhsG7jJKRc/4sFc+JTHCqWlDhHB9M4RJ5ZKDKJx4TRhlY61N6t6xO0ToyfxdNF1gH6wiSuetdXMksFrAR8UY6Q/qmkbytKl4+oIV/29vu7XXkB1fJfDZYUPDTEmz2MIAUzqnGV2V3Z1nrtVoViD956iKIh1i0PAGloLOAveIwp13VBubXCiaVBCVR8AACAASURBVJhtVdw4NfzAz/8U3OOuEm0EYxf6Vrm9bQwUxo6exBEjRpwW9puFhmOL9x5nHaim10H48x/6Cb1wV5nOI6WxzENErCG0PhUvMMvJIZ3B0snMrHIAb2ushptPhaGmYsdHPP1w+V5PYjKkU0jfaSIU+aDUE8uRe9+DG6fwmB/9l8KhaeJKFg6NSbCcmBwZXSWYYXWwrjb1OF/csTCa8mcI+z/4WURmaWm8CENbK4ikSHOIii1LWoQm1V+CgyXX2xY9MGG7nqXsvDatqDVETJYkiJK8hq2Fxi70tkw2JqMPOGsprEN8BB9pZ/P0PvVsVhV21nJIHQd3AkfmABNowTQpKzAArW9TExTK0UAcMWLEaWCFfbMHXRYzSjIQgbZtcUUBbaBoIrbxSdoGMKIUqlRi04IYQ5CUTCFqBl45xZzBaiVfLoYewaGU2TrkJO7eEXAmsK9xroJoymxWTVqTDVA7y05hoARKg3Uuh5QtMZII8tbgQ5NCyjGCRpJCZewTN0fccTB6Ek8XKzIwfQh4z3GmP05Z1jIUcsIJ0ObthjSQUUfaj3xCi5OB1/6PF7DVRkofcFGxYoi6nCG3VAlF0yDrXMpS9nVDZV3i9ZQFXpQ2tMnLOG+Y2BJR2NXIrHLcWCjP+pkfZX7AMbn0YqFKK8WuZF+PYWP3k8UZZIDvGShGS3PEiDslVmefIVNtmL0bu8pVQjI85oFrXvmX+okXv5pj84gzjrZtk+yXtWhIf9ukyNdn8AoxexZ1pYLVucG6rGIYiE+vjO+r+zrcEo/i0JPab8uph0bjnuvo9BBrVfzmhBuc8u2/9AuwaYS7HMMbwSFoAEyawxrfUrgCsoEoatK4b7rETbN3Lhhxu8ZoJJ4jrOsgQ+MRcgZYJBG06wgfuUZf+mu/xcGZ5/xYovM5UjhmvsZWVTpH63E5Pc0Ys6cKy6mI2kulmdRQOzheCic3DN/8vc/mgm9+hFBacGSJA5uolHZgG3bFVLNgaq/jmBscMGkCGIs/jxgxYgBlkeTXyW54FEEwRLoybtJGuOYL+he/+Csc22nZbPeOaUPja932dfvuaDhlGT3tEnMW+9cZiULSXyyMZTabYcsCEcHHlE0eA8wmhs9PDd/9X54HdzkmlCadXm7B0D2MnjEO9XdEjOHmc4S+s6zEW8zgR8l93UaYGLjvpfLo7/xWTlaWG2NLqEpaIpPJhLauUxhFhYkrcMb0hO9bApUuqy39bzUy9ZHzW8PRncBfvehP+czLX6/sNuBJJQYlGYh145Mt6weDtZE+I3Hpc8iG8DhajBgxokMXUWFV61UIMeRoi6ChhXnDte9+P4dmgXKfNW8Xlt1v+x3dQFyHYbhaSGF1o8uGce8l7H5soiM1TUNVVRTG0jQN1lo8Qjux3FzAd//7n4G7HBWsoka5xVPL4Av9CrzldwqMnsRzjX3Cs91mrxEnICFCE6BWrnvTW3VzN/D2l72KzTamSgNFSWw90QeKqmSuYd8C7sNV5zri9LDskqrQihI3p/xTu8tXPfg+POb/+QEJG5awOc2GoPTZ2ynDTalDiyvKhaMwn28pHH76d2/EiBF3ZOxHT6HzLKaKI+JbEMtN//Q5jsSS//Nvf1kPHa+Z6iJUemdEN04Hs9BVtApFWFRtiZJ46n0VlZil2IA2h91LNWiu6mKspQmRuhB2Ngqe/HM/Dpd9jTB1ySvQTSKji+lOgfFrvgMgYlBrYGJh6rjRKV8sleMbjlCVBI147yGm0lXztiFoRAq3GBh0yMtZRi+1kJNgVMDbrLlokt5VOfNcaCoO7QbYVbU77f/P3psHS5JdZZ6/c+91j4j3XmZlrVpKC1JJQhuUhEorIImlQaglkAAxIEAtBGqgp6fpHma6Geu2Nmgbm2kas2nDbGYwDLpnWsCA1NrYhCQktAASEoLWgkAraEdSqZZc3otw93vPmT/u9QiPePEysypLyspM/9LS3nsRHu4eHh73fvec73yHOgnBtFS/RZKWdu5OqKsaKwrN3Msz/1+a2JbHxyXKiBEjlhmVtUbxudlAXsA6SMrxDuP03Py8oTa7qD6H9waorBNE2JANsXps+NwyuugE8Z5omrXpXaSJiYNgnNoJfJYGdgNMq2WKWbsOXJaGjrj8MUYS78Xox8xoKbfDSy2VBIhAgr9++W/bR1/zh9yHGt+mLCK2RKcJgix7g2YCKGuDRq74A4qQu19Z9lXSQy1j5QOxiUTv6WrH3Amng/L9//ZfwU33ESqBUPpT49CYcEVk3ldxLyON/fHLzzGiOGLElYy+tLeMR7b6c61DlSoctHz8tW+wq850fOiNb6detBe9OvneAHWrDI234qM4LIhhlV7vyaGQX5MCLJqGnTChIve0bqYVX7p6wj/85z9OuxeoH/qgzNS9J2nKUiNW+xlxeWMkifdy9CmXHN23MmhqHkEX8MFf+jX71J+8h53WqGKi8gG1iAuOJnbZx7CQRBimm1f+WH2EEVYkUV1v3pM9yXaqGZ2mXA1XB/a7hu7ELs/+Zy+Bm79acBFKR5flktPA/Mofqz/+iBEjRmQcTRIhD3XOyMb+f/tp+68/+wtcvVCmTaRWuUu668sRvWQolaKUMEglGyUbJDnF7DamehVQZ+AdhtB0iclkxu1eefa/+ifwNY8QJtC0LZPpNBcOuUF7Q13aIY64jDF+xPcSbNSvLB8QwKkREByOlAz6KGBlPOZH/ju56vGP5CA4/GS6bKHXpkhd16goJoqKZdsHVqvLYbVZEugGK1KnRlBjJoGdUIMlHJrtsdrINcmxc9s+b/iPv4L96fsM9WC5M0uf61BLSwG19Kns/n1a76H15b6yI0aMuGRQZCnL9Kgj224ddHzk7e/i2k6Y7jfs1RPGwQP6ziVieTLvi1WSGNEb0VlpsZdTSsnl/32WqUoCnXImRg6OVfzdLPGtP/WSTBBrB85Rz6Y56lgW/qvPZrz+VwLGSOK9BFv12zb4uV7yx9KgOwJfvJN3/fJv2FWnO774oY+x6zzROsy0rLSzX1XWG5Yd2bo/Vhqkl3MrqGI0m/JKs4uRyc6Mdt7iECY+EDHOeDi563jOv/4faXegfsgDBVfEh703ztAGoT9GTxCdG8OLI0ZcoVi1Fl35yJYnVrYpbYK/+aS9/Gd/gfs3cByh6VokSE5VXMEwXKlm1mULP4DojFQuqbNVb+jewSIkWbphnHaJO45VPPulL+KLXrnvU58oTAKJhJRClWiGd7Laf4r4EMah+wrASBLvZRh+GsOq4EMssv+plv+fPIA7W3vzT/0brkmOJjZUkyp3JwgB1Fi0DdOdHTSm3H0AIZEONZL3pYglr0qtHF4o9otr1dFqQuth7oWTO4Ebnva1POMHni9y4jhUvqx0DXNZYi6quBweKL5nfiSJI0ZcgciaayVqZOLqbPJf9G7Ddm5yEPmv//hf2vWnO6ZNpCq95EExCUfu/8rASjYEQ9eKfAFrHF3T4qsAwXMQO4LzzMxBl/C+4tYQeeJP/ADHvuXJQnBQBRLgxK19Dj3W5qVx7L7sMaab76VYFvkd8SXMWkVITiA4Wp/gWCV37AYOao+rarSJy04EqsrxvWOkLhJjzEbbMa4db7lv2f57f9yhfY6qctxPuKbz3LcRrj4VkTsbo5NsAJ50uf9sru3QlIoLv4yDzIgRVzCi5jHKLDszKNCpYQokzdZfCepTC3Yaw9lKkh3q6UU++3sH+kpmYeCTWKKHLiq7k+nSC3F3MsVSwrxjLnAyKKeP1dxWJ5h4qAMNholbFhcCy85gMCb5rzSMkcSLjY3Q4aYLxFpV8ODB3j7GUqTyIb9q3sKtp/j1n/k5e8h+YHrQIt6Bh9R2mBlVlYtLouVumq589Td7hva2ONs6FNjgp3OOtm2ppMKcsO9gfxp47k/9OAeuY+fxjxamgU47XFUtVyURwzAqxpTFiBFXIvqFbq9bNlU6MbzzOa0ZFQ4a9L99yN71f/0mcuudVJOQzZy90LYtwY2RxOxOoahAV7LvQS1bmqmRTPHTaXa96Az1QhuEZhK4Y2J88w9/H9d8/eOEY1Nw2URbETwlfd3LndYG6mEDxRGXM0aSeG/AFqKYWFUFb9rH9M9DtogQckTPAaiy+MBH7fU//8tcdaZjooK2DbPJFGKHAG3bUu/toFjW+7CuSVyeysBba1vTeRNIZlTe4zoIIfeFburAF9ozdNfs8MBbbuYpP/YiYeLAEgSHhdxT1eHwS5o6YsSIKwn9ghh6MlJkKanLbgytQiO88if/hd1v7thpEmaGeJjHlmpSYcmucBsct+xJDdm6DKBOOeoq3pEEWu2DAp5UOe4McMfM8U0/9D3c75lPEyYO1YSrJ8vPRaNS9V2zVi2XC0aSeKVgJIn3FmwQxT5S2BPFngz2m6bVpiudX+zAe0hK/NinLdiU1/2b/52rIljTMPUVEjucczSSUFUmpUvqUSTR94JnWY8gUs5NJB+vJkcUCR4/relMaVEWXtiv4Ht++p/CYx8u1LkQBl8Ejm6kiCNGXKlImmvXLGY9ookimqAVPvTq37drT3W8/41vRU4v2JXA1Bw4owllbNQrmyT2uaChJtEZhDKvt2ZEMap6SqMRdZ6DWvi2f/zD3L4jXPukm4U6FAaYiWLURFXXpXCRHLjwg6hvPlKZs8bCw8sdI0m8yFhmkDc+hr6QpF+9+bWNVyQxqVG7PvyYIPgcsUvklfgXF/zuT/9rm7aRnWRMESwmkssaIIl5p2vN4QfnNPRP7M9r+NNJoF00zKoaEaO1RDLFIblgxhxNLXzJK49/1jezv1fzqO98luAcVJIJ4zjIjBhxZaJoDMUBorTdgtpV8PHP26/97L/nvp1jtlBq56lU4KBBRGiqXHQXlh1ZrkxYieT1kcRVp5UyrjtHRDEfWDhjf+J5zv/8P8AjHiTsBqgcCYfX4k/m+lfngkixPD6nQhI9rLx68xHH8fsyx0gSLyIOpVugjJgs087Q6xEHZrMDoqZATMrEr/IBURPiPD5FaAxuPc0HfvlldtsHP4qfNxybzGgX82yP4/K6cJvRan8MOFqfqCYE7/GlNaA6wQVH1+WIpYij7RKTnV1Odi3ttOLU8cBz/sVPsvfYh8laiHTEiBFXDvoBUPLwFokEQA46/u7lr7MPvv4tVGdaTviKuGhwnXJ8useibdA60KEEruxIYp/uNVlZ4Bgs7W/EoNWIzGZ8QTqe//M/Cw+8VqgF9YpKTZcSM5+7HmjM2nGEnNq3zDpjmWvqI44/4vLFla76vfdAOFQ2di7uJGT3m+DdshJaAO88rSXwAT9VmAlfrJQzM8/MT4gHc66aTrE2Ls2zMdbS2c4OE0VlRWaXjeVVkRCIXURE6G0YQyGfgrATAhwsuGE648w8ItayN49wkKDO1dlD4eUhG6BtONdG57WTESNGXBA2rFA2+dqy4E62bj7YjxHEIbEDHCeSZ3IQ2fGrTIVzRhu7nJ8m93W+Umtt+/HZZHsD5dyFxaFAmk45JYn9vQlcVQk1RImIZMrn+zZ7QiaIwDJ4VAIWqzTziCsNYyTxImM9Wjh4YnMU3Tr6rkcjYaMXchGDkwzayOt/8Zfs2IFy6wc/wg1a4xdtds1XhZiYzWbsNw3iHXWYEGPMuyjyweydqGtD82ZP0P6x5SBmtixoSQpSBxYkFpXwrT/wvdw2cdzwLU/PLq0l/dzReymur1NNFefchkG3rl0L6RXWm9iMzK5eMGLEiLuDLWNSZL3gzg+2WfNiHbzeYkRCgBjzY5/8vP32//S/ckPyOE1rBXRV2UGvSfR2WKpzKWLbohxWGZzhc8OIYSwXOLYdezt7pLajUyN5IXrPgRduePRNfCkkvu0nXyxcf7wsygVVENenq49AGSOPJPcjLnuMJPEywbDI5RDhBGjbLPw5MD7/u2+097z69RxLSoURkuJNSF2Hq2uarqV2NaqKOLckic4gqC6bw2/DUWlrxXKk0TvEe06fPk04cZw7poHjNz2QZ77we+GB9xUswu4EnKNLkeBD3zI0pz/6YUp1GVFANmnylvc/jm4jRtyzuIskcWjx1UelVDXL4BRoOkiOj/zaK+yTb3wnV7WWi1hYkaNKWVq9XE4kEQ6TQ1gniEP042wUxVcObZUYEyHUJC/odMIdFrnusQ/nKT/xYqEWOD6DOmdt2ja3bR0x4lwYSeKlDGMlIJaBgLiPtAkkTZmYYYgpnGmBis//zh/aO377dezOO46JR6JiXcxWOU5YLBZMp1Ni2+VdyvqqVt32QXqTJJoTzCy/XoQuRbz3uBDYbxe4MGXfK82k4nn/9KXwtY8UQoLaQ+1QBEGwaLhBJbS5LK52G9U2w4IfGFSFbykMgpE7jhhxQdhCAmFAFG1ju0PRMoWmQ6Ri/qGP2exUy6t+8Ze4JnqmTSSo4m21KB1q7vox6VImidvGU6/rQ/k2krh8vRO8CF0b8XVFNJgHuC0oD3ry47nln7xYmLo8T1ShVJ+UYhRTvIyawhFnx0gSL2WcB0m0UgndkajIPTtpI3TC3//Bm+3EvONPX/sHTDvYcY5mvkBECHXFYrFgUq1Wm33lXE5zlMOegyQOI4mQ9S9t7JhMJpk8JkWdcIBxphIe/OSbObNT8bQXfKdw3dWYN3CBvkc9Bnho1RAnVP37hbVIxUgSR4y4CDjbdDIkiktCWbqq7Cd+5af+pV0zh6sapVbDN5HpYMwZYliYcakj6wczqXbLdqj5uSTrzhOb71e7vOiW4JmnRJwGHvctz+DkXsWDX/AcYQpWAfiyqM4Dt5EX7qNL7YhzYSxcudQhq5/LMXjwvY9dQipfvBUF70EmuQvLyQlMJnvc4ZT7SiAtIrN6hmqOKh6b7tBojiT2bZlCYX39wN0PYP3gtalRTCnhvafygaZrCSFkoti2pJQIwWPRqJ3jBIG9M20WpLspRMvWGChRBBHJ5rkqeCcrv8gt49waORyLWEaM+LLhkF5NBw8IR37nBMVIubNKcuydiRxvYFdz1kGPcPE/W2TtUsLQTqzPyPjBOLqN/w4zOgBVXaMmdGbozpSTU7jtWODUMc+Dg4KroMiGALry4VR9X+bL5FqO+PJhjCRe8shfesMd1iQOookdkEhUOAKG7S+QUMNBx8k/fY/98ctexbFFouqUoNlaQlXRIHnFWVa4PUmMLq9yj1rh9oOYOSHGyHQ6pWmaZdo5V9TljcwSyUCd0NWeMw5OBnjhz/w0POxBgi+pEgFCIbyaMPH0FpHCqshlLXK4jSCOQuwRI+4RHPp6Kesk0R9RnNdvbApzxd7+XnvLf/5NZmca6gStdYQQcGl1hLWCuCPGnUsJwz7LQ4K4fL787NPRPcQgLBNIgX1NNFPPnZXyzB96Adf+g2/IKZaqaG9cyc07IVHmCM3juwtjzfKIs2MkiZc88miRcGtkaVnLYaBmSBjQoaS5Crh3zW8j8a8+Zq/8xV/mujOJYwkmasSuQ7ysFakEzRG86BzJlWrnwS20OWhXVcV8Ps8pkRLiXK6gU6ISR9QOmUxocnkLnYGvAo0THn7LzdzulCd873PhxvsIrugVMZDBAHe+t/EgJQ2sV4OPGDHiLsEG36Q1SUh+aK2iebO1qJBdFThQXvmif2b3aR2zSK5o7heDJS0K6xG0zQzGpYphZPCocSgVkjjUZVYJMEcLLHYnfH6aePZLf4jjT/06oSank9xq6RzbjlBVg3HS8jbj4DfiHBhJ4iWP9UgiDATjw2iay56KzrGyxoFiwirQAh//lL3s3/0C10fHdN6y53xOBWHLlWxIgkomiTlNcnaSmFKirmtSym0AvfeoKjghmOCKrc2cRPJCSspsNqPdbwghcGCRbnfKnRU86ulP5rE/+IJsl+Nzey68X1U5H4XBQHhIWH++l3nEiBGH0JPErdZTsmov6lcPrSqcVaFJfOw3X22fff07mZ1sqF3uCCUidKZYcEu9Xt8mVFhlMS51kthjG0kcRhJ73eKQMEfnOBM8p3YcL/jnP4579E3Cji9FfbngDysdaaSsjvuuXELp5TyOgCPOjpEkXsrYUriyzOZs0+LJxmv7VK2lslxV4mf/3kKjvPbnfoGrDiKzKLiomRB6j8ZM9kI1yWRvw8z1rgzabpBiSZLF6JsDoXMu2zxqwmZTTknku/7RC0mzgL/lccKuBzGseH91ajjnCFtcuZeDbl/V1zPFESNGXBgG37W+WC43U8lExAMaEy6UTARKFYGPfMpe8+9/katunXN1KaoQzS1DD9oGnQSSW40VVelHetmQRCdYUmof6NqW2gfMbFmw0luHBfOoQBM7bKdm3ynXPvImbvmxH4ZjE2FnApMaQh+9dYery8sckGCZdh4bXo04F0aSeCmjJ4lGjqZt09odQZTW0z55P9Z2OYObHHzuTvvdn/k5js+NqmmZ1hPatmVSZf9EErk62dmatvyuksRN3poGaRUgVz6qMasnLFLHAtDdwEmNnNkLfMdP/CjXPvFxggclYd4v18cOsJhwPlf2mWUCCRwW148YMeLuYWOMSWS/RFiREFHFSU5xatvhosKpOe/99VfZ373lz7gxzJhE6NoWVaWqKpJAJ3aIJDqDzl0+JDGlRC0eUyWII2EQfE4xpxxV9VGIprAz4XbruPGWx/K1L/4B4dodmBS6VwY9xbASux2alkMh16zWxyNJHHEujNXNlzwON1gfBsiGlc7D8XS5sFwWtziY5JZMdA0c35EvVWIhCnsx5HR2gkY7ah9IMRJCIOb64rs1WOcamNJZpbzeq66MdgWc90RJNNphYszqmjMHc66eTnCLxLVWQaMQwDkDp3SSr0BriTp4jKzBlKUgUbHSTnDEiBEXiD6HvPqx/A6bKSJulQJtI0483HkGtOb4fsf1kxnNQYOaMA0BVaXVhKsDqK3ZahmXV3WzlUgh5KyJAm1KoEbXJbyvqF2gs0QLNN446R0nTsxgKjDzOYtkRU+k4MQtyWCkyBPLMfufw/T/iBFnwxhJvNQxCA0OCzJ6nC2bmgsMrXRUkaWY3BtZo/ilk7z1//xVu56KT//NR5kp7OCxJhPFGOMyknh3V/TGqi1Ujizq4DlIzrJ2xrkcwSQXw8QYWVSO22rHQ55wM7H2PPFbn5GroWuHWYSqzqtywJtDY7bj6ZmzqSJ+zDePGHFPYdswkLRDnEdSh+uA6HjXL/0/duIg8bm/+ADTmL+HkhQfM2mKYqXwIqNf+IZ0eWkSxTvMDC06TBFBHUiVybKIZ38+p97bYzFxPP0F38XJqXD9Nz1VmAjmcmGhUyn5E1lLK8O6+8NICkfcVYwk8RLHMHWwrdX7mi3MUTsQ6FLERHDO5xREkyjqZmgTd7z1T+2PX/5aZrfvcxUVTowY46o5/N0+/+0kcaVNNKImpvWU+f4BlfPUVQVdLnRpphVnSleZeYBHP+MpPOxZ3wwPuE6owbyjs0QtpbLPHGZK8lnYPQ6cI0ZcOLba3ABIidqLg6SwMN71S//JPvvO93E9FXUbCQhtab/nYtbnpZJqdcjSKgYySYTLp7pZNBfhJcvFfOZLujlpbkNa1SwC3B6MZ//ID7L7DU8SguaCPQ+IW9qKHbL9KgWL+UD9Ee9KGGHEiJEkXtLYjBwe9XU/Gwnqza6xlV2FYQTrQ5Mp/+yM2//oHfYn/+8rONEoUzW8yTK6d/ffw5AkrvbVTwKqCecczvnsD0apfjRQhCZF6p1sAD4nEXcm3B4Sz/mxF3Gr73jAU54g1H2jV5cHVWwZOR01OSNGXDjWCubWkPIXOSbo4N2/9J/ts+9+H9e0jskiIZqWbgf1dILGRGw7vPeZLKa09Ansq5vh8iGJpOz4EKVou51Dsaz/PrbL6RS55pE38YSXvBBuvFaYFHmRLylmWBUgwrp9wza9tYwkccRdw0gSL2EMSeKmQBn0rOxnTZ+oilNXBp6enBney+BAEU438PHP2e///C9yzUEiLCKuRBLvrk5oa1P7wdkJ+bwWbZPTzJ0iQOU8VtJRUbtVJbSCTgKnNNIem3FmN/Dsl7yQE0+4WQiGOsFcrvzTniCPGDHiwrBRvLIqXFEkwl+88nfs+tbxnle/jmuSY5LAp4RzAeccokYTOzQ4gjhqzcRSXY4kJpdHup4kXi5t+ZzL6eaUEmaWx7XgiJXj/o9+OKd3Ao958QuFa/egFvAu6wxxqOVCl02cfSguF3B53Q5r2keMGGJU7l/C6HUmR2K5vDzbJkU47dza9s5nU+tQonneO9iboDPPrTuBtktc7WvqaGteiUPDWziaPC63sdV2PWFUA1f+UFNQZTKZ0HUdoc7V1UqOYorLdhmW8oBZATqPzKrA/ukOmbecaAMsFGrD1R4s56y888tT2Haa53H5jrim47g74tLAITeEc214HhvnTfNiTlKCBq5p4MR+xw1SU7UdTgyx7JAQY8SlnFPwVUVqO9o2Mavq/P3n3ksGh+be57Pd5raqOa0cxGFeMPHsW+Q0yrHjM+7Y8TAFKsO8LxTPES0RxG918TrbpRrHpRF3FWMk8XLD5sd5vqPCRgHM6nFb6V0sW99w6gA65cO/90b78O+9meupICYkKj4Z01ARY5tF2B6SDWxyFDyCE4GkuFJn1/skavbLwKlbG1hB14jk8vTK331rq6GtThRHWwmnPdz3MV/N47/72TAL8KD75RS0d0vhd0AyO4WsDaL3ErNSFd1b6JSzKSbgwyG6nxzXqsvPcbkZpNs3P4f17dYxDvYjLhTDTMRaO8/zQXEQMHJ/+Mp7MAMvpNKbzyeFxvjQr7/a/u6tf8bkZMOuZV1hb0fVF5Ztttvzg6zoxahm3tIyeg2rscaW2/e24muvVWESKrqmpfIeFSWp0qbcqjSlVFLOFXMSzbTi5MTxjB/+Hq555lOEWcA8S3K4ljG6kOtyT+xjxBWBkSSOWMNRHNOKkNp5n/VFzsN+w5m3/7n9/q++jONU7OGoo2EHDXvTGU1sIHiSJXCSlNDnngAAIABJREFUSZVC6rJYPWuO8hGX5rFD0leOncmiLp87agBfpqtt5cqhztMF4ZRGmqnnYOJ5/DO/nkc+9UnwVQ8QqiIAjxFCTm3HmAh1tbwWSRNeHCJCSt0yRa2qOAkbA62eVyRxSBIPEcRtHwB3KZgzYsR54dA9dT7TQbZFoCeJAuRemgIW8xcxKUT46P/3GvvE29+D3HonV4cpcdHmiH8ING2bXQvuhTfz+ZBEWEljVpx2nSQ6PCklplVNjC1muVDFe8+ibfDe04lhkwl3aMf+TsVzXvoi9p78eGGaC1k6ViT+kKzoXnjtRlxeGEniiDVs3g29H2KOJNrKeLGLOUWdFP720/af/rf/g2taODZXdhUqFZJ2eVsn2Xi7RA96AXqO4tmqRd7GwYdRQjae3+x5ahyOOPSRyGiKOY9Maw40cWCRyd4xrn7AfVlUwlO//ZtxNz86i8KrchWCx1KpGhz0OF2+D2R1LfoT2LyQ2wbws4UW+9ccNQlsRnnPsrsRI86JjfvsbNwjbfzdk5akxcffgL6ILSkcRD7x6t+zD7/h7bg79tkVx6SqSaZZGlIyCv3i8FJE3yoPVuPX5hjWd0zpTJdVyN4JLhniPXONNLOaE4+6iSe+5AegFuHaE1BVkCL4enmxj+qDPWLElxMjSRyxhkNcp0+r9tqbrsNVFaSEasR5gWTQeTjT8bmX/46974/ezmSR2PEel4xQZYuHGLOc3ZH7iYp3dGKoQKXrg2xPELcRxeFj1aDacdjSr39e1HL3hmRZ0zid0VoipgR1YO6UtBs4aYnv+8mX4L7m0cI05CW7c+AyN8atSGEfcdGUcMFnbrehsVxtuOX3frttj53t+W37GTHi7mILSVwrhBtsltY3XZIUA2KKVD7kheOi429e9yY7fqrh/W98K7sHiV0CghI1LdvQSVSqEFCzy4Mkmlvadw2JYlVVHCzm1Dszmq7NLZRVmUpg7oyTlfDgpz2Bx37/dwnXHYepLwvgnhUW4UpRtvS9sIefwYgRX06MJHHEGrbdDWqaiZ0IqYtrhtQU/RGLCC7A3Dj1h2+3d77id9ibRyYJJCrJIhI8vpjEplSmnVKhPCSIbnM1Pky5DsijGNSpZLcGfZ/XNUE56peNaYVQTYiaiJYNfBOZMNbTGae6Bbo748zM8fTvex73feLNwlUzqKvc77T3W1RDY8SV1mHGoIPBhrZTN67p0nKnWFQMowNwhJZxVKKP+DLhXNK0IXGEw/dnJixK17ZUBL7wlnfZG37117mxC0wXHbV41OLStF5jtrTy3tN1Xe6hfgnDFRuaJMWpVnqimAtx2rZlZ2eHRdsi3uHIldwC3Dkxnvij38+xb36qMPFQCZ1GzAdQoXb+SCJ/Tv/bESPuIYzVzSPOCsNw4pZ2MT4EtPQTBZaV0Tarly3+Tl014dYpzKNyQjxT56iZohppFw3iHSGEbCBbikV6ofq24Fovat8GldKj+ag3oIYLvugJE4vFAl+FXFQTOyon1Cr4g5ZrvWd+qoG5cc1+ggOFGYDiA3jxuXhHwJWWfodMzAcFJ8r6JNtvezbTnW0uZjKckTdxPgLIESO2YGvkcAtr3FrUslwj5uxA5QLsd+ztd9yHmtnBgl0JLLSD4OhSwotQUXTJTku3kXtv5fK54AykkERPKbzZ2GY2m+XqbZc9WqNAI6DBceeu4+SJKcemAkFQMcRXuVDOOSJG2DDJHrbYGzHiK4ExkjhiDYc0iZrwzpNKR4TgVhTHzEANcX1OtpC+rsNuvRNpzF7xs/+B2ck5V1vFMR/oui5HEQNEVYJzOAVvtqYvhFLt3B+rJ6V9utn6qmAtz6+fdz/x+KGOEXJ3g6KDNDMqgy42TKY7zGOLq2vmMeF3p5zUyJO+/Zto9qY86HGPRh7yQMG67AsUamLsCGFy9MUcjO9HVpFukMrNz6B/W0vBuuhKvwggo8/ZiLuHQyRx2403lFH0C5L+PwpRoVNojfbP32uv+7//CycaZU89ThOdGGE6oV0scAo7PrfUjBiuZBUuJjblLD02sxmb6BetzkIZk/r3oeX5vAOz0lY0BBoRDibCqUr43v/+R7k1KDfc8jViVcjZGnF5DeocXeyoQpXPDY7Uj45f/RFfbowkccQaznU39BFFK9Y4y0DW2qiluQI6AQv4+G+8yj70pncwXXTsuQq6DpwgrpBMyyTx0LnIYZII/YDuyuPrJHHTr9GJLM81xkhV1zSxK9XWRR8lgmJYyIN0NhPPq/02CGfEONjxPPTmr+HJ3/ZNyEMfnPv5+WJA7gZE7Ww6xG0Xe3jdtqSo17RHQ5K4xEgSR9x9HPr+9pCNjYb/eySFz9/Kn77yNXYsOj75l+/n2IGylxwSc9Fa8rk4BVUq8Ugq6dgq0HTt2qLzYuBCSSK4ElHsRyQt+mhDHagTWi8snLCYVjzy65/MVz//O4Sr92DSt+ErkciU8K4qJ7R5ooPfR5I44iuIkSSO2IrzvSu2RsiGq+oINMptf/ROe+tvvJKrF7DT5epGUQNLOO/pUBRDJUcUK/HYoHerblhL+CLoVtG1x4etutZ9E+2Qoe2R3maDfSUzCBWNJaLLnRCig06M7/7xl8CTvybribBS3GJLneXyyhiQUu63So7OOu9X2kQjp7F73Vbp45r/9SdTfo5ttUbcg7CkORMALN0LzJZeoVlxbNkvNKXs1NcBCU698z32xl95GcdapU4wSVAlqLSPph2hEyY/nheHFzfdfC6S2PuhOudyBsQ7YnF8sOL/KgY+6tLMyoKjRemC0FTCqYnjO37sh7ktGDc+7YmSxwe/GivOJifZhpEkjvgKYiSJI7bigkgiw/QLSBchOvjMl4yF4w/+l3/LnnomSRE1fO1pNWZPxZTwTtCmY+LDcvBOIsvJJlvoHE0Sl8cdFsNgWycjQ87aFcbMsvE3OQ3UqsHEcaDKfDfw+ann2f/o+0l1xY1PvFkww0SRSbXcT+xKWlqLh2KJHmiKWctVwjmx65DiodZHPw9HeHQjTTiSxBEXAIM1a6uCpHlRE8vdFhC8QvOpzzNpkr31Fb/Dre//MNc1ym6bu6t4BTdQ1W5zHFh2VSp/39tJoveeGGMmhE6KU0LKRTdVwCS7NVQmiBqWlOSFA0m0s5qvfvqTecB3PQvud7VQGRZqEhD6qGz/9d12DeQsj48Y8RXCWLgyYivOd3F7KFlUBjYxt4p6BQfOiMechIDdce0MdwA275jgSLHDTPBqYKBNZG86y0Uuzm3t77x2SDscqYD1CIYW253hO+ojjV5Xx9Byzn0EsioRBFXFTWuCd5w62Kfa3WFxMOd63eW+ZyBOFE53MKuQyoMqMXW4KuArX+yC8oywSA3BV0uyaJK7ukgx8E6AmRLkcCrOyjQ8lIqNc8aIu4scTTd8TxKLPZSEgJIwjIrcZ5nOMVmosTCuvrNBW2EadakLTr00uUA3vrfDbki2ZZt7I8wMVSVMcjvQpmnwVaCa1OByNXLUBBJAE3U1oUsdOqk5GeDk8SkP2KsEZxBqREsxCqtMxtoCe6D7NAbax81tetzLr9+ISx9jJHHEWXG2u+Os41OfrrG4Sp2a5lTVyYYPveJ37K/e/k5mHdQxETplNwRcVDxWUl65uCUTuFXhCmTrCS352qMI4tlOLEcZJfubFZLZ7y/vf5Wm9iXauGgbpjs7NKnDnODqmjZC44wDL7Qz4Ynf9i3c/5E3UT36EUKQEqEpqWSNUHlwQsTRkaizISMJxfUEsPRlFWzZw3r4vpS+ZeDAUmfEiLuBJVkrWuPiIN8/mm+2Nn9vT/7ZX9obXvZy9uaR2QLqpIQciz9kdD/EZiek/nfdIEIXA+eKJIoI0TRHFDW7OvSm2E3X5ciiZTrtQ80idrC3w21EnvvjL2L25JuFWqCq8uuU7Csbcpq595/0m6s+WfemPLJt4vjlH/FlxkgSR9yzKINcYt341QPaLqhcVYROwmfe9W7bPYi8+bdexd5c2Zl3TBplKsJB6pDKE0swzavgLae0crAyk0R16wa2QxI5hB7S8vVYL4DpsTT1NkVVmU5nNE2DM6irCouJNmVPM3NC9J7olM47WjE6D60Yz/7u57P3wPvDYx4hOSSjMCk9o4FQZik1w5xbppJ7ReKSBA5ESMN082ioO+JCkVLCe8leh1Wdo/lnzuAmO/CJzxnmefdvvJIvfuhjTBaJGYK1ymRa0aY2a4jTivip2645HMpFgubtL3bHlXNqEgWcyzrEZMpsNmPeLKiqCvGetm1R52kr4aByPOoZT+Om5/wDYSfAiWO5VSGCYWsFbrmWp+//fu6MzdolGiOJI76CGEniiAvCIQH1BknMETKh7tO9GleDZSLbZyyUL/zem+ydv/sHzPYjJ+oJXbsoVcdaOqvIsp0fgEomVDYgiX1KK2NTq3eYJObJyR0iiP3++t2klHDOE0Igth3ExCxMETEUIVrEh5p521DVNR2a24+FigWJNAmc8copEt/+fc/nhod/FeEhD8oV0i5HEjOLjliVq6W7Yi3kkK22OaNwfcQ9gZRyX3LE8lckluhhhC++7U/tTS9/DbuLyG5j7ImnPb3PbDLFhEwQg18jfT1J7L9RYfDVGso/QlpJ7u7tJNHMEO9KV/ac3aiqijPtglRXdJOKM5J43kteRP0Nt2Tx5k4gIoT+y9rrji1biqlpvu7l8eHCD85j8TcOACO+QhhJ4ogLgi3rg/sHBuRMVr1dTRUzwxfLi7adU9eTXNXbRsDDrbfznlf9rlWn53z2vR9k1hrTJIfE7TkasYoAiuWRdt22wh0x+azSyf3zq4poW1LLofWOC56uTZgZdaiKP6QQU4v3gmqiqmq6lI2F67rmYDGnCpPc2aUKLGKL39lhoRGrPJ0p3/Jdz+Ukkfs95qvhoQ/K6WlHDr/Uvkwc7uhU09qFHzHi7qBYKnWa2d28hU/+vb3zt17DbR/7JLuNEZqWmffU3tFpoksRV1dojHnxxorsDUnXpgZx+Hy/zcXGUSSxR2+d5YLHlcI6FwKLxYJ0fIfrv+4xLI5NecJznyVcdyLLSepAmzp8qDL3Tkrwgwry4UDVj5fbFn4bD4wBxBEXAyNJHHFByLG04QOHq21jlwj1qgijT29hln8PLptwJ83q93nE3vEX9rr/8lvszpVJKhHEYcW0rfc6OdzKzx3BqXRt+5UwfH3roeVOMqUKk1zRWCqPIU8ggpJSRETw3qNqxBjZKenpEGq6XruUst4r6x9zWvpgVnHaJbra8/TnPosHPuUJWcN0/Yl88OBKsGFwXS9gtrgnJE0XdbK6iAfvpRNbi4W2PPhl4/QXHEXqyyU0G4J2yplPfMbe86a3sTdPfPI9H+CqRtlpjEmCoIpqXLa+rCY1++2cSaiosx0iSfJXt0cvC9k85eHCDO5ZoniUFvKCkJRqOmHRNtnvsfJ0YkQH3/riH4RnPEWoC/ErhWdLK6tyDiKgXcSHAGW88MVCR8Rvv6G23OcjSRxxMTBWN4+4IMhmWncYOuhTS/V6la73fv13LSNp0JIlFr4wdXzpWM3CdewucgeHqot4BIuJEMgpWe/AcnWydjGTT8ipYNOl0HyJjajkilwWbWAf7TC3HJU9Do0lLaeG9GS1pJF8qUK2lHuyVqWzjHMO1ViijbHUA+SdeoWpgDvomAahS3DtvsLcjKiwUKF20EWkcrnKclkVaXg8hi2dFAXWO+DAmrWJkT3dbEmTy2djZR/lGvUpNdmgOGsRoeFnidvOhjZfuCXdj7m1CMrWl20+uYXlXggZuys8YkCtVr1zhzsY3PP9ddrkc9tb3B2+Nn1Mu/+MV5dx8Jmm0tqu7C/XSK2ezw/nYotUOnrI8p2QvTtjgmjsNR0nziy4qhVOd4qcmTMLU8wUE0VDcQBw0KVI7SusaAohf2+G10JlfeG2GdW/p8ihDcYfg0PXcl2CMuyUUlLjCiEEpCzuQmm32cSOWT1BNUGXN3aTwKJ2nHaJM8E4dSxwfCpQ+1Js4tachIYtNX1VrK5w+FDnc+u33XazbnlsJIYjLgbGSOKIi4uNWTR2DcFV2YS7jXzy9W+xY63xtlf/Htf4mnRyn6vqCbFpCZLTX6GqUM1WHH1Eb7FYUNd1HuSHB9jAZivAzQjkoQX+chIqZOroMurzhMOcp3FGrD1nXGJRC/d/1MN45C1P4Npbbs559N2dwkz6/L1kI16XSUFKq1T+pu+dWhbIW3l8SF5C/0tvDLzsorO90GcrSVxdjnWsXZqN/ZUJ85wz31HDU0/I5BwRvnsIR769c0R8hgVGg03OSRKHsW0j27A4BCeD613ev2r+uGVohg0ki6Vi2fL9kmIm5osW5g2YcOqDH7T3/ck7+PyH/5bd5Kj3W3bVM8OTYlzeD8kdThXfG9LFy8IzyvmtZRsOk8QePVnMPohlMVm8Sftikq7r8velcsxFaWc1Nz72EbTHJtzyvH8I97teqP0ycjhixOWIkSSOuGjIUijDu43iDC0RDl8U8E2ET3/B/vjlr+LOv/47jrWw24J0HaHOFZZJDPE+a4jahko8tcvp37VIAjkiEsu47ky3pKqhn3xWz+mapqrXMV7oROmCZ9F0hBBIGIScztpvF8yuuopT3YLGw+Oe9lQe8cSvgwfdKBzfzcxg4qG2bKeRK2zyNSs6qj5iK8PIUikYoI82xlS6PsiKWBbGZTroxjHEeaa/zotYnQt3JSQ4ZPtfKUgfe3Ub5HlwLiXi3JO61WsHvx8RIY0lAri2X7X8+fsNH81+LC837VIWYQZNykzvtn0+/6a32V+86W24tgONeFN8MmYScF0iZKNOQlXRaVorOOnv+d6yxbi4hSfbzmOTzA61kcNtTVi25TQviPdojPn9SSCakiY1t7uO07ue73rpi9m95WuFQB4eug5mU5CRJI64fDGSxBEXDcbKC0xMsS5HBfMDpSezFg+20t7v9re8w/7o5a+l3u/YRZh2iWnwtMuohyJR2Z1MaeaLNUNqLWnk5FaRh02PtxW2k0S4Z8X3JuAlR/lEDeeyVilaDg9ZcLTOiCGwrx02Ccw1osHx8MffzGO/8clMrr8GplPh+LEsnLdCFn0hj0Ke0Koq/54SuCp/Am59hjcsd5lx55dGHkYWz3eqPDKVfNaND79ENp+Qu/jzQlD2Y6K9ynX5lGNwX9j69suTli3vZQtR1JJuztHf7Nm53L4gpS6TJVf0bW0DvhSFRaX75Kft3W94K3ud8bd//n6ORWE6TwRVqsqjFgkmtE3D3myHrss+oGq2jCAuTZ8tSyV6cnYxLWzOZs69qXn0g2vbR0bzC5RQVzQp0VjKv2u2tmqd45RPfNP3Po/7fOMTheM7MK3Komx5Fl+W9zZixL0FI0kccdEwTKe5pVWErfJnvvy9ZCIGsYiJEnzita+zD/7uG9k50zDFU4kjLVrqul52SfHer6IGhSTKcHIRxcRWE91G+ng44Q9NgXtc6AQppmsaRsTl1GIV8nsoRE6dJxUrDpzQxogFRxcVN6lI4mhEefjXPIZHP+1JcHyHJAn/iIflqukwCH0GR08ezVaazd6fsX/fcJbIn2zX3R1Kqw6wnMjLz7Nte1dwd/jhmsRgsK/NKf+shFayxZOxYWp+RAr6cFLZHfpteb0PHc9IYiCOiC21o5CoELwq0hmYhzNz2I+kv/mofeBP3sXHP/BXXFPt4lPCmg4vDqfGxDkkdtAlnHOEEGhjlyPaIpjLkobNRdGwKOViRhIzSSxR0+U9fPh8hzicCbBskl1VxInnjtSRjk95zDO/nod/2zOF4ODEcQih3OAu6zILeR/N7Edc7hhJ4oiLCiP3iQ1LPd3gSYFs0WGrVGjS/F8kd2/5yGfsDf/hP+LPdEzVmJpDYq64DlVFE7ulb1tvneNKCsqZktz6JNMP+YeIzIAo3pMkEU15n86tRfU0ZeKae1tXWR8FS71Ub/AbJBBCYL9p0Qq0rmlEaWrHgSS6ScUNNz2Ym7720bS149ob78/1j3nEihZW1SBduirJcLDu5bYJ2U58Dvm7DRkkh18zJEfGXZ9wL3T02nb+23DovMrJprIHP3x840WbZHr19PrRtrZnM3JE0K2KfBKp0BPFo6T9A3xn3PmJT9nff/DjvPMP3sjVOqE6PWfSGSfqGTpvcSjeV5gz1CxX62tidzojpZTtmkRw3tNpyt6Aut1kftMd4GJh2JM9RzwPk8QhMewfW/mgCskLC41cc9NX0V094wkvfAE88Pq8uKpcGX4CFiPiM1lMAp1qJtpfsXc7YsRXHmN184iLCjXFO79MO0fSsloYDI+j61rqqs5k0YU8m1qZSXcrPu1brjo+YdZEdufKzGDqAvP9BTL1A4II3koauwz0hz0Y8x+9sc9mXUqferun4Cc1MWaDcVUFLX10xQjOISK0zQLvPZPJhP35HDNjtrtDMqUjsUiRemeKWSJ1EY8RFi27s5qmjdxwJnH89jkcm3Jd6+CgtF6rHVhEij+jR9fsfXxP0o+gTsKW3t2wnbkV0jOMNvb76J+XbWngs6Sbtzx9JI4ik37zySN2eOi4ZTu/afm05fXD6zTcz1bz5CGpFsXEkSuc8x0pbcyee6rQLEAC/ksHEKacOGNwWrlxLtRdR22eEARtmvz5Bo92kdR1zKZTnHe4nQmnyv0Fkiuhu5aqqrKudYverv/uwIXLLe5JiIETQw3Y+P7259tnBuqUtckLjEUlNDtTDo5NOFkZHJsKlcuyDdyapnf5dYgwDWOqecTljzGSOOKiI9O9dVtuRYu9ii4nqr76MEbFB5dli13Monxz3PFHb7e3vOK32VkoO+rwKRWj7aw7dIDT9SP3WqXDxSsrE5hNE+AhLnSSVDSn9FJOjVvSYtod0N6A3Hui6lobL6VY2pRrI31lZv/+xGcS6cAmgQOLxLqiccaCxEMf+QhuftqTmFeenWtPMLvP9cKxYyumJqwimz1RkI3/QG9js35RNv4+DxJ2V7A14niu3PL2h46Mkm7b/dbNjogcru1mC7MdprodWqKKuhFyLEbXWs64K8bzJvC5L9gnP/BBPv3hj/DZj36MujPqzjhGQOYttTl8WXg4R67+LxW8k1DRLBY470kOxDvSYHGQ12C2tTp4TbrBRlTuoqCPg28+LGv6xH5xl887b906x3zquPohD+Ip3/c8ePiDs3N/IC+gZHVvW2JVxJVYD4GPocQRlzFGkjji4uIuhIaGqcqlDixproY2yXrFL93Jh976J/beN/8x0/2GvVaZRQhYIWIOSUotnmiKumL2okYQh7fVhIp3JD1c1bx2uhc4SW5L2x11rHy8wwfbdnxbeiquigsMR+onzPI/SS6McVXgqd/4DVhVsXvd1czucx1cdw3MJsJsAnXIusbgygRpmURm35y1PHPmNauqXE1pZcWCX1VNy3p/bzgcZeyVBrFNhMoPnshZWJHV34eEhn5j55IftxLRXO6nv4Ybbj6qqyLwQ7zXBiRxC1EwW3lYrl5QPhNWv+e7T8sNoCsmk8gt8j73RaMz9j/8cT751x/hI+//IBMzaJTaCVjClwp9Z6uikm3WLyq2RZMnS8/AIfkDtpLEvJ91kvjlhKouvQv71LeI5AVUufVMIKply6u2wyu4cn1bNabTKTFG5qnD7Uw4LcZ1j3wIp6fC137rM7n25kcLdchMMFSre3mje9QS9/CiZ8SIezNGkjji4uGoW+9QuGf9JTp4WlRxUihjinn130X44p3QYp993Zv4yze/jRN+gus6XJeYhop0sMBVgYUmqkmNxgRdYlLnDimz2YyD+fxw4cvwNO+BKMq2KA2sIh+HzZvXD9hHSIfYaJS4ecT1P4vZ+CJ11Hs77DctjUv42YwzqYU6sNCI1jUPevhXcdOjHoWbBswHrrvPDUzud39hks2BqV22ZanIn4MrpGfQksysN/vOvXBT8WfMzQe3V+9u3ic9ydwkiUsetm1C3ySTrPG2XB+15fXLy12eW2Yec5VPXzaxbjO0cQoaE87392eA1OXq8hRXRVoHCzh5KofTT5/hr9793/iLP/4Tpp2xS6DujKkauohMRHBJyn2/XfzQ31fLgK/Ykn/2iwRnUKWeuJ49SnjYT3RFNL+c6P09+6j60MdwSBK7pMQY2Z1McyGOCCka1aTmzoMzVHs7tHXgNml53ktfhH/y1wnT/AZskj1thgsbyjF7G6mRF464UjGSxBEXD8ahSXxbZElgNRduidr03SR6+xYv5B11EVqDL520z//BW3j/m9/OXgIWDQ4hhFA4TEXXtFQhYDEXiCQMX4VlxKLHZnTlwoX7Gx0jBugtes42GW+ruD4q6AFbLEJEULLlTSS3CWtTS1VPaWOXuZB3mAkdEecrEgktOwguW4bEOtBNHQ967KO45qE3cvUD709XCX5vh2vvdx/xu7NyopIjaUWltzrXQofNgZTImuRwYIwNIUwwi4gLmKXczmztjRmplyY4WdNWAksj8SHRMSyn54f+kP3uyr3kNjR5AmsygHwPl6gqSjItUVyh75ZiKeKT4dRovngnp2+91T7z8U+RTp5iNxlf+tRn+eKnP0s6aNiVQGWGxKyfdQgaYzHSNmpfk7qWYJ7NFpPng9VCxIpG9zBJXHu/R8oxzv+YF4I+aggrwtiTxf53gEkobS99gJTlJd5XJFMWlXDTN97Cyd2KRzz/24XrjuXsQ+XA1YUQgvNyKPK8WpSull7rxVmjLnHE5Y2xcGXExcXGgNzjUCCxn923EctoUIGRQwsRwCeCD7nV3xS586qJ3brjWDSJqZ9QA04NaQ3XtQTvaLuOic/9VdX0EEGEFUFckli75ybMZdSnf2t9NOgcJOAogtj/vZmJHcLMSJpIKeW0niVqE+xgzm5VkTrFOaNyjpQcnbZ473HekWKkcsq8i9iOo0nG/VrhxL5yfB9iSGi7wF9FmZRz+zJxZAJYelmvSFou3Mnk0BVGrgQFoha5pDLsBmOakCqAOHwPqhWhAAAgAElEQVSf/u41flLCfcuwX8JwiBhmxX3QDcKQy/tqUHccu+w/qH2o0ZVsu6zOP6UcQRXFL/vR2aoS3wVYdLBomexHm8zh9O37VK2wq4ad6kinIy4KEwFrE04t9/vGUAu4SmjbFiEiZtkS0QTUtt5//X25Se76U9vUT96Ve/grWdGcUi5p88Uof5hy7n+KGqmYxzdNi6srFOEULY034t4OX5jC/o5gu55IR5jWaDGgF3MrgtizwlAWCsjyoZEOjrgSMUYSR1xUnO/dt5yXNl8wIJeptODzzq/0YimxDFHedjs0yf72be/gXa9/EzutcUw9dQKJSiXkVn+Lhum0pmkaJPizdnDoIzB3F0e1DDsqOnSInG5U167OdUWk+v1tnme/rQse1WyAk1Kirmti2+KcyxEsBO2yWbmvc9VrG7usAUs50thaojOF4HHBZzvLEnFUJ7mKdnHAY5/wBMKxKQ9//ONhWmeSFTxUdSaRoYLKC3Wd9Y+hVLNryubgqlkfKZJFib1GMgFa0rihvGmLgC8XM5O4ZaRySSIZ5JTLFTYgWUkF+xVxiJoZrpFJY9vm3xcLSMmIHZw+gLbFmo7uzJw7P/9F3vFHb4N5y9RlPWycN+zMZjhTrOmogsMjaMyFVpXPhuqpi6gqVVXla9obxlv2FY2a1vqgr90ndjgdPExBO8ta1eQOV/4C55VG/kqkm9eOVyKH3vvSF11JClVV0bW5Ted0usMdi33ms4o7a+NJ3/ltPPjpTxGuvzrfP06WF8BkEM0ekucyXPRG5nkbPWzvBIzUccTljpEkjrioOKT12SL+Ods2aTBRDosFrC8+yU9QSjyhS6AeTu1z6s/fZ5N5xxte+VpmrqI7fZo9qZgA1kaqYfqxn0gHE+3dicJs4u6SxB62MUmtzuX8jHqSFd2Vy638XLHdQfOErDFRVVUmMSWKo1IiOSWt65wj2eB4ScCMyvn8mQTPvO2IXmBa0ZKrrhvLLd/wDsHn7iUKKoo5jznDxNFqh59MedijHs50Z5czi30mkxnV7ozJsV1uuPF+XH3iGmLqaJsOHDjncU6YXHVCcveZkEmCDSKLvau6lcKnFKFNlub7NPsNcTHns5/4DE4Tzek5+yfvpDtoaeYHHJzaZ3//NA5PSl32q/SCS0YljqBgTcOO1HT7+xyf7kBSgjhUI+BIqcOFfI1QW15TKCSvXNtek7e8B0SW97ae5zJrM23c37/Rr3uCXvxq5cNYFqqYLYtYYowkMbSu6UzpUHwVWCTlAV99E/s7FU/7kRfm1PIsAOXaKnk88B61UkBlhR2qZrN6VlFE6NPLR32fRpI44vLGSBJHXFyc6/br2/MNHzq0j0FBhLlVBcJgw5SKVrGPFLVdjmBFhTtO8fd//l574ytfzYnomS46dqMwUwexO6Txu2fTbYcvwHqERg49t/7q0j2l39o2JjM5miyKsSR/5mRJUnp/PBFhEioODg7wzlGVoh6VnP5LbUcVPCll4+Wm65juzGjb3NVDdBX5UQWpAo1G1AmRXDq8PHNziLNVpK/8NBXwuTBBxIgGLgjB1zRdiznBh5D5f9fgXKCqfNZQaresUBbxiAcxl7vsFM1jJmOpdJ5RXDGpBodYTkVWTkrPayVIwJE1bGhEBhHKnkgszZ1JiAqVd1lX6Byp6ZhMaixmct710W/y9RcyOdR+XPaZgJvm530hjUdpBLcE2jful/X7aHjOR0XIj9IpfiXQ3z8pJUSyjrjruuxAsDflduL/3969/kpy3Ocd/1ZVd8+cyy531xR1pUhbEWlZYiJLgkRZigMngYMgQRwg/2DeOUiiVwrsXKw4gOgEhuBIQUwrhClKsiyRISlyubvcMzNdl7yo6st097ktl4yW5/kA5OzOmZ6ZM2fO9jO/qvoV7zaOcFDzxX/wOzz7L/6Z4bDOwX9VlbkNKVeEyR8ekxma0qeuRZbpfu7kOYptxHZbhMJSCVHkSlBIlP+/HiAk9l9avK8hJHbt5cZ5sW1zK5W+ehJ9fogAtInX/usL6XqA//SH3+JoGzj0hlXIrUUs8aHtVTuceIfvbPnku7+DxN4Juww15+djsdOACH0QWrznRNkv2hJCrhjuQh7SrIztT8zG5MpgGPdpNCXYlPY2gbxVXN+LLuYTsU15CLuyeRFBNNCtqO6eQ64AZ11Y6oYWu2HVVd3k7dPK17rq0nBbg3MGayu83xGSwVUGkiX1C23214sbYv8cu59pHP172N13/ikMzy+E0Fe1UijVp5jK8GQkpkSyeRWuj3k/4Oh9Psbmof2ur2XXtD3PFUz945oyV7ONAazB2bwwo+t92IV7O3p/LAXFpZDYr24u/UPHC7GWKuSXDYndh5yLDkeftSgmv8654Xw0EGvHSWhxhyuuP/kxvvRPfx+eedqwcnDzOK+obxyebmqhKQXjPB+2G0I25Pdl/gDj+4ot4w+bIQ4r88uLk4Y/ilwJConyq+FCKfAS93HOsf3c/W5+GQZiaaETIuzg9ndeSP/tX/87nkgV9f2WtbXsYihz+Dy+DNXm+XypD1AhBNZN3urMxOFJjU/iyVACcNe02PSVnPHK6dRfzoe1+qpPqWSlUafw6W4e/TGTOWcXXXhz2u0uukXh9PiL9tkbL8A4+3mOmwVd5vK9mT6/0+Z+vtewdN7jX8Z8b+O08DUz+vqZz4C9ezBxLyR2m9b1K8JLC5tuakMKeUtO1zW4Dh7rHB6TF6sAvo1UTc291PLxzz3DnTrxlT/4J/Bbfyf3N6xGT6VMHEzligcOc+eVZEWuCK1ull8ND+Mf4UvcRx8Q83QwbAXe5e7LVWUheu7eWHH7RoO/13LruOFk11JFi00BEjR1za4s8Dg6OOTdk/tYa1mtVtx/934/l6+zfEIf126ypZP+9FbDTbpKYdy7gZ3cx1I4PO2xlpx2uwc9/qJDln2F79zHiQ94+d5Mn9/iz+6S3+uDPP5l7D+f/Jel/Y07F/kg0L0/p826Y2nj44ztt5vsFkfFGHF1TbvdEkrT7Lqu2cWAaWrubje4uoHDil/6HfH6GrdO3DtyvH3NcfOoypW+U35f3hOFQhFAlUS5ykpIxAwrPEkRE0Oem7bZgGvAJ974zgvpO//mW9xoLYctpG3LgbWsreXk/n2Mtdi6yj0brcWZXC1xDPtujIfxTtvmb3aSTsvVkHHl5qwQctYCmIc/v1IeNWdtNzltyA3z98xe1dvsv39NgsY6kg+MzzO74GlWKzbtLq9OB2xyVGUB2i54ds4SDxvuErmddvyjf/UHfPKbXzXcKAtRXB7SN1o4IvK+UkiUq6sLiZT1Ll1z5QT9dh4G2G7ANLBLbL77vfRHf/jvSduWFbDaeVwI1NZhUp4DtdtsOagbUohUo1PseXO9Fq9LdqgCLn4TQ0XsrH6J0+fQH62QKMV8GHph55ZyOV3tP76u082ztIl+p5RkTR80PRFPIlqXFz4Fn/eSPjjgdnvC7/z+P8Yfr/j0N75q+OitPO7lLMlSNjNMVHu1ehF52BQS5eoavfWHljllQtN2B7UlppBXOUbA+5wmdz630Pjpz9LPX/wr/uyP/gt209LsIsfRsWoTayDuPLUZ5mxFM3nQbtFByntGj3XzEcOkUOJiPmG7biHmaPXy+GS5dPLu5zeaYTjwV63diXywXBy2uOvec2Nn7eqyVFFcWiiDy4tCfMwLiJxzbH1Ls16xi4G2ctxvDDee+gTh+povPP9VHv/tv2s4Osi9L9c1WEuIeccda+2w1aESosj7SiFRrrQUSq80GFrn7ImksvuKcy5/vVuJmhLsImxboOLku3+e/s8L3+P1l1+hOvEcYnBxaI0yetSybKJ7rLzCdhzYUhkC70/eJdSNQ6IFwqTFzVkVxGigZIJ+6YaC4tWV33NDSIT5Cunpe/e8BUhLlcfu/esx2MqBtbl9katoG8utX/8UX/6X/xye+6yhAarRJxhrwdjRrieWFMseywutrkTk4dLCFbmyEhBKi4sKwJh+p4UIEAIrW2GMxblcbQwmtzHJnYh35G1aGthGNtcPuH3ouL2yHFARfWQdcmPlHO5iqRjms5qjOxmXNj+jk10cnfvcaFjckE/EyZRdIZaG+EbGQ9t9EHjQF0w+fMw8BO59efTnHPjSZO5hWb08+vCR79b271UTE8ZZaiwRy73NhoPrx7zVnvBODebGintHhuM6QmPLrihlZ5vShiZnwfzkunm/ub/VQ3wtRGRGlUS5shLQkk8+dTnbBCD0NQuoJkuKA5BCouoqjrbs4pI7PoMHbt/lzg/+MjVt4tv/9ls0MVIHaGKkCeyFxsu0olkKgKctPFj8fhfmnKmKeLUtrXJfmtvaXRVsGvospiEkQql6D/eS32+lV2SqazbR42tLaBzP/+432a5qnvyH3zQ8cSN/EmoMyVhC3yLelH6FeW/sNE2sLBT+ReShUkiUK2sYwhq1xFkymqu1t4XZ+I4SEOPw564tTQRef52X/uJ/pcM28t1v/0cON5HjYFmFSOUTtIG6yjuchJB34Mjbt0WsG5pGU3oumq4ZtbWElIa2I6O9bbv7MsYstr+ZDgfKFWXLFnSlObj3vt/m0nuPpexyst3lpt7O4WPA1hWx9TS2Jrblt8KVYWFn8M5xQmC7suwayxee/yqb2vCbX/kizVOfMqwbqMu+3MaOehza/R6mHTP766jZqYi8XxQS5WpbevuPTz4Lk/lhISzOzmB0g9bE7Qbnmlxl3AE//Ov08xdf4r//5z/hKDls2+JCoiLhfMKV/X8tucF2TGFvGzFfGhMbZzHJ5MtuL9/S2LvbtSOltDfvcNywe9ysW66eZCCkHPhMTIRu8kOMmJhDo0k5LNZ13X842W63VKsGHyLJOGzlCBFCnd/e78aWdNDw3PNf4bNf/xo8fgOeuGXy6mST51kYSCliTJnx1L8H1dJG5FeJQqJcXaO5fpSFzdMvnxYGx0NrZ53WUozYbnOPRBmaTvkoH8G3vPP9/53+4k9f4PWXX+GatxzsEsfJ0b77LgdV01cEcRZPyPsVO5cD3y7SbedGCYahVIZMWVU6HVK+6C4p8uHnase23eGM7bdnBKhsrhTasj0gwNa3ebcha8Aa2spwxxruu0Rsaj7//Jd47utfw3zmKcP2Ptx8DEi5F2IIULuyDUuZS5gSGNd/JptW6zvdAqs9et+KfCAUEuXqmoTE6fy+OPpyKX7s3/6Uu52tj455dXRVVfSLVGLEGAsx5OAYHdy+y90//0E62uW9o4+tw+5C3n82JhIBX4aejTFE7zmwNXY0TG2tLYuvc7Ph6e/30rxEubp8zMPL1lpC66mqOm8lWYafQwhEa2h9xKxrgoEdkegMn3v+y3z8G8/DZ540pACPHeZPIHX+VJScARyp29Ul5AUnuepd9kouAXRv+sPoz93X3PiKcqVGm0Xef1rdLFeXOf2vpda3V9VI7E+U3zv8nMBVVVU5qeWTY8KSUsy93ijLlWv4v2vD9ZXj1SPDwS5wtHYcJLAnLWvrWLuG6AMulrlfJGxlwTqiD/gYhvbCIeVKEPv9EfvvyeT1AgqLV9eqXuG9J6WETYbkE846fPBsoyeY/D7jcM291HJCgIOGrYm8fbzi2iEcrwIpJIwNUDflF8Ng8tIsQohUrsKUuY5dByljzGyqhiGPSOcuA4NUru9uo7esyAdDlUS50k6rRixMMZzf/hK/OrG07djtdjRNM1yf42LZgsKDcXnu4kkLv3wrpVffwKwP+dn/+B7/84U/g03gqKogptLDLkKMVMnkRQaYPJ8xQti1/VxGGIJiGH1DColXW/eBBdtNZ8jDwyfbLc3xMdsUOCFwn8Tzv/e7PP3bz+Vq4dNPGY5XsHJ5hb+1QKkMRpPf1908WiDEMq92Uq63S4nPDBXFcTVxvFhsfL2IvH8UEkUm9gPiOV0FF85W+Sq7XyUZDa2lMmewO5EaRhWVmEYNDUP+c7sDW4EPbF/+cXrz579gc+ceP/vrl3nrRz8hvXuf2jjWxsGmpY5wYCpcSLOdXBLDLi7J5PO7XF2tj6yvXePubsPGJU4q2K0svnE8+5Uv8eXf+/t4ItVHHs87oDQOUgRX9clt13qapimzNxJptKNy8JHK2b1PV92vQrpIm8NT53TEvjIvIu8fhUSRkXkF8XIhcWirk09eMQSczcNsxFJxMfuHhpConFm8vxh9rsCEstVKMODbfJL2Cd58G958K3Fvw53XXuOHP3iRV3/8Y8zW0yRDnQwu5oqhjcMigG6HF0Nc7JV3nov0dexfInN6tXJpu7fOr9qimu77GF92zlocBPPvZbpTyYM8j/Nuk6cW2DxkbPNbJ9hhRXswFmMrtgSe/XvP8blvfI3oEvZjvwaPHRtuXMuTAbs2NSanu0Ted9lgsORV+AloY36vJyDGRG3Lh59Irg7GfDe9U+b1zir1S2V9hUSRD4RCosh7cUolcZZv3uv4WCLvjpFsPtt214WU/55KhccDd+/A7TvJ37vPqz/9G/7kP/wxKw/rNrH2kXU01LtYQmTKWxOavOrUGIP3Pu/4Ygx17djtdpiYyraEaWir4yw+5nY7hEhd18TSnieUKmkiN1OurMutVaC/vXMOYprN/ey/5dFrdVpgvOjLetFFRmfd/ryQ2P11usUi5GkFeeeQREi5TZFzjlh6EXbtimJ55KGl0TBs27c1MhB9KD0xLW3bsm7WeO+Hr1vHjkhbOXaV4cRAu3asHr/BZz7/mzz17DPEynLjN37DcLjOPQu7crbrVh/n3U7y9Xb2Hp++fu/LAuQzU6SIvN8UEkUeYf3Jelx1SeSh6tJ8m00Ld0/ANPDKz9LJyz/hT7/9xxxEi2tbajOsPI2tp6nq3FzZOXYnG5zLDZW7VdrWlZ0xQuhXq4YQcnWxrL5OtiuVuRyIuubg3cqfkFd7p7Bcqb1IQNz7/i/4Wk2nwF0mJC7eblTVG4dE2A+K3S4lEPvm1cYY3GTLkKEx+nAfxllSiLk6mBIxlkBenmUbAq6u8DHfdkdkYyJPPvNZnv78b3H8hWfhiV8zNBYOG2jqvkIYYsTVw/rF7nmJiIBWN4t8KCQDpNQtLM17SicDOw+HK4gtbFs2q8hrq8gvr1e4bcvhusaFgAuGKiSaqiFGqNpE2G45aFaEEAhtBGuIxuJLPEkWiLkS5pwj+EBdVX3IaGPAmpxa8zrX3PbHGEOKiWgiwb23SZHnDfOOdVvJjUPntO3RNPCdd5/jNNndbxiPgMb9yqCJidpabGlP5H3om1gbY/rQ3a2CX1U179y7y7XDI7a+papqbJUXQCXA15a0XnEv7Ih1xcZF4tGK+y7irlmaxwyfPoL1OkENHNZ5zDcGsA43WtikgoGITKmSKPIICylX7uwo1uRhy9w30RmbezKmlMd0Y+kvsvVwsoU2cucvX0z33rnDaz/9KQem4cXvf591qjBtSxVywKtsjSMRMBiTGwSF0NIYh40BV6qQFWZvh5euMtVtGdiFoO7vye0vDZrOMBuHwIvM3zsz0E0s7X19Xug8q8bWzfXrq4EGKutyM/Q0DLMbwGHwKZJql6utXcBMsWy1aPOD24rN7oTV4REheRKWk90JBwdHtCnSOnjmueeIleWjv/5pbj3zGXj8psFGuHkdKpPnwXbzCsfffwjYyu39bPrvJe2HWxG5mhQSRT4MJmOo3QKa7v99+5BQBl0jkEIe17Q2V7wMOeXECG++zfbt2+mNn7/K2hjuv3WXn7z0En/7o78hbTesq3UOhyliQ8L6yEHdkO5vWVcN8WTLulkNLUtSKJfDloE+hlydHD31LqZM95uG/cA2HjbuKoRj54W57rhpSOyPv8BCm+mh05AIZZg5Rmxy/VB718cykGgdtJic3S3EyuIteGvZ4jlJkaOb1/ji15/Hriuidbh1xfXHbnLr4x+Fxx4zXDuG3RbWq/ICmtJUMOXX1weMcxhr8weE2eIRDTGLyDKFRJFH2fjXd/S7nAy50bbJiybMqMOcGS0aySEtkJIBIs4Mu8IQU15BbQz4XAXDk/9+fwtvv5145x3e/sWr0AZ+9MOXsD7yt6+8gouWtN3SWEfY7miM46Cu8BsPvuWgOSAxmje54KyqXhcSu1B2mWFnGIaGLxsSz7vvcdPyYGDnI/XBimBgG3MlMFVl1bGF1iRuPvFRPvaJj3Pt1g2aw0OObz3Gwac+UeYR1mBCDoCO3Fgw+aEFzXibu1gWmtAtpJ9UDiN5oZA13RL8xT0lNS9RRDoKiSKPsBhjHmpeOqmXq9rgsS5PP84tj1NuWxLKMLQzDPvLlMtxqS60efW0KRP6UgBb59vFlOe3mVKrDCEf++btHFrffDvdefUN0t0T3P0Nv/irV3jtlR9jdwnbtqyMG4amJ9/CrG3Mwvc/LAjJZhXFS/7zNn0VlwLh+PmGhZc9lHYz3sKtJz/JE08/yfojN/AHNesnHuf4kx+Bmzdz30FrhkftVxHH4dLYPJ/U1TkcGpefVCAfW7ojubKbife+393HB4/B4Up7Je8jVTUOkZPVyaNzgUKiiIAWrog80kzZq7nrXjJu2k3Mc81qV/XVxFQuvfdUJTjmiiJDd+OQwLkSVoCqprv7zPaHOePK4zsgEjy4quLetYoqJu7et7xZedwaDusDXj92/KIJNM6xOlrh2rg4bGtOudz73lluN3Ne0BsvYDmt3+NegdYMx01vExYqcV0/Qm8s79od1fWa9Y0V921kdRC5dWjNtWNLc2hJZUvG/VBmJ5d1+bl2/1ybUQXQ4hx5dblzuKoipIg1tv/5doHQudHuOzGvZh9XDRUMRWRKlUSRR1SCsjyF/W3LLtOw7r3ctj+m9G80cdIAOQ4tefrWPORqo0+5D581eTvCqobtDlLCv/5G2m23tNsdYdeSYiS2nu3Jhs1mw26zZbPZwLbl3uu/ZG0rQgjcvXuXu3fvsttsgTzcmucDjp6qyfP/XJkHmIeGDY/dvEEksV6vuX7rJjjL4bXjvG+xtayODji6fo36YI1zDls5qqqiaRrWjz9uMAZOThJ1ZVg1ue/g4WFf7et/OMb2Aa8P95Mx3/HLfNba79nPezwXcn6Veg6KyKUpJIo8orqQ2A0SP0hQPO+3vz9sesMu4ZjJdXtimTM3LeWV1RMpgKuIyWO7uZDJQvTkQY5Yynep67czXEaT7ye4XGVL5f5SCWVdaN36PEwbIP/P5RW/rsmXdHP86jysi83tg+ietwHKMG9F/nq3+sMmaFuoS6XVt5hqNQwX772AHXvmj+a8gDhZn4QbX/lAAf+MY0TkylNIFHlEDSuYsz4gdl9cMqk2ndelsF9tPL3fkp+CGfLiqQG1P6Qb2hwqjymZsvI5hz+LG1Umy1iwGYVA49ibOxlHoZGQh2TNKGzaKh/H6PjSwqe/ny7UpVH4i5Q5gdX+HMFxSBxts5JKE3HKsDuhDPtPFo+MX8zL/Mt7Wpabhsb+SrP8k50HVG1rJyKnU0gUeYSdWkQ669f6EkFx0i1l9qCBc0LieenmtApn17vP2H5CYiLNtsXbm0c3WemdUsptX5YfoN8dxli7P5dzcrv8PE9bBj3sMmOMuXjyGwXFUd1x+uXhRuPL0Y2S2T9+OG7+U00LX3EKiSJyBoVEkUfZw/r1nVS3loLjXqXyIo+9kJn640vrmy7odQsputW5/d2nNLS4MWa2kCXEsjVgafIzawLd3Tim/b2Wp9vhleO6cNpvI1hWDfdiCZMp5f/KzildGI1lX+1p+5mZtP/6XCjkL72YZj/od486/fv0MBGRi1BIFHlUXWYhwgOMbV7o7s+pWEZSHrkdLdE4bbEFDKtu93s7zh83nVLhOy8AXWpq3mWqgl3o63aYgX4l+eL9D6tWxn+8+ONP5oROb7oUEhcDvhKjiJxBIVHkUTY62S8NHc+HIacmR6RTKmAL7V+GLy3UHZPtQ1MykEZ3sL+1cRxasIxbfo8qgimlvpK49LzO6u83rSwOI7dlOLsLceVx9u66OybtVyHHbWMsJu9kXSqV48fvh8tZev3PmzN4diXy3LmnsxuecluFRBE5g0KiyKNsUpE6bZi4M6vIlSOG0HHOMOmscjU5fno/C3PvlubgdVW32ZDxOf88LW0pd9aOIUsjtku3H1cyu+c2fv5mUulcfF5nVEIvExKnr9veHMTJQqLZ5XkUEkXkDAqJIh8iD6VYdMH2OQ9yt5fJLxe6sw/QQ33+7+FxFx//QUOiiMgZFBJFREREZEb9D0RERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRERERGYUEkVERERkRiFRRFd555sAAAAhSURBVERERGYUEkVERERkRiFRRERERGYUEkVERERk5v8B6V2D+l2C5IsAAAAASUVORK5CYII=';
  var imageBuffer2 = new Buffer(imageBase64.replace('data:image/png;base64,','') || '', 'base64');
  doc.image(imageBuffer2, 100, 130, {width: 70});

  doc.font('BiauKai');
  doc.fontSize(20);
  doc.text(data.strTitle1a+"\n"+data.strTitle1b, 183, 150, { width: 40, align: 'left'});

  doc.fontSize(32);
  doc.text(data.strTitle2, 240, 120, { width: 40, align: 'left', characterSpacing:5});

  // doc.text(data.strTitle3, 180, 330);
    // data.strTitle3 = "社員(信徒)名冊";
  doc.text("社員", 180, 330);
  doc.text("信徒", 264, 328);
  doc.text("名冊", 348, 328);

  doc.font('Times-Roman');
  doc.text("(", 248, 332);
  doc.text(")", 332, 332);

  doc.font('BiauKai');
  Api.dateTW(doc, 130, 670, 26, datetime);

  var date_x = 170;
  var date_y = 710;
  doc.text(data.strPhone1, date_x, date_y);
  doc.font('Times-Roman');
  doc.text(data.strPhone2, date_x+70, date_y+4);
}
Api.memberPage3 = function(doc, arrObj, datetime, inputVar){ // 信徒名冊
  var nowMember = 0;
  var nowPage = 1;
  var totalPage = Math.ceil(arrObj.length/23);

  // 整個頁面的loop
  var table_x = 25;
  var table_y = 65;
  var table_width = 540;
  var table_height = 720;
  var cell_height = 30;
  var col_width = [35, 27, 58, 25, 60, 165, 36, 64, 70];
  var col_title = ["市府\n編號", "班別", "姓名", "性別", "出生年月日", "住址", "蓮社\n編號", "電話", "身分證字號"];
  var col_title_y_shift  = [ 5, 10, 10, 10,  10, 10, 5,  10,  10];
  var col_title_fontsize = [10, 10, 10, 10, 10, 10, 10, 10, 10];
  var col_data_y_shift   = [11,  8,  8,  8,  8,  8, 11, 11, 11];
  var footer_width_shift = table_x+200;
  var footer_height_shift = table_y+table_height+25;

  var title_right_text_shift = 410;

  while(nowMember < arrObj.length){
    doc.font('BiauKai').fontSize(14).text("財團法人", 40, 25);
    // doc.fontSize(18).text("桃園佛教蓮社社員(信徒)名冊", 120, 25);

    doc.font('BiauKai').fontSize(18);
    doc.fontSize(18).text("桃園佛教蓮社社員", 120, 25);

    doc.font('Times-Roman').fontSize(18);
    doc.fontSize(18).text("(         )", 265, 27);

    doc.font('BiauKai');
    doc.fontSize(18).text("信徒", 273, 25);
    doc.fontSize(18).text("名冊", 320, 25);

    doc.fontSize(10).text("府民字第        冊", title_right_text_shift, 15);
    doc.fontSize(10).text("造冊人："+inputVar.member_manager, title_right_text_shift, 30);
    Api.dateTW(doc, title_right_text_shift, 45, 10, datetime);

    doc.lineJoin('miter').rect(table_x, table_y, table_width, cell_height).fillAndStroke("#CCC", "#000");
    doc.lineJoin('miter').rect(table_x, table_y, table_width, table_height).stroke();

    var left_shift = table_x;
    var top_shift = table_y;


    for(var i=0; i<col_width.length; i++){
      doc.moveTo(left_shift, table_y).lineTo(left_shift, table_y+table_height).stroke();
      doc.fontSize(col_title_fontsize[i]).fillColor("#000");
      doc.text(col_title[i], left_shift, table_y+col_title_y_shift[i], { width: col_width[i], align: 'center'});
      left_shift += col_width[i];
    }

    for(var i=0; i<23; i++){
      top_shift += cell_height;
      left_shift = table_x;

      doc.moveTo(table_x, top_shift).lineTo(table_x+table_width, top_shift).stroke();

      if(!!arrObj[nowMember]){
        //"縣府\n編號"
        doc.font('Times-Roman').fontSize(14);
        doc.text(nowMember+1, left_shift, top_shift+col_data_y_shift[0], { width: col_width[0], align: 'center'});
        left_shift += col_width[0];

        // "班別"
        doc.font('BiauKai').fontSize(12);
        if(arrObj[nowMember].templeClass_text)
          doc.text(arrObj[nowMember].templeClass_text.charAt(0), left_shift, top_shift+col_data_y_shift[1], { width: col_width[1], align: 'center'});
        left_shift += col_width[1];

        // "姓名"
        doc.font('BiauKai').fontSize(12);
        var name = arrObj[nowMember].name.trim();
        if(name.indexOf("(") != -1){
          name = name.substr(0, name.indexOf("(")).trim();
        }
        doc.text(name, left_shift, top_shift+col_data_y_shift[2], { width: col_width[2], align: 'center'});
        left_shift += col_width[2];

        // "性別"
        doc.font('BiauKai').fontSize(12);
        if(!!arrObj[nowMember].sexual_text)
          doc.text(arrObj[nowMember].sexual_text, left_shift, top_shift+col_data_y_shift[3], { width: col_width[3], align: 'center'});
        left_shift += col_width[3];

        // "出生年月日"
        var tmp = "";
      /*  if(!!arrObj[nowMember].bornYear && !!arrObj[nowMember].bornMonth && !!arrObj[nowMember].bornDay){
          doc.text("民", left_shift+3, top_shift+col_data_y_shift[4], { lineBreak: false, continued: "yes", width: col_width[4], align: 'left'});
          tmp = arrObj[nowMember].bornYear+"."+arrObj[nowMember].bornMonth+"."+arrObj[nowMember].bornDay;
        }*/
        if(!!arrObj[nowMember].birthday){
          doc.text("民", left_shift+3, top_shift+col_data_y_shift[4], { lineBreak: false, continued: "yes", width: col_width[4], align: 'left'});
          tmp = arrObj[nowMember].birthday;
        }
        doc.font('Times-Roman').fontSize(12).text(tmp, doc.x, doc.y+2);
        // doc.text(tmp, left_shift, top_shift+col_data_y_shift[4], { width: col_width[4], align: 'left'});
        left_shift += col_width[4];

        // 地址
        /*doc.font('BiauKai').fontSize(12);
        var addr = arrObj[nowMember].addr, topshift = top_shift+col_data_y_shift[5];
        if(addr.length > 13){
          addr = addr.splice(13, 0, "\n");
          topshift -= 5;
        }*/
        function mix_str(doc, str, left_shift, topshift, colwidth){
          var shift = 0;
          var break_char = 0;
          for (var x = 0; x < str.length; x++){
              var c = str.charAt(x);
              if(isNaN(c)){ // 字串
                shift += 12;
              }
              else{ // 數字
                shift += 8;
              }
              if(shift >= colwidth){
                break_char = x;
                topshift -= 5.8;
                break;
              }
          }

          var shift = 0;
          for (var x = 0; x < str.length; x++){
              var c = str.charAt(x);

              if(x != 0 && x == break_char){
                topshift += 12.6;
                shift = 0;
                if(!isNaN(c)) shift = 1;
              }

              if(isNaN(c)){ // 字串
                doc.font('BiauKai').fontSize(12);
                doc.text(c, left_shift+3+shift, topshift+1, { lineBreak: true, width: 30, align: 'left'});
                shift += 12;
              }
              else{ // 數字
                doc.font('Times-Roman').fontSize(12);
                doc.text(c, left_shift+3+shift, topshift+3.1, { lineBreak: true, width: 30, align: 'left'});
                shift += 6;
              }
          }
        }
        var addr = arrObj[nowMember].addr, topshift = top_shift+col_data_y_shift[5];
        // doc.text(addr, left_shift+3, topshift, { width: col_width[5], align: 'left'});
        mix_str(doc, addr, left_shift, topshift, col_width[5]-2);
        // mix_str(doc, addr, left_shift, topshift, 150);
        left_shift += col_width[5];

        // "蓮社\n編號"
        doc.font('Times-Roman').fontSize(12);
        if(!!arrObj[nowMember].memberId)
          doc.text(arrObj[nowMember].memberId, left_shift, top_shift+col_data_y_shift[6], { width: col_width[6], align: 'center'});
        left_shift += col_width[6];

        // "電話"
        doc.font('Times-Roman').fontSize(12);
        doc.text(arrObj[nowMember].telephone, left_shift, top_shift+col_data_y_shift[7], { width: col_width[7], align: 'center'});
        left_shift += col_width[7];

        // "身分證字號"
        doc.font('Times-Roman').fontSize(12);
        if(!!arrObj[nowMember].identity)
          doc.text(arrObj[nowMember].identity, left_shift+3, top_shift+col_data_y_shift[8], { width: col_width[8], align: 'left'});
        // left_shift += col_width[8];
      }
      nowMember++;
    }

    doc.fontSize(12);
    doc.font('BiauKai').text("第", footer_width_shift, footer_height_shift);
    doc.font('Times-Roman').text(nowPage, footer_width_shift+15, footer_height_shift+1, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁，共", footer_width_shift+43, footer_height_shift);
    doc.font('Times-Roman').text(totalPage, footer_width_shift+83, footer_height_shift+1, { width: 25, align: 'center'});
    doc.font('BiauKai').text("頁", footer_width_shift+113, footer_height_shift);

    nowPage++;

    if(nowMember < arrObj.length){
      doc.addPage();
      // nowMember+=23;
    }
    else{
      break;
    }
  }
}
Api.memberPage4 = function(doc, arrObj, row_num, isBound, isPaddingMid){  // 地址
  function mix_str(doc, str, left_shift, top_shift){
    var shift = 0;
    var f_size = 13;

    var str2 = "";
    var cut_len = 16;
    if(str.length > cut_len){
      str2 = str.substr(cut_len);
      str = str.substr(0, cut_len);
      // console.log(str2);
    }

    for (var x = 0; x < str.length; x++){
        var c = str.charAt(x);
        if(isNaN(c)){ // 字串
          doc.font('BiauKai').fontSize(f_size);
          doc.text(c, left_shift+30+shift, top_shift+3, { lineBreak: true, width: 30, align: 'left'});
          shift += f_size;
        }
        else{ // 數字
          doc.font('Times-Roman').fontSize(f_size);
          doc.text(c, left_shift+30+shift, top_shift+5, { lineBreak: true, width: 30, align: 'left'});
          shift += (f_size/2);
        }
    }
    if(!!str2){
      shift = 0;
      for (var x = 0; x < str2.length; x++){
        var c = str2.charAt(x);
        if(isNaN(c)){ // 字串
          doc.font('BiauKai').fontSize(f_size);
          doc.text(c, left_shift+30+shift, top_shift+3+f_size, { lineBreak: true, width: 30, align: 'left'});
          shift += f_size;
        }
        else{ // 數字
          doc.font('Times-Roman').fontSize(f_size);
          doc.text(c, left_shift+30+shift, top_shift+5+f_size, { lineBreak: true, width: 30, align: 'left'});
          shift += (f_size/2);
        }
      }
    }
  }
  var nowMember = 0;

  // 整個頁面的loop
  var table_x = 25;
/*  var table_y = 5;
  if(isBound){
    table_y = 40;
  }
*/
  // console.log(row_num);
  var table_y = 5; // row = 10的
  if(row_num == 7){
    table_y = 48;
  }

  var cell_height = (a4pageHeight-(table_y*2))/row_num; //35;

  while(nowMember < arrObj.length){
    var left_shift = table_x;
    var top_shift = table_y+12;
    var sumColWidth = a4pageWidthHalf - table_x; //_.reduce(col_width, function(sum, el) {return sum + el }, 0);

    for(var i=0; i<row_num; i++){
      for(var j=0; j<2; j++){ // 每一行
        var tmpMember;
        if(j==0){ // 靠左邊的
          tmpMember = nowMember;
          left_shift = table_x;
        }
        else if(j==1){ // 靠右邊的
          tmpMember = nowMember+row_num;
          left_shift = sumColWidth+75; //+85
        }
        if(!!arrObj[tmpMember] && arrObj[tmpMember].name && arrObj[tmpMember].addr){

          doc.font('Times-Roman').fontSize(13);
          // 郵遞區號
          doc.text(arrObj[tmpMember].post5code, left_shift, top_shift+5, { width: 40, align: 'left'});

          var addr = arrObj[tmpMember].addr.trim();
          mix_str(doc, addr, left_shift+3, top_shift);

          // 姓名
          var fontsize = 16;
          // if(row_num == 7){
          //   fontsize = 16;
          // }
          doc.fontSize(fontsize);

          var str = arrObj[tmpMember].name.trim();
          var x = left_shift+25 + (str.length+1)*fontsize;
          //console.log(str + " " + str.length);
          doc.font('BiauKai').text(str, left_shift+25, top_shift+33, {lineBreak: false, continued: "yes"});
          doc.font('Times-Roman').text(" ", doc.x, doc.y, {lineBreak: false, continued: "yes"});
          // doc.font('BiauKai').text("大德", x, doc.y+1);
          doc.font('BiauKai').text("大德", x, doc.y);

          if(!!arrObj[tmpMember].memberId){
            // 編號
            doc.font('Times-Roman').fontSize(10);
            var num = arrObj[tmpMember].memberId;
            if(arrObj[tmpMember].templeLevel_text == "社員"){
              num = "1"+ funcPad(num, 6);
            }
            else if(arrObj[tmpMember].templeLevel_text == "護法"){
              num = "2"+ funcPad(num, 6);
            }
            else{
              num = "";
            }
            if(str.length > 5){
              doc.text(num, left_shift+160, doc.y +18); // +18
            }
            else{
              doc.text(num, left_shift+160, doc.y +13); // +18
            }
          }
        }
      }
      top_shift += cell_height;
      var offset_y = 10;
      if(i == Math.ceil(row_num/2)){
        top_shift += offset_y;
      }
      nowMember++;
    }

    if(nowMember+row_num < arrObj.length){
      doc.addPage();
      nowMember+=row_num;
    }
    else{
      break;
    }
  }
}
Api.memberPage5 = function(doc, arrObj){
  var left_shift = 100;
  var top_shift = 10;

  // doc.font('BiauKai');
  // doc.fontSize(20);
  // doc.text("測試", 10, 10);

  // if(0)
  for(var i in arrObj){
    if(!!arrObj[i] && arrObj[i].name && arrObj[i].addr){
      // 郵遞區號
      doc.font('Times-Roman').fontSize(26);
      doc.text(arrObj[i].post5code.substr(0,1), left_shift+90 , top_shift+40, { width: 40, align: 'left'});
      doc.text(arrObj[i].post5code.substr(1,1), left_shift+110, top_shift+40, { width: 40, align: 'left'});
      doc.text(arrObj[i].post5code.substr(2,1), left_shift+130, top_shift+40, { width: 40, align: 'left'});

      doc.text(arrObj[i].post5code.substr(3,1), left_shift+155, top_shift+40, { width: 40, align: 'left'});
      doc.text(arrObj[i].post5code.substr(4,1), left_shift+175, top_shift+40, { width: 40, align: 'left'});

      // 地址
      doc.font('BiauKai').fontSize(16);
      var addr = arrObj[i].addr.trim();
      doc.text(addr.toStraight(), left_shift+140, top_shift+100, { lineBreak: true, width: 60, align: 'left'});

      // 姓名
      doc.font('BiauKai').fontSize(36);
      var name = arrObj[i].name.trim()+ " 大德";
      doc.text(name.toStraight(), left_shift+25, top_shift+150, {lineBreak: true, width: 60, align: 'left'});

      // 編號
      doc.font('Times-Roman').fontSize(12);
      var num = arrObj[i].memberId;
      if(arrObj[i].templeLevel_text == "社員"){
        num = "1"+ funcPad(num, 6);
      }
      else if(arrObj[i].templeLevel_text == "護法"){
        num = "2"+ funcPad(num, 6);
      }
      else{
        num = "";
      }

      doc.rotate(-90, {origin: [100, 400]}).text(num, left_shift+70, top_shift+480);
    }
    if(i < arrObj.length-1){
      doc.addPage();
    }
  }
}
Api.addRoute('serverpeople', {authRequired: false}, {
  get: function () {
    // console.log("in serverpeople");

    var countVar = Number(this.queryParams.count) || 99;
    var startVar = Number(this.queryParams.start) || 0;

    var filterVar = this.queryParams.filter || {};
    var sortVar = this.queryParams.sort || {};
    // console.log(this.queryParams);

    // console.log("countVar: "+countVar);
    // console.log("startVar: "+startVar);
    // console.log(filterVar);
    // console.log(sortVar);

    function peopleFindOrg(arr){
      for (var i in arr) {
        if (arr[i] === null || arr[i] === undefined|| arr[i] === "") {
        // test[i] === undefined is probably not very useful here
          delete arr[i];
        }
        else if (i == "name" || i == "identity" || i == "memberId" || i == "longLive" || i == "lotus" || i == "telephone" || i == "cellphone" || i == "email" ){
          var o = {};
          o['regex'] = arr[i];
          arr[i] = o;
        }
      }
      return arr;
    }

    filterVar = peopleFindOrg(filterVar);
    sortVar = Api.chgSortObj(sortVar);

    // console.log(filterVar);
    filterVar['isLive'] = 1;

    return {
      data: People.find(filterVar, {skip: startVar, limit: countVar, sort: sortVar}).fetch(),
      pos:  startVar,
      total_count:People.find(filterVar).count()
    }
  }
});
Api.addRoute('pdfpraying2', {authRequired: false}, {
  get: function () {
    var datetime = {};
    datetime.d = new Date();
    datetime.yyy = datetime.d.getFullYear()-1911;
    datetime.mm = datetime.d.getMonth()+1;
    datetime.dd = datetime.d.getDate();

    // console.log("Rest pdfpraying2 GET");
    var queryVar, sortVar = {}, pageVar = {};
    queryVar = this.queryParams || {};

    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);

    if(!!queryVar.year){
      queryVar.year = Number(queryVar.year);
    }

    if(!sortVar.year) sortVar.year = 1;

    // var order = queryVar.order || "1";
    // if(order == "1"){

    // }
    // sortVar.pray2_orderid = 1;
    sortVar.prayserial = 1;
    // sortVar.prayingtype_orderid = 1;
  /*  else{
      sortVar.pray2_orderid = 1;
      sortVar.prayingtype_orderid = 1;
      sortVar.prayserial = 1;
    }*/
    // delete queryVar.order;

    // console.log("queryVar:");
    // console.log(queryVar);
    // console.log("sortVar:");
    // console.log(sortVar);
    // console.log("pageVar:");
    // console.log(pageVar);

    var nowform = queryVar.nowform;
    delete queryVar.nowform;

    if(!!queryVar.praystarttime || !!queryVar.prayendtime){
      queryVar.createdAt = {};
    }

    if(!!queryVar.praystarttime){
      queryVar.createdAt['$gt'] =  new Date(queryVar.praystarttime);
      delete queryVar.praystarttime;
    }
    if(!!queryVar.prayendtime){
      queryVar.createdAt['$lt'] = new Date(queryVar.prayendtime);
      delete queryVar.prayendtime;
    }

    var tempArr = [];
    // 預先過濾
    if(nowform == "11"){ // 消災牌位
      tempArr = PrayingType.find({ print_type: "1"}, {sort: {order: 1}}).fetch();
    }
    else if(nowform == "12"){ // 拔渡牌位
      tempArr = PrayingType.find({ print_type: "2"}, {sort: {order: 1}}).fetch();
    }
    else if(nowform == "2"){ // 油燈牌
      tempArr = PrayingType.find({ print_type: "3"}).fetch();
    }
    else if(nowform == "4"){ // 功德名條
      tempArr = PrayingType.find({$or:[{ print_type: "4"}, { print_type: "5"}]}, {sort: {order: 1}}).fetch();
    }
    else if(nowform == "5"){ // 普施圓桌
      tempArr = PrayingType.find({ print_type: "5"}).fetch();
    }

    if(tempArr.length){ // 依需要，撈出所有要印的東西
      queryVar.$and = [];
      var tmp_type = [];
      for(var i=0; i< tempArr.length; i++){
        tmp_type.push({ type: tempArr[i]._id });
        // console.log(tempArr[i]);
      }
      queryVar.$and.push({'$or': tmp_type});
    }

    // console.log("queryVar:");
    // console.log(queryVar);


    // console.log(arrObj.length);

    var arrObj = [];
    // 沒有選法會項目 就是所有的法會項目
    if(!queryVar.prayitem){
      var p2 = Pray2.find({class_id: queryVar.prayname}, {sort:{order_id:1}}).fetch();
      var temArr = [];
      var temArr2 = [];

      for (var ii = 0; ii < p2.length; ii++) {

        queryVar.prayitem = p2[ii]._id;
        arrObj = Praying2.find(queryVar, {
            sort: sortVar,
            skip: pageVar.skip,
            limit: pageVar.limit
        }).fetch();
        // console.log(p2[ii].value+ " "+arrObj.length);
        // console.log(queryVar.prayitem);
        // console.log(queryVar);

        if(nowform == "3"){ // 是疏文的時候
          // 把所有各個祖先的 都分開印
          var temArr = [];
          for (var i = 0; i < arrObj.length; i++) {
            if(!!arrObj[i].livename_text && !arrObj[i].passname_text && !arrObj[i].passname_text1 && !arrObj[i].passname_text2 && !arrObj[i].passname_text3 && !arrObj[i].passname_text4){
              temArr.push(arrObj[i]);
            }
          }
          for (var i = 0; i < arrObj.length; i++) {
            if(!!arrObj[i].passname_text){
              temArr.push(arrObj[i]);
            }
          }
          for (var i = 0; i < arrObj.length; i++) {
            if(!!arrObj[i].passname_text1){
              temArr.push(arrObj[i]);
            }
          }
          for (var i = 0; i < arrObj.length; i++) {
            if(!!arrObj[i].passname_text2){
              temArr.push(arrObj[i]);
            }
          }
          for (var i = 0; i < arrObj.length; i++) {
            if(!!arrObj[i].passname_text3){
              temArr.push(arrObj[i]);
            }
          }
          for (var i = 0; i < arrObj.length; i++) {
            if(!!arrObj[i].passname_text4){
              temArr.push(arrObj[i]);
            }
          }
          temArr2 = temArr2.concat(temArr);
        }
        else{ // 不是疏文的時候
          temArr2 = temArr2.concat(arrObj);
        }

      }
      arrObj = temArr2;
    }
    else{ // 有選特定的法會項目
      arrObj = Praying2.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
      }).fetch();
      // console.log(arrObj.length);
        // console.log(arrObj);
      // console.log('nowform = '+ nowform);

      if(nowform == "3"){ // 是疏文的時候
        // console.log('nowform != 3');
        var temArr = [];
        for (var i = 0; i < arrObj.length; i++) {
          if(!!arrObj[i].livename_text && !arrObj[i].passname_text && !arrObj[i].passname_text1 && !arrObj[i].passname_text2 && !arrObj[i].passname_text3 && !arrObj[i].passname_text4){
            temArr.push(arrObj[i]);
          }
        }
        for (var i = 0; i < arrObj.length; i++) {
          if(!!arrObj[i].passname_text){
            temArr.push(arrObj[i]);
          }
        }
        for (var i = 0; i < arrObj.length; i++) {
          if(!!arrObj[i].passname_text1){
            temArr.push(arrObj[i]);
          }
        }
        for (var i = 0; i < arrObj.length; i++) {
          if(!!arrObj[i].passname_text2){
            temArr.push(arrObj[i]);
          }
        }
        for (var i = 0; i < arrObj.length; i++) {
          if(!!arrObj[i].passname_text3){
            temArr.push(arrObj[i]);
          }
        }
        for (var i = 0; i < arrObj.length; i++) {
          if(!!arrObj[i].passname_text4){
            temArr.push(arrObj[i]);
          }
        }
        // tempArr = tempArr.concat(arrObj);
        arrObj = temArr;

      }
      else{ // 不是疏文的時候
        // console.log('nowform == 3');
        // temArr = temArr.concat(arrObj);
      }

    }


    if(nowform == "11" || nowform == "12" || nowform == "13" || nowform == "14"){  // 牌位
      var doc = new PDFDocument({size: 'A4', margin: 1});
      doc.registerFont('BiauKai', BiauKaiTTF);
      doc.registerFont('msjh', msjhTTF);

      var data = "";
      var left = right = center = top = "", noMorePage=0;

      // 讀每一個資料，並傳送到製作頁面的地方

      var arr_small_data1 = []; // 小牌冤親的資料
      var arr_small_data2 = []; // 小牌其他的資料

      for(var i = 0; i< arrObj.length; i++){
        var entry =  arrObj[i];
        if(!!entry.type_text && !!entry.prayitem_text && !!entry.prayserial){

          left = right = center = "";
          top1 = entry.prayitem_text;
          top2 = entry.prayserial;

          // console.log(entry);

          // 小牌開始
          if(!!entry.type_text && entry.type_text.indexOf("小牌")!= -1){ // 拔渡有人的時候 這個要分成每一個人一個牌位

            if(!!entry.passname_text2){ // 拔渡有人的時候 這個要分成每一個人一個牌位
              if(nowform == 14) continue; // 如果選了冤親，是其他的就跳掉
              var dataArr2 = entry.livename_text.split(",");
              center = entry.passname_text2;
              if(!!entry.addr) right = entry.addr;

              for (var j = 0;  j < dataArr2.length ; j++) { // 投每一個陽上去生牌位
                left = dataArr2[j];

                if(i == arrObj.length -1 && j == dataArr2.length -1){
                  noMorePage = 1;
                }
                // Api.prayPage1(doc, top1, top2, left, center, right, noMorePage, entry);
                var obj = {
                  top1: top1,
                  top2: top2,
                  left: left,
                  center: center,
                  right: right,
                  noMorePage: noMorePage,
                  entry: entry,
                }
                arr_small_data1.push(obj);
              };
            }
            else{
              if(nowform == 13) continue; // 如果選了其他，是冤親的就跳掉
              if(!!entry.type_text && (entry.type_text.indexOf("拔") >= 0 || entry.type_text.indexOf("亡") >= 0) ){
                var passname = "";

                if(entry.passname_text) passname = entry.passname_text;
                else if(entry.passname_text1) passname = entry.passname_text1;
                // else if(entry.passname_text2) passname = entry.passname_text2;
                else if(entry.passname_text3) passname = entry.passname_text3;
                else if(entry.passname_text4) passname = entry.passname_text4;

                left = entry.livename_text;
                // center = entry.passname_text;
                center = passname;
              }
              else{
                left = "";
                center = entry.livename_text;
              }
              if(!!entry.addr) right = entry.addr;

              if(i == arrObj.length -1){
                noMorePage = 1;
              }
              // Api.prayPage1(doc, top1, top2, left, center, right, noMorePage, entry);
              var obj = {
                top1: top1,
                top2: top2,
                left: left,
                center: center,
                right: right,
                noMorePage: noMorePage,
                entry: entry,
              }
              arr_small_data2.push(obj);
            }

          } // 小牌結束
       /*   else if(!!entry.type_text && !!entry.passname_text2){ // 拔渡有人的時候 這個要分成每一個人一個牌位
            var dataArr2 = entry.livename_text.split(",");
            center = entry.passname_text2;
            if(!!entry.addr) right = entry.addr;

            for (var j = 0;  j < dataArr2.length ; j++) { // 投每一個陽上去生牌位
              left = dataArr2[j];

              if(i == arrObj.length -1 && j == dataArr2.length -1){
                noMorePage = 1;
              }
              Api.prayPage1(doc, top1, top2, left, center, right, noMorePage, entry);
            };
          }*/
          else{ // 一般的，一筆一個牌位
            if(!!entry.type_text && (entry.type_text.indexOf("拔") >= 0 || entry.type_text.indexOf("亡") >= 0)){
              var passname = "";

              if(entry.passname_text) passname = entry.passname_text;
              else if(entry.passname_text1) passname = entry.passname_text1;
              else if(entry.passname_text2) passname = entry.passname_text2;
              else if(entry.passname_text3) passname = entry.passname_text3;
              else if(entry.passname_text4) passname = entry.passname_text4;

              left = entry.livename_text;
              // center = entry.passname_text;
              center = passname;
            }
            else{
              left = "";
              center = entry.livename_text;
            }
            if(!!entry.addr) right = entry.addr;

            if(i == arrObj.length -1){
              noMorePage = 1;
            }
            Api.prayPage1(doc, top1, top2, left, center, right, noMorePage, entry);
          }
        }
      };

      // console.log("arr_small_data1: "+ arr_small_data1.length);
      // console.log("arr_small_data2: "+ arr_small_data2.length);
      // console.log(arr_small_data);
      if(arr_small_data1.length || arr_small_data2.length){
        var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
        doc.registerFont('BiauKai', BiauKaiTTF);
        doc.registerFont('msjh', msjhTTF);

        if(arr_small_data1.length > 0){
          for(var i in arr_small_data1){
            var item = arr_small_data1[i];
            if( i>3 && i%4==0 && i<arr_small_data1.length )
              doc.addPage();
            Api.prayPage1a(doc, i%4, item.top1, item.top2, item.left, item.center, item.right, 0, item);
          }
        }
        if(arr_small_data2.length > 0){
          if(arr_small_data1.length > 0)
            doc.addPage();
          for(var i in arr_small_data2){
            var item = arr_small_data2[i];
            if( i>3 && i%4==0 && i<arr_small_data2.length )
              doc.addPage();
            Api.prayPage1a(doc, i%4, item.top1, item.top2, item.left, item.center, item.right, 0, item);
          }
        }
      }
    }
    else if(nowform == "2"){ // 油燈牌
      var doc;

      var isAddPage = 0;
      var nowPage = 0;
      var tempArr = PrayingType.find({'print_type': "3"}, {sort: {order: 1}}).fetch();  // 要印的油燈牌

      // console.log(queryVar.type);
      var tt = queryVar.type;
      for(var i=0; i< tempArr.length; i++){ // 每一種要印的油燈牌
        if(typeof tt == "undefined"){
        }
        else{
          if(queryVar.type != tempArr[i]._id){
            continue;
          }
        }

        var printType = tempArr[i];
        // 尋找目前有登記此類的項目
        var queryVar2 = queryVar;
        // console.log(queryVar2);

        delete queryVar2['$and'];
        queryVar2.type = tempArr[i]._id;

        var arrObj2 = Praying2.find(queryVar2, {sort: sortVar}).fetch();
        if(arrObj2.length > 0){

          if(isAddPage){ // 每一項 都有自已的頁面，這邊第二項時 才會進來
            if(printType.oilpray_layout == 1){ // 直的
              doc.addPage({size: printType.oilpray_paper, margin: 1 });
            }
            else{
              doc.addPage({size: printType.oilpray_paper, margin: 1, layout: "landscape"});
            }
            isAddPage--;
            nowPage++;
          }

          if(nowPage == 0){
            if(printType.oilpray_layout == 1){ // 直的
              doc = new PDFDocument({size: printType.oilpray_paper, margin: 1 });
            }
            else{
              doc = new PDFDocument({size: printType.oilpray_paper, margin: 1, layout: "landscape"});
            }
            doc.registerFont('BiauKai', BiauKaiTTF);
            doc.registerFont('msjh', msjhTTF);
            nowPage++;
          }

          Api.prayPage2(doc, printType, arrObj2);
          isAddPage++;
        }
      }
    }
    else if(nowform == "3"){ // 疏文
      var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
      doc.registerFont('BiauKai', BiauKaiTTF);


      Api.prayPage3(doc, arrObj, datetime);
    }
    else if(nowform == "4"){ // 功德名條
      var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
      doc.registerFont('BiauKai', BiauKaiTTF);

      Api.prayPage4(doc, arrObj);
    }
    else if(nowform == "5"){ // 普施圓桌
      var doc = new PDFDocument({size: 'A4', margin: 1});
      doc.registerFont('BiauKai', BiauKaiTTF);

      Api.prayPage5(doc, arrObj);
    }


    return Api.returnPDF(doc);
  },
});
Api.addRoute('pdflighting2', {authRequired: false}, {
  get: function () {
    var datetime = {};

    // console.log("Rest pdflighting2 GET");
    var queryVar, sortVar = {}, pageVar = {};
    queryVar = this.queryParams || {};

    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);

    if(!!queryVar.year){
      queryVar.year = Number(queryVar.year);
    }

    var nowform = queryVar.nowform;
    delete queryVar.nowform;

    var type = queryVar.type;
    delete queryVar.type;

    if(!!queryVar.lightstarttime || !!queryVar.lightendtime){
      queryVar.createdAt = {};
    }

    if(!!queryVar.lightstarttime){
      queryVar.createdAt['$gt'] =  new Date(queryVar.lightstarttime);
      delete queryVar.lightstarttime;
    }
    if(!!queryVar.lightendtime){
      queryVar.createdAt['$lt'] = new Date(queryVar.lightendtime);
      delete queryVar.lightendtime;
    }

    // console.log("pdflighting2");
    // console.log(queryVar);
    // console.log(queryVar.printdate);
    // console.log(queryVar.printdate2);
    var printdate = queryVar.printdate2 || queryVar.printdate;
    delete queryVar.printdate;
    delete queryVar.printdate2;

    // console.log(printdate);
    if(!!printdate){
      var tmp_d = new Date(printdate);
      datetime.yyy = tmp_d.getFullYear()-1911;
      datetime.mm  = tmp_d.getMonth()+1;
      datetime.dd  = tmp_d.getDate();
    }
    else{
      datetime.d = new Date();
      datetime.yyy = datetime.d.getFullYear()-1911;
      datetime.mm = datetime.d.getMonth()+1;
      datetime.dd = datetime.d.getDate();
    }
    // console.log(datetime);

    if(type == "1"){ // 光明燈
      queryVar.p1 = "-1";
      sortVar = {year: 1, p1_num: 1,  p3_num: 1, p5_num: 1, p7_num: 1 };
    }
    else if(type == "3"){ // 光明燈
      queryVar.p3 = "-1";
      sortVar = {year: 1, p3_num: 1, p1_num: 1, p5_num: 1, p7_num: 1 };
    }
    else if(type == "5"){ // 光明燈
      queryVar.p5 = "-1";
      sortVar = {year: 1, p5_num: 1, p1_num: 1, p3_num: 1, p7_num: 1 };
    }
    else if(type == "7"){ // 光明燈
      queryVar.p7 = "-1";
      sortVar = {year: 1, p7_num: 1, p1_num: 1, p3_num: 1,  p5_num: 1 };
    }
    else{
      queryVar = {
        $and: [
          {$or: [
            {p1: "-1"},
            {p3: "-1"},
            {p5: "-1"},
            {p7: "-1"},
          ]},
          queryVar,
        ]
      };
    }


    if(isEmpty(sortVar))
      sortVar = {year: 1, p1_num: 1,  p3_num: 1,  p5_num: 1,  p7_num: 1 };

    // console.log("queryVar:");
    // console.log(queryVar);
    // console.log("sortVar:");
    // console.log(sortVar);
    // console.log("pageVar:");
    // console.log(pageVar);

    var arrObj = Lighting2.find(queryVar, {
        sort: sortVar,
        // skip: pageVar.skip,
        // limit: pageVar.limit
    }).fetch();

    if(nowform == "1" ){  // 標籤
      var doc = new PDFDocument({size: 'A4', margin: 1});
      doc.registerFont('BiauKai', BiauKaiTTF);

      if(typeof arrObj == "object")
        Api.lightPage1(doc, arrObj, type);
    }
    else if(nowform == "3"){ // 簡易疏文
      var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
      doc.registerFont('BiauKai', BiauKaiTTF);

      if(typeof arrObj == "object")
        Api.lightPage2(doc, arrObj, datetime, type);
    }
    else if(nowform == "5"){ // 詳細疏文
      var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
      doc.registerFont('BiauKai', BiauKaiTTF);

      if(typeof arrObj == "object")
        Api.lightPage3(doc, arrObj, datetime, type);
    }

    return Api.returnPDF(doc);
  },
});
Api.addRoute('pdfpeople', {authRequired: false}, {
  get: function () {
    // console.log("Rest pdfpeople GET");
    var queryVar, sortVar = {}, pageVar = {};
    queryVar = this.queryParams || {};
    // console.log(queryVar);

    var datetime = {};
    datetime.d = new Date();
    if(queryVar.printdate){
      datetime.d = new Date(queryVar.printdate);
    }
    delete queryVar.printdate;
    delete queryVar.printdate_submit;
    var my_print = queryVar.my_print;
    delete queryVar.my_print;

    var time = "";
    if(!!queryVar.time){
      time = queryVar.time;
      delete queryVar.time;
    }

    var mypage = 0;
    if(!!queryVar.mypage){
      mypage = queryVar.mypage;
      delete queryVar.mypage;
    }

    datetime.yyy = datetime.d.getFullYear()-1911;
    datetime.mm = datetime.d.getMonth()+1;
    datetime.dd = datetime.d.getDate();

    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);

    // 要增加
    sortVar = {memberId: 1};

    if(!!queryVar.isLive){
      queryVar.isLive = Number(queryVar.isLive);
    }

    if(!!queryVar.sexual){
      queryVar.sexual = Number(queryVar.sexual);
    }

    if(!!queryVar.mailThis && queryVar.mailThis=="-1"){
      queryVar.mailThis = "-1";
    }
    else{ // if(queryVar.mailThis == "false"){
      delete queryVar.mailThis;
    }

    var nowform = Number(queryVar.nowform);
    delete queryVar.nowform;

    var isBound = 1;
    if(typeof queryVar.isBound == "undefined" || queryVar.isBound != "on"){
      isBound = 0;
    }
    delete queryVar.isBound;
    // console.log("isBound");
    // console.log(isBound);

    var inputVar = {};
    inputVar.member_manager = queryVar.bossname;
    delete queryVar.bossname;

    // Job.removeJobs(myJobs, "pdfmember4");
    // Job.removeJobs(myJobs, "pdfmember5");

    ///////////// 封面
    if(nowform == 2){
      var data = {};

      data.strTitle1a = "財團";
      data.strTitle1b = "法人";
      data.strTitle2 = "桃園佛教蓮社";
      // data.strTitle3 = "社員(信徒)名冊";
      data.strDate = "中華民國 "+datetime.yyy+"年 "+datetime.mm+"月 "+datetime.dd+"日";
      data.strPhone1 = "電話:";
      data.strPhone2 = "(03) 339-7021-1";

      var doc = new PDFDocument({size: 'A4', margin: 1});
      doc.registerFont('BiauKai', BiauKaiTTF);

      Api.memberPage2(doc, data, datetime);
      return Api.returnPDF(doc);
    }

    ///////////////// 內容

    var uniType = Number(queryVar.uniType);
    delete queryVar.uniType;

  /*  if(queryVar.templeLevel == 2){
      delete queryVar.templeLevel;
      queryVar.mailThis = "-1";
    }*/

    // console.log(queryVar);
    // var arrObj = People.find(queryVar, {sort: sortVar}).fetch();


    // -1: 全選，-2: 全不選
    var tc = "";
    if(queryVar.templeClass == "-1" || queryVar.templeClass == "-2"){
      tc = queryVar.templeClass;
      delete queryVar.templeClass;
    }

    // -1: 全選，-2: 全不選
    var tl = "";
    if(queryVar.templeLevel == "-1" || queryVar.templeLevel == "-2"){
      tl = queryVar.templeLevel;
      delete queryVar.templeLevel;
    }

    // console.log("tc: "+ tc);
    // console.log("tl: "+ tl);

    // 如果是全部的話，就社員和護法都不選
    if(my_print == "1"){
      tc = "-1";
      tl = "-1";
    }
    else{
      // -1: 全選，-2: 全不選
      if(tc == "-1"){
        queryVar = {
          $and: [
            {templeClass: { $ne: "" }},
            {templeClass: { $ne: null }},
            queryVar,
          ]
        };
      }
      else if(tc == "-2"){
        queryVar = {
          $and: [
            {$or: [
              {templeClass: ""},
              {templeClass: null}
            ]},
            queryVar,
          ]
        };
      }

      // -1: 全選，-2: 全不選
      if(tl == "-1"){
        queryVar = {
          $and: [
            {templeLevel: { $ne: "" }},
            {templeLevel: { $ne: null }},
            queryVar,
          ]
        };
      }
      else if(tl == "-2"){
        queryVar = {
          $and: [
            {$or: [
              {templeLevel: ""},
              {templeLevel: null}
            ]},
            queryVar,
          ]
        };
      }

  }
    // 要陽上的
    queryVar = {$and:[{isLive: 1}, queryVar]};

    var arrObj;
    if(uniType == 1){ // 地址唯一
      // 先找出所有應該顯示的
      arrObj = People.find(queryVar, {
        sort: sortVar,
      }).fetch();
      arrObj = _.uniq(arrObj, false, function(d) {return d.addr});

    }
    else if(uniType == 2){ // 姓名 地址唯一
      // 先找出所有應該顯示的
      arrObj = People.find(queryVar, {
        sort: sortVar,
      }).fetch();
      arrObj = _.uniq(arrObj, false, function(d) {return d.name+d.addr});
    }
    else{
      arrObj = People.find(queryVar, {sort: sortVar}).fetch();
    }

    // console.log("queryVar");
    // console.log(queryVar);

    ////////////////////////
    if(nowform == 1){ // 簽到名冊
      var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
      doc.registerFont('BiauKai', BiauKaiTTF);

      Api.memberPage1(doc, arrObj, datetime);
    }
    else if(nowform == 3){ // 社員名冊
      var doc = new PDFDocument({size: 'A4', margin: 1});
      doc.registerFont('BiauKai', BiauKaiTTF);

      Api.memberPage3(doc, arrObj, datetime, inputVar);
    }
    else if(nowform == 41 || nowform == 42 || nowform == 43){ //郵寄標籤
      var len = arrObj.length;
      var size = 7000;
      var start = 0, end = size;
      // console.log("len: " + len);

      var row = 0;
      var isPaddingMid = 0;

      if(nowform == 41) row = 7;
      else if(nowform == 42) row = 10;
      else if(nowform == 43){ // 中空
        row = 7;
        isPaddingMid = 1;
      }

      isBound = 0; // MIC 1050902 強制拿掉這個

      if(len <= size){ // 一般情況
        var doc = new PDFDocument({size: 'A4', margin: 1});
        doc.registerFont('BiauKai', BiauKaiTTF);
        Api.memberPage4(doc, arrObj, row, isBound, isPaddingMid);

        return Api.returnPDF(doc);
      }
      else{// 如果是要分開pdf的話

        var arr_obj = [];

        // 準備列印陣列
        var i = 0;
        while (end <= len+size) {
          var e = (end >= len) ? len : end; // end超過長度的話 就用len的
          // console.log(i+ " start: " + start + " end: " + e + " len: " + len);
          //arr_obj.push(arrObj.slice(start, e));

          if(mypage == i){
            arr_obj = arrObj.slice(start, e);
            break;
          }

          start = start + size + 1;
          end += size;
          i++;
        };

        // return Api.returnPDF(doc);
        var doc = new PDFDocument({size: 'A4', margin: 1});
        doc.registerFont('BiauKai', BiauKaiTTF);
        Api.memberPage4(doc, arr_obj, row, isBound, isPaddingMid);
        return Api.returnPDF(doc);


       /* /// 生成PDF
        var arr_files = [];
        for(var i in arr_obj){
          var arr = arr_obj[i];
          var filename = process.cwd() + '/../web.browser/app/'+i+'.pdf';
          console.log("i: " + i + " filename: "+filename);

          var prms = {
            filename: filename,
            time: time,
            oid: i,
            file: i+".pdf",
            arrObj: arr,
            row: row
          }
          // var job = new Job(myJobs, "pdfmember4", prms);
          // job.save();
          console.log('job save: '+i );
        }
        return {
          headers: { 'Content-Type': 'text/html; charset=utf-8' },
          body: "請點選上方頁數，謝謝"
        };*/

      }

    }
    else if(nowform == 5){ //信封標籤

      var len = arrObj.length;
      var size = 700;
      var start = 0, end = size;

      if(len <= size){ // 一般情況
        var doc = new PDFDocument({size: 'A4', margin: 1});
        doc.registerFont('BiauKai', BiauKaiTTF);

        Api.memberPage5(doc, arrObj);

        return Api.returnPDF(doc);
      }
      else{ // 如果是要分開pdf的話
        var arr_obj = [];

        // 準備列印陣列
        var i = 0;
        while (end <= len+size) {
          var e = (end >= len) ? len : end; // end超過長度的話 就用len的
          // console.log(i+ " start: " + start + " end: " + e + " len: " + len);
          // arr_obj.push(arrObj.slice(start, e));

          if(mypage == i){
            arr_obj = arrObj.slice(start, e);
            break;
          }

          start = start + size + 1;
          end += size;
          i++;
        };

        var doc = new PDFDocument({size: 'A4', margin: 1});
        doc.registerFont('BiauKai', BiauKaiTTF);
        Api.memberPage5(doc, arr_obj);
        return Api.returnPDF(doc);

        /// 生成PDF
       /* var arr_files = [];
        for(var i in arr_obj){
          var arr = arr_obj[i];
          var filename = process.cwd() + '/../web.browser/app/'+i+'.pdf';
          console.log("i: " + i + " filename: "+filename);

          var prms = {
            filename: filename,
            time: time,
            oid: i,
            file: i+".pdf",
            arrObj: arr
          }
          // var job = new Job(myJobs, "pdfmember5", prms);
          // job.save();
          console.log('job save: '+i );
        }*/

        /// 合併PDF
        // var pdfMerge = new PDFMerge(arr_files);
        // pdfMerge.merge(function(error, result) {
        // //Handle your error / result here
        //   console.log("merge done");

        //   return {
        //     headers: { 'Content-Type': 'application/pdf' },
        //     body: result
        //   };;
        // });


        return {
          headers: { 'Content-Type': 'text/html; charset=utf-8' },
          body: "請點選上方頁數，謝謝"
        };
        // return {
        //   headers: { 'Content-Type': 'application/pdf' },
        //   body: doc.outputSync()
        // };
      }
    }

    return Api.returnPDF(doc);
  }
});
Api.addRoute('pdffamily', {authRequired: false},{
  get: function () {
    var queryVar, sortVar = {}, pageVar = {};
    queryVar = this.queryParams || {};
    // console.log(this.request);
    // console.log(this.queryParams);
    // console.log(this.urlParams);

    if(!queryVar.familyId || People.find({familyId: queryVar.familyId}).count()==0){
      return "無資料";
    }

    var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
    doc.registerFont('BiauKai', BiauKaiTTF);


    // doc.lineWidth(1);
    // doc.lineJoin('miter').rect(25, 25, 550, 350).stroke();

    doc.font('BiauKai');
    doc.fontSize(20);
    doc.text("財團法人桃園佛教蓮社 家庭成員一覽", 240, 30);
    // doc.fontSize(20);

    // var pray1 = "";
    // if(Pray1.findOne(queryVar.prayname)){
    //   pray1 = Pray1.findOne(queryVar.prayname).value;
    // }
    // doc.text("傳授護國 "+ pray1 +" 繳款單", 180, 70);
    // doc.fontSize(16);
    // doc.text("中華民國  年  月  日", 360, 98);
    // doc.font('BiauKai').text("感謝狀", 100, 100);

    var fontsize = 12;
    var fontsize2 = fontsize+4;
    doc.fontSize(12);

    var x = 20;
    var y = doc.y;

    var pp = People.findOne({familyId: queryVar.familyId, isLive: 1, mainPerson: -1});
    doc.text("戶長："+pp.name, x, y+5);

    y = doc.y+fontsize2*2;
    // doc.text("", x, y);
    // y = doc.y+fontsize2;

    var p1 = People.find({familyId: queryVar.familyId, isLive: 1}).fetch();

    var arrP1field = [
      'name', '', 'post5code', 'addr',
      'templeClass_text', 'templeLevel_text', 'memberId'];
    var arrP1text = [
      '陽上', '農曆生日', '區號', '地址',
      '班別', '身份', '社員編號'];
    var arrP1width = [
      100, 110, 40, 365,
      50, 50, 50];

    var x1 = x;
    // doc.font('Times-Roman');
    for (var i in arrP1text){
      var str = arrP1text[i];
      doc.text(str, x1, y);
      x1 += arrP1width[i];
    }

    y = doc.y+fontsize2;
    for (var i = 0; i < p1.length; i++) {
      var entry = p1[i];
      var xx = x;
      for(var j in arrP1field){
        var str = entry[arrP1field[j]] || "";

        if(j == 1){
          var str = "";
          if(!!entry.bornYear) str = entry.bornYear + "年";
          if(!!entry.bornMonth) str = str + entry.bornMonth + "月";
          if(!!entry.bornDay) str = str + entry.bornDay + "日";
          if(!!entry.bornTime_text) str = str + entry.bornTime_text + "時";
        }

        if(!isNaN(str)){
          str = str.toString();
          // console.log(str);
        }

        Api.strChtEngMix(doc, str, xx, y+i*fontsize2, fontsize);

        xx += arrP1width[j];
      }
    }




    arrP1field = ['name', '', '', 'lifeYear', 'addr'];
    arrP1text = ['拔渡', '農曆生日', '農曆卒日', '享壽', '地址'];
    arrP1width = [120, 125, 125, 60, 360];

    var p0 = People.find({familyId: queryVar.familyId, isLive: 0}).fetch();
    doc.font('BiauKai');

    var x1 = x;
    y = doc.y+fontsize2*2;
    for (var i in arrP1text){
      var str = arrP1text[i];
      doc.text(str, x1, y);
      x1 += arrP1width[i];
    }
    // doc.text("拔渡", x, y);
    y = doc.y+fontsize2;

    for (var i = 0; i < p0.length; i++) {
      var entry = p0[i];
      // console.log(entry);
      var xx = x;
      for(var j in arrP1field){
        var str = entry[arrP1field[j]] || "";

        if(j == 1){
          var str = "";
          if(!!entry.bornYear) str = entry.bornYear + "年";
          if(!!entry.bornMonth) str = str + entry.bornMonth + "月";
          if(!!entry.bornDay) str = str + entry.bornDay + "日";
          if(!!entry.bornTime_text) str = str + entry.bornTime_text + "時";
        }
        else if(j == 2){
          var str = "";
          if(!!entry.passYear) str =            entry.passYear + "年";
          if(!!entry.passMonth) str = str +     entry.passMonth + "月";
          if(!!entry.passDay) str = str +       entry.passDay + "日";
          if(!!entry.passTime_text) str = str + entry.passTime_text + "時";
        }

        Api.strChtEngMix(doc, str, xx, y+i*fontsize2, fontsize);

        xx += arrP1width[j];
      }
    }


    return Api.returnPDF(doc);
  }
});
Api.addRoute('ping2update', {authRequired: false},{
  get: function () {
    // console.log(this.request);
    // console.log(this.queryParams);
    // console.log(this.urlParams);

    queryVar = this.queryParams || {};
    // file = req.params.file;

    Praying2.find({}).forEach(function(u){

      if(!isNaN(u.pray2_orderid) && !isNaN(u.prayingtype_orderid)){
      }
      else{
        var obj2 = Pray2.findOne(u.prayitem);
        var obj3 = PrayingType.findOne(u.type);
        if(!!obj2 && !!obj3){
          Praying2.update(u, {$set:{
            "pray2_orderid" : Number(obj2.order_id),
            "prayingtype_orderid" : Number(obj3.order)
          }});
          console.log("Praying2 pray2_orderid: "+obj2.order_id+" prayingtype_orderid: "+ obj3.order + " "+ u.prayname_text+" "+u.prayitem_text);
        }
        // if(!!obj3){
        //   Praying2.update(u, {$set:{
        //     "prayingtype_orderid" : Number(obj3.order)
        //   }});
        //   console.log("Praying2 prayingtype_orderid: "+obj3.order+" " + u.prayname_text+" "+u.prayitem_text);
        // }
      }

    });
    return "OK";
  }
});
