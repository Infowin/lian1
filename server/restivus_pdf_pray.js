Api.addRoute('printpray', {authRequired: false}, {
  get: function () {
    var inputVar = this.queryParams.input || {};
    var datetime = {};
    datetime.d = new Date();
    if(!!inputVar.pray_date && inputVar.pray_date != "null"){
      datetime.d = new Date(inputVar['pray_date']);
    }
    datetime.yyy = datetime.d.getFullYear()-1911;
    datetime.mm = datetime.d.getMonth()+1;
    datetime.dd = datetime.d.getDate(); 

    var countVar = Number(this.queryParams.count) || 99;
    var startVar = Number(this.queryParams.start) || 0;
    var filterVar = this.queryParams.filter || {};
    var sortVar = this.queryParams.sort || {};
 
    function chgSortObj(arr){
      for (var i in arr) {
        if (arr[i] == "asc") arr[i] = 1;
        else if (arr[i] == "desc") arr[i] = -1;

        if(i == "pray"){
          arr['year'] = arr[i];
          arr['prayname_text'] = arr[i];
          arr['pray2_orderid'] = arr[i];
          arr['prayingtype_orderid'] = arr[i];
          arr['prayserial'] = arr[i];
          
          delete arr[i];
        }
      }
      return arr;
    }

    var tempArr = [];
    // 預先過濾
    if(Number(this.queryParams.nowform) == 1){
      tempArr = PrayingType.find({$or:[{ print_type: "1"}, { print_type: "2"}]}, {sort: {order: 1}}).fetch();
    }
    else if(Number(this.queryParams.nowform) == 2){ // 油燈牌
      tempArr = PrayingType.find({ print_type: "3"}).fetch();
    }
    else if(Number(this.queryParams.nowform) == 4){ // 功德名條
      tempArr = PrayingType.find({$or:[{ print_type: "4"}, { print_type: "5"}]}, {sort: {order: 1}}).fetch();
    }
    else if(Number(this.queryParams.nowform) == 5){ // 普施圓桌
      tempArr = PrayingType.find({ print_type: "5"}).fetch();
    }
    
    if(tempArr.length){ // 依需要，撈出所有要印的東西
      filterVar.$and = [];
      var tmp_type = [];
      for(var i=0; i< tempArr.length; i++){
        tmp_type.push({ type: tempArr[i]._id });
      }
      filterVar.$and.push({'$or': tmp_type});
    }

    // isPrint: 0
    // filterVar.
    filterVar = Api.clearFilter(filterVar);
    sortVar = chgSortObj(sortVar);

    // console.log(filterVar);
    // console.log(tmp_type);
    // console.log(sortVar);

    if(this.queryParams.show == "table"){
      var arrObj = Praying2.find(filterVar, {skip: startVar, limit: countVar, sort: sortVar}).fetch();
      return {
        data: arrObj,
        pos:  startVar,
        total_count:Praying2.find(filterVar).count()
      }
    }
    else{
      var arrObj = Praying2.find(filterVar, {sort: sortVar}).fetch();
      
      if(Number(this.queryParams.nowform) == 1){  // 牌位
        var doc = new PDFDocument({size: 'A4', margin: 1});
        doc.registerFont('BiauKai', BiauKaiTTF);
        doc.registerFont('msjh', msjhTTF);

        var data = "";
        var left = right = center = top = "", noMorePage=0;

        // 讀每一個資料，並傳送到製作頁面的地方
        for(var i = 0; i< arrObj.length; i++){
          var entry =  arrObj[i];
          if(!!entry.type_text && !!entry.prayitem_text && !!entry.prayserial && !!entry.livename_text){
            
            left = right = center = "";
            top1 = entry.prayitem_text;
            top2 = entry.prayserial;
            
            if(!!entry.type_text && entry.type_text == "拔 - 冤親債主"){ // 這個要分成每一個人一個牌位
              var dataArr2 = entry.livename_text.split(",");
              center = entry.passname_text;
              if(!!entry.addr) right = entry.addr;

              for (var j = 0;  j <dataArr2.length ; j++) {
                left = dataArr2[j];

                if(i == arrObj.length -1 && j == dataArr2.length -1){
                  noMorePage = 1;
                }
                Api.prayPage1(doc, top1, top2, left, center, right, noMorePage, entry); 
              };
            }
            else{ // 一般的，一筆一個牌位
              if(!!entry.type_text && entry.type_text.indexOf("拔") >= 0){
                left = entry.livename_text;
                center = entry.passname_text;
              }
              else{
                left = "";
                center = entry.livename_text;
              }
              if(!!entry.addr) right = entry.addr;
              
              if(i == arrObj.length -1){
                noMorePage = 1;
              }
              Api.prayPage1(doc, top1, top2, left, center, right, noMorePage, entry); 
            }
          }
        };
      }
      else if(Number(this.queryParams.nowform) == 2){ // 油燈牌
        var doc;

        var isAddPage = 0;
        var nowPage = 0;
        var tempArr = PrayingType.find({'print_type': "3"}, {sort: {order: 1}}).fetch();  // 要印的油燈牌
        
        // console.log(filterVar.type);
        var tt = filterVar.type;
        for(var i=0; i< tempArr.length; i++){ // 每一種要印的油燈牌
          if(typeof tt == "undefined"){
          }
          else{
            if(filterVar.type != tempArr[i]._id){
              continue;
            }
          }

          var printType = tempArr[i];
          // 尋找目前有登記此類的項目
          var filterVar2 = filterVar;
          // console.log(filterVar2);
          
          delete filterVar2['$and'];
          filterVar2.type = tempArr[i]._id;
          
          var arrObj2 = Praying2.find(filterVar2, {sort: sortVar}).fetch();
          if(arrObj2.length > 0){

            if(isAddPage){ // 每一項 都有自已的頁面，這邊第二項時 才會進來
              if(printType.oilpray_layout == 1){ // 直的
                doc.addPage({size: printType.oilpray_paper, margin: 1 });
              }
              else{
                doc.addPage({size: printType.oilpray_paper, margin: 1, layout: "landscape"});
              }
              isAddPage--;
              nowPage++;
            }

            if(nowPage == 0){
              if(printType.oilpray_layout == 1){ // 直的
                doc = new PDFDocument({size: printType.oilpray_paper, margin: 1 });
              }
              else{
                doc = new PDFDocument({size: printType.oilpray_paper, margin: 1, layout: "landscape"});
              }
              doc.registerFont('BiauKai', BiauKaiTTF);
              doc.registerFont('msjh', msjhTTF);
              nowPage++;
            }

            Api.prayPage2(doc, printType, arrObj2);
            isAddPage++;
          } 
        }
      }
      else if(Number(this.queryParams.nowform) == 3){ // 疏文
        var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
        doc.registerFont('BiauKai', BiauKaiTTF);
        
        Api.prayPage3(doc, arrObj, datetime); 
      }
      else if(Number(this.queryParams.nowform) == 4){ // 功德名條
        var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
        doc.registerFont('BiauKai', BiauKaiTTF);
        
        Api.prayPage4(doc, arrObj); 
      }
      else if(Number(this.queryParams.nowform) == 5){ // 普施圓桌
        var doc = new PDFDocument({size: 'A4', margin: 1});
        doc.registerFont('BiauKai', BiauKaiTTF);

        Api.prayPage5(doc, arrObj); 
      }

      return Api.returnPDF(doc);
    }
  }
});