Api.addRoute('printlight', {authRequired: false}, {
  get: function () {
    var inputVar = this.queryParams.input || {};
    var datetime = {};
    datetime.d = new Date();
    if(!!inputVar.light_date && inputVar.light_date != "null"){
      datetime.d = new Date(inputVar['light_date']);
    }
    datetime.yyy = datetime.d.getFullYear()-1911;
    datetime.mm = datetime.d.getMonth()+1;
    datetime.dd = datetime.d.getDate(); 

    var countVar = Number(this.queryParams.count) || 99;
    var startVar = Number(this.queryParams.start) || 0;

    var filterVar = this.queryParams.filter || {};
    var sortVar = this.queryParams.sort || {};

    filterVar['year'] = Number(filterVar['year']);
    filterVar = Api.clearFilter(filterVar);

    // sortVar = Api.chgSortObj(sortVar);
    if(!!sortVar['light']){
      var a;
      if(sortVar['light']=="asc") a=1; else a=-1;
      sortVar['year'] = a;
      sortVar['type_text'] = a;
      sortVar['serial'] = a;
      delete sortVar['light'];
    }
    // console.log(sortVar); 

    if(this.queryParams.show == "table"){
      var arrObj = Lighting2.find(filterVar, {skip: startVar, limit: countVar, sort: sortVar}).fetch();
      return {
        data: arrObj,
        pos: startVar,
        total_count: Lighting2.find(filterVar).count()
      }
    }
    else{
      var arrObj = Lighting2.find(filterVar, {sort: sortVar}).fetch();

      if(Number(this.queryParams.nowform) == 1){  // 標籤
        var doc = new PDFDocument({size: 'A4', margin: 1});
        doc.registerFont('BiauKai', basePath+'print/BiauKai.ttf');
        
        Api.lightPage1(doc, arrObj, filterVar);
      }
      if(Number(this.queryParams.nowform) == 2){  // 簡易
        var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
        doc.registerFont('BiauKai', basePath+'print/BiauKai.ttf');
        doc.font('BiauKai');
        // doc.fontSize(30).text("簡易疏文 處理中…", a4pageWidthHalf-100, 100);
        
        Api.lightPage2(doc, arrObj, datetime);
      }
      if(Number(this.queryParams.nowform) == 3){  // 詳細
        var doc = new PDFDocument({size: 'A4', margin: 1, layout: "landscape"});
        doc.registerFont('BiauKai', basePath+'print/BiauKai.ttf');
     
        Api.lightPage3(doc, arrObj, datetime);
      }

      return Api.returnPDF(doc);   
    }
  }
});