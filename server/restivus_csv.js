
Api.toNewAddr = function(addr){
  function isArea(str){
    if(str.indexOf("鄉") != -1){
      return str.indexOf("鄉");
    }
    else if(str.indexOf("鎮") != -1 && str.indexOf("平鎮") == -1){
      return str.indexOf("鎮");
    }
    else if(str.indexOf("縣") != -1){
      return str.indexOf("縣");
    }
    else if(str.indexOf("市") != -1){
      return str.indexOf("市");
    }
    return -1;
  }

  function isCity(str){
    if(str.indexOf("桃園縣") != -1){
      return str.indexOf("縣");
    }
    else if(str.indexOf("台中縣") != -1){
      return str.indexOf("縣");
    }
    else if(str.indexOf("台南縣") != -1){
      return str.indexOf("縣");
    }
    else if(str.indexOf("高雄縣") != -1){
      return str.indexOf("縣");
    }
    return -1;
  }

  var tmp1 = isCity(addr);
  if(tmp1 != -1 ){ // 如果要升格的
    var tmp2 = isArea(addr.substr(tmp1+1));
    if(tmp2 != -1){ // 有縣有市的
      // console.log(i+ " ori: "+ addr);
      addr = addr.replaceAt(tmp1, "市");
      addr = addr.replaceAt(tmp1+tmp2+1, "區");
      // console.log(i+ " chg: "+ addr);
      // console.log("\n");
    }
    else{ // 有縣有區
      // j++; if(j>30) break;
      // console.log(i+ " noarea: "+ addr);
      addr = addr.replaceAt(tmp1, "市");
      // console.log(i+ " chg: "+ addr);
    }
  }
  return addr;
}

Api.addRoute('parseaddr', {authRequired: false}, {
  get: function () {
    // console.log("in serverpeople"); 
    
    var data = Assets.getText('temple0305_b1100_utf8.csv');
    n = data.split('\n').length; 
    var myArray = data.split('\n'); 

    console.log("number: " + n); 

    function isArea(str){
      if(str.indexOf("鄉") != -1){
        return str.indexOf("鄉");
      }
      else if(str.indexOf("鎮") != -1){
        return str.indexOf("鎮");
      }
      else if(str.indexOf("縣") != -1){
        return str.indexOf("縣");
      }
      else if(str.indexOf("市") != -1){
        return str.indexOf("市");
      }
      return -1;
    }

    function isCity(str){
      if(str.indexOf("桃園縣") != -1){
        return str.indexOf("縣");
      }
      else if(str.indexOf("台中縣") != -1){
        return str.indexOf("縣");
      }
      else if(str.indexOf("台南縣") != -1){
        return str.indexOf("縣");
      }
      else if(str.indexOf("高雄縣") != -1){
        return str.indexOf("縣");
      }
      return -1;
    }

    // var j=0;
    for (var i in myArray) {
      var data = myArray[i].split(',');
      if(data.length < 12 || i == 1){
        continue;
      }
      var addr = data[12];

      var tmp1 = isCity(addr);
      if(tmp1 != -1 ){ // 如果要升格的
        var tmp2 = isArea(addr.substr(tmp1+1));
        if(tmp2 != -1){ // 有縣有市的
          // console.log(i+ " ori: "+ addr);
          addr = addr.replaceAt(tmp1, "市");
          addr = addr.replaceAt(tmp1+tmp2+1, "區");
          // console.log(i+ " chg: "+ addr);
          // console.log("\n");
        }
        else{ // 有縣有區
          // j++; if(j>30) break;
          console.log(i+ " noarea: "+ addr);
          addr = addr.replaceAt(tmp1, "市");
          console.log(i+ " chg: "+ addr);
        }
      }
      // var obj = {};
    }

    return 1;
  }
});
Api.addRoute('createaddrdb', {authRequired: false}, {
  get: function () {
  /*
  post5,addr1,addr2,addr3,addr4
  10058,台北市,中正區,八德路一段,全
   */
    var filestr = Assets.getText('Zip32_csv_10501_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data; 
    for (var i in myArray) {
      var data = myArray[i];
      
      var post3 = data.post5.substr(0, 3);
      if(Addr1.find({addr1: data.addr1, addr2: data.addr2}).count() == 0){
        var obj1 = {
          post3: post3,
          addr1: data.addr1,
          addr2: data.addr2
        }
        Addr1.insert(obj1);
      }
      
      Addr2.insert(data);
      // console.log(data.post5 + " " + data.addr1 + " " + data.addr2 + " " + data.addr3 + " " + data.addr4);

    }
    console.log("addr1 ok");
    return "";
  }
});
Api.updateBirthday = function(u){
  if(u.bornYear == "吉" || u.bornMonth == "吉" || u.bornDay == "吉" || u.bornMonth == "0" || u.bornDay == "0"){
    // continue;
  }
  else if(!!u.bornYear && !!u.bornMonth && !!u.bornDay && !!u.name){

    var l = 0;
    var m = u.bornMonth;
    if( u.bornMonth.indexOf("閏")!= -1 ){
        l = 1;
        m = u.bornMonth.substr(1);
    }
    var birthday = funcLunar(1, Number(u.bornYear)+1911, m, u.bornDay, l);

    var str = u.name +" 農："+u.bornYear+"."+u.bornMonth+"."+u.bornDay+" => 國："+birthday;
    // console.log(str);  
    if(birthday == "閏月錯誤"){
      // obj.fails.push(str);
      console.log("閏月錯誤" + str);  
    }
    else{
      // obj.success.push(str);
      People.update(u, {$set:{
        "birthday" : birthday
      }});
    }
  }
}
Api.addRoute('createbirthday', {authRequired: false}, {
  get: function () {
    // var obj = {};
    // obj.fails = [];
    // obj.success = [];
    People.find().forEach(function(u){
      Api.updateBirthday(u);
    });

    console.log("birthday ok");
    return obj;
  }
});

Api.addRoute('buildpassname', {authRequired: false}, {
  get: function () {
    var ba = PrayingType.findOne({value: "拔"});
    Praying2.find({type_text: /.*拔.*/}).forEach(function(u){
      var passname = u.passname_text;
      var obj = {};

      obj.type = ba._id;
      obj.type_text = "拔";
      obj.passname_text = obj.passname_text1 = obj.passname_text2 = obj.passname_text3 = obj.passname_text4 = "";
      obj.prayingtype_orderid = ba.order;

      if(passname.indexOf("歷代祖先") != -1){
        obj.passname_text1 = passname;
        obj.pass_order = 1;
      }
      else if(passname.indexOf("冤親債主") != -1){
        obj.passname_text2 = passname;
        obj.pass_order = 2;
      }
      else if(passname.indexOf("地基主") != -1){
        obj.passname_text3 = passname;
        obj.pass_order = 3;
      }
      else if(passname.indexOf("嬰靈") != -1){
        obj.passname_text4 = passname;
        obj.pass_order = 4;
      }
      else{
        obj.passname_text = passname;
        obj.pass_order = 0;
      }
      console.log(u.ordernum + " 0: " + obj.passname_text + " 1: " + obj.passname_text1 + " 2: " + obj.passname_text2 + " 3: " + obj.passname_text3 + " 4: " + obj.passname_text4);

      Praying2.update(u, {$set: obj});

    });

    return "Done";
  }
});

Api.addRoute('createpray', {authRequired: false}, {
  get: function () {
    /*
    ano,name,if_m
    17,佛七法會,0
    14,盂蘭盆法會,0
    9,藥師法會,0
    18,浴佛法會,0
     */
    /*    console.log("building Pray1"); 
    var data = Assets.getText('temple0305_ac8000_pray1_utf8.csv');
    var n = data.split('\n').length; 
    var myArray = data.split('\n'); 
    for (var i in myArray) {
      var data = myArray[i].split(',');
      if(data.length < 3 || i == 0) continue;
      var obj = {
        ori_id: data[0].trim(),
        value: data[1].trim(),
        now_use: data[2].trim(),
      };
      Pray1.insert(obj); 
      if(i % 10 == 0) console.log("進度：" + i + "/" + n);
      // if(i==1000) break;
    }*/


  /*
  0ano,1name,2lev_name,3now_num,4ac_money,5num
  16,7月份梁皇法會,隨喜,3,0,1
  17,法會資料,隨喜,1,0,1
  18,藥師法會,總燈主,2,50000,1
   */
  /*    console.log("building Pray2"); 
    var data = Assets.getText('temple0305_ac8000lig_pray2_utf8.csv');
    var n = data.split('\n').length; 
    var myArray = data.split('\n'); 
    for (var i in myArray) {
      var data = myArray[i].split(',');
      if(data.length < 3 || i == 0) continue;

      var class_id = "", ori_class_id = "";
      if(Pray1.find({value: data[1].trim()}).count()){
        var item = Pray1.find({value: data[1].trim()}).fetch()[0];
        class_id = item._id;
        ori_class_id = item.ori_id;
      }
      else{
        console.log(i+ " 錯誤："+ i + " cannot find parent's id");
        // return "錯誤";
      }
      var obj = {
        ori_id: data[0].trim(),
        class_id: class_id,
        class_id_text: data[1].trim(),
        ori_class_id: ori_class_id,
        order_id: Number(data[0].trim()),
        value: data[2].trim(),
        now_num: data[3].trim(),
        price: data[4].trim(),
      };
      Pray2.insert(obj); 
      if(i % 50 == 0) console.log("進度：" + i + "/" + n);
      // if(i==1000) break;
    }
  */
  /*
  0 ano, 1 c_date , 2 c_year,3 if_is, 4 log_name, 5 num_no,
  3477 , 2014/2/14, 103     ,0      , 軟體維護    ,        ,

  6 c_memo, 7 total_m, 8 p_name, 9 anum  , 10 pp,
  29      ,100000    ,         ,R00002309, 0    ,

  11 b1000_non, 12 n_money, 13 ctype, 14 ctype1, 15 b1100ano, 16 c_memo2
  2738        , 0        ,  修繕法會  , 藥師佛10萬, 11492      , 6-37

   */
    console.log("building Praying1"); 
    var data = Assets.getText('temple0305_c4000_praying1_utf8.csv');
    var n = data.split('\n').length; 
    var myArray = data.split('\n'); 
 if(0)
    for (var i in myArray) {
      var data = myArray[i].split(',');
      if(data.length < 3 || i == 0) continue;

      var p2 = Pray2.find({class_id_text: data[13].trim(), value: data[14].trim()});
      if( p2.count() ){
        pray2 = p2.fetch()[0];
        // console.log(pray2);

        var p = People.find({ori_id: "l"+data[15].trim()}).fetch()[0];
        var obj = {
          ori_id: data[0].trim(),
          familyId : p.familyId, //"xaapqcuXtBY3hQd24",
          ori_family_id : data[11].trim(), // "2738",
          if_is : data[3].trim(), // "0",
          isSave : 1,
          date : data[1].trim(), // "2014/2/14",
          // user_id : "KH5uFTvrzeYgAX8hn",
          // ori_user_id : "13",
          user_name : data[4].trim(), // "軟體維護",
          prayserial : data[6].trim(), // "29",
          cash : data[7].trim(), // "100000",
          ordernum : data[9].trim(), // "R00002309",
          year : Number(data[2].trim()), // 103,
          whoapply : p._id, //"C9BHDvWP2rZZT3DQc",
          whoapply_text : p.name, //"許宸銘"
          ori_whoapply : data[15].trim(), // "11492",
          c_memo2 : data[16].trim(), // "6-37 ",
          ori_class_id : pray2.ori_class_id ,//"20",
          prayname : pray2.class_id, //"aBCKghu9BiJG9snrT",
          prayname_text : data[13].trim(), // "修繕法會",
          ori_type_id : pray2.ori_id, //"91",
          prayitem : pray2._id, //"DZ4mSMuwcWM3h9cTE",
          prayitem_text : data[14].trim(), // "藥師佛10萬",
        };
        Praying1.insert(obj); 
        console.log(i+ " " + data[9].trim() + " " + data[13].trim() + " " + data[14].trim() + " " +p.name);
      }
      else{
        console.log(i+ " 錯誤："+ "" + " cannot find parent's id");
        // return "錯誤";
      }
     
      if(i % 200 == 0) console.log("進度：" + i + "/" + n);
      // if(i==1000) break;
    }

  /*
  ano,c1000_no,b1100_no,p1,p1_num,p2,p2_num,p3,p3_num,p4,p4_t,p4_num,p5,p5_num,p6,p6_num,p7,p7_num,print_if,money_1,money_2,money_3,money_4,b1000_no,name1,f_name,f_name_num,bir,bir_1,sex,p8,p8_num,p9,p9_num,p10,p10_num,p11,p11_num,addr,sh1,sh2

   */

  /*

  8133,3832,0,,,0,,0,,0,,,0,,0,,0,,0,0,0,0,0,5237,陳姿珊,陳秀峰,,,,,0,,0,,0,,0,,,拔,
  {
    "_id" : "2BAjMjgfujYS6ZLSu",
    "ori_id" : 8133,
    "listId" : "kKu42x7vMCPYD8moZ",
    "ori_praying1" : "3832",
    "type" : "jY3ki7xwjaBnbiR3d",
    "type2" : 2,
    "ori_type" : 2,
    "type_text" : "拔",
    "livename_text" : "陳姿珊",
    "passname_text" : "陳秀峰",
    "addr" : "桃園市桃園區中山路79號",
    "prayname" : "dSaMAypuatTuhs6Te",
    "prayname_text" : "梁皇法會",
    "prayitem" : "ZNE44DHYFcSH25A5N",
    "prayitem_text" : "副總功德主",
    "year" : 103,
    "prayserial" : 5,
    "ordernum" : "R00002664",
    "pray2_orderid" : 2,
    "prayingtype_orderid" : 2
  }

  8137,3832,0,鍾氏歷代祖先,,0,,0,,0,,,0,,0,,0,,0,0,0,0,0,5237,陳姿珊,,,,,,0,,0,,0,,0,,,拔,
  {
      "_id" : "SqwcnC7rFuZHit7Cn",
      "ori_id" : 8137,
      "listId" : "kKu42x7vMCPYD8moZ",
      "ori_praying1" : "3832",
      "type" : "LkoLc3ZKFfcGcs3Ph",
      "type2" : 9,
      "ori_type" : 2,
      "type_text" : "拔 - 歷代祖先",
      "livename_text" : "陳姿珊",
      "passname_text" : "鍾氏歷代祖先",
      "addr" : "桃園市桃園區中山路79號",
      "prayname" : "dSaMAypuatTuhs6Te",
      "prayname_text" : "梁皇法會",
      "prayitem" : "ZNE44DHYFcSH25A5N",
      "prayitem_text" : "副總功德主",
      "year" : 103,
      "prayserial" : 5,
      "ordernum" : "R00002664",
      "pray2_orderid" : 2,
      "prayingtype_orderid" : 3
  }

  8232,3915,0,,,-1,,0,,0,,,0,,0,,0,,0,0,0,0,0,2646,"古陳月香,古淑華,古詠馨,許允,許榮德,李麗英,許榮華,陳儀綿,許書豪,沈晁弘,許芳華,郭儀汝,釋緣空,許嘉,王岩,許芳榆",,,,,,0,,0,,0,,0,,,拔,

  7815,3616,0,,,0,,0,,0,,,0,,0,,0,,0,0,0,0,0,1971,邱聰溪闔家,,,,,,0,,0,,0,,0,,,吉祥,
  7819,3620,0,,,0,,0,,0,,,0,,0,,0,,0,0,0,0,0,2132,"雷成亮,雷秋萍",,,,,,0,,0,,0,,0,,,如意,
  8855,4382,0,,,-1,,0,,0,,,0,,0,,0,,0,0,0,0,0,16349,"簡謝螢月,簡民勝,簡榛葶,簡士凱",,,,,,0,,0,,0,,0,,,拔,
  9039,4543,0,,,0,,0,,0,,,0,,0,,0,,0,0,0,0,0,244,王賴秀玉闔家,,,,,,0,,0,,0,,0,,,小燈,

  11196,5686,0,,,0,,0,,0,,,0,,0,,0,,0,0,0,0,0,303,"釋性玄,簡雅淑,劉沛怡,劉沛歆,簡惠銘",,,,,,0,,0,,0,,0,,,燈主,
  11197,5686,0,,,0,,0,,0,,,0,,0,,0,,0,0,0,0,0,303,"釋性玄,簡雅淑,劉沛怡,劉沛歆,簡惠銘",,,,,,0,,0,,0,,0,,,消,
   
  ano ,c1000_no,b1100_no,p1,p1_num,p2,p2_num,p3,p3_num,p4,p4_t,p4_num,p5,p5_num,p6,p6_num,
  8963,4479    ,0       ,  ,      ,0 ,      ,0 ,      ,0 ,    ,      ,0 ,      ,0 ,      ,

  p7,p7_num,print_if,money_1,money_2,money_3,money_4,b1000_no,name1                    ,
  0 ,      ,0       ,0      ,0      ,0      ,0      ,11162   ,"陳萬枝,陳怡真,何秀英,廖文珍",

  f_name,f_name_num,bir,bir_1,sex,p8,p8_num,p9,p9_num,p10,p10_num,p11,p11_num,addr,sh1,sh2
        ,          ,   ,     ,   ,0 ,      ,0 ,      ,0  ,       ,0  ,       ,    ,消 ,
   */

    console.log("building Praying2"); 
    var filestr = Assets.getText('temple0305_c4100_praying2_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data; 

    for (var i in myArray) {
      var data = myArray[i];
      if(data.length < 3 || i == 0) continue;

      var passname = passname1 = passname2 = passname3 = passname4 = "";
      var prayingtype = data.sh1;
      if(data.sh1 == "拔"){

        if(data.p1){
          passname1 = data.p1; // x氏歷代
        }
        else if(data.p2 == "-1") {
          passname2 = "累劫冤親債主";
        }
        else if(data.p3 == "-1") {
          passname3 = "地基主";
        }
        else{
          passname = data.f_name;
        }
      }
      else if(data.sh1 == "總功"){
        prayingtype = "總功德主";
      }
  // console.log("=========data==============");
  // console.log(data);

        var pt  = PrayingType.find({value : prayingtype}).fetch()[0];
        if(isEmpty(pt)) {
          console.log(i+ " 錯誤："+ "" + " cannot find PrayingType's id");
          continue;
        }

        var py1 = Praying1.find({ori_id : data.c1000_no}).fetch()[0];
  // console.log("=========pt==============");
  // console.log(pt);
  // console.log("=========py1==============");
  // console.log(py1);
        
        if(!!py1){
          var p2  = Pray2.find({_id: py1.prayitem}).fetch()[0];
          var p = People.find({_id: py1.whoapply}).fetch()[0];

  // console.log("=========p==============");
  // console.log(p);

          var addr = "";
        if(!!p && p.addr){
          addr = p.addr;
        }
        if(!!data.p11_num){
          addr = data.p11_num;
        }

        var obj = {
          "ori_id" : Number(data.ano), // 8963
          "listId" : py1._id, //"fTp7E5itvmyZE9twm", // praying1 _id
          "ori_praying1" : data.c1000_no, // "4479",
          // "type2" : 1,
          // "ori_type" : 1,
          "type" : pt._id, //"987jZBKLhTtaQ6kuW", // praying type
          "type_text" : data.sh1, //"消",
          "livename_text" : data.name1, //"陳萬枝,陳怡真,何秀英,廖文珍",
          "passname_text" : passname,
          "passname_text1" : passname1,
          "passname_text2" : passname2,
          "passname_text3" : passname3,
          "passname_text4" : passname4,
          "addr" : addr,

          "prayname" : py1.prayname ,// "aBCKghu9BiJG9snrT",
          "prayname_text" : py1.prayname_text ,// "修繕法會",
          "prayitem" : py1.prayitem ,// "ywRBrL72TF4nfcF74",
          "prayitem_text" : py1.prayitem_text ,// "隨喜",
          "year" : Number(py1.year) ,// 103,
          "prayserial" : Number(py1.prayserial) ,// 952,
          "ordernum" : py1.ordernum ,// "R00003309",
          
          "pray2_orderid" : Number(p2.order_id),
          "prayingtype_orderid" : Number(pt.order)
        };
        Praying2.insert(obj); 
        console.log(i+ " " + obj.year + " " + obj.ori_id + " " +obj.prayname_text + " " + obj.prayitem_text + " " + obj.type_text + " L: " + obj.livename_text+ " P: " + obj.passname_text + " " + addr);
      }
      else{
        console.log(i+ " 錯誤："+ "" + " cannot find parent's id");
      }

      if(i % 200 == 0) console.log("進度：" + i + "/" + n);
      // if(i==1000) break;
    }

    console.log("資料轉換完畢");
    return "資料轉換完畢";
  }
});


Api.addRoute('createlighting12', {authRequired: false}, {
  get: function () {
    console.log("building Lighting1"); 

    var filestr = Assets.getText('temple0305_c1000_lighting1_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data; 

 if(0)
    for (var i in myArray) {
      var data = myArray[i];
      var p = People.find({ori_family_id: data.b1000_no, mainPerson: -1}).fetch()[0];

      if(!!p){
        var obj = {
          ori_id: data.ano,
          familyId : p.familyId, //"xaapqcuXtBY3hQd24",
          ori_family_id : data.b1000_no, // "2738",
          if_is : data.if_is, // "0",
          isSave : 1,
          date : data.c_date, // "2014/2/14",
          username_text : "許允", // "軟體維護",
          // cash : data[7].trim(), // "100000",
          ordernum : data.anum, // "R00002309",
          year : Number(data.c_year), // 103,
          apply_id : p._id, //"C9BHDvWP2rZZT3DQc",
          apply_text : p.name, //"許宸銘"
          ori_log_name : data.log_name,
          // c_memo2 : data[16].trim(), // "6-37 ",
          price_total: data.total_m,
          price_receive: data.f_money,
        };
        Lighting1.insert(obj); 
      }  
      if(i % 200 == 0) console.log("進度：" + i + "/" + myArray.length);
    }

    console.log("building Lighting2 pass1"); 

    var filestr = Assets.getText('temple0305_c1100_lighting2_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data; 

  // if(0)
    for (var i in myArray) {
      var data = myArray[i];

      // if(data.p1 == "0" && data.p4 == "0" && data.p6 == "0" ){
      //   continue;
      // }

      var lighting1 = Lighting1.findOne({ ori_id: data.c1000_no});
      if(!!lighting1){

        var p = People.findOne({ori_id: "l"+data.b1100_no} );

        var light_text = "";
        if(data.p1 == "-1") light_text = "光明燈";
        else if(data.p3 == "-1") light_text = "藥師燈";
        else if(data.p5 == "-1") light_text = "光明大燈";

        var birth = "";
        if(!!p.bornYear && !!p.bornMonth && !!p.bornDay){
          birth = p.bornYear + "年" + p.bornMonth + "月" + p.bornDay + "日";
        }
        var time = "";
        if(!!p.bornTime_text){
          time = p.bornTime_text + "時";
        }
        else{
          time = "吉時";
        }

        var obj = {
          "ori_id" : Number(data.ano), // 8963

          "lighting1" : lighting1._id, //"fTp7E5itvmyZE9twm", // praying1 _id
          "ori_lighting1" : data.c1000_no, // "4479",
          
          people_id : p._id,
          people_ori_id : data.b1100_no,
          people_name : p.name, // 主要的名字

          familyId : p.familyId, //"xaapqcuXtBY3hQd24",
          ori_family_id : data.b1000_no, // "2738",

          p1: data.p1,
          p1_num: data.p1_num,
          p1_isallfamily: data.p2,
          p1_name2: data.p2_num,

          p3: data.p3,
          p3_num: data.p3_num,
          p3_isp3: data.p4,

          p5: data.p5,
          p5_num: data.p5_num,
          p5_isp5: data.p6,

          money_1: data.money_1,
          money_3: data.money_3,
          money_5: data.money_4,
          year: lighting1.year,
          light_text: light_text,
          addr: p.addr,
          lunar_birth_text: birth,
          lunar_time_text: time,


          // "year" : Number(py1.year) ,// 103,
          // "prayserial" : Number(py1.prayserial) ,// 952,
          // "ordernum" : py1.ordernum ,// "R00003309",
        };
        Lighting2.insert(obj); 
        // console.log(i+ " " + obj.year + " " + obj.ori_id + " " +obj.prayname_text + " " + obj.prayitem_text + " " + obj.type_text + " L: " + obj.livename_text+ " P: " + obj.passname_text + " " + addr);
      }
      else{
        console.log(i+ " 錯誤："+ "" + " cannot find parent's id");
      }

      if(i % 200 == 0) console.log("進度：" + i + "/" + myArray.length);
      // if(i==1000) break;
    }

    console.log("building Lighting2 pass2-1"); 
    Lighting2.find({p3: "-1"}).forEach(function(u){
      if(u.p3 == "-1"){
        var p3_name2 = "";
        var arr_tmp = Lighting2.find({"ori_lighting1": u.ori_lighting1, p3: "0", p3_isp3:"-1"}).fetch();
        var arr_name = [];
        for(var i in arr_tmp){
          arr_name.push(arr_tmp[i].people_name);
        }
        p3_name2 = arr_name.toString();   
        Lighting2.update(u, {$set:{
            "p3_name2" : p3_name2
        }});
        console.log("p3:-1, ori_lighting1: "+ u.ori_lighting1 + " p3_name2: "+ p3_name2); 
      }
    });

    console.log("building Lighting2 pass2-2"); 
    Lighting2.find({p5: "-1"}).forEach(function(u){
      if(u.p5 == "-1"){
        var p5_name2 = "";
        var arr_tmp = Lighting2.find({"ori_lighting1": u.ori_lighting1, p5: "0", p5_isp5:"-1"}).fetch();
        var arr_name = [];
        for(var i in arr_tmp){
          arr_name.push(arr_tmp[i].people_name);
        }
        p5_name2 = arr_name.toString();   
        Lighting2.update(u, {$set:{
            "p5_name2" : p5_name2
        }});
        console.log("p5:-1, ori_lighting1: "+ u.ori_lighting1 + " p5_name2: "+ p5_name2); 
      }
    });


    console.log("資料轉換完畢");
    return "資料轉換完畢";
  }
});

//////////////
///
///
///////////////
Api.addRoute('createpeople', {authRequired: false}, {
  get: function () {

    function parseDate(str){
      var obj = {};
      var arr = str.split(" ");
      if(arr.length < 2){
      }
      else{
        var arr_date = arr[0].split(".");

        if(Number(arr_date[0])>0)
          obj.year = arr_date[0].toString() || 0;
        
        if(Number(arr_date[1])>0)
          obj.month = arr_date[1].toString() || 0;
              
        if(Number(arr_date[2])>0)
          obj.day = arr_date[2].toString() || 0;

        obj.time_str = arr[1].trim().replace("時", "").trim();
        obj.time = arrTime2.indexOf( obj.time_str ).toString() || "0";
      }
      return obj;

    }

    // 
    // 建 family 名單
    // 
    var filestr = Assets.getText('temple0305_b1000_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data; 
    
    console.log("重建家庭清單"); 
if(0)
    for (var i in myArray) {
      var data = myArray[i].split(',');
      if(data.length < 5 || i == 0){
        continue;
      }

      var ori_id = data[5].trim();
      // if(Family.find({ori_id: ori_id}).count() == 0){
        var obj = {
          ori_id: ori_id,
          if_is: data[3].trim(),
          // user_id: ori_id,
          // ori_user_id: ori_id,
          log_name: data[4].trim(),
        };
        // console.log("進度：" + i + "/" + n + " Family insert: "+ ori_id);
        Family.insert(obj); 
        // console.log(obj);
      // }
      if(i % 200 == 0) console.log("進度：" + i + "/" + n);
      // if(i==1000) break;
    }

    // 
    // 建 live 名單
    // 
    var filestr = Assets.getText('temple0305_b1100_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data; 

    console.log("重建信眾清單"); 
  // if(0)
    for (var i in myArray) {
      var data = myArray[i];
      // if(data.length < 22 || i == 0){
        // continue;
      // }
        //
        // 0:  .b1100_no,
        // 1:  .name,
        // 2:  .sex,
        // 3:  .if_is,
        // 4:  .root,
        // 5:  .bir1,
        // 6:  .bir2,
        // 7:  .bir3,
        // 8:  .bir4,
        // 9:  .bir,
        // 10: .tel,
        // 11: .pos,
        // 12: .address,
        // 13: .ano, 
        // 14: .bsend,
        // 15: .tel2,
        // 16: .intro
        // 17: .if_ad
        // 18: .btype,
        // 19: .btype_1,
        // 20: .btype_num,
        // 21: .bb1,
        // 22: .bb2
        // 
      
      var familyid = "";
      var ori_family_id =  data.b1100_no;
      var ori_id =  data.ano;

      if(Family.find({ori_id: data.b1100_no}).count()){
        familyid = Family.find({ori_id: ori_family_id}).fetch()[0]._id;
      }
      else{
        console.log(i+ " 錯誤："+ ori_id + " 無家庭id");
        // return "錯誤";
      }

      var templeclass = templeclass_ori = "";
      if(TempleClass.find({value: data.btype}).count()){
        var c = TempleClass.find({value: data.btype}).fetch()[0];
        templeclass = c._id;
        templeclass_ori = c.ori_id;
      }

      var templelevel = templelevel_ori = "";
      if(TempleLevel.find({value: data.btype_1}).count()){
        var c = TempleLevel.find({value: data.btype_1}).fetch()[0];
        templelevel = c._id;
        templelevel_ori = c.ori_id;
      }

      var sexual = arrSexual2.indexOf(data.sex);
      var bornTime = arrTime2.indexOf(data.bir4);
      var zodiac_id = arrChineseYear2.indexOf(data.bir);
      var addr = Api.toNewAddr(data.address);

      var obj = {
        ori_id: "l"+ori_id,
        familyId: familyid,
        ori_family_id: ori_family_id,
        name: data.name,
        isLive: 1,
        sexual: sexual,
        sexual_text: data.sex,
        if_is: data.if_is,
        mainPerson: parseInt(data.root),
        bornYear: data.bir1,
        bornMonth: data.bir2,
        bornDay: data.bir3,
        bornTime: bornTime,
        bornTime_text: data.bir4,
        zodiac_id: zodiac_id,
        zodiac_text: data.bir,
        telephone: data.tel,
        post5code: data.pos,
        addr: addr,
        mailThis: data.bsend,
        cellphone: data.tel2,
        intro: data.intro,
        if_ad: data.if_ad,
        templeClass: templeclass,
        templeClass_ori: templeclass_ori,
        templeClass_text: data.btype,
        templeLevel: templelevel,
        templeLevel_ori: templelevel_ori,
        templeLevel_text: data.btype_1,
        memberId: parseInt(data.btype_num) || 0, // 社員編號
        longLive: data.bb1,
        lotus: data.bb2,
        // if_is: data[3].trim(),
        // user_id: ori_id,
        // ori_user_id: ori_id,

      };
      // if(data[19].trim()) console.log(obj);

      // var c = People.find({ori_id: ori_id});

/*      if(c.count() == 0){       // insert (原本沒有的)
        console.log("進度：" + i + "/" + n + " People Insert: " + ori_id + " " + obj.name + " " + obj.addr);
        People.insert(obj);
      }
      else{                     // update (原本有的)
        if(data[12].trim() != addr){
          // console.log("進度：" + i + "/" + n + " People Update: " + ori_id + " " + obj.name + " " + data[12].trim()  + " -> " + obj.addr);
        }
        People.update(c.fetch()[0]._id, {$set: obj });
      }*/
      People.insert(obj);
      // console.log(obj);

      if(i % 200 == 0) console.log("進度：" + i + "/" + myArray.length);

      // if(i==1000)break;

    }


    // 
    // 建 pass 名單
    //

    var filestr = Assets.getText('temple0305_b1200_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data; 

    console.log("重建往生者清單"); 
    for (var i in myArray) {
      // var data = myArray[i].split(',');
      var data = myArray[i];

      // 0  .ano,         0
      // 1  .b1000_no,    1
      // 2  .name,        2
      // 3  .name1,
      // 4  .bir,    生時
      // 5  .bir_1,  卒時
      // 6  .if_is,
      // 7  .b_call,
      // 8  .sex     享壽

      var familyid = "";
      var ori_family_id =  data.b1000_no;
      var ori_id =  "p"+data.ano;

      if(Family.find({ori_id: ori_family_id}).count()){
        familyid = Family.find({ori_id: ori_family_id}).fetch()[0]._id;
      }
      else{
        console.log(i+ " 錯誤："+ ori_id + " 無家庭id");
        // return "錯誤";
      }
      var sexual = arrSexual2.indexOf(data.name);
      var addr = Api.toNewAddr(data.b_call);
 
      var obj_live = parseDate(data.bir);
      // if(Object.keys(obj_bir).length)console.log(obj_bir);
      
      var obj_pass = parseDate(data.bir_1);
      // if(Object.keys(obj_bir).length)console.log(obj_bir);
      
      var obj = {
        ori_id: ori_id,
        familyId: familyid,
        ori_family_id: ori_family_id,
        isLive: 0,
        name: data.name,
        name1: data.name1,
        bornYear: obj_live.year || "",
        bornMonth: obj_live.month || "",
        bornDay: obj_live.day || "",
        bornTime: obj_live.time || "",
        bornTime_text: obj_live.time_str || "",
        passYear: obj_pass.year || "",
        passMonth: obj_pass.month || "",
        passDay: obj_pass.day || "",
        passTime: obj_pass.time || "",
        passTime_text: obj_pass.time_str || "",
        // post5code: data[11].trim(),
        addr: addr,
        // longLive: data[21].trim(),
        // lotus: data[22].trim(),
        lifeYear: data.sex,
        if_is: data.if_is,
      };
      // console.log(obj);
      // var c = People.find({ori_id: ori_id});

/*      if(c.count() == 0){       // insert (原本沒有的)
        console.log("進度：" + i + "/" + n + " People pass Insert: " +ori_id + " " + obj.name + " " + obj.addr);
        People.insert(obj);
      }
      else{                     // update (原本有的)
        if(data[7].trim() != obj.addr){
          // console.log("進度：" + i + "/" + n + " People pass Update: " +ori_id + " " + obj.name + " " + data[7].trim() + " -> " + obj.addr);
        }
        People.update(c.fetch()[0]._id, {$set: obj });
      }*/
      People.insert(obj);
      // console.log(obj);
      if(i % 200 == 0) console.log("進度：" + i + "/" + myArray.length);
      // if(i == 1000) break;
    }

    console.log("資料轉換完畢");
    return "資料轉換完畢";
  }
});
