
Api.addRoute('people', {authRequired: false}, {
  get: function () {
    // console.log("Rest People GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};

    // console.log("before queryVar: ");
    // console.log(queryVar);

    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // console.log("sortVar: ");
    // console.log(sortVar);
    // console.log("pageVar: ");
    // console.log(pageVar);

    if(!!queryVar.isLive){
      queryVar.isLive = Number(queryVar.isLive);
    }

    if(!!queryVar.sexual){
      queryVar.sexual = Number(queryVar.sexual);
    }

    /*    if(!!queryVar.mailThis && queryVar.mailThis=="true"){
          queryVar.mailThis = "-1";
        }
        else{ // if(queryVar.mailThis == "false"){
          delete queryVar.mailThis;
        }
    */
    if(!!queryVar.nowform) delete queryVar.nowform;
    if(!!queryVar.bossname) delete queryVar.bossname;
    if(!!queryVar.isBound) delete queryVar.isBound;

    if(!!queryVar.findData){
      var searchValue = queryVar.findData;
      delete queryVar.findData;

      queryVar = {
        $and: [
          {$or: [
            {name: {$regex : ".*"+searchValue+".*"}},
            {identify: {$regex : ".*"+searchValue+".*"}},
            {telephone: {$regex : ".*"+searchValue+".*"}},
            {cellphone: {$regex : ".*"+searchValue+".*"}},
            {addr: {$regex : ".*"+searchValue+".*"}},
          ]},
          queryVar,
        ]
      };
    }
    else{
      delete queryVar.findData;
    }

    var uniType = Number(queryVar.uniType);
    delete queryVar.uniType;

    var my_print = queryVar.my_print;
    delete queryVar.my_print;

    var tc = "";
    if(queryVar.templeClass == "-1" || queryVar.templeClass == "-2"){
      tc = queryVar.templeClass;
      delete queryVar.templeClass;
    }

    var tl = "";
    if(queryVar.templeLevel == "-1" || queryVar.templeLevel == "-2"){
      tl = queryVar.templeLevel;
      delete queryVar.templeLevel;
    }

    // 如果是全部的話，就社員和護法都不選
    if(my_print == "1"){
//      tc = "-2";
//      tl = "-2";
      tc = "-1";
      tl = "-1";
    }
    else{


      if(tc == "-1"){
        queryVar = {
          $and: [
            {templeClass: { $ne: "" }},
            {templeClass: { $ne: null }},
            queryVar,
          ]
        };
      }
      else if(tc == "-2"){
        queryVar = {
          $and: [
            {$or: [
              {templeClass: ""},
              {templeClass: null}
            ]},
            queryVar,
          ]
        };
      }

      if(tl == "-1"){
        queryVar = {
          $and: [
            {templeLevel: { $ne: "" }},
            {templeLevel: { $ne: null }},
            queryVar,
          ]
        };
      }
      else if(tl == "-2"){
        queryVar = {
          $and: [
            {$or: [
              {templeLevel: ""},
              {templeLevel: null}
            ]},
            queryVar,
          ]
        };
      }
    }

    //  console.log("queryVar: ");
    //  console.log(queryVar);

    if(uniType == 1){ // 地址唯一
      // 先找出所有應該顯示的
      var arrObj = People.find(queryVar, {
        sort: sortVar,
      }).fetch();
      arrObj = _.uniq(arrObj, false, function(d) {return d.addr});

      return {
        data: arrObj.slice(pageVar.skip, pageVar.skip+pageVar.limit),
        itemsCount: arrObj.length
      }
    }
    else if(uniType == 2){ // 姓名 地址唯一
      // 先找出所有應該顯示的
      var arrObj = People.find(queryVar, {
        sort: sortVar,
      }).fetch();
      arrObj = _.uniq(arrObj, false, function(d) {return d.name+d.addr});

      return {
        data: arrObj.slice(pageVar.skip, pageVar.skip+pageVar.limit),
        itemsCount: arrObj.length
      }
    }
    else{
      var arrObj = People.find(queryVar, {
        sort: sortVar,
        skip: pageVar.skip,
        limit: pageVar.limit
      }).fetch();

      return {
        data: arrObj,
        itemsCount: People.find(queryVar).count()
      }
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest People insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = "H"+funcStrPad(getNextSequence('People'), 7);
      // params.fullname = params.countryname+" - "+params.value;
      params.isLive = Number(params.isLive);

      if(params.mainPerson == '-1'){
        var arr = People.find({"familyId": params.familyId}).fetch();
        for(var i=0; i<arr.length; i++){
        // for(var index in arr){
         People.update(arr[i]._id, {$set: {"mainPerson": "0"} })
        }
      }
      params.mainPerson = Number(params.mainPerson);
      params.memberId = Number(params.memberId);
      // params.mailThis = params.mailThis;
      params.sexual = Number(params.sexual);
      params.sexual_text = arrSexual2[Number(params.sexual)];
      params.bornTime = Number(params.bornTime);
      params.bornTime_text = arrTime2[Number(params.bornTime)];

      if(!!params.passTime){
        params.passTime = Number(params.passTime);
        params.passTime_text = arrTime2[Number(params.passTime)];
      }
      params.zodiac_id = Number(params.zodiac_id);
      params.zodiac_text = arrChineseYear2[Number(params.zodiac_id)];

      if(!!params.templeClass){
        params.templeClass_text = TempleClass.findOne(params.templeClass).value;
      }
      if(!!params.templeLevel){
        params.templeLevel_text = TempleLevel.findOne(params.templeLevel).value;
      }

      if (People.insert(params)) {
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest People update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;

      params.isLive = Number(params.isLive);

      if(params.mainPerson == '-1'){
        var arr = People.find({"familyId": params.familyId}).fetch();
        for(var i=0; i<arr.length; i++){
        // for(var index in arr){
         People.update(arr[i]._id, {$set: {"mainPerson": "0"} })
        }
      }
      params.mainPerson = Number(params.mainPerson);
      params.memberId = Number(params.memberId);
      // params.mailThis = Number(params.mailThis);
      params.sexual = Number(params.sexual);
      params.sexual_text = arrSexual2[Number(params.sexual)];
      params.bornTime = Number(params.bornTime);
      params.bornTime_text = arrTime2[Number(params.bornTime)];

      if(!!params.passTime){
        params.passTime = Number(params.passTime);
        params.passTime_text = arrTime2[Number(params.passTime)];
      }
      params.zodiac_id = Number(params.zodiac_id);
      params.zodiac_text = arrChineseYear2[Number(params.zodiac_id)];

      if(!!params.templeClass){
        params.templeClass_text = TempleClass.findOne(params.templeClass).value;
      }
      if(!!params.templeLevel){
        params.templeLevel_text = TempleLevel.findOne(params.templeLevel).value;
      }

      // console.log(params);
      if (!!params && People.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest People DELETE");
      // console.log(this.bodyParams);
      if (People.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('mgyear', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    if(isEmpty(sortVar))
      sortVar = {order_id: 1};
    // console.log(sortVar);
    return {
      data: Mgyear.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Mgyear.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray1 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      params.order_id = Number(params.order_id);

      if(params.now_use == '-1'){
        var arr = Mgyear.find().fetch();
        for(var i=0; i<arr.length; i++){
         Mgyear.update(arr[i]._id, {$set: {"now_use": "0"} })
        }
      }

      if (Mgyear.insert(params)) {
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray1 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if(params.now_use == '-1'){
        var arr = Mgyear.find().fetch();
        for(var i=0; i<arr.length; i++){
         Mgyear.update(arr[i]._id, {$set: {"now_use": "0"} })
        }
      }

      params.order_id = Number(params.order_id);
      if (Mgyear.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray1 DELETE");
      // console.log(this.bodyParams);
      if (Mgyear.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('pray1', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    if(isEmpty(sortVar))
      sortVar = {order_id: 1};
    // console.log(sortVar);
    return {
      data: Pray1.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Pray1.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray1 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      params.order_id = Number(params.order_id);

      if(params.now_use == '-1'){
        var arr = Pray1.find().fetch();
        for(var i=0; i<arr.length; i++){
        // for(var index in arr){
         Pray1.update(arr[i]._id, {$set: {"now_use": "0"} })
        }
      }
      params.order_id = Pray1.find().count()+1;

      if (Pray1.insert(params)) {
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray1 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

    /*      if(params.now_use == 'true'){
        var arr = Pray1.find().fetch();
        for(var index in arr){
         Pray1.update(arr[index], {$set: {"now_use": "false"} })
        }
      }*/

      if(params.now_use == '-1'){
        var arr = Pray1.find().fetch();
        for(var i=0; i<arr.length; i++){
        // for(var index in arr){
         Pray1.update(arr[i]._id, {$set: {"now_use": "0"} })
        }
      }

      params.order_id = Number(params.order_id);
      if (Pray1.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray1 DELETE");
      // console.log(this.bodyParams);
      if (Pray1.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('pray2', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // console.log(queryVar);
    if(isEmpty(sortVar))
      sortVar = {order_id: 1};
    return {
      data: Pray2.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Pray2.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray2 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      params.order_id = Pray2.find({class_id: params.class_id}).count()+1;
      // params.order_id = Number(params.order_id);
      if (Pray2.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray2 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if(params.now_use == 'true'){
        var arr = Pray2.find().fetch();
        // for(var index in arr){
        for(var i=0; i<arr.length; i++){
         Pray2.update(arr[i], {$set: {"now_use": "false"} })
        }
      }
      params.order_id = Number(params.order_id);
      if (Pray2.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Pray2 DELETE");
      // console.log(this.bodyParams);
      if (Pray2.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('lighting1', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    queryVar.year = Number(queryVar.year);
    // console.log(queryVar);
    if(!!queryVar.nowform) delete queryVar.nowform;

    if(queryVar.type == "1"){ // 光明燈
      queryVar.p1 = "-1";
    }
    else if(queryVar.type == "3"){ // 光明燈
      queryVar.p3 = "-1";
    }
    else if(queryVar.type == "5"){ // 光明燈
      queryVar.p5 = "-1";
    }
    else{
      queryVar = {
        $and: [
          {$or: [
            {p1: "-1"},
            {p3: "-1"},
            {p5: "-1"},
          ]},
          queryVar,
        ]
      };
    }
    if(queryVar.type){
      delete queryVar.type;
    }

    if(isEmpty(sortVar))
      sortVar = {year: 1, p1_num: 1,  p3_num: 1,  p5_num: 1 };
    return {
      data: Lighting1.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Lighting1.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting1 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // params.order_id = Number(params.order_id);
      if (Lighting1.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Lighting1 insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting1 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      // if(params.now_use == 'true'){
      //   var arr = Pray2.find().fetch();
      //   for(var index in arr){
      //    Pray2.update(arr[index], {$set: {"now_use": "false"} })
      //   }
      // }
      // params.order_id = Number(params.order_id);
      if (Lighting1.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting1 DELETE");
      // console.log(this.bodyParams);
      if (Lighting1.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('lighting1add', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // queryVar.year = Number(queryVar.year);
    // console.log(queryVar);

    queryVar.isSave = 1;

    if(isEmpty(sortVar))
      sortVar = {ordernum: -1, year: -1};

    return {
      data: Lighting1.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Lighting1.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting1 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // params.order_id = Number(params.order_id);
      if (Lighting1.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Lighting1 insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting1 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      // if(params.now_use == 'true'){
      //   var arr = Pray2.find().fetch();
      //   for(var index in arr){
      //    Pray2.update(arr[index], {$set: {"now_use": "false"} })
      //   }
      // }
      // params.order_id = Number(params.order_id);
      if (Lighting1.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting1 DELETE");
      // console.log(this.bodyParams);
      if (Lighting1.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('lighting2', {authRequired: false}, {
  get: function () {
    // console.log("Rest lighting2 GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    queryVar.year = Number(queryVar.year);
    // console.log(queryVar);
    //

Lighting2.find({year: 106}, { sort: { p1_num: 1 } }).forEach(function(u) {
    // var arr = People.find({ $and: [{ familyId: u.familyId }, { name: { $exists: 1 } }, { name: u.name }, { bornYear: { $ne: "" } }] }).fetch();
    // console.log(arr[0]);
    // if (!!arr && typeof arr[0] != "undefined") {
        // console.log(arr[0].);
        // var thisdate = arr[0].bornYear + "年" + arr[0].bornMonth + "月" + arr[0].bornDay + "日";
        // if (!!arr[0].bornTime_text && typeof arr[0].bornTime_text != "undefined") {
        //     thisdate += " " + arr[0].bornTime_text + "時"
        // }
    // }
    if(!!u.p1_num && u.p1_num.length > 0 && u.p1_num.indexOf("-") > 0){

        var arr_num = u.p1_num.split("-");
        var new_num = "";
        if(arr_num[1].length == 1){
          new_num = arr_num[0] + "-00" + arr_num[1];
        }
        else if(arr_num[1].length == 2){
          new_num = arr_num[0] + "-0" + arr_num[1];
        }

        if(!!new_num){

          Lighting2.update(u, {
              $set: {
                  "p1_num": new_num,
              }
          });
          console.log("Lighting2 " + u.year + " " + u.people_name + " ori p1_num: " + u.p1_num + " new p1_num: " + new_num);

        }
    }

});

    if(!!queryVar.lightstarttime || !!queryVar.lightendtime){
      queryVar.createdAt = {};
    }

    if(!!queryVar.lightstarttime){
      queryVar.createdAt['$gt'] =  new Date(queryVar.lightstarttime);
      delete queryVar.lightstarttime;
    }
    if(!!queryVar.lightendtime){
      queryVar.createdAt['$lt'] = new Date(queryVar.lightendtime);
      delete queryVar.lightendtime;
    }

    if(!!queryVar.nowform) delete queryVar.nowform;

    if(queryVar.type == "1"){ // 光明燈
      queryVar.p1 = "-1";
      sortVar = {year: 1, p1_num: 1,  p3_num: 1, p5_num: 1, p7_num: 1 };
    }
    else if(queryVar.type == "3"){ // 光明燈
      queryVar.p3 = "-1";
      sortVar = {year: 1, p3_num: 1, p1_num: 1, p5_num: 1, p7_num: 1 };
    }
    else if(queryVar.type == "5"){ // 光明燈
      queryVar.p5 = "-1";
      sortVar = {year: 1, p5_num: 1, p1_num: 1, p3_num: 1, p7_num: 1 };
    }
    else if(queryVar.type == "7"){ // 光明燈
      queryVar.p7 = "-1";
      sortVar = {year: 1, p7_num: 1, p1_num: 1, p3_num: 1,  p5_num: 1 };
    }
    else{
      queryVar = {
        $and: [
          {$or: [
            {p1: "-1"},
            {p3: "-1"},
            {p5: "-1"},
            {p7: "-1"},
          ]},
          queryVar,
        ]
      };
    }
    if(queryVar.type){
      delete queryVar.type;
    }

    if(isEmpty(sortVar))
      sortVar = {year: 1, p1_num: 1,  p3_num: 1,  p5_num: 1 };
    return {
      data: Lighting2.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Lighting2.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting2 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      params.year = Number(params.year);
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // params.order_id = Number(params.order_id);
      if (Lighting2.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Lighting2 insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting2 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);
      params.year = Number(params.year);

      // if(params.now_use == 'true'){
      //   var arr = Pray2.find().fetch();
      //   for(var index in arr){
      //    Pray2.update(arr[index], {$set: {"now_use": "false"} })
      //   }
      // }
      // params.order_id = Number(params.order_id);
      if (Lighting2.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting2 DELETE");
      // console.log(this.bodyParams);
      if (Lighting2.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('lighting2add', {authRequired: false}, { // modal light用的
  get: function () {
    // console.log("Rest lighting2add GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // queryVar.year = Number(queryVar.year);
    // console.log(queryVar);
    queryVar.isSave = Number(queryVar.isSave);

    var l1_id = "";
    if(queryVar.isSave == 1){ // 已有資料的
      // var l1 = Lighting1.find({familyId: queryVar.familyId, isSave: 1});
      // l1_id = l1.fetch()[0]._id;

    }
    else{ // 新建的資料
      var l1 = Lighting1.find({familyId: queryVar.familyId, isSave: 0});

      if(l1.count() != 0){ // 如果有lighting1的話 就把1和2的 都刪掉 (再重建)
        Lighting1.remove(l1.fetch()[0]._id);
        Lighting2.remove({lighting1: l1_id, familyId: queryVar.familyId});
      }

      var arr_p = People.find({familyId: queryVar.familyId, isLive: 1}).fetch();

      //////////////// 建1和2的
      var obj1 = {
        familyId: queryVar.familyId,
        "if_is" : "0",
        "isSave" : 0,
      };
      l1_id = Lighting1.insert(obj1);

      for(var i=0; i<arr_p.length; i++){
        var p = arr_p[i];
        /*
          "year" : 102,
          "light_text" : "光明燈",
          "addr" : "桃園市桃園市區撫街352巷6號",
        */
        var birth = "";
        if(!!p.bornYear && !!p.bornMonth && !!p.bornDay){
          birth = p.bornYear + "年" + p.bornMonth + "月" + p.bornDay + "日";
        }
        var time = "";
        if(!!p.bornTime_text){
          time = p.bornTime_text + "時";
        }
        else{
          time = "吉時";
        }

        var obj2 = {
          lighting1: l1_id,
          people_id: p._id,
          "people_name" : p.name,
          familyId: queryVar.familyId,
          lunar_birth_text: birth,
          lunar_time_text: time,
          addr: p.addr,
        };
        Lighting2.insert(obj2);
      }
      queryVar.lighting1 = l1_id;
    }

    delete queryVar.isSave;
    // console.log("lighting2add final queryVar");
    // console.log(queryVar);

    return {
      data: Lighting2.find(queryVar, {
          sort: sortVar,
          // skip: pageVar.skip,
          // limit: pageVar.limit
        }).fetch(),
      itemsCount: Lighting2.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting2 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // params.order_id = Number(params.order_id);
      if (Lighting2.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Lighting2 insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting2 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      // if(params.now_use == 'true'){
      //   var arr = Pray2.find().fetch();
      //   for(var index in arr){
      //    Pray2.update(arr[index], {$set: {"now_use": "false"} })
      //   }
      // }
      // params.order_id = Number(params.order_id);
      if (Lighting2.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Lighting2 DELETE");
      // console.log(this.bodyParams);
      if (Lighting2.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('light1', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    if(isEmpty(sortVar))
      sortVar = {order_id: 1};
    // console.log(sortVar);
    return {
      data: Light1.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Light1.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Light1 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      params.order_id = Number(params.order_id);
      if (Light1.insert(params)) {

        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Light1 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if(params.now_use == 'true'){
        var arr = Light1.find().fetch();
        for(var i=0; i<arr.length; i++){
         Light1.update(arr[i], {$set: {"now_use": "false"} })
        }
      }
      params.order_id = Number(params.order_id);
      if (Light1.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Light1 DELETE");
      // console.log(this.bodyParams);
      if (Light1.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('light2', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // console.log(queryVar);
    if(isEmpty(sortVar))
      sortVar = {order_id: 1};
    return {
      data: Light2.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Light2.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Light2 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      params.order_id = Number(params.order_id);
      if (Light2.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Light2 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if(params.now_use == 'true'){
        var arr = Light2.find().fetch();
        // for(var index in arr){
        for(var i=0; i<arr.length; i++){
         Light2.update(arr[i], {$set: {"now_use": "false"} })
        }
      }
      params.order_id = Number(params.order_id);
      if (Light2.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Light2 DELETE");
      // console.log(this.bodyParams);
      if (Light2.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('account1', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    if(isEmpty(sortVar))
      sortVar = {order_id: 1};
    // console.log(sortVar);
    return {
      data: Acc1type.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Acc1type.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Acc1type insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      params.order_id = Number(params.order_id);
      if (Acc1type.insert(params)) {

        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Acc1type update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if(params.now_use == 'true'){
        var arr = Acc1type.find().fetch();
        for(var i=0; i<arr.length; i++){
        // for(var index in arr){
         Acc1type.update(arr[i], {$set: {"now_use": "false"} })
        }
      }
      params.order_id = Number(params.order_id);
      if (Acc1type.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Acc1type DELETE");
      // console.log(this.bodyParams);
      if (Acc1type.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('account2', {authRequired: false}, {
  get: function () {
    // console.log("Rest account2 GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    if(isEmpty(sortVar))
      sortVar = {order_id: 1};

    // console.log(queryVar);
    return {
      data: Acc2subject.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Acc2subject.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Acc2subject insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      params.order_id = Number(params.order_id);
      if (Acc2subject.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Acc2subject update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if(params.now_use == 'true'){
        var arr = Acc2subject.find().fetch();
        for(var i=0; i<arr.length; i++){
        // for(var index in arr){
         Acc2subject.update(arr[i], {$set: {"now_use": "false"} })
        }
      }
      params.order_id = Number(params.order_id);
      if (Acc2subject.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Acc2subject DELETE");
      // console.log(this.bodyParams);
      if (Acc2subject.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('prayingtype', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParamsNo(queryVar, pageVar);
    // console.log(queryVar);
    if(isEmpty(sortVar))
      sortVar = {order: 1};
    return {
      data: PrayingType.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: PrayingType.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest PrayingType insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // params.order = Number(params.order);
      params.order = PrayingType.find().count()+1;
      if (PrayingType.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest PrayingType update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

 /*     if(params.now_use == 'true'){
        var arr = PrayingType.find().fetch();
        for(var index in arr){
         PrayingType.update(arr[index], {$set: {"now_use": "false"} })
        }
      }*/
      // params.order_id = Number(params.order_id);
      params.order = Number(params.order);
      if (PrayingType.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest PrayingType DELETE");
      // console.log(this.bodyParams);
      if (PrayingType.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('booking', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // console.log(queryVar);
    return {
      data: Booking.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Booking.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Booking insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // console.log(params);
      if (Booking.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Booking update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if (Booking.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Booking DELETE");
      // console.log(this.bodyParams);
      if (Booking.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('templeclass', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // console.log(queryVar);
    return {
      data: TempleClass.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: TempleClass.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest TempleClass insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // console.log(params);
      if (TempleClass.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest TempleClass update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if (TempleClass.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest TempleClass DELETE");
      // console.log(this.bodyParams);
      if (TempleClass.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('templelevel', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // console.log(queryVar);
    return {
      data: TempleLevel.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: TempleLevel.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest TempleLevel insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // console.log(params);
      if (TempleLevel.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest TempleLevel update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if (TempleLevel.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest TempleLevel DELETE");
      // console.log(this.bodyParams);
      if (TempleLevel.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('cashier', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // console.log(queryVar);
    return {
      data: Booking.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Booking.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Booking insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      // console.log(params);
      if (Booking.insert(params)) {
        // return params;
        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Booking update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

      if (Booking.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Booking DELETE");
      // console.log(this.bodyParams);
      if (Booking.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('clientlist', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};

    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);


    var object = Meteor.users.find(queryVar, {
      sort: sortVar,
      skip: pageVar.skip,
      limit: pageVar.limit
    }).fetch()

    var new_obj = [];
    for(var i=0; i<object.length; i++){
    // for(var index in object) {
        var attr = object[i];

        var d1 = attr.services.resume.loginTokens[attr.services.resume.loginTokens.length-1].when;
        var d2 = attr.createdAt;

        var obj = {
          _id: attr._id,
          // uid: attr.uid,
          // team: attr.profile.team,
          username: attr.username,
          email: attr.emails[0].address,
          name: attr.profile.name,
          // name2: attr.profile.name2,
          roles: attr.profile.roles,
          // cms1: attr.profile.cms1,
          // cms2: attr.profile.cms2,
          // cms3: attr.profile.cms3,
          loginedAt: Api.mydate(d1),
          createdAt: Api.mydate(d2),
        }
        new_obj.push(obj);

    }

    // console.log(new_obj);

    // console.log("queryVar: ");
    // console.log(queryVar);
    // console.log("sortVar: ");
    // console.log(sortVar);
    // console.log("pageVar: ");
    // console.log(pageVar);

    return {
      data: new_obj,
      itemsCount: Meteor.users.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest User insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('provider');
      // params.uid = getNextSequence('users');
      // params.fullname = params.countryname+" - "+params.value;
    /*  if (Teams.insert(params)) {

        return {insert: 'success', data: {message: 'Teams insert'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };*/
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest User update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      // var id = params._id;
      // delete params._id;
      // console.log(params);
      // if (Teams.update(id, {$set: params })) {
      Meteor.users.update(params._id, {$set: {
          "username": params.username,
          // "emails[0].address": params.email,
          "profile.name": params.name,
          // "profile.name2": params.name2,
          "profile.team": params.team,
          // "profile.cms1": params.cms1,
          // "profile.cms2": params.cms2,
          // "profile.cms3": params.cms3,
          "profile.roles": params.roles,
      }});

      var arrRoles = [];
      if(params.roles == 10){
        arrRoles.push('agent');
      }
      else if(params.roles == 20){
        arrRoles.push('agent');
        arrRoles.push('agentleader');
      }
      else if(params.roles == 30){
        arrRoles.push('commission');
      }
      else if(params.roles == 40){
        arrRoles.push('commission');
        arrRoles.push('director');
      }
      else if(params.roles == 50){
        arrRoles.push('agent');
        arrRoles.push('commission');
        arrRoles.push('director');
        arrRoles.push('supervisor');
      }
      else if(params.roles == 90){
        arrRoles.push('administrative');
      }
      else if(params.roles == 95 || params.roles == 99){
        arrRoles.push('agent');
        arrRoles.push('agentleader');
        arrRoles.push('commission');
        arrRoles.push('director');
        arrRoles.push('supervisor');
        arrRoles.push('administrative');
        arrRoles.push('admin');
      }

      // Roles.addUsersToRoles(params._id, arrRoles);
      Roles.setUserRoles(params._id, arrRoles);

      if (1) {

        // var d1 = params.services.resume.loginTokens[params.services.resume.loginTokens.length-1].when;
        // var d2 = params.createdAt;

        var obj = {
          _id: params._id,
          // uid: params.uid,
          team: params.team,
          username: params.username,
          email: params.email,
          name: params.name,
          // name2: params.name2,
          roles: params.roles,
          // cms1: params.cms1,
          // cms2: params.cms2,
          // cms3: params.cms3,
          loginedAt: new Date( params.loginedAt ),
          createdAt: new Date( params.createdAt ),
          message: 'User updated'
        }
        // console.log(obj);
        // return { status: 'success', data: obj };
        return obj;
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
 /* delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      console.log("Rest Teams DELETE");
      // console.log(this.bodyParams);
      if (Teams.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },*/
});
Api.addRoute('praying1', {authRequired: false}, {
  get: function () {
    // console.log("Rest Teams GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    // console.log(queryVar);

    if(queryVar.year){
      queryVar.year = Number(queryVar.year);
    }

    if(!! queryVar.paystatus ){
      if(queryVar.paystatus == "2"){ // 已付清
        queryVar.cash_balance = 0;
      }
      else if(queryVar.paystatus == "1"){ // 未付清
        queryVar.cash_balance = { $nin: [ "", 0 ] };
      }
      delete queryVar.paystatus;
    }
    if(!! queryVar.cashier ){
      delete queryVar.cashier;
      queryVar = {
        $and: [
          { cash_paid: {$exists: true }},
          { cash_balance: {$exists: true }},
          queryVar,
        ]
      }
    }

    if(isEmpty(sortVar))
      sortVar = {ordernum: -1};
    return {
      data: Praying1.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch(),
      itemsCount: Praying1.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Praying1 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      params.ordernum = "R"+funcPad(getNextSequence('praying2'), 8);

      Pray2.update(params.prayitem, {$set: {now_num: params.prayserial} })

      if(params.year){
        params.year = Number(params.year);
      }

      var id = "";
      if (id = Praying1.insert(params)) {
        // return params;
        params._id = id;
        return {insert: 'success', data: params};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Praying1 update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);

 /*     if(params.now_use == 'true'){
        var arr = Praying1.find().fetch();
        for(var index in arr){
         Praying1.update(arr[index], {$set: {"now_use": "false"} })
        }
      }*/
      // params.order_id = Number(params.order_id);
      // params.order = Number(params.order);
      if (Praying1.update(id, {$set: params })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Praying1 DELETE");
      // console.log(this.bodyParams);
      if (Praying1.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('praying2', {authRequired: false}, {
  get: function () {
    // console.log("Rest praying2 GET");
    var queryVar, sortVar = {}, pageVar = {};

    queryVar = this.queryParams || {};

    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);

    if(!!queryVar.year){
      queryVar.year = Number(queryVar.year);
    }

    if(!sortVar.year) sortVar.year = 1;
 /*
    var order = queryVar.order || "1";
    delete queryVar.order;*/

    // sortVar.pray2_orderid = 1;
    sortVar.prayserial = 1;
    sortVar.prayingtype_orderid = 1;
    sortVar.pass_order = 1;

    if(!!queryVar.nowform) delete queryVar.nowform;

    if(!!queryVar.praystarttime || !!queryVar.prayendtime){
      queryVar.createdAt = {};
    }

    if(!!queryVar.praystarttime){
      queryVar.createdAt['$gt'] =  new Date(queryVar.praystarttime);
      delete queryVar.praystarttime;
    }
    if(!!queryVar.prayendtime){
      queryVar.createdAt['$lt'] = new Date(queryVar.prayendtime);
      delete queryVar.prayendtime;
    }

    // console.log(queryVar);
    // console.log("sortVar:");
    // console.log(sortVar);
    // console.log("pageVar:");
    // console.log(pageVar);

    var arr = Praying2.find(queryVar, {
          sort: sortVar,
          skip: pageVar.skip,
          limit: pageVar.limit
        }).fetch();

   /* if(order == "3"){
      var temArr = [];
      for (var i = 0; i < arr.length; i++) {
        if(!!arr[i].passname_text){
          temArr.push(arr[i]);
        }
      }
      for (var i = 0; i < arr.length; i++) {
        if(!!arr[i].passname1_text){
          temArr.push(arr[i]);
        }
      }
      for (var i = 0; i < arr.length; i++) {
        if(!!arr[i].passname2_text){
          temArr.push(arr[i]);
        }
      }
      for (var i = 0; i < arr.length; i++) {
        if(!!arr[i].passname3_text){
          temArr.push(arr[i]);
        }
      }
      for (var i = 0; i < arr.length; i++) {
        if(!!arr[i].passname4_text){
          temArr.push(arr[i]);
        }
      }
      arr = temArr;
    }*/
    return {
      data: arr,
      itemsCount: Praying2.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Praying2 insert POST");
      // console.log(this.bodyParams);

      var params = this.bodyParams;
      // params.uid = getNextSequence('cmsget');
      // params.fullname = params.countryname+" - "+params.value;
      params.year = Number(params.year);
      params.prayserial = Number(params.prayserial);

      var obj2 = Pray2.findOne(params.prayitem);
      var obj3 = PrayingType.findOne(params.type);
      params.pray2_orderid = Number(obj2.order_id);
      params.prayingtype_orderid = Number(obj3.order);

      // 加總所有praying2的價錢 回寫praying1的價錢
      var total = Number(params.row_total);

      var p = Praying1.findOne( params.listId );

      Praying2.find({listId: params.listId}).map(function(doc) {
        total += Number(doc.row_total);
      });

      if(typeof p.cash1 == "undefined"){
        p.cash1 = p.cash;
      }
      var p1 = {
        "cash2": Number(total), // 外加
        "cash": Number(total) + Number(p.cash1) //總和
      };
      Praying1.update( params.listId, {$set: p1 } );

      // console.log(params);

      // console.log("p1");
      // console.log(p1);
      // console.log("p2 post params");
      // console.log(params);
      if (Praying2.insert(params)) {
        params.p1 = p1;
        return params;
        // return {insert: 'success', data: params, p1: p1};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      console.log("Rest Praying2 update PUT");
      console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;

      var cBa = PrayingType.findOne({_id: params.type});
      if(!!cBa){
        params.type_text = cBa.value;
        params.prayingtype_orderid = cBa.order;

        if(cBa.value == "拔"){
          if(!!params.passname_text){
            params.pass_order = 0;
          }
          else if(!!params.passname_text1){
            params.pass_order = 1;
          }
          else if(!!params.passname_text2){
            params.pass_order = 2;
          }
          else if(!!params.passname_text3){
            params.pass_order = 3;
          }
          else if(!!params.passname_text4){
            params.pass_order = 4;
          }
        }
      }

      params.pray2_orderid = 0;
      if(!!Pray2.findOne({_id: params.prayitem}) && typeof Pray2.findOne({_id: params.prayitem}).order_id != "undefined"){
        // console.log("Pray2.findOne({_id: "+params.prayitem+"})");
        // console.log(Pray2.findOne({_id: params.prayitem}));
        params.pray2_orderid = Pray2.findOne({_id: params.prayitem}).order_id;
      }

      // var obj2 = Pray2.findOne(params.prayitem);
      // var obj3 = PrayingType.findOne(params.type);
      // params.pray2_orderid = Number(obj2.order_id);
      // params.prayingtype_orderid = Number(obj3.order);

      params.year = Number(params.year);
      params.prayserial = Number(params.prayserial);

/*
      var p1 = {
        "cash1": params.p1_cash1,
        "cash2": params.p1_cash2,

      var p1 = {
        "cash1": params.p1_cash1,
        "cash2": params.p1_cash2,
        "cash": params.p1_cash
      };
      // console.log("Praying2 put params");
      // console.log(params);

      delete params.p1_cash1;
      delete params.p1_cash2;
      delete params.p1_cash;
*/
      // 讀所有praying2的價錢 來寫回praying1價錢的值
      var total = Number(params.row_total);

      Praying2.find({listId: params.listId}).map(function(doc) {
        if(id != doc._id)
          total += Number(doc.row_total);
      });

      var p = Praying1.findOne( params.listId );
      // console.log(p);
      if(typeof p.cash1 == "undefined"){
        p.cash1 = p.cash;
      }
      var p1 = {
        "cash2": Number(total), // 細項
        "cash": Number(total) + Number(p.cash1) //總和
      };
      Praying1.update(params.listId, {$set: p1 });
      params.cash = Number(total);

      // console.log("p2 put params");
      // console.log(params);
      // console.log("p1");
      // console.log(p1);

      if (Praying2.update(id, {$set: params })) {
        params._id = id;
        params.p1 = p1;
        // return {insert: 'success', data: params, p1: p1};
        return params;
        // return {status: 'success', data: {message: 'Teams updated'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found', input: this.bodyParams, data: params}
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Praying2 DELETE");
      // console.log(this.bodyParams);
      if (Praying2.remove(this.bodyParams._id)) {
        return {status: 'success', data: {message: 'Teams removed'}};
      }
      return {
        statusCode: 404,
        body: {status: 'fail', message: 'Article not found'}
      };
    }
  },
});
Api.addRoute('bill', {authRequired: false},{
  get: function () {
    var queryVar, sortVar = {}, pageVar = {};
    queryVar = this.queryParams || {};
    // console.log(this.request);
    // console.log(this.queryParams);
    // console.log(this.urlParams);

  /*
  { date: '2016/6/1',
    date_submit: '',
    year: '105',
    whoapply: 'xAjpAHv6qaBSuoPup',
    prayname: 'nqnw6qAPFwJHbYR5L',
    prayitem: 'WC46TzrPgupuwpTAk',
    prayserial: '15',
    cash: '50000' }
   */
    var objP1 = Praying1.findOne(queryVar.p1_id)
    var objPray1 = Pray1.findOne(objP1.prayname)
    var objPeople = People.findOne(objP1.whoapply)

    var doc = new PDFDocument({size: 'A4', margin: 1});
    doc.registerFont('BiauKai', BiauKaiTTF);


    doc.lineWidth(1);
    doc.lineJoin('miter').rect(25, 25, 550, 350).stroke();

    doc.font('BiauKai');
    doc.fontSize(26);
    doc.text("財團法人桃園佛教蓮社", 170, 40);
    doc.fontSize(20);
    // var pray1 = "";
    // if(Pray1.findOne(queryVar.prayname)){
    //   pray1 = Pray1.findOne(queryVar.prayname).value;
    // }
    doc.text("傳授護國 "+ objPray1.value +" 繳款單", 180, 70);
    doc.fontSize(16);
    var d = new Date()
    var now_year = d.getFullYear() - 1911
    var now_month = d.getMonth() + 1
    var now_date = d.getDate()
    Api.strChtEngMix(doc, "中華民國" + now_year + "年 " + now_month + "月 " + now_date + "日", 390, 98, 16);
    doc.font('BiauKai')
    // doc.font('BiauKai').text("感謝狀", 100, 100);
    // doc.text("繳款單", 130, 100);
    doc.fontSize(16);
    // var name = "", addr = "";
    // if(People.findOne(queryVar.whoapply)){
    //   var p = People.findOne(queryVar.whoapply);
    //   name = p.name;
    //   addr = p.addr;
    // }
    doc.text("茲承 ", 50, 120);
    doc.text("大德", 500, 120);
    doc.fontSize(20);
    doc.text(objPeople.name, 100, 118);

    doc.fontSize(16);
    doc.text("住址", 50, 160);
    Api.strChtEngMix(doc, objPeople.addr, 100, 160, 16);
    doc.font('BiauKai')
    // doc.text(objPeople.addr, 100, 160);
    doc.text("護戒功德金新台幣 " + numToChinese(objP1.cash) + "元整", 50, 200);


    doc.fontSize(18);
    doc.text("護持戒法 功德無量 謹以感恩之心 誠致謝意 並祝 福慧增長", 35, 267);
    doc.fontSize(14);
    Api.strChtEngMix(doc, "地址：桃園市桃園區朝陽街12號", 50, 290, 14)
    doc.font('BiauKai')
    // doc.text("地址：桃園市桃園區朝陽街12號", 50, 290);
    doc.text("電話：", 50, 304);
    doc.text("統一編號：", 50, 316);
    doc.text("桃園市政府", 50, 328);
    doc.text("核准設立財團法人", 50, 340);

    var qr = QRimage.imageSync("http://asdf.com", {
      type: 'png',
      ec_level: 'M',
      size: 15,
      margin: 0,
      parse_url: true
    });
    doc.image(qr, 500, 35,{ height: 55 });

    return Api.returnPDF(doc);
  }
});
Api.addRoute('receipt', {authRequired: false},{
  get: function () {
    var queryVar, sortVar = {}, pageVar = {};
    queryVar = this.queryParams || {};
    
    var objP1 = Praying1.findOne(queryVar.p1_id)
    if (Number(objP1.cash_paid) == 0) {
      return "尚未有付款紀錄"
    }
    var objPeople = People.findOne(objP1.whoapply)
    var objBooking = Booking.findOne({ ordernum: objP1.ordernum }, { sort: { paid_date: 1 }})

    var doc = new PDFDocument({size: 'A4', margin: 1});
    doc.registerFont('BiauKai', BiauKaiTTF);


    doc.lineWidth(1);
    var red = "#B22222";
    var black = "#000";
    doc.strokeColor(red);
    doc.lineJoin('miter').rect(25, 25, 550, 350).stroke();

    doc.fillColor(red);

    doc.font('BiauKai');
    doc.fontSize(22);
    doc.text("財團法人桃園佛教蓮社捐款收據", 40, 38);

    doc.moveTo(25, 70).lineTo(480, 70).stroke();

    doc.fontSize(16);
    doc.text("地 址", 40, 77); // 12號
    Api.strChtEngMix(doc, "桃園市桃園區朝陽街12號", 110, 77, 16)
    doc.font('BiauKai')
    // doc.text("桃園市桃園區朝陽街 號", 110, 77); // 12號

    doc.text("電話", 323, 77);
    doc.font('Times-Roman');
    doc.text("(03) 3397021-2", 365, 79);


    doc.font('Times-Roman');
    // doc.text("12", 253, 79); // 12號
    doc.moveTo(25, 100).lineTo(575, 100).stroke();

    doc.font('BiauKai');
    doc.text("捐款號", 40, 108);
    doc.fillColor(black);
    doc.font('Times-Roman');
    doc.text(objP1.ordernum, 110, 108);
    doc.font('BiauKai');
    doc.fillColor(red);
    doc.moveTo(25, 130).lineTo(575, 130).stroke();

    // 直的線
    doc.moveTo(100, 70).lineTo(100, 260).stroke();
    doc.moveTo(310, 70).lineTo(310, 130).stroke();
    doc.moveTo(480, 25).lineTo(480, 100).stroke();

    doc.moveTo(400, 130).lineTo(400, 200).stroke();
    doc.moveTo(448, 130).lineTo(448, 200).stroke();

    doc.text("班別", 410, 139);
    doc.fillColor(black);
    doc.text(objPeople.templeClass_text, 460, 139);
    doc.text(objPeople.templeLevel_text, 500, 139);
    doc.fillColor(red);
    doc.moveTo(400, 165).lineTo(575, 165).stroke();
    doc.text("號碼", 410, 173);
    doc.fillColor(black);
    doc.font('Times-Roman');
    doc.text(objPeople.memberId, 460, 177);
    doc.font('BiauKai');
    doc.fillColor(red);


    /*    var pray1 = "";
        if(Pray1.findOne(queryVar.prayname)){
          pray1 = Pray1.findOne(queryVar.prayname).value;
        }
        doc.text("傳授護國 "+ pray1 +" 感謝狀", 180, 70);
    */
    var bookingDate = new Date(objBooking.paid_date)
    var booking_year = bookingDate.getFullYear() - 1911
    var booking_month = bookingDate.getMonth() + 1
    var booking_date = bookingDate.getDate()

    doc.fontSize(16);
    doc.text("收款日期", 323, 108);
    doc.font('Times-Roman');
    doc.fillColor(black);
    Api.strChtEngMix(doc, booking_year + "年 " + booking_month + "月 " + booking_date + "日", 390, 108, 16);
    doc.font('BiauKai')
    // doc.text(bookingDate.getFullYear(), 400, 110);
    // doc.font('BiauKai');
    // doc.text("年", 440, 108);
    // doc.font('Times-Roman');
    // doc.text(bookingDate.getMonth() + 1, 475, 110);
    // doc.font('BiauKai');
    // doc.text("月", 490, 108);
    // doc.font('Times-Roman');
    // doc.text(bookingDate.getDate(), 530, 110);
    // doc.font('BiauKai')
    // doc.text("日", 550, 108);
    doc.fillColor(red);
    
    // doc.font('BiauKai').text("感謝狀", 100, 100);
    // doc.text("繳款單", 130, 100);
    doc.fontSize(16);
    var name = "", addr = "";
    if(People.findOne(queryVar.whoapply)){
      var p = People.findOne(queryVar.whoapply);
      name = p.name;
      addr = p.addr;
    }
    doc.text("捐款人 ", 40, 157);
    doc.fillColor(black);
    doc.text(objP1.whoapply_text, 110, 157);
    doc.fillColor(red);
    doc.moveTo(25, 200).lineTo(575, 200).stroke();
    
    // doc.text("大德", 500, 120);
    doc.fontSize(20);
    doc.text(name, 110, 157);

    doc.fontSize(16);
    doc.text("住 址 ", 40, 207);
    doc.fillColor(black);
    Api.strChtEngMix(doc, objPeople.addr, 110, 207, 16);
    doc.font('BiauKai')
    doc.fillColor(red);
    doc.moveTo(25, 230).lineTo(575, 230).stroke();

    doc.text("金 額", 40, 237);

    doc.fillColor(black);
    doc.text("新台幣 " + numToChinese(objBooking.cash_paid) + "元整", 110, 237); //    萬   仟   佰   拾
    // doc.text(numToChinese(objBooking.cash_paid) + "元整", 160, 237);
    doc.fillColor(red);
    doc.moveTo(25, 260).lineTo(575, 260).stroke();

    doc.text("董事長：     住持：    會計：     收款人：", 40, 280);
    doc.moveTo(25, 315).lineTo(575, 315).stroke();

    doc.fontSize(18);
    // doc.text("護持戒法 功德無量 謹以感恩之心 誠致謝意 並祝 福慧增長", 35, 267);
    doc.fontSize(13);
    doc.text("一、桃園縣政府    第   號核准設立財團法人。", 40, 322);
    doc.text("二、扣免繳統一編號：第    號", 40, 338);
    doc.text("第一聯：會計存根 第二聯：捐款人收執", 40, 353);

    doc.fontSize(16);
    doc.text("第一聯 會計", 440, 335);


    var qr = QRimage.imageSync("http://asdf.com", {
      type: 'png',
      ec_level: 'M',
      size: 15,
      margin: 0,
      parse_url: true
    });
    doc.image(qr, 500, 35,{ height: 55 });

    return Api.returnPDF(doc);
  }
});
Api.addRoute('uploads', {authRequired: false},{
  get: function () {
    // console.log(this.request);
    // console.log(this.queryParams);
    // console.log(this.urlParams);

    queryVar = this.queryParams || {};
    // file = req.params.file;
    var filename = process.cwd() + '/../web.browser/app/'+queryVar.file+'.pdf';
    // console.log(filename);

    var fs = require('fs');
    var content = fs.readFileSync(filename);

    // var dirname = "/home/rajamalw/Node/file-upload";
    // var img = fs.readFileSync(dirname + "/uploads/" + file);
    // res.writeHead(200, {'Content-Type': 'image/jpg' });
    // res.end(img, 'binary');
    // return "aaa";

    return {
      headers: { 'Content-Type': 'application/pdf' },
      body: content
    };
  }
});
Api.strChtEngMix = function(doc, str, x, y, fontsize){
  doc.fontSize(fontsize);
  var top_shift = Math.ceil(fontsize/10);
  var now_x = x;
  var now_y = y;
  for(var i=0; i< str.length; i++){
    var nowchar = str.charAt(i);

    if(nowchar.Blength() == 1){// 如果是英文或數字
      if(nowchar == "\n"){
        now_y = doc.y+fontsize;
        now_x = x;
      }
      else{
        doc.font('Helvetica').text(nowchar, now_x, now_y+top_shift);
        now_x = doc.x;
      }
    }
    else{ // 如果是中文      
      // doc.font('msjh').text(nowchar, now_x, now_y-4);
      // doc.font('BiauKai').text(nowchar, now_x, now_y-4);
      doc.font('BiauKai').text(nowchar, now_x, now_y);
      if(doc.x-now_x != fontsize){
        now_x = doc.x+fontsize;
      }
      else{
        now_x = doc.x;
      }
    }
  }
  doc.font('Helvetica');
  // now_x = doc.x;
  // doc.y = now_y;
}
