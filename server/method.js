funcNextLightNum = function(name){
  function retFitLength(ref_str, mod_str){
    var diff = ref_str.length - mod_str.length;
    // console.log("retFitLength");
    // console.log(ref_str);
    // console.log(mod_str);
    // console.log(diff);

    if(diff > 0){
      var zero_str = "";
      for (var i = 0; i < diff; i++) {
        zero_str = zero_str + "0";
      }
      return zero_str + mod_str;
    }
    else{
      return mod_str;
    }

  }

  // console.log("funcNextLightNum");
  // console.log(name);
  // var l = Light1.find({value: "光明燈"});
  var l = Light1.find({value: name});
  if(l.count() == 0){
    return "沒有對應的燈號";
  }

  var now_id = l.fetch()[0].now_id;

  // console.log("l.fetch()[0]");
  // console.log(l.fetch()[0]);

  // 建檔的資料
  var arr_l2 = Light2.find({type_id: l.fetch()[0]._id}, {sort: {order_id: 1}}).fetch();

  // console.log("arr_l2");
  // console.log(arr_l2);
  // console.log("now_num");
  // console.log(now_num);
  var ret = "";

  if(now_id == ""){   // 如果原本的燈號是空的 用最前面的一個就可以了
    ret = arr_l2[0].name+"-"+retFitLength(arr_l2[0].end_num, arr_l2[0].start_num);
    // console.log(arr_l2[0]);
    // console.log(ret);
  }
  else{
    var arr = now_id.split("-");
    // console.log("arr");
    // console.log(arr);

    for(var i=0; i<arr_l2.length; i++){
      if(arr_l2[i].name == arr[0]){
        // console.log("find!");
        // console.log(arr_l2[i]);
        var num = Number(arr[1])+1;
        if(num <= Number(arr_l2[i].end_num)){
          ret = arr_l2[i].name+"-"+retFitLength(arr_l2[i].end_num, num);
        }
        else{
          var j = Number(i)+1;
          ret = arr_l2[j].name + "-" + retFitLength(arr_l2[j].end_num, arr_l2[j].start_num);
        }
        break;
      }
    }
  }
  // console.log("ret: "+ret);

  Light1.update(l.fetch()[0]._id, {$set: {now_id: ret}});
  return ret;
}

Meteor.methods({
  chgAddrsLive: function(familyId, post5code, addr){
    var currentUserId = Meteor.userId();
    if(currentUserId){

      var params = {};
      params.addr = addr;
      params.post5code = post5code;
      People.update({familyId: familyId}, {$set: params }, {multi: true});
    }
    return 0;
  },
  addMultiPraying2: function(arr_name, formVar){
    var currentUserId = Meteor.userId();
    if(currentUserId){

      for(var i in arr_name){
        var name = arr_name[i];
        var addr = People.find({familyId: formVar.familyId, name: name, isLive:1}).fetch()[0].addr;

        formVar.livename_text  = name;
        formVar.addr           = addr;

        Praying2.insert(formVar);
      }

      var p = Praying1.findOne( formVar.listId );
      if(typeof p.cash1 == "undefined"){
        p.cash1 = p.cash; // 法會
      }

      var total = 0;
      Praying2.find({listId: formVar.listId}).map(function(doc) {
        total += Number(doc.row_total);
      });

      var p1 = {
        "cash2": Number(total), // 外加
        "cash": Number(total) + Number(p.cash1) //總和
      };
      Praying1.update( formVar.listId, {$set: p1 } );

      return p1;
    }
    return 1;
  },
  closeAccount: function(isClose, ordernum){
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var str = "未關帳";
      if(isClose == 1){
        str = "已關帳";
      }
      Praying1.update({ordernum: ordernum},
        {$set: {
          isClose: isClose,
          isClose_text: str
        }});
    }
    return 1;
  },
  returnAgents: function(){
    // var results = [];
    var results = Roles.getUsersInRole(['agent']).map(function(user, index, originalCursor){
      var result = {
        _id: user._id,
        emails: user.emails,
        name: user.profile.name,
        createdAt: new Date(user.createdAt),
        roles: user.roles
      };
      // console.log("result: ", result);
      return result;
    });
    // console.log("all results - this needs to get returned: ", results);
    return results;
  },
  'updateChairName': function(name) {
    var currentUserId = Meteor.userId();
    if(currentUserId){

      if(System.find({"chairname": 1}).count() == 0){
        System.insert({"chairname": 1, "value": name});
      }
      else{
        var id = System.find({"chairname": 1}).fetch()[0];
        System.update(id, {$set:{"value": name}});
      }
    }
    return 1;
  },
  'deleteFamily': function(id) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var arr = People.find({"familyId": id}).fetch();
        for(var index in arr){
          People.remove({_id: arr[index]._id});
          console.log("People delete  " +arr[index].name);
      }
      // console.log("family delete ok " +id);
    }
    return 1;
  },
  'insertPraying1': function(formVar) {
    var currentUserId = Meteor.userId();
    if(currentUserId){

      formVar.ordernum = "R"+funcPad(getNextSequence('praying1'), 8);

      formVar.cash_paid = "0";
      // formVar.cash_balance = formVar.cash;
      formVar.year = Number(formVar.year);
      formVar._id =  Praying1.insert(formVar);

      Pray2.update(formVar.prayitem, {$set: { now_num: formVar.prayserial}});

      return formVar;
    }
    return "1";
  },
  'dupPraying1': function(formVar) {
    var currentUserId = Meteor.userId();
    if(currentUserId){

      var obj = Praying1.findOne({_id: formVar._id});
      delete obj._id;
      delete obj.ori_id;
      delete obj.ori_family_id;
      delete obj.prayserial;
      delete obj.createdAt;
      delete obj.updatedAt;

      var id = "";
/// insertPraying1 start
      obj.ordernum = "R"+funcPad(getNextSequence('praying1'), 8);
      obj.cash_paid = "0";
      // obj.cash_balance = formVar.cash;
      obj.year = Number(currentMGyear());
      obj.date = currentdate();

      var p2 = Pray2.findOne({_id: obj.prayitem});
      obj.prayserial = Number(p2.now_num)+1;
      Pray2.update(obj.prayitem, {$set: { now_num: obj.prayserial}});

      id =  Praying1.insert(obj);
      // console.log("dup Praying1 ok " +id);
      // console.log(obj);
/// insertPraying1 end

      var arr = Praying2.find({"listId": formVar._id}).fetch();
      for(var i in arr){
        var obj2 = arr[i];
        delete obj2._id;
        delete obj2.ori_id;
        delete obj2.createdAt;
        delete obj2.updatedAt;

        obj2.listId = id;
        obj2.year = obj.year;
        obj2.prayserial = Number(obj.prayserial);
        obj2.ordernum = obj.ordernum;

        obj2.pray2_orderid = Number(p2.order_id);

        var pt = PrayingType.findOne(obj2.type);
        obj2.prayingtype_orderid = Number(pt.order);

        Praying2.insert(obj2);

        // console.log("dup Praying2");
        // console.log(obj2);
      }

      return obj;
    }
    return "1";
  },
  'updatePraying1': function(formVar) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var id = formVar._id;
      delete formVar._id;
      formVar.year = Number(formVar.year);
      Praying1.update(id, {$set: formVar});

      Pray2.update(formVar.prayitem, {$set: { now_num: formVar.prayserial}});

      return formVar;
    }
    return 1;
  },
  'deletePraying1': function(id) {
    var currentUserId = Meteor.userId();
    if(currentUserId){

      Praying2.remove({"listId": id});
      Praying1.remove({"_id": id});
      // console.log("family delete ok " +id);
    }
    return 1;
  },
  'deleteLighting1': function(id) {
    var currentUserId = Meteor.userId();
    if(currentUserId){

      Lighting2.remove({"lighting1": id});

      Lighting1.remove({"_id": id});
      // console.log("family delete ok " +id);
    }
    return 1;
  },
  'updateLighting1': function(formVar) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      // console.log("updateLighting1 ok");
      // var id = formVar._id;
      // delete formVar._id;
      /*
LIGHTING1

date: "2016/5/12",
date_submit: "",
year: "105",
apply_id: "22Kg9R5NunT4obJde",
price_total: "20000"
apply_text:"黃美梅"
familyId:"6eojK59EHG9KYGdcx"
isSave:1
price_total:"20000"
year:"105"
       */

      var isArray = 1;
      var l1_id;
      if(typeof formVar.lighting1 == "string"){
        l1_id = formVar.lighting1; // 只有一列的情況
        isArray = 0;
      }
      else{
        l1_id = formVar.lighting1[0]; // 所有的這個都一樣
      }

      var l1 = Lighting1.findOne(l1_id);
      // console.log(formVar);

      var ordernum = "";
      if(l1.isSave == 0){
        ordernum = "H"+funcStrPad(getNextSequence('Lighting1'), 8);
      }
      else{
        ordernum = l1.ordernum;
      }

      var obj1 = {
        ordernum: ordernum,
        date: formVar.date,
        date_submit: formVar.date_submit,
        year: formVar.year,
        apply_id: formVar.apply_id,
        apply_text: formVar.apply_text,
        price_total: formVar.price_total,
        familyId: formVar.familyId,
        isSave: formVar.isSave,
        price_total: formVar.price_total,
        year: Number(formVar.year),
      }
      Lighting1.update(l1_id, {$set: obj1});

///////// Lighting2

    /*
    LIGHTING2

      id:Array[8]
      lighting1:Array[8]
      p1:Array[2]
      p1_2:"eNrNuCsBuBR5Bhkrz"
      p1_isallfamily:"cqiBNd5uEramuNmwx"
      p1_name2:Array[8]
      p1_num:Array[8]
      p3:"5aN4qQ99F9yL2DETe"
      p3_isp3:Array[2]
      p3_name2:Array[8]
      p3_num:Array[8]
      p5:Array[2]
      p5_isp5:Array[2]
      p5_name2:Array[8]
      p5_num:Array[8]
      people_name:Array[8]
     */
      function strarrFindId(strarr, id){
        if(typeof strarr === "string"){
          if(strarr == id){
            return "-1";
          }
        }
        else{
          for(var j in strarr){
            if(strarr[j] == id){
              return "-1";
            }
          }
        }
        return "0";
      }

      // console.log(formVar);

      var arr_num = [];
      var light_text = "";

      if(isArray){
        for(var i=0; i<formVar.id.length; i++){
          var id = formVar.id[i];

          var p1             = strarrFindId(formVar.p1, id);
          var p1_2           = strarrFindId(formVar.p1_2, id);
          var p1_isallfamily = strarrFindId(formVar.p1_isallfamily, id);
          var p3             = strarrFindId(formVar.p3, id);
          var p3_isp3        = strarrFindId(formVar.p3_isp3, id);
          var p5             = strarrFindId(formVar.p5, id);
          var p5_isp5        = strarrFindId(formVar.p5_isp5, id);
          var p7             = strarrFindId(formVar.p7, id);
          var p7_isp7        = strarrFindId(formVar.p7_isp7, id);


          var p1_num = formVar.p1_num[i];
          if(p1 == "-1" && !p1_num){
            p1_num = funcNextLightNum("光明燈");
            light_text = "光明燈";
          }
          var p3_num = formVar.p3_num[i];
          if(p3 == "-1" && !p3_num){
            p3_num = funcNextLightNum("藥師燈");
            light_text = "藥師燈";
          }
          var p5_num = formVar.p5_num[i];
          if(p5 == "-1" && !p5_num){
            p5_num = funcNextLightNum("光明大燈");
            light_text = "光明大燈";
          }
          var p7_num = formVar.p7_num[i];
          if(p7 == "-1" && !p7_num){
            p7_num = funcNextLightNum("藥師大燈");
            light_text = "藥師大燈";
          }

          if(p1 == "-1"){
            light_text = "光明燈";
          }
          if(p3 == "-1"){
            light_text = "藥師燈";
          }
          if(p5 == "-1"){
            light_text = "光明大燈";
          }
          if(p7 == "-1"){
            light_text = "藥師大燈";
          }

          var obj2 = {
            p1: p1,
            p1_2: p1_2,
            p1_isallfamily: p1_isallfamily,
            p3: p3,
            p3_isp3: p3_isp3,
            p5: p5,
            p5_isp5: p5_isp5,
            p7: p7,
            p7_isp7: p7_isp7,
            p1_name2:formVar.p1_name2[i],
            p1_num:  p1_num,
            p3_name2:formVar.p3_name2[i],
            p3_num:  p3_num,
            p5_name2:formVar.p5_name2[i],
            p5_num:  p5_num,
            p7_name2:formVar.p7_name2[i],
            p7_num:  p7_num,
            year: Number(formVar.year),
            light_text: light_text
          }

          // console.log(obj2);
          Lighting2.update(id, {$set: obj2});

          obj2.id = id;
          arr_num.push(obj2);
        }
      }
      else{
        var id = formVar.id;

        var p1             = strarrFindId(formVar.p1, id);
        var p1_2           = strarrFindId(formVar.p1_2, id);
        var p1_isallfamily = strarrFindId(formVar.p1_isallfamily, id);
        var p3             = strarrFindId(formVar.p3, id);
        var p3_isp3        = strarrFindId(formVar.p3_isp3, id);
        var p5             = strarrFindId(formVar.p5, id);
        var p5_isp5        = strarrFindId(formVar.p5_isp5, id);
        var p7             = strarrFindId(formVar.p7, id);
        var p7_isp7        = strarrFindId(formVar.p7_isp7, id);

        var p1_num = formVar.p1_num;
        if(p1 == "-1" && !p1_num){
          p1_num = funcNextLightNum("光明燈");
        }
        var p3_num = formVar.p3_num;
        if(p3 == "-1" && !p3_num){
          p3_num = funcNextLightNum("藥師燈");
        }
        var p5_num = formVar.p5_num;
        if(p5 == "-1" && !p5_num){
          p5_num = funcNextLightNum("光明大燈");
        }
        var p7_num = formVar.p7_num;
        if(p7 == "-1" && !p7_num){
          p7_num = funcNextLightNum("藥師大燈");
        }

        if(p1 == "-1"){
          light_text = "光明燈";
        }
        if(p3 == "-1"){
          light_text = "藥師燈";
        }
        if(p5 == "-1"){
          light_text = "光明大燈";
        }
        if(p7 == "-1"){
          light_text = "藥師大燈";
        }

        var obj2 = {
          p1: p1,
          p1_2: p1_2,
          p1_isallfamily: p1_isallfamily,
          p3: p3,
          p3_isp3: p3_isp3,
          p5: p5,
          p5_isp5: p5_isp5,
          p7: p7,
          p7_isp7: p7_isp7,
          p1_name2:formVar.p1_name2,
          p1_num:  p1_num,
          p3_name2:formVar.p3_name2,
          p3_num:  p3_num,
          p5_name2:formVar.p5_name2,
          p5_num:  p5_num,
          p7_name2:formVar.p7_name2,
          p7_num:  p7_num,
          year: Number(formVar.year),
          light_text: light_text,
        }

        Lighting2.update(id, {$set: obj2});
        obj2.id = id;
        arr_num.push(obj2);

        // console.log("obj2");
        // console.log(obj2);
      }

      // var result = {
      //   ordernum : ordernum,
      // }
      obj1.arr_num = arr_num;
      return obj1;
    }
    return 1;
  },
  'updatePraying2total': function(listId) {
    var currentUserId = Meteor.userId();
    if(currentUserId){

      // 讀所有praying2的價錢 來寫回praying1價錢的值
      var total = 0;

      Praying2.find({listId: listId}).map(function(doc) {
        // if(id != doc._id){
          total += Number(doc.row_total);
          // console.log(doc.row_total);
        // }
      });
      var p1 = { "cash": total };
      Praying1.update(listId, {$set: p1 });

      // console.log("family delete ok " +id);
      return p1;
    }
    return 1;
  },
  'insertBooking': function(formVar) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      formVar.insertedAt = new Date();
      formVar.insertedBy = currentUserId;
      formVar.insertedName = Meteor.user().profile.name;

      var praying1 = Praying1.findOne({ ordernum: formVar.ordernum });

      // console.log(praying1);

      var p1_cash_paid = "";
      // var p1_cash_balance = "";

      if(!praying1.cash_paid){
        p1_cash_paid = 0;
      }
      else{
        p1_cash_paid = praying1.cash_paid;
      }

      // if(!praying1.cash_balance){
      //   p1_cash_balance = praying1.cash;
      // }
      // else{
      //   p1_cash_balance = praying1.cash_balance;
      // }

      formVar.cash_paid = Number(p1_cash_paid) + Number(formVar.received);
      // formVar.cash_balance = Number(p1_cash_balance) - Number(formVar.received);
      formVar.cash_balance = Number(formVar.shouldpay) - Number(formVar.cash_paid);

      Praying1.update(praying1._id, {$set: {
        cash_paid: formVar.cash_paid,
        cash_balance: formVar.cash_balance
      }});
      var id = Booking.insert(formVar);
      // console.log("Booking insert ok " +id);
      // console.log(formVar);
    }
    return formVar;
  },
  'getIsPaid': function(id) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var count = Booking.find({praying1_id: id, status:"1"}).count();
      // console.log("Booking get ok " +id);
      return count;
    }
    return 0;
  },
  'delFamily': function(fid) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      Family.remove({_id: fid});
    }
    return 1;
  },
  'createFamily': function(formVar) {
    var currentUserId = Meteor.userId();
    // console.log(formVar);
    if(currentUserId){
      var f = Family.insert({log_id: currentUserId});
      // console.log("insert ok " +f);

      var name = "";
      var name_count = 0;

      if(!!formVar.name){
        var p = People.find({name: formVar.name});
        name_count = p.count();
        var arrP = p.fetch();
        for(var i in arrP){
          var entry = arrP[i];
          name = name + entry.name + " " + entry.addr + "\n";

          if(i>=5){
            name = name + "...";
            break;
          }
        }
      }

      var nameaddrs = "";
      var addrs = "";
      var nameaddrs_count = 0;
      var addrs_count = 0;

      if(!!formVar.addr){
        var p = People.find({name: formVar.name, addr: formVar.addr});
        nameaddrs_count = p.count();
        var arrP = p.fetch();
        for(var i in arrP){
          var entry = arrP[i];
          nameaddrs = nameaddrs + entry.name + " " + entry.addr + "\n";

          if(i>=5){
            nameaddrs = nameaddrs + "...";
            break;
          }
        }
        var p = People.find({addr: formVar.addr});
        var arrP = p.fetch();
        addrs_count = p.count();
        for(var i in arrP){
          var entry = arrP[i];
          addrs = addrs + entry.name + " " + entry.addr + "\n";

          if(i>=5){
            addrs = addrs + "...";
            break;
          }
        }
      }

      return {
        fid: f,
        dupName: name_count,
        dupNameAddr: nameaddrs_count,
        dupAddr: addrs_count,
        names: name,
        nameaddrs: nameaddrs,
        addrs: addrs,
      };
    }
    return 1;
  },
  'getFamilyLive': function(familyId) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var f = People.find({ familyId: familyId, isLive:1 }, {fields:{name:1, addr:1}}).fetch();
      return f;
    }
    return [];
  },
  'getAddr3': function(addr1, addr2) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var arr = [];
      var arr1 = [];

      Addr2.find({ addr1: addr1, addr2: addr2 }, {sort:{addr3:1}}).forEach(function (obj) {
        if(arr1.indexOf(obj.addr3) == -1){
          arr1.push(obj.addr3);
          arr.push(obj);
        }
      });
      return arr;
    }
    return [];
  },
  'getAddr4': function(addr1, addr2, addr3) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var arr = [];
      arr = Addr2.find({ addr1: addr1, addr2: addr2, addr3: addr3 }, {sort:{addr4:1}}).fetch();
      return arr;
    }
    return [];
  },
  'getPray2': function(pray1) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var f = Pray2.find({ class_id: pray1, value:{$ne:""} }, {sort: {order_id:1}}).fetch();
      return f;
    }
    return [];
  },
  'getAddrFamilyLive': function(familyId) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var f = People.find({familyId: familyId, isLive:1}, {fields:{name: 1, post5code: 1, addr: 1}}).fetch();
      return f;
    }
    return [];
  },
  'getAddrFamilyPass': function(familyId) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var f = People.find({familyId: familyId, isLive:0}, {fields:{name: 1, addr: 1}}).fetch();
      return f;
    }
    return [];
  },
  'getFamilyPass': function(familyId) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      var f = People.find({ familyId: familyId, isLive:0 }, {fields:{name:1, addr:1}}).fetch();
      return f;
    }
    return [];
  },
  'updateClient': function(id, formVar) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      Clients.update(id, {$set: formVar });

    }
    return 1;
  },
  'updateClientLive0': function(id) {
      console.log("updateClientLive0 " +id);
    var currentUserId = Meteor.userId();
    if(currentUserId){
      People.update(id, {$set: {
        isLive: 0
      } });
    }
    return "1";
  },
  'updateClientLive1': function(id) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      People.update(id, {$set: {
        isLive: 1
      } });
    }
    return "1";
  },
  'updateSortTempleclass': function(arr) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      for(var i in arr){
        var ord = Number(i)+1;
        TempleClass.update(arr[i]._id, {$set:{order: ord}});
      }
    }
    return "1";
  },
  'updateSortTemplelevel': function(arr) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      for(var i in arr){
        var ord = Number(i)+1;
        TempleLevel.update(arr[i]._id, {$set:{order: ord}});
      }
    }
    return "1";
  },
  'updateSortPrayingtype': function(arr) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      for(var i in arr){
        var ord = Number(i)+1;
        PrayingType.update(arr[i]._id, {$set:{order: ord}});
      }
    }
    return "1";
  },
  'updateSortPray1': function(arr) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      for(var i in arr){
        var ord = Number(i)+1;
        Pray1.update(arr[i]._id, {$set:{order_id: ord}});
      }
    }
    return "1";
  },
  'updateSortPray2': function(arr) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      for(var i in arr){
        var ord = Number(i)+1;
        Pray2.update(arr[i]._id, {$set:{order_id: ord}});
      }
    }
    return "1";
  },
  'updateSortAcc1type': function(arr) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      for(var i in arr){
        var ord = Number(i)+1;
        Acc1type.update(arr[i]._id, {$set:{order_id: ord}});
      }
    }
    return "1";
  },
  'updateSortAcc2subject': function(arr) {
    var currentUserId = Meteor.userId();
    if(currentUserId){
      for(var i in arr){
        var ord = Number(i)+1;
        Acc2subject.update(arr[i]._id, {$set:{order_id: ord}});
      }
    }
    return "1";
  },
});
