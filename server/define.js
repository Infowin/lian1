basePath = process.cwd() + '/../web.browser/app/'; 

prayLight1png = basePath+'print/images/lightnote_onlymain.png';
prayLight2png = basePath+'print/images/lightnote_onlymain2.png';
prayCandle1png = basePath+'print/images/candle.png';
prayCandle2png = basePath+'print/images/candle2.png';


 BiauKaiTTF = basePath+'print/BiauKai.ttf';
// BiauKaiTTF = basePath+'print/edukai-3.ttf'; // 掉字
// BiauKaiTTF = basePath+'print/BiauKai2.ttf'; // 多字時還是不行
// BiauKaiTTF = basePath+'print/BiauKai3.ttf'; // 完全不行
// BiauKaiTTF = basePath+'print/BiauKai4.ttf'; // 不是unicode的
// BiauKaiTTF = basePath+'print/msyh.ttf'; // 雅黑 完全正常
// BiauKaiTTF = basePath+'print/msjh.ttf'; // 正體 完全正常
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; // No unicode cmap
// BiauKaiTTF = basePath+'print/kaiu.ttf'; // no
// BiauKaiTTF = basePath+'print/kaiu_1.ttf'; // no
// BiauKaiTTF = basePath+'print/kai-pc.ttf'; // no
// BiauKaiTTF = basePath+'print/ukai_1.ttf'; // some
// BiauKaiTTF = basePath+'print/ukai_2.ttf'; // no
//BiauKaiTTF = basePath+'print/ukai_3.ttf'; // no
// BiauKaiTTF = basePath+'print/ukai_0.ttf'; // no
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; // 
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; // 
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; // 
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; // 

msjhTTF = basePath+'print/msjh.ttf'; // ok 正體
msyhTTF = basePath+'print/msyh.ttf'; // ok 雅黑


//A4: [595.28, 841.89],
a4pageHeight = 841.89;
a4pageWidth = 595.28;
a4pageWidthHalf = a4pageWidth/2;
a4pageWidthHalf2 = a4pageWidth/4;
