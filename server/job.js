/*myJobs = JobCollection('myJobQueue');

// job = new Job(jc, type, data) - Anywhere
myJobs.allow({
  // Grant full permission to any authenticated user
  admin: function (userId, method, params) {
    return (userId ? true : false);
  },
  jobSave: function (userId, method, params) {
    console.log("jobSave");
    console.log("method");
    console.log(method);
    console.log("params");
    console.log(params);

    return true;

    // params[0] is the new job doc
    if ( params[0].type === 'email') {
      console.log("in jobsave email");
      return true;
    }
    else if ( params[0].type === 'pdfmember5') {
      console.log("in pdfmember5");



      return true;
    }
    return false;
  }
});
myJobs.events.on('jobDone', function (msg) {
  if (!msg.error) {
    console.log("Job " + msg.params[0] + " finished!");
    // console.log(msg);
  }
});

Meteor.startup(function () {
  // Normal Meteor publish call, the server always
  // controls what each client can see
  Meteor.publish('allJobs', function () {
    return myJobs.find({});
  });

  // Create a worker
   var queue5 = Job.processJobs(
    'myJobQueue', // also tried JobsQueue here
    'pdfmember5',   // Type of job to request
    {
      concurrency: 1,
      payload: 1,
      prefetch: 1
    },
    function (job, callback) {
      // Only called when there is a valid job
      var data = job.data;
      console.log('[JobsQueue] Working memberPage5 job: '+data.oid);

      var doc = new PDFDocument({size: 'A4', margin: 1});
      doc.registerFont('BiauKai', BiauKaiTTF);
      Api.memberPage5(doc, data.arrObj);
      doc.writeSync(data.filename);
      Pdfpath.insert({file: data.file, oid: data.oid, time: data.time});

      job.done();
      callback();
    }
  );  
   var queue4 = Job.processJobs(
    'myJobQueue', // also tried JobsQueue here
    'pdfmember4',   // Type of job to request
    {
      concurrency: 1,
      payload: 1,
      prefetch: 1
    },
    function (job, callback) {
      // Only called when there is a valid job
      var data = job.data;
      console.log('[JobsQueue] Working pdfmember4 job: '+data.oid);

      var doc = new PDFDocument({size: 'A4', margin: 1});
      doc.registerFont('BiauKai', BiauKaiTTF);
      Api.memberPage4(doc, data.arrObj, data.row);
      doc.writeSync(data.filename);
      Pdfpath.insert({file: data.file, oid: data.oid, time: data.time});

      job.done();
      callback();
    }
  );  
  // Start the myJobs queue running
  return myJobs.startJobServer();
});
  
/*
// node.js Worker
var DDP = require('ddp');
var DDPlogin = require('ddp-login');
var Job = require('meteor-job');

// `Job` here has essentially the same API as JobCollection on Meteor.
// In fact, job-collection is built on top of the 'meteor-job' npm package!

// Setup the DDP connection
var ddp = new DDP({
  host: "localhost",
  port: 3000,
  use_ejson: true
});

// Connect Job with this DDP session
Job.setDDP(ddp);

// Open the DDP connection
ddp.connect(function (err) {
  if (err) throw err;
  // Call below will prompt for email/password if an
  // authToken isn't available in the process environment
  DDPlogin(ddp, function (err, token) {
    if (err) throw err;
    // We're in!
    // Create a worker to get sendMail jobs from 'myJobQueue'
    // This will keep running indefinitely, obtaining new work
    // from the server whenever it is available.
    // Note: If this worker was running within the Meteor environment,
    // Then only the call below is necessary to setup a worker!
    // However in that case processJobs is a method on the JobCollection
    // object, and not Job.
    var workers = Job.processJobs('myJobQueue', 'pdfmember5',
      function (job, cb) {
        // This will only be called if a
        // 'sendEmail' job is obtained
        var email = job.data; // Only one email per job
        pdfmember5(email.address, email.subject, email.message,
          function(err) {
            if (err) {
              job.log("Sending failed with error" + err,
                {level: 'warning'});
              job.fail("" + err);
            } else {
              job.done();
            }
            // Be sure to invoke the callback
            // when work on this job has finished
            cb();
          }
        );
      }
    );
  });
});*/