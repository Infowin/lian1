module.exports = {
  servers: {
    one: {
      host: "13.75.111.29",
      username: 'infowintech',
      password: 'Info24934467'
    }
  },

  meteor: {
    name: 'lian',
    path: "~/Downloads/_code/lian1",
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
      debug: true,
      // debug: false,
    },
    env: {
      PORT: '3118',
      ROOT_URL: "http://13.75.111.29",
      MONGO_URL: 'mongodb://localhost:27017/meteor'
    },
    dockerImage: 'abernix/meteord:base',
    //dockerImage: 'kadirahq/meteord'
    enableUploadProgressBar: true,
    deployCheckWaitTime: 60
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};
