Template.login.helpers({
    /*test: function(){
        // console.log(this);
        return "";
    }*/
});
Template.register.events({
    "submit form": function(event, template) {
        event.preventDefault();

        // 1. Collect the username and password from the form
        var username = template.find('#username').value,
            password = template.find('#password').value,
            email = template.find('#email').value,
            password2 = template.find('#password-again').value,
            chtname = template.find('#chtname').value;


        if(password != password2){
            var message = "錯誤: 兩欄密碼不相符";
            $('#form-messages').html(message);
            return;
        }
        else if(!chtname){
            var message = "錯誤: 請輸入中文姓名";
            $('#form-messages').html(message);
            return;   
        }

        Accounts.createUser({
            username: username,
            password: password,
            email: email,
            profile: {name: chtname}
        }, function(error) {
            if (error) {
                // console.log(err);
                var message = "錯誤: <strong>" + error.reason + "</strong>";
                $('#form-messages').html(message);
            } else {
                // console.log('success!');
                Router.go('/');
            }
        });
    }
});
Template.login.events({
    'submit form': function(event, template) {
        event.preventDefault();

        // 1. Collect the username and password from the form
        var username = template.find('#username').value,
            password = template.find('#password').value;

        // 2. Attempt to login.
        Meteor.loginWithPassword(username, password, function(error) {
            // 3. Handle the response
            if (Meteor.user()) {
                // Redirect the user to where they're loggin into. Here, Router.go uses
                // the iron:router package.
                Router.go('/');
            } else {
                // If no user resulted from the attempt, an error variable will be available
                // in this callback. We can output the error to the user here.
                var message = "錯誤: <strong>" + error.reason + "</strong>";
                $('#form-messages').html(message);
            }
            return;
        });

        return false;
    }
});

Template.logout.rendered = function() {
    // console.log("logout");

    Meteor.logout();
    Router.go('login');
};

Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
});
