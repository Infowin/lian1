Template.eYear.rendered = function() {
  $("#e_year").jsGrid({
    height: "90%",
    width: "100%",

    sorting: true,
    paging: true,
    // filtering: true,
    editing: true,

    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        filter.sortField = "value";
        filter.sortOrder = "desc";

        return jsgridAjax(filter, "mgyear", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "mgyear", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "mgyear", "PUT");
      },
      deleteItem: function(item) {
        return jsgridAjax(item, "mgyear", "DELETE");
      }
    },
    fields: [
      { type: "control", width: 60 },
      // { name: "order", title: "順序", width: 70, editing: false },
      {
        name: "now_use",
        type: "checkbox",
        title: "預設",
        width: 50,
        itemTemplate: function(value, item) {
          // console.log(item);
          if (item.now_use == "-1")
            return '<input type="checkbox" checked="checked" disabled="disabled">';
          else return '<input type="checkbox" disabled="disabled">';
        },
        editTemplate: function(value, item) {
          var editPicker = (this._editPicker = $("<input>").attr(
            "type",
            "checkbox"
          ));

          setTimeout(function() {
            if (item.now_use == "-1") {
              editPicker.prop("checked", "checked");
            }
          });
          return editPicker;
        },
        editValue: function() {
          // console.log("editValue");
          var val = this._editPicker.prop("checked");
          if (val == true) return -1;
          return 0;
        }
      },
      { name: "value", title: "年份", type: "text", width: 200 }
      // { name: "ps", title: "備註", type: "text", width: 200 },
      // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
    ],
    onDataLoading: function(args) {
      $(".jsgrid select").material_select();
    },
    onItemInserted: function(args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function(args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      $("#e_year").jsGrid("loadData");
    },
    onItemDeleted: function(args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#e_year").jsGrid("loadData");
    }
    /*   onRefreshed: function() {
            var $gridData = $("#e_year .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    Meteor.call("updateSortTempleclass", items, function(error, result){
                        if(error){
                            console.log("error from updateSortTempleclass: ", error);
                        } 
                        else {
                            $("#e_year").jsGrid("loadData");
                        }
                    });
                }
            });
        },*/
  });
  $(".modal select").material_select();
};

Template.eYear.helpers({
  get_countries: function() {
    return objCountries;
  }
});

Template.eYear.events({
  // 'click .my-btn-submit': function () {
  "submit form": function(event) {
    event.preventDefault();
    event.stopPropagation();
    var formVar = $(".form-modal2").serializeObject();

    $("#eTclass")
      .jsGrid("insertItem", formVar)
      .done(function(ret) {
        console.log("insertion completed");
        console.log(ret);

        if (ret.insert == "success") {
          $("#modal2").closeModal();
          $(".form-modal2").trigger("reset");
          $("#eTclass").jsGrid("loadData");
          // Materialize.toast('資料已新增', 3000, 'rounded');
        } else {
          $(".modal2-error").text(ret);
        }
      });
  },
  "click .my-open-modal2": function() {
    // $('#modal2').openModal();
    var formVar = {};

    var d = new Date();
    var year = d.getFullYear() - 1911 + 1;

    if (Mgyear.find().count() > 0) {
      year = Number(Mgyear.findOne({}, { sort: { value: -1 } }).value) + 1;
    }

    formVar.value = year;
    formVar.now_use = "-1";

    $("#e_year")
      .jsGrid("insertItem", formVar)
      .done(function(ret) {
        // console.log("insertion completed");
        // console.log(ret);

        if (ret.insert == "success") {
          // $('#modal2').closeModal();
          // $(".form-modal2").trigger('reset');
          $("#e_year").jsGrid("loadData");
          // Materialize.toast('資料已新增', 3000, 'rounded');
        } else {
          $(".modal2-error").text(ret);
        }
      });
  },
  //  'click .label-provider': function () {
  // $('#modal2').closeModal();
  // Meteor.setTimeout(function(){ Router.go('e_provider'); }, 10);
  //  },
  "click .my-btn-cancel": function() {
    $("#modal2").closeModal();
  }
});
