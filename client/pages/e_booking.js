Template.eBooking.rendered = function() {
    $("#e_booking").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "booking", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "booking", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "booking", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "booking", "DELETE");
            },
        },
        fields: [
            { type: "control" },
            { name: "uid", title: "#", width: 70 },
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
            { name: "value", title: "會計科目", type: "text", width: 200 },
            // { name: "ps", title: "備註", type: "text", width: 200 },
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_booking").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_booking").jsGrid("loadData");
        },
    });
    $('.modal select').material_select();
};

Template.eBooking.helpers({
    get_countries: function() {
        return objCountries;
    },
});

Template.eBooking.events({
    // 'click .my-btn-submit': function () {
    'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal2").serializeObject();


        $("#e_booking").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                $('#modal2').closeModal();
                $(".form-modal2").trigger('reset');
                $("#e_booking").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .my-open-modal2': function() {
        $('#modal2').openModal();
    },
    //  'click .label-provider': function () {
    // $('#modal2').closeModal();
    // Meteor.setTimeout(function(){ Router.go('e_provider'); }, 10); 
    //  },
    'click .my-btn-cancel': function() {
        $('#modal2').closeModal();
    }
});
