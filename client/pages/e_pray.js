Template.ePray.rendered = function() {
  $("#ePray1").jsGrid({
    height: "90%",
    width: "100%",
    sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    // rowClick: $.noop,
    rowClick: function(args) {
      $("#show-detail").hide();

      Session.set("nowrow_pray", args.item);
      $("#ePray2").jsGrid("loadData");

      $("#show-detail").fadeIn("slow");
    },
    rowDoubleClick: jsGrid.Grid.prototype.rowClick,
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        return jsgridAjax(filter, "pray1", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "pray1", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "pray1", "PUT");
      },
      deleteItem: function(item) {
        return jsgridAjax(item, "pray1", "DELETE");
      }
    },
    fields: [
      { type: "control", width: 60 },
      {
        name: "order_id",
        title: "順序",
        type: "number",
        width: 60,
        editing: false
      },
      // { name: "isopen", type: "checkbox", title: "開啟" },
      // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
      {
        name: "now_use",
        type: "checkbox",
        title: "預設",
        width: 50,
        itemTemplate: function(value, item) {
          // console.log(item);
          if (item.now_use == "-1")
            return '<input type="checkbox" checked="checked" disabled="disabled">';
          else return '<input type="checkbox" disabled="disabled">';
        },
        editTemplate: function(value, item) {
          var editPicker = (this._editPicker = $("<input>").attr(
            "type",
            "checkbox"
          ));

          setTimeout(function() {
            if (item.now_use == "-1") {
              editPicker.prop("checked", "checked");
            }
          });
          return editPicker;
        },
        editValue: function() {
          // console.log("editValue");
          var val = this._editPicker.prop("checked");
          if (val == true) return -1;
          return 0;
        }
      },
      { name: "value", title: "名稱", type: "text", width: 200 }
      // { name: "ps", title: "備註", type: "text", width: 200 },
      // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
    ],
    onDataLoading: function(args) {
      $(".jsgrid select").material_select();
    },
    onItemInserted: function(args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function(args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      $("#ePray1").jsGrid("loadData");
    },
    onItemDeleted: function(args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      // $("#ePray").jsGrid("loadData");
    },
    onRefreshed: function() {
      var $gridData = $("#ePray1 .jsgrid-grid-body tbody");

      $gridData.sortable({
        update: function(e, ui) {
          // arrays of items
          var items = $.map($gridData.find("tr"), function(row) {
            return $(row).data("JSGridItem");
          });
          // console.log("Reordered items", items);
          Meteor.call("updateSortPray1", items, function(error, result) {
            if (error) {
              console.log("error from updateSortPray1: ", error);
            } else {
              $("#ePray1").jsGrid("loadData");
            }
          });
        }
      });
    }
  });

  $("#ePray2").jsGrid({
    height: "90%",
    width: "100%",
    sorting: true,
    paging: true,
    pageSize: 50,
    // filtering: true,
    editing: true,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        var item = Session.get("nowrow_pray");
        if (!!item && !!item._id) {
          filter.class_id = item._id;
        }

        if (!!filter.sortField) {
          filter.sortField = "order_id";
          filter.sortOrder = "asc";
        }

        return jsgridAjax(filter, "pray2", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "pray2", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "pray2", "PUT");
      },
      deleteItem: function(item) {
        return jsgridAjax(item, "pray2", "DELETE");
      }
    },
    /*
          { id:"",  header:"順序", width:55 },
      { id:"",     editor:"text",  header:"名稱", fillspace:1},
      { id:"now_num",   editor:"text",  header:"目前編號", width:90 },
         */
    fields: [
      { type: "control", editButton: false },
      {
        name: "order_id",
        title: "順序",
        width: 70,
        type: "number",
        editing: false
      },
      { name: "value", title: "法會項目", type: "text", width: 200 },
      { name: "now_num", type: "text", title: "目前編號", width: 90 },
      // { name:"pray_paper", type:"select", title:"列印紙張", items:objPaperSize, valueField: "id", textField: "value"  },
      // { name: "ps", title: "備註", type: "text", width: 200 },
      { name: "price", title: "法會項目 價錢", type: "text", width: 200 }
    ],
    onDataLoading: function(args) {
      $(".jsgrid select").material_select();
    },
    onItemInserted: function(args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    // onItemUpdating: function(args) {
    //     console.log(args);
    //     var item = Session.get("nowrow_pray");
    // },
    onItemUpdated: function(args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      // $("#ePray2").jsGrid("loadData");
    },
    onItemDeleted: function(args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      // $("#ePray").jsGrid("loadData");
      var item = Session.get("nowrow_pray");
      $("#ePray2").jsGrid("loadData", { class_id: item._id });
    },
    onRefreshed: function() {
      var $gridData = $("#ePray2 .jsgrid-grid-body tbody");
      $gridData.sortable({
        update: function(e, ui) {
          var items = $.map($gridData.find("tr"), function(row) {
            return $(row).data("JSGridItem");
          });
          // console.log("Reordered items", items);
          Meteor.call("updateSortPray2", items, function(error, result) {
            if (error) {
              console.log("error from updateSortPray1: ", error);
            } else {
              $("#ePray2").jsGrid("loadData");
            }
          });
        }
      });
    }
  });
  $(".modal select").material_select();
  $("#show-detail").hide();
};

Template.ePray.helpers({
  get_countries: function() {
    return objCountries;
  },
  show_name: function() {
    var item = Session.get("nowrow_pray");
    if (!!item && !!item.value) return item.value;
    return "";
  },
  show_name_id: function() {
    var item = Session.get("nowrow_pray");
    if (!!item && !!item._id) return item._id;
    return "";
  }
});

Template.ePray.events({
  "click .my-open-modal1": function() {
    // $('#modal1').openModal();
    $("#modal1").toggle();
  },
  "click .my-btn-cancel1": function() {
    // $('#modal1').closeModal();
    $("#modal1").toggle();
  },
  "click .my-btn-submit1": function() {
    var formVar = $(".form-modal1").serializeObject();

    $("#ePray1")
      .jsGrid("insertItem", formVar)
      .done(function(ret) {
        console.log("insertion completed");
        console.log(ret);

        if (ret.insert == "success") {
          // $('#modal1').closeModal();
          $("#modal1").toggle();
          $(".form-modal1").trigger("reset");
          $("#ePray1").jsGrid("loadData");
          // Materialize.toast('資料已新增', 3000, 'rounded');
        } else {
          $(".modal1-error").text(ret);
        }
      });
  },
  "click .my-open-modal2": function() {
    // $('#modal2').openModal();
    $("#modal2").toggle();
  },
  "click .my-btn-cancel2": function() {
    // $('#modal2').closeModal();
    $("#modal2").toggle();
  },
  "click .my-btn-submit2": function() {
    var formVar = $(".form-modal2").serializeObject();

    $("#ePray2")
      .jsGrid("insertItem", formVar)
      .done(function(ret) {
        console.log("insertion completed");
        console.log(ret);

        if (ret.insert == "success") {
          // $('#modal2').closeModal();
          $("#modal2").toggle();
          $(".form-modal2").trigger("reset");

          var item = Session.get("nowrow_pray");
          $("#ePray2").jsGrid("loadData", { class_id: item._id });
          // Materialize.toast('資料已新增', 3000, 'rounded');
        } else {
          $(".modal2-error").text(ret);
        }
      });
  }
});
