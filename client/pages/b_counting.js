Template.bCounting.rendered = function() {
    $("#bCounting").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "accounting", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "accounting", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "accounting", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "accounting", "DELETE");
            },
        },
        fields: [
            { name: "date", title: "日期", width: 60 },
            // { name: "productclassid", title: "分類", type: "select", items: [{ id: "", value: "" }].concat(objProductClass), valueField: "id", textField: "value" },
            // { name: "providerid", title: "供應商", type: "select", items: [{ _id: "", fullname: "" }].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            { name: "cost1", title: "支出(1)", type: "text" },
            { name: "cost2", title: "支出(2)", type: "text" },
            { name: "cost3", title: "支出(3)", type: "text" },
            { name: "cost4", title: "支出(4)", type: "text" },
            { name: "cost5", title: "支出(5)", type: "text" },
            { name: "total", title: "支出總計", 
            	itemTemplate: function(value, item) { 
            		return Number(item.cost1)+Number(item.cost2)+Number(item.cost3)+Number(item.cost4)+Number(item.cost5);
             	}, 
            },
            { name: "deposit", title: "存入", type: "text" },
            // { name: "name", title: "產品名稱", type: "text" },
            { name: "ps", title: "備註", type: "text" },
            { name: "balance", title: "餘額" },
            { type: "control" }
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            console.log(args);
            $("#bCounting").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#bCounting").jsGrid("loadData");
        },
    });
    $('.modal select').material_select();
};

Template.bCounting.helpers({
    get_productclass: function() {
        return objProductClass;
    },
    get_providers: function() {
        return Provider.find();
    }
});

Template.bCounting.events({
    // 'click .my-btn-submit': function () {
    'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal1").serializeObject();
        formVar.productclassname = $('#productclassid :selected').text();
        formVar.providername = $('#providerid :selected').text();
        console.log(formVar);

        $("#bCounting").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                $('#modal1').closeModal();
                $(".form-modal1").trigger('reset');
                $("#bCounting").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    'click .my-open-modal1': function() {
        $('#modal1').openModal();

        var $input = $('.datepicker').pickadate();
		var picker = $input.pickadate('picker');
		picker.set('select', new Date());

    },
    'click .label-provider': function() {
        $('#modal1').closeModal();
        Meteor.setTimeout(function() { Router.go('e_provider'); }, 10);
    },
    'click .my-btn-cancel': function() {
        $('#modal1').closeModal();
    }
});