Template.cCashier.rendered = function() {
    $("#cCashier").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "cashier", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "cashier", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "cashier", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "cashier", "DELETE");
            },
        },
        fields: [
        /*
        {
    "_id" : "MtbtSree5gSvzTRef",
    "shouldpay" : "50000",
    "cash_paid" : 0,
    "cash_balance" : 50000,
    "paid_date" : "2016/6/1",
    "paid_date_submit" : "",
    "paymethod" : "1",
    "received" : "0",
    "ps" : "",
    "ordernum" : "R00010030",
    "paymethod_text" : "現金",
    "pray1_id" : "nqnw6qAPFwJHbYR5L",
    "pray1_text" : "梁皇法會",
    "pray2_id" : "WC46TzrPgupuwpTAk",
    "pray2_text" : "總功德主",
    "prayserial" : "15",
    "insertedAt" : ISODate("2016-06-01T13:48:03.956Z"),
    "insertedBy" : "426u7A2NWrgey544W",
    "insertedName" : "aaa"
        }
         */
            // { type: "control" },
            { name: "ordernum", title: "編號" },
            // { name: "pray1_text", title: "法會名稱",
            //     itemTemplate: function(value, item) {
            //         if(!!item.insertedAt)
            //             return (new Date(item.insertedAt)).yyyymmddhm();
            //         else
            //             return "";
            //     },
            //  },
            // { name: "pray2_text", title: "法會項目" },
            { name: "service", title: "服務項目", sorting: false,
                itemTemplate: function(value, item) {
                    if(!!item.pray1_text)
                        return "法會 " + item.pray1_text + " " + item.pray2_text;
                    else
                        return "";
                },
             },
            { name: "paid_date", title: "收款日期", type: "text", width: 80},
            { name: "paymethod_text", title: "收款方式", type: "text", width: 70},
            { name: "shouldpay", title: "服務金額", type: "text", width: 70},
            { name: "received", title: "本次收款金額", type: "text", width: 70},
            { name: "cash_paid", title: "已收金額", type: "text", width: 70},
            { name: "cash_balance", title: "剩餘金額", type: "text", width: 70},
            { name: "ps", title: "備註", type: "text"},
            { name: "insertedName", title: "經辦人", type: "text", width: 60},
            { name: "insertedAt", title: "登錄時間", //width: 150,
                itemTemplate: function(value, item) {
                    if(!!item.insertedAt)
                        return (new Date(item.insertedAt)).yyyymmddhm();
                    else
                        return "";
                },
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            console.log(args);
            $("#cCashier").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cCashier").jsGrid("loadData");
        },
    });
    $("#cPraying1").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.cashier = 1;
                if(!filter.sortField){
                    filter.sortField = "ordernum";
                    filter.sortOrder = "desc";
                }

                var pay = $('option:selected', "#cashier-pay-status").val();
                var year = $('option:selected', "#cashier-year").val();
                var prayname = $('option:selected', "#cashier-pray1").val();
                var prayitem = $('option:selected', "#cashier-pray2").val();

                if(!!pay){
                    filter.paystatus = pay;
                }
                if(!!year){
                    filter.year = year;
                }
                if(prayname){
                    filter.prayname = prayname;
                }
                if(prayitem){
                    filter.prayitem = prayitem;
                }

                return jsgridAjax(filter, "praying1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "praying1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "praying1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "praying1", "DELETE");
            },
        },
        fields: [
        /*
        {
            "_id" : "LkBWzYfcfePQrK5rD",
            "ori_id" : "4040",
            "familyId" : "x6RukBhGNriWQ2p2P",
            "ori_family_id" : "2646",
            "if_is" : "0",
            "isSave" : 1,
            "date" : "2014/4/26",
            "user_name" : "軟體維護",
            "prayserial" : "5",
            "cash" : "50000",
            "ordernum" : "R00002872",
            "year" : 103,
            "whoapply" : "PHKMTa7uzecucM4hi",
            "whoapply_text" : "古陳月香",
            "ori_whoapply" : "4147",
            "c_memo2" : "",
            "ori_class_id" : "19",
            "prayname" : "nqnw6qAPFwJHbYR5L",
            "prayname_text" : "梁皇法會",
            "ori_type_id" : "46",
            "prayitem" : "WC46TzrPgupuwpTAk",
            "prayitem_text" : "總功德主",
            "cash_paid" : 5000,
            "cash_balance" : 45000
        }
         */
            // { type: "control" },
            { name: "c1", title: "", width: 40, align:"center",
                itemTemplate: function(value, item) { 
                    // console.log(item);
                    return '<input type="button" value="繳款單" data-id="'+item._id+'" class="my-open-modal10">';
                }, 
            },
            { name: "c2", title: "", width: 40, align:"center",
                itemTemplate: function(value, item) { 
                    // console.log(item);
                    return '<input type="button" value="付款" class="my-open-modal8" data-cash="'+item.cash+'" data-cash_paid="'+item.cash_paid+'" data-cash_balance="'+item.cash_balance+'" data-ordernum="'+item.ordernum+'">';
                }, 
            },
            { name: "c3", title: "", width: 40, align:"center",
                itemTemplate: function(value, item) { 
                    return '<input type="button" value="收據" data-id="'+item._id+'" class="my-open-modal12">';
                }, 
            },
            { name: "ordernum", title: "單號", width: 60 },
            { name: "year", title: "年度", width: 30, align:"center", },
            // { name: "prayname_text", title: "法會名稱", width: 60 },
            // { name: "prayitem_text", title: "法會項目", width: 60 },
            { name: "service", title: "服務項目",
                itemTemplate: function(value, item) {
                    if(!!item.prayname_text)
                        return item.prayname_text + " " + item.prayitem_text + " #" + item.prayserial;
                    else
                        return "";
                },
             },
            { name: "date", title: "建立日期", type: "text", width: 80},
            // { name: "paymethod_text", title: "收款方式", type: "text", width: 70},
            { name: "cash", title: "服務金額", type: "text", width: 70},
            { name: "cash_paid", title: "已收金額", type: "text", width: 70},
            { name: "cash_balance", title: "剩餘金額", type: "text", width: 70},
            // { name: "user_name", title: "經辦人", type: "text", width: 60},
            // { name: "insertedAt", title: "登錄時間", //width: 150,
            //     itemTemplate: function(value, item) {
            //         if(!!item.insertedAt)
            //             return (new Date(item.insertedAt)).yyyymmddhm();
            //         else
            //             return "";
            //     },
            // },
            { name: "whoapply_text", title: "申請人", type: "text", width: 70},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            console.log(args);
            $("#cCashier").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cCashier").jsGrid("loadData");
        },
    });
    $('.modal select').material_select();
    $('select').material_select();
};

Template.cCashier.helpers({
    get_productclass: function() {
        return objProductClass;
    },
    get_providers: function() {
        return Provider.find();
    },
    get_year: function() {
        // return arrMGYear;
        return Mgyear.find({}, {sort:{value: -1}});
    },
    get_pray1: function() {
        return Pray1.find({}, { sort: { order_id: 1 }});
    },
    // get_pray2: function() {
    //     var sel_pray1_id = Session.get("sel_pray1")
    //     // return Pray2.find({ class_id: sel_pray1_id }, { sort: { order_id: 1 }});
    //     if(sel_pray1_id) {
    //         return Pray2.find({ class_id: sel_pray1_id }, { sort: { order_id: 1 }});
    //     }
    // }
});

Template.cCashier.events({
    'change #cashier-year': function() {
        $("#cPraying1").jsGrid("loadData");
    },
    'change #cashier-pray1': function() {
        var sel_pray1_id = $('#cashier-pray1 option:selected').val()
        
        Meteor.call("getPray2", sel_pray1_id, function(error, result){
            $('select').material_select('destroy');
            $('#cashier-pray2')
                .find('option')
                .remove()
                .end()
                .append('<option value="" selected>請選擇</option>')
                // .val('whatever')
            ;
            $.each(result, function (i, item) {
                // if(!!item.value)
                $('#cashier-pray2').append($('<option>', {
                    value: item._id,
                    text : item.value,
                    price:  item.price,
                    serial:  item.now_num,
                }));
            });
            // $("#p4a-prayitem").val(item.prayitem);
            $('select').material_select();
        });
        $("#cPraying1").jsGrid("loadData");
    },
    'change #cashier-pray2': function() {
        $("#cPraying1").jsGrid("loadData");
    },
    'change #cashier-pay-status': function() {
        $("#cPraying1").jsGrid("loadData");
    },
    'click .my-open-modal8': function(event) { // 付款
        var ordernum = $(event.currentTarget).attr("data-ordernum");
        Session.set("nowrow_pay", ordernum);

        var cash = {};
        cash.cash = Number($(event.currentTarget).attr("data-cash"));
        cash.cash_paid = Number($(event.currentTarget).attr("data-cash_paid"));
        cash.cash_balance = Number($(event.currentTarget).attr("data-cash_balance"));

        Template.modalPay(cash);
        // $('#modalPay').openModal();
        $('#modalPay').toggle();
    },
    'click .my-btn-cancel8': function() {
        // $('#modalPay').closeModal();
        $('#modalPay').toggle();
    },
    'click .my-open-modal10': function(e) { // 繳款單     
        var praying1_id = $(e.currentTarget).attr('data-id')
        var formVar = "/api/bill?p1_id=" + praying1_id;
        // var formVar = "/api/bill?"+$(".form-modal4a").serialize();
        $("#pdf-iframe").prop("src", formVar);
        $("#pdf_url").prop("href", formVar);
        $("#modal1-header-text").text("繳款單 - " + Session.get("nowrow_pay"));
        // $('#modal1').openModal();
        $('#modal1').toggle();
    },
    'click .my-btn-cancel10': function() {
        // $('#modal1').closeModal();
        $('#modal1').toggle();
    },
    'click .my-open-modal12': function(e) { // 收據  
        var praying1_id = $(e.currentTarget).attr('data-id')
        var formVar = "/api/receipt?p1_id=" + praying1_id;
        // var formVar = "/api/receipt?"+$(".form-modal4a").serialize();
        $("#pdf-iframe").prop("src", formVar);
        $("#pdf_url").prop("href", formVar);
        $("#modal1-header-text").text("收據(感謝狀) - " + Session.get("nowrow_pay"));
        // $('#modal1').openModal();
        $('#modal1').toggle();
    },
});