Template.eOperatingrec.rendered = function(){
    $("#eOperatingrec").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "oprec", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "oprec", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "oprec", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "oprec", "DELETE");
            },
        },
        fields: [
            // { name: "uid", title: "#", width: 30 },
            // { name: "team", title: "日期", type: "select", items: [{ id: "", value: "" }].concat(Teams.find().fetch()), valueField: "_id", textField: "teamname",
            	// itemTemplate: function(value, item) { 
            		// return item.profile.teamname;
             	// },  
			    // insertTemplate: function(value) {
			    //     return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
			    // },
			    // editTemplate: function(value) {
			    //     return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
			    // },
			    // insertValue: function() {
			    //     return this._insertPicker.datepicker("getDate").toISOString();
			    // },
			    // editValue: function() {
			    //     return this._editPicker.datepicker("getDate").toISOString();
			    // }
         	// },
            // { name: "roles", title: "權限", type: "select", items: [{ id: "", value: "" }].concat(objUserRole), valueField: "id", textField: "value" },
            { name: "datetime", title: "日期"},
            { name: "name", title: "使用者"},
            { name: "ip", title: "IP"  },
            { name: "rec", title: "記錄"},
            // { type: "control" }
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        // onItemInserted: function(args) {
        //     // Materialize.toast('資料已新增!', 3000, 'rounded')
        // },
        // onItemUpdated: function(args) {
        //     Materialize.toast('資料已更新', 3000, 'rounded')
        //     console.log(args);
        //     $("#aProduct").jsGrid("loadData");
        // },
        // onItemDeleted: function(args) {
        //     Materialize.toast('資料已刪除', 3000, 'rounded')
        //     $("#aProduct").jsGrid("loadData");
        // },
    });
    $('.modal select').material_select();

};

Template.eOperatingrec.helpers({
  counter: function () {
    // return Session.get('counter');
  }
});

Template.eOperatingrec.events({
  'click button': function () {
    // Session.set('counter', Session.get('counter') + 1);
  }
});
