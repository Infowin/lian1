Template.cModalPray.newPray = function(){
	$("#ordernum-header").text("新建");
    Session.set("nowrow_praying1", "");
    Session.set("nowrow_pay", "");

    $(".title-cash").text("0");
    $(".title-cash-paid").text("0");
    $(".title-cash-balance").text("0");

	$(".form4a-btn-del").attr("disabled", "disabled");
    $(".my-btn-dup4a").attr("disabled", "disabled");
	$(".form4a-btn-save").removeAttr("disabled");

	$("#cPray2").hide();
	$("#cPray2-hidden").show();
    $(".form-modal4a").trigger('reset');

    var pray1 = Pray1.find({}, {sort:{order_id:1}}).fetch();
    // $('select').material_select('destroy');

    $('#p4a-prayname')
        .find('option')
        .remove()
        .end()
        .append('<option value="" disabled selected>請選擇</option>')
        // .val('whatever')
    ;
    $.each(pray1, function (i, item) {
        $('#p4a-prayname').append($('<option>', {
            value: item._id,
            text : item.value,
        }));
    });

    var pray1_set = Pray1.find({now_use: "-1"}).fetch()[0]._id;
    if(pray1_set) $("#p4a-prayname").val(pray1_set);
    // $('select').material_select();


    Meteor.call("getPray2", pray1_set, function(error, result){
    // console.log(result);
        $('#p4a-prayitem')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>請選擇</option>')
            // .val('whatever')
        ;
        $.each(result, function (i, item) {
            var pr = item.price || "0";
            $('#p4a-prayitem').append($('<option>', {
                value: item._id,
                text : item.value + " ($"+pr+")",
                price:  item.price,
                serial:  item.now_num,
            }));
        });
    });

    $('#p4a-date').val(currentdate());
    $('#p4a-year').val(Mgyear.findOne({now_use: "-1"}).value);

    $('#p4a-cash1').val("0");
    $('#p4a-cash2').val("0");
    $('#p4a-cash').val("0");
    $('#p4a-cash1-label').addClass("active");
    $('#p4a-cash2-label').addClass("active");
    $('#p4a-cash-label').addClass("active");

    $('#p4a-prayserial').val("0");
    $('#p4a-prayserial-label').addClass("active");

    // $('select').material_select();

    Tracker.autorun(function () {
        // 控制按鈕的
        if (!!Session.get("nowrow_pay")){
            $(".my-open-modal8").attr("disabled", false);
            $(".my-open-modal10").attr("disabled", false);
            $(".my-open-modal12").attr("disabled", false);
        }
        else{
            $(".my-open-modal8").attr("disabled", true);
            $(".my-open-modal10").attr("disabled", true);
            $(".my-open-modal12").attr("disabled", true);
        }
        // 剩餘的
        // console.log("check balance");
        if(!!Session.get("nowrow_praying1")){
            var s_p1 = Session.get("nowrow_praying1");

            $(".title-cash").text(s_p1.cash || "0"); // 應收
            $(".title-cash-paid").text(s_p1.cash_paid || "0"); // 已收
            // $(".title-cash-balance").text(s_p1.cash_balance || "0");

            var balance = Number($(".title-cash").text())- Number($(".title-cash-paid").text());
            $(".title-cash-balance").text( balance );
        }
    });
}
Template.cModalPray.rendered = function() {
    $("#cPray1").jsGrid({
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 4,
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                var item = Session.get("nowrow");
                filter.familyId = item.familyId;
                // filter.isLive = 1;

                return jsgridAjax(filter, "praying1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "praying1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "praying1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "praying1", "DELETE");
            },
        },
        fields: [
            { name: "ordernum", title: "編號", width:60 },
            { name: "year", title: "年度", width:40 },
            { name: "prayallitems", title: "法會",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    return item.prayname_text+" "+item.prayitem_text+" #"+item.prayserial;
                },
            },
            { name: "date", title: "申請日", width:60 },
            { name: "whoapply_text", title: "申請人", width:60 },
            { name: "isclose", title: "帳務", width:60,
                itemTemplate: function(value, item) {
                    if(item.isClose_text)
                        return item.isClose_text;
                    else
                        return "未關帳";
                },
            },
            // { type: "control" }
        ],
        rowClick: function(args) {
            // console.log(args);
            $("#cPray2-hidden").hide();
            $("#cPray2").fadeIn();

            Session.set("nowrow_praying1", args.item);

            // $('select').material_select('destroy');
            var item = args.item;
            // console.log(item);
            $("#ordernum-header").text(item.ordernum);

            $("#p4a-date").val(item.date);
            $("#p4a-year").val(item.year);
            $("#p4a-prayname").val(item.prayname);

            $("#p4a-prayserial").val(item.prayserial);
            $("#p4a-prayserial-label").addClass("active");

            $("#p4a-whoapply").val(item.whoapply);

            $("#p4a-cash").val(item.cash);
            $("#p4a-cash-label").addClass("active");
            $("#p4a-cash1").val(item.cash1 || item.cash);
            $("#p4a-cash1-label").addClass("active");
            $("#p4a-cash2").val(item.cash2 || "0");
            $("#p4a-cash2-label").addClass("active");
            // Session.set("ori_p4a_cash", $("#p4a-cash2").val());


            $("#p4a-booknum").val(item.booknum);
            $("#p4a-booknum-label").addClass("active");

            $("#p4a-ps2").val(item.ps);
            $("#p4a-ps2-label").addClass("active");

            // $(".title-cash").text(item.cash || "0");

			    	$(".form4a-btn-save").attr("disabled", "disabled");
			    	$(".form4a-btn-del").removeAttr("disabled");
            $(".my-btn-dup4a").removeAttr("disabled");

            Session.set("nowrow_pay", item.ordernum);


	        Meteor.call("getPray2", item.prayname, function(error, result){
	        // console.log(result);

	            $('#p4a-prayitem')
	                .find('option')
	                .remove()
	                .end()
	                .append('<option value="" disabled selected>請選擇</option>')
	                // .val('whatever')
	            ;
	            $.each(result, function (i, item) {

                    var pr = item.price || "0";
                    $('#p4a-prayitem').append($('<option>', {
                        value: item._id,
                        text : item.value + " ($"+pr+")",
                        price:  item.price,
                        serial:  item.now_num,
                    }));
	            });
	            $("#p4a-prayitem").val(item.prayitem);
	            // $('select').material_select();
	        });

            $(".my-praying2-detail").fadeIn();

            $("#cPray2").jsGrid("loadData");
            // $("#cBook").jsGrid("loadData");
        },
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
    /*       onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#cPray1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cPray1").jsGrid("loadData");
        },*/
    });
    $("#cPray2").jsGrid({
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 10,
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller:{
            loadData: function(filter) {
                // filter.clientid = $("#show_uid").text();
                // console.log("cPray2 filter");

                var item = Session.get("nowrow_praying1");
                filter.listId = item._id;
                // console.log(filter);

                return jsgridAjax(filter, "praying2", "GET");
            },
            insertItem: function(item) {
                item.p1_cash = $("#p4a-cash").val();
                item.p1_cash1 = $("#p4a-cash1").val();
                item.p1_cash2 = $("#p4a-cash2").val();

                return jsgridAjax(item, "praying2", "POST");
            },
            updateItem: function(item) {
                item.p1_cash = $("#p4a-cash").val();
                item.p1_cash1 = $("#p4a-cash1").val();
                item.p1_cash2 = $("#p4a-cash2").val();

                return jsgridAjax(item, "praying2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "praying2", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40,
                // modeSwitchButton: false,
                editButton: false,
               /* headerTemplate: function() {
                    return $("<button>").attr("type", "button").addClass("my-btn-submit4b").text("新增");
                            // .on("click", function () {
                                // showDetailsDialog("Add", {});
                            // });
                }*/
            },
            // { name: "uid", title: "編號" },

            //
            { name: "type", title: "列印種類", width:50, //type: "select", valueField: "_id", textField: "value",
                // items: [{_id:"",value:""}].concat(
                //     PrayingType.find({},{sort:{order: 1}}).fetch()
                    // PrayingType.find(
                    //     {$and:[
                    //         {$or: [
                    //             {only_pray:{ $exists: false }},
                    //             {only_pray: Session.get("nowrow_praying1").prayname },
                    //             {only_pray: "" }]},
                    //         {only_pray: {$ne: "0"}}
                    //     ]},{sort:{order: 1}} ).fetch()
                    // ),

                itemTemplate: function(value, item) {
                    if(!!item && !!item.type)
                        return PrayingType.findOne({_id: item.type }).value;
                    return "";
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var sel = $('<select class="my-praying2-edi-type-sel"></select>')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>請選擇</option>')
                    ;

                    var type = PrayingType.find(
                        {$and:[
                            {$or: [
                                {only_pray:{ $exists: false }},
                                {only_pray: Session.get("nowrow_praying1").prayname },
                                {only_pray: "" }]},
                            {only_pray: {$ne: "0"}}
                        ]},{sort:{order: 1}} ).fetch();

                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text : item2.value + " (+$"+(item2.price || "0")+")",
                        }));
                    });
                    sel.val(value);
                    return this._editPicker = sel;
                },
                editValue: function() {
                    return this._editPicker.val();
                }
            },
            { name: "row_total", title: "外加價錢", width:50, type: "number", editcss: "edi-pray2-row_total" },
            { name: "live_select", align:"center", editing: false, sorting: false, title: "選擇", width:40,
                editTemplate: function(value, item) {
                    // console.log(item._id);
                    return $("<button>").attr("type", "button").attr("data-p2-id", item._id).addClass("my-open-modal4b1").text("點選");
                },
            },
            { name: "livename_text", title: "陽上", type: "text", editcss: "edi-pray2-livename", },
            { name: "pass_select", align:"center", editing: false, sorting: false, title: "選擇", width:40,
                editTemplate: function(value, item) {
                    // console.log(item);
                    return $("<button>").attr("type", "button").attr("data-p2-id", item._id).addClass("my-open-modal4b2").text("點選");
                },
            },
            // { name: "passname_text", title: "亡者(拔渡)", type: "text", editcss: "edi-pray2-passname" },
            { name: "passname_text", title: "拔", width:70, type:"text", editcss: "edi-pray2-passname" },
            { name: "passname_text1", title: "拔 歷代祖先", width:70, type:"text", editcss: "edi-pray2-passname1" },
            { name: "passname_text2", title: "拔 冤親債主", width:70, type:"text", editcss: "edi-pray2-passname2" },
            { name: "passname_text3", title: "拔 地基主", width:70, type:"text", editcss: "edi-pray2-passname3" },
            { name: "passname_text4", title: "拔 嬰靈", width:70, type:"text", editcss: "edi-pray2-passname4" },
            { name: "addr", title: "地址", type: "text", editcss: "edi-pray2-addr" },
        ],
        // rowClick: function(args) {
        //     // console.log(args);
        // },
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onDataLoaded: function(args) {
            // $('.jsgrid select').material_select();
            $("#cPray2 .jsgrid-grid-body tbody").prepend('<tr class="jsgrid-edit-row"> \
                <td class="jsgrid-control-field jsgrid-align-center" style="width: 40px;"> \
                    <button type="button" class="my-btn-submit6b">儲存</button></td> \
                </td> \
                <td style="width: 50px;"> \
                    <select class="my-praying2-add-type-sel"></select> \
                </td> \
              <td class="add-pray2-row_total jsgrid-align-right" style="width: 50px;"><input class="my-praying2-row_total" type="number"></td>\
              <td class="jsgrid-align-center" style="width: 40px;"> \
                    <button type="button" data-p2-id="" class="my-open-modal6b1">點選</button> \
                </td> \
                <td class="add-pray2-livename" style="width: 100px;"> \
                    <input type="text"> \
                </td> \
                <td class="jsgrid-align-center" style="width: 40px;"> \
                    <button type="button" data-p2-id="" class="my-open-modal6b2">點選</button> \
                </td> \
                <td class="add-pray2-passname" style="width: 70px;"><input type="text"></td> \
                <td class="add-pray2-passname1" style="width: 70px;"><input type="text"></td> \
                <td class="add-pray2-passname2" style="width: 70px;"><input type="text"></td> \
                <td class="add-pray2-passname3" style="width: 70px;"><input type="text"></td> \
                <td class="add-pray2-passname4" style="width: 70px;"><input type="text"></td> \
                <td class="add-pray2-addr" style="width: 100px;"><input type="text"></td> \
            </tr>');

            var sel = $('.my-praying2-add-type-sel')
                .find('option')
                .remove()
                .end()
                .append('<option value="" disabled selected>請選擇</option>')
            ;

            var type = PrayingType.find(
                {$and:[
                    {$or: [
                        {only_pray:{ $exists: false }},
                        {only_pray: Session.get("nowrow_praying1").prayname },
                        {only_pray: "" }]},
                    {only_pray: {$ne: "0"}}
                ]},{sort:{order: 1}} ).fetch();

            $.each(type, function (i, item2) {
                sel.append($('<option>', {
                    value: item2._id,
                    // text : item2.value,
                    text : item2.value + " (+$"+(item2.price || "0")+")",
                }));
            });

        },
        onItemInserted: function(args) {
            // console.log(args);
            Materialize.toast('資料已新增!', 3000, 'rounded')
            $(".title-cash").text( $("#p4a-cash").val() );

            $("#p4a-cash2").val(args.item.p1.cash2);
            $("#p4a-cash2-label").addClass("active");
            $("#p4a-cash").val(args.item.p1.cash);
            $("#p4a-cash-label").addClass("active");


            var sp1 = Session.get("nowrow_praying1");
            sp1.cash = args.item.p1.cash;
            sp1.cash2 = args.item.p1.cash2;
            Session.set("nowrow_praying1", sp1);

            $("#cPray1").jsGrid("loadData");
        },
        onItemUpdated: function(args) {
            Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#p4a-cash2").val(args.item.p1.cash2);
            $("#p4a-cash2-label").addClass("active");
            $("#p4a-cash").val(args.item.p1.cash);
            $("#p4a-cash-label").addClass("active");

            var sp1 = Session.get("nowrow_praying1");
            sp1.cash = args.item.p1.cash;
            sp1.cash2 = args.item.p1.cash2;
            Session.set("nowrow_praying1", sp1);

            $("#cPray1").jsGrid("loadData");
        },
       /* onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cPray").jsGrid("loadData");
        },*/
    });

};

Template.cModalPray.helpers({
    get_livepeople: function() {
        return People.find({isLive:1});
    },
    get_passpeople: function() {
        return People.find({isLive:0});
    },
    isSelected: function (name) {
        if(name.indexOf("歷代")!= -1){
            return 1;
        }
        return 0;
    },
    get_mgyear:function () {
        // return arrMGYear;
        return Mgyear.find({}, {sort:{value: -1}});
    },
});


Template.cModalPray.fieldNpraytype = function(){
}

Template.cModalPray.events({
    "change #p4a-prayname": function(e){
        var pray1Selected = $("#p4a-prayname option:selected").val();
        // console.log(pray1Selected);
        // Session.set("pray4aSelected", pray1Selected);

        Meteor.call("getPray2", pray1Selected, function(error, result){
        // console.log(result);

            // $('select').material_select('destroy');
            $('#p4a-prayitem')
                .find('option')
                .remove()
                .end()
                .append('<option value="" disabled selected>請選擇</option>')
                // .val('whatever')
            ;
            $.each(result, function (i, item) {
                var pr = item.price || "0";
                $('#p4a-prayitem').append($('<option>', {
                    value: item._id,
                    text : item.value + " ($"+pr+")",
                    price:  item.price,
                    serial:  item.now_num,
                }));
            });
            $("#p4a-prayserial").val("");
            $("#p4a-prayserial-label").removeClass("active");
            $("#p4a-cash").val(0);
            $("#p4a-cash-label").removeClass("active");
        });
    },
    "keyup #p4a-cash1": function(e){
        // console.log("cash1");
        // console.log(Number($("#p4a-cash1").val()));
        // console.log(Number($("#p4a-cash2").val()));
        $("#p4a-cash").val( Number($("#p4a-cash1").val()) + Number($("#p4a-cash2").val()) );
    },
    "keyup #p4a-cash2": function(e){
        // console.log("cash2")
        $("#p4a-cash").val( Number($("#p4a-cash1").val()) + Number($("#p4a-cash2").val()) );
    },
    "change #p4a-prayitem": function(e){
        // console.log(e);
        $("#p4a-prayserial").val( Number($('option:selected', "#p4a-prayitem").attr('serial'))+1);
        $("#p4a-cash1").val($('option:selected', "#p4a-prayitem").attr('price'));
        $("#p4a-cash").val( Number($("#p4a-cash1").val()) + Number($("#p4a-cash2").val()) );

        $("#p4a-prayserial-label").addClass("active");
        $("#p4a-cash1-label").addClass("active");
        $("#p4a-cash2-label").addClass("active");
        $("#p4a-cash-label").addClass("active");
    },
    'click .my-btn-submit4a': function (event) { // 儲存praying1
        event.preventDefault();
        event.stopPropagation();
        // console.log("aaa");

        if($('#p4a-prayitem :selected').text() == "請選擇"){
            alert("請先選擇 法會項目 再儲存");
            return;
        }

        var formVar = $(".form-modal4a").serializeObject();
        if($('#p4a-whoapply :selected').text() != "請選擇")
            formVar.whoapply_text = $('#p4a-whoapply :selected').text();
        if($('#p4a-prayname :selected').text() != "請選擇")
            formVar.prayname_text = $('#p4a-prayname :selected').text();
        if($('#p4a-prayitem :selected').text() != "請選擇")
            formVar.prayitem_text = $('#p4a-prayitem :selected').text();

        if(formVar.prayitem_text.indexOf(" (") != -1){
            var index = formVar.prayitem_text.indexOf(" (");
            formVar.prayitem_text = formVar.prayitem_text.substr(0, index);
        }
        // console.log(formVar);

        var item = Session.get("nowrow");
        formVar.familyId = item.familyId;
        formVar.isSave = 1;
        formVar.cash = $("#p4a-cash").val();   // 總和
        formVar.cash1 = $("#p4a-cash1").val(); // 法會
        formVar.cash2 = $("#p4a-cash2").val(); // 外加

        if(Session.get("nowrow_praying1") == ""){ // 新建的
	        Meteor.call("insertPraying1", formVar, function(error, result){
	            Materialize.toast('法會服務已新增', 3000, 'rounded');

                Session.set("nowrow_pay", result.ordernum);
		        $("#cPray2-hidden").hide();
		        $("#cPray2").fadeIn();

        	    $("#ordernum-header").text(result.ordernum);
		    	$(".form4a-btn-save").attr("disabled", "disabled");

                $(".form4a-btn-del").removeAttr("disabled");
                $(".my-btn-dup4a").removeAttr("disabled");
                Session.set("nowrow_praying1", result);

                $("#cPray1").jsGrid("loadData");
                $("#cPray2").jsGrid("loadData");
	        });
        }
        else{ // 儲存舊有的
        	formVar._id = Session.get("nowrow_praying1")._id;
	        Meteor.call("updatePraying1", formVar, function(error, result){
	            Materialize.toast('法會服務已儲存', 3000, 'rounded');
                // console.log(result);

		    	$(".form4a-btn-save").attr("disabled", "disabled");

                $(".form4a-btn-del").removeAttr("disabled");
                $(".my-btn-dup4a").removeAttr("disabled");

                Session.set("nowrow_praying1", result);
                $("#cPray1").jsGrid("loadData");

                // 儲存praying1時 也要寫回session裡面
                // $("#cPray2").jsGrid("loadData");

	        });
        }
    },
    "change select": function(event, template) {
        // console.log(event.currentTarget);
        // console.log(template);
        if($(event.target).hasClass("my-praying2-add-type-sel") || $(event.target).hasClass("my-praying2-edi-type-sel")){
            var pre_class = "add";
            if($(event.target).hasClass("my-praying2-edi-type-sel")){
                pre_class = "edi";
            }

            var id = $('.my-praying2-'+pre_class+'-type-sel :selected').val();
            var obj = PrayingType.findOne(id);
            var row_total = 0;

         /*   if(obj.table_name == "-1"){
                row_total = Number(obj.price);
                $("."+pre_class+"-pray2-box_num input").val( 1 );
                $("."+pre_class+"-pray2-row_total input").val( row_total );
            }
            else{
                row_total = Number(obj.price)+ Number(obj.box_num)*500;
                $("."+pre_class+"-pray2-box_num input").val( Number(obj.box_num) );
                $("."+pre_class+"-pray2-row_total input").val( row_total );
            }*/

           row_total = Number(obj.price) || 0;
           $("."+pre_class+"-pray2-row_total input").val( row_total );
        }
    },
    'click .my-btn-new4a': function (event) {
        Template.cModalPray.newPray();
    },
    'click .my-btn-dup4a': function (event) {
        var formVar = Session.get("nowrow_praying1");

        Meteor.call("dupPraying1", formVar, function(error, result){
            $("#cPray1").jsGrid("loadData");
            $("#cPray1").jsGrid("openPage",1);
            // console.log(result);
        });
    	// Template.cModalPray.newPray();
    },
    'change .form-modal4a input, keypress .form-modal4a input': function (event) {
        $(".form4a-btn-save").removeAttr("disabled");
    },
    'change .form-modal4a select': function (event) {
    	$(".form4a-btn-save").removeAttr("disabled");
    },
    'click .my-btn-submit6b': function (event) {  // 新增按鈕的
        event.preventDefault();
        event.stopPropagation();

        var formVar = {};
        if($('.my-praying2-add-type-sel :selected').text() == "請選擇"){
            alert("請先選擇 列印種類 (消、拔…等)");
            return;
        }

        // console.log(formVar);
        var item  = Session.get("nowrow");
        var pray1 = Session.get("nowrow_praying1");

        formVar.familyId = pray1.familyId;
        formVar.listId = pray1._id;
        formVar.prayname = pray1.prayname;
        formVar.prayname_text = pray1.prayname_text;
        formVar.prayitem = pray1.prayitem;
        formVar.prayitem_text = pray1.prayitem_text;
        formVar.year = pray1.year;
        formVar.prayserial = pray1.prayserial;
        formVar.ordernum = pray1.ordernum;
        formVar.whoapply = pray1.whoapply;
        formVar.whoapply_text = pray1.whoapply_text;

        formVar.type =  $(".my-praying2-add-type-sel").val();
        formVar.type_text =  $('.my-praying2-add-type-sel :selected').text();
        if(formVar.type_text.indexOf(" (") != -1){
            var index = formVar.type_text.indexOf(" (");
            formVar.type_text = formVar.type_text.substr(0, index);
        }
        var p2 = Pray2.findOne(formVar.prayitem);
        formVar.pray2_orderid = Number(p2.order_id);
        var pt = PrayingType.findOne(formVar.type);
        formVar.prayingtype_orderid = Number(pt.order);

        formVar.row_total =  Number($(".add-pray2-row_total input").val());
        formVar.livename_text =  $(".add-pray2-livename input").val();
        formVar.passname_text =  $(".add-pray2-passname input").val();
        formVar.passname_text1 = $(".add-pray2-passname1 input").val();
        formVar.passname_text2 = $(".add-pray2-passname2 input").val();
        formVar.passname_text3 = $(".add-pray2-passname3 input").val();
        formVar.passname_text4 = $(".add-pray2-passname4 input").val();
        formVar.addr           = $(".add-pray2-addr input").val();

        // console.log(formVar);
        // return;

        $("#cPray2").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                $("#cPray2").jsGrid("loadData");

                $(".my-praying2-add-type-sel").val("");
                $(".add-pray2-livename input").val("");
                $(".add-pray2-row_total input").val("");
                $(".add-pray2-passname input").val("");
                $(".add-pray2-passname1 input").val("");
                $(".add-pray2-passname2 input").val("");
                $(".add-pray2-passname3 input").val("");
                $(".add-pray2-passname4 input").val("");
                $(".add-pray2-addr input").val("");
            }
            else {
                $(".modal4a-error").text(ret);
            }
        });
    },
    'click .my-btn-submit6b2': function (event) {  // 新增各別消災
        event.preventDefault();
        event.stopPropagation();

        // $('#modal4b1').closeModal();
        $('#modal4b1').toggle();
        var formVar = $(".form-modal4b1").serializeObject();

        if(typeof formVar.name2 === "undefined"){
            alert("請勾選要代入的對象");
            return;
        }
        else if(typeof formVar.name2 === "string"){
            var str = formVar.name2;
            formVar.name2 = [];
            formVar.name2.push(str);
        }
        var arr_name = formVar.name2;
        delete formVar.name1;
        delete formVar.name2;

        // console.log(formVar);
        var item  = Session.get("nowrow");
        var pray1 = Session.get("nowrow_praying1");

        formVar.familyId = pray1.familyId;
        formVar.listId = pray1._id;
        formVar.prayname = pray1.prayname;
        formVar.prayname_text = pray1.prayname_text;
        formVar.prayitem = pray1.prayitem;
        formVar.prayitem_text = pray1.prayitem_text;
        formVar.year = pray1.year;
        formVar.prayserial = pray1.prayserial;
        formVar.ordernum = pray1.ordernum;
        formVar.whoapply = pray1.whoapply;
        formVar.whoapply_text = pray1.whoapply_text;


        formVar.type = $('.my-praying2-'+Session.get("modalPraying2")+'-type-sel :selected').val();
        formVar.type_text =  $('.my-praying2-'+Session.get("modalPraying2")+'-type-sel :selected').text();
        if(formVar.type_text.indexOf(" (") != -1){
            var index = formVar.type_text.indexOf(" (");
            formVar.type_text = formVar.type_text.substr(0, index);
        }

        var p2 = Pray2.findOne({_id: formVar.prayitem});
        formVar.pray2_orderid = Number(p2.order_id);
        var pt = PrayingType.findOne(formVar.type);
        formVar.prayingtype_orderid = Number(pt.order);

        formVar.passname_text  = $(".add-pray2-passname  input").val();
        formVar.passname_text1 = $(".add-pray2-passname1 input").val();
        formVar.passname_text2 = $(".add-pray2-passname2 input").val();
        formVar.passname_text3 = $(".add-pray2-passname3 input").val();
        formVar.passname_text4 = $(".add-pray2-passname4 input").val();
        formVar.row_total      =  Number($(".add-pray2-row_total input").val());

        // console.log("formVar");
        // console.log(formVar);
        Meteor.call("addMultiPraying2", arr_name, formVar, function(error, result){
            if(error){}
            else{
                Materialize.toast('資料已更新', 3000, 'rounded')
                // console.log(result);
                $("#p4a-cash2").val(result.cash2);
                $("#p4a-cash2-label").addClass("active");
                $("#p4a-cash").val(result.cash); // 應收
                $("#p4a-cash-label").addClass("active");

                var sp1 = Session.get("nowrow_praying1");
                sp1.cash = result.cash;
                sp1.cash2 = result.cash2;
                Session.set("nowrow_praying1", sp1);

                // $(".title-cash").text( $("#p4a-cash").val() );

                $("#cPray1").jsGrid("loadData");
                $("#cPray2").jsGrid("loadData");
            }
        });

        $(".my-praying2-add-type-sel").val("");
        $(".add-pray2-row_total input").val("");
        $(".add-pray2-livename input").val("");
        $(".add-pray2-passname input").val("");
        $(".add-pray2-passname1 input").val("");
        $(".add-pray2-passname2 input").val("");
        $(".add-pray2-passname3 input").val("");
        $(".add-pray2-passname4 input").val("");
        $(".add-pray2-addr input").val("");
    },
    'click .my-btn-submit4b': function (event) { // 原本舊的上面的新增 就只多空的一列出來而已
        event.preventDefault();
        event.stopPropagation();
        var formVar = {};

        // console.log(formVar);
        var item  = Session.get("nowrow");
        var pray1 = Session.get("nowrow_praying1");

        formVar.listId = pray1._id;
        formVar.prayname = pray1.prayname;
        formVar.prayname_text = pray1.prayname_text;
        formVar.prayitem = pray1.prayitem;
        formVar.prayitem_text = pray1.prayitem_text;
        formVar.year = pray1.year;
        formVar.prayserial = pray1.prayserial;
        formVar.ordernum = pray1.ordernum;

        $("#cPray2").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                $("#cPray2").jsGrid("loadData");
            }
            else {
                $(".modal4a-error").text(ret);
            }
        });
    },
    'click .my-btn-submit4b1': function (event) {
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal4b1").serializeObject();

        // console.log(formVar);
        var addr = People.find({name: formVar.name1}).fetch()[0].addr;
        // console.log(addr);

        var name = formVar.name1;
        if(!!formVar.name2 && formVar.name2.length ){
        	if(typeof formVar.name2 != "string"){
	        	var arr = [];

	            for(var i in formVar.name2){
	                if(formVar.name2[i] != name){
	                    // delete arr[i];
	                    arr.push(formVar.name2[i]);
	                }
	            }
	            // console.log(arr);

	            if(arr.length > 0){
	                name = name + "," + arr.toString();
	            }
        	}
        	else{
        		if(name != formVar.name2){
    	    		name = name + "," + formVar.name2;
        		}
        	}

        }
        /// 接著用Session.get("modalPraying2") 來讀是edi或是add的值 再去更改裡面的東西
        if(name){
			$(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-livename input").val(name);
        }

        var row_addr = $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val();
        var row_type_text = $('.my-praying2-add-type-sel :selected').text();
        if(addr && row_type_text.indexOf("消") != -1 ){ // 是消的話 就一定換新的地址
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val(addr);
        }
        else if(addr && !row_addr){
			$(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val(addr);
        }
        // $('#modal4b1').closeModal();
        $('#modal4b1').toggle();
    },
    'click .my-btn-submit4c0': function (event) {
        event.preventDefault();
        event.stopPropagation();

        var name = $(event.currentTarget).attr("data-val");
        if(name){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname input").val(name);
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname1 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname2 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname3 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname4 input").val("");
        }

        var addr = People.find({name: name}).fetch()[0].addr;
        if(addr){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val(addr);
        }
        // $('#modal4b2').closeModal();
        $('#modal4b2').toggle();
    },
    'click .my-btn-submit4c1': function (event) { // 系統預設的
        event.preventDefault();
        event.stopPropagation();

        var name = $(event.currentTarget).attr("data-val");
        if(name){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname1 input").val(name);
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname2 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname3 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname4 input").val("");
        }
        var addr = $(event.currentTarget).attr("data-addr");
        if(addr){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val(addr);
        }
        // else{
        //     $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val("");
        // }
        // $('#modal4b2').closeModal();
        $('#modal4b2').toggle();
    },
    'click .my-btn-submit4c2': function (event) { // 系統預設的
        event.preventDefault();
        event.stopPropagation();

        var name = $(event.currentTarget).attr("data-val");
        if(name){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname2 input").val(name);
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname1 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname3 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname4 input").val("");
        }
        var addr = $(event.currentTarget).attr("data-addr");
        if(addr){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val(addr);
        }
        // else{
        //     $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val("");
        // }
        // $('#modal4b2').closeModal();
        $('#modal4b2').toggle();
    },
    'click .my-btn-submit4c3': function (event) { // 系統預設的
        event.preventDefault();
        event.stopPropagation();

        var name = $(event.currentTarget).attr("data-val");
        if(name){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname3 input").val(name);
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname2 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname1 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname4 input").val("");
        }
        var addr = $(event.currentTarget).attr("data-addr");
        if(addr){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val(addr);
        }
        // else{
        //     $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val("");
        // }
        // $('#modal4b2').closeModal();
        $('#modal4b2').toggle();
    },
    'click .my-btn-submit4c4': function (event) { // 系統預設的
        event.preventDefault();
        event.stopPropagation();

        var name = $(event.currentTarget).attr("data-val");
        if(name){
	    $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname4 input").val(name);
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname2 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname3 input").val("");
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-passname1 input").val("");
        }
        var addr = $(event.currentTarget).attr("data-addr");
        if(addr){
            $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val(addr);
        }
        // else{
        //     $(".jsgrid-edit-row ."+Session.get("modalPraying2")+"-pray2-addr input").val("");
        // }
        // $('#modal4b2').closeModal();
        $('#modal4b2').toggle();
    },
    'click .my-open-modal8': function() { // 付款
        var num = Session.get("nowrow_pay");
        if(Session.get("isClose")==0 && !confirm("服務 "+num+" 是否要關帳?")){
            return;
        }
        if(Session.get("isClose")==0){
            Meteor.call("closeAccount", 1, num, function(error, result){
                if(error){}
                else{
                    Session.set("isClose", 1);
                    Materialize.toast('已成功關帳', 3000, 'rounded');
                    $("#cPray1").jsGrid("loadData");
                    Template.cModalPray.fieldNpraytype();
                    Template.modalPay();
                    $('#modalPay').toggle();
                }
            });
        }
        else{ // 之前已經關帳的
            Template.cModalPray.fieldNpraytype();
            Template.modalPay();
            $('#modalPay').toggle();
        }
    },
    'click .my-btn-cancel8': function() {
        // $('#modalPay').closeModal();
        $('#modalPay').toggle();
    },
    'click .my-open-modal10': function() { // 繳款單
        var formVar = "/api/bill?"+$(".form-modal4a").serialize();
        $("#pdf-iframe").prop("src", formVar);
        $("#pdf_url").prop("href", formVar);
        $("#modal1-header-text").text("繳款單 - " + Session.get("nowrow_pay"));
        $('#modal1').toggle();
    },
    'click .my-btn-cancel10': function() {
        // $('#modal1').closeModal();
        $('#modal1').toggle();
    },
    'click .my-open-modal12': function() { // 收據
        var formVar = "/api/receipt?"+$(".form-modal4a").serialize();
        $("#pdf-iframe").prop("src", formVar);
        $("#pdf_url").prop("href", formVar);
        $("#modal1-header-text").text("收據(感謝狀) - " + Session.get("nowrow_pay"));
        $('#modal1').toggle();
    },
    'click .my-open-modal4a': function() {
    	Template.cModalPray.newPray();

        $('#modal4a').toggle();
        $("#cPray1").jsGrid("loadData");
    },
    'click .my-btn-cancel4a': function() {
        // $('#modal4a').closeModal();
        $('#modal4a').toggle();
    },
    'click .my-btn-delete4a': function() {
        if(confirm("確定刪除法會服務："+ Session.get("nowrow_praying1").ordernum+ " 嗎?")){
            var item = Session.get("nowrow_praying1")._id;
            Meteor.call("deletePraying1", item, function(){
                $("#cPray1").jsGrid("loadData");
                // $(".my-praying2-detail").hide();
				Template.cModalPray.newPray();
            });
        }
    },
    'click .my-open-modal4b': function() {
        $(".form-modal4b").trigger('reset');
        $("#p4a-livename_text-label").removeClass("active");
        $("#p4a-passname_text-label").removeClass("active");
        $("#p4a-addr-label").removeClass("active");
        $('#modal4b').toggle();
    },
    'click .my-btn-cancel4b': function() {
        // $('#modal4b').closeModal();
        $('#modal4b').toggle();
    },
    'click .my-open-modal4b1': function() {
        // console.log(event.target.className);
        Session.set("modalPraying2", "edi");
        $(".form-modal4b1").trigger('reset');
        $('#modal4b1').toggle();
        $(".type-sel-text").text($('.my-praying2-edi-type-sel :selected').text());
        var main = People.find({mainPerson:-1}).fetch()[0]._id;
        $('#test1-'+main).attr('checked', true);
        $(".my-btn-submit6b2").hide();
    },
    'click .my-open-modal6b1': function(event) {
        // console.log(event.target.className);

        if($('.my-praying2-add-type-sel :selected').text() == "請選擇"){
            alert("請先選擇 列印種類 (消、拔…等)");
            return;
        }
        Session.set("modalPraying2", "add");
        $(".form-modal4b1").trigger('reset');
        $('#modal4b1').toggle();
        $(".type-sel-text").text($('.my-praying2-add-type-sel :selected').text());
        var main = People.find({mainPerson:-1}).fetch()[0]._id;
        $('#test1-'+main).attr('checked', true);
        $(".my-btn-submit6b2").show();
    },
    'click .my-btn-cancel4b1': function() {
        // $('#modal4b1').closeModal();
        $('#modal4b1').toggle();
    },
    'click .my-open-modal4b2': function(event) {
        Session.set("modalPraying2", "edi");
        $(".form-modal4b2").trigger('reset');
        // if(People.find({isLive:0}).count() == 0){
        //     alert("請先在家庭清單中建立超渡對象");
        // }
        // else{
        //     $('#modal4b2').toggle();
        // }
        $('#modal4b2').toggle();

    },
    'click .my-open-modal6b2': function(event) {
        Session.set("modalPraying2", "add");
        $(".form-modal4b2").trigger('reset');
        /*if(People.find({isLive:0}).count() == 0){
            alert("請先在家庭清單中建立超渡對象");
        }
        else{
            $('#modal4b2').toggle();
        }*/
        $('#modal4b2').toggle();

    },
    'click .my-btn-cancel4b2': function() {
        // $('#modal4b2').closeModal();
        $('#modal4b2').toggle();
    },

});
