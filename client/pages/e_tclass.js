Template.eTclass.rendered = function() {
    $("#eTclass").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.sortField = "order";
                filter.sortOrder = "asc";

                return jsgridAjax(filter, "templeclass", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "templeclass", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "templeclass", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "templeclass", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60 },
            { name: "order", title: "順序", width: 70, editing: false },
            { name: "value", title: "名稱", type: "text", width: 200 },
            // { name: "ps", title: "備註", type: "text", width: 200 },
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#eTclass").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#eTclass").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#eTclass .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    Meteor.call("updateSortTempleclass", items, function(error, result){
                        if(error){
                            console.log("error from updateSortTempleclass: ", error);
                        } 
                        else {
                            $("#eTclass").jsGrid("loadData");
                        }
                    });
                }
            });
        },
    });
    $('.modal select').material_select();
};

Template.eTclass.helpers({
    get_countries: function() {
        return objCountries;
    },
});

Template.eTclass.events({
    // 'click .my-btn-submit': function () {
    'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal2").serializeObject();


        $("#eTclass").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#modal2').toggle();
                $(".form-modal2").trigger('reset');
                $("#eTclass").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .my-open-modal2': function() {
        // $('#modal2').openModal();
        $('#modal2').toggle();
    },
    //  'click .label-provider': function () {
    // $('#modal2').closeModal();
    // Meteor.setTimeout(function(){ Router.go('e_provider'); }, 10); 
    //  },
    'click .my-btn-cancel': function() {
        // $('#modal2').closeModal();
        $('#modal2').toggle();
    }
});
