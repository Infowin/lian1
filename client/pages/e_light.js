Template.eLight.rendered = function() {
    $("#eLight1").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        // rowClick: $.noop,
        rowClick: function(args) {
            // console.log(args);
            $("#show-detail").hide();
            
            Session.set("nowrow_Light", args.item);
            $("#eLight2").jsGrid("loadData", { type_id: args.item._id});

            $("#show-detail").fadeIn("slow");

        },
        rowDoubleClick: jsGrid.Grid.prototype.rowClick,
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "light1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "light1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "light1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "light1", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60 },
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
           /* { name: "now_use", type: "checkbox", title: "預設",
                itemTemplate: function(value, item) { 
                    // console.log(item);
                    if(item.now_use == "true")
                        return '<input type="checkbox" checked="checked" disabled="disabled">';
                    else
                        return '<input type="checkbox" disabled="disabled">';
                },
            },*/
            { name: "value", title: "名稱", type: "text" },
            { name: "now_id", title: "目前編號", type: "text" },
            { name: "order_id", title: "順序", type: "number", editing: false, width:50, align: "center" },
            // { name: "ps", title: "備註", type: "text", width: 200 },
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#eLight1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#eLight").jsGrid("loadData");
        },
    });    

    $("#eLight2").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "/api/light2",
                    data: filter,
                    dataType: "json"
                });
            },
            insertItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "/api/light2",
                    data: item,
                    dataType: "json"
                });
            },
            updateItem: function(item) {
                // console.log(item);
                // item.fullname = item.countryname + " - " + item.value;
                return $.ajax({
                    type: "PUT",
                    url: "/api/light2",
                    data: item,
                    dataType: "json"
                });
            },
            deleteItem: function(item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/api/light2",
                    data: item,
                    dataType: "json"
                });
            },
        },

        /*
          { id:"",  header:"順序", width:55 },
      { id:"",     editor:"text",  header:"名稱", fillspace:1},
      { id:"now_num",   editor:"text",  header:"目前編號", width:90 },
         */
        fields: [
            { type: "control", width: 60 },
            { name: "name", title: "編號種類", type: "text" },
            { name:"start_num",   type:"text",  title:"開始編號" },
            { name:"end_num",   type:"text",  title:"結束編號" },
            { name: "order_id", title: "順序", width: 60, type:"number", align:"center" },
            // { name:"Light_paper", type:"select", title:"列印紙張", items:objPaperSize, valueField: "id", textField: "value"  },
            // { name: "ps", title: "備註", type: "text", width: 200 },
            { type: "control" }
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        // onItemUpdating: function(args) {
        //     console.log(args);
        //     var item = Session.get("nowrow_Light");
        // },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            // $("#eLight2").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#eLight").jsGrid("loadData");
            var item = Session.get("nowrow_Light");
            $("#eLight2").jsGrid("loadData", { type_id: item._id});
        },
    });
    $('.modal select').material_select();
    $("#show-detail").hide();
};

Template.eLight.helpers({
    get_countries: function() {
        return objCountries;
    },
    show_name: function(){
        var item = Session.get("nowrow_Light");
        if(!!item && !!item.value) 
            return item.value;
        return "";
    },
    show_name_id: function(){
        var item = Session.get("nowrow_Light");
        if(!!item && !!item._id) 
            return item._id;
        return "";
    }
});

Template.eLight.events({
    'click .my-btn-submit1': function () {
        var formVar = $(".form-modal1").serializeObject();

        $("#eLight1").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                $('#modal1').closeModal();
                $('#modal1').toggle();
                $(".form-modal1").trigger('reset');
                $("#eLight1").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    'click .my-open-modal1': function() {
        // $('#modal1').openModal();
        $('#modal1').toggle();
    },
    'click .my-btn-cancel1': function() {
        // $('#modal1').closeModal();
        $('#modal1').toggle();
    },
    'click .my-btn-submit2': function () {
        var formVar = $(".form-modal2").serializeObject();

        $("#eLight2").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#modal2').toggle();
                $(".form-modal2").trigger('reset');
                
                var item = Session.get("nowrow_Light");
                $("#eLight2").jsGrid("loadData", { type_id: item._id});
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .my-open-modal2': function() {
        // $('#modal2').openModal();
        $('#modal2').toggle();
    },
    'click .my-btn-cancel2': function() {
        // $('#modal2').closeModal();
        $('#modal2').toggle();
    }
});
