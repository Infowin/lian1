Template.cAccounting.rendered = function() {
  $("#cPraying1").jsGrid({
    height: "90%",
    width: "100%",

    sorting: true,
    paging: true,
    // filtering: true,
    // editing: true,

    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        filter.cashier = 1;
        if (!filter.sortField) {
          filter.sortField = "ordernum";
          filter.sortOrder = "desc";
        }
        var pay = $("option:selected", "#cashier-pay-status").val();
        var year = $("option:selected", "#cashier-year").val();
        var prayname = $("option:selected", "#cashier-pray1").val();
        var prayitem = $("option:selected", "#cashier-pray2").val();

        if (pay) {
          filter.paystatus = pay;
        }
        if (year) {
          filter.year = year;
        }
        if (prayname) {
          filter.prayname = prayname;
        }
        if (prayitem) {
          filter.prayitem = prayitem;
        }

        return jsgridAjax(filter, "praying1", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "praying1", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "praying1", "PUT");
      },
      deleteItem: function(item) {
        return jsgridAjax(item, "praying1", "DELETE");
      }
    },
    fields: [
      // { type: "control" },
      { name: "ordernum", title: "單號", width: 60 },
      { name: "year", title: "年度", width: 30, align: "center" },
      // { name: "prayname_text", title: "法會名稱", width: 60 },
      // { name: "prayitem_text", title: "法會項目", width: 60 },
      {
        name: "service",
        title: "服務項目",
        itemTemplate: function(value, item) {
          if (!!item.prayname_text)
            return (
              item.prayname_text +
              " " +
              item.prayitem_text +
              " #" +
              item.prayserial
            );
          else return "";
        }
      },
      { name: "date", title: "建立日期", type: "text", width: 80 },
      // { name: "paymethod_text", title: "收款方式", type: "text", width: 70},
      { name: "cash", title: "服務金額", type: "text", width: 70 },
      { name: "cash_paid", title: "已收金額", type: "text", width: 70 },
      { name: "cash_balance", title: "剩餘金額", type: "text", width: 70 },
      // { name: "user_name", title: "經辦人", type: "text", width: 60},
      // { name: "insertedAt", title: "登錄時間", //width: 150,
      //     itemTemplate: function(value, item) {
      //         if(!!item.insertedAt)
      //             return (new Date(item.insertedAt)).yyyymmddhm();
      //         else
      //             return "";
      //     },
      // },
      { name: "whoapply_text", title: "申請人", type: "text", width: 70 }
    ],
    onDataLoading: function(args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function(args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function(args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      console.log(args);
      $("#cCashier").jsGrid("loadData");
    },
    onItemDeleted: function(args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#cCashier").jsGrid("loadData");
    }
  });
  $(".modal select").material_select();
  $("select").material_select();
};

Template.cAccounting.helpers({
  get_productclass: function() {
    return objProductClass;
  },
  get_providers: function() {
    return Provider.find();
  },
  get_year: function() {
    // return arrMGYear;
    return Mgyear.find({}, { sort: { value: -1 } });
  },
  get_pray1: function() {
    return Pray1.find({}, { sort: { order_id: 1 } });
  }
});

Template.cAccounting.events({
  "change #cashier-year": function() {
    $("#cPraying1").jsGrid("loadData");
  },
  "change #cashier-pray1": function() {
    var sel_pray1_id = $("#cashier-pray1 option:selected").val();

    Meteor.call("getPray2", sel_pray1_id, function(error, result) {
      $("select").material_select("destroy");
      $("#cashier-pray2")
        .find("option")
        .remove()
        .end()
        .append('<option value="" selected>請選擇</option>');
      // .val('whatever')
      $.each(result, function(i, item) {
        // if(!!item.value)
        $("#cashier-pray2").append(
          $("<option>", {
            value: item._id,
            text: item.value,
            price: item.price,
            serial: item.now_num
          })
        );
      });
      // $("#p4a-prayitem").val(item.prayitem);
      $("select").material_select();
    });
    $("#cPraying1").jsGrid("loadData");
  },
  "change #cashier-pray2": function() {
    $("#cPraying1").jsGrid("loadData");
  },
  "change #cashier-pay-status": function() {
    $("#cPraying1").jsGrid("loadData");
  },
  "click .my-open-modal8": function(event) {
    // 付款
    var ordernum = $(event.currentTarget).attr("data-ordernum");
    Session.set("nowrow_pay", ordernum);

    var cash = {};
    cash.cash = Number($(event.currentTarget).attr("data-cash"));
    cash.cash_paid = Number($(event.currentTarget).attr("data-cash_paid"));
    cash.cash_balance = Number(
      $(event.currentTarget).attr("data-cash_balance")
    );

    Template.modalPay(cash);
    $("#modalPay").openModal();
  },
  "click .my-btn-cancel8": function() {
    $("#modalPay").closeModal();
  },
  "click .my-open-modal10": function() {
    // 繳款單
    var formVar = "/api/bill?" + $(".form-modal4a").serialize();
    $("#pdf-iframe").prop("src", formVar);
    $("#pdf_url").prop("href", formVar);
    $("#modal1-header-text").text("繳款單 - " + Session.get("nowrow_pay"));
    $("#modal1").openModal();
  },
  "click .my-btn-cancel10": function() {
    $("#modal1").closeModal();
  },
  "click .my-open-modal12": function() {
    // 收據
    var formVar = "/api/receipt?" + $(".form-modal4a").serialize();
    $("#pdf-iframe").prop("src", formVar);
    $("#pdf_url").prop("href", formVar);
    $("#modal1-header-text").text("收據(感謝狀) - " + Session.get("nowrow_pay"));
    $("#modal1").openModal();
  }
});
