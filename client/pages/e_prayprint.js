Template.ePrayprint.rendered = function() {
    $("#ePrayprint").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        // paging: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "prayingtype", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "prayingtype", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "prayingtype", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "prayingtype", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60 },
            { name: "order", title: "順序", type: "number", width: 70, editing: false },
            // { name: "isopen", type: "checkbox", title: "開啟" },
            { name: "value", title: "列印名稱", type: "text" },
            { name: "price", title: "外加價錢", type: "text" },
            { name: "only_pray", title: "適用法會", width:70, type: "select", items: [{_id:"",value:"全部"},{_id:"0",value:"無"}].concat(Pray1.find({},{sort:{order: 1}}).fetch()), valueField: "_id", textField: "value" },
            { name: "print_type", title: "列印種類", type: "select", items: [{ id: "", value: "" }].concat(objPrintType), valueField: "id", textField: "value" },
            { name: "def_value", title: "拔 預設值", type: "text" },
            { name: "oilpray_paper", title: "用紙", type: "select", items: [{ id: "", value: "" }].concat(objPaperSize), valueField: "id", textField: "value" },
            { name: "oilpray_layout", title: "方向", type: "select", items: [{ id: "", value: "" }].concat(objPaperLayout), valueField: "id", textField: "value" },
            { name: "oilpray_x", title: "寬度數", type: "select", items: [{ id: "", value: "" }].concat(objOilPrayX), valueField: "id", textField: "value" },
            { name: "oilpray_y", title: "長度數", type: "select", items: [{ id: "", value: "" }].concat(objOilPrayY), valueField: "id", textField: "value" },
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#ePrayprint").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#ePrayprint").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#ePrayprint .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    Meteor.call("updateSortPrayingtype", items, function(error, result){
                        if(error){
                            console.log("error from updateSortPrayingtype: ", error);
                        } 
                        else {
                            $("#ePrayprint").jsGrid("loadData");
                        }
                    });
                }
            });
        },
    });
    $('.modal select').material_select();
};

Template.ePrayprint.helpers({
    // get_countries: function() {
    //     return objCountries;
    // },
});

Template.ePrayprint.events({
    // 'click .my-btn-submit': function () {
    'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal2").serializeObject();


        $("#ePrayprint").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#modal2').toggle();
                $(".form-modal2").trigger('reset');
                $("#ePrayprint").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .my-open-modal2': function() {
        // $('#modal2').openModal();
        $('#modal2').toggle();
    },
    //  'click .label-provider': function () {
    // $('#modal2').closeModal();
    // Meteor.setTimeout(function(){ Router.go('e_provider'); }, 10); 
    //  },
    'click .my-btn-cancel': function() {
        // $('#modal2').closeModal();
        $('#modal2').toggle();
    }
});
