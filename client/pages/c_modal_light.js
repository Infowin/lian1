Template.cModalLight.newLight = function(){
	$("#ordernum-header2").text("新建");
    Session.set("nowrow_lighting1", "");

	$(".form8a-btn-del").attr("disabled", "disabled");
	$(".form8a-btn-save").removeAttr("disabled");

	// $("#cLight2").hide();
	// $("#cLight2-hidden").show();

    $(".form-modal8a").trigger('reset');

    $('#p8a-date').val(currentdate());
    
    // $('#p8a-year').val(nextMGyear());
    $('#p8a-year').val(Mgyear.findOne({now_use: "-1"}).value);

    $("#cLight2").jsGrid("loadData");
    // $('select').material_select();  
}
Template.cModalLight.rendered = function() {
    $("#cLight1").jsGrid({
        height: "90%",
        width: "100%",
 
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 4,

        loadIndication: true,
        // autoload: true,
        pageLoading: true, 
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                var item = Session.get("nowrow");
                filter.familyId = item.familyId;

                if(!!filter.sortField){
                    filter.sortField = "ordernum";
                    filter.sortOrder = "desc";
                }

                return jsgridAjax(filter, "lighting1add", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "lighting1add", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "lighting1add", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "lighting1add", "DELETE");
            },
        },
        fields: [
            { name: "ordernum", title: "編號", width:60 },
            { name: "year", title: "年度", width:40 },
            { name: "price_total", title: "應收金額", width:70 },
            { name: "date", title: "申請日", width:70 },
            { name: "apply_text", title: "申請人", width:70 },
            // { type: "control" }
        ],
        rowClick: function(args) {
            Session.set("nowrow_lighting1", args.item);

            // $('select').material_select('destroy');
            var item = args.item;
            // $("#p8a-ordernum").val(item.ordernum);
            // $("#p1-ordernum").text(item.ordernum);
            $("#ordernum-header2").text(item.ordernum);

            $("#p8a-date").val(item.date);
            $("#p8a-year").val(item.year);
            $("#p8a-lightname").val(item.lightname);

            $("#p8a-lightserial").val(item.lightserial);
            $("#p8a-lightserial-label").addClass("active");

            $("#p8a-whoapply").val(item.apply_id);
            $("#p8a-cash").val(item.price_total);
            $("#p8a-cash-label").addClass("active");
            

	    	$(".form8a-btn-save").attr("disabled", "disabled");
	    	$(".form8a-btn-del").removeAttr("disabled");


            $(".my-lighting2-detail").fadeIn();

            // Meteor.call("getIsPaid", item._id, function(error, result){
            //     // console.log(result);
            //     if(result == 0){
            //         $("#show-acc-insert").fadeIn();
            //     }
            //     else{
            //         $("#show-acc-insert").hide();
            //     }

            // });

            $("#cLight2").jsGrid("loadData");
            // $("#cBook").jsGrid("loadData");
        },
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
 /*       onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#cLight1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cLight1").jsGrid("loadData");
        },*/
    });

    $("#cLight2").jsGrid({
        height: "90%",
        width: "100%",
 
        // sorting: true,
        // paging: true,
        // filtering: true,
        // editing: true,
        // pageSize: 10,

        loadIndication: true,
        // autoload: true,
        pageLoading: true, 
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                // filter.clientid = $("#show_uid").text();
                // console.log("cLight2 filter");
          
                // var item = Session.get("nowrow_lighting1");
                // filter.listId = item._id;
                // console.log(filter);

                var item = Session.get("nowrow");
                filter.familyId = item.familyId;

                var s_l1 = Session.get("nowrow_lighting1");
                if(!s_l1){
                    filter.isSave = 0;
                }
                else{
                    filter.isSave = 1;
                    filter.lighting1 = s_l1._id;
                }

                return jsgridAjax(filter, "lighting2add", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "lighting2add", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "lighting2add", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "lighting2add", "DELETE");
            },
        },
        fields: [
            { name: "people_name", title: "名字",},
            { name: "lunar_birth_text", title: "生日", },
            { name: "lunar_time_text", title: "時辰", width:60},
            { name: "p1", title: "光明燈", width: 60, align: "center",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    var checked = "";
                    if(item.p1 == '-1'){
                        checked = "checked";
                    }

                    var ret = '<INPUT TYPE="hidden" name="lighting1" id="lighting1-'+item._id+'" value="'+item.lighting1+'">';
                    ret += '<INPUT TYPE="hidden" name="people_name" id="people_name-'+item._id+'" value="'+item.people_name+'">';
                    ret += '<INPUT TYPE="hidden" name="id" id="id-'+item._id+'" value="'+item._id+'">';
                    ret += '<INPUT class="lighting2-p1" TYPE="checkbox" name="p1" id="p1-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                    return ret;
                }
            },
            { name: "p1_2", title: "副", width: 40, align: "center",
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p1_2 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT class="lighting2-p1_2" TYPE="checkbox" name="p1_2" id="p1_2-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                }
            },
            { name: "p1_isallfamily", title: "闔家", width: 60, align: "center",
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p1_isallfamily == '-1'){
                        checked = "checked";
                    }
                    var ret = '<INPUT class="lighting2-p1_isallfamily" TYPE="checkbox" name="p1_isallfamily" id="p1_isallfamily-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                    return ret;
                },
            },
            { name: "p1_name2", title: "第二位",
                itemTemplate: function(value, item) {
                    var str = item.p1_name2 || "";
                    return '<INPUT class="lighting2-p1_name2" TYPE="text" name="p1_name2" id="p1_name2-'+item._id+'" value="'+str+'">'
                },
            },
            { name: "p1_num", title: "燈號",
                itemTemplate: function(value, item) {
                    var str = item.p1_num || "";
                    return '<INPUT class="lighting2-p1_num" TYPE="text" name="p1_num" id="p1_num-'+item._id+'" value="'+str+'">'
                },
            },
            { name: "p3", title: "藥師燈", align: "center", width: 60,
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p3 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT class="lighting2-p3" TYPE="checkbox" name="p3" id="p3-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                }
            },
            // { name: "p3_2", title: "副", width: 40, align: "center",
            { name: "p3_isp3", title: "副", width: 40, align: "center",
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p3_isp3 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT class="lighting2-p3_isp3" TYPE="checkbox" name="p3_isp3" id="p3_isp3-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                }
            },
            { name: "p3_name2", title: "其他位", 
                itemTemplate: function(value, item) {
                    var str = item.p3_name2 || "";
                    return '<INPUT class="lighting2-p3_name2" TYPE="text" name="p3_name2" id="p3_name2-'+item._id+'" value="'+str+'">'
                },
            },
            { name: "p3_num", title: "燈號",
                itemTemplate: function(value, item) {
                    var str = item.p3_num || "";
                    return '<INPUT class="lighting2-p3_num" TYPE="text" name="p3_num" id="p3_num-'+item._id+'" value="'+str+'">'
                },
            },
            { name: "p5", title: "光明大燈", align: "center", width: 60,
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p5 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT class="lighting2-p5" TYPE="checkbox" name="p5" id="p5-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                }
            },
            // { name: "p5_2", title: "副", width: 40, align: "center",
            { name: "p5_isp5", title: "副", width: 40, align: "center",
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p5_isp5 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT class="lighting2-p5_isp5" TYPE="checkbox" name="p5_isp5" id="p5_isp5-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                }
            },
            { name: "p5_name2", title: "其他位",
                itemTemplate: function(value, item) {
                    var str = item.p5_name2 || "";
                    return '<INPUT class="lighting2-p5_name2" TYPE="text" name="p5_name2" id="p5_name2-'+item._id+'" value="'+str+'">'
                },
            },
            { name: "p5_num", title: "燈號",
                itemTemplate: function(value, item) {
                    var str = item.p5_num || "";
                    return '<INPUT class="lighting2-p5_num" TYPE="text" name="p5_num" id="p5_num-'+item._id+'" value="'+str+'">'
                },
            },
            { name: "p7", title: "藥師大燈", align: "center", width: 60,
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p7 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT class="lighting2-p7" TYPE="checkbox" name="p7" id="p7-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                }
            },
            // { name: "p7_2", title: "副", width: 40, align: "center",
            { name: "p7_isp7", title: "副", width: 40, align: "center",
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p7_isp7 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT class="lighting2-p7_isp7" TYPE="checkbox" name="p7_isp7" id="p7_isp7-'+item._id+'" data-id="'+item._id+'" value="'+item._id+'" '+checked+'>';
                }
            },
            { name: "p7_name2", title: "其他位",
                itemTemplate: function(value, item) {
                    var str = item.p7_name2 || "";
                    return '<INPUT class="lighting2-p7_name2" TYPE="text" name="p7_name2" id="p7_name2-'+item._id+'" value="'+str+'">'
                },
            },
            { name: "p7_num", title: "燈號",
                itemTemplate: function(value, item) {
                    var str = item.p7_num || "";
                    return '<INPUT class="lighting2-p7_num" TYPE="text" name="p7_num" id="p7_num-'+item._id+'" value="'+str+'">'
                },
            },
            { name: "addr", title: "地址", type: "text", width: 300},
        ],
        // rowClick: function(args) {
        //     // console.log(args);
        // },
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onDataLoaded: function(args) {
            // $('.jsgrid select').material_select();
            // console.log("onRefreshed");
            var is_any_p1_checked = false;
            $("input[name='p1']").each(function() {
              if($(this).prop("checked") ==  true){
                is_any_p1_checked = true;
                return false;
              }
            });
            if(is_any_p1_checked == false){
                $("input[name='p1_2']").each(function() {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", true);
                });
            }

            var is_any_p3_checked = false;
            $("input[name='p3']").each(function() {
              if($(this).prop("checked") ==  true){
                is_any_p3_checked = true;
                return false;
              }
            });
            if(is_any_p3_checked == false){
                $("input[name='p3_isp3']").each(function() {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", true);
                });
            }

            var is_any_p5_checked = false;
            $("input[name='p5']").each(function() {
              if($(this).prop("checked") ==  true){
                is_any_p5_checked = true;
                return false;
              }
            });
            if(is_any_p5_checked == false){
                $("input[name='p5_isp5']").each(function() {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", true);
                });
            }

            var is_any_p7_checked = false;
            $("input[name='p7']").each(function() {
              if($(this).prop("checked") ==  true){
                is_any_p7_checked = true;
                return false;
              }
            });
            if(is_any_p7_checked == false){
                $("input[name='p7_isp7']").each(function() {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", true);
                });
            }
        },
    /*  onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#cLight").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cLight").jsGrid("loadData");
        },*/
    });
    $("#cBook").jsGrid({
        height: "90%",
        width: "100%",
 
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 15,
        noDataContent: "尚無繳款記錄",

        loadIndication: true,
        // autoload: true,
        pageLoading: true, 
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                if(!!Session.get("nowrow_lighting1")){
                    filter.lighting1_id = Session.get("nowrow_lighting1")._id;
                }

                return jsgridAjax(filter, "booking", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "booking", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "booking", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "booking", "DELETE");
            },
        },
        fields: [
            // { type: "control" },
            // { name: "uid", title: "編號" },
            { name: "status_text", title: "狀態", type: "text"},
            { name: "shouldpay", title: "應收", type: "text"},
            { name: "paid", title: "已收", type: "text"},
            { name: "paidday", title: "日期", type: "text"},
        ],
  /*      rowClick: function(args) {
            // console.log(args);
        },
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            // $("#cPass").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#cPass").jsGrid("loadData");
        },*/
    });
    // $(".my-lighting2-detail").hide();
    $('#p1-paidday').val(currentdate());

};

Template.cModalLight.helpers({
    get_livepeople: function() {
        return People.find({isLive:1});
    },
    get_passpeople: function() {
        return People.find({isLive:0});
    },
    isSelected: function (name) {
        if(name.indexOf("歷代")!= -1){
            return 1;
        }
        return 0;
    },
    get_mgyear:function () {
        // return arrMGYear;
        return Mgyear.find({}, {sort:{value: -1}});
    },
});

Template.cModalLight.events({
    'click .lighting2-p1': function (event) { // 
        var id = $(event.currentTarget).attr("data-id");
        
        var ori = $("#p1-"+id).prop("checked");
        // var ori = $(event.currentTarget).attr("checked");
        if(ori == true){ // 如果有選 

            $("input[name='p1_2']").each(function() { // 所有副的都打開
                $(this).prop("disabled", false);
            });

            $("#p1_2-"+id).prop("checked", false); // 自已 副的就關掉
            $("#p1_2-"+id).prop("disabled", true);

            var cash = Number($("#p8a-cash").val()) + 1000;
            $("#p8a-cash").val( cash );
        }
        else{ // 如果沒選
            console.log("disabled");
            $("#p1_2-"+id).prop("disabled", false); // 自已副的打開
            
            $("#p1_isallfamily-"+id).prop("checked", false); 
            $("#p1_isallfamily-"+id).prop("disabled", false);
            $("#p1_name2-"+id).val("");

            
            // 如果所有主的都沒有的話 那副的一樣關掉
            var is_any_p1_checked = false;
            $("input[name='p1']").each(function() {
              if($(this).prop("checked") ==  true){
                is_any_p1_checked = true;
              }
            });

            if(is_any_p1_checked == false){
                $("input[name='p1_2']").each(function() {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", true);
                });
            }

            var cash = Number($("#p8a-cash").val()) - 1000;
            if(cash < 0) cash = 0;
            $("#p8a-cash").val( cash );
        }
    },
    'click .lighting2-p1_2': function (event) { // 副的 至少要一組主的 且無燈號
        var id = $(event.currentTarget).attr("data-id");
        
        var ori = $("#p1_2-"+id).prop("checked");
        if(ori == false){
            return;
        }
        
        var is_any_p1_checked = false;
        $("input[name='p1']").each(function() {
          if($(this).prop("checked") ==  true){
            is_any_p1_checked = true;
          }
        });

        if(is_any_p1_checked == false){
            alert("至少要選一位主的");
            $("#p1_2-"+id).prop("checked", false); // 副的就關掉
        }
        else{ // 填名字到$("#p1_name2-"+id).val("");
            var people_name = $("#people_name-"+id).val();
            var isOk = false;
            $("input[name='p1']").each(function() { // 找那個有勾主 且 沒有燈號的
                var thisid = $(this).attr("data-id");
                if($(this).prop("checked")==true && $("#p1_name2-"+thisid).val()=="" && $("#p1_num-"+thisid).val()=="" ){
                    $("#p1_name2-"+thisid).val(people_name);
                    isOk = true;
                    return false; // break;
                }
            });
            if(!isOk){
                alert("沒有空的燈主可搭配");
                $(event.currentTarget).prop("checked", false);
            }
        }
    },
    'click .lighting2-p1_isallfamily': function (event) { // 按下闔家
        var id = $(event.currentTarget).attr("data-id");

        var ori = $("#p1-"+id).prop("checked"); // 光明燈
        // console.log(ori);
        if(ori == false){ // 如果沒有選 
            var cash = Number($("#p8a-cash").val()) + 1000;
            $("#p8a-cash").val( cash );
        }

        var ori = $("#p1_name2-"+id).val();
        if(ori != "闔家"){
            $("#p1_name2-"+id).val("闔家");
            $("#p1_2-"+id).prop("checked", false);
            $("#p1_2-"+id).prop("disabled", true);
            $("#p1-"+id).prop("checked", true);


            $("input[name='p1_2']").each(function() { // 所有副的都打開
                $(this).prop("disabled", false);
            });

            $("#p1_2-"+id).prop("checked", false); // 自已 副的就關掉
            $("#p1_2-"+id).prop("disabled", true);

        }
        else{
            $("#p1_name2-"+id).val("");
            if($("#p1-"+id).prop("checked") == false)
                $("#p1_2-"+id).prop("disabled", false);
        }


    },
    'click .lighting2-p3': function (event) { // 
        var id = $(event.currentTarget).attr("data-id");
        
        var ori = $("#p3-"+id).prop("checked");
        // var ori = $(event.currentTarget).attr("checked");
        if(ori == true){ // 如果有選 

            $("input[name='p3_isp3']").each(function() { // 所有副的都打開
                $(this).prop("disabled", false);
            });

            $("#p3_isp3-"+id).prop("checked", false); // 自已 副的就關掉
            $("#p3_isp3-"+id).prop("disabled", true);

            var cash = Number($("#p8a-cash").val()) + 2000;
            $("#p8a-cash").val( cash );
        }
        else{ // 如果沒選
            console.log("disabled");
            $("#p3_isp3-"+id).prop("disabled", false); // 自已副的打開
            
            $("#p1_isallfamily-"+id).prop("checked", false); 
            $("#p1_isallfamily-"+id).prop("disabled", false);
            $("#p1_name2-"+id).val("");

            
            // 如果所有主的都沒有的話 那副的一樣關掉
            var is_any_p1_checked = false;
            $("input[name='p1']").each(function() {
              if($(this).prop("checked") ==  true){
                is_any_p1_checked = true;
              }
            });

            if(is_any_p1_checked == false){
                $("input[name='p3_isp3']").each(function() {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", true);
                });
            }

            var cash = Number($("#p8a-cash").val()) - 2000;
            if(cash < 0) cash = 0;
            $("#p8a-cash").val( cash );
        }
    },
    'click .lighting2-p3_isp3': function (event) { // 副的 至少要一組主的 且無燈號
        var id = $(event.currentTarget).attr("data-id");
        
        var ori = $("#p3_isp3-"+id).prop("checked");
        if(ori == false){
            return;
        }
        
        var is_any_p3_checked = false;
        $("input[name='p3']").each(function() {
          if($(this).prop("checked") ==  true){
            is_any_p3_checked = true;
          }
        });

        if(is_any_p3_checked == false){
            alert("至少要選一位主的");
            $("#p3_isp3-"+id).prop("checked", false); // 副的就關掉
        }
        else{ // 填名字到$("#p1_name2-"+id).val("");
            var people_name = $("#people_name-"+id).val();
            
            var isOk = false;
            $("input[name='p3']").each(function() { // 找那個有勾主 且 沒有燈號的
                var thisid = $(this).attr("data-id");
                if($(this).prop("checked")==true && $("#p3_num-"+thisid).val()=="" ){
                    var ori_name = $("#p3_name2-"+thisid).val();
                    if(ori_name == ""){
                        $("#p3_name2-"+thisid).val(people_name);
                    }
                    else{
                        $("#p3_name2-"+thisid).val(ori_name +","+ people_name);
                    }
                    isOk = true;
                    return false; // break;
                }
            });
            if(!isOk){
                alert("沒有空的燈主可搭配");
                $(event.currentTarget).prop("checked", false);
            }
        }
    },
    'click .lighting2-p5': function (event) { // 
        var id = $(event.currentTarget).attr("data-id");
        
        var ori = $("#p5-"+id).prop("checked");
        // var ori = $(event.currentTarget).attr("checked");
        if(ori == true){ // 如果有選 

            $("input[name='p5_isp5']").each(function() { // 所有副的都打開
                $(this).prop("disabled", false);
            });

            $("#p5_isp5-"+id).prop("checked", false); // 自已 副的就關掉
            $("#p5_isp5-"+id).prop("disabled", true);

            var cash = Number($("#p8a-cash").val()) + 6000;
            $("#p8a-cash").val( cash );
        }
        else{ // 如果沒選
            // console.log("disabled");
            $("#p5_isp5-"+id).prop("disabled", false); // 自已副的打開
            
            $("#p1_isallfamily-"+id).prop("checked", false); 
            $("#p1_isallfamily-"+id).prop("disabled", false);
            $("#p1_name2-"+id).val("");

            
            // 如果所有主的都沒有的話 那副的一樣關掉
            var is_any_p1_checked = false;
            $("input[name='p1']").each(function() {
              if($(this).prop("checked") ==  true){
                is_any_p1_checked = true;
              }
            });

            if(is_any_p1_checked == false){
                $("input[name='p5_isp5']").each(function() {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", true);
                });
            }

            var cash = Number($("#p8a-cash").val()) - 6000;
            if(cash < 0) cash = 0;
            $("#p8a-cash").val( cash );
        }
    },
    'click .lighting2-p5_isp5': function (event) { // 副的 至少要一組主的 且無燈號
        var id = $(event.currentTarget).attr("data-id");
        
        var ori = $("#p5_isp5-"+id).prop("checked");
        if(ori == false){
            return;
        }
        
        var is_any_p5_checked = false;
        $("input[name='p5']").each(function() {
          if($(this).prop("checked") ==  true){
            is_any_p5_checked = true;
          }
        });

        if(is_any_p5_checked == false){
            alert("至少要選一位主的");
            $("#p5_isp5-"+id).prop("checked", false); // 副的就關掉
        }
        else{ // 填名字到$("#p1_name2-"+id).val("");
            var people_name = $("#people_name-"+id).val();
            
            var isOk = false;
            $("input[name='p5']").each(function() { // 找那個有勾主 且 沒有燈號的
                var thisid = $(this).attr("data-id");
                if($(this).prop("checked")==true && $("#p5_num-"+thisid).val()=="" ){
                    var ori_name = $("#p5_name2-"+thisid).val();
                    if(ori_name == ""){
                        $("#p5_name2-"+thisid).val(people_name);
                    }
                    else{
                        $("#p5_name2-"+thisid).val(ori_name +","+ people_name);
                    }
                    isOk = true;
                    return false; // break;
                }
            });
            if(!isOk){
                alert("沒有空的燈主可搭配");
                $(event.currentTarget).prop("checked", false);
            }
        }
    },
    'click .lighting2-p7': function (event) { // 
        var id = $(event.currentTarget).attr("data-id");
        
        var ori = $("#p7-"+id).prop("checked");
        // var ori = $(event.currentTarget).attr("checked");
        if(ori == true){ // 如果有選 

            $("input[name='p7_isp7']").each(function() { // 所有副的都打開
                $(this).prop("disabled", false);
            });

            $("#p7_isp7-"+id).prop("checked", false); // 自已 副的就關掉
            $("#p7_isp7-"+id).prop("disabled", true);

            var cash = Number($("#p8a-cash").val()) + 10000;
            $("#p8a-cash").val( cash );
        }
        else{ // 如果沒選
            console.log("disabled");
            $("#p7_isp7-"+id).prop("disabled", false); // 自已副的打開
            
            $("#p1_isallfamily-"+id).prop("checked", false); 
            $("#p1_isallfamily-"+id).prop("disabled", false);
            $("#p1_name2-"+id).val("");

            
            // 如果所有主的都沒有的話 那副的一樣關掉
            var is_any_p1_checked = false;
            $("input[name='p1']").each(function() {
              if($(this).prop("checked") ==  true){
                is_any_p1_checked = true;
              }
            });

            if(is_any_p1_checked == false){
                $("input[name='p7_isp7']").each(function() {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", true);
                });
            }

            var cash = Number($("#p8a-cash").val()) - 10000;
            if(cash < 0) cash = 0;
            $("#p8a-cash").val( cash );
        }
    },
    'click .lighting2-p7_isp7': function (event) { // 副的 至少要一組主的 且無燈號
        var id = $(event.currentTarget).attr("data-id");
        
        var ori = $("#p7_isp7-"+id).prop("checked");
        if(ori == false){
            return;
        }
        
        var is_any_p7_checked = false;
        $("input[name='p7']").each(function() {
          if($(this).prop("checked") ==  true){
            is_any_p7_checked = true;
          }
        });

        if(is_any_p7_checked == false){
            alert("至少要選一位主的");
            $("#p7_isp7-"+id).prop("checked", false); // 副的就關掉
        }
        else{ // 填名字到$("#p1_name2-"+id).val("");
            var people_name = $("#people_name-"+id).val();
            
            var isOk = false;
            $("input[name='p7']").each(function() { // 找那個有勾主 且 沒有燈號的
                var thisid = $(this).attr("data-id");
                if($(this).prop("checked")==true && $("#p7_num-"+thisid).val()=="" ){
                    var ori_name = $("#p7_name2-"+thisid).val();
                    if(ori_name == ""){
                        $("#p7_name2-"+thisid).val(people_name);
                    }
                    else{
                        $("#p7_name2-"+thisid).val(ori_name +","+ people_name);
                    }
                    isOk = true;
                    return false; // break;
                }
            });
            if(!isOk){
                alert("沒有空的燈主可搭配");
                $(event.currentTarget).prop("checked", false);
            }
        }
    },
    'click .my-btn-submit8a': function (event) { // 儲存
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal8a").serializeObject();
        if($('#p8a-whoapply :selected').text() != "請選擇")
            formVar.apply_text = $('#p8a-whoapply :selected').text();
        
        var item = Session.get("nowrow");
        formVar.familyId = item.familyId;
        formVar.isSave = 1;

        // console.log("my-btn-submit8a");
        // console.log(formVar);

        Meteor.call("updateLighting1", formVar, function(error, result){
            // Materialize.toast('點燈服務已儲存', 3000, 'rounded');

            var arr_num = result.arr_num;
            // console.log(result);
            delete result.arr_num;

            Session.set("nowrow_lighting1", result);
           
            $("#cLight1").jsGrid("loadData");
            // $("#cLight2").jsGrid("loadData");
            // 
            for(var i=0; i<arr_num.length; i++){
                var data = arr_num[i];
                $("#p1_num-" + data.id).val(data.p1_num);
                $("#p3_num-" + data.id).val(data.p3_num);
                $("#p5_num-" + data.id).val(data.p5_num);
                $("#p7_num-" + data.id).val(data.p7_num);
            }
        
            $("#ordernum-header2").text(result.ordernum);
            $(".form8a-btn-save").attr("disabled", "disabled");
            $(".form8a-btn-del").removeAttr("disabled");
        });
    },
    'click .my-btn-new8a': function (event) {
        event.preventDefault();
        event.stopPropagation();
    	Template.cModalLight.newLight();
    },
    'change .form-modal8a input, keypress .form-modal8a input': function (event) {
        // $("#form8a-btn-del").attr("disabled", "disabled");
        $(".form8a-btn-save").removeAttr("disabled");
    },
    'change .form-modal8a select': function (event) {
    	// $("#form8a-btn-del").attr("disabled", "disabled");
    	$(".form8a-btn-save").removeAttr("disabled");
    },
    "change #p8a-lightitem": function(e){
        // console.log(e);
        // console.log( $('option:selected', this).attr('serial') );
        // console.log( $('option:selected', this).attr('price') );

        $("#p8a-lightserial").val( Number($('option:selected', "#p8a-lightitem").attr('serial'))+1);
        $("#p8a-cash").val($('option:selected', "#p8a-lightitem").attr('price'));
    },
    'click .my-btn-submit4b': function (event) { // 新增
        event.preventDefault();
        event.stopPropagation();
        // console.log("aaa");

        // var formVar = $(".form-modal4b").serializeObject();
        var formVar = {};
        // if($('#p4b-type :selected').text() != "請選擇")
        //     formVar.type_text = $('#p4b-type :selected').text();

        // console.log(formVar);   
        var item  = Session.get("nowrow");
        var light1 = Session.get("nowrow_lighting1");

        formVar.listId = light1._id;
        formVar.lightname = light1.lightname;
        formVar.lightname_text = light1.lightname_text;
        formVar.lightitem = light1.lightitem;
        formVar.lightitem_text = light1.lightitem_text;
        formVar.year = light1.year;
        formVar.lightserial = light1.lightserial;
        formVar.ordernum = light1.ordernum;
        // formVar.light2_orderid = Light2.find({_id: light1.lightitem}).fetch()[0].order_id;
        // formVar.lightingtype_orderid = LightingType.find({_id: formVar.type}).fetch()[0].order;

        $("#cLight2").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                // $('#modal4b').toggle();
                // $(".form-modal4b").trigger('reset');
                $("#cLight2").jsGrid("loadData");

                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal8a-error").text(ret);
            }
        });
    },
    'click .my-btn-submit-acc': function (event) {
        event.preventDefault();
        event.stopPropagation();
        // console.log("aaa");
        var formVar = $(".form-8acc").serializeObject();
        // formVar.status = $('#p1-paid').val();
        formVar.shouldpaid = $('#p1-shouldpay').text();
        formVar.paid = $('#p1-paid').val();
        formVar.paidday = $('#p1-paidday').val();
        if($('#p1-acc-status :selected').text() != "請選擇")
            formVar.status_text = $('#p1-acc-status :selected').text();

        var light1 = Session.get("nowrow_lighting1");

        formVar.p1CreatedDate = light1.date;
        formVar.lighting1_id = light1._id;
        formVar.ordernum = light1.ordernum;
        formVar.lightname = light1.lightname;
        formVar.lightname_text = light1.lightname_text;
        formVar.lightitem = light1.lightitem;
        formVar.lightitem_text = light1.lightitem_text;
        formVar.lightserial = light1.lightserial;

        Meteor.call("insertAcc", formVar, function(){
            // Materialize.toast('款項已輸入', 3000, 'rounded');
            $(".form-8acc").trigger('reset');
            $('#p1-paidday').val(currentdate());
                if(formVar.status == 1){
                    $("#show-acc-insert").hide();
                }

            $("#cBook").jsGrid("loadData");
        });
    },
    'click .my-open-modal8a': function() {
    	Template.cModalLight.newLight();

        $('#modal8a').toggle();
        $("#cLight1").jsGrid("loadData");
    },
    'click .my-btn-cancel8a': function() {
        event.preventDefault();
        event.stopPropagation();

        $('#modal8a').toggle();
    },
    'click .my-btn-delete8a': function() {
        var item = Session.get("nowrow_lighting1")
        if(confirm("確定刪除點燈服務 "+ item.ordernum +" 嗎?")){
            Meteor.call("deleteLighting1", item._id, function(){
                $("#cLight1").jsGrid("loadData");
                // $(".my-lighting2-detail").hide();
				Template.cModalLight.newLight();
            });
        }
    },
    'click .my-open-modal4b': function() {
        $(".form-modal4b").trigger('reset');
        $("#p8a-livename_text-label").removeClass("active");
        $("#p8a-passname_text-label").removeClass("active");
        $("#p8a-addr-label").removeClass("active");
        $('#modal4b').toggle();
    },
    'click .my-btn-cancel4b': function() {
        $('#modal4b').toggle();
    },
    'click .my-open-modal4b1': function() {
        $(".form-modal4b1").trigger('reset');
        $('#modal4b1').toggle();
        var main = People.find({mainPerson:-1}).fetch()[0]._id;
        $('#test1-'+main).attr('checked', true);
    },
    'click .my-btn-cancel4b1': function() {
        $('#modal4b1').toggle();
    },
    'click .my-open-modal4b2': function() {
        $(".form-modal4b2").trigger('reset');
        $('#modal4b2').toggle();

        // var main = People.find({isLive:0}).fetch()[0]._id;
        // $('#test1b-'+main).attr('checked', true);
    },
    'click .my-btn-cancel4b2': function() {
        $('#modal4b2').toggle();
    },

});
