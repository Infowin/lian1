Template.cPrint.rendered = function(){
    $('.datepicker').pickadate({
        formatSubmit: 'yyyy/mm/dd',
        format: 'yyyy/mm/dd',
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 10, // Creates a dropdown of 15 years to control year

        monthsFull: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        monthsShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        weekdaysFull: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
        weekdaysShort: ['日', '一', '二', '三', '四', '五', '六'],
        showMonthsShort: undefined,
    });

    $('.datetimepicker').datetimepicker({
        lang: "zh-TW",
        format:'Y/M/D H:mm',
    });

    $("#pPray1").jsGrid({
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 20,
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                var formVar = $("#form-pray-filter").serializeObject();
                $.extend(filter, formVar);
                // console.log("pPray1 filter");
                // console.log(filter);
                return jsgridAjax(filter, "praying2", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "praying2", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "praying2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "praying2", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60, editButton: false,  },
            { name: "ordernum", title: "編號", width:90, editing: false, },
            // { name: "year", title: "年度", width:50, editing: false, type: "select", items: [{id:"",value:""}].concat(arrMGYear), valueField: "id", textField: "value"  },
            { name: "year", title: "年度", width:50, editing: false, type: "text" },
            { name: "createdAt", title: "建立時間", width:130, align: "",
                itemTemplate: function(value, item) {
                    if(!!item.createdAt)
                        return (new Date(item.createdAt)).yyyymmddhm();
                    else return "";
                }
             },
            // { name: "updatedAt", title: "更新時間", width:100, align: "",
            //     itemTemplate: function(value, item) {
            //         if(!!item.updatedAt)
            //             return (new Date(item.updatedAt)).yyyymmddhm();
            //         else return "";
            //     }
            //  },
            { name: "prayname", title: "法會", editing: false, type: "select", items: [{_id:"",value:""}].concat(Pray1.find({},{sort:{order: 1}}).fetch()), valueField: "_id", textField: "value"  },
            { name: "prayitem", title: "項目", editing: false, type: "select", items: [{_id:"",value:""}].concat(Pray2.find({},{sort:{order: 1}}).fetch()), valueField: "_id", textField: "value"  },
            { name: "prayserial", type:"text", title: "序號", width:50, align: "center", },
            { name: "type", title: "列印", width:100, type: "select", items: [{_id:"",value:""}].concat(PrayingType.find({},{sort:{order: 1}}).fetch()), valueField: "_id", textField: "value" },
            { name: "livename_text", title: "消", type:"text", width:180 },
            { name: "passname_text", title: "拔", type:"text" },
            { name: "passname_text1", title: "拔 歷代祖先", type:"text" },
            { name: "passname_text2", title: "拔 冤親債主", type:"text" },
            { name: "passname_text3", title: "拔 地基主", type:"text" },
            { name: "passname_text4", title: "拔 嬰靈", type:"text" },
            { name: "addr", title: "地址", type:"text", width:300 },
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
       /* onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            // $("#cNewclient").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cNewclient").jsGrid("loadData");
        },*/
    });

    $("#pLight1").jsGrid({
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
		// filtering: true,
		editing: true,
        pageSize: 20,
 		loadIndication: true,
 		// autoload: true,
 		pageLoading: true,
 		loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
 		controller:{
		    loadData: function(filter) {
		    	var formVar = $("#form-light-filter").serializeObject();
                $.extend(filter, formVar);

                delete filter.printdate;
                delete filter.printdate2;

                return jsgridAjax(filter, "lighting2", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "lighting2", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "lighting2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "lighting2", "DELETE");
            },
		},
        fields: [
            { type: "control", width:60, editButton: false, editing: false,  },
            // { name: "ordernum", title: "編號",},
            { name: "year", title: "年度", type: "text", width:50, align: "center" },
            { name: "people_name", title: "申請者", type:"text" },
            { name: "p1", title: "光明燈", width: 40, align: "center", editing: false,
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p1 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT TYPE="checkbox" disabled="disabled" value="'+item._id+'" '+checked+'>';
                }
            },
            { name: "p1_num", title: "燈號", type:"text", width: 65 },
            { name: "p1_name2", title: "第二位", type:"text" },
            { name: "p3", title: "藥師燈", width: 40, align: "center", editing: false,
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p3 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT TYPE="checkbox" disabled="disabled" value="'+item._id+'" '+checked+'>';
                }
            },
            { name: "p3_num", title: "燈號", type:"text", width: 65 },
            { name: "p3_name2", title: "其他位", type:"text" },
            { name: "p5", title: "光明大燈", width: 40, align: "center", editing: false,
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p5 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT TYPE="checkbox" disabled="disabled" value="'+item._id+'" '+checked+'>';
                }
            },
            { name: "p5_num", title: "燈號", type:"text", width: 65 },
            { name: "p5_name2", title: "其他位", type:"text" },
            { name: "p7", title: "藥師大燈", width: 40, align: "center", editing: false,
                itemTemplate: function(value, item) {
                    var checked = "";
                    if(item.p7 == '-1'){
                        checked = "checked";
                    }
                    return '<INPUT TYPE="checkbox" disabled="disabled" value="'+item._id+'" '+checked+'>';
                }
            },
            { name: "p7_num", title: "燈號", type:"text", width: 65 },
            { name: "p7_name2", title: "其他位", type:"text" },
            { name: "createdAt", title: "建立時間", width:130, align: "",
                itemTemplate: function(value, item) {
                    if(!!item.createdAt)
                        return (new Date(item.createdAt)).yyyymmddhm();
                    else return "";
                }
             },
            // { name: "addr", title: "地址", type:"text", width:300 },
        ],
        onDataLoading: function(args) {
			$('.jsgrid select').material_select();
        },
       /* onItemInserted: function(args) {
        	// Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
        	// Materialize.toast('資料已更新', 3000, 'rounded')
        	// console.log(args);
        	// $("#cNewclient").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
        	// Materialize.toast('資料已刪除', 3000, 'rounded')
        	$("#cNewclient").jsGrid("loadData");
        },*/
    });

    $("#pPeople").jsGrid({
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
		// filtering: true,
		editing: true,
        pageSize: 20,
 		loadIndication: true,
 		// autoload: true,
 		pageLoading: true,
 		loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
 		controller:{
		    loadData: function(filter) {
		    	var formVar = $("#form-member-filter").serializeObject();
                $.extend(filter, formVar);

                // delete filter.my_print;

    			filter.isLive = "1";
    			if(!filter.sortField){
                    filter.sortField = "memberId";
                    filter.sortOrder = "asc";
                }

                return jsgridAjax(filter, "people", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "people", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "people", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "people", "DELETE");
            },
		},
        fields: [
            { type: "control", width:60, editButton: false,  },
            // { name: "uid", title: "編號" },
            // { name: "mailThis", title: "郵寄", type:"checkbox" },
            { name: "name", title: "姓名", type:"text", width:140 },
            { name: "mailThis", title: "郵寄", width:60, type: "select", items: objTrueFalse, valueField: "id", textField: "value"  },
            { name: "sexual", title: "性別", width:60, type: "select", items: [{id:"",value:""}].concat(arrSexual), valueField: "id", textField: "value"  },
          /*  { name: "birthday2", title: "生日(國)", type: "text", width:100,
                itemTemplate: function(value, item) {
                    // console.log(item);
                    if(!!item.bornYear && !!item.bornMonth && !!item.bornDay){
                        var l = 0;
                        if( item.bornMonth.indexOf("閏")!= -1 ){
                            l = 1;
                            item.bornMonth = item.bornMonth.substr(1);
                        }
                        return funcLunar(1, Number(item.bornYear)+1911, item.bornMonth, item.bornDay, l);
                    }
                    else
                        return '';
                },
            },*/
            { name: "birthday", title: "生日(國)", type: "text", width:80},
            { name: "bornYear", title: "生年(農)", type: "number", width:60 },
            { name: "bornMonth", title: "生月(農)", type: "number", width:60 },
            { name: "bornDay", title: "生日(農)", type: "number", width:60 },
            { name: "telephone", title: "電話", type: "text" },
            { name: "identity", title: "身分證字號", type: "text" },
			{ name: "templeClass", sorting:false, title: "班別", type: "select", items: [{_id:"",value:""}].concat(TempleClass.find({}, { sort: { order: 1}} ).fetch()), valueField: "_id", textField: "value"     },
            { name: "templeLevel", sorting:false, title: "身份", type: "select", items: [{_id:"",value:""}].concat(TempleLevel.find({}, { sort: { order: 1}} ).fetch()), valueField: "_id", textField: "value"     },
            { name: "memberId", sorting:false, title: "社員\n編號", type: "text", width:55},
            { name: "longLive", sorting:false, title: "長生", type: "text", width:60},
            { name: "lotus", sorting:false, title: "往生堂", type: "text", width:80},
            { name: "post5code", title: "區號", type: "text", width:80 },
            { name: "addr", title: "地址", width:300, type: "text" },
        ],
        onDataLoading: function(args) {
			$('.jsgrid select').material_select();
        },
        onRefreshed: function() {
            // $(".jsgrid-pager").contents().filter(function() {
            //     return this.nodeType == 3;
            // }).each(function(){
            //     this.textContent = this.textContent.replace('{itemsCount}', $("#jsGrid").jsGrid("_itemsCount"));
            // });
            // console.log($("#pPeople").jsGrid("_itemsCount"));
            $("#range31").val("1");
            $("#range32").val($("#pPeople").jsGrid("_itemsCount"));
        }
      /*  onItemInserted: function(args) {
        	// Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
        	// Materialize.toast('資料已更新', 3000, 'rounded')
        	// console.log(args);
        	// $("#cNewclient").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
        	// Materialize.toast('資料已刪除', 3000, 'rounded')
        	// $("#cNewclient").jsGrid("loadData");
        },*/
    });

    $('ul.tabs').tabs();
	// var pray1_set = Pray1.find({now_use: "-1"}).fetch()[0]._id;
	// if(pray1_set) $("#pray-prayname").val(pray1_set);
	$("#pray-prayname").trigger("change");
	$("#pPray1").jsGrid("loadData");

    var pray1Selected = $("#pray-prayname option:selected").val();
    Session.set("pray1", pray1Selected);


	// $('#printdate').val();

};
Template.cPrint.helpers({
	get_today:function () {
        // return funcLunar(0,dd.getFullYear(),(dd.getMonth()+1),dd.getDate());
		return currentdate();
	},
	get_prayyear:function () {
        // return arrMGYear;
		return Mgyear.find({}, {sort:{value: -1}});
	},
	get_praytype: function() {
	    // return PrayingType.find({},{sort: {order: 1}});
        return  PrayingType.find(
            {$and:[
                {$or: [
                    {only_pray:{ $exists: false }},
                    {only_pray: Session.get("pray1") },
                    {only_pray: "" }]},
                {only_pray: {$ne: "0"}}
            ]},{sort:{order: 1}} ).fetch();
	},
    get_class: function() {
        return TempleClass.find({},{sort: {order: 1}});
    },
    get_level: function() {
        return TempleLevel.find({},{sort: {order: 1}});
    },
    get_pray1: function() {
        return Pray1.find({},{sort: {order_id: 1}});
    },
    get_chairname: function(){
        if(System.find({"chairname": 1}).count()){
            return System.find({"chairname": 1}).fetch()[0].value || "";
        }
        return "";
    },
    get_pdffilename: function(){
        var time = Session.get("time");
        return Pdfpath.find({time: time}, {sort:{oid:1}});
    }
});
Template.cPrint.adjustDetail = function(){
    var firstVal = Number($('#form-member-filter input[name="my_print"]:checked').val());
    var secVal =  Number($('#form-member-filter input[name="nowform"]:checked').val());

    // console.log(firstVal);
    // console.log(secVal);

    if(firstVal == 2){ // 社員
        var id = TempleLevel.findOne({value:"社員"})._id;
        $('#templeLevel').val(id);
    }
    else if(firstVal == 3){ // 護法
        var id = TempleLevel.findOne({value:"護法"})._id;
        $('#templeLevel').val(id);
    }

    if(secVal == 1 || secVal == 2 || secVal == 3){ // 非郵寄的
        $("#mailThis").val("");
        $('#uniType').val("0");
    }
    else{
        $("#mailThis").val("-1");
        $('#uniType').val("1");
    }
    $("#pPeople").jsGrid("loadData");

}
Template.cPrint.events({
    'change #my-test6': function(event) {
        // console.log($(event.target).checked);

        if($(event.target).is(':checked')){
            $("#mail-sel-detail").fadeIn();
        }
        else{
            $("#mail-sel-detail").hide();
        }
    },
    'click #my_print1': function() { // 全部 ok
        $('#templeLevel').val("");
        $('#templeClass').val("");
        Template.cPrint.adjustDetail();
    },
    'click #my_print2': function() { // 社員
        Template.cPrint.adjustDetail();
    },
    'click #my_print3': function() { // 護法
        Template.cPrint.adjustDetail();
    },
    'click #test1a': function() {
        Template.cPrint.adjustDetail();
    },
    'click #test2c': function() {
        Template.cPrint.adjustDetail();
    },
    'click #test2b': function() { // 名冊封面
        Template.cPrint.adjustDetail();
    },
    'click #test2e, click #test2d,  click #test2d2': function() {
        Template.cPrint.adjustDetail();
    },
    'click .my-btn-submit1': function() {
    	$("#pdf-iframe").get(0).contentWindow.print();
        // $('#modal1').closeModal();
        $('#modal1').toggle();
    },
    'click .my-open-modal1': function() {
        $("#pdf-iframe").prop("src", "");
        // $('#modal1').openModal();
        $('#modal1').toggle();
        // 起始的東西在這邊加
        var formVar = "/api/pdfpraying2?"+$("#form-pray-filter").serialize();
        // console.log(formVar);

        $("#pdf_url").prop("href", formVar);
        $("#pdf-iframe").prop("src", formVar);
    },
    'click .my-open-modal3': function() {
        $("#pdf-iframe").prop("src", "");
        // $('#modal1').openModal();
        $('#modal1').toggle();

        // 起始的東西在這邊加
        var formVar = "/api/pdflighting2?"+$("#form-light-filter").serialize();
        // console.log(formVar);

        $("#pdf_url").prop("href", formVar);
        $("#pdf-iframe").prop("src", formVar);
    },
    'click .my-open-modal2': function() {
        var cnt = $("#pPeople").jsGrid("_itemsCount");
      /*  if(cnt > 1000 && $('input[name=nowform]:checked', '#form-member-filter').val() != "2"){
            if(!confirm("目前資料有 "+ cnt +" 筆，確定要產生PDF嗎?")){
                return;
            }
        }*/
    	$("#pdf-iframe").prop("src", "");
        // $('#modal1').openModal();
        $('#modal1').toggle();

        var time = new Date().getTime();
        // var subscription = Meteor.subscribe("pdf_path", time);
		// console.log("subscribe pdf_path ", time);

        // Session.set("time", time);
		var formVar = "api/pdfpeople?time="+time+"&"+$("#form-member-filter").serialize();
        if(!!$("#bossname").val()){
            Meteor.call("updateChairName", $("#bossname").val(), function(error, result){
                if(error){
                    console.log("error from updateChairName: ", error);
                }
            });
        }

    	$("#pdf_url").prop("href", formVar);
    	$("#pdf-iframe").prop("src", formVar);
    	if(cnt <= 700){
			// do nothing
    	}
    	else{
    		var obj = $("#form-member-filter").serializeObject();
    		var nowform = obj.nowform;

    		var needpage = 0;
    		if(nowform == "41"){
    			needpage = cnt/7000;
    		}
    		else if(nowform == "42"){
    			needpage = cnt/7000;
    		}
    		else if(nowform == "5"){
    			needpage = cnt/700;
    		}
    		needpage = Math.floor(needpage);
    		// console.log("needpage: "+needpage);

    		$(".my-extra-url-link").html("");
    		for(var i = 1; i<=needpage; i++){
    			var url = formVar + "&mypage=" + i;
    			var page = i+1;
    			$(".my-extra-url-link").append('<a class="my-extra-url-btn" href="'+url+'" target="_blank">第'+page+'份</a> ');
    		}
    	}
    },
    'click .my-extra-url-btn': function(e) {
    	e.preventDefault();
        $("#pdf-iframe").prop("src", "");

    	var url = $(e.currentTarget).attr("href");
    	$("#pdf-iframe").prop("src", url);
    },
    'click .my-btn-cancel1': function() {
        $("#pdf-iframe").prop("src", "");
        // $('#modal1').closeModal();
        $('#modal1').toggle();
    },
    'click .my-test2': function () {
        $("#pLight1").jsGrid("loadData");
        $("#pLight1").jsGrid("openPage", 1);
    },
    'click .my-test3': function () {
        $("#pPeople").jsGrid("loadData");
        $("#pPeople").jsGrid("openPage", 1);
    },
	'click .lean-overlay': function () {
		// console.log("close modal");
	},
    "change #light-year": function(e){
        $("#pLight1").jsGrid("loadData");
        $("#pLight1").jsGrid("openPage", 1);
    },
    "change #light-type": function(e){
        $("#pLight1").jsGrid("loadData");
        $("#pLight1").jsGrid("openPage", 1);
    },
    "change #pray-start-time": function(e){
        $("#pPray1").jsGrid("loadData");
        $("#pPray1").jsGrid("openPage", 1);
    },
    "change #pray-end-time": function(e){
        $("#pPray1").jsGrid("loadData");
        $("#pPray1").jsGrid("openPage", 1);
    },
    "change #pray-year": function(e){
		$("#pPray1").jsGrid("loadData");
        $("#pPray1").jsGrid("openPage", 1);
    },
    "change #pray-type": function(e){
        $("#pPray1").jsGrid("loadData");
        $("#pPray1").jsGrid("openPage", 1);
    },
    "change #pray-order": function(e){
		// $("#pPray1").jsGrid("loadData");
  //       $("#pPray1").jsGrid("openPage", 1);
    },
    "change #pray-prayitem": function(e){
		$("#pPray1").jsGrid("loadData");
        $("#pPray1").jsGrid("openPage", 1);
    },
    "change #pray-prayname": function(e){
        var pray1Selected = $("#pray-prayname option:selected").val();
        Session.set("pray1", pray1Selected);
        Meteor.call("getPray2", pray1Selected, function(error, result){
            $('select').material_select('destroy');
            $('#pray-prayitem')
                .find('option')
                .remove()
                .end()
                .append('<option value="" selected>請選擇</option>')
                // .val('whatever')
            ;
            $.each(result, function (i, item) {
                // if(!!item.value)
                $('#pray-prayitem').append($('<option>', {
                    value: item._id,
                    text : item.value,
                    price:  item.price,
                    serial:  item.now_num,
                }));
            });
            // $("#p4a-prayitem").val(item.prayitem);
            $('select').material_select();
        });

		$("#pPray1").jsGrid("loadData");
        $("#pPray1").jsGrid("openPage", 1);
    },
    "change #light-start-time": function(e){
        $("#pLight1").jsGrid("loadData");
        $("#pLight1").jsGrid("openPage", 1);
    },
    "change #light-end-time": function(e){
        $("#pLight1").jsGrid("loadData");
        $("#pLight1").jsGrid("openPage", 1);
    },
    "change #templeClass": function(e){
        $("#pPeople").jsGrid("loadData");
        $("#pPeople").jsGrid("openPage", 1);
    },
    "change #templeLevel": function(e){
        $("#pPeople").jsGrid("loadData");
        $("#pPeople").jsGrid("openPage", 1);
    },
    "change #mailThis": function(e){
        $("#pPeople").jsGrid("loadData");
        $("#pPeople").jsGrid("openPage", 1);
    },
    "change #uniType": function(e){
        $("#pPeople").jsGrid("loadData");
        $("#pPeople").jsGrid("openPage", 1);
    },
});
