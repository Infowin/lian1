Template.cNewclient.loadClients = function(){
    // console.log("loadClients " + $("input#icon_prefix3").val());
    $("#cNewclient").jsGrid("loadData");
    $("#cNewclient").jsGrid("openPage", 1);
}

Template.cNewclient.rendered = function(){
    var first_open = 1;
    function refresh_data(args){
        Session.set("nowrow", args.item);
    }

    $('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
    $('ul.tabs').tabs();
    $("#cNewclient").jsGrid({
        height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
		// filtering: true,
		// editing: true,
        pageSize: 5,
 		loadIndication: true,
 		autoload: true,
 		pageLoading: true, 
 		loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
 		controller:{
		    loadData: function(filter) {
                filter.isLive = 1;
                filter.findData = $("input#icon_prefix3").val() || "";

                return jsgridAjax(filter, "people", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "people", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "people", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "people", "DELETE");
            },
		},
        fields: [
            // { name: "uid", title: "編號" },
            { name: "name", title: "姓名", type:"text" },
            { name: "sexual_text", title: "性別", width:50 },
            { name: "identify", title: "身分證字號" },
            { name: "birthday", title: "生日", 
                itemTemplate: function(value, item) { 
                    if(item.bornYear)
                        return item.bornYear+"/"+item.bornMonth+"/"+item.bornDay+" "+item.bornTime_text;
                    else
                        return "";
                }, 
            },
            { name: "telephone", title: "電話", type:"text" },
            { name: "cellphone", title: "手機", type:"text" },
            { name: "addr", title: "地址", width:220 },
            // { type: "control" }
        ],
        rowClick: function(args) {
            // console.log(args);
            $(".my-praying2-detail").hide();
            $("#show-client-detail").hide();

            Session.set("nowrow", args.item);

            $("#show-client-detail").fadeIn();

            $("#cLive").jsGrid("loadData");
            $("#cPass").jsGrid("loadData");
            
            $("#cLive").jsGrid("openPage", 1);
            $("#cPass").jsGrid("openPage", 1);
            // $("#cPray1").jsGrid("loadData");
            // $("#cLight").jsGrid("loadData");
        },
        rowDoubleClick: function(args) { 
            $(".my-open-modal4a").trigger("click");
            $("#p4a-whoapply").val(args.item._id);
        },
        onDataLoading: function(args) {
			$('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
        	// Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
        	// Materialize.toast('資料已更新', 3000, 'rounded')
        	// console.log(args);
        	$("#cNewclient").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
        	// Materialize.toast('資料已刪除', 3000, 'rounded')
        	$("#cNewclient").jsGrid("loadData");
        },
        onDataLoaded: function(args) {
            // console.log("onDataLoaded");
            var item = $("#cNewclient").data("JSGrid").data[0];
            if(first_open==1 && !!item && !!item.familyId){
                $("#show-client-detail").hide();
                Session.set("nowrow", item);

                $("#cLive").jsGrid("loadData");
                $("#cPass").jsGrid("loadData"); 
                $("#show-client-detail").fadeIn(); 
                first_open = 0;   
            }
        },
    });

    $("#cLive").jsGrid({
        height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 10,
        loadIndication: true,
        // autoload: true,
        pageLoading: true, 
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller:{
            loadData: function(filter) {
                var item = Session.get("nowrow");
                filter.familyId = item.familyId;
                filter.isLive = 1;

                return jsgridAjax(filter, "people", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "people", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "people", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "people", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60 },
            // { name: "uid", title: "編號" },
            { name: "mainPerson", type: "checkbox", title: "戶長", width: 30,
                itemTemplate: function(value, item) { 
                    // console.log(item);
                    if(item.mainPerson == -1)
                        return '<input type="checkbox" checked="checked" disabled="disabled">';
                    else
                        return '<input type="checkbox" disabled="disabled">';
                },
                editTemplate: function(value, item) { 
                    var editPicker = this._editPicker = $("<input>").attr("type", "checkbox");
                    
                    setTimeout(function() {
                        if(item.mainPerson == -1){
                            editPicker.prop("checked", "checked");
                        }
                    });
                    return editPicker;
                },
                editValue: function() {
                    // console.log("editValue");
                    var val = this._editPicker.prop("checked");
                    if(val == true) return -1;
                    return 0;
                }
            },
            { name: "mailThis", type: "checkbox", title: "郵寄", width: 30,
                itemTemplate: function(value, item) { 
                    // console.log(item);
                    if(item.mailThis == "-1")
                        return '<input type="checkbox" checked="checked" disabled="disabled">';
                    else
                        return '<input type="checkbox" disabled="disabled">';
                },
                editTemplate: function(value, item) { 
                    var editPicker = this._editPicker = $("<input>").attr("type", "checkbox");
                    
                    setTimeout(function() {
                        if(item.mailThis == "-1"){
                            editPicker.prop("checked", "checked");
                        }
                    });
                    return editPicker;
                },
                editValue: function() {
                    // console.log("editValue");
                    var val = this._editPicker.prop("checked");
                    if(val == true) return -1;
                    return 0;
                }
            },
            { name: "name", title: "姓名", type: "text" },
            { name: "sexual", title: "性別", width:60, type: "select", items: [{id:"",value:""}].concat(arrSexual), valueField: "id", textField: "value"  },
            // { name: "identity", title: "身分證字號", type: "text" },
            // { name: "birthday", title: "生日(國)", type: "text", width:100 },
            { name: "bornYear", title: "生年(農)", type: "number", width:60 },
            { name: "bornMonth", title: "生月(農)", type: "number", width:60 },
            { name: "bornDay", title: "生日(農)", type: "number", width:60 },
            { name: "bornTime", title: "生時", type: "select", width:60, items: [{id:"",value:""}].concat(arrTime), valueField: "id", textField: "value" },
            // { name: "zodiac_id", title: "生肖", type: "select", width:60, items: [{id:"",value:""}].concat(arrChineseYear), valueField: "id", textField: "value" },
            { name: "post5code", title: "區號", type: "text", width:60 },
            { name: "addr", title: "地址", css:"", type: "text", width:200 },
            { name: "telephone", title: "電話", type: "text" },
            { name: "cellphone", title: "手機", type: "text" },
            { name: "templeClass", title: "班別", type: "select", width:60, items: [{_id:"",value:""}].concat(TempleClass.find({}, { sort: { order: 1}} ).fetch()), valueField: "_id", textField: "value"     },
            { name: "templeLevel", title: "身份", type: "select", width:60, items: [{_id:"",value:""}].concat(TempleLevel.find({}, { sort: { order: 1}} ).fetch()), valueField: "_id", textField: "value"     },
            { name: "memberId", title: "社員\n編號", type: "text", width:60},
            { name: "longLive", title: "功德堂", type: "text", width:60},
			{ name: "buddhawall", title: "天王殿\n萬佛牆", type: "text", width:60},
            { name: "lotus", title: "往生堂", type: "text", width:60},
            { name: "lotus2", title: "淨土堂", type: "text", width:60},
            // { name: "ps", title: "備註", type: "text" },
            { name: "move_live0", align: "center", title: "",
                itemTemplate: function(value, item) {
                    return '<input class="move_live0" type="button" data-id="'+item._id+'" value="移至拔渡">';
                },
            },
        ],
        rowClick: function(args) {
            // console.log(args);
        },  
        rowDoubleClick: function(args) { 
            $(".my-open-modal4a").trigger("click");
            $("#p4a-whoapply").val(args.item._id);
        },
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#cLive").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cLive").jsGrid("loadData");
        },
    });

    $("#cPass").jsGrid({
        height: "90%",
        width: "100%",
 
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 15,

        loadIndication: true,
        // autoload: true,
        pageLoading: true, 
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller:{
            loadData: function(filter) {
                var item = Session.get("nowrow");
                filter.familyId = item.familyId;
                filter.isLive = 0;

                return jsgridAjax(filter, "people", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "people", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "people", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "people", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60, editButton: false, },
            // { name: "uid", title: "編號" },
            { name: "name", title: "名稱", type: "text" },
            { name: "bornYear", title: "生年", type: "number", width:50 },
            { name: "bornMonth", title: "生月", type: "number", width:50 },
            { name: "bornDay", title: "生日", type: "number"  , width:50    },
            { name: "bornTime", title: "生時", type: "select", width:50 , items: [{id:"",value:""}].concat(arrTime), valueField: "id", textField: "value"     },
            { name: "passYear", title: "卒年", type: "number", width:50 },
            { name: "passMonth", title: "卒月", type: "number", width:50 },
            { name: "passDay", title: "卒日", type: "number", width:50 },
            { name: "passTime", title: "卒時", type: "select", width:50, items: [{id:"",value:""}].concat(arrTime), valueField: "id", textField: "value"     },
            { name: "lifeYear", title: "享壽", type: "number", width:70},
            { name: "addr", title: "地址", type:"text", width: 300 },
            { name: "move_live1", align: "center", title: "",
                itemTemplate: function(value, item) {
                    return '<input class="move_live1" type="button" data-id="'+item._id+'" value="移至陽上">';
                },
            },
        ],
        // rowClick: function(args) {
        //     // console.log(args);
        // },
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#cPass").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cPass").jsGrid("loadData");
        },
    });   
/*
    $("#cLight").jsGrid({
        height: "90%",
        width: "100%",
 
        sorting: true,
        paging: true,
        filtering: true,
        editing: true,
        pageSize: 5,

        loadIndication: true,
        // autoload: true,
        pageLoading: true, 
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                filter.clientid = $("#show_uid").text();
                // console.log("cPortfolio filter");
                // console.log(filter);

                return jsgridAjax(filter, "people", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "people", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "people", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "people", "DELETE");
            },
        },
        fields: [
            { name: "uid", title: "編號" },
            { name: "name", title: "姓名" },
            { name: "sexual_text", title: "性別", width:60 },
            { name: "identify", title: "身分證字號" },
            { name: "birthday", title: "生日", 
                itemTemplate: function(value, item) { 
                    // console.log(item);
                    if(item.bornYear)
                        return item.bornYear+"/"+item.bornMonth+"/"+item.bornDay+" "+item.bornTime_text;
                    else
                        return "";
                }, 
            },
            { name: "telephone", title: "電話" },
            { name: "cellphone", title: "手機" },
            { name: "addr", title: "地址" },

            // { name: "productclassid", title: "分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "code", title: "產品代碼", type: "text" },
            // { name: "name", title: "產品名稱", type: "text" },
            { name: "ps", title: "備註", type: "text" },
            { type: "control" }
        ],
        rowClick: function(args) {
            // console.log(args);
        },
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#cLight").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#cLight").jsGrid("loadData");
        },
    });*/


	$('.modal select').material_select();

    Meteor.call("returnAgents", function(error, result){
        if(error){
            console.log("error from returnAgents: ", error);
        } 
        else {
            Session.set("agentUsers", result);
        }
    });

    $("#show-client-detail").hide();

    // $('.datepickerTW').datepickerTW();
    $('.datepicker').pickadate({
        // formatSubmit: 'yyyy/mm/dd',
        formatSubmit: 'R/mm/dd',
        // format: 'yyyy/mm/dd',
        format: 'R/mm/dd',
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 10, // Creates a dropdown of 15 years to control year
        
        monthsFull: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        monthsShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        weekdaysFull: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
        weekdaysShort: ['日', '一', '二', '三', '四', '五', '六'],
        showMonthsShort: undefined,
    });


    Tracker.autorun(function(){
        $('select').material_select('destroy');

        if(!Session.get('nowrow') ){
            $(".op-btn").hide();
        }
        else{
            $(".op-btn").show();
            var familyId = Session.get('nowrow').familyId;
            // console.log(familyId);

            Meteor.call("getFamilyLive", familyId, function(error, result){
            // console.log(result);
            
                $('#p1-whoapply')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>請選擇</option>')
                    // .val('whatever')
                ;
                $.each(result, function (i, item) {
                    $('#p1-whoapply').append($('<option>', { 
                        value: item._id,
                        text : item.name 
                    }));
                });
                $('#p4a-whoapply')
                    .find('option')
                    .remove()
                    .end()
                    // .append('<option value="" disabled selected>請選擇</option>')
                    // .val('whatever')
                ;
                $.each(result, function (i, item) {
                    $('#p4a-whoapply').append($('<option>', { 
                        value: item._id,
                        text : item.name 
                    }));
                });
                
                $('#p8a-whoapply')
                    .find('option')
                    .remove()
                    .end()
                    // .append('<option value="" disabled selected>請選擇</option>')
                    // .val('whatever')
                ;
                $.each(result, function (i, item) {
                    $('#p8a-whoapply').append($('<option>', { 
                        value: item._id,
                        text : item.name 
                    }));
                });
            });

            Meteor.call("getAddrFamilyLive", familyId, function(error, result){
                $('select').material_select('destroy');
                $('#addrlist')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>請選擇</option>')
                    // .val('whatever')
                ;
                $.each(result, function (i, item) {
                    $('#addrlist').append($('<option>', { 
                        value: item.addr,
                        text : item.name + " " + item.post5code + " " + item.addr,
                        post5:  item.post5code,
                        // serial:  item.now_num,
                    }));
                });
                $('select').material_select();
            });
            Meteor.call("getAddrFamilyPass", familyId, function(error, result){
                $('select').material_select('destroy');
                $('#addrlist2')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>請選擇</option>')
                    // .val('whatever')
                ;
                $.each(result, function (i, item) {
                    $('#addrlist2').append($('<option>', { 
                        value: item.addr,
                        text : item.name + " " + item.addr,
                        // post5:  item.post5code,
                        // serial:  item.now_num,
                    }));
                });
                $('#addrlist2').append('<option value="" disabled>內建地址</option>');
                $('#addrlist2').append($('<option>', {
                        value: "桃園佛教蓮社三樓往生堂",
                        text : "桃園佛教蓮社三樓往生堂",
                }));

                $('select').material_select();
            });
        
        }

        Meteor.subscribe("familyPeople", familyId);
    });
};
Template.cNewclient.helpers({
    agentUsers: function(){ 
        return Session.get("agentUsers");
    },
    now_familyid: function(){ 
        if(Session.get("nowrow")){

            var item = Session.get("nowrow");
            return item.familyId;
        }
        return "";
    },
/*    get_live_people: function() {
        if(Session.get("livePeople")){
            $('select').material_select('destroy');
            $('select').material_select();

            return Session.get("livePeople");
        }
        // return People.find({isLive:1});
    },
    get_pass_people: function() {
        // return Session.get("passPeople");
        return People.find({isLive:0});
    },*/
    get_sexual: function() {
        return objSexual2;
    },

    get_class: function() {
        return TempleClass.find({},{sort: {order: 1}});
    },
    get_level: function() {
        return TempleLevel.find({},{sort: {order: 1}});
    },
    get_time: function() {
        return arrTime;
    },
    get_zodiac: function() {
        return arrChineseYear;
    },
    get_pray1: function() {
        return Pray1.find({},{sort: {order_id: 1}});
    },
    get_praytype: function() {
        return PrayingType.find({},{sort: {order: 1}});
    },
    get_addr1: function(){
        var addr1 = Addr1.find({}, {sort:{post3:1}}).fetch();
        var groupedAddr = _.groupBy(_.pluck(addr1, 'addr1'));

        var arr = [];
        _.each(_.values(groupedAddr), function(data) {
            // var post3 = Addr1.find({addr1: data[0]}).fetch()[0].post3;
            arr.push({value: data[0]});
        });
        arr.splice(0, 1);
        // console.log(arr);

        return arr;
    },

/*    get_select: function() {
        console.log(this);
        if(this.now_use == "-1")
            return "0";
        else
            return "1";
    },
    isSelected: function(menuName){
        console.log(menuName);
       return (menuName == '-1') ? 'selected': '';
    }*/
/*    get_pray2: function() {
        return Pray2.find({class_id: Session.get("pray1Selected")});
    },
    get_pray4a_2: function() {
        return Pray2.find({class_id: Session.get("pray4aSelected")});
    },*/
});

Template.cNewclient.events({
    "change #p1-prayname": function(e){
        var pray1Selected = $("#p1-prayname option:selected").val();
        // console.log(pray1Selected);
        // Session.set("pray1Selected", pray1Selected);
        
        Meteor.call("getPray2", item.prayname, function(error, result){
        // console.log(result);
        
            $('select').material_select('destroy');
            $('#p1-prayitem')
                .find('option')
                .remove()
                .end()
                .append('<option value="" disabled selected>請選擇</option>')
                // .val('whatever')
            ;
            $.each(result, function (i, item) {
                $('#p1-prayitem').append($('<option>', { 
                    value: item._id,
                    text : item.value 
                }));
            });
            // $("#p1-prayitem").val(item.prayitem);
            $('select').material_select();
            // $(".my-praying2-detail").fadeIn();
            // $("#cPray2").jsGrid("loadData");
        });
    },
    "change #addrlist": function(e){
        var str = $('option:selected', "#addrlist").val();
        if(str){
            $("#addr").val(str);
            $("#m2-addr").addClass("active");
        }
        var post = $('option:selected', "#addrlist").attr("post5");
        if(post){
            $("#post5code").val(post);
            $("#m2-post5code").addClass("active");
        }        
    },
    "change #addrlist2": function(e){
        var str = $('option:selected', "#addrlist2").val();
        if(str){
            $("#addr").val(str);
            $("#m2-addr").addClass("active");
        }
        $("#post5code").val("");
        $("#m2-post5code").removeClass("active");
    },
    "change #addr1": function(e){
        var addr1Selected = $("#addr1 option:selected").val();
        // var addrText1 = $('#addr1 :selected').text();
        // console.log(addr1Selected + " "+ addrText1);

        // $("#post5code").val();
        
        if(!!addr1Selected){
            $("#addr").val(addr1Selected);
            $("#m2-addr").addClass("active");
            $("#addr2").val("");
        }
        else{
            $("#addr").val("");
            $("#m2-addr").removeClass("active");
        }

        var result = Addr1.find({addr1: addr1Selected}, {sort:{post3:1}}).fetch();

        $('#addr2')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>請選擇</option>')
            // .val('whatever')
        ;
        $('#addr3')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>請選擇</option>')
            // .val('whatever')
        ;
        $('#addr4')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>請選擇</option>')
            // .val('whatever')
        ;
        $.each(result, function (i, item) {
            $('#addr2').append($('<option>', { 
                value: item.post3,
                text : item.addr2,
                addr1:  item.addr1,
            }));
        });
    },
    "change #addr2": function(e){
        var addr1 = $('option:selected', "#addr2").attr("addr1");
        var addr2 = $('#addr2 :selected').text();
        var addr2_post = $("#addr2 option:selected").val();
        
        // console.log(addr2_post  + " "+ addr1 + " "+ addr2);

        if(!!addr2){
            $("#post5code").val(addr2_post);
            $("#addr").val(addr1 + addr2);
            $("#m2-post5code").addClass("active");
            $("#m2-addr").addClass("active");
        }
        else{
            $("#post5code").val("");
            $("#addr").val("");
            $("#m2-post5code").removeClass("active");
            $("#m2-addr").removeClass("active");
        }

        $('#addr3')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>請選擇</option>')
        ;
        $('#addr4')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>請選擇</option>')
        ;
        Meteor.call("getAddr3", addr1, addr2, function(error, result){
            // console.log(result);
        
            $('select').material_select('destroy');

            $.each(result, function (i, item) {
                $('#addr3').append($('<option>', { 
                    value: item.post5,
                    text : item.addr3,
                    addr1 : item.addr1,
                    addr2 : item.addr2,
                }));
            });
            $('select').material_select();
        });
    },
    "change #addr3": function(e){
        var addr1 = $('option:selected', "#addr3").attr("addr1");
        var addr2 = $('option:selected', "#addr3").attr("addr2");
        var addr3 = $('#addr3 :selected').text();
        var post3 = $("#addr3 option:selected").val().substr(0, 3);

        if(!!addr3){
            $("#post5code").val(post3);
            $("#addr").val(addr1 + addr2 + addr3);
            $("#m2-post5code").addClass("active");
            $("#m2-addr").addClass("active");
        }
        else{
            $("#post5code").val("");
            $("#addr").val("");
            $("#m2-post5code").removeClass("active");
            $("#m2-addr").removeClass("active");
        }

        $('#addr4')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>請選擇</option>')
        ;

        Meteor.call("getAddr4", addr1, addr2, addr3, function(error, result){
            // console.log(result);
        
            $('select').material_select('destroy');

            $.each(result, function (i, item) {
                $('#addr4').append($('<option>', { 
                    value: item.post5,
                    text : item.addr4,
                    addr1 : item.addr1,
                    addr2 : item.addr2,
                    addr3 : item.addr3,
                }));
            });
            $('select').material_select();
        });
    },
    "change #addr4": function(e){
        var addr1 = $('option:selected', "#addr4").attr("addr1");
        var addr2 = $('option:selected', "#addr4").attr("addr2");
        var addr3 = $('option:selected', "#addr4").attr("addr3");
        var addr4 = $('#addr4 :selected').text().substr(2).replace("以下", "").replace("以上", "").trim();
        var post5 = $("#addr4 option:selected").val();

        // if(!!addr4){
            $("#post5code").val(post5);
            $("#addr").val(addr1 + addr2 + addr3 + addr4);
            $("#m2-post5code").addClass("active");
            $("#m2-addr").addClass("active");
        // }
        // else{
        //     $("#post5code").val("");
        //     $("#addr").val("");
        //     $("#m2-post5code").removeClass("active");
        //     $("#m2-addr").removeClass("active");
        // }
    },
    "change #post5code": function(e){

        // var item = Session.get("nowrow").familyId;
        // console.log(item);

        // Meteor.call("getAddrFamilyLive", item, function(){
        //     $('select').material_select('destroy');
        //     $('#addrlist')
        //         .find('option')
        //         .remove()
        //         .end()
        //         .append('<option value="" selected>請選擇</option>')
        //         // .val('whatever')
        //     ;
        //     $.each(result, function (i, item) {
        //         $('#addrlist').append($('<option>', { 
        //             value: item.addr,
        //             text : item.addr,
        //             // price:  item.price,
        //             // serial:  item.now_num,
        //         }));
        //     });
        //     $('select').material_select();
        // });
    },
    "change #p4b-type": function(e){
        var str = $('option:selected', "#p4b-type").attr("data-def");
        if(str){
            $("#p4a-passname_text").val(str);
            $("#p4a-passname_text-label").addClass("active");
        }
        else{
            $("#p4a-passname_text").val("");
            $("#p4a-passname_text-label").removeClass("active");
        }
    },
    // 'change #icon_prefix3, keypress #icon_prefix3, click #icon_prefix3': function (event) { //#
    'change #icon_prefix3, click #icon_prefix3': function (event) { //#
        // $("#cNewclient").jsGrid("loadData");
        // $("#cNewclient").jsGrid("openPage",1);
        Template.cNewclient.loadClients();
    },
    'click .addr-key': function (event) {
        var t = $(event.currentTarget).text();
        $("#addr").val( $("#addr").val()+ t);
    },
    'click .addr-key2': function (event) {
        var str = $("#addr").val();
        $("#addr").val( str.substr(0, str.length-1) );
    },
    'click .move_live0': function (event) {
        // console.log($(event.currentTarget).attr('data-id'));
        if(confirm("確定要將此人移至拔渡嗎?")){
            Meteor.call("updateClientLive0", $(event.currentTarget).attr('data-id'), function(){
                $("#cLive").jsGrid("loadData");
                $("#cPass").jsGrid("loadData"); 
            });
        }
    },
    'click .move_live1': function (event) {
        if(confirm("確定要將此人移至陽上嗎?")){
            Meteor.call("updateClientLive1", $(event.currentTarget).attr('data-id'), function(){
                $("#cLive").jsGrid("loadData");
                $("#cPass").jsGrid("loadData"); 
            });
        }
    },
    'click .my-del-family': function (event) {
        if(confirm("確定刪除此戶嗎?")){
            Meteor.call("deleteFamily", $(event.currentTarget).attr('data-id'), function(){
                $("#show-client-detail").hide();
                $("input#icon_prefix3").val("");
                $("#cNewclient").jsGrid("loadData");
            });
        }
    },
    'click .my-btn-submit2a': function (event) { // 新增家庭
    // 'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();

        if(!$("#name").val()){
            alert("姓名欄位不得為空");
            $("#name").focus();
            return;
        }

        var formVar = $(".form-modal2").serializeObject();
        if($('#sexual :selected').text() != "請選擇")
          formVar.sexual_text = $('#sexual :selected').text();
        if($('#bornTime :selected').text() != "請選擇")
         formVar.bornTime_text = $('#bornTime :selected').text();
        if($('#zodiac_id :selected').text() != "請選擇")
          formVar.zodiac_text = $('#zodiac_id :selected').text();
        if($('#templeClass :selected').text() != "請選擇")
         formVar.templeClass_text = $('#templeClass :selected').text();
        if($('#templeLevel :selected').text() != "請選擇")
          formVar.templeLevel_text = $('#templeLevel :selected').text();

        Meteor.call("createFamily", formVar, function(error, result){
            // console.log(result);
            // console.log(formVar);
            // $("#cLive").jsGrid("loadData");
            // $("#cPass").jsGrid("loadData"); 
            if(result.dupName || result.dupNameAddr || result.dupAddr){

                var str = "";
                if(result.dupName){
                    str = str + "同名 "+result.dupName+" 位：\n"+result.names + "\n";
                }
                if(result.dupNameAddr){
                    str = str + "同名同地址 "+result.dupNameAddr+" 位：\n"+result.nameaddrs+ "\n";
                }
                if(result.dupAddr){
                    str = str + "同地址 "+result.dupAddr+"位：\n"+result.addrs+ "\n";
                }



                if(!confirm(str+"仍要強制寫入此筆資料嗎?")){
                    Meteor.call("delFamily", result.fid);
                    return;
                }
            }

            formVar.familyId = result.fid;
            formVar.isLive = 1;
            formVar.mainPerson = -1;
            formVar.mailThis = "-1";


            $("#cNewclient").jsGrid("insertItem", formVar).done(function(ret) {
                console.log("insertion completed");
                console.log(ret);
                if (ret.insert == "success") {
                    // $('#modal2').closeModal();
                    $('#modal2').toggle();
                    Session.set("nowrow", formVar);
                console.log(formVar);
                    $(".form-modal2").trigger('reset');

                    $("#show-client-detail").hide();
                    $("input#icon_prefix3").val("");
                    $("#cNewclient").jsGrid("loadData");
                    $("#cLive").jsGrid("loadData");
                    $("#cPass").jsGrid("loadData");
                    $("#show-client-detail").fadeIn();

                    // Materialize.toast('資料已新增', 3000, 'rounded');
                } else {
                    $(".modal2-error").text(ret);
                }
            });
        });
    },
    'click .my-btn-submit2b': function (event) { // 新增成員
    // 'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();

        if(!$("#name").val()){
            alert("姓名欄位不得為空");
            $("#name").focus();
            return;
        }

        var formVar = $(".form-modal2").serializeObject();
        if($('#sexual :selected').text() != "請選擇")
          formVar.sexual_text = $('#sexual :selected').text();
        if($('#bornTime :selected').text() != "請選擇")
         formVar.bornTime_text = $('#bornTime :selected').text();
        if($('#zodiac_id :selected').text() != "請選擇")
          formVar.zodiac_text = $('#zodiac_id :selected').text();
        if($('#templeClass :selected').text() != "請選擇")
         formVar.templeClass_text = $('#templeClass :selected').text();
        if($('#templeLevel :selected').text() != "請選擇")
          formVar.templeLevel_text = $('#templeLevel :selected').text();

        // console.log(formVar);
        
        var item = Session.get("nowrow");
        formVar.familyId = item.familyId;
        formVar.isLive = 1;
        formVar.mainPerson = 0;
        formVar.mailThis = "0";

        $("#cNewclient").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#modal2').toggle();
                $(".form-modal2").trigger('reset');
                $("#cLive").jsGrid("loadData");

                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .my-btn-submit3': function (event) { // 新增拔渡對象
    // 'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();

        if(!$("#name").val()){
            alert("姓名欄位不得為空");
            $("#name").focus();
            return;
        }

        var formVar = $(".form-modal2").serializeObject();
        if($('#bornTime :selected').text() != "請選擇")
            formVar.bornTime_text = $('#bornTime :selected').text();
        if($('#passTime :selected').text() != "請選擇")
            formVar.passTime_text = $('#passTime :selected').text();

        // console.log(formVar);
        
        var item = Session.get("nowrow");
        formVar.familyId = item.familyId;
        formVar.isLive = 0;

        $("#cNewclient").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#modal2').toggle();
                $(".form-modal2").trigger('reset');
                $("#cPass").jsGrid("loadData");

                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .my-open-modal2a': function() {
        $("#modal2-header-text").text("新增家庭 - 戶長資料");
        $(".form-modal2").trigger('reset');
        
        $(".my-btn-submit2a").show();
        $(".my-btn-submit2b").hide();
        $(".my-btn-submit3").hide();

        $(".live-form-field").show(); 
        $(".pass-form-field").hide(); 
        $(".live-addr-select").hide(); // 陽上的地址
        
        // $('#modal2').openModal();
        $('#modal2').toggle();
    },
    'click .my-open-modal2b': function() {
        $("#modal2-header-text").text("新增家庭成員");
        $(".form-modal2").trigger('reset');

        $(".my-btn-submit2b").show();
        $(".my-btn-submit2a").hide();
        $(".my-btn-submit3").hide();


        $(".live-form-field").show(); // 陽上的地址
        $(".pass-form-field").hide(); // 拔渡的地址

        // $('#modal2').openModal();
        $('#modal2').toggle();
    },
    'click .my-btn-cancel2': function() {
        // $('#modal2').closeModal();
        $('#modal2').toggle();
    },
    'click .my-open-modal3': function() { // 拔渡
        $("#modal2-header-text").text("新增拔渡對象");
        $(".form-modal2").trigger('reset');

        $(".live-form-field").hide();
        $(".live-addr-select").show();
        $(".pass-form-field").show();

        $(".my-btn-submit2b").hide();
        $(".my-btn-submit2a").hide();
        $(".my-btn-submit3").show();

        // $('#modal2').openModal();
        $('#modal2').toggle();
    },
    'click .my-btn-cancel3': function() {
        // $('#modal3').closeModal();
        $('#modal3').toggle();
    },
    "click #b1-test2": function(e){
        $('input[name="name2"]:checkbox').prop('checked', $("#b1-test2").prop("checked"));
    },
    "click #b2-test2": function(e){
        $('input[name="name2b"]:checkbox').prop('checked', $("#b2-test2").prop("checked"));
    },
    'click .my-btn-submit1': function() {
        $("#pdf-iframe").get(0).contentWindow.print();
        // $('#modal1').closeModal();
        $('#modal1').toggle();
    },
    'click .my-open-modal6': function() {
        if(!Session.get("nowrow") || !Session.get("nowrow").familyId){
            return;
        }
        $("#pdf-iframe").prop("src", "");
        // $('#modal1').openModal();
        $('#modal1').toggle();

        // 起始的東西在這邊加
        var formVar = "/api/pdffamily?familyId="+Session.get("nowrow").familyId;
        // console.log(formVar);
        
        $("#pdf_url").prop("href", formVar);
        $("#pdf-iframe").prop("src", formVar);
    },
/*    'click .my-open-modal5': function() {
        $("#modal2-header-text").text("修改客戶資料");
        $(".form-modal2").trigger('reset');
        var item = Session.get("nowrow");
        // console.log(item);
        
        $('select').material_select('destroy');

        $("#agentid").val(item.agentid);
        $("#name_cht").val(item.name_cht);
        $("#name_eng").val(item.name_eng);
        $("#gender").val(item.gender);
        $("#countryid").val(item.countryid);
        $("#birthday").val(item.birthday);
        $("#identify").val(item.identify);
        $("#marriageid").val(item.marriageid);
        $("#graduateid").val(item.graduateid);
        $("#cellnum").val(item.cellnum);
        $("#faxnum").val(item.faxnum);
        $("#email").val(item.email);
        $("#addr1_phone").val(item.addr1_phone);
        $("#addr1_post5").val(item.addr1_post5);
        $("#addr1_cht").val(item.addr1_cht);
        $("#addr1_eng").val(item.addr1_eng);
        $("#addr2_issame").val(item.addr2_issame);
        $("#addr2_post5").val(item.addr2_post5);
        $("#addr2_cht").val(item.addr2_cht);
        $("#addr2_eng").val(item.addr2_eng);
        $("#addr2_phone").val(item.addr2_phone);
        $("#fin_type").val(item.fin_type);
        $("#fin_namecht").val(item.fin_namecht);
        $("#fin_nameeng").val(item.fin_nameeng);
        $("#fin_addr").val(item.fin_addr);
        $("#fin_workexp").val(item.fin_workexp);
        $("#fin_yearsalary").val(item.fin_yearsalary);
        $("#fin_cashtype").val(item.fin_cashtype);
        $("#eval_exp").val(item.eval_exp);
        $("#eval_purpose").val(item.eval_purpose);
        $("#eval_risk").val(item.eval_risk);
        $("#eval_ret").val(item.eval_ret);
        // $("#agentid").val(item.agentid);
        
        $('select').material_select();
        $(".my-btn-submit5").show();
        $(".my-btn-submit").hide();
        
        $('#modal2').openModal();
    },*/
});
