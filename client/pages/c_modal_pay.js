Template.modalPay = function(cash){
    if (!Session.get("nowrow_pay")){
        alert("無法取得對應的服務編號");
        return;
    }

    $('.datepicker').pickadate({
        formatSubmit: 'yyyy/mm/dd',
        format: 'yyyy/mm/dd',
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 10, // Creates a dropdown of 15 years to control year
        
        monthsFull: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        monthsShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        weekdaysFull: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
        weekdaysShort: ['日', '一', '二', '三', '四', '五', '六'],
        showMonthsShort: undefined,
    });

    var num = Session.get("nowrow_pay");
    $(".pay-number").text(num);
    $('#p4a-date-paid').val(currentdate());

    $("#p4a-cash-paid1").val(cash.cash); // 應收
    $("#p4a-cash-paid2").val(cash.cash_paid); // 已收
    $("#p4a-cash-paid3").val(cash.cash_balance); // 未收
    
    $("#cPay").jsGrid("loadData");   
    // Tracker.autorun(function () {
    //     // 剩餘的
    //     if(Session.get("nowrow_praying1")){
    //         var s_p1 = Session.get("nowrow_praying1");

    //         $("#p4a-cash-paid1").val(cash.cash); // 應收
    //         $("#p4a-cash-paid2").val(cash.cash_paid); // 已收
    //         $("#p4a-cash-paid3").val(cash.cash_balance); // 未收
    //         // $("#p4a-cash-paid1").val( $(".title-cash").text() ); // 應收
    //         // $("#p4a-cash-paid2").val( $(".title-cash-paid").text() ); // 已收
    //         // $("#p4a-cash-paid3").val( $(".title-cash-balance").text() ); // 未收
    //     }
    // }); 
}
Template.cModalPay.rendered = function() {
    $("#cPay").jsGrid({
        height: "90%",
        width: "100%",
        // sorting: true,
        // paging: true,
        // filtering: true,
        // editing: true,
        // pageSize: 15,
        noDataContent: "尚無繳款記錄",
        loadIndication: true,
        // autoload: true,
        pageLoading: true, 
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                var item = Session.get("nowrow_pay");
                filter.ordernum = item;
                if(!filter.sortField){
                    filter.sortField = "insertedAt";
                    filter.sortOrder = "desc";
                }

                return jsgridAjax(filter, "booking", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "booking", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "booking", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "booking", "DELETE");
            },
        },
        fields: [
            // { type: "control" },
            // { name: "uid", title: "編號" },
            { name: "paid_date", title: "收款日期", type: "text", width: 80},
            { name: "paymethod_text", title: "收款方式", type: "text", width: 70},
            // { name: "shouldpay", title: "服務金額", type: "text", width: 70},
            { name: "received", title: "本次收款金額", type: "text", width: 70},
            // { name: "cash_paid", title: "已收金額", type: "text", width: 70},
            // { name: "cash_balance", title: "剩餘金額", type: "text", width: 70},
            { name: "ps", title: "備註", type: "text"},
            { name: "insertedName", title: "經辦人", type: "text", width: 60},
            { name: "insertedAt", title: "登錄時間", //width: 150,
                itemTemplate: function(value, item) {
                    if(!!item.insertedAt)
                        return (new Date(item.insertedAt)).yyyymmddhm();
                    else
                        return "";
                },
            },
            
        ],
  /*      rowClick: function(args) {
            // console.log(args);
        },
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            // $("#cPass").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#cPass").jsGrid("loadData");
        },*/
    });
};

Template.cModalPay.helpers({
    // get_countries: function() {
    //     return objCountries;
    // },
    objPayMethod: function() {
        return objPayMethod;
    },
});

Template.cModalPay.events({
    'click .my-btn-submit8': function (event) { // 確認付款
        event.preventDefault();
        event.stopPropagation();

        if(!$("#p4a-cash-now").val()){
            alert("請填入本次收款金額");
            return;
        }
        if(!$("#p4a-cash-now").val() <= 0){
            alert("收款金額錯誤");
            return;
        }
        var formVar = $(".form-modal8").serializeObject();

        formVar.ordernum = Session.get("nowrow_pay");
        formVar.paymethod_text = $('#p4a-paymethod :selected').text();

        // var pray1 = Session.get("nowrow_praying1");
        // formVar.pray1_id = pray1.prayname;
        // formVar.pray1_text = pray1.prayname_text;
        // formVar.pray2_id = pray1.prayitem;
        // formVar.pray2_text = pray1.prayitem_text;
        // formVar.prayserial = pray1.prayserial;

        // console.log(formVar);
        // return;

        Meteor.call("insertBooking", formVar, function(error, result){
            Materialize.toast('款項已輸入', 3000, 'rounded');
            $("#p4a-cash-now").val("");
            $("#p4a-ps").val("");

            // console.log("result");
            // console.log(result);

            // pray1.cash_paid = result.cash_paid;
            // Session.set("nowrow_praying1", pray1);
            // Session.set("nowrow_praying1", result);

            $("#p4a-cash-paid1").val(result.shouldpay); // 應收
            $("#p4a-cash-paid2").val(result.cash_paid); // 已收
            $("#p4a-cash-paid3").val(result.cash_balance); // 未收

            $("#cPay").jsGrid("loadData");
            $("#cPray1").jsGrid("loadData");

            $("#cPraying1").jsGrid("loadData");
            // $('#modalPay').toggle();
        });
    },

});

