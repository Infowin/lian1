Template.bPeople.rendered = function(){

    $('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
    $('ul.tabs').tabs();
    $("#bPeople").jsGrid({
        height: "90%",
        width: "100%",
 
        sorting: true,
        paging: true,
		filtering: true,
		// editing: true,

 		loadIndication: true,
 		autoload: true,
 		pageLoading: true, 
 		loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
 		controller:{
		    loadData: function(filter) {
                return jsgridAjax(filter, "clientlist", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "clientlist", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "clientlist", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "clientlist", "DELETE");
            },
		},
        fields: [
            { name: "reviewid", title: "職稱" },
            // { name: "uid", title: "#" },
            { name: "username", title: "姓名" },

            // { name: "productclassid", title: "分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "code", title: "產品代碼", type: "text" },
            // { name: "name", title: "產品名稱", type: "text" },
            // { name: "ps", title: "備註", type: "text" },
            // { type: "control" }
        ],
        onDataLoading: function(args) {
			$('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
        	// Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
        	// Materialize.toast('資料已更新', 3000, 'rounded')
        	// console.log(args);
        	$("#bPeople").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
        	// Materialize.toast('資料已刪除', 3000, 'rounded')
        	$("#bPeople").jsGrid("loadData");
        },
    });
	$('.modal select').material_select();
};

Template.bPeople.helpers({
  counter: function () {
    // return Session.get('counter');
  }
});

Template.bPeople.events({
  'click button': function () {
    // Session.set('counter', Session.get('counter') + 1);
  }
});
