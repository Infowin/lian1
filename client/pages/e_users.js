Template.eUsers.rendered = function() {
    $("#eUsers").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "clientlist", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "clientlist", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "clientlist", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "clientlist", "DELETE");
            },
        },
        fields: [
            { name: "uid", title: "#", width: 30 },
            // { name: "team", title: "團隊", type: "select", items: [{ id: "", value: "" }].concat(Teams.find().fetch()), valueField: "_id", textField: "teamname",
            	// itemTemplate: function(value, item) { 
            		// return item.profile.teamname;
             	// },  
			    // insertTemplate: function(value) {
			    //     return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
			    // },
			    // editTemplate: function(value) {
			    //     return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
			    // },
			    // insertValue: function() {
			    //     return this._insertPicker.datepicker("getDate").toISOString();
			    // },
			    // editValue: function() {
			    //     return this._editPicker.datepicker("getDate").toISOString();
			    // }
         	// },
            { name: "roles", title: "權限", type: "select", items: [{ id: "", value: "" }].concat(objUserRole), valueField: "id", textField: "value" },
            { name: "username", title: "帳號", type: "text" },
            { name: "name", title: "中文姓名", type: "text" },
            { name: "email", title: "電子郵件"  },
            // { name: "name2", title: "英文姓名", type: "text" },
            /*{ name: "cms1", title: "房產等級", type: "text" },
            { name: "cms2", title: "安養院等級", type: "text" },
            { name: "cms3", title: "保險等級", type: "text" },*/
            { name: "loginedAt", title: "上次上線" },
            { name: "createdAt", title: "建立日期" },
            { type: "control" }
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            // $("#aProduct").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#aProduct").jsGrid("loadData");
        },
    });
    $('.modal select').material_select();
};

Template.eUsers.helpers({
  /*  get_productclass: function() {
        return objProductClass;
    },*/
    /*get_providers: function() {
        return Provider.find();
    }*/
});

Template.eUsers.events({
    // 'click .my-btn-submit': function () {
    'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal1").serializeObject();
        formVar.productclassname = $('#productclassid :selected').text();
        formVar.providername = $('#providerid :selected').text();
        console.log(formVar);

        $("#eUsers").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                $('#modal1').closeModal();
                $(".form-modal1").trigger('reset');
                $("#eUsers").jsGrid("loadData");
                Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    'click .my-open-modal1': function() {
        $('#modal1').openModal();
    },
    'click .label-provider': function() {
        $('#modal1').closeModal();
        Meteor.setTimeout(function() { Router.go('e_provider'); }, 10);
    },
    'click .my-btn-cancel': function() {
        $('#modal1').closeModal();
    }
});

// Template.eUsers.rendered = function(){
	// var proxy = webix.proxy("meteor", users);
/*

	var grid = {
		id:"productsData",
		view:"datatable", 
		select:true, 
		editable:true, editaction:"dblclick",
    	scroll:"xy",
		columns:[
			{ id: "uRole", sort: "server", header: "帳戶等級", fillspace: 1, minWidth: 100}, 
			{ id: "uStatus", header: [{ content: "columnGroup", batch: "system", groupText: "基本資料", colspan:5 }, "帳戶狀態"], fillspace: 1, minWidth: 90, editor: "text" }, 
	      	{ id: "username", batch: "system", header: [null, "登入帳號"], fillspace: 1, minWidth: 120 },
	      // { id:"uPassword",       batch:"system", sort:"server",  header:[null, "重設密碼"], fillspace:1 , minWidth:120 },
	        { id: "uName", template: "#profile.name#", batch: "system", sort: "server", header: [null, "中文姓名"], fillspace: 1, minWidth: 120}, 
			{ id: "uGroupLoc", batch: "system", sort: "server", header: [null, "地區團隊"], fillspace: 1, minWidth: 120}, 
	      	
	      	{
	        id: "uEmail",
	        template: "#emails[0].address#",
	        batch: "system",
	        sort: "server",
	        header: [null, "電子信箱"],
	        fillspace: 1,
	        minWidth: 120
	      }, {
	        id: "uCommssionReal",
	        header: [{
	          content: "columnGroup",
	          batch: "commission",
	          groupText: "目前發佣等級",
	          colspan: 3
	        }, "房產"],
	        fillspace: 1,
	        minWidth: 90
	      }, {
	        id: "uCommssionOlder",
	        batch: "commission",
	        sort: "server",
	        header: [null, "安養院"],
	        fillspace: 1,
	        minWidth: 120
	      }, {
	        id: "uCommssionIns",
	        batch: "commission",
	        sort: "server",
	        header: [null, "保險"],
	        fillspace: 1,
	        minWidth: 120
	      }, {
	        id: "uOperate",
	        sort: "server",
	        header: "操作",
	        fillspace: 1,
	        minWidth: 100
	      }, {
	        id: "createdAt",
	        sort: "server",
	        header: "建立日期",
	        fillspace: 1,
	        minWidth: 100
	      }, {
	        id: "updatedAt",
	        sort: "server",
	        header: "修改日期",
	        fillspace: 1,
	        minWidth: 100
	      },
	      
			{id:"edit", header:"&nbsp;", width:35, template:"<span  style=' cursor:pointer;' class='webix_icon fa-pencil'></span>"},
			{id:"delete", header:"&nbsp;", width:35, template:"<span  style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},
		],
		pager:"pagerA",
		resizeColumn:true,
	    url: webix.proxy("meteor", Meteor.users.find()),
	    save: webix.proxy("meteor", Meteor.users),
	    onClick:{
			"fa-trash-o":function(e,id,node){
				webix.confirm({
					text:"確定刪除本項目嗎?", ok:"確定", cancel:"取消",
					callback:function(res){
						if(res){
							var item = $$("productsData").getItem(id);
				          	$$("productsData").remove(item.id);
						}
					}
				});
			}
		}
	};
	var layout = {
		type: "space",
		rows:[
			{
				height:40,
				cols:[
					{ view: "button", type: "iconButton", icon: "plus", label: "新增", width: 90, click: function(){
						$$("product-win").show();
					}},
					{},
					{view:"richselect", id:"order_filter", value: "all", maxWidth: 300, minWidth: 250, vertical: true, labelWidth: 110, options:[
						{id:"all", value:"All"},
						{id:"1", value:"Published"},
						{id:"2", value:"Not published"},
						{id:"0", value:"Deleted"}
					],  label:"Filter products", on:{
						onChange:function(){
							var val = this.getValue();
							if(val=="all")
								$$("productsData").filter("#status#","");
							else
								$$("productsData").filter("#status#",val);
						}
					}
					}
				]
			},
			{
				rows:[
					grid,
					{
						view: "toolbar",
						css: "highlighted_header header6",
						paddingX:5,
						paddingY:5,
						height:40,
						cols:[{
							view:"pager", id:"pagerA",
							template:"{common.first()}{common.prev()}&nbsp; {common.pages()}&nbsp; {common.next()}{common.last()}",
							autosize:true,
							height: 35,
							group:5
						}]
					}
				]
			}
		]
	};
	var webixContainer = webix.ui({
		id:"my-all-create",
		// type:"wide",
		type:"clean", //, css:"app-right-panel"
		rows:[
			layout
		]//,
	}, this.find("#divA"));
	 Template.hello.myResize();*/
// };
