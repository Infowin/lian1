Template.eAccount.rendered = function() {
    $("#eAccount1").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        // paging: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        // rowClick: $.noop,
        rowClick: function(args) {
            // console.log(args);
            $("#show-detail").hide();
            
            Session.set("nowrow_Account", args.item);
            // $("#eAccount2").jsGrid("loadData", { type_id: args.item._id});
            $("#eAccount2").jsGrid("loadData");

            $("#show-detail").fadeIn("slow");
        },
        rowDoubleClick: jsGrid.Grid.prototype.rowClick,
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.sortField = "order_id";
                filter.sortOrder = "asc";

                return jsgridAjax(filter, "account1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "account1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "account1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "account1", "DELETE");
            },
        },
        fields: [
            { name: "order_id", title: "順序", type: "number", editing: false },
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
           /* { name: "now_use", type: "checkbox", title: "預設",
                itemTemplate: function(value, item) { 
                    // console.log(item);
                    if(item.now_use == "true")
                        return '<input type="checkbox" checked="checked" disabled="disabled">';
                    else
                        return '<input type="checkbox" disabled="disabled">';
                },
            },*/
            // { name: "now_id", title: "目前編號", type: "text" },
            { name: "value", title: "類別", type: "text" },
            // { name: "ps", title: "備註", type: "text", width: 200 },
            { type: "control" }
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onDataLoading: function(args) {
            $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#eAccount1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#eAccount").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#eAccount1 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    Meteor.call("updateSortAcc1type", items, function(error, result){
                        if(error){
                            console.log("error from updateSortAcc1type: ", error);
                        } 
                        else {
                            $("#eAccount1").jsGrid("loadData");
                        }
                    });
                }
            });
        },
    });    

    $("#eAccount2").jsGrid({
        height: "90%",
        width: "100%",

        sorting: true,
        // paging: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_Account");
                filter.type_id = item._id;

                filter.sortField = "order_id";
                filter.sortOrder = "asc";
                
                return jsgridAjax(filter, "account2", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "account2", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "account2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "account2", "DELETE");
            },
        },

        /*
          { id:"",  header:"順序", width:55 },
      { id:"",     editor:"text",  header:"名稱", fillspace:1},
      { id:"now_num",   editor:"text",  header:"目前編號", width:90 },
         */
        fields: [
            { type: "control", width: 60 },
            { name: "order_id", title: "順序", width: 70, type:"number", editing:false },
            // { name: "name", title: "編號種類", type: "text" },
            { name:"sub_code",   type:"text",  title:"代碼" },
            { name:"sub_name",   type:"text",  title:"名稱" },
            // { name:"Account_paper", type:"select", title:"列印紙張", items:objPaperSize, valueField: "id", textField: "value"  },
            // { name: "ps", title: "備註", type: "text", width: 200 },
        ],
        // onDataLoading: function(args) {
        //     $('.jsgrid select').material_select();
        // },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        // onItemUpdating: function(args) {
        //     console.log(args);
        //     var item = Session.get("nowrow_Account");
        // },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            // $("#eAccount2").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#eAccount").jsGrid("loadData");
            var item = Session.get("nowrow_Account");
            $("#eAccount2").jsGrid("loadData", { type_id: item._id});
        },
        onRefreshed: function() {
            var $gridData = $("#eAccount2 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    Meteor.call("updateSortAcc2subject", items, function(error, result){
                        if(error){
                            console.log("error from updateSortAcc2subject: ", error);
                        } 
                        else {
                            $("#eAccount2").jsGrid("loadData");
                        }
                    });
                }
            });
        },
    });
    $('.modal select').material_select();
    $("#show-detail").hide();
};

Template.eAccount.helpers({
    get_countries: function() {
        return objCountries;
    },
    show_name: function(){
        var item = Session.get("nowrow_Account");
        if(!!item && !!item.value) 
            return item.value;
        return "";
    },
    show_name_id: function(){
        var item = Session.get("nowrow_Account");
        if(!!item && !!item._id) 
            return item._id;
        return "";
    }
});

Template.eAccount.events({
    'click .my-btn-submit1': function () {
        var formVar = $(".form-modal1").serializeObject();

        $("#eAccount1").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                // $('#modal1').closeModal();
                $('#modal1').toggle();
                $(".form-modal1").trigger('reset');
                $("#eAccount1").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    'click .my-open-modal1': function() {
        // $('#modal1').openModal();
        $('#modal1').toggle();
    },
    'click .my-btn-cancel1': function() {
        // $('#modal1').closeModal();
        $('#modal1').toggle();
    },
    'click .my-btn-submit2': function () {
        var formVar = $(".form-modal2").serializeObject();

        $("#eAccount2").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#modal2').toggle();
                $(".form-modal2").trigger('reset');
                
                var item = Session.get("nowrow_Account");
                $("#eAccount2").jsGrid("loadData", { type_id: item._id});
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .my-open-modal2': function() {
        // $('#modal2').openModal();
        $('#modal2').toggle();
    },
    'click .my-btn-cancel2': function() {
        // $('#modal2').closeModal();
        $('#modal2').toggle();
    }
});
