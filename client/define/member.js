Member = {};

Member.custom_addr = function(obj, common, value){
	var str = "";
	if(obj.post5code) str = obj.post5code+" ";
	if(obj.addr) str += obj.addr;
	return str;
}
Member.custom_checkbox = function (obj, common, value){
	return (Number(value))?'<input type="checkbox" disabled checked>':'<input type="checkbox" disabled >';
}
Member.custom_birstday = function(obj, common, value){
	var year = month = day = time = 0;
	if(obj.bornYear) 	year = obj.bornYear;
	if(obj.bornMonth) 	month = obj.bornMonth;
	if(obj.bornDay) 	day = obj.bornDay;
	if(obj.bornTime) 	time = obj.bornTime;

	if( year || month || day )
		return year+"/"+month+"/"+day+" "+arrTime[time].value;
	return "";
}