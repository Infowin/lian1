
// MyWebix = {};
// MyWebix.datatable = {};
// MyWebix.datatable.sort = function(dt, col, ord){
// 	  	// console.log(ord);
// 	var index = 1;
// 	dt.eachRow( 
// 	  function (row){ 
// 	  	console.log(row);
// 	    // col.update(this.getItem(row).id, { $set:{ ord: index++ }});
// 	    col.update(row, { $set:{ ord: index++ }});
// 	  }
// 	);
// }
// 
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

jsgridAjax = function(item, path, type){
    // var token = Meteor.user().services.resume.loginTokens[Meteor.user().services.resume.loginTokens.length-1].hashedToken;
    return $.ajax({
        // headers: {
        //     'x-auth-token' : token,
        //     'x-user-id': Meteor.user()._id
        // },
        type: type,
        url: "/api/"+path,
        data: item,
        dataType: "json"
    });
}