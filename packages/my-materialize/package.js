Package.describe({
  name: 'my-materialize',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use('ecmascript');
  api.use('jquery', 'client');
  api.use('materialize:materialize');

  api.addFiles('css/materialize.css', 'client');
  api.addFiles('css/style.css', 'client');
  api.addFiles('css/custom/custom.css', 'client');
  api.addFiles('js/plugins/prism/prism.css', 'client');

  // api.addFiles('js/materialize.js', 'client');
  api.addFiles('js/custom-script.js', 'client');
});

// Package.onTest(function(api) {
//   api.use('ecmascript');
//   api.use('tinytest');
//   api.use('yinchen618:materialize');
//   api.addFiles('materialize-tests.js');
// });
