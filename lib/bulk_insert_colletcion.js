/*
// Define some data you want to insert.
// Each element will be added (or updated) as a document to the collection
var data = [
  {
    firstName: "Daniel",
    lastName:  "Schroeder",
    email:     "daniel.phatthanan@example.com"
  },
  {
    firstName: "Carl",
    lastName:  "Jennings",
    email:     "carl.jennings@example.com"
  }
];

// define the collection
var students = new Meteor.Collection("students");

// bulk insert the data. The field email will be used as primary identifier for the records
bulkCollectionUpdate(students, data, {
  primaryKey: "email",
  callback: function() {
    console.log("Done. Collection now has " + students.find().count() + " documents.");
  }
});
*/
