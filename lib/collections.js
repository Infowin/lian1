System = new Mongo.Collection('system');
Pdfpath = new Mongo.Collection('pdfpath');

Mgyear = new Mongo.Collection('mgyear');

Addr1 = new Mongo.Collection('addr1');
Addr2 = new Mongo.Collection('addr2');

Family = new Mongo.Collection('family');
People = new Mongo.Collection('people');

TempleClass = new Mongo.Collection("templeclass");
TempleLevel = new Mongo.Collection("templelevel");

Acc1type = new Mongo.Collection("acc1type");
Acc2subject = new Mongo.Collection("acc2subject");
Acc3cash = new Mongo.Collection("acc3cash");

Addr1country = new Mongo.Collection("addr1country");
Addr2city = new Mongo.Collection("addr2city");
Addr3district = new Mongo.Collection("addr3district");
Addr4road = new Mongo.Collection("addr4road");

Pray1 = new Mongo.Collection("pray1");
Pray2 = new Mongo.Collection("pray2");

Praying1 = new Mongo.Collection("praying1");
Praying2 = new Mongo.Collection("praying2");
PrayingType = new Mongo.Collection("prayingtype");

Light1 = new Mongo.Collection("light1");
Light2 = new Mongo.Collection("light2");

Lighting1 = new Mongo.Collection("lighting1");
Lighting2 = new Mongo.Collection("lighting2");

Print = new Mongo.Collection("print");


///////////
Account = new Mongo.Collection("account"); // 會計帳本
Booking = new Mongo.Collection("booking"); // 會計科目


Clients = new Mongo.Collection("clients");
Portfolios = new Mongo.Collection("portfolios");
Interviews = new Mongo.Collection("interviews");

Products = new Mongo.Collection("products");
Provider = new Mongo.Collection("provider");

CustomerLoc = new Mongo.Collection("customerloc");
Teams = new Mongo.Collection("teams");

Cmslevel = new Mongo.Collection("cmslevel");
Cmsget = new Mongo.Collection("cmsget");
Accounting = new Mongo.Collection("accounting");

Files = new Mongo.Collection("files");
Oprec = new Mongo.Collection("oprec");