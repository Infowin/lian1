Router.configure({
  // we use the  appBody template to define the layout for the entire app
  layoutTemplate: 'hello',
  // layoutTemplate: 'appBody',

  // the appNotFound template is used for unknown routes and missing lists
  notFoundTemplate: 'appNotFound',

  // show the appLoading template whilst the subscriptions below load their data
  loadingTemplate: 'appLoading'//,
});

// Router.onBeforeAction(function() {
//   console.log("Router.onBeforeAction");
//   if (! Meteor.userId()) {
//     // this.render('login');
//     Router.go("login");
//   } else {
//     this.next();
// }
// });

dataReadyHold = null;

if (Meteor.isClient) {
  // Keep showing the launch screen on mobile devices until we have loaded
  // the app's data
  dataReadyHold = LaunchScreen.hold();

  // Show the loading screen on desktop
  Router.onBeforeAction('loading', {except: ['join', 'signin']});
  Router.onBeforeAction('dataNotFound', {except: ['join', 'signin']});
}

// Router.route('/family/:_id', {
//     name: 'create',
//     template: 'create',
//     data: function(){
//         // code goes here
//     }
// });

Router.map(function() {

  this.route('home', {
    path: '/'//,
    // action: function() {
    //   // Router.go('listsShow', Lists.findOne());
    // }
  });

  this.route('login', {
    path: '/login',
    template: 'login',
    layoutTemplate:"loginTemplate"
  });

  this.route('logout', {
    path: '/logout',
    template: 'logout',
    layoutTemplate:"loginTemplate"
  });
  this.route('register', {
    path: '/register',
    template: 'register',
    layoutTemplate:"loginTemplate"
  });
  this.route('forgot', {
    path: '/forgot',
    template: 'forgot',
    layoutTemplate:"loginTemplate"
  });

  this.route('b_people', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "已登記 信眾",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
      ];
    }
  });
  this.route('e_year', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "年度",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        Meteor.subscribe("Mgyear"),
      ];
    }
  });
  this.route('b_pray', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "法會",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
      ];
    }
  });
  this.route('b_light', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "點燈",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
      ];
    }
  });
  this.route('c_newclient', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "建檔",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        Meteor.subscribe("Addr1"),
        Meteor.subscribe("Pray1"),
        Meteor.subscribe("Pray2"),
        Meteor.subscribe("Light1"),
        Meteor.subscribe("Light2"),
        Meteor.subscribe("Mgyear"),
        Meteor.subscribe("templeClass"),
        Meteor.subscribe("templeLevel"),
        Meteor.subscribe("prayingtype"),
        // Meteor.subscribe("Products"),
      ];
    }
  });
  this.route('c_cashier', {
    data: function(){
      return {
        page_title: "出納",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        Meteor.subscribe("Addr1"),
        Meteor.subscribe("Pray1"),
        Meteor.subscribe("Pray2"),
        Meteor.subscribe("Light1"),
        Meteor.subscribe("Light2"),
        Meteor.subscribe("Mgyear"),
        // Meteor.subscribe("templeClass"),
        // Meteor.subscribe("templeLevel"),
        // Meteor.subscribe("prayingtype"),
        // Meteor.subscribe("Products"),
      ];
    }
  });
  this.route('c_accounting', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "會計",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        Meteor.subscribe("Addr1"),
        Meteor.subscribe("Pray1"),
        Meteor.subscribe("Pray2"),
        Meteor.subscribe("Light1"),
        Meteor.subscribe("Light2"),
        Meteor.subscribe("Mgyear"),
        // Meteor.subscribe("templeClass"),
        // Meteor.subscribe("templeLevel"),
        // Meteor.subscribe("prayingtype"),
        // Meteor.subscribe("Products"),
      ];
    }
  });
  this.route('c_print', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "列印",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        Meteor.subscribe("System"),
        Meteor.subscribe("Pray1"),
        Meteor.subscribe("Pray2"),
        Meteor.subscribe("Light1"),
        Meteor.subscribe("Light2"),
        Meteor.subscribe("Mgyear"),
        Meteor.subscribe("templeClass"),
        Meteor.subscribe("templeLevel"),
        Meteor.subscribe("prayingtype"),
      ];
    }
  });
  this.route('e_users', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "使用者帳號",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        Meteor.subscribe("userList"),
        Meteor.subscribe("Teams"),
      ];
    }
  });
  this.route('e_pray', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "法會種類",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
      ];
    }
  });
  this.route('e_prayprint', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "牌位列印",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        Meteor.subscribe("Pray1"),
      ];
    }
  });
  this.route('e_light', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "點燈",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
      ];
    }
  });
  this.route('e_tclass', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "信眾班別",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
      ];
    }
  });
  this.route('e_ttype', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "信眾身份",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        // Meteor.subscribe("Provider"),
      ];
    }
  });
  this.route('e_account', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "會計帳",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
        // Meteor.subscribe("Cmslevel"),
      ];
    }
  });
  this.route('e_operatingrec', {
    // path: '/print',
    // template: 'print',
    data: function(){
      return {
        page_title: "系統操作記錄",
        page_description: ""
      }
    },
    waitOn: function(){
      return [
      ];
    }
  });
});
