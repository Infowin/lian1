String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}

String.prototype.Blength = function() {
    var arr = this.match(/[^\x00-\xff]/ig);
    return  arr == null ? this.length : this.length + arr.length;
}

String.prototype.mydate = function() {
	var dd = new Date(this);
	return dd.getFullYear()+'-' + (dd.getMonth()+1) + '-'+dd.getDate() +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds();
}

String.prototype.splice = function( idx, rem, s ) {
    return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};

String.prototype.cutStrToArr = function(cutNum){
	var arrStr = [];
	var start = next = 0;

	if(this.length < cutNum){ // 不用分割
		arrStr.push(this.toString());
	}
	else{
		// console.log("ori: " + this);
		cutNum = cutNum - 3;
		while(start < this.length && next != -1){
	  
			next = this.indexOf(" ", start+cutNum);
			if(next != -1){
				arrStr.push(this.substring(start, next).trim());
				start = next;
			}
			else{
				arrStr.push(this.substring(start, this.length).trim());
				// console.log("start: "+ start + " next: "+ next );
			}
		}
	}
	// 每列的名字齊頭
	// console.log("arrStr");
	// console.log(arrStr);

	// 超過16個人的時候
	if(arrStr.length >= 6){
		cutNum = 17;
		arrStr = [];
		start = next = 0;

		cutNum = cutNum - 3;
		while(start < this.length && next != -1){
	  
			next = this.indexOf(" ", start+cutNum);
			if(next != -1){
				arrStr.push(this.substring(start, next).trim());
				start = next;
			}
			else{
				arrStr.push(this.substring(start, this.length).trim());
				// console.log("start: "+ start + " next: "+ next );
			}
		}

		// 每列的名字齊頭
		// console.log("arrStr");
		// console.log(arrStr);
	}

	/*for(var i in arrStr){
		arrStr[i] = arrStr[i]
		    .replace(/[\r]/g, 'r')
		    .replace(/[\b]/g, 'b')
		    .replace(/[\f]/g, 'f')
		    .replace(/[\n]/g, 'n')
		    .replace(/\\/g, '');
	}*/
	
	if(arrStr.length == 1){
	    return arrStr;
	}
	else if(arrStr.length > 1){
		// 先弄成名字的二維陣列
		var arrAll = [];
		for(i=0; i<arrStr.length; i++){
			arrAll.push(arrStr[i].split(" "));
		}
		// console.log("");
		// console.log("arrAll");
		// console.log(arrAll);
		// 如果有7位的時候，第一個的第三個 要放到第三個的第二個
		if(arrAll.length == 3){
			if(arrAll[1].length == 3 && arrAll[2].length == 1){
				arrAll[2].unshift(arrAll[1].pop());
			}
		}
		// 如果有10位的時候，第一個的第三個 要放到第三個的第二個
		else if(arrAll.length == 4){
			if(arrAll[2].length == 3 && arrAll[3].length == 1){
				arrAll[3].unshift(arrAll[2].pop());
			}
		}
		else if(arrAll.length == 5){
			// 如果有13位的時候，第一個的第三個 要放到第三個的第二個
			if(arrAll[3].length == 3 && arrAll[4].length == 1){
				arrAll[4].unshift(arrAll[3].pop());
			}
			// 如果有17位的時候，第一個的第三個 要放到第三個的第二個
			else if(arrAll[3].length == 4 && arrAll[4].length == 1){
				arrAll[4].unshift(arrAll[3].pop());
				arrAll[4].unshift(arrAll[1].pop());
			}
			// 18
			else if(arrAll[3].length == 4 && arrAll[4].length == 2){
				arrAll[4].unshift(arrAll[3].pop());
			}
		}
		// console.log(arrStr);

		var maxArr = 0;
		for(i=0; i<arrAll.length; i++){
			if(arrAll[i].length > maxArr) maxArr = arrAll[i].length;
		}
		var arrMaxNum = [];
		for(i=0; i<maxArr; i++){
			arrMaxNum.push(0);
		}
		for(i=0; i<arrAll.length; i++){
			for(j=0; j<arrAll[i].length; j++){
				if(!!arrAll[i][j] && arrAll[i][j].length > arrMaxNum[j])
					arrMaxNum[j] = arrAll[i][j].length;
			}
		}
		// console.log("arrMaxNum");
		// console.log(arrMaxNum);

		for(i=0; i<arrAll.length; i++){
			for(j=0; j<arrAll[i].length; j++){
				if(!!arrAll[i][j] && arrAll[i][j].length < arrMaxNum[j]){
					var sp = "";
					for(k=0; k< arrMaxNum[j]-arrAll[i][j].length; k++){
						sp += " ";
					}
					arrAll[i][j] += sp;
				}
			}
		}
		// console.log("arrAll");
		// console.log(arrAll);

		var arrAll2 = [];
		for(i=0; i<arrAll.length; i++){
			arrAll2.push(arrAll[i].join(" "));
		}
		// console.log("arrAll2");
		// console.log(arrAll2);

    	return arrAll2;
	}
}

String.prototype.commaToSpace = function(){
    return this.replace(/,/g, " ");
}

String.prototype.toStraight = function(){
    var hex, i;

    var result = "";
    for (i=0; i<this.length; i++) {
        hex = this.charAt(i) + "\n";
        result += hex;
    }
    return result
}

// prayAlayout = {};
String.prototype.hexEncode = function(){
    var hex, i;

    var result = "";
    for (i=0; i<this.length; i++) {
        hex = this.charCodeAt(i).toString(16);
        result += ("000"+hex).slice(-4);
    }

    return result
}

String.prototype.hexDecode = function(){
    var j;
    var hexes = this.match(/.{1,4}/g) || [];
    var back = "";
    for(j = 0; j<hexes.length; j++) {
        back += String.fromCharCode(parseInt(hexes[j], 16));
    }

    return back;
}

funcPrayAlayout = function (obj){

	// console.log(obj.data);
	var arrObj = obj.data;

	function pageData(str_top, str_left, str_center, str_right){
	// function pageData(){
		return [
			{			
				columns:[
	                {
	                  width: 140,
	                  text: ''
	                },
	                {
	                  width: 20,
	 			      fontSize: 18,
	                  text: '|'
	                },
	                {
	                  width: 200,
	 			      fontSize: 18,
	 			      alignment:"center",
	                  // text: 'hea總燈主der here 123'
	                  text: str_top
	                },
	                {
	                  width: 20,
	 			      fontSize: 18,
	                  text: '|'
	                }
	            ],
	            margin: [0, 0, 0, 20]
			},
			{			
				columns:[
	                {
	                  width: 132,
	                  text: ''
	                },
	                {
	        	        image: prayA_top,
	        	        width: 250
	        		}   
	            ]	
			},
	    	{
	    	    columns:[
	                { // total width: 160
	                  width: 100,
	                  text: ''
	                },
	                {
	                	width: 60,
						alignment:"right",
						margin: [0, 0, 5, 0],
	 			    	fontSize: 20,
	                 	text: str_left
	                },
	        	    {
	        	        image: prayA_middle,
	        	        width: 20,
	        	        height: 460
	        		},
	                {
						width: 158,
						alignment:"center",
						fontSize: 30,
						margin: [5, 20, 5, 20],
						text: str_center
	                },
	        	    {
	        	        image: prayA_middle,
	        	        width: 20,
	        	        height: 460
	        		}, 
	                {
						width: 24,
						fontSize: 20,
						margin: [5, 0, 0, 0],
						text: str_right
	                }
	    	    ]
	    	},
	    	{
	    	    columns:[
	                {
	                  width: 132,
	                  text: ''
	                },
	        	    {
	        	        image: prayA_bottom,
	        	        width: 250
	        		}    
	    	    ]
	    	},
			{			
				columns:[
	                {
	                  width: 140,
	                  text: ''
	                },
	                {
	                  width: 20,
	 			      fontSize: 18,
	                  text: '|'
	                },
	                {
	                  width: 200,
	 			      // fontSize: 18,
	 			      // alignment:"center",
	                  text: ''
	                },
	                {
	                  width: 20,
	 			      fontSize: 18,
	                  text: '|'
	                }
	            ],
	            margin: [0, 30, 0, 0]
			}
		];
	}

	var content = [];
	
	arrObj.forEach(function(entry) {
		if(!!entry.type_text && !!entry.prayitem_text && !!entry.prayserial && !!entry.livename_text){

			var top = entry.prayitem_text + " " + entry.prayserial;
			if(entry.type_text.indexOf("拔") >= 0){
				var left = entry.livename_text;
				var center = entry.passname_text;
			}
			else{
				var left = "";
				var center = entry.livename_text;
			}
			var right = entry.addr || "";
			
			content.push(pageData(top, left, center, right));
		}
	});
	return content;
}