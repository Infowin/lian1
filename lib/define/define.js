arrTime = [{id:0, value:"吉"},{id:1, value:"子"},{id:2, value:"丑"},{id:3, value:"寅"},{id:4, value:"卯"},{id:5, value:"辰"},{id:6, value:"巳"},{id:7, value:"午"},{id:8, value:"未"},{id:9, value:"申"},{id:10, value:"酉"},{id:11, value:"戌"},{id:12, value:"亥"}, {id:13, value:"夜子"}];
arrTime2 = ["吉", "子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戍", "亥", "夜子"];//戌
// arrSexual = [{id:1, value:"女"},{id:2, value:"男"},{id:3, value:""}];
// arrSexual2 = ["", "女", "男"];
arrSexual = [{id:1, value:"男"},{id:2, value:"女"}];
objSexual2 = [{id:1, value:"男"},{id:2, value:"女"}];
arrSexual2 = ["", "男", "女"];

arrChineseYear = [{id:1, value:"鼠"},{id:2, value:"牛"},{id:3, value:"虎"},{id:4, value:"兔"},{id:5, value:"龍"},{id:6, value:"蛇"},{id:7, value:"馬"},{id:8, value:"羊"},{id:9, value:"猴"},{id:10, value:"雞"},{id:11, value:"狗"},{id:12, value:"豬"}];
arrChineseYear2 = ["", "鼠", "牛", "虎", "兔", "龍", "蛇", "馬", "羊", "猴", "雞", "狗", "豬"];

arrTruth = [{id:"-1", value:"有"},{id:"0", value:"無"}];

arrMGYear = [{ id:106, value:"106" }, { id:105, value:"105" }, { id:104, value:"104" }, { id:103, value:"103" }, { id:102, value:"102" }, { id:101, value:"101" }];
arrMGYear2 = [{ id:0, value:"" }, { id:106, value:"106" }, { id:105, value:"105" }, { id:104, value:"104" }, { id:103, value:"103" }, { id:102, value:"102" }, { id:101, value:"101" }];
        
arrSelEmpty = [{ id:"", value:""}];                             
arrSelMember = [{ id:"0", value:""}, { id:"2", value:"信眾"}];
objTrueFalse = [{ id:"0", value:"無"}, { id:"-1", value:"有"}];

// objOilPrayPaper = [{ id:"", value:"無"}, { id:"1", value:"A4直式"}, { id:"2", value:"A4橫式"}];
objPaperSize = [{ id:"0", value:"自訂"}, { id:"1", value:"無牌位"}, { id:"A3", value:"A3"}, { id:"B4", value:"B4"}, { id:"A4", value:"A4"}];
objPaperLayout = [{ id:"1", value:"直式"}, { id:"2", value:"橫式"}];
objOilPrayX = [{ id:"0", value:""}, { id:"1", value:"1"}, { id:"2", value:"2"}, { id:"3", value:"3"}, { id:"4", value:"4"}, { id:"5", value:"5"}, { id:"6", value:"6"}];                                
objOilPrayY = [{ id:"0", value:""}, { id:"1", value:"1"}, { id:"2", value:"2"}, { id:"3", value:"3"}, { id:"4", value:"4"}, { id:"5", value:"5"}, { id:"6", value:"6"}];    

objPrintType = [{ id:"0", value:"不需印"}, { id:"1", value:"一般牌位 消"}, { id:"2", value:"一般牌位 拔"}, { id:"3", value:"油燈牌位"}, { id:"4", value:"功德名條"}, { id:"5", value:"功德名條+桌牌"}];    

objDefaultType = [{ id:"0", value:"否"}, { id:"-1", value:"是"}];    

objUserRole = [
    // { id:"", value:""}, 
    { id:"10", role:"agent", value:"訪客"}, 
    { id:"30", role:"commission", value:"志工"}, 
    // { id:"40", role:"director", value:""}, 
    { id:"50", role:"supervisor", value:"會計/出納"}, 
    { id:"95", role:"designer", value:"系統開發者"},
    { id:"90", role:"administrative", value:"董監事"}, 
    { id:"99", role:"admin", value:"住持"}
];


objPayMethod = [
    { id:"1", value:"現金"}, 
    { id:"2", value:"支票"}, 
    { id:"3", value:"匯款"}, 
    { id:"0", value:"無"}, 
];

bool_set = {
    "true":"On",
    "false":"Off",
    "undefined":"Off"
};

segmentLightOpt = [
    { id:"1", value:"燈牌標籤" }, 
    { id:"2", value:"簡易疏文" }, 
    { id:"3", value:"詳細疏文" }
];

segmentPrayOpt = [
    { id:"1", value:"牌位" },
    { id:"2", value:"油燈牌" }, 
    { id:"3", value:"疏文" },
    { id:"4", value:"功德名條" },
    { id:"5", value:"普施圓桌" }
];
segmentMemberOpt = [
    { id:"1", value:"簽到名冊" }, 
    { id:"2", value:"名冊封面" }, 
    { id:"3", value:"社員名冊" },
    { id:"4", value:"郵寄標籤" }
];




years = [];
var year = new Date();
for (var i = 0; i <= (year.getFullYear()-1911); i++)
    years.push({ id:i, value: i });

months = [];
for (var i = 1; i <= 12; i++)
    months.push({ id:i, value: i });

days = [];
for (var i = 1; i <= 31; i++)
    days.push({ id:i, value: i });

arrPageSize = ["", "A3", "B4", "A4"];
objPageSize = [{ id:0, value:"" }, { id:1, value:"A3" }, { id:2, value:"B4" }, { id:3, value:"A4" } ];


funcObjFind = function (obj, id){
    var ret = "";
    obj.forEach(function(entry) {
        // console.log(entry);
        if(Number(entry.id) == id){
            ret = entry.value;
        }
    });
    return ret;
}

currentdate = function() {
    var dd = new Date();
    return dd.getFullYear()+'/' + (dd.getMonth()+1) + '/'+dd.getDate(); // +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds()
}
currentdate2 = function() {
    var dd = new Date();
    return dd.getFullYear()+'-' + (dd.getMonth()+1) + '-'+dd.getDate(); // +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds()
}
currentMGyear = function() {
    var dd = new Date();
    return Number(dd.getFullYear()-1911);
}
nextMGyear = function() {
    var dd = new Date();
    return Number(dd.getFullYear()-1911)+1;
}

Date.prototype.yyyymmddhm = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();
    var h  = this.getHours().toString();
    var m  = this.getMinutes().toString();
    // var s  = this.getSeconds().toString();
    return yyyy + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + " " + (h[1]?h:"0"+h[0]) + ":" + (m[1]?m:"0"+m[0]); // padding
};

funcStrPad = function(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

funcPad = function(num, size) {
    var s = "00000000" + num;
    return s.substr(s.length-size);
}


// 檢查這個object是不是空的
// Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

isEmpty = function(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}


// http://liaosankai.pixnet.net/blog/post/24165900-%E8%BA%AB%E4%BB%BD%E8%AD%89%E9%A9%97%E8%AD%89%E7%A8%8B%E5%BC%8F-for-javascript-(%E7%B2%BE%E7%B0%A1%E7%89%88)
//***************************
// 台灣身份證產生簡短版 javascript 版
//***************************
getTwID = function(value){
    //建立字母分數陣列(A~Z)
    var city = new Array(
         1,10,19,28,37,46,55,64,39,73,82, 2,11,
        20,48,29,38,47,56,65,74,83,21, 3,12,30
    )
    //建立隨機身份證碼
    var id = new Array();

    if(value.length != 10){
        return false;
    }

    for (var i = 0; i < value.length-1; i++) {
        id[i] = value.charAt(i);
    };

    // id[0] = String.fromCharCode(Math.floor(Math.random() * (26)) + 65);
    // id[1] = Math.floor(Math.random() * (2)) + 1;
    // for(var i=2; i<9; i++){
    //     id[i] = Math.floor(Math.random() * (9)) + 0;
    // }


    //計算總分
    var total = city[id[0].charCodeAt(0)-65];
    for(var i=1; i<=8; i++){
        total += eval(id[i]) * (9 - i);
    }
    //計算最尾碼
    var total_arr = (total+'').split('');
    var lastChar = eval(10-total_arr[total_arr.length-1]);
    var lastChar_arr = (lastChar+'').split('');
    //補上最後檢查碼
    id[id.length++] = lastChar_arr[lastChar_arr.length-1];

    if(id[9] == value[9]){
        return true;
    }
    return false;
    //回傳結果
    // return id.join('');
}



