
funcLunar = function(tt, yy, mm, dd, ll){
	if (typeof ll === 'undefined') { ll = 0; }

	var BYEAR = 1201
	var Nyear = 150;
	var Nmonth = 13
	var title;
	// var y, m, d;
	var jieAlert;

	function make_array() {
	    var tmparg = make_array.arguments;
	    for (var i = 0; i < tmparg.length; i++) { this[i] = tmparg[i]; }
	    this.length = tmparg.length; }

	function make_n_array(num) {
	    for (var i = 0; i < num; i++) { this[i] = i; }
	    this.length = num; }

	function MyDate(y, m, d, h, w, l) { 
		this.y = y;
	    this.m = m;
	    this.d = d;
	    this.h = h;
	    this.w = w;
	    this.l = l; 
	}
	var yearInfo = new make_array(
		0x04bd8, 0x04ae0, 0x0a570, 0x054d5, 0x0d260, 
		0x0d950, 0x16554, 0x056a0, 0x09ad0, 0x055d2, 0x04ae0, 
		
		/*0x0a5b6, 0x0a4d0, 0x0d250, 0x1d255, 0x0b540, 
		0x0d6a0, 0x0ada2, 0x095b0, 0x14977, 0x04970, 
		0x0a4b0, 0x0b4b5, 0x06a50, 0x06d40, 0x1ab54, 
		0x02b60, 0x09570, 0x052f2, 0x04970, 0x06566, 
		0x0d4a0, 0x0ea50, 0x06e95, 0x05ad0, 0x02b60, 
		0x186e3, 0x092e0, 0x1c8d7, 0x0c950, 0x0d4a0, 
		0x1d8a6, 0x0b550, 0x056a0, 0x1a5b4, 0x025d0, 
		0x092d0, 0x0d2b2, 0x0a950, 0x0b557, 0x06ca0, 
		0x0b550, 0x15355, 0x04da0, 0x0a5d0, 0x14573, 
		0x052d0, 0x0a9a8, 0x0e950, 0x06aa0, 0x0aea6, 
		0x0ab50, 0x04b60, 0x0aae4, 0x0a570, 0x05260, 
		0x0f263, 0x0d950, 0x05b57, 0x056a0, 0x096d0, 
		0x04dd5, 0x04ad0, 0x0a4d0, 0x0d4d4, 0x0d250, 
		0x0d558, 0x0b540, 0x0b5a0, 0x195a6, 0x095b0, 
		0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50, 
		0x06d40, 0x0af46, 0x0ab60, 0x09570, 0x04af5, 
		0x04970, 0x064b0, 0x074a3, 0x0ea50, 0x06b58, 
		0x055c0, 0x0ab60, 0x096d5, 0x092e0, 0x0c960, 
		0x0d954, 0x0d4a0, 0x0da50, 0x07552, 0x056a0, 
		0x0abb7, 0x025d0, 0x092d0, 0x0cab5, 0x0a950, 
		0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9, 0x04ba0, 
		0x0a5b0, 0x15176, 0x052b0, 0x0a930, 0x07954, 
		0x06aa0, 0x0ad50, 0x05b52, 0x04b60, 0x0a6e6, 
		0x0a4e0, 0x0d260, 0x0ea65, 0x0d530, 0x05aa0, 0x076a3, */

// https://www.javaworld.com.tw/jute/post/view?bid=20&id=308501
		0x0a5b6, 0x0a4d0, 0x0d250, 0x1d295, 0x0b540,  /* 1915 */
		0x0d6a0, 0x0ada2, 0x095b0, 0x14977, 0x049b0,  /* 1920 */
		0x0a4b0, 0x0b4b5, 0x06a50, 0x06d40, 0x1ab54,  /* 1925 */
		0x02b60, 0x09570, 0x052f2, 0x04970, 0x06566,  /* 1930 */
		0x0d4a0, 0x0ea50, 0x16a95, 0x05ad0, 0x02b60,  /* 1935 */
		0x186e3, 0x092e0, 0x1c8d7, 0x0c950, 0x0d4a0,  /* 1940 */
		0x1d8a6, 0x0b550, 0x056a0, 0x1a5b4, 0x025d0,  /* 1945 */
		0x092d0, 0x0d2b2, 0x0a950, 0x0b557, 0x06ca0,  /* 1950 */
		0x0b550, 0x15355, 0x04da0, 0x0a5b0, 0x14573,  /* 1955 */
		0x052b0, 0x0a9a8, 0x0e950, 0x06aa0, 0x0aea6,  /* 1960 */
		0x0ab50, 0x04b60, 0x0aae4, 0x0a570, 0x05260,  /* 1965 */
		0x0f263, 0x0d950, 0x05b57, 0x056a0, 0x096d0,  /* 1970 */
		0x04dd5, 0x04ad0, 0x0a4d0, 0x0d4d4, 0x0d250,  /* 1975 */
		0x0d558, 0x0b540, 0x0b6a0, 0x195a6, 0x095b0,  /* 1980 */
		0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50,  /* 1985 */
		0x06d40, 0x0af46, 0x0ab60, 0x09570, 0x04af5,  /* 1990 */
		0x04970, 0x064b0, 0x074a3, 0x0ea50, 0x06b58,  /* 1995 */
		0x05ac0, 0x0ab60, 0x096d5, 0x092e0, 0x0c960,  /* 2000 */
		0x0d954, 0x0d4a0, 0x0da50, 0x07552, 0x056a0,  /* 2005 */
		0x0abb7, 0x025d0, 0x092d0, 0x0cab5, 0x0a950,  /* 2010 */
		0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9, 0x04ba0,  /* 2015 */
		0x0a5b0, 0x15176, 0x052b0, 0x0a930, 0x07954,  /* 2020 */
		0x06aa0, 0x0ad50, 0x05b52, 0x04b60, 0x0a6e6,  /* 2025 */
		0x0a4e0, 0x0d260, 0x0ea65, 0x0d530, 0x05aa0,  /* 2030 */
		0x076a3,

		         0x096d0, 0x04bd7, 0x04ad0, 0x0a4d0, 
		0x1d0b6, 0x0d250, 0x0d520, 0x0dd45, 0x0b5a0, 
		0x056d0, 0x055b2, 0x049b0, 0x0a577, 0x0a4b0, 
		0x0aa50, 0x1b255, 0x06d20, 0x0ada0);
	var fest = new make_array(4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 7, 8, 8, 8, 9, 8, 8, 6, 5, 7, 6, 7, 7, 8, 9, 9, 9, 8, 8, 7, 5, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 7, 6, 7, 7, 8, 9, 9, 9, 8, 8, 7, 5, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 7, 6, 7, 7, 8, 9, 9, 9, 8, 8, 7, 5, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 7, 8, 8, 9, 9, 8, 8, 6, 5, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 7, 8, 8, 9, 9, 8, 8, 6, 5, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 9, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 7, 8, 8, 9, 9, 8, 8, 6, 5, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 7, 8, 8, 8, 9, 8, 8, 6, 5, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 7, 8, 8, 8, 9, 8, 8, 6, 5, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 6, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 6, 5, 5, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 5, 5, 6, 7, 7, 8, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 5, 5, 6, 7, 7, 8, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 5, 5, 6, 7, 7, 8, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 5, 5, 6, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 5, 5, 6, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 5, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 8, 8, 8, 9, 8, 8, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 5, 5, 5, 5, 5, 8, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 5, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 5, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 5, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 9, 8, 7, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 3, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 5, 5, 6, 7, 7, 8, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 5, 4, 5, 5, 6, 7, 7, 8, 7, 7, 5, 3, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 5, 5, 6, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 8, 7, 6, 4, 5, 4, 5, 5, 6, 7, 7, 8, 7, 6, 5, 3, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 5, 4, 5, 5, 6, 7, 7, 8, 7, 6, 5, 3, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 6, 6, 7, 8, 8, 8, 7, 7, 6, 4, 5, 4, 5, 5, 6, 7, 7, 8, 7, 6, 5, 3, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 5, 4, 5, 5, 6, 7, 7, 8, 7, 6, 5, 3, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 5, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 5, 4, 5, 5, 6, 7, 7, 8, 7, 6, 5, 3, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 5, 4, 5, 5, 6, 7, 7, 7, 7, 6, 5, 3, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 5, 4, 5, 5, 7, 7, 7, 8, 7, 7, 5, 4, 6, 5, 5, 6, 7, 7, 8, 8, 7, 7, 6, 4, 5, 4, 5, 5, 6, 7, 7, 7, 7, 6, 5, 3, 5, 4, 5, 5, 6, 7, 7, 8, 7, 7, 5);
	var ymonth = new make_n_array(Nyear);
	var yday = new make_n_array(Nyear);
	var mday = new make_n_array(Nmonth + 1);
	var moon = new make_array(29, 30);
	var weekdayGB = new make_array("日", "一", "二", "三", "四", "五", "六");
	var ShengXiaoGB = new make_array("鼠", "牛", "虎", "兔", "龍", "蛇", "馬", "羊", "猴", "雞", "狗", "豬");
	var GanGB = new make_array("甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸");
	var ZhiGB = new make_array("子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥");
	var daysInSolarMonth = new make_array(0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var solarFirstDate = new MyDate(1900, 1, 31, 0, 3, 0);
	var LunarFirstDate = new MyDate(1900, 1, 1, 0, 3, 0);
	var GanFirstDate = new MyDate(6, 4, 0, 0, 3, 0);
	var ZhiFirstDate = new MyDate(0, 2, 4, 0, 3, 0);
	var solar = new MyDate(0, 0, 0, 0, 0, 0);
	var lunar = new MyDate(0, 0, 0, 0, 0, 0);
	var gan = new MyDate(0, 0, 0, 0, 0, 0);
	var zhi = new MyDate(0, 0, 0, 0, 0, 0);
	var gan2 = new MyDate(0, 0, 0, 0, 0, 0);
	var zhi2 = new MyDate(0, 0, 0, 0, 0, 0);
	var lunar2 = new MyDate(0, 0, 0, 0, 0, 0);

	// function report(type, ans) {
	function report(type) {
	    var answer, answer2;
	    if (type == 0) { 
	    	answer = "陰曆" + lunar.y + "(" + GanGB[gan.y] + ZhiGB[zhi.y] + ")年" + (lunar.l ? "閏" : "") + lunar.m + "月" + lunar.d + "日,生肖屬" + ShengXiaoGB[zhi.y]; 
	    } 
	    else { 
	    	// answer = "陽曆" + solar.y + "年" + solar.m + "月" + solar.d + "日,星期" + weekdayGB[solar.w]; 
	    	var mg = Number(solar.y)-1911;
	    	if(mg == -11){
	    		answer = "閏月錯誤";
	    	}
	    	else{
		    	answer = mg + "." + solar.m + "." + solar.d; 
	    	}
	    }
		return answer;	    
	    // ans.value = answer; 
	}

	function LeapYear(y) {
	    return (((y % 4) == 0) && ((y % 100) != 0) || ((y % 400) == 0)); }

	function solar2Day1(d) {
	    var offset, delta;
	    var i;
	    delta = d.y - BYEAR;
	    if (delta < 0) { 
	    	// alert("internal error:pick a larger constant for BYEAR");
	    	console.log("internal error:pick a larger constant for BYEAR");
	        return -1; }
	    offset = Math.floor(delta * 365) + Math.floor(delta / 4) - Math.floor(delta / 100) + Math.floor(delta / 400);
	    for (i = 1; i < d.m; i++) { offset += daysInSolarMonth[i]; }
	    if ((d.m > 2) && (LeapYear(d.y))) offset++;
	    offset += d.d - 1;
	    if ((d.m == 2) && LeapYear(d.y)) {
	        if (d.d > 29) { 
	        	// alert("day1 out of range");
	        	console.log("day1 out of range");
	            return -1; } } else if (d.d > daysInSolarMonth[d.m]) { alert("day2 out of range");
	        return -1; }
	    return offset; }

	function solar2Day(d) {
	    return (solar2Day1(d) - solar2Day1(solarFirstDate)); 
	}

	function make_yday() {
	    var year, i, leap;
	    var code;
	    for (year = 0; year < Nyear; year++) { code = yearInfo[year];
	        leap = code & 0xf;
	        yday[year] = 0;
	        if (leap != 0) { i = (code >> 16) & 0x1;
	            yday[year] += moon[i]; }
	        code >>= 4;
	        for (i = 0; i < Nmonth - 1; i++) { yday[year] += moon[code & 0x1];
	            code >>= 1; }
	        ymonth[year] = 12;
	        if (leap != 0) ymonth[year]++; }
	    return Nyear; 
	}

	function make_mday(year) {
	    var i, leapMonth, code;
	    code = yearInfo[year];
	    leapMonth = code & 0xf;
	    code >>= 4;
	    if (leapMonth == 0) { 
	    	mday[Nmonth] = 0;
	        for (i = Nmonth - 1; i >= 1; i--) { mday[i] = moon[code & 0x1];
	            code >>= 1; } 
	    }
	    else { 
	    	i = (yearInfo[year] >> 16) & 0x1;
	        mday[leapMonth + 1] = moon[i];
	        for (i = Nmonth; i >= 1; i--) {
	            if (i == leapMonth + 1) i--;
	            mday[i] = moon[code & 0x1];
	            code >>= 1; 
	        } 
	    }
	    return leapMonth; }

	function day2Lunar(offset, d) {
	    var i, m, nYear, leapMonth;
	    nYear = make_yday();
	    for (i = 0; i < nYear && offset > 0; i++) offset -= yday[i];
	    if (offset < 0) offset += yday[--i];
	    if (i == Nyear) { 
	    	// alert("Year out of range.");
	    	console.log("Year out of range.");
	        return; }
	    d.y = i + LunarFirstDate.y;
	    leapMonth = make_mday(i);
	    for (m = 1; m <= Nmonth && offset > 0; m++) offset -= mday[m];
	    if (offset < 0) offset += mday[--m];
	    d.l = 0;
	    if (leapMonth > 0) { d.l = (leapMonth == (m - 1));
	        if (m > leapMonth) --m; }
	    d.m = m;
	    d.d = offset + 1; }

	function CalGZ(offset, d, g, z) {
	    var year, month;
	    year = d.y - LunarFirstDate.y;
	    month = year * 12 + d.m - 1;
	    g.y = (GanFirstDate.y + year) % 10;
	    z.y = (ZhiFirstDate.y + year) % 12;
	    g.m = (GanFirstDate.m + month) % 10;
	    z.m = (ZhiFirstDate.m + month) % 12;
	    g.d = (GanFirstDate.d + offset) % 10;
	    z.d = (ZhiFirstDate.d + offset) % 12;
	    z.h = Math.floor((d.h + 1) / 2) % 12;
	    g.h = (g.d * 12 + z.h) % 10; }

	function CmpDate(month1, day1, month2, day2) {
	    if (month1 != month2) return (month1 - month2);
	    if (day1 != day2) return (day1 - day2);
	    return (0); }

	function JieDate(ds, dl) {
	    var m, flag;
	    if (ds.m == 1) { flag = CmpDate(ds.m, ds.d, 1, fest[(ds.y - solarFirstDate.y - 1) * 12 + 11]);
	        if (flag < 0) dl.m = 11;
	        else if (flag > 0) dl.m = 12;
	        dl.y = ds.y - 1;
	        return (flag == 0); }
	    for (m = 2; m <= 12; m++) { flag = CmpDate(ds.m, ds.d, m, fest[(ds.y - solarFirstDate.y) * 12 + m - 2]);
	        if (flag == 0) m++;
	        if (flag <= 0) break; }
	    dl.m = (m - 2) % 12;
	    dl.y = ds.y;
	    if ((dl.m) == 0) { dl.y = ds.y - 1;
	        dl.m = 12; }
	    return (flag == 0); }

	function solar2Lunar() {
	    var offset;
	    offset = solar2Day(solar);
	    solar.w = (offset + solarFirstDate.w) % 7;
	    if (solar.h == 23) offset++;
	    day2Lunar(offset, lunar);
	    lunar.h = solar.h;
	    CalGZ(offset, lunar, gan, zhi);
	    jieAlert = JieDate(solar, lunar2);
	    lunar2.d = lunar.d;
	    lunar2.hour = lunar.hour;
	    CalGZ(offset, lunar2, gan2, zhi2); }

	function Lunar2Day(d) {
	    var offset = 0;
	    var year, i, m, nYear, leapMonth;
	    nYear = make_yday();
	    year = d.y - LunarFirstDate.y;
	    for (i = 0; i < year; i++) offset += yday[i];
	    leapMonth = make_mday(year);
	    if ((d.l) && (leapMonth != d.m)) { 
	    	// alert(d.m + "is not a leap month in year " + d.y);
	    	console.log(d.m + " is not a leap month in year " + d.y);
	        return -1; }
	    for (m = 1; m < d.m; m++) offset += mday[m];
	    if (leapMonth && ((d.m > leapMonth) || (d.l && (d.m == leapMonth)))) offset += mday[m++];
	    offset += d.d - 1;
	    if (d.d > mday[m]) Error("Day out of range.");
	    return offset; 
	}

	function Solar2Day(d) {
	    return (Solar2Day1(d) - Solar2Day1(solarFirstDate)); }

	function Solar2Day1(d) {
	    var offset, delta;
	    var i;
	    delta = d.y - BYEAR;
	    if (delta < 0) { 
	    	// alert("Internal error: pick a larger constant for BYEAR.");
	    	console.log("Internal error: pick a larger constant for BYEAR.");
	        return -1; }
	    offset = delta * 365 + Math.floor(delta / 4) - Math.floor(delta / 100) + Math.floor(delta / 400);
	    for (i = 1; i < d.m; i++) offset += daysInSolarMonth[i];
	    if ((d.m > 2) && LeapYear(d.y)) offset++;
	    offset += d.d - 1;
	    if ((d.m == 2) && LeapYear(d.y)) {
	        if (d.d > 29) { 
	        	// alert("Day out of range.");
	        	console.log("Day out of range.");
	            return -1; 
	        } 
	    } 
	    else if (d.d > daysInSolarMonth[d.m]) { 
	    	// alert("Day out of range.");
	    	console.log("Day out of range.");
	        return -1; }
	    return offset; 
	}

	function Day2Solar(offset, d) { // yin -> yang 1
	    var i, m, days;
	    offset -= Solar2Day(LunarFirstDate);
	    for (i = solarFirstDate.y;
	        (i < solarFirstDate.y + Nyear) && (offset > 0); i++) offset -= 365 + LeapYear(i);
	    if (offset < 0) {--i;
	        offset += 365 + LeapYear(i); }
	    if (i == (solarFirstDate.y + Nyear)) { 
	    	// alert("Year out of range.");
	    	console.log("Year out of range.");
	        return; }
	    d.y = i;
	    for (m = 1; m <= 12; m++) { days = daysInSolarMonth[m];
	        if ((m == 2) && LeapYear(i)) days++;
	        if (offset < days) { d.m = m;
	            d.d = offset + 1;
	            return; }
	        offset -= days; } }

	function Lunar2Solar() { // yin -> yang 2
	    var offset;
	    var adj;
	    var d;
	    adj = (lunar.h == 23) ? -1 : 0;
	    offset = Lunar2Day(lunar);
	    solar.w = (offset + adj + solarFirstDate.w) % 7;
	    Day2Solar(offset + adj, solar);
	    solar.h = lunar.h;
	    CalGZ(offset, lunar, gan, zhi);
	    jieAlert = JieDate(solar, lunar2);
	    lunar2.d = lunar.d;
	    lunar2.h = lunar.h;
	    CalGZ(offset, lunar2, gan2, zhi2); }

	function Lunar(type, my_y, my_m, my_d, my_l) {
	    if (type == 0) { solar.y = my_y;
	        solar.m = my_m;
	        solar.d = my_d;
	        title = "陰曆:";
	        solar2Lunar(); 
	    } 
	    else { 
	    	lunar.y = my_y;
	        lunar.m = my_m;
	        lunar.d = my_d;
	        lunar.l = my_l;
	        title = "陽曆:";
	        Lunar2Solar(); } 
	    }

	function get_select_value(myinput) {
	    var i;
	    if (myinput.type == "text") {
	        return myinput.value; } else if (myinput.type == "select-one") {
	        return myinput[myinput.selectedIndex].value; }
	    return 0; }

	function check_ymd(y, m, d, myerr) {
	    if (y < 1901 || y > 2049) { myerr.value = "年份只能介於 1901 至 2049 之間";
	        return false; }
	    switch (m) {
	        case 2:
	            if ((y % 4 == 0) && d > 29) { myerr.value = "" + m + "月只有 29天";
	                return false; } else if ((y % 4 != 0) && d > 28) { myerr.value = "" + m + "月只有 28天";
	                return false; }
	            break;
	        case 4:
	        case 6:
	        case 9:
	        case 11:
	            if (d > 30) { myerr.value = "" + m + "月只有 30天";
	                return false; } }
	    return true; }

	function mainx() {
	    var type;
	    var x = document.forms["ymd"];
	    var year, month, day;
	    year = parseInt(get_select_value(x.year), 10);
	    if (isNaN(year)) { x.answer.value = "年份只能填數字,你亂填喔..";
	        return; 
	    }
	    month = parseInt(get_select_value(x.month), 10);
	    day = parseInt(get_select_value(x.day), 10);
	    if (!check_ymd(year, month, day, x.answer)) {
	        return; }
	    type = parseInt(get_select_value(x.type), 10);
	    Lunar(type, year, month, day);
	    report(type, x.answer); 
	}

    var type; // 0: 國-> 農, 1: 農->國
    Lunar(Number(tt), Number(yy), Number(mm), Number(dd),  Number(ll));
    var res = report(tt); 
	// console.log(res);
	return res;

};

numToChinese = function(str) {
	str = str.toString()
	str = str.replace(/,/g, "");
	var AU = [];
	var ans = "";
	var chi = "零壹貳參肆伍陸柒捌玖";
	for (var i = 0; i < str.length; i++)
	  ans += chi.charAt(parseInt(str.charAt(i)));
	for (var i = ans.length; i > 0; i--) {
	  var U = "";
	  var k = i;
	  while (k - 13 >= 0) {
		k -= 12;
		U += "兆";
	  }
	  while (k - 9 >= 0) {
		k -= 8;
		U += "億";
	  }
	  while (k - 5 >= 0) {
		k -= 4;
		U += "萬";
	  }
	  if (k > 1) U = "";
	  while (k - 4 >= 0) {
		k -= 3;
		U += "仟";
	  }
	  while (k - 3 >= 0) {
		k -= 2;
		U += "佰";
	  }
	  while (k - 2 >= 0) {
		k -= 1;
		U += "拾";
	  }
	  var tU = "";
	  var r = U.length;
	  while (r-- > 0) tU = tU + U.charAt(r);
	  if (tU != "") AU.push(tU);
	}
	AU.push("");
	var final = "";
	for (i = 0; i < ans.length; i++) {
	  final += ans.charAt(i) + AU[i];
	}
	while (final.match(/零[仟佰拾]零/)) final = final.replace(/零[仟佰拾]零/g, "零");
	final = final
	  .replace(/零[仟佰拾]/g, "零")
	  .replace(/零兆/g, "兆")
	  .replace(/零億/g, "億")
	  .replace(/零萬/g, "萬");
	final = final
	  .replace(/兆[仟佰拾億萬][兆億萬仟佰拾]*/g, "兆零")
	  .replace(/億[仟佰拾萬][兆億萬仟佰拾]*/g, "億零")
	  .replace(/萬[仟佰拾][兆億萬仟佰拾]*/g, "萬零");
	final = final.replace(/^壹拾/, "拾");
	while (final.match(/零$/)) final = final.replace(/零$/, "");
	return final;
  };

